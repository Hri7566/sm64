/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage15 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx gfx_e8_base[];

extern Gfx gfx_e8_bg01[];
extern Gfx gfx_e8_bg02[];
extern Gfx gfx_e8_bg03[];
extern Gfx gfx_e8_bg04[];
extern Gfx gfx_e8_bg05[];
extern Gfx gfx_e8_bg06[];
extern Gfx gfx_e8_bg07[];
extern Gfx gfx_e8_bg08[];
extern Gfx gfx_e8_bg09[];
extern Gfx gfx_e8_bg10[];
extern Gfx gfx_e8_bg10_ana[];
extern Gfx gfx_e8_bg11[];
extern Gfx gfx_e8_bg12[];
extern Gfx gfx_e8_bg13[];
extern Gfx gfx_e8_bg14[];
extern Gfx gfx_e8_bg15[];
extern Gfx gfx_e8_bg16[];
extern Gfx gfx_e8_bg17[];
extern Gfx gfx_e8_bg18[];
extern Gfx gfx_e8_bg19[];
extern Gfx gfx_e8_bg20[];

extern Gfx gfx_e8_buranko[]	 ;
extern Gfx gfx_e8_guruguru[] ;
extern Gfx gfx_e8_hane[]	 ;
extern Gfx gfx_e8_kaiten[]	 ;
extern Gfx gfx_e8_rifuto01[] ;
extern Gfx gfx_e8_rifuto02[] ;
extern Gfx gfx_e8_rifuto03[] ;
extern Gfx gfx_e8_shiso[]	 ;
extern Gfx gfx_e8_tikuwa01[] ;
extern Gfx gfx_e8_tikuwa02[] ;
extern Gfx gfx_e8_yokoshiso[];
extern Gfx gfx_e8_dorifu00[] ;
extern Gfx gfx_e8_dorifu01[] ;
extern Gfx gfx_e8_dorifu02[] ;
extern Gfx gfx_e8_dorifu03[] ;
extern Gfx gfx_e8_dorifu04[] ;

extern ulong ArajinInit(int, MapNode*, void*);
extern ulong ArajinWave(int, MapNode*, void*);

/* ===============================================================================
        : Hierarchy map data of Ext8Bg01.
================================================================================== */
Hierarchy RCP_HmsExt8Bg01[] = {
    hmsHeader(3000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_bg01)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg02.
================================================================================== */
Hierarchy RCP_HmsExt8Bg02[] = {
    hmsHeader(1000)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e8_bg02)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg03.
================================================================================== */
Hierarchy RCP_HmsExt8Bg03[] = {
    hmsHeader(3000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_bg03)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg04.
================================================================================== */
Hierarchy RCP_HmsExt8Bg04[] = {
    hmsHeader(3000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_bg04)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg05.
================================================================================== */
Hierarchy RCP_HmsExt8Bg05[] = {
    hmsHeader(3500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_bg05)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg06.
================================================================================== */
Hierarchy RCP_HmsExt8Bg06[] = {
    hmsHeader(3500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_bg06)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg07.
================================================================================== */
Hierarchy RCP_HmsExt8Bg07[] = {
    hmsHeader(2800)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_bg07)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg08.
================================================================================== */
Hierarchy RCP_HmsExt8Bg08[] = {
    hmsHeader(4000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_bg08)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg09.
================================================================================== */
Hierarchy RCP_HmsExt8Bg09[] = {
    hmsHeader(3000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_bg09)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg10.
================================================================================== */
Hierarchy RCP_HmsExt8Bg10[] = {
    hmsHeader(4000)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_e8_bg10)
		hmsGfx(RM_SPRITE, gfx_e8_bg10_ana)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg11.
================================================================================== */
Hierarchy RCP_HmsExt8Bg11[] = {
    hmsHeader(1500)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e8_bg11)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg12.
================================================================================== */
Hierarchy RCP_HmsExt8Bg12[] = {
    hmsHeader(1500)
    hmsBegin()
        hmsGfx(RM_XSURF, gfx_e8_bg12)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg13.
================================================================================== */
Hierarchy RCP_HmsExt8Bg13[] = {
    hmsHeader(4000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_bg13)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg14.
================================================================================== */
Hierarchy RCP_HmsExt8Bg14[] = {
    hmsHeader(3500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_bg14)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg15.
================================================================================== */
Hierarchy RCP_HmsExt8Bg15[] = {
    hmsHeader(1600)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_bg15)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg16.
================================================================================== */
Hierarchy RCP_HmsExt8Bg16[] = {
    hmsHeader(3800)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e8_bg16)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg17.
================================================================================== */
Hierarchy RCP_HmsExt8Bg17[] = {
    hmsHeader(2500)
    hmsBegin()
        hmsGfx(RM_XSURF, gfx_e8_bg17)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg18.
================================================================================== */
Hierarchy RCP_HmsExt8Bg18[] = {
    hmsHeader(4500)
    hmsBegin()
        hmsGfx(RM_XSURF, gfx_e8_bg18)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg19.
================================================================================== */
Hierarchy RCP_HmsExt8Bg19[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_XSURF, gfx_e8_bg19)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Bg19.
================================================================================== */
Hierarchy RCP_HmsExt8Bg20[] = {
    hmsHeader(3000)
    hmsBegin()
		hmsGfx(RM_SPRITE, gfx_e8_bg20)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Arajin.
================================================================================== */
Hierarchy RCP_HmsExt8Arajin[] = {
    hmsHeader(500)
    hmsBegin()
		hmsCProg(0, ArajinWave)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Buranko.
================================================================================== */
Hierarchy RCP_HmsExt8Buranko[] = {
    hmsHeader(1300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_buranko)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Guruguru.
================================================================================== */
Hierarchy RCP_HmsExt8Guruguru[] = {
    hmsHeader(1500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_guruguru)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Hane.
================================================================================== */
Hierarchy RCP_HmsExt8Hane[] = {
    hmsHeader(1200)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e8_hane)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Kaiten.
================================================================================== */
Hierarchy RCP_HmsExt8Kaiten[] = {
    hmsHeader(1300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_kaiten)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Rifuto01.
================================================================================== */
Hierarchy RCP_HmsExt8Rifuto01[] = {
    hmsHeader(700)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_rifuto01)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Rifuto02.
================================================================================== */
Hierarchy RCP_HmsExt8Rifuto02[] = {
    hmsHeader(700)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_rifuto02)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Rifuto03.
================================================================================== */
Hierarchy RCP_HmsExt8Rifuto03[] = {
    hmsHeader(500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_rifuto03)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Shiso.
================================================================================== */
Hierarchy RCP_HmsExt8Shiso[] = {
    hmsHeader(1000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_shiso)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Tikuwa01.
================================================================================== */
Hierarchy RCP_HmsExt8Tikuwa01[] = {
    hmsHeader(420)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_e8_tikuwa01)
        hmsGfx(RM_SPRITE, gfx_e8_tikuwa02)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Yokoshiso.
================================================================================== */
Hierarchy RCP_HmsExt8Yokoshiso[] = {
    hmsHeader(1100)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_yokoshiso)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Dorifu00.
================================================================================== */
Hierarchy RCP_HmsExt8Dorifu00[] = {
    hmsHeader(1500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_dorifu00)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Dorifu01.
================================================================================== */
Hierarchy RCP_HmsExt8Dorifu01[] = {
    hmsHeader(1500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_dorifu01)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Dorifu02.
================================================================================== */
Hierarchy RCP_HmsExt8Dorifu02[] = {
    hmsHeader(1500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_dorifu02)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Dorifu03.
================================================================================== */
Hierarchy RCP_HmsExt8Dorifu03[] = {
    hmsHeader(1500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_dorifu03)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext8Dorifu04.
================================================================================== */
Hierarchy RCP_HmsExt8Dorifu04[] = {
    hmsHeader(1500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e8_dorifu04)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage15Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(3, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, 20000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_PARALLEL,  0,2000,6000,  0,0,-8000, GameCamera)
				hmsBegin()
					hmsCProg(0, ArajinInit)
					hmsGfx(RM_SPRITE, gfx_e8_base)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

		hmsLayer(0)
		hmsBegin()
			hmsCProg(0, CannonEdge)
		hmsEnd()

	hmsEnd()
	hmsExit()
};
