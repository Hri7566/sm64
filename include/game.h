/********************************************************************************
	game.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	April 15, 1996
 ********************************************************************************/

#ifndef GAME_H
#define	GAME_H


#define	PL_RETURN_SELECT	-9
#define	PL_RETURN_LOGO		-8
#define	PL_RETURN_TITLE		-2
#define	PL_GAME_OVER		-3
#define	PL_GAME_CLEAR		-1

#define	PL_FADE_VIEWROOF	0x01
#define	PL_FADE_TRIPKAGO	0x02
#define	PL_FADE_TRIPDOOR	0x03
#define	PL_FADE_TRIPTUBE	0x04
#define	PL_FADE_WARPIN		0x05


#define	PL_FADE_WINNING		0x11
#define	PL_FADE_LOSING		0x12
#define	PL_FADE_FALLING		0x13
#define	PL_FADE_GAMEOVR		0x14
#define	PL_FADE_GAMEEND		0x15
#define	PL_FADE_GOTITLE		0x16

#define	PL_FADE_ENDING		0x17
#define	PL_FADE_STAFFROLL	0x18
#define	PL_FADE_RESTART		0x19



/********************************************************************************/
/*																				*/
/*	Define function prototype.													*/
/*																				*/
/********************************************************************************/

typedef void (*FreezeProc)(short *counter);

#define	GmShowTimer()			GmStopWatch(0)
#define	GmStartTimer()			GmStopWatch(1)
#define	GmStopTimer()			GmStopWatch(2)
#define	GmHideTimer()			GmStopWatch(3)
#define	GmGetTimer()			GmStopWatch(4)


extern void
PL_ExitGameMode(int code, int color);

extern ushort
GmStopWatch(int mode);

extern void
PL_DisplayMessage(int mesgno);

extern void
PL_SaveDialog(int mesgno);

extern void
FreezeGame(short count, FreezeProc proc);

extern int
PL_StartFadeout(PlayerPtr player, int mode);

extern ulong
GameTheEnd(short code, long param);

#endif
