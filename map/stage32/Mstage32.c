/********************************************************************************
						Ultra 64 MARIO Brothers

						  stage 64 data module
						   
			Copyright 1995 Nintendo co., ltd.  All rights reserved

				This module was programmed by Y.Nishida

							Sept 5, 1995
 ********************************************************************************/


#ifdef yajima
#include "headers.h"
#include "testmap1.sou"
#include "testmap1.flk"
#include "testmap1.tag"
#else
#define	TAGDECODE_END  0x8000
#include "../../headers.h"
#include "stageyj/testmap1.sou"
#include "stageyj/testmap1.flk"
#endif

/*------- yajima special stage -------*/

#include "stage.dat"

/*------------------------------------*/


