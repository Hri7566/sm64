/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 6 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage06

/***********************************************************************************
		: 1F port sequence.
************************************************************************************/
SEQ_SelRoom1F_Port:

		seqPort(0, 16, 1, 0)					/*	Syomen door 1.	*/
		seqPort(1, 16, 1, 1)					/*	Syomen door 2.	*/
		seqPort(2, 26, 1, 1)					/*	Uraguchi.		*/

/*	Auto door.	[ To Kuppa 1 ]	*/

		seqActor(S_autodoor_1F,  -2706,512,-1409,  0, 45,0,  8,0,0,  e_autodoor)
		seqActor(S_autodoor_1F,	 -2598,512,-1517,  0,225,0,  8,0,0,  e_autodoor)


/*	Key-Hole trip door.		*/

		seqActor(S_doorD,  -1100,  512,-1074,  0,  0,0,  1,3,0,  e_tripdoor)
		seqActor(S_doorD,   -946,  512,-1074,  0,180,0,  1,4,0,  e_tripdoor)
		seqActor(S_doorD,  -1100,-1074,  922,  0,  0,0,  2,5,0,  e_tripdoor)
		seqActor(S_doorD,   -946,-1074,  922,  0,180,0,  2,6,0,  e_tripdoor)
		seqPort(3, 6, 2, 0)				/*	Key-Hole trip door to 2F.	*/
		seqPort(4, 6, 2, 1)				/*				"				*/
		seqPort(5, 6, 3, 0)				/*	Key-Hole trip door to B1.	*/
		seqPort(6, 6, 3, 1)				/*				"				*/

/*	Port to the each stage.	*/

		seqBGPort( 0,  9, 1, 10)				/*	To St9 [ Battle Fd ].	*/
		seqBGPort( 1,  9, 1, 10)				/*			"				*/
		seqBGPort( 2,  9, 1, 10)				/*			"				*/

		seqBGPort( 3,  5, 1, 10)				/*	To St5 [ Yukiyama1 ].	*/
		seqBGPort( 4,  5, 1, 10)				/*			"				*/
		seqBGPort( 5,  5, 1, 10)				/*			"				*/

		seqBGPort( 6, 24, 1, 10)				/*	To St24 [ Mountain ].	*/
		seqBGPort( 7, 24, 1, 10)				/*			"				*/
		seqBGPort( 8, 24, 1, 10)				/*			"				*/

		seqBGPort( 9, 12, 1, 10)				/*	To St12 [ WaterDG ].	*/
		seqBGPort(10, 12, 1, 10)				/*			"				*/
		seqBGPort(11, 12, 1, 10)				/*			"				*/

		seqActor(S_NULL,   2013,768,-2014,  0,0,0,  0,10,0,  e_tripchimney2)
		seqPort(10, 27, 1, 10)					/*	To St27 [ Ext4 Mini Slider ].	*/

		seqActor(S_NULL,  -5513,512,-4324,  0,0,0, 48,11,0,  e_tripchimney2)
		seqPort(11, 17, 1, 10)					/*	To St17 [ Ext1 before Kuppa1 ].	*/

		seqActor(S_NULL,   1963,819, 1280,	0,0,0,  5,12,0,  e_tripchimney2)
		seqPort(12, 20, 1, 10)					/*	To St20 [ Ext9 Suisou ].		*/


/*	View roof trip.		*/

		seqViewRoof(29, 1, 10)					/*	To St29 [ Ext6 Flying MARIO ].	*/

/*	Port from the another stage.	*/

		seqActor(S_NULL,  -1024,512,-650,   0,  0,0,  0,30,0,  e_player_waiting	  )
		seqActor(S_NULL,  -1024,-50, 717,   0,180,0,  0,31,0,  e_player_waiting	  )

		seqActor(S_NULL,  -1024,900, 717,   0,180,0,  0,32,0,  e_player_landing	  )
		seqActor(S_NULL,  -1024,900, 717,   0,180,0,  0,33,0,  e_player_landloser )

		seqActor(S_NULL,  -1024,900, 717,   0,180,0,  0,34,0,  e_player_falling	  )
		seqActor(S_NULL,  -1024,900, 717,   0,180,0,  0,35,0,  e_player_downing	  )

		seqActor(S_NULL,  -5513,512,-4324,  0,-135,0, 0,36,0,  e_player_pushout	  )
		seqActor(S_NULL,  -5513,512,-4324,  0,-135,0, 0,37,0,  e_player_pushdown  )

		seqActor(S_NULL,  -1024,900,  717,  0,   0,0, 0,38,0,  e_player_landwinner)

		seqActor(S_NULL,   2816,1200,-256,  0, 90,0,  0,39,0,  e_player_landwinner)
		seqActor(S_NULL,   2816,1200,-256,  0,270,0,  0,40,0,  e_player_downing	  )

		seqPort(30, 6, 1, 30)
		seqPort(31, 6, 1, 31)

		seqPort(32, 6, 1, 32)
		seqPort(33, 6, 1, 33)

		seqPort(34, 6, 1, 34)
		seqPort(35, 6, 1, 35)

		seqPort(36, 6, 1, 36)
		seqPort(37, 6, 1, 37)

		seqPort(38, 6, 1, 38)

		seqPort(39, 6, 1, 39)
		seqPort(40, 6, 1, 40)


/*	Port from the each stage by clear.	*/

		seqActor(S_NULL,	-5422,717, -461,	0,270,0,	0,50,0,		e_player_entwinner)
		seqActor(S_NULL,	-2304,  0,-4552,	0,180,0,	0,51,0,		e_player_entwinner)
		seqActor(S_NULL,	  256,102,-4706,	0,180,0,	0,52,0,		e_player_entwinner)
		seqActor(S_NULL,	 4501,717, -230,	0, 90,0,	0,53,0,		e_player_entwinner)
		seqPort(50, 6, 1, 50)		/*	St9  [ Battle Fd ] clear.	*/
		seqPort(51, 6, 1, 51)		/*	St5  [ Yukiyama1 ] clear.	*/
		seqPort(52, 6, 1, 52)		/*	St24 [ Mountain  ] clear.	*/
		seqPort(53, 6, 1, 53)		/*	St12 [ WaterDG   ] clear.	*/

/*	Port from the each stage by missing.	*/

		seqActor(S_NULL,	-5422,717, -461,	0,270,0,	0,100,0,	e_player_entloser) 		
		seqActor(S_NULL,	-2304,  0,-4552,	0,180,0,	0,101,0,	e_player_entloser) 		
		seqActor(S_NULL,	  256,102,-4706,	0,180,0,	0,102,0,	e_player_entloser)
		seqActor(S_NULL,	 4501,717, -230,	0, 90,0,  	0,103,0,	e_player_entloser)
		seqPort(100, 6, 1, 100)		/*	St9  [ Battle Fd ] missed.	*/
		seqPort(101, 6, 1, 101)		/*	St5  [ Yukiyama1 ] missed.	*/
		seqPort(102, 6, 1, 102)		/*	St24 [ Mountain  ] missed.	*/
		seqPort(103, 6, 1, 103)		/*	St12 [ WaterDG   ] missed.	*/

		seqReturn



/***********************************************************************************
		: 2F port sequence.
************************************************************************************/
SEQ_SelRoom2F_Port:

/*	Key-Hole trip door.		*/

		seqActor(S_doorD,  -1100,512,3021,  0,  0,0,  0,0,0,  e_tripdoor)
		seqActor(S_doorD,   -946,512,3021,  0,180,0,  0,1,0,  e_tripdoor)
		seqPort(0, 6, 1, 3)				/*	Key-Hole trip door to 1F.	*/
		seqPort(1, 6, 1, 4)				/*				"				*/


/*	Auto door.	*/

		seqActor(S_autodoor_2F,  -281,2253,4762,  0,  0,0, 50,0,0,  e_autodoor)
		seqActor(S_autodoor_2F,	 -127,2253,4762,  0,180,0, 50,0,0,  e_autodoor)

		seqActor(S_autodoor_4F,  -281,3174,3772,  0,  0,0, 70,0,0,  e_autodoor)
		seqActor(S_autodoor_4F,	 -127,3174,3772,  0,180,0, 70,0,0,  e_autodoor)


/*	Port to the each stage.	*/

		seqBGPort(24, 11, 1, 10)			/*	To St11 [ Pool ].				*/
		seqBGPort(25, 11, 1, 10)			/*			"						*/
		seqBGPort(26, 11, 1, 10)			/*			"						*/

		seqBGPort(27, 13, 2, 10)			/*	To St13-2 [ Big world small ].	*/
		seqBGPort(28, 13, 2, 10)			/*			"						*/
		seqBGPort(29, 13, 2, 10)			/*			"						*/

		seqBGPort(30, 36, 1, 10)			/*	To St36 [ Donkey ].				*/
		seqBGPort(31, 36, 1, 10)			/*			"						*/
		seqBGPort(32, 36, 1, 10)			/*			"						*/

		seqBGPort(33, 14, 1, 10)			/*	To St14 [ Clock Tower ].		*/
		seqBGPort(34, 14, 1, 10)			/*			"						*/
		seqBGPort(35, 14, 1, 10)			/*			"						*/

		seqBGPort(36, 10, 1, 10)			/*	To St10 [ Yukiyama2 ].			*/
		seqBGPort(37, 10, 1, 10)			/*			"						*/
		seqBGPort(38, 10, 1, 10)			/*			"						*/

		seqBGPort(39, 13, 1, 10)			/*	To St13-1 [ Big world big ].	*/
		seqBGPort(40, 13, 1, 10)			/*			"						*/
		seqBGPort(41, 13, 1, 10)			/*			"						*/

	  	seqBGPort(42, 15, 1, 10)			/*	To St15 [ Fire bubble 2 ].		*/

		seqActor(S_NULL,   3002,2816,5886,  0,0,0,  15,10,0,  e_tripchimney2)
		seqPort(10, 31, 1, 10)				/*	To St31 [ Ext8 Blue sky ].		*/

		seqActor(S_NULL,  -230,4813,-3352,  0,0,0,  15,11,0,  e_tripchimney2)
		seqPort(11, 21, 1, 10)				/*	To St21 [ Ext3 Heaven ].		*/


/*	Port from the each stage by clear.	*/

		seqActor(S_NULL,    -659,1613, -350,   0,180,0,	  0,50,0,	e_player_entwinner	)
		seqActor(S_NULL,   -4693,2157, 1828,   0,270,0,	  0,51,0,	e_player_landwinner	)
		seqActor(S_NULL,    -675,1400, 3870,   0,  0,0,	  0,52,0,	e_player_entwinner	)
		seqActor(S_NULL,    -205,2918, 7300,   0,  0,0,	  0,53,0,	e_player_entwinner	)
		seqActor(S_NULL,    3538,1766, -400,   0,180,0,	  0,54,0,	e_player_entwinner	)
		seqActor(S_NULL,   -4693,2157, 1828,   0,270,0,	  0,55,0,	e_player_landwinner	)
		seqActor(S_NULL,    3002,2816, 5886,   0, 90,0,   0,56,0,   e_player_pushout  	)
		seqActor(S_NULL,   -3412,2816, 5886,   0,270,0,   0,58,0,   e_player_pushout  	)
		seqPort(50, 6, 2, 50)		/*	St11   [ Pool ] 		   clear.	*/
		seqPort(51, 6, 2, 51)		/*	St13-2 [ Big world small ] clear.	*/
		seqPort(52, 6, 2, 52)		/*	St36   [ Donkey ] 		   clear.	*/
		seqPort(53, 6, 2, 53)		/*	St14   [ Clock Tower ]	   clear.	*/
		seqPort(54, 6, 2, 54)		/*	St10   [ Yukiyama2 ]	   clear.	*/
		seqPort(55, 6, 2, 55)		/*	St13-1 [ Big world big ]   clear.	*/
		seqPort(56, 6, 2, 56)		/*	St31   [ Ext8 Blue sky ]   clear.	*/
		seqPort(58, 6, 2, 58)		/*	St15   [ Fire bubble 2 ]   clear.	*/

/*	Port from the each stage by missing.	*/

		seqActor(S_NULL,    -659,1613, -350,   0,180,0,	  0,100,0,  e_player_entloser) 		
		seqActor(S_NULL,   -4693,2157, 1828,   0,270,0,	  0,101,0,  e_player_downing )
		seqActor(S_NULL,    -675,1400, 3870,   0,  0,0,	  0,102,0,  e_player_entloser)
		seqActor(S_NULL,    -205,2918, 7300,   0,  0,0,	  0,103,0,  e_player_entloser)
		seqActor(S_NULL,    3538,1766, -400,   0,180,0,	  0,104,0,  e_player_entloser)
		seqActor(S_NULL,   -4693,2157, 1828,   0,270,0,	  0,105,0,	e_player_downing )
		seqActor(S_NULL,    -230,4813,-3352,   0,180,0,	  0,107,0,	e_player_pushdown)
		seqActor(S_NULL,   -3412,2816, 5886,   0,270,0,   0,108,0,  e_player_pushdown)
		seqActor(S_NULL,    3002,2816, 5886,   0, 90,0,   0,109,0,  e_player_pushdown)
		seqPort(100, 6, 2, 100)		/*	St11   [ Pool ] 		   missed.	*/
		seqPort(101, 6, 2, 101)		/*	St13-2 [ Big world small ] missed.	*/
		seqPort(102, 6, 2, 102)		/*	St36   [ Donkey ]		   missed.	*/
		seqPort(103, 6, 2, 103)		/*	St14   [ Clock Tower ]	   missed.	*/
		seqPort(104, 6, 2, 104)		/*	St10   [ Yukiyama2 ]	   missed.	*/
		seqPort(105, 6, 2, 105)		/*	St13-1 [ Big world big ]   missed.	*/
		seqPort(107, 6, 2, 107)		/*	St21   [ Ext3 Heaven ]	   missed.	*/
		seqPort(108, 6, 2, 108)		/*	St15   [ Fire bubble 2 ]   missed.	*/
		seqPort(109, 6, 2, 109)		/*	St31   [ Ext8 Blue sky ]   missed.	*/

		seqReturn



/***********************************************************************************
		: B1 port sequence.
************************************************************************************/
SEQ_SelRoom1B_Port:

		seqActor(S_doorD,  -1100,-1074,922,  0,  0,0,  0,0,0,  e_tripdoor)
		seqActor(S_doorD,   -946,-1074,922,  0,180,0,  0,1,0,  e_tripdoor)
		seqPort(0,  6, 1, 5)		/*	Key-Hole trip door to 1F.	*/
		seqPort(1,  6, 1, 6)		/*				"				*/
		seqPort(2, 16, 1, 2)		/*	Iron trip door. To st16.	*/


/*  Auto door.	*/

		seqActor(S_autodoor_B1,  307,-1074,2074,  0, 90,0, 30,0,0,  e_autodoor)
		seqActor(S_autodoor_B1,	 307,-1074,1920,  0,270,0, 30,0,0,  e_autodoor)


/*	Port to the each stage.	*/

		seqBGPort(12, 22, 1, 10)			/*	To St22 [ Fire Bubble1 ].	*/
		seqBGPort(13, 22, 1, 10)			/*				"				*/
		seqBGPort(14, 22, 1, 10)			/*				"				*/

		seqBGPort(15,  8, 1, 10)			/*	To St8  [ Sabaku ].			*/
		seqBGPort(16,  8, 1, 10)			/*				"				*/
		seqBGPort(17,  8, 1, 10)			/*				"				*/

		seqBGPort(42, 7, 1, 10)				/*	To St7  [ Horror Dungeon ].	*/

		seqBGPort(21, 23, 1, 10)			/*	To St23 [ Water Land ].		*/
		seqBGPort(22, 23, 1, 10)			/*				"				*/
		seqBGPort(23, 23, 1, 10)			/*				"				*/

		seqActor(S_NULL,  4147,-1280,1997,  0,0,0,  15,24,0,  e_tripchimney2)
		seqPort(24, 19, 1, 10)				/*	To St19 [ Ext2. Lava ].		*/


/*	Port from the each stage by clear.	*/

		seqActor(S_NULL,  -1382, -819,-4150,   0,180,0,	  0,50,0,	e_player_entwinner	)
		seqActor(S_NULL,  -2918, -870, -875,   0,  0,0,	  0,51,0,	e_player_entwinner	)
		seqActor(S_NULL,   2483,-1688,-2662,   0,270,0,	  0,52,0,	e_player_pushout	)
		seqActor(S_NULL,   2381, -500, 2011,   0, 90,0,	  0,53,0,	e_player_landwinner	)
		seqActor(S_NULL,   4147,-1100, 1997,   0, 90,0,	  0,54,0,	e_player_pushout	)
		seqPort(50, 6, 3, 50)			/*	St22 [ Fire Bubble1 ] clear.	*/
		seqPort(51, 6, 3, 51)			/*	St8  [ Sabaku ] 	  clear.	*/
		seqPort(52, 6, 3, 52)			/*	St7  [ Horror DG ] 	  clear.	*/
		seqPort(53, 6, 3, 53)			/*	St23 [ Water Land ]	  clear. 	*/
		seqPort(54, 6, 3, 54)			/*	St33 [ Kuppa2 ]	  	  clear.	*/


/*	Port from the each stage by missing.	*/

		seqActor(S_NULL,  -1382, -819,-4150,   0,180,0,	  0,100,0,	e_player_entloser)
		seqActor(S_NULL,  -2918, -870, -875,   0,  0,0,	  0,101,0,	e_player_entloser)
		seqActor(S_NULL,   2483,-1688,-2662,   0,270,0,	  0,102,0,	e_player_pushdown)
		seqActor(S_NULL,   2381, -500, 2011,   0, 90,0,	  0,103,0,	e_player_downing )
		seqActor(S_NULL,   4147,-1100, 1997,   0, 90,0,	  0,104,0,	e_player_pushdown)
		seqPort(100, 6, 3, 100)			/*	St22 [ Fire Bubble1 ] missed.	*/
		seqPort(101, 6, 3, 101)			/*	St8  [ Sabaku ]		  missed.	*/
		seqPort(102, 6, 3, 102)			/*	St7  [ Horror DG ] 	  missed.	*/
		seqPort(103, 6, 3, 103)			/*	St23 [ Water Land ]	  missed.	*/
		seqPort(104, 6, 3, 104)			/*	St19 [ Ext2. Lava ]	  missed.	*/


		seqReturn



/***********************************************************************************
		: B1 Move BGs sequence.
************************************************************************************/
SEQ_SelRoom1B_MoveBGs:

		seqActor(S_fireball_yellow,   -2037, -818, -716,   0,0,0,   0,0,0,   e_fire_animation)
		seqActor(S_fireball_yellow,   -1648, -818, -716,   0,0,0,   0,0,0,   e_fire_animation)
		seqActor(S_fireball_yellow,   -1648, -818, -101,   0,0,0,   0,0,0,   e_fire_animation)
		seqActor(S_fireball_yellow,   -1648, -818,  512,   0,0,0,   0,0,0,   e_fire_animation)
		seqActor(S_fireball_yellow,   -2037, -818, -101,   0,0,0,   0,0,0,   e_fire_animation)
		seqActor(S_fireball_yellow,   -2969, -921,  420,   0,0,0,   0,0,0,   e_fire_animation)
		seqActor(S_fireball_yellow,   -2037, -818,-1330,   0,0,0,   0,0,0,   e_fire_animation)
		seqActor(S_fireball_yellow,   -3839,-1023,-1422,   0,0,0,   0,0,0,   e_fire_animation)
		seqActor(S_fireball_yellow,   -1929, -818,-3615,   0,0,0,   0,0,0,   e_fire_animation)
		seqActor(S_fireball_yellow,    -834, -818,-3615,   0,0,0,   0,0,0,   e_fire_animation)
		seqActor(S_fireball_blue,     -3317, -921, 1229,   0,0,0,   0,0,0,   e_fire_animation)
		seqActor(S_fireball_blue,      -204, -921, -624,   0,0,0,   0,0,0,   e_fire_animation)
		seqActor(S_fireball_blue,     -2876, -921, 1229,   0,0,0,   0,0,0,   e_fire_animation)

		seqReturn



/***********************************************************************************
		: Select room main sequence.
************************************************************************************/
SEQ_DoStage06:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  ,  _GfxStage6SegmentRomStart		 , _GfxStage6SegmentRomEnd			)
	seqLoadText(SEGMENT_TEXTURE	  , _LCastleOutTextureSegmentRomStart, _LCastleOutTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_fSegmentRomStart	  	 , _GfxEnemy2_fSegmentRomEnd		)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_fSegmentRomStart	  	 , _HmsEnemy2_fSegmentRomEnd		)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetEnemy2_f)

		seqHmsShape(S_movebg00, RCP_HmsSel1fTrap	)		/*	1F. Trap Hole.		*/
		seqHmsShape(S_movebg01, RCP_HmsSel1BSwitch	)		/*	B1. Water switch.	*/
		seqHmsShape(S_movebg02, RCP_HmsHmsSel2fHariA)		/*	2F. Clock hand L.	*/
		seqHmsShape(S_movebg03, RCP_HmsHmsSel2fHariB)		/*		 ""		   S.	*/
		seqHmsShape(S_movebg04, RCP_HmsSel2Furi		)		/*		 ""	  furiko.	*/

		seqHmsShape(S_tripdoor0	, RCP_HmsMainDoor		)	/*	KOIZUMI door.	*/
		seqHmsShape(S_tripdoor1	, RCP_HmsDoor1			)	/*	Wood door.		*/
		seqHmsShape(S_tripdoor3 , RCP_HmsDoor3			)	/*	Iron 	door.	*/
		seqHmsShape(S_door0		, RCP_HmsMainDoor		)	/*	KOIZUMI door.	*/
		seqHmsShape(S_door1	   	, RCP_HmsDoor1			)	/*	Wood	door.	*/
		seqHmsShape(S_doorA		, RCP_HmsMainroomDoorA	)	/*	1 star door		*/
		seqHmsShape(S_doorB		, RCP_HmsMainroomDoorB	)	/*	2 star door		*/
		seqHmsShape(S_doorC		, RCP_HmsMainroomDoorC	)	/*	5 star door		*/
		seqHmsShape(S_doorD		, RCP_HmsMainroomDoorD	)	/*	Key Hole door	*/

		seqHmsShape(S_autodoor_B1, RCP_HmsAutoDoor)
		seqHmsShape(S_autodoor_1F, RCP_HmsAutoDoor)
		seqHmsShape(S_autodoor_2F, RCP_HmsAutoDoor)
		seqHmsShape(S_autodoor_4F, RCP_HmsAutoDoor)

		seqBeginScene(1, RCP_Stage6Scene1)
			seqActor(S_NULL, -5513,717,-4324,  0, 45,0,  0,20,0,  e_sel1ftrap		)
			seqActor(S_NULL,  2477,307,-2000,  0,  0,0,  0, 0,0,  e_sel1f_bubblejet	)
			seqActor(S_NULL,  2774,507,-1716,  0, 90,0,  0, 0,0,  e_loopfish		)
			seqActor(S_NULL,  3672,507,-1307,  0, 45,0,  0, 0,0,  e_loopfish		)
			seqActor(S_NULL,  3748,507,  773,  0,-45,0,  0, 0,0,  e_loopfish		)
			seqActor(S_NULL,  2778,507, 1255,  0,-90,0,  0, 0,0,  e_loopfish		)
			seqActor(S_michi_teresa,  -1000,50,-3500,  0,0,0,  0,0,0,  e_michi_teresa)
			seqActor(S_kinopio, -1671,    0, 1313,  0, 83,0,  133,0,0,  e_decor_kinopio)
			seqActor(S_kinopio,  1524,  307,  458,  0,110,0,  134,0,0,  e_decor_kinopio)
			seqActor(S_kinopio,   596, -306,-2637,  0,152,0,  135,0,0,  e_decor_kinopio)
			seqCall(SEQ_SelRoom1F_Port)
			seqGameOver(16, 1, 3)
			seqMapInfo(sel1f_info)
			seqAreaInfo(sel1f_area)
			seqTagInfo(sel1f_info_tag)
			seqSetMusic(NA_STG_CASTLE, NA_CASTLE_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

		seqBeginScene(2, RCP_Stage6Scene2)
			seqActor(S_movebg02,      -205,2918,7222,  0,180,0,  0,0,0,  e_clock_long	)
			seqActor(S_movebg03,      -205,2918,7222,  0,180,0,  0,0,0,  e_clock_short	)
			seqActor(S_movebg04,      -205,2611,7140,  0,  0,0,  0,0,0,  e_clock_furiko	)
			seqActor(S_camera_jugem, 4231,1408, 1601,  0,  0,0,  0,0,0,  e_camera_jugem	)
			seqActor(S_kinopio,  -977, 1203, 2569,  0,  0,0,   76,0,0,  e_decor_kinopio)	
			seqActor(S_kinopio, -1584, 2253, 7157,  0,136,0,   83,0,0,  e_decor_kinopio)
			seqActor(S_kinopio,   837, 1203, 3020,  0,180,0,  137,0,0,  e_decor_kinopio)
			seqCall(SEQ_SelRoom2F_Port)
			seqGameOver(16, 1, 3)
			seqMapInfo(sel2f_info)
			seqAreaInfo(sel2f_area)
			seqTagInfo(sel2f_info_tag)
			seqConnect(0, 2, 0,-205,410)
			seqSetMusic(NA_STG_CASTLE, NA_CASTLE_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

		seqBeginScene(3, RCP_Stage6Scene3)
			seqActor(S_movebg01,  7066,-1178,-819,  0,0,0,  0,0,0,  e_sel1b_erase_water)
			seqActor(S_movebg01,  7066,-1178,-205,  0,0,0,  0,0,0,  e_sel1b_erase_water)
			seqActor(S_NULL,      0,0,0,  0,0,0,  0,0,0,       e_sel1b_waterwall_switch)
			seqActor(S_rabbit,   -1509,-1177,-1564,  0,0,0,  0,0,0,    		   e_rabbit)
			seqActor(S_kinopio,  1787,-1381,-1957,  0,126,0,   82,0,0,  e_decor_kinopio)
			seqActor(S_kinopio, -4048,-1381,-1334,  0, 30,0,  136,0,0,  e_decor_kinopio)
			seqCall(SEQ_SelRoom1B_Port)
			seqCall(SEQ_SelRoom1B_MoveBGs)
			seqGameOver(16, 1, 3)
			seqMapInfo(sel1b_info)
			seqAreaInfo(sel1b_area)
			seqTagInfo(sel1b_info_tag)
			seqSetMusic(NA_STG_CASTLE, NA_CASTLE_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 180,  -1023,0,1152)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
