/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: pathhanswitch.s
					Description	: 
					Date		: 1995.
					Author		: H.yajima

 ********************************************************************************/


/*################*/
#ifdef ASSEMBLER
/*################*/

/*================== switchdoor switch =================*/
e_onoff_switch:
	p_initialize(moveBG)
	p_setd(programselect,1)
	p_jmp(el_hanswitch_common)

/*================== switchdoor switch =================*/
e_switchdoor_switch:
	p_initialize(moveBG)
	p_jmp(el_hanswitch_common)

/*================== switchdoor ========================*/
e_hanswitch:
	p_initialize(moveBG)
el_hanswitch_common:
	p_setbit(flag,stf_moveON )
	p_setshapeinfo(hanbutton_info)
	p_while
		p_program(s_hanswitch)
		p_program(stMainMoveBG)
	p_loop


/*################*/
#else
/*################*/
/********************************************************************************
		-----------------------
		 C Program (hanswitch)
		-----------------------
			code ... 0  ON only switch
					 1  ON/OFF  switch
					 2  ON/OFF  timer switch					 
 ********************************************************************************/
extern void s_hanswitch(void)
{

	StrategyRecord *stp;


	switch(obj_mode){
	 	case	MODE_SWITCH_OFF:
			s_change_shape(S_hanbutton);
			s_set_scale(1.5);
			if ( ((player1stp->ride_strat) == execstp ) && (( playerWorks[0].status & PS_TYPE_SWIM ) == 0 ) ){
				if ( s_distanceXZ_obj2obj(execstp,player1stp) < 85*1.5 ) obj_mode = MODE_SWITCH_ON_ANIME;
			}
			break;
		case	MODE_SWITCH_ON_ANIME:
			s_scale_timezoom(TIMESCALE_Y,3,1.5,0.2);
			if ( obj_timer == 3 ){
				objsound(NA_SE2_PLATESWITCH);
				obj_mode = MODE_SWITCH_ON;
				s_call_Viewshake(VS_SMALL);
				SendMotorEvent(5,80);		/* MOTOR 1997.5.29 */
			}
			break;
		case	MODE_SWITCH_ON:
			if ( obj_programselect != 0 ){
			 	if ( ( obj_programselect == 1  ) && ((player1stp->ride_strat) != execstp ) ) obj_mode++;
				else {
					if (  obj_timer   < HANSWITCH_TIMER-40 ) Na_FixSeFlagEntry(NA_ENV_TIME_SLOW);
					else									 Na_FixSeFlagEntry(NA_ENV_TIME_FAST);
					if (  obj_timer	  > HANSWITCH_TIMER  ) obj_mode = MODE_SWITCH_OFF_WAIT;
				}
			}
			break;
		case	MODE_SWITCH_OFF_ANIME:
			s_scale_timezoom(TIMESCALE_Y,3,0.2,1.5);
			if ( obj_timer == 3 ) obj_mode = MODE_SWITCH_OFF;
			break;					
		case	MODE_SWITCH_OFF_WAIT:
			if ( s_rideon_player() == 0 ) obj_mode = MODE_SWITCH_OFF_ANIME;
			break;
	}

}


/*################*/
#endif
/*################*/
/*===============================================================================
		end end end end end end end end 
===============================================================================*/



