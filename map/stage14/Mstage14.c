/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage14 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

#define		STAGE14_CLEAR_R		24
#define		STAGE14_CLEAR_G		31
#define		STAGE14_CLEAR_B		31

extern Gfx gfx_ct_1[]			;
extern Gfx gfx_ct_clear[]		;
extern Gfx gfx_ct_alpha[]		;
extern Gfx gfx_mecha_obj01[]	;
extern Gfx gfx_mecha_obj02[]	;
extern Gfx gfx_mecha_obj03[]	;
extern Gfx gfx_mecha_obj04_l[]	;
extern Gfx gfx_mecha_obj04_s[]	;
extern Gfx gfx_mecha_obj05[]	;
extern Gfx gfx_mecha_obj06[]	;
extern Gfx gfx_mecha_obj07[]	;
extern Gfx gfx_mecha_obj08_a[]	;
extern Gfx gfx_mecha_obj08_b[]	;
extern Gfx gfx_mecha_obj09[]	;
extern Gfx gfx_mecha_obj10[]	;
extern Gfx gfx_mecha_obj11[]	;
extern Gfx gfx_mecha_obj12s[]	;
extern Gfx gfx_mecha_obj12l[]	;

extern ulong BeltConv_L(int, MapNode*, void*);

/* ===============================================================================
        : Hierarchy map data of MechaObj01.
================================================================================== */
Hierarchy RCP_HmsMechaObj01[] = {
    hmsHeader(410)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_mecha_obj01)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MechaObj02.
================================================================================== */
Hierarchy RCP_HmsMechaObj02[] = {
    hmsHeader(410)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_mecha_obj02)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MechaObj03.
================================================================================== */
Hierarchy RCP_HmsMechaObj03[] = {
    hmsHeader(1100)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_mecha_obj03)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MechaObj04_l.
================================================================================== */
Hierarchy RCP_HmsMechaObj04_l[] = {
    hmsHeader(720)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_mecha_obj04_l)
		hmsCProg(0x1400, BeltConv_L)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MechaObj04_s.
================================================================================== */
Hierarchy RCP_HmsMechaObj04_s[] = {
    hmsHeader(520)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_mecha_obj04_s)
		hmsCProg(0x1401, BeltConv_L)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MechaObj05.
================================================================================== */
Hierarchy RCP_HmsMechaObj05[] = {
    hmsHeader(500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_mecha_obj05)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MechaObj06.
================================================================================== */
Hierarchy RCP_HmsMechaObj06[] = {
    hmsHeader(400)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_mecha_obj06)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MechaObj07.
================================================================================== */
Hierarchy RCP_HmsMechaObj07[] = {
    hmsHeader(520)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_mecha_obj07)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MechaObj08_a.
================================================================================== */
Hierarchy RCP_HmsMechaObj08_a[] = {
    hmsHeader(250)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_mecha_obj08_a)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MechaObj08_b.
================================================================================== */
Hierarchy RCP_HmsMechaObj08_b[] = {
    hmsHeader(250)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_mecha_obj08_b)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MechaObj09.
================================================================================== */
Hierarchy RCP_HmsMechaObj09[] = {
    hmsHeader(380)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_mecha_obj09)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MechaObj10.
================================================================================== */
Hierarchy RCP_HmsMechaObj10[] = {
    hmsHeader(1700)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_mecha_obj10)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MechaObj11.
================================================================================== */
Hierarchy RCP_HmsMechaObj11[] = {
    hmsHeader(500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_mecha_obj11)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MechaObj12s.
================================================================================== */
Hierarchy RCP_HmsMechaObj12s[] = {
    hmsHeader(200)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_mecha_obj12s)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MechaObj12l.
================================================================================== */
Hierarchy RCP_HmsMechaObj12l[] = {
    hmsHeader(300)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_mecha_obj12l)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage14Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(STAGE14_CLEAR_R, STAGE14_CLEAR_G, STAGE14_CLEAR_B, 1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_INSIDE, 0,2000,6000, 0,0,0, GameCamera)
				hmsBegin()
					hmsCProg(0, WaterInit)
					hmsCProg(0x1400, SandConeInit)
					hmsCProg(0x1401, SandConeInit)
					hmsGfx(RM_SURF	, gfx_ct_1)
					hmsGfx(RM_XSURF	, gfx_ct_clear)
					hmsGfx(RM_SPRITE, gfx_ct_alpha)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
