/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage19 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx gfx_e2_ana[];

extern Gfx gfx_e2_bg01[];
extern Gfx gfx_e2_bg02[];
extern Gfx gfx_e2_bg03[];
extern Gfx gfx_e2_bg04[];
extern Gfx gfx_e2_bg05[];
extern Gfx gfx_e2_bg06[];
extern Gfx gfx_e2_bg07[];
extern Gfx gfx_e2_bg08[];
extern Gfx gfx_e2_bg09[];
extern Gfx gfx_e2_bg10[];
extern Gfx gfx_e2_bg11[];
extern Gfx gfx_e2_bg12[];
extern Gfx gfx_e2_bg13[];
extern Gfx gfx_e2_bg14[];
extern Gfx gfx_e2_bg15[];
extern Gfx gfx_e2_bg16[];
extern Gfx gfx_e2_bg17[];
extern Gfx gfx_e2_bg18[];
extern Gfx gfx_e2_bg19[];

extern Gfx gfx_e2_agaru[]	 ;
extern Gfx gfx_e2_amiyuka[]	 ;
extern Gfx gfx_e2_bou[]		 ;
extern Gfx gfx_e2_gura[]	 ;
extern Gfx gfx_e2_kuzure01[] ;
extern Gfx gfx_e2_kuzure02[] ;
extern Gfx gfx_e2_nobiru01[] ;
extern Gfx gfx_e2_nobiru02[] ;
extern Gfx gfx_e2_rifuto01[] ;
extern Gfx gfx_e2_rifuto02[] ;
extern Gfx gfx_e2_susumu[]	 ;
extern Gfx gfx_e2_ukishima[] ;
extern Gfx gfx_e2_yokoshiso[];


/* --------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------- */


/* ===============================================================================
        : Hierarchy map data of Ext2Bg01.
================================================================================== */
Hierarchy RCP_HmsExt2Bg01[] = {
    hmsHeader(2700)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_bg01)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg02.
================================================================================== */
Hierarchy RCP_HmsExt2Bg02[] = {
    hmsHeader(2100)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_bg02)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg03.
================================================================================== */
Hierarchy RCP_HmsExt2Bg03[] = {
    hmsHeader(2500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_bg03)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg04.
================================================================================== */
Hierarchy RCP_HmsExt2Bg04[] = {
    hmsHeader(3200)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_bg04)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg05.
================================================================================== */
Hierarchy RCP_HmsExt2Bg05[] = {
    hmsHeader(2600)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e2_bg05)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg06.
================================================================================== */
Hierarchy RCP_HmsExt2Bg06[] = {
    hmsHeader(3500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_bg06)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg07.
================================================================================== */
Hierarchy RCP_HmsExt2Bg07[] = {
    hmsHeader(1900)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e2_bg07)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg08.
================================================================================== */
Hierarchy RCP_HmsExt2Bg08[] = {
    hmsHeader(4600)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_bg08)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg09.
================================================================================== */
Hierarchy RCP_HmsExt2Bg09[] = {
    hmsHeader(500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_bg09)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg10.
================================================================================== */
Hierarchy RCP_HmsExt2Bg10[] = {
    hmsHeader(2200)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_bg10)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg11.
================================================================================== */
Hierarchy RCP_HmsExt2Bg11[] = {
    hmsHeader(2100)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e2_bg11)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg12.
================================================================================== */
Hierarchy RCP_HmsExt2Bg12[] = {
    hmsHeader(1300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_bg12)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg13.
================================================================================== */
Hierarchy RCP_HmsExt2Bg13[] = {
    hmsHeader(1900)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_bg13)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg14.
================================================================================== */
Hierarchy RCP_HmsExt2Bg14[] = {
    hmsHeader(900)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e2_bg14)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg15.
================================================================================== */
Hierarchy RCP_HmsExt2Bg15[] = {
    hmsHeader(2800)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_bg15)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg16.
================================================================================== */
Hierarchy RCP_HmsExt2Bg16[] = {
    hmsHeader(2500)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e2_bg16)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg17.
================================================================================== */
Hierarchy RCP_HmsExt2Bg17[] = {
    hmsHeader(1600)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_bg17)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg18.
================================================================================== */
Hierarchy RCP_HmsExt2Bg18[] = {
    hmsHeader(600)
    hmsBegin()
        hmsGfx(RM_XSURF, gfx_e2_bg18)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bg19.
================================================================================== */
Hierarchy RCP_HmsExt2Bg19[] = {
    hmsHeader(4100)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_bg19)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Agaru.
================================================================================== */
Hierarchy RCP_HmsExt2Agaru[] = {
    hmsHeader(400)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_agaru)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Amiyuka.
================================================================================== */
Hierarchy RCP_HmsExt2Amiyuka[] = {
    hmsHeader(550)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e2_amiyuka)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Bou.
================================================================================== */
Hierarchy RCP_HmsExt2Bou[] = {
    hmsHeader(900)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_bou)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Gura.
================================================================================== */
Hierarchy RCP_HmsExt2Gura[] = {
    hmsHeader(700)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_gura)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Kuzure01.
================================================================================== */
Hierarchy RCP_HmsExt2Kuzure01[] = {
    hmsHeader(400)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_kuzure01)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Kuzure02.
================================================================================== */
Hierarchy RCP_HmsExt2Kuzure02[] = {
    hmsHeader(800)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_kuzure02)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Nobiru.
================================================================================== */
Hierarchy RCP_HmsExt2Nobiru[] = {
    hmsHeader(3000)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e2_nobiru01)
        hmsGfx(RM_SURF  , gfx_e2_nobiru02)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Rifuto01.
================================================================================== */
Hierarchy RCP_HmsExt2Rifuto01[] = {
    hmsHeader(650)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_rifuto01)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Rifuto02.
================================================================================== */
Hierarchy RCP_HmsExt2Rifuto02[] = {
    hmsHeader(550)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_rifuto02)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Susumu.
================================================================================== */
Hierarchy RCP_HmsExt2Susumu[] = {
    hmsHeader(600)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e2_susumu)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Ukishima.
================================================================================== */
Hierarchy RCP_HmsExt2Ukishima[] = {
    hmsHeader(1300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_ukishima)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext2Yokoshiso.
================================================================================== */
Hierarchy RCP_HmsExt2Yokoshiso[] = {
    hmsHeader(800)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e2_yokoshiso)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage19Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(1, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, 20000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_PARALLEL,  0,2000,6000,  0,-4500,-8000, GameCamera)
				hmsBegin()
					hmsGfx(RM_SPRITE, gfx_e2_ana)
					hmsCProg(0, WaterInit)
					hmsCProg(0x1901, WaterFall)
					hmsCProg(0x1902, WaterFall)
					hmsCProg(0x1903, WaterFall)
					hmsObject()
					hmsCProg(12, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
