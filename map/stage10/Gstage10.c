/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage10 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"


#define		STAGE10_FOG_R		0
#define		STAGE10_FOG_G		0
#define		STAGE10_FOG_B		0
#define		STAGE10_FOG_START	980

#include "yukiyama2/yukiyama2_ext_texture.h"

#include "yukiyama2/sf2_shape.sou"
#include "yukiyama2/sf2_p1_shape.sou"
#include "yukiyama2/sf2_p2_shape.sou"
#include "yukiyama2/sf2_p3_shape.sou"
#include "yukiyama2/sf2_p4_shape.sou"
#include "yukiyama2/sf2_p5_shape.sou"
#include "yukiyama2/sf2_p6_shape.sou"
#include "yukiyama2/sf2_nami_shape.sou"
#include "yukiyama2/sf2_hip_shape.sou"
#include "yukiyama2/sf2_hahen_shape.sou"

#include "kamakura/sf2_kama1_shape.sou"
#include "kamakura/sf2_kama2_shape.sou"
#include "kamakura/sf2_kama3_shape.sou"
#include "kamakura/sf2_kama4_shape.sou"

#include "yukiyama2/cx1001.flk"
#include "yukiyama2/cx1001.tag"
#include "yukiyama2/sf2_nami_check.flk"
#include "yukiyama2/sf2_hip_check.flk"

#include "kamakura/cx1002.flk"
#include "kamakura/cx1002.tag"


/********************************************************************************/
/*	Water surface records in 10 - 1.											*/
/********************************************************************************/
static short waterSurf_10_01_00[] = {
	1,
	0,   8,  10,  -6194, -409,   -6194, 4198,     154, 4198,     154, -409,  1,  150,  0,
};

static short waterSurf_10_01_01[] = {
	1,
	0,   8,  10, -1279,-6143,   -1279,-3071,    1792,-3071,    1792,-6143,  1,  150,  0,
};

WaterSurfRec waterSurf_10_01[] = {
		{	 0, waterSurf_10_01_00	 },
		{	 1, waterSurf_10_01_01	 },
		{	-1,	NULL	}					//	End of  WaterSurfRec data.
};
