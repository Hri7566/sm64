/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage11 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE11_FOG_R		0
#define		STAGE11_FOG_G		0
#define		STAGE11_FOG_B		0
#define		STAGE11_FOG_START	980

#include "pool/pool_ext_texture.h"

#include "pool/po_body01_shape.sou"
#include "pool/po_rgba01_shape.sou"
#include "pool/po_body02_shape.sou"
#include "pool/po_rgba02_shape.sou"
#include "pool/po_seal_shape.sou"
#include "pool/po_seal2_shape.sou"
#include "pool/po_mobj01_shape.sou"
#include "pool/po_mobj02_shape.sou"
#include "pool/po_mobj03_shape.sou"
#include "pool/po_mobj04_shape.sou"
#include "pool/po_mobj05_shape.sou"
#include "pool/po_mobj06_shape.sou"
#include "pool/po_mobj07_shape.sou"

#include "pool/cx1101.flk"
#include "pool/cx1101.tag"

#include "pool/cx1102.flk"
#include "pool/cx1102.tag"

#include "pool/po_mobj01_check.flk"
#include "pool/po_mobj02_check.flk"
#include "pool/po_mobj03_check.flk"
#include "pool/po_mobj04_check.flk"
#include "pool/po_mobj05_check.flk"
#include "pool/po_mobj06_check.flk"
#include "pool/po_mobj07_check.flk"


/********************************************************************************/
/*	Water surface records in 11 - 1.											*/
/********************************************************************************/
static short waterSurf_11_01_00[] = {
	1,
	0,   10,  10, -3839,-3839,   -3839, 4608,    4608, 4608,    4608,-3839,  1,  150,  0,
};

WaterSurfRec waterSurf_11_01[] = {
		{	 0, waterSurf_11_01_00	 },
		{	-1, NULL	}					//	End of  WaterSurfRec data.
};

/********************************************************************************/
/*	Water surface records in 11 - 2.											*/
/********************************************************************************/
static short waterSurf_11_02_00[] = {
	1,
	0,   10,  10, -3839,-3839,   -3839, 4608,    4608, 4608,    4608,-3839,  1,  150,  0,
};

WaterSurfRec waterSurf_11_02[] = {
		{	 0, waterSurf_11_02_00	 },
		{	-1, NULL	}					//	End of  WaterSurfRec data.
};
