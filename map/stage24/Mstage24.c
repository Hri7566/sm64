/********************************************************************************
						Ultra 64 MARIO Brothers

					 stage24 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 11, 1995
 ********************************************************************************/

#include "../../headers.h"

#define		STAGE24_CLEAR_R		0
#define		STAGE24_CLEAR_G		12
#define		STAGE24_CLEAR_B		31

extern Gfx gfx_ca_n_body[]	;
extern Gfx gfx_ca_n_dodai[]	;
extern Gfx gfx_ca_n_seal[]	;
extern Gfx gfx_ca_n_kabe[]	;

extern Gfx gfx_ca_n_bg01[]	;
extern Gfx gfx_ca_f_bg01[]	;
extern Gfx gfx_ca_n_bg02[]	;
extern Gfx gfx_ca_f_bg02[]	;
extern Gfx gfx_ca_n_bg03[]	;
extern Gfx gfx_ca_n_bg04[]	;
extern Gfx gfx_ca_n_bg05[]	;
extern Gfx gfx_ca_n_bg06[]	;
extern Gfx gfx_ca_f_bg06[]	;
extern Gfx gfx_ca_n_bg07[]	;	
extern Gfx gfx_ca_n_bg08[]	;
extern Gfx gfx_ca_f_bg08[]	;
extern Gfx gfx_ca_n_bg10[]	;
extern Gfx gfx_ca_n_bg12_o[];
extern Gfx gfx_ca_f_bg12_o[];
extern Gfx gfx_ca_n_bg12_t[];
extern Gfx gfx_ca_n_bg13[]	;
extern Gfx gfx_ca_n_bg14[]	;
extern Gfx gfx_ca_n_bg15[]	;
extern Gfx gfx_ca_n_bg16[]	;

extern Gfx gfx_ca_n_dossunbar[]	;
extern Gfx gfx_ca_f_dossunbar[]	;
extern Gfx gfx_ca_n_dossunbar2[];
extern Gfx gfx_ca_n_rotebar[]	;
extern Gfx gfx_ca_f_rotebar[]	;
extern Gfx gfx_ca_n_transbar50[];
extern Gfx gfx_ca_n_downbar[]	;
extern Gfx gfx_ca_f_downbar[]	;
extern Gfx gfx_ca_n_stick[]		;
extern Gfx gfx_ca_n_moveobj01[]	;
extern Gfx gfx_ca_n_moveobj02[]	;
extern Gfx gfx_ca_n_moveobj03[]	;
extern Gfx gfx_ca_n_moveobj03_shadow[];
extern Gfx gfx_ca_n_moveobj04[]	;

extern Gfx gfx_ca_n_lift0[];
extern Gfx gfx_ca_n_lift1[];

/********************************************************************************/
/*	Hierarchy map data of HfCastleBG01.											*/
/********************************************************************************/
Hierarchy RCP_HmsHfCastle2BG01[] = { 
	hmsHeader(600)
	hmsBegin()
		hmsLOD(-2048,5000)
		hmsBegin()
			hmsGfx(RM_SURF, gfx_ca_n_bg01)
		hmsEnd()

		hmsLOD(5000,32767)
		hmsBegin()
			hmsGfx(RM_SURF, gfx_ca_f_bg01)
		hmsEnd()
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of HfcastleBG02.											*/
/********************************************************************************/
Hierarchy RCP_HmsHfCastle2BG02[] = { 
	hmsHeader(1800)
	hmsBegin()
		hmsLOD(-2048,5000)
		hmsBegin()
			hmsGfx(RM_SURF, gfx_ca_n_bg02)
		hmsEnd()

		hmsLOD(5000,32767)
		hmsBegin()
			hmsGfx(RM_SURF, gfx_ca_f_bg02)
		hmsEnd()
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of HfCastleBG03.											*/
/********************************************************************************/
Hierarchy RCP_HmsHfCastle2BG03[] = { 
	hmsHeader(1400)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_ca_n_bg03)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of HfCastleBG04.											*/
/********************************************************************************/
Hierarchy RCP_HmsHfCastle2BG04[] = { 
	hmsHeader(9000)
	hmsBegin()
		hmsGfx(RM_SPRITE, gfx_ca_n_bg04)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of HfCastleBG05.											*/
/********************************************************************************/
Hierarchy RCP_HmsHfCastle2BG05[] = { 
	hmsHeader(1000)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_ca_n_bg05)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of HfCastleBG06.											*/
/********************************************************************************/
Hierarchy RCP_HmsHfCastle2BG06[] = { 
	hmsHeader(2700)
	hmsBegin()
		hmsLOD(-2048,5000)
		hmsBegin()
			hmsGfx(RM_SURF, gfx_ca_n_bg06)
		hmsEnd()

		hmsLOD(5000,32767)
		hmsBegin()
			hmsGfx(RM_SURF, gfx_ca_f_bg06)
		hmsEnd()
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of HfCastleBG07.											*/
/********************************************************************************/
Hierarchy RCP_HmsHfCastle2BG07[] = { 
	hmsHeader(400)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_ca_n_bg07)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of HfCastleBG08.											*/
/********************************************************************************/
Hierarchy RCP_HmsHfCastle2BG08[] = { 
	hmsHeader(1600)
	hmsBegin()
		hmsLOD(-2048,5000)
		hmsBegin()
			hmsGfx(RM_SURF, gfx_ca_n_bg08)
		hmsEnd()

		hmsLOD(5000,32767)
		hmsBegin()
			hmsGfx(RM_SURF, gfx_ca_f_bg08)
		hmsEnd()
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of HfCastle2BG10.										*/
/********************************************************************************/
Hierarchy RCP_HmsHfCastle2BG10[] = { 
	hmsHeader(800)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_ca_n_bg10)
	hmsEnd()
  	hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of CaBG12.
================================================================================== */
Hierarchy RCP_HmsCaBG12[] = {
    hmsHeader(2000)
    hmsBegin()
		hmsLOD(-2000, 8000)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_ca_n_bg12_o)
	        hmsGfx(RM_XDECAL, gfx_ca_n_bg12_t)
		hmsEnd()
		hmsLOD(8000, 20000)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_ca_f_bg12_o)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of CaBG13.
================================================================================== */
Hierarchy RCP_HmsCaBG13[] = {
    hmsHeader(3000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_ca_n_bg13)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of CaBG14.
================================================================================== */
Hierarchy RCP_HmsCaBG14[] = {
    hmsHeader(1100)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_ca_n_bg14)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of CaBG15.
================================================================================== */
Hierarchy RCP_HmsCaBG15[] = {
    hmsHeader(1200)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_ca_n_bg15)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of CaBG16.
================================================================================== */
Hierarchy RCP_HmsCaBG16[] = {
    hmsHeader(2700)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_ca_n_bg16)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of DossunBar.											*/
/********************************************************************************/
Hierarchy RCP_HmsDossunBar[] = { 
	hmsHeader(500)
	hmsBegin()
		hmsLOD(-2048,5000)
		hmsBegin()
			hmsGfx(RM_SURF, gfx_ca_n_dossunbar)
		hmsEnd()

		hmsLOD(5000,32767)
		hmsBegin()
			hmsGfx(RM_SURF, gfx_ca_f_dossunbar)
		hmsEnd()
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of DossunBar2.											*/
/********************************************************************************/
Hierarchy RCP_HmsDossunBar2[] = { 
	hmsHeader(700)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_ca_n_dossunbar2)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of RoteBar.												*/
/********************************************************************************/
Hierarchy RCP_HmsRoteBar[] = { 
	hmsHeader(1500)
	hmsBegin()
		hmsLOD(-2048,5000)
		hmsBegin()
			hmsGfx(RM_SURF, gfx_ca_n_rotebar)
		hmsEnd()

		hmsLOD(5000,32767)
		hmsBegin()
			hmsGfx(RM_SURF, gfx_ca_f_rotebar)
		hmsEnd()
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of Transbar50.											*/
/********************************************************************************/
Hierarchy RCP_HmsTransbar50[] = { 
	hmsHeader(500)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_ca_n_transbar50)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of DownB.												*/
/********************************************************************************/
Hierarchy RCP_HmsDownBNear[] = { 
	hmsHeader(400)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_ca_n_downbar)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsDownBFar[] = { 
	hmsHeader(800)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_ca_f_downbar)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of MappingBar.											*/
/********************************************************************************/
Hierarchy RCP_HmsMappingBar[] = { 
	hmsHeader(2000)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_ca_n_stick)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of Lift0.												*/
/********************************************************************************/
Hierarchy RCP_HmsLift0[] = { 
	hmsHeader(650)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_ca_n_lift0)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of Lift1.												*/
/********************************************************************************/
Hierarchy RCP_HmsLift1[] = { 
	hmsHeader(650)
	hmsBegin()
		hmsShadow(420, 150, 10)
		hmsBegin()
			hmsGfx(RM_SURF, gfx_ca_n_lift1)
		hmsEnd()
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of Lift2.												*/
/********************************************************************************/
Hierarchy RCP_HmsLift2[] = { 
	hmsHeader(650)
	hmsBegin()
		hmsShadow(420, 150, 10)
		hmsBegin()
			hmsGfx(RM_SURF, gfx_ca_n_lift1)
		hmsEnd()
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of Lift3.												*/
/********************************************************************************/
Hierarchy RCP_HmsLift3[] = { 
	hmsHeader(650)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_ca_n_lift1)
	hmsEnd()
  	hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of CasMobj01.
================================================================================== */
Hierarchy RCP_HmsCasMobj01[] = {
    hmsHeader(750)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_ca_n_moveobj01)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of CasMobj02.
================================================================================== */
Hierarchy RCP_HmsCasMobj02[] = {
    hmsHeader(690)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_ca_n_moveobj02)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of CasMobj03.
================================================================================== */
Hierarchy RCP_HmsCasMobj03[] = {
    hmsHeader(1300)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_ca_n_moveobj03)
        hmsGfx(RM_XDECAL, gfx_ca_n_moveobj03_shadow)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of CasMobj03.
================================================================================== */
Hierarchy RCP_HmsCasMobj03_ns[] = {
    hmsHeader(1300)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_ca_n_moveobj03)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of CasMobj04.
================================================================================== */
Hierarchy RCP_HmsCasMobj04[] = {
    hmsHeader(320)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_ca_n_moveobj04)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage24Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(3, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_FIELD,  0,2000,6000,  0,2000,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF  , gfx_ca_n_body)
					hmsGfx(RM_DECAL , gfx_ca_n_dodai)
					hmsGfx(RM_XDECAL, gfx_ca_n_seal)
					hmsGfx(RM_XSURF , gfx_ca_n_kabe)
					hmsObject()
					hmsCProg(0	   , WaterInit)
					hmsCProg(0x2401, WaterDraw)
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

		hmsLayer(0)
		hmsBegin()
			hmsCProg(0, CannonEdge)
		hmsEnd()

	hmsEnd()
	hmsExit()
};
