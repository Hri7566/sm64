/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: pathmovebg.s
					Description	: moving bg test program
					Date		: 1995.11.6
					Author		: H.yajima

 ********************************************************************************/


/*################*/
#ifdef ASSEMBLER
/*################*/

/*################*/
#else
/*################*/
/********************************************************************************
		moving bg test program
 ********************************************************************************/


#define		mbg_dx_now	(execstp->s[stw_work0].f)
#define		mbg_dy_now	(execstp->s[stw_work1].f)
#define		mbg_dz_now	(execstp->s[stw_work2].f)
#define		mbg_dx	(execstp->s[stw_work3].f)
#define		mbg_dy	(execstp->s[stw_work4].f)
#define		mbg_dz	(execstp->s[stw_work5].f)
#define		mbg_flag	(execstp->s[stw_work6].d)
#define		mbg_angle	(execstp->s[stw_work7].d)


extern void makeIXmatrix(AffineMtx *matrix,float dx,float dy, float dz)
{


	FVector vec;
	FVector pos;
	
	pos[0] = obj_worldX;
	pos[1] = obj_worldY;
	pos[2] = obj_worldZ;
	vec[0] = dx;
	vec[1] = dy;
	vec[2] = dz;

	CreateRotationAffineMtx(*matrix,vec,pos,0);

}

extern void s_movebg_1_init(void)
{
	AffineMtx *mtx = &(execstp->matrix);
	mbg_dx_now = 0.0;
	mbg_dy_now = 1.0;
	mbg_dz_now = 0.0;
	makeIXmatrix(mtx,0.0,1.0,0.0);
}

static float chase(float a, float b,float x)
{
	float ans;

	if ( a >= b ){
	 	if ((a-b) < x )		ans = a;
		else				ans = b+x;
	} else {
	 	if ((a-b) > -x )	ans = a;
		else				ans = b-x;
	}

	return(ans);

}

extern void s_movebg_1(void)
{

	float	dx,dy,dz,dr;
	float	src[3],dst[3],dst2[3];
	float	wx,wy,wz;
	int		flag = 0;
	short	angle;

	AffineMtx *mtx = &(execstp->matrix);
	FVector vectorY;
	FVector position;

#if 0
	dx = player1stp->s[stw_worldX].f - obj_worldX;
	dz = player1stp->s[stw_worldZ].f - obj_worldZ;
	angle		= atan(dz,dx);
	mbg_angle	= s_chase_angle(mbg_angle,angle,0x200);	
#endif

	if ( player1stp->ride_strat == execstp ){

		ReadPlayerPos(&wx,&wy,&wz);
		src[0] = player1stp->s[stw_worldX].f - obj_worldX;
		src[1] = player1stp->s[stw_worldY].f - obj_worldY;
		src[2] = player1stp->s[stw_worldZ].f - obj_worldZ;
		stRotatePoint(*mtx,dst,src);

		dx = player1stp->s[stw_worldX].f - obj_worldX;
		dy = 500;
		dz = player1stp->s[stw_worldZ].f - obj_worldZ;

#if 0
		dz = cos(mbg_angle);
		dy = 0.7;
		dx = sin(mbg_angle);
#endif

		dr = sqrtf(dx*dx+dy*dy+dz*dz);


		if ( dr != 0 ){
			dr = 1.0/dr;
			dx = dx*dr;
			dy = dy*dr;
			dz = dz*dr;
		} else {
		 	dx  =0; dy =1.0; dz = 0;
		}

		if ( mbg_flag == 1 )	flag++;
		mbg_flag = 1;

	} else {
		dx  =0; dy =1.0; dz = 0;
		mbg_flag = 0;
		
	}




	mbg_dx_now = chase(dx,mbg_dx_now,0.01);
	mbg_dy_now = chase(dy,mbg_dy_now,0.01);
	mbg_dz_now = chase(dz,mbg_dz_now,0.01);

	makeIXmatrix(mtx,mbg_dx_now,mbg_dy_now,mbg_dz_now);

	if ( flag ){
	
		stRotatePoint(*mtx,dst2,src);
		wx += ( dst2[0] - dst[0] );
		wy += ( dst2[1] - dst[1] );
		wz += ( dst2[2] - dst[2] );
		WritePlayerPos(wx,wy,wz);
	}	

	execstp->map.matrix = mtx;

}

/********************************************************************************
 ********************************************************************************/

/*################*/
#endif
/*################*/
/*===============================================================================
		end end end end end end end end 
===============================================================================*/



