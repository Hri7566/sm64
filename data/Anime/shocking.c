/********************************************************************************
						Ultra 64 MARIO Brothers

				   mario electric shoking animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							January 11, 1996
 ********************************************************************************/

#include "../headers.h"


static short mario_shocking_prm[]={
	    0,  180,    0,   32,   29,    8,   -9,   -1,
	16383,-3012,-3994,-4311,-4488,-4409,-3012,-3917,
	-6098,-6352,-6225,-5801,-3917,-4299,-3343,-4386,
	-5770,-6591,-4299,-5840,-12307,-3733,-5736,-9908,
	-6393,-18925,-17385,-14525,-12985,-15955,-18925,-10681,
	-10002,-8741,-8062,-9371,-10681, 9995, 8408, 5461,
	 3874, 6935, 9995, 1315, 7374,-8674,-5006,-12620,
	-11903,-9277,-5440,-5006,12677,20302,30153,23693,
	24537,12677,13219, 9298,14361,13791,12497,13219,
	 5218,13491,20033,12402,13263, 5218,  493,  922,
	 1132,  948, -990,  493, 1184, 1175, 1160, 1117,
	  943, 1184,-20824,-20720,-20023,-19252,-18695,-20824,
	 9527,11091,10877,10391, 9869, 9548, 5908, 6885,
	 6412, 4817, 3680, 5598, 4398, 5801, 7005, 5835,
	 4489, 4368,-17670,-15406,-16599,-19895,-22217,-18302,
	-2333,-2471,-2699,-2446,-2180,-2333,  963, -506,
	  186, -672,-1274,  963,-11074,-17163,-14689,-18192,
	-20531,-11074,21172,-30015,-31726,31816,32095,-32261,
	-30015,-28311,-28489,-28849,-29442,-29764,-28311, 3756,
	 4707, 5657, 6706, 7332, 3756,-32767,-32767,16753,
	-16545,-32767,-32767,16753,-16545,  116, 4914,  249,
	-4170, 2097,  116,    0,    0,-2744,  731, 3962,
	  579,    0,    0,  587,  888,  943,  118,-2409,
	-3556,-1698,-1494,-1706,-2325, 3337, 3911, 4484,
	 4484, 4484, 4484,16545,15450,16329,17546,18318,
	16788
};
static short mario_shocking_tbl[]={
	    1,    0,	/* chn14translate x */
	    1,    1,	/* chn14 translate y */
	    6,    2,	/* chn14 translate z */
	    1,    0,	/* chn14 rotate x */
	    1,    8,	/* chn14 rotate y */
	    1,    0,	/* chn14 rotate z */
	    1,    0,	/* <m_waist>1 rotate x */
	    6,  189,	/* <m_waist>1 rotate y */
	    6,  195,	/* <m_waist>1 rotate z */
	    6,  171,	/* <m_body>1 rotate x */
	    6,  177,	/* <m_body>1 rotate y */
	    6,  183,	/* <m_body>1 rotate z */
	    1,    0,	/* <m_head>1 rotate x */
	    1,    0,	/* <m_head>1 rotate y */
	    6,  165,	/* <m_head>1 rotate z */

	    1,  161,	/* chn6 rotate x */
	    1,  162,	/* chn6 rotate y */
	    1,  163,	/* chn6 rotate z */
	    6,   33,	/* <m_larmA>1 rotate x */
	    6,   39,	/* <m_larmA>1 rotate y */
	    6,   45,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1_1 rotate x */
	    1,    0,	/* <m_larmB>1_1 rotate y */
	    6,   27,	/* <m_larmB>1_1 rotate z */
	    6,    9,	/* <m_lhand>1_1 rotate x */
	    6,   15,	/* <m_lhand>1_1 rotate y */
	    6,   21,	/* <m_lhand>1_1 rotate z */

	    1,  157,	/* chn10 rotate x */
	    1,  158,	/* chn10 rotate y */
	    1,  159,	/* chn10 rotate z */
	    6,   60,	/* <m_rarmA>1 rotate x */
	    6,   66,	/* <m_rarmA>1 rotate y */
	    6,   72,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	    6,   54,	/* <m_rarmB>1 rotate z */
	    1,   51,	/* <m_rhand>1_1 rotate x */
	    1,   52,	/* <m_rhand>1_1 rotate y */
	    1,   53,	/* <m_rhand>1_1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  160,	/* chn15 rotate z */
	    6,  139,	/* <m_llegA>1 rotate x */
	    6,  145,	/* <m_llegA>1 rotate y */
	    6,  151,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	    1,  138,	/* <m_llegB>1 rotate z */
	    6,  120,	/* <m_ltoot>1 rotate x */
	    6,  126,	/* <m_ltoot>1 rotate y */
	    6,  132,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  164,	/* chn17 rotate z */
	    6,  102,	/* <m_rlegA>1 rotate x */
	    6,  108,	/* <m_rlegA>1 rotate y */
	    6,  114,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	    6,   96,	/* <m_rlegB>1 rotate z */
	    6,   78,	/* <m_rtoot>1 rotate x */
	    6,   84,	/* <m_rtoot>1 rotate y */
	    6,   90,	/* <m_rtoot>1 rotate z */
};
AnimeRecord animMarioShocking = {
	 MAP_ANIM_NORMAL,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	 6,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_shocking_prm,
	mario_shocking_tbl
};
