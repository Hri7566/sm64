/**********************************************************************
 *                                                                    *
 *  Application Data for model:                                       *
 *     ./newmario/nin/objects/mario_title.nin                         *
 *                                                                    *
 *  THIS FILE WAS CREATED BY NINGEN                                   *
 *                                                                    *
 *                                                                    *
 **********************************************************************/


#include <mbi.h>
#include "model.h"

extern Vtx mario_title_v[];
extern Material mario_title_material[];
extern Mtx mario_title_matrix[];



extern Gfx mario_title_model[];
extern Gfx mario_title_grp_g1[];



static NinStruc mario_title_grp_g1_structure = {
	NULL,		/* matrix */ 
	0,	/* num children */
	NULL,	/* no children */
};

static NinDL mario_title_grp_g1_dlist = {
	332,
	mario_title_grp_g1,
	0,
	NULL,
};

static Nin mario_title_grp_g1_data = {
	NIN_GROUP,
	&mario_title_grp_g1_dlist,
	NULL,	/* no custom data */
	&mario_title_grp_g1_structure,
};

static Nin *mario_title_model_children[] = {
	&mario_title_grp_g1_data,
};

static NinStruc mario_title_model_structure = {
	NULL,		/* matrix */ 
	1,	/* num children */
	mario_title_model_children,
};

static NinDL mario_title_model_dlist = {
	2,
	mario_title_model,
	0,
	NULL,
};

Nin mario_title_model_data = {
	NIN_HEADER,
	&mario_title_model_dlist,
	NULL,	/* no custom data */
	&mario_title_model_structure,
};

float mario_title_box[][3] = {
	{ -1270.177544, -522.330602, -402.581733 }, 
	{ 1509.737312, 583.715458, 360.350110 } 
}; 
