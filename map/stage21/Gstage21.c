/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage21 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"


#include "ext3_tatescroll/ext3_heaven_ext_texture.h"

/* ----------------------------------------------------------------	*/

#include "ext3_tatescroll/e3_base_shape.sou"

#include "ext3_tatescroll/e3_bg01_shape.sou"
#include "ext3_tatescroll/e3_bg02_shape.sou"
#include "ext3_tatescroll/e3_bg03_shape.sou"
#include "ext3_tatescroll/e3_bg04_shape.sou"
#include "ext3_tatescroll/e3_bg05_shape.sou"
#include "ext3_tatescroll/e3_bg06_shape.sou"
#include "ext3_tatescroll/e3_bg07_shape.sou"
#include "ext3_tatescroll/e3_bg08_shape.sou"
#include "ext3_tatescroll/e3_bg09_shape.sou"
#include "ext3_tatescroll/e3_bg10_shape.sou"
#include "ext3_tatescroll/e3_bg11_shape.sou"
#include "ext3_tatescroll/e3_bg12_shape.sou"
#include "ext3_tatescroll/e3_bg13_shape.sou"
#include "ext3_tatescroll/e3_bg14_shape.sou"
#include "ext3_tatescroll/e3_bg15_shape.sou"
#include "ext3_tatescroll/e3_bg16_shape.sou"
#include "ext3_tatescroll/e3_bg17_shape.sou"
#include "ext3_tatescroll/e3_bg18_shape.sou"

#include "ext3_tatescroll/e3_deru01_shape.sou"
#include "ext3_tatescroll/e3_deru02_shape.sou"
#include "ext3_tatescroll/e3_kaitendai_shape.sou"
#include "ext3_tatescroll/e3_kuruma_shape.sou"
#include "ext3_tatescroll/e3_kurumajiku_shape.sou"
#include "ext3_tatescroll/e3_rifuto_shape.sou"
#include "ext3_tatescroll/e3_shiso_shape.sou"
#include "ext3_tatescroll/e3_yokoshiso_shape.sou"
#include "ext3_tatescroll/e3_dan0_shape.sou"
#include "ext3_tatescroll/e3_dan1_shape.sou"
#include "ext3_tatescroll/e3_dan2_shape.sou"
#include "ext3_tatescroll/e3_dan3_shape.sou"
#include "ext3_tatescroll/e3_dan4_shape.sou"

/* ----------------------------------------------------------------	*/

#include "ext3_tatescroll/cx2101.flk"
#include "ext3_tatescroll/cx2101.tag"

#include "ext3_tatescroll/e3_deru01_check.flk"
#include "ext3_tatescroll/e3_deru02_check.flk"
#include "ext3_tatescroll/e3_kaitendai_check.flk"
#include "ext3_tatescroll/e3_kuruma_check.flk"
#include "ext3_tatescroll/e3_kurumajiku_check.flk"
#include "ext3_tatescroll/e3_rifuto_check.flk"
#include "ext3_tatescroll/e3_shiso_check.flk"
#include "ext3_tatescroll/e3_yokoshiso_check.flk"
#include "ext3_tatescroll/e3_dan0_check.flk"
#include "ext3_tatescroll/e3_dan1_check.flk"
#include "ext3_tatescroll/e3_dan2_check.flk"
#include "ext3_tatescroll/e3_dan3_check.flk"
#include "ext3_tatescroll/e3_dan4_check.flk"
