/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
			castle move BG   PathData
											December 27 1995
 ********************************************************************************/


/********************************************************************************/
/*		Path Strategys															*/
/********************************************************************************/

	.globl		e_i_test
	.globl		e_button1
	.globl		e_istop
	.globl		e_fileSelect
	.globl		e_select_star
	.globl		e_select_polystar
	.globl		e_courseStarmain

/********************************************************************************/
/*		Test Coin															    */
/********************************************************************************/
e_i_test:
	p_initialize(item)
	p_setbit( flag,stf_moveON )
	p_setf(animepositionY,-16)
	p_softspritemodeON
	p_animereset

	p_while
		p_animeinc
	p_loop


#if 0
/********************************************************************************/
/*		Cursol Path Data															    */
/********************************************************************************/
e_fs_cursol:
	p_initialize(item)
	p_setname(COIN)
	p_setbit( flag,stf_moveON )
	p_setf(animepositionY,-16)
	p_softspritemodeON
	p_animereset

	p_while
		p_program(s_cursolMove)
		p_animeinc
	p_loop
#endif

/********************************************************************************/
/*		istop															    */
/********************************************************************************/
e_istop:
	p_initialize(item)
	p_setbit(flag,stf_moveON)
	p_program(s_istop_init)

istop_main:
	p_while
		p_hitON
		p_program(s_istop_main)
	p_loop
/********************************************************************************/
/*		Button 1															    */
/********************************************************************************/
e_button1:
	p_initialize(item)
	p_setbit(flag,stf_moveON)
	p_program(s_button_init)

button_main:
	p_while
		p_hitON
		p_program(s_buttonMove)
	p_loop

/********************************************************************************/
/*		File SelectStage													    */
/********************************************************************************/
e_fileSelect:
	p_initialize(item)
	p_setbit( flag,stf_moveON | stf_matrixON | stf_createON )
	p_program(s_fileSelect_init)

fileSelect_main:
	p_while
		p_hitON
		p_program(s_fileSelect)
	p_loop



/*===============================================================================*/
/*		Course Star Select Strategy												 */
/*===============================================================================*/

/********************************************************************************/
/*		Course Select Star													    */
/********************************************************************************/
e_select_polystar:
	p_initialize(option)
	p_setbit( flag,stf_moveON )
	p_save_nowpos

	p_while
		p_program(s_select_polystar)
	p_loop

/********************************************************************************/
/*		File SelectStage													    */
/********************************************************************************/
e_courseStarmain:
	p_initialize(option)
	p_setbit( flag,stf_moveON )
	p_program(s_CoStarmain_init)

CoStar_main:
	p_while
		p_program(s_CoStar_main)
	p_loop






















#if 0
/********************************************************************************/
/*		Test											    */
/********************************************************************************/
e_itest:
	p_initialize(item)
	p_setbit(flag,stf_moveON|stf_matrixON | stf_createON )
	p_program(s_itest_init)

itest_main:
	p_while
		p_hitON
		p_program(s_itest_main)
	p_loop

/********************************************************************************/
/*		Test											    */
/********************************************************************************/
e_itest2:
	p_initialize(item)
	p_setbit(flag,stf_moveON)
	p_program(s_itest2_init)

itest2_main:
	p_while
		p_hitON
		p_program(s_itest2_main)
	p_loop
#endif




