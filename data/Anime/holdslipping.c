/********************************************************************************
						Ultra 64 MARIO Brothers

					mario hold slipping animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 19, 1995
 ********************************************************************************/

#include "../headers.h"


static short mario_carry_slip_prm[]={
	    0,   54,   54,   54,   61,   77,   92,  103,
	  117,  140,  158,  161,  157,  154,  154,    0,
	    0,    0,    0,    1,    1,    2,    3,    4,
	    5,    6,    6,    7,    7,16383,-8256,-11506,
	-14563,-13772,-12338,-10515,-8555,-6710,-5232,-4372,
	-4384,-19347,-19347,-19347,-1577,-2257,-2955,-2926,
	-2795,-2599,-2374,-2157,-1985,-1893,-1920,-3829,
	-3829,-3829,-1925,   82, 2060, 1856, 1408,  784,
	   56, -706,-1436,-2061,-2513,-1018,-1018,-1018,
	-5632,-5557,-5352,-5042,-4652,-4211,-3743,-3275,
	-2834,-2445,-2134,-1929,-1854,-1854,-55853,-53952,
	-51409,-52006,-52766,-53879,-55998,-58119,-59199,-60011,
	-61115,-46894,-46894,-46894,-3930,-4408,-5066,-5009,
	-4578,-4128,-3853,-3611,-3351,-2988,-2140,-3624,
	-3624,-3624,33422,32501,30975,29787,28459,27544,
	27664,28524,30230,32201,33611,32970,32970,32970,
	16031,18617,20650,19254,17376,15163,12764,10327,
	 8001, 5934, 4273, 3169, 2767, 2767, 3928, 3762,
	 3453, 3141, 2786, 2404, 2014, 1635, 1284,  979,
	  739,  582,  525,  525, 1404, 3541, 5425, 4734,
	 3730, 2505, 1150, -245,-1591,-2795,-3768,-4417,
	-4654,-4654,-5319,-5294,-5224,-5120,-4989,-4840,
	-4682,-4524,-4375,-4244,-4139,-4070,-4045,-4045,
	15318,14221,12968,13484,14076,12495,-15083,-10094,
	-8137,-6303,-4771,-3720,-3330,-3330,27281,27035,
	26822,27162,27554,27339, 4642, 5107, 4783, 4326,
	 3854, 3485, 3335, 3335, -745,-1399,-2492,-3213,
	-4253,-7035,-38962,-36016,-34208,-32316,-30618,-29395,
	-28925,-28925,  -95,  171,   27,-1192,-2805,-4203,
	-4317,-4112,-2811,-1547, -731, -107,  140,  140,
	  596,  636,  603,  445,  221, -119,-1067,-1556,
	-1280, -848, -362,  136,  365,  365,-15725,-14609,
	-13531,-14022,-15034,-16097,-17257,-17443,-16523,-15845,
	-16285,-17086,-17516,-17516, 1600, 7964,16202,18853,
	19925,20346,19914,18229,14818,11942,11777,12550,
	13073,13073,  -61, -127,  170, 1173, 2449, 3588,
	 4287, 4840, 5597, 6158, 6248, 6156, 6081, 6081,
	 3002, 1399, -294,   67, 1079, 2081, 2894, 3585,
	 3924, 4197, 4733, 5311, 5580, 5580,-25587,-29117,
	-32872,-32070,-29849,-27823,-26814,-25917,-24491,-23144,
	-22099,-21220,-20850,-20850,  208,  105,  149,  601,
	 1197, 1675, 1477, 2022, 2632, 2791, 1783,  397,
	 -300, -300, -200, -224, -247, -240, -221, -192,
	 -107,  -91,  103,  239,  152,   -7,  -94,  -94,
	-15958,-14581,-13246,-13896,-15187,-16375,-16801,-16495,
	-17265,-18134,-18825,-19438,-19702,-19702, 2503, 8267,
	15853,18661,20121,20712,19666,17441,14647,12552,
	12698,13644,14220,14220,-1358, -783,  -77,   47,
	    3,  -41,  354,  -29,-3014,-6240,-8171,-9626,
	-10201,-10201,-2147,-1147,   75,  276,  183,   88,
	  279,   94,-1276,-2909,-4352,-5694,-6286,-6286,
	-25294,-28733,-32505,-32021,-30231,-28629,-28177,-27458,
	-25052,-22451,-20377,-18554,-17772,-17772,32767,32767,
	16753,16796,16915,17095,17321,17577,17848,18119,
	18375,18601,18781,18900,18943,18943,    0,   -6,
	    0,   38,   90,  137,  181,  199,  150,   88,
	   47,   14,    0,    0,    0, -183,    0, 1041,
	 2408, 3671, 4851, 5333, 4031, 2360, 1274,  374,
	    0,    0,-16545,-16549,-16545,-16520,-16487,-16457,
	-16428,-16417,-16448,-16488,-16514,-16536,-16545,-16545,
	32767,32767,16753,16800,16930,17127,17375,17655,
	17952,18249,18530,18777,18974,19104,19152,19152,
	-16545, -370, -414, -443, -406, -357, -302, -243,
	 -183, -126,  -76,  -36,   -9,    0,    0,  747,
	  742,  706,  636,  553,  462,  368,  275,  189,
	  113,   53,   14,    0,    0, 1354,  771,  309,
	  615, 1028, 1515, 2044, 2581, 3095, 3551, 3917,
	 4161, 4250, 4250,    0,  -87, -198, -228, -234,
	 -234, -228, -204, -162, -108,  -56,  -15,    0,
	    0, -834, -846, -810, -677, -506, -345, -206,
	 -112,  -69,  -37,  -15,   -3,    0,    0, 5100,
	 6193, 7611, 7993, 8107, 8356, 9622, 9868, 7535,
	 4272,  910,-1716,-2777,-2777, 7784, 7566, 7784,
	 8742, 9842,11029,12253,13461,14599,15616,15293,
	14955,14625,14477,
};


static short mario_carry_slip_tbl[]={
	    1,    0,	/* chn14translate x */
	   14,    1,	/* chn14 translate y */
	   14,   15,	/* chn14 translate z */
	    1,    0,	/* chn14 rotate x */
	    1,   29,	/* chn14 rotate y */
	    1,    0,	/* chn14 rotate z */
	    1,    0,	/* <m_waist>1_3 rotate x */
	    1,    0,	/* <m_waist>1_3 rotate y */
	   14,  581,	/* <m_waist>1_3 rotate z */
	   14,  539,	/* <m_body>1 rotate x */
	   14,  553,	/* <m_body>1 rotate y */
	   14,  567,	/* <m_body>1 rotate z */
	   14,  497,	/* <m_head>2 rotate x */
	   14,  511,	/* <m_head>2 rotate y */
	   14,  525,	/* <m_head>2 rotate z */

	    1,  480,	/* chn6 rotate x */
	    1,  481,	/* chn6 rotate y */
	   14,  482,	/* chn6 rotate z */
	   14,   86,	/* <m_larmA>1 rotate x */
	   14,  100,	/* <m_larmA>1 rotate y */
	   14,  114,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	   14,   72,	/* <m_larmB>1 rotate z */
	   14,   30,	/* <m_lhand>1 rotate x */
	   14,   44,	/* <m_lhand>1 rotate y */
	   14,   58,	/* <m_lhand>1 rotate z */

	    1,  422,	/* chn10 rotate x */
	    1,  423,	/* chn10 rotate y */
	   14,  424,	/* chn10 rotate z */
	   14,  184,	/* <m_rarmA>1 rotate x */
	   14,  198,	/* <m_rarmA>1 rotate y */
	   14,  212,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	   14,  170,	/* <m_rarmB>1 rotate z */
	   14,  128,	/* <m_rhand>1 rotate x */
	   14,  142,	/* <m_rhand>1 rotate y */
	   14,  156,	/* <m_rhand>1 rotate z */

	   14,  438,	/* chn15 rotate x */
	   14,  452,	/* chn15 rotate y */
	   14,  466,	/* chn15 rotate z */
	   14,  380,	/* <m_llegA>1 rotate x */
	   14,  394,	/* <m_llegA>1 rotate y */
	   14,  408,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	   14,  366,	/* <m_llegB>1 rotate z */
	   14,  324,	/* <m_ltoot>1 rotate x */
	   14,  338,	/* <m_ltoot>1 rotate y */
	   14,  352,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  496,	/* chn17 rotate z */
	   14,  282,	/* <m_rlegA>1 rotate x */
	   14,  296,	/* <m_rlegA>1 rotate y */
	   14,  310,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	   14,  268,	/* <m_rlegB>1 rotate z */
	   14,  226,	/* <m_rfoot>1 rotate x */
	   14,  240,	/* <m_rfoot>1 rotate y */
	   14,  254,	/* <m_rfoot>1 rotate z */
};

AnimeRecord animMarioHoldSlipping = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	 1,
	20,
	mario_carry_slip_prm,
	mario_carry_slip_tbl
};
AnimeRecord animMarioHoldSlipEnd = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 1,							/* start frame     	 */
	 0,							/* loop frame		 */
	14,
	20,
	mario_carry_slip_prm,
	mario_carry_slip_tbl
};
