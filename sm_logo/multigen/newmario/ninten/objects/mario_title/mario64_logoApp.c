/**********************************************************************
 *                                                                    *
 *  Application Data for model:                                       *
 *     ./newmario/nin/objects/mario64_logo.nin                        *
 *                                                                    *
 *  THIS FILE WAS CREATED BY NINGEN                                   *
 *                                                                    *
 *                                                                    *
 **********************************************************************/


#include <mbi.h>
#include "model.h"

extern Vtx mario64_logo_v[];
extern Material mario64_logo_material[];
extern Mtx mario64_logo_matrix[];



extern Gfx mario64_logo_model[];
extern Gfx mario64_logo_grp_g3[];



static NinStruc mario64_logo_grp_g3_structure = {
	NULL,		/* matrix */ 
	0,	/* num children */
	NULL,	/* no children */
};

static NinDL mario64_logo_grp_g3_dlist = {
	333,
	mario64_logo_grp_g3,
	0,
	NULL,
};

static Nin mario64_logo_grp_g3_data = {
	NIN_GROUP,
	&mario64_logo_grp_g3_dlist,
	NULL,	/* no custom data */
	&mario64_logo_grp_g3_structure,
};

static Nin *mario64_logo_model_children[] = {
	&mario64_logo_grp_g3_data,
};

static NinStruc mario64_logo_model_structure = {
	NULL,		/* matrix */ 
	1,	/* num children */
	mario64_logo_model_children,
};

static NinDL mario64_logo_model_dlist = {
	2,
	mario64_logo_model,
	0,
	NULL,
};

Nin mario64_logo_model_data = {
	NIN_HEADER,
	&mario64_logo_model_dlist,
	NULL,	/* no custom data */
	&mario64_logo_model_structure,
};

float mario64_logo_box[][3] = {
	{ -1259.781843, -279.179088, -412.654665 }, 
	{ 1520.133013, 881.302810, 115.077608 } 
}; 
