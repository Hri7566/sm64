/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: pathremovesound.s
					Description	: 
					Date		: 1995.
					Author		: H.yajima

 ********************************************************************************/


/*################*/
#ifdef ASSEMBLER
/*################*/

e_removesound:
	p_initialize(effect)
	p_setbit(flag,stf_moveON )
	p_wait(3)
	p_program(s_removesound)
	p_wait(30)
	p_killobj


/*################*/
#else
/*################*/

extern void s_removesound(void)
{

	unsigned long sound = execstp->s[stw_work0].flag32;
	AudStartSound( execstp, sound );

}

/*################*/
#endif
/*################*/
/*===============================================================================
		end end end end end end end end 
===============================================================================*/



