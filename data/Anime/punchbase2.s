/********************************************************************************
						Ultra 64 MARIO Brothers

				  mario punching base 2 animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							February 8, 1996
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioSecPunchBase:
	.half	 MAP_ANIM_ONETIME 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	 4 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x0268						/* total bytes       */


	.half	    4,    1 	/* chn14_2_1translate x */
	.half	    4,    5 	/* chn14_2_1 translate y */
	.half	    4,    9 	/* chn14_2_1 translate z */
	.half	    1,    0 	/* chn14_2_1 rotate x */
	.half	    1,   13 	/* chn14_2_1 rotate y */
	.half	    1,    0 	/* chn14_2_1 rotate z */
	.half	    4,  158 	/* <m_waist>1_3 rotate x */
	.half	    4,  162 	/* <m_waist>1_3 rotate y */
	.half	    4,  166 	/* <m_waist>1_3 rotate z */
	.half	    4,  146 	/* <m_body>1 rotate x */
	.half	    4,  150 	/* <m_body>1 rotate y */
	.half	    4,  154 	/* <m_body>1 rotate z */
	.half	    4,  134 	/* <m_head>2 rotate x */
	.half	    4,  138 	/* <m_head>2 rotate y */
	.half	    4,  142 	/* <m_head>2 rotate z */
	.half	    1,  130 	/* chn6 rotate x */
	.half	    1,  131 	/* chn6 rotate y */
	.half	    1,  132 	/* chn6 rotate z */
	.half	    4,   30 	/* <m_larmA>1 rotate x */
	.half	    4,   34 	/* <m_larmA>1 rotate y */
	.half	    4,   38 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1 rotate x */
	.half	    1,    0 	/* <m_larmB>1 rotate y */
	.half	    4,   26 	/* <m_larmB>1 rotate z */
	.half	    4,   14 	/* <m_lhand>1 rotate x */
	.half	    4,   18 	/* <m_lhand>1 rotate y */
	.half	    4,   22 	/* <m_lhand>1 rotate z */
	.half	    1,  126 	/* chn10 rotate x */
	.half	    1,  127 	/* chn10 rotate y */
	.half	    1,  128 	/* chn10 rotate z */
	.half	    4,   58 	/* <m_rarmA>1 rotate x */
	.half	    4,   62 	/* <m_rarmA>1 rotate y */
	.half	    4,   66 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1 rotate y */
	.half	    4,   54 	/* <m_rarmB>1 rotate z */
	.half	    4,   42 	/* <m_rhand>1 rotate x */
	.half	    4,   46 	/* <m_rhand>1 rotate y */
	.half	    4,   50 	/* <m_rhand>1 rotate z */
	.half	    1,    0 	/* chn15 rotate x */
	.half	    1,    0 	/* chn15 rotate y */
	.half	    1,  129 	/* chn15 rotate z */
	.half	    4,  114 	/* <m_llegA>1 rotate x */
	.half	    4,  118 	/* <m_llegA>1 rotate y */
	.half	    4,  122 	/* <m_llegA>1 rotate z */
	.half	    1,    0 	/* <m_llegB>1 rotate x */
	.half	    1,    0 	/* <m_llegB>1 rotate y */
	.half	    4,  110 	/* <m_llegB>1 rotate z */
	.half	    4,   98 	/* <m_ltoot>1 rotate x */
	.half	    4,  102 	/* <m_ltoot>1 rotate y */
	.half	    4,  106 	/* <m_ltoot>1 rotate z */
	.half	    1,    0 	/* chn17 rotate x */
	.half	    1,    0 	/* chn17 rotate y */
	.half	    1,  133 	/* chn17 rotate z */
	.half	    4,   86 	/* <m_rlegA>1 rotate x */
	.half	    4,   90 	/* <m_rlegA>1 rotate y */
	.half	    4,   94 	/* <m_rlegA>1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1 rotate y */
	.half	    4,   82 	/* <m_rlegB>1 rotate z */
	.half	    4,   70 	/* <m_rfoot>1 rotate x */
	.half	    4,   74 	/* <m_rfoot>1 rotate y */
	.half	    4,   78 	/* <m_rfoot>1 rotate z */

	.half	0x0000,0xFFF9,0xFFF8,0xFFF7,0xFFF6,0x009E,0x00A1,0x00A4,0x00A7,0x0020,0x0021,0x0023,0x0024,0x3FFF,0xF37D,0xE82F
	.half	0xE9E1,0xEFB8,0xF6B8,0xF443,0xF1F9,0xF128,0xEAB7,0xEB15,0xEB82,0xEC62,0xC034,0xD35E,0xE6C8,0xF498,0xC4B4,0xB9C6
	.half	0x6EB7,0x4A7E,0xEF30,0xEC2B,0xCF70,0xD913,0xF334,0xE152,0x3B57,0x5CF3,0xFDE2,0xFE02,0xFE23,0xFE44,0xFFAD,0xFFB2
	.half	0xFFB5,0xFFB3,0xF0CB,0xF10F,0xF162,0xF1CC,0xC1A5,0xB8E8,0xB4BE,0xB7DA,0x3CAB,0x2446,0x1F05,0x1EBA,0x2448,0x1E00
	.half	0x178E,0x18CC,0xD92B,0xBE4F,0xBEBD,0xC833,0x039C,0x03AF,0x03B9,0x03C4,0xFE70,0xFDDF,0xFD4A,0xFCBA,0xB0F1,0xB230
	.half	0xB3BE,0xB577,0x43B7,0x4323,0x424D,0x4118,0x1311,0x13C7,0x14A0,0x15AD,0x10B3,0x0F99,0x0F2C,0x0FF5,0xBA09,0xBBF2
	.half	0xBDD7,0xC018,0xFF57,0xFF72,0xFF3B,0xFE72,0x00C2,0x00B2,0x00A3,0x0092,0xB456,0xB53F,0xB5E1,0xB5FA,0x32AB,0x3197
	.half	0x30A4,0x2FF6,0xE3E2,0xE279,0xE32F,0xE73E,0xEF3A,0xEDE8,0xEDFE,0xF06A,0xA33A,0xA4B5,0xA658,0xA87B,0x8001,0x8001
	.half	0x4171,0xBF5F,0x8001,0x8001,0x4171,0xBF5F,0x0271,0x1815,0x2741,0x2E11,0x0313,0x0598,0x06FC,0x06A6,0xF214,0xEFB6
	.half	0xEE86,0xEF31,0xF681,0xE3D6,0xD672,0xCFB2,0xFE97,0x04B1,0x09D3,0x0E0B,0x1437,0x0C08,0x05CE,0x0192,0x05AD,0x029A
	.half	0x0079,0xFFAD,0xFE56,0xFE65,0xFEE7,0x0062,0x4CDE,0x4BE9,0x4AC4,0x494C
