/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: pathwaterwave.s
					Description	: 
					Date		: 1995.
					Author		: H.yajima

 ********************************************************************************/


/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************/
/*		player wave program														*/
/********************************************************************************/

e_enemywave:
	p_initialize(effect)		
	p_setbit(flag,stf_moveON)	
	p_jmp(wave_common)

e_playerwave:
	p_initialize(option)
	p_setbit(flag,stf_moveON)
	p_mother_clrbit(effect,stf_wave)
wave_common:
	p_setf(animeangleX,0)
	p_setf(animeangleY,0)
	p_setf(animeangleZ,0)
	p_animereset
	p_do(8)
		p_animeinc				/* 0,1,2,3,4,5,6,7 */
		p_program(s_wave_main)
		p_wait(1)
		p_program(s_wave_main)
	p_next
	p_killshape


/*################*/
#else
/*################*/


/*------------------------------------------------------------------------------*/
/*		enemy(shark) wave initialize											*/
/*------------------------------------------------------------------------------*/

#define		wavescale	(execstp->s[stw_work1].f)

extern void s_wave_main(void)
{
	float water = mcWaterCheck(obj_worldX,obj_worldZ);
	if ( ( obj_timer == 0 ) && ( frameCounter % 2 ) )	s_remove_obj(execstp);

	execstp->s[stw_worldY].f =  water + 5;
	if ( obj_timer == 0 ) wavescale = execstp->map.scale[0];

	if (obj_animecounter > 3 ){
		wavescale  -=  0.1;
		if ( wavescale < 0 ) wavescale = 0;
		execstp->map.scale[0] =  wavescale;
		execstp->map.scale[2] =  wavescale;
	}

}

/*------------------------------------------------------------------------------*/


/*################*/
#endif
/*################*/
/*===============================================================================
		end end end end end end end end 
===============================================================================*/



