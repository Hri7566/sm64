/********************************************************************************
						Ultra 64 MARIO Brothers

				mario spin jumping start animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

						   September 19, 1995
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioSpinJpStart:
	.half	 MAP_ANIM_ONETIME 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	 5 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x022C						/* total bytes       */


	.half	    1,    0 	/* chn14translate x */
	.half	    1,    1 	/* chn14 translate y */
	.half	    1,    0 	/* chn14 translate z */
	.half	    1,    0 	/* chn14 rotate x */
	.half	    1,    2 	/* chn14 rotate y */
	.half	    1,    0 	/* chn14 rotate z */
	.half	    1,    0 	/* <m_waist>1_3 rotate x */
	.half	    1,    0 	/* <m_waist>1_3 rotate y */
	.half	    5,  135 	/* <m_waist>1_3 rotate z */
	.half	    1,    0 	/* <m_body>1 rotate x */
	.half	    1,    0 	/* <m_body>1 rotate y */
	.half	    5,  130 	/* <m_body>1 rotate z */
	.half	    1,    0 	/* <m_head>2 rotate x */
	.half	    1,    0 	/* <m_head>2 rotate y */
	.half	    5,  125 	/* <m_head>2 rotate z */
	.half	    1,  121 	/* chn6 rotate x */
	.half	    1,  122 	/* chn6 rotate y */
	.half	    1,  123 	/* chn6 rotate z */
	.half	    5,   11 	/* <m_larmA>1 rotate x */
	.half	    5,   16 	/* <m_larmA>1 rotate y */
	.half	    5,   21 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1 rotate x */
	.half	    1,    0 	/* <m_larmB>1 rotate y */
	.half	    5,    6 	/* <m_larmB>1 rotate z */
	.half	    1,    3 	/* <m_lhand>1 rotate x */
	.half	    1,    4 	/* <m_lhand>1 rotate y */
	.half	    1,    5 	/* <m_lhand>1 rotate z */
	.half	    1,  117 	/* chn10 rotate x */
	.half	    1,  118 	/* chn10 rotate y */
	.half	    1,  119 	/* chn10 rotate z */
	.half	    5,   32 	/* <m_rarmA>1 rotate x */
	.half	    5,   37 	/* <m_rarmA>1 rotate y */
	.half	    5,   42 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1 rotate y */
	.half	    5,   27 	/* <m_rarmB>1 rotate z */
	.half	    1,    0 	/* <m_rhand>1 rotate x */
	.half	    1,    0 	/* <m_rhand>1 rotate y */
	.half	    1,   26 	/* <m_rhand>1 rotate z */
	.half	    1,    0 	/* chn15 rotate x */
	.half	    1,    0 	/* chn15 rotate y */
	.half	    1,  120 	/* chn15 rotate z */
	.half	    5,  102 	/* <m_llegA>1 rotate x */
	.half	    5,  107 	/* <m_llegA>1 rotate y */
	.half	    5,  112 	/* <m_llegA>1 rotate z */
	.half	    1,    0 	/* <m_llegB>1 rotate x */
	.half	    1,    0 	/* <m_llegB>1 rotate y */
	.half	    5,   97 	/* <m_llegB>1 rotate z */
	.half	    5,   82 	/* <m_ltoot>1 rotate x */
	.half	    5,   87 	/* <m_ltoot>1 rotate y */
	.half	    5,   92 	/* <m_ltoot>1 rotate z */
	.half	    1,    0 	/* chn17 rotate x */
	.half	    1,    0 	/* chn17 rotate y */
	.half	    1,  124 	/* chn17 rotate z */
	.half	    5,   67 	/* <m_rlegA>1 rotate x */
	.half	    5,   72 	/* <m_rlegA>1 rotate y */
	.half	    5,   77 	/* <m_rlegA>1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1 rotate y */
	.half	    5,   62 	/* <m_rlegB>1 rotate z */
	.half	    5,   47 	/* <m_rfoot>1 rotate x */
	.half	    5,   52 	/* <m_rfoot>1 rotate y */
	.half	    5,   57 	/* <m_rfoot>1 rotate z */

	.half	0x0000,0x00BD,0x3FFF,0xFF8A,0xF980,0xF06F,0xFB85,0xFB4B,0xFAC7,0xFA3B,0xF9E8,0x85E8,0x7BDC,0x6532,0x4D21,0x3EE0
	.half	0x94B7,0x9980,0xA44C,0xAFC3,0xB68D,0x3403,0x4131,0x5EEC,0x7E7E,0x9130,0xED8C,0xF913,0xF95C,0xFA01,0xFAB0,0xFB18
	.half	0x031B,0x0DB6,0x25A1,0x3F06,0x4E11,0x1597,0x1A1F,0x2458,0x2F34,0x35A2,0xB412,0xC285,0xE31E,0x05B9,0x1A39,0xFFEC
	.half	0x0032,0x00D2,0x017C,0x01E1,0x025C,0x0239,0x01EC,0x0199,0x0168,0xC7E1,0xCD6B,0xD9EA,0xE72F,0xEF0B,0x4AB2,0x411D
	.half	0x2B7D,0x1488,0x06EF,0xFFD8,0xFFB6,0xFF6B,0xFF1A,0xFEEB,0xFEAE,0xFEC3,0xFEF0,0xFF20,0xFF3D,0x99AF,0x9E88,0xA97A
	.half	0xB518,0xBBFA,0x00A8,0x0083,0x002E,0xFFD6,0xFFA1,0xFF16,0xFF10,0xFF05,0xFEF8,0xFEF1,0xC900,0xCE89,0xDB06,0xE849
	.half	0xF023,0x4613,0x3D01,0x288A,0x12CF,0x05F1,0xFFEE,0xFFFA,0x0013,0x002F,0x003F,0x006A,0x0067,0x0061,0x005B,0x0057
	.half	0x9C3A,0xA0CB,0xAB17,0xB606,0xBC7F,0x7FFF,0x7FFF,0x4171,0xBF5F,0x7FFF,0x7FFF,0x4171,0xBF5F,0x07C9,0x0529,0xFF3F
	.half	0xF8F5,0xF53C,0x13EC,0x1103,0x0A73,0x037A,0xFF5B,0x4A97,0x4911,0x45A6,0x4212,0x400F
