/********************************************************************************
						Ultra 64 MARIO Brothers

					mario swim taking animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							February 1, 1996
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioSwimTaking:
	.half	 MAP_ANIM_ONETIME 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	 5 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x02CC						/* total bytes       */


	.half	    1,    0 	/* chn14_2_3translate x */
	.half	    1,    1 	/* chn14_2_3 translate y */
	.half	    1,    0 	/* chn14_2_3 translate z */
	.half	    1,    0 	/* chn14_2_3 rotate x */
	.half	    1,    2 	/* chn14_2_3 rotate y */
	.half	    1,    0 	/* chn14_2_3 rotate z */
	.half	    5,  205 	/* <m_waist>1_3 rotate x */
	.half	    5,  210 	/* <m_waist>1_3 rotate y */
	.half	    5,  215 	/* <m_waist>1_3 rotate z */
	.half	    5,  190 	/* <m_body>1 rotate x */
	.half	    5,  195 	/* <m_body>1 rotate y */
	.half	    5,  200 	/* <m_body>1 rotate z */
	.half	    5,  175 	/* <m_head>2 rotate x */
	.half	    5,  180 	/* <m_head>2 rotate y */
	.half	    5,  185 	/* <m_head>2 rotate z */
	.half	    5,  159 	/* chn6 rotate x */
	.half	    5,  164 	/* chn6 rotate y */
	.half	    5,  169 	/* chn6 rotate z */
	.half	    5,   23 	/* <m_larmA>1 rotate x */
	.half	    5,   28 	/* <m_larmA>1 rotate y */
	.half	    5,   33 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1 rotate x */
	.half	    1,    0 	/* <m_larmB>1 rotate y */
	.half	    5,   18 	/* <m_larmB>1 rotate z */
	.half	    5,    3 	/* <m_rhand2>2 rotate x */
	.half	    5,    8 	/* <m_rhand2>2 rotate y */
	.half	    5,   13 	/* <m_rhand2>2 rotate z */
	.half	    5,  143 	/* chn10 rotate x */
	.half	    5,  148 	/* chn10 rotate y */
	.half	    5,  153 	/* chn10 rotate z */
	.half	    5,   58 	/* <m_rarmA>1 rotate x */
	.half	    5,   63 	/* <m_rarmA>1 rotate y */
	.half	    5,   68 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1 rotate y */
	.half	    5,   53 	/* <m_rarmB>1 rotate z */
	.half	    5,   38 	/* <m_rhand2>1 rotate x */
	.half	    5,   43 	/* <m_rhand2>1 rotate y */
	.half	    5,   48 	/* <m_rhand2>1 rotate z */
	.half	    1,    0 	/* chn15 rotate x */
	.half	    1,    0 	/* chn15 rotate y */
	.half	    1,  158 	/* chn15 rotate z */
	.half	    5,  128 	/* <m_llegA>1 rotate x */
	.half	    5,  133 	/* <m_llegA>1 rotate y */
	.half	    5,  138 	/* <m_llegA>1 rotate z */
	.half	    1,    0 	/* <m_llegB>1 rotate x */
	.half	    1,    0 	/* <m_llegB>1 rotate y */
	.half	    5,  123 	/* <m_llegB>1 rotate z */
	.half	    5,  108 	/* <m_ltoot>1 rotate x */
	.half	    5,  113 	/* <m_ltoot>1 rotate y */
	.half	    5,  118 	/* <m_ltoot>1 rotate z */
	.half	    1,    0 	/* chn17 rotate x */
	.half	    1,    0 	/* chn17 rotate y */
	.half	    1,  174 	/* chn17 rotate z */
	.half	    5,   93 	/* <m_rlegA>1 rotate x */
	.half	    5,   98 	/* <m_rlegA>1 rotate y */
	.half	    5,  103 	/* <m_rlegA>1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1 rotate y */
	.half	    5,   88 	/* <m_rlegB>1 rotate z */
	.half	    5,   73 	/* <m_rfoot>1 rotate x */
	.half	    5,   78 	/* <m_rfoot>1 rotate y */
	.half	    5,   83 	/* <m_rfoot>1 rotate z */

	.half	0x0000,0x00BD,0x3FFF,0x165C,0x106F,0x031D,0xF51D,0xED1F,0xFE3D,0xFC6D,0xF868,0xF44E,0xF240,0xE9A6,0xE8EC,0xE72F
	.half	0xE51C,0xE35C,0xC035,0xC770,0xD7AB,0xE8B2,0xF252,0x601D,0x687E,0x7BE7,0x91C2,0xA173,0x9B40,0x9A8C,0x9872,0x94F6
	.half	0x901C,0x3400,0x2669,0x0800,0xE84F,0xD6DF,0xFE96,0x071D,0x156F,0x24E2,0x2ED5,0x0C9F,0x16CB,0x236F,0x23DD,0x212F
	.half	0xE561,0xE91F,0xEF79,0xF6C3,0xFB27,0xD2F4,0xD66F,0xDE38,0xE653,0xEAC6,0xB7D6,0xB8DA,0xBAD6,0xBC30,0xBB51,0x377E
	.half	0x330D,0x2879,0x1BDF,0x115A,0x4D79,0x4CE0,0x4BF5,0x4C07,0x4E65,0xFF40,0xFEF3,0xFEFA,0x008E,0x01FA,0x01EF,0x01A5
	.half	0x0144,0x0125,0x0124,0xBB29,0xB09B,0xACE8,0xD219,0xF445,0x51B6,0x4935,0x35F1,0x2146,0x1492,0x0AE5,0x0BD8,0x0DEE
	.half	0x1002,0x10EF,0x112B,0x1108,0x10BE,0x1078,0x1063,0xAF11,0xB1FF,0xB8B5,0xC014,0xC4FC,0x00D8,0x00E4,0x00CE,0x003C
	.half	0xFFBD,0xFF60,0xFF87,0xFF9F,0xFF46,0xFEF0,0xBCC5,0xB5B2,0xB455,0xD18E,0xEC55,0x536B,0x49C4,0x33F0,0x1CA0,0x0E86
	.half	0xF70F,0xF512,0xF0A4,0xEC14,0xE9B0,0xEBAE,0xEC5A,0xEDDE,0xEF7C,0xF076,0xA7A0,0xABA2,0xB4CD,0xBED8,0xC57B,0x8098
	.half	0x7E68,0x78DA,0x715C,0x6960,0x7FFD,0x8005,0x8010,0x8020,0x8031,0x414D,0x41D1,0x4325,0x44EE,0x46D6,0xBF5F,0x80BC
	.half	0x82C1,0x87E9,0x8EDF,0x9648,0x7FF4,0x7FD5,0x7F88,0x7F20,0x7EB1,0x41A1,0x4226,0x437C,0x4548,0x4732,0xBF5F,0x1071
	.half	0x0E3B,0x092D,0x03A4,0x0000,0xF7E8,0xF8F2,0xFB58,0xFE0D,0x0000,0xDC4C,0xDBCB,0xDA96,0xD923,0xD7E9,0x0037,0x0045
	.half	0x0057,0x004B,0x0000,0x158F,0x1297,0x0BDA,0x0494,0x0000,0x15C6,0x12D8,0x0C4E,0x058B,0x01F3,0xFFFA,0xFFF8,0xFFF4
	.half	0xFFF5,0x0000,0xFFFA,0xFFF8,0xFFF7,0xFFF8,0x0000,0x5729,0x5B5E,0x64D7,0x6EDB,0x74AF
