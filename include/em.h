#ifndef _EM_H_
#define	_EM_H_

typedef struct {
	int		x;
	int		y;
	unsigned int	buttons;
} MouseState;

#define	BUTTON_LEFT		0x4
#define	BUTTON_MIDDLE	0x2
#define	BUTTON_RIGHT	0x1


typedef struct {
	unsigned short button;		/* all of the 14 buttons */
	signed char	   stick_x;
	signed char	   stick_y;
} GamePad;

/* bit assignments for buttons */

#define	CONT_RIGHT		0x0001
#define	CONT_LEFT		0x0002
#define	CONT_DOWN		0x0004
#define	CONT_UP			0x0008
#define	CONT_START		0x0010
#define	CONT_BACK		0x0020
#define	CONT_B			0x0040
#define	CONT_A			0x0080

#define	CONT_XR			0x0100
#define	CONT_XL			0x0200
#define	CONT_XD			0x0400
#define	CONT_XU			0x0800
#define	CONT_R			0x1000
#define	CONT_L			0x2000
#define	CONT_RESET		0x8000

/*
 * Function Prototypes
 */

extern void	emDisplayBuffer(void *);
extern void	emGetMouseState(MouseState *);
extern int	emInitController(int);
extern int	emGetController(int, GamePad *);
extern int	emMemoryDump(const char *);
extern int	emPrintf(const char *, ...);
extern void	emTaskStart(Task *, int);

#endif /* !_EM_H_ */
