/********************************************************************************
	debug.h: Mario Brothers 64  system header

	January 14, 1997
 ********************************************************************************/

#ifndef DEBUG_H
#define	DEBUG_H


/********************************************************************************/
/*																				*/
/*																				*/
/*																				*/
/*																				*/
/*		Debug thread.															*/
/*																				*/
/*																				*/
/*																				*/
/*																				*/
/********************************************************************************/

#define	DEBUG_THREAD_ID		2
#define	DEBUG_NMESSAGES		1
#define	DEBUG_STACKSIZE		0x800
#define	DEBUG_STACKSIZE64	(DEBUG_STACKSIZE/8)

typedef struct DebugThread {
	OSThread	thread;							/* Debug thread record			*/
	u64			stack[DEBUG_STACKSIZE64];		/* Debug thread stack memory	*/
	OSMesgQueue	mesgque;						/* Debug thread message queue	*/
	OSMesg		mesgbuf[DEBUG_NMESSAGES];		/* Debug thread message buffer	*/

	u16 *		framebuf;						/* pointer to the frame buffer	*/
	u16			width;							/* screen width					*/
	u16			depth;							/* screen depth (8 or 16 or 32)	*/
} DebugThread;

extern void	DbStartDebugger(void);
extern void DbSetFrameBuffer(u16 *framebuf, u16 width, u16 depth);


/********************************************************************************/
/*																				*/
/*																				*/
/*																				*/
/*																				*/
/*		Watch register															*/
/*																				*/
/*																				*/
/*																				*/
/*																				*/
/********************************************************************************/

#define	DEBUG_WATCH_NO	0x00000000
#define	DEBUG_WATCH_WO	0x00000001
#define	DEBUG_WATCH_RO	0x00000002
#define	DEBUG_WATCH_RW	0x00000003

#define	WatchAddress(addr,flag)		SetWatch(((u32)(addr)&0xfffffff8)|(flag))

extern void  SetWatch(u32 watch);


#endif
