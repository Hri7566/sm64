/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					  move BG stage36  Strategy

					March 31 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

e_don_maruta:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON )
	p_setshapeinfo(maruta_check_info)
	p_save_nowpos
	p_setf(movebg_checkdist,2000)
	p_program(s_don_maruta_init)

	p_while
		p_program(s_don_maruta_main)
		p_program(stMainMoveBG)
	p_loop

/*################*/
#else
/*################*/


/*==================================================================================================
		C Program
 ===================================================================================================*/
#define don_maruta_move_length		(execstp->s[stw_work0].f)
#define don_maruta_stX				(execstp->s[stw_work1].f)
#define don_maruta_stZ				(execstp->s[stw_work2].f)

/***************************************************************************************************
						    Init
***************************************************************************************************/
extern void s_don_maruta_init(void)
{
	don_maruta_stX = 3970;
	don_maruta_stZ = 3654;

	don_maruta_move_length = (3581-3970)*(3581-3970) + (3308-3654)*(3308-3654);

	obj_angleY = degree(48.4);

	obj_speedF = 0;
	obj_speedX = 0;
	obj_speedZ = 0;

	obj_animeangleX = 0;
	obj_anglespeedX = 0;
}


/***************************************************************************************************
						    Main
***************************************************************************************************/
static void don_maruta_PlayerOnCheck(void)
{
	float p_posz;


	if (player1stp->ride_strat == execstp) {
		p_posz = (player_posZ - obj_worldZ)*cos(obj_angleY*-1) - (player_posX - obj_worldX)*sin(obj_angleY*-1);
		
		if (p_posz > 0) obj_anglespeedX += 0x10;
		else			obj_anglespeedX -= 0x10;

		if (obj_anglespeedX >  0x200) obj_anglespeedX =  0x200;
		if (obj_anglespeedX < -0x200) obj_anglespeedX = -0x200;

	}
	else {
		if (ObjApproach(execstp,obj_attX,obj_attY,obj_attZ,100) ) {
			if (obj_anglespeedX != 0x0) {
				if (obj_anglespeedX > 0x0) obj_anglespeedX -= 0x10;
				else					   obj_anglespeedX += 0x10;
				if (obj_anglespeedX < 0x10  && obj_anglespeedX > -0x10)   obj_anglespeedX = 0x0;
			}
		}
		else {
			if (obj_anglespeedX != 0x100) {
				if (obj_anglespeedX > 0x100) obj_anglespeedX -= 0x10;
				else					 	 obj_anglespeedX += 0x10;
				if (obj_anglespeedX < 0x110  && obj_anglespeedX > 0xf0)   obj_anglespeedX = 0x100;
			}
		}
	}

}
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_don_maruta_main(void)
{

	float be_worldX = obj_worldX;
	float be_worldZ = obj_worldZ;


	don_maruta_PlayerOnCheck();

	obj_speedF = obj_anglespeedX/0x40;

	obj_speedX = obj_speedF*sin(obj_angleY);
	obj_speedZ = obj_speedF*cos(obj_angleY);

	obj_worldX += obj_speedX;
	obj_worldZ += obj_speedZ;



	if (don_maruta_move_length <  ((obj_worldX-don_maruta_stX)*(obj_worldX-don_maruta_stX) + (obj_worldZ-don_maruta_stZ)*(obj_worldZ-don_maruta_stZ))   ) {
		obj_speedF = 0;
		obj_worldX = be_worldX;
		obj_worldZ = be_worldZ;
		obj_speedX = 0;
		obj_speedZ = 0;
	}

	obj_animeangleX += obj_anglespeedX;
	
	if(I_abs(obj_animeangleX & 0x1fff)<0x210 && obj_anglespeedX != 0) {
		objsound(NA_SE2_LOGROLL);
	}


}




/*################*/
#endif
/*################*/


