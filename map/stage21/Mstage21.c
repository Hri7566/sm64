/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage21 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

#define		STAGE21_CLEAR_R		0
#define		STAGE21_CLEAR_G		12
#define		STAGE21_CLEAR_B		31

extern Gfx gfx_e3_base[];

extern Gfx gfx_e3_bg01[];
extern Gfx gfx_e3_bg02[];
extern Gfx gfx_e3_bg03[];
extern Gfx gfx_e3_bg04[];
extern Gfx gfx_e3_bg05[];
extern Gfx gfx_e3_bg06[];
extern Gfx gfx_e3_bg07[];
extern Gfx gfx_e3_bg08[];
extern Gfx gfx_e3_bg09[];
extern Gfx gfx_e3_bg10[];
extern Gfx gfx_e3_bg11[];
extern Gfx gfx_e3_bg12[];
extern Gfx gfx_e3_bg13[];
extern Gfx gfx_e3_bg14[];
extern Gfx gfx_e3_bg15[];
extern Gfx gfx_e3_bg16[];
extern Gfx gfx_e3_bg17[];
extern Gfx gfx_e3_bg18[];

extern Gfx gfx_e3_deru01[]	  ;
extern Gfx gfx_e3_deru02[]	  ;
extern Gfx gfx_e3_kaitendai[] ;
extern Gfx gfx_e3_kuruma[]	  ;
extern Gfx gfx_e3_kurumajiku[];
extern Gfx gfx_e3_rifuto[]	  ;
extern Gfx gfx_e3_shiso[]	  ;
extern Gfx gfx_e3_yokoshiso[] ;
extern Gfx gfx_e3_dan0[]	  ;
extern Gfx gfx_e3_dan1[]	  ;
extern Gfx gfx_e3_dan2[]	  ;
extern Gfx gfx_e3_dan3[]	  ;
extern Gfx gfx_e3_dan4[]	  ;


/* ===============================================================================
        : Hierarchy map data of Ext3Bg01.
================================================================================== */
Hierarchy RCP_HmsExt3Bg01[] = {
    hmsHeader(3200)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_bg01)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg02.
================================================================================== */
Hierarchy RCP_HmsExt3Bg02[] = {
    hmsHeader(3200)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_bg02)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg03.
================================================================================== */
Hierarchy RCP_HmsExt3Bg03[] = {
    hmsHeader(3900)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_bg03)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg04.
================================================================================== */
Hierarchy RCP_HmsExt3Bg04[] = {
    hmsHeader(2500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_bg04)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg05.
================================================================================== */
Hierarchy RCP_HmsExt3Bg05[] = {
    hmsHeader(1000)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e3_bg05)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg06.
================================================================================== */
Hierarchy RCP_HmsExt3Bg06[] = {
    hmsHeader(1000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_bg06)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg07.
================================================================================== */
Hierarchy RCP_HmsExt3Bg07[] = {
    hmsHeader(2700)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_bg07)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg08.
================================================================================== */
Hierarchy RCP_HmsExt3Bg08[] = {
    hmsHeader(1600)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e3_bg08)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg09.
================================================================================== */
Hierarchy RCP_HmsExt3Bg09[] = {
    hmsHeader(1800)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_bg09)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg10.
================================================================================== */
Hierarchy RCP_HmsExt3Bg10[] = {
    hmsHeader(3200)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_bg10)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg11.
================================================================================== */
Hierarchy RCP_HmsExt3Bg11[] = {
    hmsHeader(1100)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_bg11)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg12.
================================================================================== */
Hierarchy RCP_HmsExt3Bg12[] = {
    hmsHeader(800)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e3_bg12)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg13.
================================================================================== */
Hierarchy RCP_HmsExt3Bg13[] = {
    hmsHeader(2900)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_bg13)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg14.
================================================================================== */
Hierarchy RCP_HmsExt3Bg14[] = {
    hmsHeader(2400)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_bg14)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg15.
================================================================================== */
Hierarchy RCP_HmsExt3Bg15[] = {
    hmsHeader(3500)
    hmsBegin()
        hmsGfx(RM_XSURF, gfx_e3_bg15)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg16.
================================================================================== */
Hierarchy RCP_HmsExt3Bg16[] = {
    hmsHeader(2200)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_bg16)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg17.
================================================================================== */
Hierarchy RCP_HmsExt3Bg17[] = {
    hmsHeader(2700)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_bg17)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Bg18.
================================================================================== */
Hierarchy RCP_HmsExt3Bg18[] = {
    hmsHeader(3300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_bg18)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Deru01.
================================================================================== */
Hierarchy RCP_HmsExt3Deru01[] = {
    hmsHeader(1100)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_deru01)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Deru02.
================================================================================== */
Hierarchy RCP_HmsExt3Deru02[] = {
    hmsHeader(700)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_e3_deru02)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Kaitendai.
================================================================================== */
Hierarchy RCP_HmsExt3Kaitendai[] = {
    hmsHeader(1300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_kaitendai)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Kuruma.
================================================================================== */
Hierarchy RCP_HmsExt3Kuruma[] = {
    hmsHeader(600)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_kuruma)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3KurumaJiku.
================================================================================== */
Hierarchy RCP_HmsExt3KurumaJiku[] = {
    hmsHeader(600)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_kurumajiku)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Rifuto.
================================================================================== */
Hierarchy RCP_HmsExt3Rifuto[] = {
    hmsHeader(700)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_rifuto)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Shiso.
================================================================================== */
Hierarchy RCP_HmsExt3Shiso[] = {
    hmsHeader(1100)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_shiso)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3YokoShiso.
================================================================================== */
Hierarchy RCP_HmsExt3YokoShiso[] = {
    hmsHeader(1900)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_yokoshiso)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Dan0.
================================================================================== */
Hierarchy RCP_HmsExt3Dan0[] = {
    hmsHeader(2100)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_dan0)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Dan1.
================================================================================== */
Hierarchy RCP_HmsExt3Dan1[] = {
    hmsHeader(2100)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_dan1)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Dan2.
================================================================================== */
Hierarchy RCP_HmsExt3Dan2[] = {
    hmsHeader(2100)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_dan2)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Dan3.
================================================================================== */
Hierarchy RCP_HmsExt3Dan3[] = {
    hmsHeader(2100)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_dan3)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext3Dan4.
================================================================================== */
Hierarchy RCP_HmsExt3Dan4[] = {
    hmsHeader(2100)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e3_dan4)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage21Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(9, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, 20000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_PARALLEL,  0,2000,6000,  0,-4500,-8000, GameCamera)
				hmsBegin()
					hmsGfx(RM_SPRITE, gfx_e3_base)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

	hmsEnd()
	hmsExit()
};
