/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					 stage11 MoveBg Strategy

				April 3 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
						Path Data
 ********************************************************************************/

/*===============================================================================
		Floating Board
=================================================================================*/

e_po_floatingbord:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON | stf_YangleSAME)
	p_setshapeinfo(po_mobj01_check_info)
	p_setf(work2,64)
	p_save_nowpos

	p_while
		p_program(s_floatingboard_main)
		p_program(stMainMoveBG)
	p_loop

e_po_floatingbord2:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON | stf_YangleSAME)
	p_setshapeinfo(po_mobj06_check_info)
	p_setf(work2,64)
	p_save_nowpos

	p_while
		p_program(s_floatingboard_main)
		p_program(stMainMoveBG)
	p_loop

e_12_floatingbord:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON | stf_YangleSAME)
	p_setshapeinfo(tin_ita_check_info)
	p_setf(work2,64)
	p_save_nowpos

	p_while
		p_program(s_floatingboard_main)
		p_program(stMainMoveBG)
	p_loop


/*===============================================================================
		Move Block
=================================================================================*/

e_po_moveblock:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON )
	p_setshapeinfo(po_mobj02_check_info)
	p_setangle_random(work3,1,0x20)
	p_save_nowpos

	p_while
		p_program(s_po_moveblock_main)
		p_program(stMainMoveBG)
	p_loop




/*################*/
#else
/*################*/


/***************************************************************************************************
		C Program
 ****************************************************************************************************/



/*===============================================================================
		Floating Board
=================================================================================*/
#define board_BGflag	(execstp->s[stw_work0].d)
#define board_Woffset	(execstp->s[stw_work1].f)
#define board_offset	(execstp->s[stw_work2].f)
#define board_counter	(execstp->s[stw_work3].d)


/***************************************************************************************************
		BG Check Board
****************************************************************************************************/
static float board_YCheck(void)
{
	BGCheckData* bgcp;
	float waterY  = mcWaterCheck(obj_worldX,obj_worldZ);
	float groundY = mcBGGroundCheck(obj_worldX,obj_worldY,obj_worldZ,&bgcp);
	
	if (waterY > groundY+board_offset) {
		board_BGflag = 0;
		return(waterY+board_offset);
	}
	else	{
		board_BGflag = 1;
	    return(groundY+board_offset);
	}

}
/***************************************************************************************************
		BG Check Board
****************************************************************************************************/
static void board_Floating(void)
{

	short p_posx = (player_posX - obj_worldX)*cos(obj_angleY*-1) + (player_posZ - obj_worldZ)*sin(obj_angleY*-1);
	short p_posz = (player_posZ - obj_worldZ)*cos(obj_angleY*-1) - (player_posX - obj_worldX)*sin(obj_angleY*-1);

	if (player1stp->ride_strat == execstp) {
		obj_animeangleX =  p_posz*2;
		obj_animeangleZ = -p_posx*2;
		obj_speedY -= 1;
		if (obj_speedY < 0) obj_speedY = 0;
		board_Woffset += obj_speedY;
		if (board_Woffset > 90) board_Woffset = 90;
	}
	else {
		obj_animeangleX /= 2;
		obj_animeangleZ /= 2;
		board_Woffset -= 5.;
		obj_speedY = 10;
		if (board_Woffset < 0) board_Woffset = 0;
	}

	obj_worldY = (obj_attY - 64) - board_Woffset + sin(board_counter*0x800)*10;

	board_counter++;
	if (board_counter == 0x20) board_counter = 0;

}
/***************************************************************************************************
		Main
****************************************************************************************************/
extern void s_floatingboard_main(void)
{

	
	obj_attY = board_YCheck();

	if (board_BGflag == 0) { obj_mode = 0; }
	else				   { obj_mode = 1; }
	
	switch (obj_mode) {
		case 0: board_Floating();
				break;
				
		case 1: obj_worldY = obj_attY;
				break;

	}
}



/*===============================================================================
		Move Block
=================================================================================*/
#define move_offt	(execstp->s[stw_work0].f)

/***************************************************************************************************
		GO
****************************************************************************************************/
static char moveblock_GO(void)
{
	char flag = 0;
	obj_angleY = obj_animeangleY-0x4000;
	obj_speedY = 0;
	obj_speedF = 12;
	move_offt += obj_speedF;

	if (move_offt > 384) {
		obj_speedF = 0;
		move_offt  = 384;
		flag = 1;
	}

	ObjSpeedOn(execstp);

	return(flag);

}
/***************************************************************************************************
		BACK
****************************************************************************************************/
static char moveblock_BACK(void)
{

	char flag = 0;
	obj_angleY = obj_animeangleY+0x4000;
	obj_speedY = 0;
	obj_speedF = 12;
	move_offt -= obj_speedF;

	if (move_offt < 0) {
		obj_speedF = 0;
		move_offt = 0;
		flag = 1;
	}

	ObjSpeedOn(execstp);

	return(flag);
}
/***************************************************************************************************
		Main
****************************************************************************************************/
extern void s_po_moveblock_main(void)
{


	switch (obj_mode) {
		case 0: if (obj_timer > 60) {
					if (player1stp->ride_strat == execstp) {
						obj_mode = 1;
					}
				}
				break;

		case 1: if (moveblock_GO() == 1) {
					obj_mode = 2;
				}
				break;

		case 2: if (obj_timer > 60) {
					if (moveblock_BACK() == 1) {
						obj_mode = 0;
					}
				}
				break; 
	}

}








/*################*/
#endif
/*################*/


