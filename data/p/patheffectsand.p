/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: patheffectsand.s
					Description	: 
					Date		: 1995.
					Author		: H.yajima

 ********************************************************************************/


/*################*/
#ifdef ASSEMBLER
/*################*/

e_effectsand:
	p_initialize(option)
	p_initialize(option)
	p_setbit(flag,stf_moveON )
	p_program(s_effectsand)
	p_wait(1)
	p_killobj

/*################*/
#else
/*################*/
/********************************************************************************
		C Program (effectsand)
 ********************************************************************************/

static EffectRecord playersand_effect = {
	0,
	5,				/* counter			*/
	S_sand_dust,	/* effect character */
	0,				/* ================ */	
	0,20,			/* speed F     		*/
	20,0,			/* speed Y     		*/
	-4,				/* gravity	   		*/
	30,				/* air fric	   		*/		
	5.0f,			/* ================ */
	2.0f			/* ================ */
};

extern void s_effectsand(void)
{

	s_clear_motherbit(stf_sand);
	s_makeeffect_chiri(&playersand_effect);

}

/*################*/
#endif
/*################*/
/*===============================================================================
		end end end end end end end end 
===============================================================================*/



