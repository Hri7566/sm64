/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Kuramochi Strategy

				Feburary 31 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
						Kuramochi Path Data
 ********************************************************************************/

e_kura_real:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON| stf_YangleSAME| stf_playerangleON)
	p_set_pointer(skelanime,kuramochi_anime)
	p_BGcheckYset
	p_save_nowpos
	p_hitOFF
	p_program(s_kuramochi_init)

	p_while
		p_program(s_kuramochi_main)
	p_loop

e_kuramochi:
	p_initialize(item)
	p_setbit(flag,stf_moveON)
	p_setf(animepositionY,27)
	p_softspritemodeON
	p_sethitbox(110,100)
	p_hitON
	p_animereset

	p_while
		p_animeinc
		p_program(s_kura_coin_main)
	p_loop

/*################*/
#else
/*################*/


/***************************************************************************************************
						    C Program
***************************************************************************************************/
#define jump_flag	 (execstp->s[stw_work0].d)

static HitCheckRecord kuramoti_hit = {
	OBJNAME_PUNCHATTACK,
	0,2,1,0,	/* flag,ap,hp,coin 		 */
	120,60,		/* player attack hitarea */
	100,50,		/* player damage hitarea */
};

static HitCheckRecord kura_coin_hit = {
	OBJNAME_DAMAGE,
	0,2,1,0,	/* flag,ap,hp,coin 		 */
	120,60,		/* player attack hitarea */
	100,50,		/* player damage hitarea */
};

/***************************************************************************************************
						    Initial
***************************************************************************************************/
extern void s_kuramochi_init(void)
{
	execstp->s[stw_gravity].f   = 3.0;
	execstp->s[stw_friction].f  = 1.0;
	execstp->s[stw_specificG].f = 2.0;

	stSetSkelAnimeNumber(0);
	execstp->s[stw_alpha].d = 0;

}
/***************************************************************************************************
						    HitCheck
***************************************************************************************************/
static void kura_HitCheck(void)
{

	s_set_hitparam(execstp,&kuramoti_hit);		/* Set HitCheck Param */

	if ( obj_mail & EMAIL_PLAYERHIT ){
		if ( obj_mail & EMAIL_PLAYERDAMAGE ) { 
			obj_angleY = obj_targetangle + 0x8000;
			obj_speedY = 30;
		}

		if ( obj_mail & EMAIL_PLAYERATTACK ) { 
			obj_mode = 5;
		}

		Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
	}


}
/***************************************************************************************************
		Kuramochi Move
 ****************************************************************************************************/
static void kura_JumpEvent(char bgflag)
{
	short anim_frame = execstp->map.skelanim.frame;

	switch (jump_flag) {
		case 1: stSetSkelAnimeNumber(1);
				if (anim_frame == 5) {
					obj_speedF = 20;
					obj_speedY = 40;
				} 
				if (s_check_animeend() == 1) {
					jump_flag = 2;
					objsound(NA_SE2_FLOG_JUMP);
				}
				break;

		case 2: stSetSkelAnimeNumber(2);
				if ((bgflag & 0x01) == 0x01) {
					obj_speedF = 0;
					obj_speedY = 0;
//					jump_flag = 3;
					jump_flag = 0;
				}
				break;

		case 3: stSetSkelAnimeNumber(3);
				if (s_check_animeend() == 1) {
					jump_flag = 0;
				}
				break;

		case 4: stSetSkelAnimeNumber(4);
				obj_speedF = 10;
				if (obj_timer > FRAME*2) {
					jump_flag  = 0;
					obj_speedF = 0;
					execstp->map.skelanim.frame = 0;
				}
				break;

		case 5: stSetSkelAnimeNumber(4);
				obj_speedF = 5;
				break;
					
	}

//rmonpf(("%d .. frame %d \n",jump_flag,anim_frame));

}
/***************************************************************************************************
		Kuramochi Move
 ****************************************************************************************************/
static void kura_MoveEvent(void){

	short bgflag;

	ShapeRandomAngle(execstp,obj_attX,obj_attY,obj_attZ,200);

	bgflag = ObjMoveEvent();

	if ((bgflag & 0x09)==0x09 && jump_flag == 0) {
		if ((int)(Randomf()*6) == 1) { 		//Walk
			jump_flag = 4; 
			obj_timer = 0;
		}
		else jump_flag = 1;
	}

	kura_JumpEvent(bgflag);
	kura_HitCheck();

	if (PlayerApproach(obj_attX,obj_attY,obj_attZ,800)==0 && (bgflag & 0x09)==0x09) {
		obj_mode = 3;
	}
}
/***************************************************************************************************
		Kuramochi Home
 ****************************************************************************************************/
static void kura_HomeEvent(void){

	short bgflag;
	float dx = obj_attX - obj_worldX;
	float dz = obj_attZ - obj_worldZ;
	short targetAngleY = atan(dz,dx);

 	execstp->s[stw_angleY].d = s_chase_angle(execstp->s[stw_angleY].d,targetAngleY,0x800);

	bgflag = ObjMoveEvent();

	if ((bgflag & 0x09)==0x09 && jump_flag == 0) {
		jump_flag = 5;
	}

	kura_JumpEvent(bgflag);
	kura_HitCheck();


	if (ObjApproach(execstp,obj_attX,obj_attY,obj_attZ,100)) {
		s_makeobj_nowpos(execstp,S_coin,e_kuramochi);
		objsound(NA_SE2_FLOG_APPEAR);
		stSetSkelAnimeNumber(0);
		obj_mode = 4;
		jump_flag = 0;
	}

	if (PlayerApproach(obj_attX,obj_attY,obj_attZ,800)==1) {
		obj_mode = 2;
		jump_flag = 0;
	}

}
/***************************************************************************************************
		Kuramochi Sleep
 ****************************************************************************************************/
static void kura_SleepEvent(void){


	obj_alpha -= 6;


	if (obj_alpha < 0 ) {
		obj_alpha = 0;
		execstp->status = 0;
	}


}
/***************************************************************************************************
		Kuramochi Remove
 ****************************************************************************************************/
static void kura_RemoveEvent(void){

	if (obj_timer == 1) {
		iwa_MakeCoin(execstp,5);
		obj_remove_sound(NA_SE2_FLOG_DOWN);
//		kura_coinPtr = s_makeobj_nowpos(execstp,S_bluecoin,e_escape_coin2);
//		kura_coinPtr->s[stw_angleY].d = obj_angleY;
//		kura_coinPtr->s[stw_speedF].f = 10;
//		kura_coinPtr->s[stw_speedY].f = 50;
		s_kemuri();
		execstp->status = 0;
	}
			
}
/***************************************************************************************************
		Main
 ****************************************************************************************************/
extern void s_kuramochi_main(){


	switch(obj_mode) {
		case 0:
		case 1: obj_alpha += 12;
				if (obj_alpha > 255) {
					obj_alpha = 255;
					motherp->status = 0;
					obj_mode = 2;
				}
				break;

		case 2: kura_MoveEvent();
				if (obj_timer > 30) s_hitON();
				break;

		case 3: kura_HomeEvent();
				break;

		case 4: kura_SleepEvent();
				break;

		case 5: kura_RemoveEvent();
				break;
	}



} 

/***************************************************************************************************
		Coin
 ****************************************************************************************************/
extern void s_kura_coin_main()
{

	s_set_hitparam(execstp,&kura_coin_hit);		/* Set HitCheck Param */

	switch (obj_mode) {
		case 0: if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,400)) {
					s_makeobj_nowpos(execstp,S_kuramochi,e_kura_real);
					objsound(NA_SE2_FLOG_APPEAR);
					obj_mode = 1;
				}
				break;

		case 1: break;
	}

	obj_mail = 0;
}

/*################*/
#endif
/*################*/


