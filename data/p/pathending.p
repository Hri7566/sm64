/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: pathending.s
					Description	: 
					Date		: 1995.
					Author		: H.yajima

 ********************************************************************************/


/*################*/
#ifdef ASSEMBLER
/*################*/

/*--------- select room kinopio program ----------*/
e_kinop_test:
	p_initialize(option)
	p_setbit(flag,stf_moveON )
	p_set_pointer(skelanime,kinopio_anime)
	p_set_skelanime_number(0)
	p_while
		p_program(DoKinopioProcess)
	p_loop

/*--------- openning peach & ending peach --------*/
e_peach_test:
	p_initialize(option)
	p_setbit(flag,stf_moveON )
	p_set_pointer(skelanime,peach_anime)
	p_set_skelanime_number(0)
	p_while
		p_program(DoPeachProcess)
	p_loop

/*################*/
#else
/*################*/
/*################*/
#endif
/*################*/
/*===============================================================================
		end end end end end end end end 
===============================================================================*/



