/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Kirai Strategy

				Feburary 14 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
						Kirai Path Data
 ********************************************************************************/
e_kirai:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON )
	p_hitON
	p_sethitbox2(40,40,40)

	p_wait(1)
	p_while
		p_hitON
		p_program(s_kirai_event)
	p_loop

/*------------------------------------------------------------*/	

e_kopakirai_fire:
	p_initialize(option)
	p_setbit(flag,stf_moveON )
	p_softspritemodeON
	p_setf(animepositionY,-288)
	p_animereset

	p_while
		p_program(s_kopakirai_fire)
	p_loop
/*------------------------------------------------------------*/	

e_kopakirai_smoke:
	p_initialize(option)
	p_setbit(flag,stf_moveON )
	p_softspritemodeON
	p_setf(animepositionY,-288)
	p_setd(alpha,255)
	p_animereset

	p_while
		p_program(s_kopakirai_smoke)
	p_loop

/*################*/
#else
/*################*/


/***************************************************************************************************
		C Program
 ****************************************************************************************************/

/***************************************************************************************************
						Kirai Event  (Player Kirai)
***************************************************************************************************/
extern void s_kirai_event(void)
{

		if ( s_hitcheck(execstp,player1stp) == 1  ){
			Mbitclr(execstp->s[stw_mail].d,EMAIL_PLAYERHIT);
			s_makeobj_nowpos(execstp,S_bombfire,e_bombfire);
			execstp->status = 0;		/* s_removeobj */	/* object remove */
		}

		if ( execstp->s[stw_mail].d & EMAIL_KOPADOWN){
			s_makeobj_nowpos(execstp,S_kopakirai_fire,e_kopakirai_fire);
			obj_remove_sound(NA_SE2_KUPPABOMB);  //SOUND
			Viewshaking(VS_LARGE,obj_worldX,obj_worldY,obj_worldZ);
			execstp->status = 0;		/* s_removeobj */	/* object remove */
		}

	PlayerApproachOnOff(execstp,7000);

}
/***************************************************************************************************
						Kuppa Kirai Fire (Kuppa Kirai)
***************************************************************************************************/
extern void s_kopakirai_fire(void)
{

	StrategyRecord *stratp;

	s_set_scale(((float)obj_timer/14)*9. + 1.);		

	if ((obj_timer & 0x03)==0 && obj_timer < 20) {
			stratp = s_makeobj_nowpos(execstp,S_kopakirai_smoke,e_kopakirai_smoke);
			stratp->s[stw_worldX].f += Randomf()*600 - 400;
			stratp->s[stw_worldZ].f += Randomf()*600 - 400;
			stratp->s[stw_speedY].f += Randomf()*10;
	}


	if ((obj_timer & 0x01)== 0) (execstp->s[stw_animecounter].d)++;

	if (obj_timer == 28) execstp->status = 0; 
}

/***************************************************************************************************
						Kuppa Kirai Fire (Kuppa Kirai)
***************************************************************************************************/
extern void s_kopakirai_smoke(void)
{

	s_set_scale(((float)obj_timer/14)*9. + 1.);		

	if ((obj_timer & 0x01)== 0) (execstp->s[stw_animecounter].d)++;

	(execstp->s[stw_alpha].d) -= 10;
	if (execstp->s[stw_alpha].d < 10 ) execstp->s[stw_alpha].d = 0;

	execstp->s[stw_worldY].f += execstp->s[stw_speedY].f;
	if (obj_timer == 28) execstp->status = 0; 

}



/*################*/
#endif
/*################*/


