/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Strategy

					March 31 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/*==============================================================================
						Path Data
 ===============================================================================*/
e_fm_battan:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON )
	p_setshapeinfo(fm_battan_check_info)
	p_save_nowpos
	p_while
		p_program(s_fm_battan_main)
		p_program(stMainMoveBG)
	p_loop


e_fm_maruta:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON )
	p_setshapeinfo(mo_mobj04_check_info)
	p_save_nowpos
	p_setf(movebg_checkdist,2000)
	p_program(s_fm_maruta_init)

	p_while
		p_program(s_don_maruta_main)
		p_program(stMainMoveBG)
	p_loop
/*################*/
#else
/*################*/


/*==================================================================================================
		C Program
 ===================================================================================================*/

/*------------------------ Battan ----------------------------------------------------------------*/



#define batan_accel	(execstp->s[stw_work0].f)
/***************************************************************************************************
						Battan Move
***************************************************************************************************/
static void battan_Move(void)
{

	batan_accel	+= 4;

	obj_anglespeedX += batan_accel;

	obj_animeangleX -= obj_anglespeedX;

	if (obj_animeangleX < -0x4000) { 
		obj_animeangleX = -0x4000; 
		obj_anglespeedX = 0x0;
		batan_accel = 0;
		obj_mode = 2;
		objsound(NA_SE2_WALL_HIT); 
		Viewshaking(VS_LARGE,obj_worldX,obj_worldY,obj_worldZ);
	}

}
/***************************************************************************************************
						Battan Home
***************************************************************************************************/
static void battan_Home(void)
{

	obj_anglespeedX = 0x90;


	obj_animeangleX += obj_anglespeedX;

	if (obj_animeangleX > 0x00) { obj_animeangleX = -0x00; }

	if (obj_timer == 200) {
		obj_mode = 0;
	}

}
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_fm_battan_main(void)
{

	switch (obj_mode) {

		case 0:	if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,1000)) {
					obj_mode = 1;
					objsound(NA_SE2_WALL_MOVE_DOWN); 
				}
				break;
		case 1:	battan_Move();
				break;


		case 2:	if (obj_timer < 8) obj_worldY = obj_attY + sin(obj_timer*0x1000)*10;
				if (obj_timer == 50) {
					objsound(NA_SE2_WALL_UP);
					obj_mode = 3;
				}
				break;

		case 3: battan_Home();
				break;




	}

}


/*==================================================================================================
		Fire Babulle Maruta
 ===================================================================================================*/
#define fm_maruta_move_length		(execstp->s[stw_work0].f)
#define fm_maruta_stX				(execstp->s[stw_work1].f)
#define fm_maruta_stZ				(execstp->s[stw_work2].f)
/***************************************************************************************************
						    Init
***************************************************************************************************/
extern void s_fm_maruta_init(void)
{
	fm_maruta_stX = 5120;
	fm_maruta_stZ = 6016;

//	fm_maruta_stX = 4096;
//	fm_maruta_stZ = 6016;

	fm_maruta_move_length = (5120-4096)*(5120-4096) + (6016-6016)*(6016-6016);

	obj_angleY = degree(90);

	obj_speedF = 0;
	obj_speedX = 0;
	obj_speedZ = 0;

	obj_animeangleX = 0;
	obj_anglespeedX = 0;
}














/*################*/
#endif
/*################*/


