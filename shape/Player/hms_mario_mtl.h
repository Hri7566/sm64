/********************************************************************************
						Ultra 64 MARIO Brothers

				   Metal mario hierarchy data module

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							February 21, 1996
 ********************************************************************************/




/*################################################################################
 *
 *
 *	Near Mario
 *
 *
 */

/********************************************************************************/
/*	Near mario head hierarchy map data.											*/
/********************************************************************************/

static Hierarchy RCP_MTL_NearMarioHead[] = {
	hmsCProg(0, CtrlMarioHead)
	hmsRotate(0, 0, 0)
	hmsBegin()
		hmsSelect(0, CtrlMarioCap)
		hmsBegin()
			hmsGfx(RM_SURF, RCP_mario_head_metal )
			hmsGfx(RM_SURF, RCP_mario_nohat_metal)
		hmsEnd()

		hmsCoord(142,-51,-126, 22,-40,-135)
		hmsBegin()
			hmsCProg(0, CtrlMarioWing)
			hmsRotate(0,0,0)
			hmsBegin()
				hmsGfx(RM_SPRITE, RCP_move_metal_wing)
			hmsEnd()
		hmsEnd()

		hmsCoord(142,-51,126, -22,40,-135)
		hmsBegin()
			hmsCProg(1, CtrlMarioWing)
			hmsRotate(0,0,0)
			hmsBegin()
				hmsGfx(RM_SPRITE, RCP_move_metal_wing)
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Near mario left hand hierarchy map data.									*/
/********************************************************************************/

static Hierarchy RCP_MTL_NearMarioLeftHand[] = {
	hmsSelect(1, CtrlMarioHand)
	hmsBegin()

		hmsJoint(RM_SURF, NULL, 60, 0, 0)
		hmsBegin()
			hmsCProg(1, CtrlHandScale)
			hmsScale(1.0f)
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_near0_draw0)					/* m_rhand1		*/
			hmsEnd()
		hmsEnd()

		hmsJoint(RM_SURF, RCP_swim_hand_l_draw0, 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_near0_draw0, 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_near0_draw0, 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_near0_draw0, 60, 0, 0)

	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Near mario right hand hierarchy map data.									*/
/********************************************************************************/

static Hierarchy RCP_MTL_NearMarioRightHand[] = {
	hmsSelect(0, CtrlMarioHand)
	hmsBegin()

		hmsJoint(RM_SURF, NULL, 60, 0, 0)
		hmsBegin()
			hmsCProg(0, CtrlHandScale)
			hmsScale(1.0f)
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_near3_metal)					/* m_rhand1		*/
			hmsEnd()
			hmsSucker(0,0,0, 0,CtrlMarioTaking)
		hmsEnd()

		hmsJoint(RM_SURF, RCP_swim_hand_r_metal, 60, 0, 0)
		hmsBegin()
			hmsSucker(0,0,0, 0,CtrlMarioTaking)
		hmsEnd()

		hmsJoint(RM_SURF, RCP_mario_vsign_hand_draw0, 60, 0, 0)

		hmsJoint(RM_SURF, RCP_hat_hand_r_metal, 60, 0, 0)

		hmsJoint(RM_SURF, RCP_hat_hand_r_metal, 60, 0, 0)
		hmsBegin()
			hmsGfx(RM_SPRITE, RCP_hat_hand_wing_metal)
		hmsEnd()
	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Near metal mario hierarchy map data.										*/
/********************************************************************************/

static Hierarchy RCP_MTL_NearMarioHierarchy[] = {
	hmsJoint(RM_SURF, NULL, 0, 0, 0)										/* chn14		*/
	hmsBegin()
		hmsJoint(RM_SURF, RCP_mario_near14_metal, 0, 0, 0)					/* m_waist1_3	*/
		hmsBegin()

			hmsCProg(0, MarioProc1)
			hmsCProg(0, CtrlMarioWaist)
			hmsRotate(0,0,0)
			hmsBegin()

				hmsJoint(RM_SURF, RCP_mario_near_body_metal, 68, 0, 0)		/* m_body1		*/
				hmsBegin()

					hmsJoint(RM_SURF, NULL, 87, 0, 0)						/* m_head2		*/
					hmsBegin()
						hmsCall(RCP_MTL_NearMarioHead)
					hmsEnd()

					hmsJoint(RM_SURF, NULL, 67, -10, 79)					/* chn6			*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_near2_draw0, 0, 0, 0)	/* m_larmA1		*/
						hmsBegin()
							hmsJoint(RM_SURF, RCP_mario_near1, 65, 0, 0)	/* m_larmB1		*/
							hmsBegin()
								hmsCall(RCP_MTL_NearMarioLeftHand)
							hmsEnd()
						hmsEnd()
					hmsEnd()

					hmsJoint(RM_SURF, NULL, 68, -10, -79)					/* chn10		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_near5_draw0, 0, 0, 0)	/* m_rarmA1		*/
						hmsBegin()
							hmsJoint(RM_SURF, RCP_mario_near4, 65, 0, 0)	/* m_rarmB1		*/
							hmsBegin()
								hmsCall(RCP_MTL_NearMarioRightHand)
							hmsEnd()
						hmsEnd()
					hmsEnd()

				hmsEnd()
			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, 42)								/* chn15		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_near11_metal, 0, 0, 0)			/* m_llegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_near10, 89, 0, 0)			/* m_llegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_near9_draw0, 67, 0, 0)	/* m_lfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, -42)							/* chn17		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_near8_draw0, 0, 0, 0)			/* m_rlegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_near7, 89, 0, 0)			/* m_rlegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, NULL, 67, 0, 0)
						hmsBegin()
							hmsCProg(2, CtrlHandScale)
							hmsScale(1.0f)
							hmsBegin()
								hmsGfx(RM_SURF, RCP_mario_near6_metal)			/* m_rfoot1		*/
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()
			hmsEnd()

		hmsEnd()
	hmsEnd()
	hmsReturn()
};
















/*################################################################################
 *
 *
 *	Middle Mario
 *
 *
 */

/********************************************************************************/
/*	Near mario left hand hierarchy map data.									*/
/********************************************************************************/

static Hierarchy RCP_MTL_MidMarioLeftHand[] = {
	hmsSelect(1, CtrlMarioHand)
	hmsBegin()

		hmsJoint(RM_SURF, NULL, 60, 0, 0)
		hmsBegin()
			hmsCProg(1, CtrlHandScale)
			hmsScale(1.0f)
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_mid0_draw0)					/* m_rhand1		*/
			hmsEnd()
		hmsEnd()

		hmsJoint(RM_SURF, RCP_swim_hand_l_draw0, 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_mid0_draw0 , 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_mid0_draw0 , 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_mid0_draw0 , 60, 0, 0)

	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Near mario right hand hierarchy map data.									*/
/********************************************************************************/

static Hierarchy RCP_MTL_MidMarioRightHand[] = {
	hmsSelect(0, CtrlMarioHand)
	hmsBegin()
		hmsJoint(RM_SURF, NULL, 60, 0, 0)
		hmsBegin()
			hmsCProg(0, CtrlHandScale)
			hmsScale(1.0f)
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_mid3_metal)					/* m_rhand1		*/
			hmsEnd()
			hmsSucker(0,0,0, 0,CtrlMarioTaking)
		hmsEnd()

		hmsJoint(RM_SURF, RCP_swim_hand_r_metal, 60, 0, 0)
		hmsBegin()
			hmsSucker(0,0,0, 0,CtrlMarioTaking)
		hmsEnd()

		hmsJoint(RM_SURF, RCP_mario_vsign_hand_draw0, 60, 0, 0)

		hmsJoint(RM_SURF, RCP_hat_hand_r_metal, 60, 0, 0)

		hmsJoint(RM_SURF, RCP_hat_hand_r_metal, 60, 0, 0)
		hmsBegin()
			hmsGfx(RM_SPRITE, RCP_hat_hand_wing_metal)
		hmsEnd()

	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Middle metal mario hierarchy map data.										*/
/********************************************************************************/

static Hierarchy RCP_MTL_MidMarioHierarchy[] = {
	hmsJoint(RM_SURF, NULL, 0, 0, 0)										/* chn14		*/
	hmsBegin()
		hmsJoint(RM_SURF, RCP_mario_mid14_metal, 0, 0, 0)					/* m_waist1_3	*/
		hmsBegin()

			hmsCProg(0, MarioProc1)
			hmsCProg(0, CtrlMarioWaist)
			hmsRotate(0,0,0)
			hmsBegin()

				hmsJoint(RM_SURF, RCP_mario_mid_body_metal, 68, 0, 0)		/* m_body1		*/
				hmsBegin()

					hmsJoint(RM_SURF, NULL, 87, 0, 0)						/* m_head2		*/
					hmsBegin()
						hmsCall(RCP_MTL_NearMarioHead)
					hmsEnd()

					hmsJoint(RM_SURF, NULL, 67, -10, 79)					/* chn6			*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_mid2_draw0, 0, 0, 0)	/* m_larmA1		*/
						hmsBegin()
							hmsJoint(RM_SURF, RCP_mario_mid1, 65, 0, 0)		/* m_larmB1		*/
							hmsBegin()
								hmsCall(RCP_MTL_MidMarioLeftHand)
							hmsEnd()
						hmsEnd()
					hmsEnd()

					hmsJoint(RM_SURF, NULL, 68, -10, -79)					/* chn10		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_mid5_draw0, 0, 0, 0)	/* m_rarmA1		*/
						hmsBegin()
							hmsJoint(RM_SURF, RCP_mario_mid4, 65, 0, 0)		/* m_rarmB1		*/
							hmsBegin()
								hmsCall(RCP_MTL_MidMarioRightHand)
							hmsEnd()
						hmsEnd()
					hmsEnd()

				hmsEnd()
			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, 42)								/* chn15		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_mid11_metal, 0, 0, 0)			/* m_llegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_mid10, 89, 0, 0)			/* m_llegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_mid9_draw0, 67, 0, 0)	/* m_lfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, -42)							/* chn17		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_mid8_draw0, 0, 0, 0)			/* m_rlegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_mid7, 89, 0, 0)				/* m_rlegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, NULL, 67, 0, 0)
						hmsBegin()
							hmsCProg(2, CtrlHandScale)
							hmsScale(1.0f)
							hmsBegin()
								hmsGfx(RM_SURF, RCP_mario_mid6_metal)		/* m_rfoot1		*/
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()
			hmsEnd()

		hmsEnd()
	hmsEnd()
	hmsReturn()
};
















/*################################################################################
 *
 *
 *	Far Mario
 *
 *
 */

/********************************************************************************/
/*	Far mario head hierarchy map data.											*/
/********************************************************************************/

static Hierarchy RCP_MTL_FarMarioHead[] = {
	hmsCProg(0, CtrlMarioHead)
	hmsRotate(0, 0, 0)
	hmsBegin()
		hmsSelect(0, CtrlMarioCap)
		hmsBegin()
			hmsGfx(RM_SURF, RCP_mario_far_head_metal )
			hmsGfx(RM_SURF, RCP_mario_far_nohat_metal)
		hmsEnd()

		hmsCoord(142,-51,-126, 22,-40,-135)
		hmsBegin()
			hmsCProg(0, CtrlMarioWing)
			hmsRotate(0,0,0)
			hmsBegin()
				hmsGfx(RM_SPRITE, RCP_move_metal_wing)
			hmsEnd()
		hmsEnd()

		hmsCoord(142,-51,126, -22,40,-135)
		hmsBegin()
			hmsCProg(1, CtrlMarioWing)
			hmsRotate(0,0,0)
			hmsBegin()
				hmsGfx(RM_SPRITE, RCP_move_metal_wing)
			hmsEnd()
		hmsEnd()

	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Far mario left hand hierarchy map data.										*/
/********************************************************************************/

static Hierarchy RCP_MTL_FarMarioLeftHand[] = {
	hmsSelect(1, CtrlMarioHand)
	hmsBegin()

		hmsJoint(RM_SURF, NULL, 60, 0, 0)
		hmsBegin()
			hmsCProg(1, CtrlHandScale)
			hmsScale(1.0f)
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_far0_draw0)					/* m_rhand1		*/
			hmsEnd()
		hmsEnd()

		hmsJoint(RM_SURF, RCP_swim_hand_l_draw0, 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_far0_draw0 , 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_far0_draw0 , 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_far0_draw0 , 60, 0, 0)

	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Far mario right hand hierarchy map data.									*/
/********************************************************************************/

static Hierarchy RCP_MTL_FarMarioRightHand[] = {
	hmsSelect(0, CtrlMarioHand)
	hmsBegin()

		hmsJoint(RM_SURF, NULL, 60, 0, 0)
		hmsBegin()
			hmsCProg(0, CtrlHandScale)
			hmsScale(1.0f)
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_far3_metal)					/* m_rhand1		*/
			hmsEnd()
			hmsSucker(0,0,0, 0,CtrlMarioTaking)
		hmsEnd()

		hmsJoint(RM_SURF, RCP_swim_hand_r_metal, 60, 0, 0)
		hmsBegin()
			hmsSucker(0,0,0, 0,CtrlMarioTaking)
		hmsEnd()

		hmsJoint(RM_SURF, RCP_mario_vsign_hand_draw0, 60, 0, 0)

		hmsJoint(RM_SURF, RCP_hat_hand_r_metal, 60, 0, 0)

		hmsJoint(RM_SURF, RCP_hat_hand_r_metal, 60, 0, 0)
		hmsBegin()
			hmsGfx(RM_SPRITE, RCP_hat_hand_wing_metal)
		hmsEnd()

	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Far metal mario hierarchy map data.											*/
/********************************************************************************/

static Hierarchy RCP_MTL_FarMarioHierarchy[] = {
	hmsJoint(RM_SURF, NULL, 0, 0, 0)										/* chn14		*/
	hmsBegin()
		hmsJoint(RM_SURF, RCP_mario_far14_metal, 0, 0, 0)					/* m_waist1_3	*/
		hmsBegin()

			hmsCProg(0, MarioProc1)
			hmsCProg(0, CtrlMarioWaist)
			hmsRotate(0,0,0)
			hmsBegin()

				hmsJoint(RM_SURF, RCP_mario_far_body_metal, 68, 0, 0)		/* m_body1		*/
				hmsBegin()

					hmsJoint(RM_SURF, NULL, 87, 0, 0)						/* m_head2		*/
					hmsBegin()
						hmsCall(RCP_MTL_FarMarioHead)
					hmsEnd()

					hmsJoint(RM_SURF, NULL, 67, -10, 79)					/* chn6			*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_far2_draw0, 0, 0, 0)	/* m_larmA1		*/
						hmsBegin()
							hmsJoint(RM_SURF, RCP_mario_far1, 65, 0, 0)		/* m_larmB1		*/
							hmsBegin()
								hmsCall(RCP_MTL_FarMarioLeftHand)
							hmsEnd()
						hmsEnd()
					hmsEnd()

					hmsJoint(RM_SURF, NULL, 68, -10, -79)					/* chn10		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_far5_draw0, 0, 0, 0)	/* m_rarmA1		*/
						hmsBegin()
							hmsJoint(RM_SURF, RCP_mario_far4, 65, 0, 0)		/* m_rarmB1		*/
							hmsBegin()
								hmsCall(RCP_MTL_FarMarioRightHand)
							hmsEnd()
						hmsEnd()
					hmsEnd()

				hmsEnd()
			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, 42)								/* chn15		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_far11_metal, 0, 0, 0)			/* m_llegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_far10, 89, 0, 0)			/* m_llegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_far9_draw0, 67, 0, 0)	/* m_lfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, -42)							/* chn17		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_far8_draw0, 0, 0, 0)			/* m_rlegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_far7, 89, 0, 0)				/* m_rlegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, NULL, 67, 0, 0)
						hmsBegin()
							hmsCProg(2, CtrlHandScale)
							hmsScale(1.0f)
							hmsBegin()
								hmsGfx(RM_SURF, RCP_mario_far6_metal)		/* m_rfoot1		*/
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()
			hmsEnd()

		hmsEnd()
	hmsEnd()
	hmsReturn()
};
