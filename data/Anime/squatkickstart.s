/********************************************************************************
						Ultra 64 MARIO Brothers

				  mario squat kick start animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							January 23, 1996
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioSquatKickStart:
	.half	 MAP_ANIM_ONETIME 				/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 								/* start frame     	 */
	.half	 0 								/* loop frame		 */
	.half	 4 								/* number of frames	 */
	.half	20 								/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x0230						/* total bytes       */


	.half	    1,    0 	/* chn14_3_1translate x */
	.half	    4,    1 	/* chn14_3_1 translate y */
	.half	    4,    5 	/* chn14_3_1 translate z */
	.half	    1,    0 	/* chn14_3_1 rotate x */
	.half	    1,    9 	/* chn14_3_1 rotate y */
	.half	    1,    0 	/* chn14_3_1 rotate z */
	.half	    1,    0 	/* <m_waist>1_3 rotate x */
	.half	    1,    0 	/* <m_waist>1_3 rotate y */
	.half	    4,  138 	/* <m_waist>1_3 rotate z */
	.half	    1,    0 	/* <m_body>1 rotate x */
	.half	    1,    0 	/* <m_body>1 rotate y */
	.half	    4,  134 	/* <m_body>1 rotate z */
	.half	    1,    0 	/* <m_head>2 rotate x */
	.half	    1,    0 	/* <m_head>2 rotate y */
	.half	    4,  130 	/* <m_head>2 rotate z */
	.half	    1,  126 	/* chn6 rotate x */
	.half	    1,  127 	/* chn6 rotate y */
	.half	    1,  128 	/* chn6 rotate z */
	.half	    4,   26 	/* <m_larmA>1 rotate x */
	.half	    4,   30 	/* <m_larmA>1 rotate y */
	.half	    4,   34 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1 rotate x */
	.half	    1,    0 	/* <m_larmB>1 rotate y */
	.half	    4,   22 	/* <m_larmB>1 rotate z */
	.half	    4,   10 	/* <m_lhand>1 rotate x */
	.half	    4,   14 	/* <m_lhand>1 rotate y */
	.half	    4,   18 	/* <m_lhand>1 rotate z */
	.half	    1,  122 	/* chn10 rotate x */
	.half	    1,  123 	/* chn10 rotate y */
	.half	    1,  124 	/* chn10 rotate z */
	.half	    4,   54 	/* <m_rarmA>1 rotate x */
	.half	    4,   58 	/* <m_rarmA>1 rotate y */
	.half	    4,   62 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1 rotate y */
	.half	    4,   50 	/* <m_rarmB>1 rotate z */
	.half	    4,   38 	/* <m_rhand>1 rotate x */
	.half	    4,   42 	/* <m_rhand>1 rotate y */
	.half	    4,   46 	/* <m_rhand>1 rotate z */
	.half	    1,    0 	/* chn15 rotate x */
	.half	    1,    0 	/* chn15 rotate y */
	.half	    1,  125 	/* chn15 rotate z */
	.half	    4,  110 	/* <m_llegA>1 rotate x */
	.half	    4,  114 	/* <m_llegA>1 rotate y */
	.half	    4,  118 	/* <m_llegA>1 rotate z */
	.half	    1,    0 	/* <m_llegB>1 rotate x */
	.half	    1,    0 	/* <m_llegB>1 rotate y */
	.half	    4,  106 	/* <m_llegB>1 rotate z */
	.half	    4,   94 	/* <m_ltoot>1 rotate x */
	.half	    4,   98 	/* <m_ltoot>1 rotate y */
	.half	    4,  102 	/* <m_ltoot>1 rotate z */
	.half	    1,    0 	/* chn17 rotate x */
	.half	    1,    0 	/* chn17 rotate y */
	.half	    1,  129 	/* chn17 rotate z */
	.half	    4,   82 	/* <m_rlegA>1 rotate x */
	.half	    4,   86 	/* <m_rlegA>1 rotate y */
	.half	    4,   90 	/* <m_rlegA>1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1 rotate y */
	.half	    4,   78 	/* <m_rlegB>1 rotate z */
	.half	    4,   66 	/* <m_rfoot>1 rotate x */
	.half	    4,   70 	/* <m_rfoot>1 rotate y */
	.half	    4,   74 	/* <m_rfoot>1 rotate z */

	.half	0x0000,0x008A,0x00B6,0x00E3,0x0111,0xFFD5,0xFFD0,0xFFE5,0x003D,0x3FFF,0xF49F,0xE61A,0xDF80,0x06D2,0xE58B,0xE5F7
	.half	0xE6A0,0xE87F,0xF658,0xFA0F,0xFA0F,0xE614,0xE4CE,0xDE43,0xD416,0xB814,0x38EC,0x3BA5,0x3D04,0x3668,0xE567,0xE419
	.half	0xDF88,0xCAF1,0x61A5,0x5B83,0x55E0,0x5693,0x19B5,0x1846,0x1722,0x1631,0x2218,0x22F8,0x25B4,0x3279,0x03C6,0x0623
	.half	0x09A7,0x1363,0xE884,0xDB55,0xCB09,0xAC6B,0xA823,0xA8B9,0xA72A,0x980A,0x12AF,0x1940,0x219C,0x31B1,0x543D,0x5018
	.half	0x47C0,0x2AC7,0x0011,0x0012,0x0017,0x001F,0xFF33,0xFF31,0xFF21,0xFF0F,0xC653,0xC62D,0xC58D,0xC4AB,0x4567,0x44FE
	.half	0x4302,0x404D,0x0284,0x0283,0x0287,0x028B,0x1600,0x1549,0x12AF,0x0ED6,0x71E5,0x735F,0x78C7,0x80C8,0xFA1B,0xFA4C
	.half	0xFAB5,0xFB38,0x0007,0x0000,0xFFF0,0xFFDC,0xBAA7,0xBB2D,0xBC49,0xBDAE,0x4CA4,0x4C54,0x4D0A,0x4D4C,0x02E2,0x0259
	.half	0x0094,0xFE07,0xECEE,0xED25,0xED88,0xEE3B,0x765F,0x7774,0x79D2,0x7DC7,0x8001,0x8001,0x4171,0xBF5F,0x8001,0x8001
	.half	0x4171,0xBF5F,0xD507,0xD02C,0xCA45,0xB588,0x3272,0x2786,0x1B44,0x08EE,0x5E5E,0x6EDA,0x830D,0xA9C2
