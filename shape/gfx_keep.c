
#include "../headers.h"
#define GFX_SHAPE

#include	"keep/attack_effect.shape"
static long long patch_0 = 0;
#include	"keep/bombfire.shape"
static long long patch_1 = 1;
#include	"keep/butterfly.shape"
static long long patch_2 = 2;
#include	"keep/coin.shape"
static long long patch_3 = 3;
#include	"keep/dokan.shape"
static long long patch_4 = 4;
#include	"keep/door.shape"
static long long patch_5 = 5;
#include	"keep/doorkey.shape"
static long long patch_6 = 6;
#include	"keep/fireD.shape"
static long long patch_7 = 7;
#include	"keep/fish.shape"
static long long patch_8 = 8;
#include	"keep/gfx_stone.shape"
static long long patch_9 = 9;
#include	"keep/grass.shape"
static long long patch_10 = 10;
#include	"keep/hitdatas.shape"
static long long patch_11 = 11;
#include	"keep/itemhat.shape"
static long long patch_12 = 12;
#include	"keep/meter.shape"
static long long patch_13 = 13;
#include	"keep/numbers.shape"
static long long patch_14 = 14;
#include	"keep/one_up_kinoko.shape"
static long long patch_15 = 15;
#include	"keep/polygon_star.shape"
static long long patch_16 = 16;
#include	"keep/sand_dust.shape"
static long long patch_17 = 17;
#include	"keep/sankaku.shape"
static long long patch_18 = 18;
#include	"keep/shadestar.shape"
static long long patch_19 = 19;
#include	"keep/snow_ball.shape"
static long long patch_20 = 20;
#include	"keep/tatefuda.shape"
static long long patch_21 = 21;
#include	"keep/tree.shape"
static long long patch_22 = 22;
