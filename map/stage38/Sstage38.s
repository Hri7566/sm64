/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 38 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage38

SEQ_DoStage38:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1, _GfxStage38SegmentRomStart, _GfxStage38SegmentRomEnd)

	seqBeginConstruction()
		seqHmsMario(S_Mario, ShapePlayer1, e_mario)

		seqBeginScene(1, RCP_Stage38Scene1)
			seqActor(S_coin,  836,0, 1306,  0,90,0,  10,10,0,  e_warp)
			seqActor(S_coin,  854,0,-1335,  0,90,0,  10,20,0,  e_warp)
			seqPort(10, 38, 1, 20)
			seqPort(20, 38, 1, 10)

			seqMapInfo(sand_info)

		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 0,  0,0,1000)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit

