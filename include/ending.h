/********************************************************************************
	staffroll.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	May 5, 1996
 ********************************************************************************/

#ifndef STAFFROLL_H
#define	STAFFROLL_H


#if 0

static String staff_01[] = { "1GAME DIRECTOR", "SHIGERU MIYAMOTO" };

static String staff_02[] = { "2ASSISTANT DIRECTORS", "YOSHIAKI KOIZUMI", "TAKASHI TEZUKA" };

static String staff_03[] = { "2SYSTEM PROGRAMMERS", "YASUNARI NISHIDA", "YOSHINORI TANIMOTO" };

static String staff_04[] = { "3PROGRAMMERS", "HAJIME YAJIMA", "DAIKI IWAMOTO", "TOSHIO IWAWAKI" };

static String staff_05[] = { "4CAMERA PROGRAMMER", "MARIO FACE PROGRAMMER", "TAKUMI KAWAGOE", "GILES GODDARD" };

static String staff_06[] = { "2COURSE DIRECTORS", "YOICHI YAMADA", "YASUHISA YAMAMURA" };

static String staff_07[] = { "2COURSE DESIGNERS", "KENTA USUI", "NAOKI MORI" };

static String staff_08[] = { "3COURSE DESIGNERS", "YOSHIKI HARUHANA", "MAKOTO MIYANAGA", "KATSUHIKO KANNO" };

static String staff_09[] = { "1SOUND COMPOSER", "KOJI KONDO" };

static String staff_10[] = { "4SOUND EFFECTS", "SOUND PROGRAMMER", "YOJI INAGAKI", "HIDEAKI SHIMIZU" };

static String staff_11[] = { "23-D ANIMATORS", "YOSHIAKI KOIZUMI", "SATORU TAKIZAWA" };

static String staff_12[] = { "1ADDITIONAL GRAPHICS", "MASANAO ARIMOTO" };

static String staff_13[] = { "3TECHNICAL SUPPORT", "TAKAO SAWANO", "HIROHITO YOSHIMOTO", "HIROTO YADA" };

static String staff_14[] = { "1TECHNICAL SUPPORT", "SGI N64 PROJECT STAFF" };

static String staff_15[] = { "2PROGRESS MANAGEMENT", "KIMIYOSHI FUKUI", "KEIZO KATO" };

static String staff_16[] = { "5SCREEN TEXT WRITER", "TRANSLATION", "LESLIE SWAN", "MINA AKINO", "HIRO YAMADA" };

static String staff_17[] = { "4MARIO VOICE", "PEACH VOICE", "CHARLES MARTINET", "LESLIE SWAN" };

static String staff_18[] = { "3SPECIAL THANKS TO", "EAD STAFF", "ALL NINTENDO PERSONNEL", "MARIO CLUB STAFF" };

static String staff_19[] = { "1PRODUCER", "SHIGERU MIYAMOTO" };

static String staff_20[] = { "1EXECUTIVE PRODUCER", "HIROSHI YAMAUCHI" };

#endif

#if 0

static String staff_01[] = { "1GAME DIRECTOR", "SHIGERU MIYAMOTO" };

static String staff_02[] = { "2ASSISTANT DIRECTORS", "YOSHIAKI KOIZUMI", "TAKASHI TEZUKA" };

static String staff_03[] = { "2SYSTEM PROGRAMMERS", "YASUNARI NISHIDA", "YOSHINORI TANIMOTO" };

static String staff_04[] = { "3PROGRAMMERS", "HAJIME YAJIMA", "DAIKI IWAMOTO", "TOSHIO IWAWAKI" };

static String staff_05[] = { "1CAMERA PROGRAMMER", "TAKUMI KAWAGOE" };

static String staff_06[] = { "1MARIO FACE PROGRAMMER", "GILES GODDARD" };

static String staff_07[] = { "2COURSE DIRECTORS", "YOICHI YAMADA", "YASUHISA YAMAMURA" };

static String staff_08[] = { "2COURSE DESIGNERS", "KENTA USUI", "NAOKI MORI" };

static String staff_09[] = { "3COURSE DESIGNERS", "YOSHIKI HARUHANA", "MAKOTO MIYANAGA", "KATSUHIKO KANNO" };

static String staff_10[] = { "1SOUND COMPOSER", "KOJI KONDO" };

static String staff_11[] = { "1SOUND EFFECTS", "YOJI INAGAKI" };

static String staff_12[] = { "1SOUND PROGRAMMER", "HIDEAKI SHIMIZU" };

static String staff_13[] = { "23D ANIMATORS", "YOSHIAKI KOIZUMI", "SATORU TAKIZAWA" };

static String staff_14[] = { "1CG DESIGNER", "MASANAO ARIMOTO" };

static String staff_15[] = { "3TECHNICAL SUPPORT", "TAKAO SAWANO", "HIROHITO YOSHIMOTO", "HIROTO YADA" };

static String staff_16[] = { "1TECHNICAL SUPPORT", "SGI. 64PROJECT STAFF" };

static String staff_17[] = { "2PROGRESS MANAGEMENT", "KIMIYOSHI FUKUI", "KEIZO KATO" };

static String staff_18[] = { "3SPECIAL THANKS TO", "JYOHO KAIHATUBU", "ALL NINTENDO", "MARIO CLUB STAFF" };

static String staff_19[] = { "1PRODUCER", "SHIGERU MIYAMOTO" };

static String staff_20[] = { "1EXECUTIVE PRODUCER", "HIROSHI YAMAUCHI" };

#endif



static String staff_01[] = { "1GAME DIRECTOR", "SHIGERU MIYAMOTO" };

static String staff_02[] = { "2ASSISTANT DIRECTORS", "YOSHIAKI KOIZUMI", "TAKASHI TEZUKA" };

static String staff_03[] = { "2SYSTEM PROGRAMMERS", "YASUNARI NISHIDA", "YOSHINORI TANIMOTO" };

static String staff_04[] = { "3PROGRAMMERS", "HAJIME YAJIMA", "DAIKI IWAMOTO", "TOSHIO IWAWAKI" };

static String staff_05[] = { "1CAMERA PROGRAMMER", "TAKUMI KAWAGOE" };

static String staff_06[] = { "1MARIO FACE PROGRAMMER", "GILES GODDARD" };

static String staff_07[] = { "2COURSE DIRECTORS", "YOICHI YAMADA", "YASUHISA YAMAMURA" };

static String staff_08[] = { "2COURSE DESIGNERS", "KENTA USUI", "NAOKI MORI" };

static String staff_09[] = { "3COURSE DESIGNERS", "YOSHIKI HARUHANA", "MAKOTO MIYANAGA", "KATSUHIKO KANNO" };

static String staff_10[] = { "1SOUND COMPOSER", "KOJI KONDO" };

static String staff_11[] = { "4SOUND EFFECTS", "SOUND PROGRAMMER", "YOJI INAGAKI", "HIDEAKI SHIMIZU" };

static String staff_12[] = { "23D ANIMATORS", "YOSHIAKI KOIZUMI", "SATORU TAKIZAWA" };

static String staff_13[] = { "1CG DESIGNER", "MASANAO ARIMOTO" };

static String staff_14[] = { "3TECHNICAL SUPPORT", "TAKAO SAWANO", "HIROHITO YOSHIMOTO", "HIROTO YADA" };

static String staff_15[] = { "1TECHNICAL SUPPORT", "SGI. 64PROJECT STAFF" };

static String staff_16[] = { "2PROGRESS MANAGEMENT", "KIMIYOSHI FUKUI", "KEIZO KATO" };

static String staff_17[] = { "4MARIO VOICE", "PEACH VOICE", "CHARLES MARTINET", "LESLIE SWAN" };

static String staff_18[] = { "3SPECIAL THANKS TO", "JYOHO KAIHATUBU", "ALL NINTENDO", "MARIO CLUB STAFF" };

#if CHINA
static String staff_19[] = { "4PRODUCER", "EXECUTIVE PRODUCER", "SHIGERU MIYAMOTO", "HIROSHI YAMAUCHI" };
static String staff_20[] = { "1CHINA PRODUCTION","IQUE ENGINEERING" };
#else
static String staff_19[] = { "1PRODUCER", "SHIGERU MIYAMOTO" };
static String staff_20[] = { "1EXECUTIVE PRODUCER", "HIROSHI YAMAUCHI" };
#endif


static EndingRecord endingSceneList[] = {
	{ 16, 1, 0x00 + 0*16 + 1,  0x80,     0,  8000,     0, NULL	 	},		/* ending demo	(main map)	*/

	{  9, 1, 0x00 + 0*16 + 1,  0x75,  713,   3918,  -3889, staff_01 },		/* battle field 			*/
	{ 24, 1, 0x00 + 3*16 + 2,  0x2e,  347,   5376,   326, staff_02 },		/* mountain					*/
	{ 12, 1, 0x00 + 1*16 + 2,  0x16,  3800, -4840,  2727, staff_03 },		/* water dungeon			*/
	{  5, 2, 0x00 + 2*16 + 2,  0x19, -5464,  6656, -6575, staff_04 },		/* snow slider				*/
	{  4, 1, 0x00 + 0*16 + 1,  0x3C,   257,  1922,  2580, staff_05 },		/* teresa obake				*/
	{  7, 1, 0xC0 + 3*16 + 1,  0x7B, -6469,  1616, -6054, staff_06 },		/* horror dungeon			*/
	{ 13, 3, 0x00 + 1*16 + 1, -0x20,   508,  1024,  1942, staff_07 },		/* big world(hanacyan)			*/
	{ 22, 2, 0x00 + 2*16 + 1,  0x7C,   -73,    82, -1467, staff_08 },		/* in vlc					*/
	{  8, 1, 0x40 + 0*16 + 1,  0x62, -5906,  1024, -2576, staff_09 },		/* sabaku					*/
	{ 23, 1, 0x00 + 3*16 + 2,  0x2F, -4884, -4607,  -272, staff_10 },		/* water land				*/
	{ 10, 1, 0x00 + 1*16 + 1, -0x22,  1925,  3328,   563, staff_11 },		/* snow mountain 2			*/
	{ 11, 1, 0x00 + 2*16 + 1,  0x69,  -537,  1850,  1818, staff_12 },		/* poll kai					*/
	{ 36, 1, 0x00 + 0*16 + 2, -0x21,  2613,  313 ,  1074, staff_13 },		/* donkey					*/
	{ 13, 1, 0x00 + 3*16 + 3,  0x36, -2609,  512 ,   856, staff_14 },		/* big world				*/
	{ 14, 1, 0x00 + 1*16 + 1, -0x48, -1304,   -71,  -967, staff_15 },		/* clock tower				*/
	{ 15, 1, 0x00 + 2*16 + 1,  0x40,  1565,  1024,  -148, staff_16 },		/* rainbow cruise			*/
	{ 20, 1, 0x00 + 0*16 + 1,  0x18, -1050, -1330, -1559, staff_17 },		/* suisou					*/
	{ 28, 1, 0x00 + 3*16 + 1,  0xf0,  -254,   415, -6045, staff_18 },		/* in the fall				*/
	{ 23, 2, 0x80 + 1*16 + 1,  0xc0,  3948,  1185,   -104, staff_19 },		/* submarine				*/
	{  5, 1, 0x00 + 2*16 + 1,  0x1F,  3169, -4607,  5240, staff_20 },		/* snow mountain			*/
	{ 16, 1, 0x00 + 0*16 + 1,  0x80,     0,   906, -1200, NULL	 	},		/* fin	(main map)			*/
	{  0, 0, 0x00 + 0*16 + 1,  0x00,     0,     0,     0, NULL		}
};


#endif
