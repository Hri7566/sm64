/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 27 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							  Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage27


/* ================================================================================
		: Stage 27 main sequence.
===================================================================================	*/
SEQ_DoStage27:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage27SegmentRomStart	  , _GfxStage27SegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE	  , _ISliderTextureSegmentRomStart, _ISliderTextureSegmentRomEnd)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_hSegmentRomStart	  , _GfxEnemy1_hSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_hSegmentRomStart	  , _HmsEnemy1_hSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_h)

		seqBeginScene(1, RCP_Stage27Scene1)
			seqActor(S_NULL,   5632, 6751,-5631,  0,270,0,  0,10,0,  e_player_landing)
			seqPort(10, 27, 1, 10)				/*	MARIO stage in.	*/
			seqCourseOut(6, 1, 32)
			seqGameClear(6, 1, 38)
			seqGameOver (6, 1, 35)
			seqMapInfo(cx2701_info)
			seqTagInfo(cx2701_info_tag)
			seqEnvironment(ENV_SLIDER)
			seqSetMusic(NA_STG_CASTLE, NA_SLIDER_BGM)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 270,  5632,6451,-5631)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
