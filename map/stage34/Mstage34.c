/********************************************************************************
						Ultra 64 MARIO Brothers

					 stage34 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							Decemver 11, 1995
*********************************************************************************/
#include "../../headers.h"

extern Gfx gfx_kopa3_star[]	;
extern Gfx gfx_kopa3_A[]	;
extern Gfx gfx_kopa3_B[]	;
extern Gfx gfx_kopa3_C[]	;
extern Gfx gfx_kopa3_D[]	;
extern Gfx gfx_kopa3_E[]	;
extern Gfx gfx_kopa3_F[]	;
extern Gfx gfx_kopa3_G[]	;
extern Gfx gfx_kopa3_H[]	;
extern Gfx gfx_kopa3_I[]	;
extern Gfx gfx_kopa3_J[]	;
extern Gfx gfx_kopa3_buoy[]	;

/* ===============================================================================
        : Hierarchy map data of Kopa3A.
================================================================================== */
Hierarchy RCP_HmsKopa3A[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kopa3_A)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Kopa3B.
================================================================================== */
Hierarchy RCP_HmsKopa3B[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kopa3_B)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Kopa3C.
================================================================================== */
Hierarchy RCP_HmsKopa3C[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kopa3_C)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Kopa3D.
================================================================================== */
Hierarchy RCP_HmsKopa3D[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kopa3_D)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Kopa3E.
================================================================================== */
Hierarchy RCP_HmsKopa3E[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kopa3_E)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Kopa3F.
================================================================================== */
Hierarchy RCP_HmsKopa3F[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kopa3_F)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Kopa3G.
================================================================================== */
Hierarchy RCP_HmsKopa3G[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kopa3_G)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Kopa3H.
================================================================================== */
Hierarchy RCP_HmsKopa3H[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kopa3_H)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Kopa3I.
================================================================================== */
Hierarchy RCP_HmsKopa3I[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kopa3_I)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Kopa3J.
================================================================================== */
Hierarchy RCP_HmsKopa3J[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kopa3_J)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Kopa3Buoy.
================================================================================== */
Hierarchy RCP_HmsKopa3Buoy[] = {
    hmsHeader(700)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kopa3_buoy)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage34Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(9, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_KUPPA,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF, gfx_kopa3_star)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
