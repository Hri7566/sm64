/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage6 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx gfx_sel_a[]			;		//		Select room 1F.
extern Gfx gfx_sel_a_a[]		;
extern Gfx gfx_sel_a_g[]		;
extern Gfx gfx_sel_a_s[]		;
extern Gfx gfx_sel_a_ia[]		;
extern Gfx gfx_sel_a_p[]		;
extern Gfx gfx_sel_b[]			;
extern Gfx gfx_sel_c[]			;
extern Gfx gfx_sel_d[]			;
extern Gfx gfx_sel_d_p[]		;
extern Gfx gfx_sel_d_a[]		;
extern Gfx gfx_sel_e[]			;
extern Gfx gfx_sel_e_a[]		;
extern Gfx gfx_sel_f[]			;
extern Gfx gfx_sel_f_a[]		;
extern Gfx gfx_sel_f_aa[]		;
extern Gfx gfx_sel_g[]			;
extern Gfx gfx_sel_g_an[]		;
extern Gfx gfx_sel_g_g[]		;
extern Gfx gfx_sel_g_k[]		;
extern Gfx gfx_sel_g_ia[]		;
extern Gfx gfx_sel_g_a[]		;
extern Gfx gfx_sel_h[]			;
extern Gfx gfx_sel_h_a[]		;
extern Gfx gfx_sel_i[]			;
extern Gfx gfx_sel_c_trap[]		;
extern gfx_selr_wave01_dymmyL[]	;
extern gfx_selr_wave01_dymmyR[]	;
extern Gfx gfx_sr_mipmap[]		;
extern Gfx gfx_auto_door[]		;

extern Gfx gfx_sel2_a[]			;		//		Select room 2F.
extern Gfx gfx_sel2_b[]			;
extern Gfx gfx_sel2_b_s[]		;
extern Gfx gfx_sel2_b_a[]		;
extern Gfx gfx_sel2_c[]			;
extern Gfx gfx_sel2_c_k[]		;
extern Gfx gfx_sel2_c_s[]		;
extern Gfx gfx_sel2_d[]			;
extern Gfx gfx_sel2_e[]			;
extern Gfx gfx_sel2_e_a[]		;
extern Gfx gfx_sel2_e_ia[]		;
extern Gfx gfx_sel2_e_gr[]		;
extern Gfx gfx_sel2_e_furi[]	;
extern Gfx gfx_sel2_f[]			;
extern Gfx gfx_sel2_f_s[]		;
extern Gfx gfx_sel2_f_ng[]		;
extern Gfx gfx_hari_a[]			;
extern Gfx gfx_hari_b[]			;

extern Gfx gfx_selt_b[]			;		//		Select room B1.
extern Gfx gfx_selt_b_a[]		;
extern Gfx gfx_selt_b_ia[]		;
extern Gfx gfx_selt_c[]			;
extern Gfx gfx_selt_c_ia[]		;
extern Gfx gfx_selt_d[]			;
extern Gfx gfx_selt_d_a[]		;
extern Gfx gfx_selt_e[]			;
extern Gfx gfx_selt_e_ht[]		;
extern Gfx gfx_selt_e_a[]		;
extern Gfx gfx_selt_f[]			;
extern Gfx gfx_selt_sss[]		;


extern ulong SelRoomEffect(int, MapNode*, void*);


// --------------------------------------------------------------------------------------------
//
//
//		Select room 1F.
//
//
/* ===============================================================================
        : Hierarchy map data of AutoDoor.
================================================================================== */
Hierarchy RCP_HmsAutoDoor[] = {
    hmsHeader(400)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_auto_door)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Sel1fTrap.
================================================================================== */
Hierarchy RCP_HmsSel1fTrap[] = {
    hmsHeader(600)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_sel_c_trap)
    hmsEnd()
    hmsExit()
};

/* ================================================================================
        : Switch map shape 01.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap01[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel_a)
        hmsGfx(RM_SPRITE, gfx_sel_a_a)
        hmsGfx(RM_SURF	, gfx_sel_a_g)
		hmsGfx(RM_XDECAL, gfx_sel_a_s)
		hmsGfx(RM_SPRITE, gfx_sel_a_p)
		hmsCProg(0, SelRoomEffect)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 02.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap02[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF, gfx_sel_b)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 03.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap03[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF, gfx_sel_c)
		hmsGfx(RM_SURF, gfx_sr_mipmap)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 04.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap04[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_sel_d)
		hmsGfx(RM_SPRITE, gfx_sel_d_p)
		hmsGfx(RM_SPRITE, gfx_sel_d_a)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 05.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap05[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel_e)
		hmsGfx(RM_SPRITE, gfx_sel_e_a)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0100, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 06.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap06[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel_f)
        hmsGfx(RM_SPRITE, gfx_sel_f_a)
		hmsGfx(RM_SPRITE, gfx_sel_f_aa)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0102, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 07.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap07[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_sel_g)
        hmsGfx(RM_SURF  , gfx_sel_g_an)
        hmsGfx(RM_SURF  , gfx_sel_g_g)
        hmsGfx(RM_XSURF , gfx_sel_g_k)
		hmsGfx(RM_XSURF , gfx_sel_g_ia)
		hmsGfx(RM_SPRITE, gfx_sel_g_a)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0103, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 08.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap08[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel_h)
		hmsGfx(RM_SPRITE, gfx_sel_h_a)
		hmsGfx(RM_SURF, gfx_selr_wave01_dymmyL)
		hmsGfx(RM_SURF, gfx_selr_wave01_dymmyR)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0101, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 09.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap09[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel_a)
        hmsGfx(RM_SPRITE, gfx_sel_a_a)
        hmsGfx(RM_SURF	, gfx_sel_a_g)
		hmsGfx(RM_XDECAL, gfx_sel_a_s)
		hmsGfx(RM_SPRITE, gfx_sel_a_p)
		hmsCProg(0, SelRoomEffect)
        hmsGfx(RM_SURF, gfx_sel_i)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 10.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap10[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel_a)
        hmsGfx(RM_SPRITE, gfx_sel_a_a)
        hmsGfx(RM_SURF	, gfx_sel_a_g)
		hmsGfx(RM_XDECAL, gfx_sel_a_s)
		hmsGfx(RM_SPRITE, gfx_sel_a_p)
		hmsCProg(0, SelRoomEffect)
        hmsGfx(RM_SURF, gfx_sel_b)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 11.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap11[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel_a)
        hmsGfx(RM_SPRITE, gfx_sel_a_a)
        hmsGfx(RM_SURF	, gfx_sel_a_g)
		hmsGfx(RM_XDECAL, gfx_sel_a_s)
		hmsGfx(RM_SPRITE, gfx_sel_a_p)
		hmsCProg(0, SelRoomEffect)
        hmsGfx(RM_SURF, gfx_sel_c)
		hmsGfx(RM_SURF, gfx_sr_mipmap)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 12.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap12[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel_a)
        hmsGfx(RM_SPRITE, gfx_sel_a_a)
        hmsGfx(RM_SURF	, gfx_sel_a_g)
		hmsGfx(RM_XDECAL, gfx_sel_a_s)
		hmsGfx(RM_SPRITE, gfx_sel_a_p)
		hmsCProg(0, SelRoomEffect)
        hmsGfx(RM_SURF	, gfx_sel_d)
		hmsGfx(RM_SPRITE, gfx_sel_d_p)
		hmsGfx(RM_SPRITE, gfx_sel_d_a)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 13.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap13[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel_a)
        hmsGfx(RM_SPRITE, gfx_sel_a_a)
        hmsGfx(RM_SURF	, gfx_sel_a_g)
		hmsGfx(RM_XDECAL, gfx_sel_a_s)
		hmsGfx(RM_SPRITE, gfx_sel_a_p)
		hmsCProg(0, SelRoomEffect)
        hmsGfx(RM_SURF	, gfx_sel_e)
		hmsGfx(RM_SPRITE, gfx_sel_e_a)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0100, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 14.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap14[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel_a)
        hmsGfx(RM_SPRITE, gfx_sel_a_a)
        hmsGfx(RM_SURF	, gfx_sel_a_g)
		hmsGfx(RM_XDECAL, gfx_sel_a_s)
		hmsGfx(RM_SPRITE, gfx_sel_a_p)
		hmsCProg(0, SelRoomEffect)
        hmsGfx(RM_SURF	, gfx_sel_f)
        hmsGfx(RM_SPRITE, gfx_sel_f_a)
		hmsGfx(RM_SPRITE, gfx_sel_f_aa)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0102, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 15.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap15[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel_a)
        hmsGfx(RM_SPRITE, gfx_sel_a_a)
        hmsGfx(RM_SURF	, gfx_sel_a_g)
		hmsGfx(RM_XDECAL, gfx_sel_a_s)
		hmsGfx(RM_SPRITE, gfx_sel_a_p)
		hmsCProg(0, SelRoomEffect)
        hmsGfx(RM_SURF	, gfx_sel_g)
        hmsGfx(RM_SURF	, gfx_sel_g_an)
        hmsGfx(RM_SURF	, gfx_sel_g_g)
        hmsGfx(RM_XSURF	, gfx_sel_g_k)
		hmsGfx(RM_XSURF , gfx_sel_g_ia)
		hmsGfx(RM_SPRITE, gfx_sel_g_a)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0103, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 16.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap16[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel_a)
        hmsGfx(RM_SPRITE, gfx_sel_a_a)
        hmsGfx(RM_SURF	, gfx_sel_a_g)
		hmsGfx(RM_XDECAL, gfx_sel_a_s)
		hmsGfx(RM_SPRITE, gfx_sel_a_p)
		hmsCProg(0, SelRoomEffect)
        hmsGfx(RM_SURF	, gfx_sel_h)
		hmsGfx(RM_SPRITE, gfx_sel_h_a)
		hmsGfx(RM_SURF, gfx_selr_wave01_dymmyL)
		hmsGfx(RM_SURF, gfx_selr_wave01_dymmyR)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0101, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 17.
=================================================================================== */
Hierarchy RCP_Hms_selroom_switchmap17[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel_a)
        hmsGfx(RM_SPRITE, gfx_sel_a_a)
        hmsGfx(RM_SURF	, gfx_sel_a_g)
		hmsGfx(RM_XDECAL, gfx_sel_a_s)
		hmsGfx(RM_SPRITE, gfx_sel_a_p)
		hmsCProg(0, SelRoomEffect)
        hmsGfx(RM_SURF	, gfx_sel_i)
    hmsEnd()
    hmsReturn()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage6Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0, 0, 0, 1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()

			hmsPerspective(64, 50, 7000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_FIX,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsSelect(17, ControlSwitchMap)
					hmsBegin()
						hmsCall(RCP_Hms_selroom_switchmap01)
						hmsCall(RCP_Hms_selroom_switchmap02)
						hmsCall(RCP_Hms_selroom_switchmap03)
						hmsCall(RCP_Hms_selroom_switchmap04)
						hmsCall(RCP_Hms_selroom_switchmap05)
						hmsCall(RCP_Hms_selroom_switchmap06)
						hmsCall(RCP_Hms_selroom_switchmap07)
						hmsCall(RCP_Hms_selroom_switchmap08)
						hmsCall(RCP_Hms_selroom_switchmap09)
						hmsCall(RCP_Hms_selroom_switchmap10)
						hmsCall(RCP_Hms_selroom_switchmap11)
						hmsCall(RCP_Hms_selroom_switchmap12)
						hmsCall(RCP_Hms_selroom_switchmap13)
						hmsCall(RCP_Hms_selroom_switchmap14)
						hmsCall(RCP_Hms_selroom_switchmap15)
						hmsCall(RCP_Hms_selroom_switchmap16)
						hmsCall(RCP_Hms_selroom_switchmap17)
					hmsEnd()
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};


// --------------------------------------------------------------------------------------------
//
//
//		Select room 2F.
//
//
/* ===============================================================================
        : Hierarchy map data of Sel2Furi.
================================================================================== */
Hierarchy RCP_HmsSel2Furi[] = {
    hmsHeader(600)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_sel2_e_furi)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of HmsSel2fHariA.
================================================================================== */
Hierarchy RCP_HmsHmsSel2fHariA[] = {
    hmsHeader(300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_hari_a)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of HmsSel2fHariB.
================================================================================== */
Hierarchy RCP_HmsHmsSel2fHariB[] = {
    hmsHeader(300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_hari_b)
    hmsEnd()
    hmsExit()
};

/* ================================================================================
        : Switch map shape 01.
=================================================================================== */
Hierarchy RCP_Hms_selroom2f_switchmap01[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF, gfx_sel2_a)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 02.
=================================================================================== */
Hierarchy RCP_Hms_selroom2f_switchmap02[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF, gfx_sel2_b)
		hmsGfx(RM_SURF, gfx_sel2_b_s)
		hmsGfx(RM_SPRITE, gfx_sel2_b_a)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0108, WaveMove)
		hmsCProg(0x010a, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 03.
=================================================================================== */
Hierarchy RCP_Hms_selroom2f_switchmap03[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF , gfx_sel2_c)
        hmsGfx(RM_XSURF, gfx_sel2_c_k)
		hmsGfx(RM_SURF, gfx_sel2_c_s)
		hmsCProg(0, WaveInit)
		hmsCProg(0x010c, WaveMove)
		hmsCProg(0, CtrlMirrorMario)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 04.
=================================================================================== */
Hierarchy RCP_Hms_selroom2f_switchmap04[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF, gfx_sel2_d)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0109, WaveMove)
		hmsCProg(0x010d, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 05.
=================================================================================== */
Hierarchy RCP_Hms_selroom2f_switchmap05[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel2_e)
        hmsGfx(RM_SPRITE, gfx_sel2_e_a)
		hmsGfx(RM_XSURF , gfx_sel2_e_ia)
		hmsGfx(RM_XSURF , gfx_sel2_e_gr)
		hmsCProg(0, WaveInit)
		hmsCProg(0x010b, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 06.
=================================================================================== */
Hierarchy RCP_Hms_selroom2f_switchmap06[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF, gfx_sel2_f)
		hmsGfx(RM_SURF, gfx_sel2_f_s)
		hmsGfx(RM_SURF, gfx_sel2_f_ng)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 07.
=================================================================================== */
Hierarchy RCP_Hms_selroom2f_switchmap07[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF, gfx_sel2_a)
        hmsGfx(RM_SURF, gfx_sel2_b)
		hmsGfx(RM_SURF, gfx_sel2_b_s)
		hmsGfx(RM_SPRITE, gfx_sel2_b_a)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0108, WaveMove)
		hmsCProg(0x010a, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 08.
=================================================================================== */
Hierarchy RCP_Hms_selroom2f_switchmap08[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF , gfx_sel2_b)
		hmsGfx(RM_SURF , gfx_sel2_b_s)
		hmsGfx(RM_SPRITE, gfx_sel2_b_a)
        hmsGfx(RM_SURF , gfx_sel2_c)
        hmsGfx(RM_XSURF, gfx_sel2_c_k)
		hmsGfx(RM_SURF, gfx_sel2_c_s)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0108, WaveMove)
		hmsCProg(0x010a, WaveMove)
		hmsCProg(0x010c, WaveMove)
		hmsCProg(0, CtrlMirrorMario)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 09.
=================================================================================== */
Hierarchy RCP_Hms_selroom2f_switchmap09[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF, gfx_sel2_b)
		hmsGfx(RM_SURF, gfx_sel2_b_s)
		hmsGfx(RM_SPRITE, gfx_sel2_b_a)
        hmsGfx(RM_SURF, gfx_sel2_d)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0108, WaveMove)
		hmsCProg(0x0109, WaveMove)
		hmsCProg(0x010a, WaveMove)
		hmsCProg(0x010d, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 10.
=================================================================================== */
Hierarchy RCP_Hms_selroom2f_switchmap10[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel2_b)
		hmsGfx(RM_SURF  , gfx_sel2_b_s)
		hmsGfx(RM_SPRITE, gfx_sel2_b_a)
        hmsGfx(RM_SURF	, gfx_sel2_e)
        hmsGfx(RM_SPRITE, gfx_sel2_e_a)
		hmsGfx(RM_XSURF , gfx_sel2_e_ia)
		hmsGfx(RM_XSURF , gfx_sel2_e_gr)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0108, WaveMove)
		hmsCProg(0x010a, WaveMove)
		hmsCProg(0x010b, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 11.
=================================================================================== */
Hierarchy RCP_Hms_selroom2f_switchmap11[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_sel2_e)
        hmsGfx(RM_SPRITE, gfx_sel2_e_a)
		hmsGfx(RM_XSURF , gfx_sel2_e_ia)
		hmsGfx(RM_XSURF , gfx_sel2_e_gr)
        hmsGfx(RM_SURF	, gfx_sel2_f)
		hmsGfx(RM_SURF  , gfx_sel2_f_s)
		hmsGfx(RM_SURF  , gfx_sel2_f_ng)
		hmsCProg(0, WaveInit)
		hmsCProg(0x010b, WaveMove)
    hmsEnd()
    hmsReturn()
};

/********************************************************************************/
/*	Hierarchy map scene 2.														*/
/********************************************************************************/
Hierarchy RCP_Stage6Scene2[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0, 0, 0, 1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()

			hmsPerspective(64, 50, 8000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsSelect(11, ControlSwitchMap)
					hmsBegin()
						hmsCall(RCP_Hms_selroom2f_switchmap01)
						hmsCall(RCP_Hms_selroom2f_switchmap02)
						hmsCall(RCP_Hms_selroom2f_switchmap03)
						hmsCall(RCP_Hms_selroom2f_switchmap04)
						hmsCall(RCP_Hms_selroom2f_switchmap05)
						hmsCall(RCP_Hms_selroom2f_switchmap06)
						hmsCall(RCP_Hms_selroom2f_switchmap07)
						hmsCall(RCP_Hms_selroom2f_switchmap08)
						hmsCall(RCP_Hms_selroom2f_switchmap09)
						hmsCall(RCP_Hms_selroom2f_switchmap10)
						hmsCall(RCP_Hms_selroom2f_switchmap11)
					hmsEnd()
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};


// --------------------------------------------------------------------------------------------
//
//
//		Select room B1.
//
//
/* ===============================================================================
        : Hierarchy map data of Sel1BSwitch.
================================================================================== */
Hierarchy RCP_HmsSel1BSwitch[] = {
    hmsHeader(550)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_selt_sss)
    hmsEnd()
    hmsExit()
};

/* ================================================================================
        : Switch map shape 01.
=================================================================================== */
Hierarchy RCP_Hms_selroom1b_switchmap01[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_selt_b)
        hmsGfx(RM_SPRITE, gfx_selt_b_a)
		hmsGfx(RM_XSURF , gfx_selt_b_ia)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 02.
=================================================================================== */
Hierarchy RCP_Hms_selroom1b_switchmap02[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF , gfx_selt_c)
		hmsGfx(RM_XSURF, gfx_selt_c_ia)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0104, WaveMove)
		hmsCProg(0x0105, WaveMove)
		hmsCProg(0, WaterInit)
		hmsCProg(0x0600, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 03.
=================================================================================== */
Hierarchy RCP_Hms_selroom1b_switchmap03[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_selt_d)
        hmsGfx(RM_SPRITE, gfx_selt_d_a)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0106, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 04.
=================================================================================== */
Hierarchy RCP_Hms_selroom1b_switchmap04[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF , gfx_selt_e)
		hmsGfx(RM_XSURF, gfx_selt_e_ht)
		hmsGfx(RM_SPRITE, gfx_selt_e_a)
		hmsCProg(0, WaterInit)
		hmsCProg(0x0612, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 05.
=================================================================================== */
Hierarchy RCP_Hms_selroom1b_switchmap05[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF, gfx_selt_f)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0107, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 06.
=================================================================================== */
Hierarchy RCP_Hms_selroom1b_switchmap06[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_selt_b)
        hmsGfx(RM_SPRITE, gfx_selt_b_a)
		hmsGfx(RM_XSURF , gfx_selt_b_ia)
        hmsGfx(RM_SURF	, gfx_selt_c)
		hmsGfx(RM_XSURF , gfx_selt_c_ia)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0104, WaveMove)
		hmsCProg(0x0105, WaveMove)
		hmsCProg(0, WaterInit)
		hmsCProg(0x0600, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 07.
=================================================================================== */
Hierarchy RCP_Hms_selroom1b_switchmap07[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_selt_b)
        hmsGfx(RM_SPRITE, gfx_selt_b_a)
		hmsGfx(RM_XSURF , gfx_selt_b_ia)
        hmsGfx(RM_SURF	, gfx_selt_f)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0107, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 08.
=================================================================================== */
Hierarchy RCP_Hms_selroom1b_switchmap08[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_selt_b)
        hmsGfx(RM_SPRITE, gfx_selt_b_a)
		hmsGfx(RM_XSURF , gfx_selt_b_ia)
        hmsGfx(RM_SURF	, gfx_selt_e)
		hmsGfx(RM_XSURF , gfx_selt_e_ht)
		hmsGfx(RM_SPRITE, gfx_selt_e_a)
		hmsCProg(0, WaterInit)
		hmsCProg(0x0612, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 09.
=================================================================================== */
Hierarchy RCP_Hms_selroom1b_switchmap09[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF , gfx_selt_c)
		hmsGfx(RM_XSURF, gfx_selt_c_ia)
        hmsGfx(RM_SURF , gfx_selt_e)
		hmsGfx(RM_XSURF, gfx_selt_e_ht)
		hmsGfx(RM_SPRITE, gfx_selt_e_a)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0104, WaveMove)
		hmsCProg(0x0105, WaveMove)
		hmsCProg(0, WaterInit)
		hmsCProg(0x0600, WaterDraw)
		hmsCProg(0x0612, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 10.
=================================================================================== */
Hierarchy RCP_Hms_selroom1b_switchmap10[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_selt_c)
		hmsGfx(RM_XSURF , gfx_selt_c_ia)
        hmsGfx(RM_SURF	, gfx_selt_d)
        hmsGfx(RM_SPRITE, gfx_selt_d_a)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0104, WaveMove)
		hmsCProg(0x0105, WaveMove)
		hmsCProg(0x0106, WaveMove)
		hmsCProg(0, WaterInit)
		hmsCProg(0x0600, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/********************************************************************************/
/*	Hierarchy map scene 3.														*/
/********************************************************************************/
Hierarchy RCP_Stage6Scene3[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0, 0, 0, 1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(64, 50, 6400, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsSelect(10, ControlSwitchMap)
					hmsBegin()
						hmsCall(RCP_Hms_selroom1b_switchmap01)
						hmsCall(RCP_Hms_selroom1b_switchmap02)
						hmsCall(RCP_Hms_selroom1b_switchmap03)
						hmsCall(RCP_Hms_selroom1b_switchmap04)
						hmsCall(RCP_Hms_selroom1b_switchmap05)
						hmsCall(RCP_Hms_selroom1b_switchmap06)
						hmsCall(RCP_Hms_selroom1b_switchmap07)
						hmsCall(RCP_Hms_selroom1b_switchmap08)
						hmsCall(RCP_Hms_selroom1b_switchmap09)
						hmsCall(RCP_Hms_selroom1b_switchmap10)
					hmsEnd()
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
