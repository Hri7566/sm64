/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: pathchiri.s
					Description	: 
					Date		: 1995.
					Author		: H.yajima

 ********************************************************************************/


/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************

	EFFECT OBJECT	<< chiri >>

 ********************************************************************************/

e_chiri:
	p_initialize(effect)
	p_setbit(flag,stf_moveON )
	p_softspritemodeON
	p_while
		p_program(s_chiri)
	p_loop

/*################*/
#else
/*################*/

#define			chiri_scale	(execstp->s[stw_work0].f)
#define			chiri_scale_timer	(execstp->s[stw_work1].d)
#define			chiri_scale_mode	(execstp->s[stw_work2].d)

/********************************************************************************

	EFFECT OBJECT	<< chiri >>

 ********************************************************************************/
extern void s_chiri(void)
{

	float	scale;

	if ( obj_timer == 0 ){
		s_calcobj_speedF();
		chiri_scale = execstp->map.scale[0];
		switch(obj_programselect){
		 	case	EFFECT_BURN:
				obj_alpha 		  = 254;
				chiri_scale_timer = -21;
				chiri_scale_mode  = 0;
				break;
		 	case	EFFECT_MOWAN:
				obj_alpha 		  = 254;
				chiri_scale_timer = -13;
				chiri_scale_mode  = 1;
				break;
		}
	}

	s_optionmove_XZ();
	s_calc_friction(obj_air_fric);
	if ( obj_speedY > 100 ) obj_speedY = 100;
	if ( obj_timer  > 20 ) s_remove_obj(execstp);

	if ( obj_alpha != 0 ){
		obj_alpha += chiri_scale_timer;
		if ( obj_alpha <= 1 ) s_remove_obj(execstp);
		if ( chiri_scale_mode )	scale = chiri_scale * ( (254 - obj_alpha) / 254.0 );
		else					scale = chiri_scale * (        obj_alpha  / 254.0 );	
		s_set_scale(scale);
	}

}


/********************************************************************************

		bomb effect

 ********************************************************************************/

static EffectRecord burneffect = {
	EFFECT_BURN,
	20,				/* effect count 	*/
	S_hiteffect,	/* effect character */
//	S_blackeffect,	/* effect character */
	0,				/* ================ */			
	40,5,			/* speed F     		*/
	30,20,			/* speed Y     		*/
	-4,				/* gravity	   		*/
	30,				/* air fric	   		*/		
	330.0f,			/* ================ */
	10.0f			/* ================ */
};

extern void s_burneffect(int flag,int offsetY,float scale)
{

	burneffect.scale_min	 	= scale;
	burneffect.scale_random 	= scale / 20.0;
	burneffect.offsetY 			= offsetY;
	if ( flag == 0 )		burneffect.total  =  20;
	else if	(flag > 20 )	burneffect.total  =  flag;
	else					burneffect.total  =  4;	
	s_makeeffect_chiri(&burneffect);

}



/*################*/
#endif
/*################*/
/*===============================================================================
		end end end end end end end end 
===============================================================================*/



