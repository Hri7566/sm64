/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage13 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx gfx_bs1[] ;
extern Gfx gfx_bs2[] ;
extern Gfx gfx_bs3[] ;
extern Gfx gfx_bs4[] ;
extern Gfx gfx_bs5b[];
extern Gfx gfx_bs5s[];

extern Gfx gfx_bs_naka1[];
extern Gfx gfx_bs_naka2[];
extern Gfx gfx_bs_naka3[];
extern Gfx gfx_bs_naka4[];

extern Gfx gfx_bs_futa[];
extern Gfx gfx_bs_sima[];

/* ===============================================================================
        : Hierarchy map data of BSfutaBig.
================================================================================== */
Hierarchy RCP_HmsBSfutaBig[] = {
    hmsHeader(300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_bs_futa)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of BSfutaSmall.
================================================================================== */
Hierarchy RCP_HmsBSfutaSmall[] = {
    hmsHeader(300)
    hmsBegin()
		hmsScale(0.3f)
		hmsBegin()
	        hmsGfx(RM_SURF, gfx_bs_futa)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of BSSima.
================================================================================== */
Hierarchy RCP_HmsBSSima[] = {
    hmsHeader(1200)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_bs_sima)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage13Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(0, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, 20000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_FIELD,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF	, gfx_bs1)
					hmsGfx(RM_SURF	, gfx_bs2)
					hmsGfx(RM_SPRITE, gfx_bs3)
					hmsGfx(RM_XSURF	, gfx_bs4)
					hmsGfx(RM_XDECAL, gfx_bs5b)
					hmsCProg(0,		 WaterInit)
					hmsCProg(0x1301, WaterDraw)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

		hmsLayer(0)
		hmsBegin()
			hmsCProg(0, CannonEdge)
		hmsEnd()

	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 2.														*/
/********************************************************************************/
Hierarchy RCP_Stage13Scene2[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(0, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_FIELD,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsScale(0.3f)
					hmsBegin()
						hmsGfx(RM_SURF	, gfx_bs1)
						hmsGfx(RM_SURF	, gfx_bs2)
						hmsGfx(RM_SPRITE, gfx_bs3)
						hmsGfx(RM_XSURF	, gfx_bs4)
					hmsEnd()
					hmsGfx(RM_XDECAL, gfx_bs5s)
					hmsCProg(0	   , WaterInit)
					hmsCProg(0x1302, WaterDraw)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 3.														*/
/********************************************************************************/
Hierarchy RCP_Stage13Scene3[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0,0,0,1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON_O,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_XSURF	, gfx_bs_naka1)
					hmsGfx(RM_SURF	, gfx_bs_naka2)
					hmsGfx(RM_SURF	, gfx_bs_naka3)
					hmsGfx(RM_SPRITE, gfx_bs_naka4)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
