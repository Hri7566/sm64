/********************************************************************************
	callback.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	February 20, 1996
 ********************************************************************************/

#ifndef CALLBACK_H
#define	CALLBACK_H


/********************************************************************************/
/*	Player shape control record													*/
/********************************************************************************/

typedef struct {
	ulong		status;					/* player's status						*/
	char		ctrlCap;				/* player's cap control					*/
	char		ctrlEye;				/* player's eye control					*/
	char		ctrlHand;				/* player's hand control				*/
	char		ctrlWing;				/* player's wing control				*/
	short		ctrlSkin;				/* player's skin control				*/
	char		ctrlSucker;				/* player's sucker control				*/
	uchar		handscale;				/* player's hand scale					*/
	SVector		waistang;				/* player's waist angle					*/
	SVector		headang;				/* player's head angle					*/
	FVector		handpos;				/* player's hand position				*/
	StrategyPtr	taking;					/* pointer to the taken object			*/
} PLShapeCtrl;

extern PLShapeCtrl	playerShapeCtrl[2];


/********************************************************************************/
/*	Stage effects control call back routines									*/
/********************************************************************************/

extern ulong
WeatherProc(int code, MapNode *node, void *data);

extern ulong
DrawBackGround(int code, MapNode *node, void *data);

extern ulong
WaterFilter(int code, MapNode *node, void *data);

extern ulong
DoMarioFaceAnime(int code, MapNode *node, void *data);

extern ulong
SwitchArea(int code, MapNode *node, void *data);

extern void
ExecDecorKinopio(void);

extern void
InitDecorKinopio(void);

extern void
InitStarDoorEffect(void);

extern void
ExecStarDoorEffect(void);

/********************************************************************************/
/*	Mario and Luigi shape control call back routines							*/
/********************************************************************************/

extern ulong
CtrlMarioAlpha(int code, MapNode *node, void *data);

extern ulong
CtrlMarioLOD(int code, MapNode *node, void *data);

extern ulong
CtrlMarioEye(int code, MapNode *node, void *data);

extern ulong
CtrlMarioWaist(int code, MapNode *node, void *data);

extern ulong
CtrlMarioHead(int code, MapNode *node, void *data);

extern ulong
CtrlMarioHand(int code, MapNode *node, void *data);

extern ulong
CtrlHandScale(int code, MapNode *node, void *data);

extern ulong
CtrlMarioSkin(int code, MapNode *node, void *data);

extern ulong
CtrlMarioCap(int code, MapNode *node, void *data);

extern ulong
CtrlMarioWing(int code, MapNode *node, void *data);

extern ulong
CtrlMarioTaking(int code, MapNode *node, void *data);

extern ulong
CtrlMirrorMario(int code, MapNode *node, void *data);

extern ulong
CtrlMarioGeoMode(int code, MapNode *node, void *data);

#endif
