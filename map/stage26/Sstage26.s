/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 26 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage26


/* ================================================================================
		: BG parts sequence.
===================================================================================	*/
SEQ_Stage26_BGParts:

		seqActor(S_NULL,      0,200,-1652,  0,0,0,  0,0,0,  e_soyokaze)
		seqActor(S_NULL,  -2700,  0,-1652,  0,0,0,  0,0,0,  e_birdsing)
		seqActor(S_NULL,   2700,  0,-1652,  0,0,0,  0,1,0,  e_birdsing)

		seqReturn


/* ====================================================================================
		: Enemies sequence.
=======================================================================================	*/
SEQ_Stage26_Enemies:

		seqActor(S_teresa,  -3217, 100, -101,  0,0,0,  0,0,0,   e_obake_fly	 )
		seqActor(S_teresa,   3317, 100,-1701,  0,0,0,  0,0,0,	e_obake_fly	 )
		seqActor(S_teresa,    -71,   1,-1387,  0,0,0,  0,0,0,	e_obake_fly	 )

		seqReturn


/* ====================================================================================
		: Stage 26 main sequence.
=======================================================================================	*/
SEQ_DoStage26:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage26SegmentRomStart		, _GfxStage26SegmentRomEnd		)
	seqLoadPres(SEGMENT_BACK  	  , _BackMainmapSegmentRomStart		, _BackMainmapSegmentRomEnd		)
	seqLoadText(SEGMENT_TEXTURE	  , _KCastleInTextureSegmentRomStart, _KCastleInTextureSegmentRomEnd)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_iSegmentRomStart		, _GfxEnemy1_iSegmentRomEnd		)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_iSegmentRomStart		, _HmsEnemy1_iSegmentRomEnd		)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_i)

		seqHmsShape(S_tree2	   , RCP_HmsTree02		)
		seqHmsShape(S_tripdoor1, RCP_HmsDoor1 		)
		seqHmsShape(S_bg01	   , RCP_HmsGardenTower	)

		seqBeginScene(1, RCP_Stage26Scene1)
			seqActor(S_teresa,	-2360,-100,-2712,  0,  0,0,  1, 5,0,  e_boss_teresa		)
			seqActor(S_NULL, 		0,  51,-1000,  0,180,0,  0,10,0,  e_player_pushout 	)
			seqActor(S_NULL, 		0,  51,-1000,  0,180,0,  0,11,0,  e_player_pushdown	)
			seqPort( 5,  4, 1, 10)		/*	To St4 [ Teresa ].			*/
			seqPort(10, 26, 1, 10)		/*	St4    [ Teresa ] clear.	*/
			seqPort(11, 26, 1, 11)		/*			"		  missed.	*/
			seqPort(1, 6, 1, 2)			/*	Uraniwa door.				*/
			seqGameOver(16, 1, 3)
			seqCall(SEQ_Stage26_BGParts)
			seqCall(SEQ_Stage26_Enemies)
			seqMapInfo(cx2601_info)
			seqTagInfo(cx2601_info_tag)
			seqSetMusic(NA_STG_GROUND, 0)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 0,  -14,0,-201)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
