/********************************************************************************
						Ultra 64 MARIO Brothers

					 mario waiting animation data 1

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							July 19, 1995
 ********************************************************************************/

#include "../headers.h"
#include "../include/anime.h"


static short mario_wait1_prm[]={
	    0,  170,  170,  169,  168,  166,  165,  163,
	  161,  159,  158,  156,  155,  154,  153,  153,
	  153,  154,  155,  156,  158,  160,  162,  163,
	  165,  167,  168,  170,  171,  171,  172,    0,
	    0,    0,    0,    0,    1,    1,    2,    2,
	    2,    3,    3,    3,    3,    3,    3,    3,
	    3,    2,    2,    1,    1,    0,    0,    0,
	    0,   -1,   -1,   -1,   -1,16383,    0,    3,
	   13,   29,   49,   73,   99,  125,  152,  178,
	  201,  221,  237,  247,  251,  248,  239,  225,
	  207,  186,  162,  138,  113,   88,   65,   44,
	   26,   12,    3,    0,-1668,-1668,-1667,-1666,
	-1665,-1663,-1661,-1659,-1657,-1655,-1653,-1652,
	-1650,-1650,-1649,-1650,-1650,-1651,-1653,-1654,
	-1656,-1658,-1660,-1662,-1663,-1665,-1666,-1667,
	-1668,-1668,-4724,-4748,-4812,-4912,-5039,-5186,
	-5348,-5517,-5685,-5847,-5995,-6122,-6221,-6286,
	-6309,-6289,-6232,-6144,-6031,-5898,-5751,-5596,
	-5438,-5282,-5135,-5002,-4889,-4801,-4745,-4724,
	-13745,-13763,-13814,-13893,-13993,-14110,-14237,-14371,
	-14504,-14631,-14748,-14849,-14927,-14978,-14996,-14980,
	-14936,-14866,-14777,-14672,-14556,-14433,-14308,-14185,
	-14069,-13964,-13875,-13806,-13761,-13745,-1251,-1296,
	-1422,-1616,-1864,-2152,-2467,-2796,-3125,-3440,
	-3729,-3976,-4170,-4296,-4341,-4302,-4191,-4020,
	-3799,-3540,-3254,-2950,-2642,-2339,-2052,-1793,
	-1572,-1401,-1290,-1251,-11556,-11509,-11379,-11179,
	-10923,-10626,-10300,-9960,-9621,-9295,-8997,-8741,
	-8541,-8411,-8364,-8405,-8520,-8696,-8924,-9192,
	-9488,-9801,-10120,-10433,-10729,-10996,-11224,-11401,
	-11515,-11556,-11084,-11022,-10847,-10577,-10233,-9833,
	-9394,-8938,-8481,-8043,-7642,-7298,-7029,-6853,
	-6791,-6845,-6999,-7237,-7544,-7904,-8302,-8723,
	-9152,-9573,-9971,-10331,-10638,-10876,-11030,-11084,
	-2743,-2794,-2939,-3161,-3446,-3777,-4138,-4516,
	-4893,-5255,-5586,-5870,-6093,-6237,-6289,-6244,
	-6117,-5920,-5667,-5370,-5041,-4693,-4339,-3991,
	-3662,-3365,-3112,-2915,-2788,-2743,-14371,-14366,
	-14353,-14334,-14309,-14279,-14247,-14214,-14181,-14149,
	-14120,-14095,-14075,-14062,-14058,-14062,-14073,-14090,
	-14113,-14139,-14168,-14198,-14230,-14260,-14289,-14316,
	-14338,-14355,-14367,-14371, 1355, 1407, 1553, 1777,
	 2064, 2397, 2762, 3142, 3522, 3886, 4220, 4506,
	 4730, 4876, 4928, 4883, 4754, 4556, 4301, 4002,
	 3670, 3320, 2963, 2613, 2282, 1982, 1727, 1529,
	 1401, 1355,11222,11200,11136,11039,10915,10771,
	10613,10448,10283,10125, 9980, 9856, 9759, 9696,
	 9673, 9693, 9749, 9834, 9945,10075,10219,10370,
	10525,10677,10821,10951,11061,11147,11203,11222,
	-13066,-12992,-12785,-12467,-12060,-11586,-11068,-10527,
	-9987,-9469,-8995,-8588,-8270,-8063,-7989,-8053,
	-8235,-8517,-8879,-9305,-9776,-10274,-10781,-11279,
	-11750,-12176,-12538,-12819,-13001,-13066,    0,    6,
	   25,   54,   92,  135,  183,  232,  281,  329,
	  372,  410,  439,  458,  465,  459,  442,  416,
	  383,  344,  301,  255,  209,  163,  120,   81,
	   48,   22,    5,    0, -208, -206, -201, -192,
	 -181, -168, -154, -140, -125, -112,  -99,  -88,
	  -79,  -74,  -72,  -74,  -78,  -86,  -96, -107,
	 -120, -133, -147, -160, -173, -184, -194, -201,
	 -206, -208,-13864,-13913,-14049,-14258,-14525,-14836,
	-15177,-15531,-15886,-16227,-16538,-16805,-17014,-17150,
	-17199,-17156,-17037,-16852,-16614,-16334,-16025,-15698,
	-15365,-15038,-14729,-14449,-14211,-14026,-13906,-13864,
	 7754, 7836, 8066, 8419, 8870, 9396, 9970,10570,
	11169,11744,12269,12721,13074,13304,13386,13314,
	13112,12800,12398,11926,11403,10851,10288, 9736,
	 9214, 8741, 8339, 8027, 7825, 7754, 6921, 6904,
	 6856, 6783, 6690, 6581, 6463, 6339, 6215, 6096,
	 5988, 5894, 5821, 5774, 5757, 5772, 5813, 5878,
	 5961, 6059, 6167, 6281, 6397, 6511, 6619, 6717,
	 6800, 6864, 6906, 6921, 4420, 4442, 4503, 4597,
	 4717, 4857, 5009, 5169, 5328, 5481, 5620, 5740,
	 5834, 5895, 5917, 5898, 5844, 5761, 5654, 5529,
	 5390, 5243, 5094, 4947, 4808, 4683, 4576, 4493,
	 4439, 4420,-21436,-21472,-21571,-21723,-21918,-22145,
	-22393,-22652,-22911,-23159,-23386,-23581,-23734,-23833,
	-23869,-23838,-23750,-23616,-23442,-23238,-23012,-22774,
	-22531,-22292,-22067,-21863,-21689,-21554,-21467,-21436,
	    0,   -5,  -22,  -47,  -80, -117, -159, -202,
	 -245, -286, -324, -356, -381, -398, -404, -399,
	 -384, -362, -333, -299, -261, -222, -181, -142,
	 -104,  -70,  -42,  -19,   -5,    0,    0,   -1,
	   -5,  -12,  -20,  -30,  -40,  -51,  -62,  -73,
	  -82,  -91,  -97, -101, -103, -101,  -98,  -92,
	  -85,  -76,  -66,  -56,  -46,  -36,  -26,  -18,
	  -10,   -5,   -1,    0,-16576,-16614,-16720,-16883,
	-17092,-17335,-17601,-17878,-18156,-18422,-18665,-18874,
	-19037,-19143,-19181,-19148,-19055,-18910,-18724,-18506,
	-18264,-18009,-17748,-17493,-17251,-17033,-16847,-16702,
	-16609,-16576, 9527, 9594, 9781,10069,10436,10865,
	11333,11821,12310,12778,13206,13574,13862,14049,
	14116,14057,13893,13639,13311,12926,12500,12050,
	11592,11142,10716,10332,10004, 9750, 9585, 9527,
	-10678,-10666,-10634,-10585,-10523,-10450,-10370,-10287,
	-10204,-10124,-10051,-9988,-9939,-9907,-9896,-9906,
	-9934,-9977,-10033,-10099,-10171,-10248,-10326,-10403,
	-10475,-10541,-10596,-10640,-10668,-10678,-5055,-5082,
	-5157,-5271,-5418,-5589,-5775,-5970,-6165,-6352,
	-6522,-6669,-6784,-6859,-6885,-6862,-6796,-6695,
	-6564,-6411,-6241,-6062,-5879,-5699,-5530,-5376,
	-5246,-5144,-5079,-5055,-19042,-19063,-19121,-19211,
	-19327,-19461,-19607,-19760,-19913,-20059,-20193,-20308,
	-20398,-20457,-20478,-20460,-20408,-20329,-20226,-20106,
	-19972,-19832,-19688,-19547,-19414,-19294,-19191,-19112,
	-19060,-19042,32767,32767,16753,-16545,32767,32767,
	16753,-16545,    0,   52,  202,  437,  746, 1115,
	 1534, 1990, 2471, 2966, 3461, 3946, 4408, 4835,
	 5215, 5608, 6070, 6583, 7129, 7691, 8251, 8790,
	 9291, 9736,10106,10385,10553,10594,10489,10221,
	    0,    0,    0,    0,    0,    0,    0,    0,
	    0,    0,    0,    0,    0,    0,    0,    1,
	    6,   13,   21,   31,   40,   48,   55,   60,
	   62,   60,   53,   42,   24,    0, -926, -941,
	 -981,-1042,-1119,-1207,-1302,-1397,-1489,-1573,
	-1643,-1695,-1724,-1725,-1692,-1617,-1497,-1341,
	-1156, -950, -731, -506, -285,  -73,  119,  285,
	  418,  509,  550,  533, 2180,16545,16551,16568,
	16594,16628,16667,16709,16754,16798,16841,16880,
	16913,16939,16956,16962,16957,16942,16919,16889,
	16854,16815,16774,16733,16692,16653,16618,16588,
	16565,16550,16545
};
static short mario_wait1_tbl[]={
	    1,    0,	/* chn14_3translate x */
	   30,    1,	/* chn14_3 translate y */
	   30,   31,	/* chn14_3 translate z */
	    1,    0,	/* chn14_3 rotate x */
	    1,   61,	/* chn14_3 rotate y */
	    1,    0,	/* chn14_3 rotate z */
	    1,    0,	/* <m_waist>1_3 rotate x */
	    1,    0,	/* <m_waist>1_3 rotate y */
	   30,  941,	/* <m_waist>1_3 rotate z */
	    1,    0,	/* <m_body>1 rotate x */
	    1,    0,	/* <m_body>1 rotate y */
	    1,  940,	/* <m_body>1 rotate z */
	   30,  850,	/* <m_head>2 rotate x */
	   30,  880,	/* <m_head>2 rotate y */
	   30,  910,	/* <m_head>2 rotate z */

	    1,  846,	/* chn6 rotate x */
	    1,  847,	/* chn6 rotate y */
	    1,  848,	/* chn6 rotate z */
	   30,  182,	/* <m_larmA>1 rotate x */
	   30,  212,	/* <m_larmA>1 rotate y */
	   30,  242,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	   30,  152,	/* <m_larmB>1 rotate z */
	   30,   62,	/* <m_lhand>1 rotate x */
	   30,   92,	/* <m_lhand>1 rotate y */
	   30,  122,	/* <m_lhand>1 rotate z */

	    1,  842,	/* chn10 rotate x */
	    1,  843,	/* chn10 rotate y */
	    1,  844,	/* chn10 rotate z */
	   30,  332,	/* <m_rarmA>1 rotate x */
	   30,  362,	/* <m_rarmA>1 rotate y */
	   30,  392,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	   30,  302,	/* <m_rarmB>1 rotate z */
	    1,    0,	/* <m_rhand>1 rotate x */
	    1,    0,	/* <m_rhand>1 rotate y */
	   30,  272,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  845,	/* chn15 rotate z */
	   30,  752,	/* <m_llegA>1 rotate x */
	   30,  782,	/* <m_llegA>1 rotate y */
	   30,  812,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	   30,  722,	/* <m_llegB>1 rotate z */
	   30,  632,	/* <m_ltoot>1 rotate x */
	   30,  662,	/* <m_ltoot>1 rotate y */
	   30,  692,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  849,	/* chn17 rotate z */
	   30,  542,	/* <m_rlegA>1 rotate x */
	   30,  572,	/* <m_rlegA>1 rotate y */
	   30,  602,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	   30,  512,	/* <m_rlegB>1 rotate z */
	   30,  422,	/* <m_rfoot>1 rotate x */
	   30,  452,	/* <m_rfoot>1 rotate y */
	   30,  482,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioWaiting1 = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	30,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_wait1_prm,
	mario_wait1_tbl
};
