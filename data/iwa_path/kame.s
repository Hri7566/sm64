/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Strategy

				Feburary 31 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
				Kame (nokonoko) Path Data
 ********************************************************************************/
e_new_kame:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON | stf_catchON )
	p_setd(objname,OBJNAME_TAKE)
	p_BGcheckYset
	p_sethitbox(40,50)
	p_program(s_new_kame_init)

	p_while
		p_hitON
		p_program(s_new_kame_main)
	p_loop



/********************************************************************************
				Kame (nokonoko) Path Data (Old Version)
 ********************************************************************************/

e_kame2:
	p_initialize(option)
	p_end
/*-----------------------------------------*/
emode_kame_wait:
	p_initialize(option)
	p_end
/*-----------------------------------------*/
emode_kame_catch:
	p_initialize(option)
 	p_end
/*-----------------------------------------*/
emode_kame_drop:
	p_initialize(option)
	p_end
/*-----------------------------------------*/
emode_kame_throw:
	p_initialize(option)
	p_end
emode_kame_next:
	p_initialize(option)
	p_end


/*################*/
#else
/*################*/


/*=================================================================================================
						New Kame Move Event
===================================================================================================*/
/***************************************************************************************************
						New Kame Init
***************************************************************************************************/
extern void s_new_kame_init()
{
	execstp->s[stw_gravity].f   = 2.5;
//	execstp->s[stw_friction].f  = 0.9999;
	execstp->s[stw_friction].f  = 0.8;
	execstp->s[stw_specificG].f = 1.3;
}

/***************************************************************************************************
						Kame Drop
***************************************************************************************************/
static void KameDrop(void)
{

	s_shapeON();
	s_mode_drop();	

	execstp->s[stw_actionmode].d = CHILEDMODE_NO_CATCH;

	execstp->s[stw_speedF].f = 0.;
	execstp->s[stw_speedY].f = 0.;
}
/***************************************************************************************************
						Kame Throw
***************************************************************************************************/
static void KameThrow(void)
{

	s_throw_object();	
	s_shapeON();
	execstp->s[stw_actionmode].d = CHILEDMODE_NO_CATCH;
	execstp->s[stw_flag].d &= (stf_YangleSAME^0xffffffff); 
	
	execstp->s[stw_speedF].f = 40.;
	execstp->s[stw_speedY].f = 20.;
}
/***************************************************************************************************
						Kame Main
***************************************************************************************************/
extern void s_new_kame_main(void)
{


	switch(execstp->s[stw_actionmode].d) {
		case CHILEDMODE_NO_CATCH:	ObjMoveEvent();						break;
		case CHILEDMODE_CATCH:		s_shapeOFF();						break;
		case CHILEDMODE_THROW:		KameThrow();						break;
		case CHILEDMODE_DROP:		KameDrop();							break;
	}





}








/*################*/
#endif
/*################*/


