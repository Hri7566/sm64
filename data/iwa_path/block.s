/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Block Strategy

					March 31 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/*==============================================================================
						Path Data
 ===============================================================================*/

/*******************************************************************************
						Take Block
********************************************************************************/
e_takeblock:
	p_initialize(plfire)
	p_setbit(flag,stf_moveON  | stf_catchON |stf_YangleSAME | stf_playerdistON )
	p_BGcheckYset
	p_save_nowpos
	p_program(s_takeblock_init)

	p_while
		p_hitON
		p_program(s_takeblock_main)
	p_loop

/*################*/
#else
/*################*/


/*==================================================================================================
		C Program
 ===================================================================================================*/
#define block_returnflag	(execstp->s[stw_work0].d)
#define block_counter		(execstp->s[stw_work2].d)


static HitCheckRecord takeblock_hit = {
	OBJNAME_TAKE,
	20,0,1,0,		/* flag,ap,hp,coin 		 */
	130+20,210+40,	/* player attack hitarea */
	130+20,210+40,	/* player attack hitarea */
};



/*==================================================================================================
		Take Block
 ===================================================================================================*/
/***************************************************************************************************
						    Init
***************************************************************************************************/
extern void s_takeblock_init(void)
{

	execstp->s[stw_gravity].f   = 2.5;
	execstp->s[stw_friction].f  = 0.99;
	execstp->s[stw_specificG].f = 1.4;

	s_set_scale(0.4);
	s_set_hitparam(execstp,&takeblock_hit);	
	obj_animecounter = 1;

	Mbitset(execstp->status,OBJECT_PLFIRE_OFF);

}
/***************************************************************************************************
						    Kemuri
***************************************************************************************************/
static void takeblock_Kemuri(void)
{
	StrategyRecord* stp = s_makeobj_nowpos(execstp,S_dust,e_iwa_smoke);

	stp->s[stw_worldX].f += (int) (Randomf()*80)-40;
	stp->s[stw_worldZ].f += (int) (Randomf()*80)-40;


}
/***************************************************************************************************
						Take Block Event
***************************************************************************************************/
static void takeblock_Move(void)
{
	short move_flag;

	move_flag = ObjMoveEvent();
	s_fire_hitcheck(execstp);

	if (move_flag == 0x01) { objsound(NA_SE2_WOODBOX_BOUND); }

	if (move_flag & OM_TOUCH && obj_speedF>20 ) {
		objsound(NA_LSE2_BOXSLIDE_S); 
		takeblock_Kemuri();
	}

	if (move_flag & OM_WALL) {
		s_kemuri();
		s_boxeffect(20,S_sankaku,0.7f,3);
		iwa_MakeCoin(execstp,3);
		obj_remove_sound(NA_SE2_BLOCK_BURST); 
		execstp->status = 0;
	}

	ObjDangerCheck(move_flag,moveobj_bgcp);
	
}
/***************************************************************************************************
						Take Block Event
***************************************************************************************************/
static void takeblock_Return(void)
{
	block_counter++;

	if (block_counter > FRAME*27) {
		if (block_counter & 0x01) MapHideShape(&execstp->map);
		else				  	  MapDispShape(&execstp->map);
	}

	if (block_counter > FRAME*30) {
    	Obj_reset(S_hibiblock_noshadow,e_takeblock,3000);
		execstp->status = 0;

	}


}
/***************************************************************************************************
						Take Block Event
***************************************************************************************************/
static void takeblock_Event(void)
{
	short move_flag;

	switch (obj_mode) {
		case 0: takeblock_Move();
				break;

		case OBJ_MELT : ObjMeltEvent();
						break;
		case OBJ_AIRDEAD : execstp->status = 0;  
						   Obj_reset(S_hibiblock_noshadow,e_takeblock,3000);
						   break;
	}

	if (block_returnflag == 1) {
		takeblock_Return();
	}

	
}
/***************************************************************************************************
						Block Drop
***************************************************************************************************/
static void takeblock_Drop(void)
{

	s_hitON();
	s_shapeON();
	s_mode_drop();	
	MapDispShape(&execstp->map);

	execstp->s[stw_actionmode].d = CHILEDMODE_NO_CATCH;
	block_returnflag = 1;
	block_counter = 0;
}
/***************************************************************************************************
						Take Block Throw
***************************************************************************************************/
static void takeblock_Throw(void)
{
	s_hitON();
	s_throw_object();	
	s_shapeON();
	MapDispShape(&execstp->map);
	execstp->s[stw_actionmode].d = CHILEDMODE_NO_CATCH;
	execstp->s[stw_flag].d &= (stf_YangleSAME^0xffffffff); 

	execstp->s[stw_speedF].f = 40.;
	execstp->s[stw_speedY].f = 20.;

	block_returnflag = 1;
	block_counter = 0;

	Mbitclr(execstp->status,OBJECT_PLFIRE_OFF);
}
/***************************************************************************************************
						Take Block Main
***************************************************************************************************/
extern void s_takeblock_main(void)
{


	switch(execstp->s[stw_actionmode].d) {
		case CHILEDMODE_NO_CATCH:	takeblock_Event();					break;
		case CHILEDMODE_CATCH:		s_shapeOFF();
									s_hitOFF();
									break;
		case CHILEDMODE_THROW:		takeblock_Throw();					break;
		case CHILEDMODE_DROP:		takeblock_Drop();					break;
	}

	obj_mail = 0;
}




/*################*/
#endif
/*################*/


