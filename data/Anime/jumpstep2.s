/********************************************************************************
						Ultra 64 MARIO Brothers

				  mario junmping step 2 animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							August 25, 1995
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioJumpStep2:
	.half	 MAP_ANIM_ONETIME 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	 6 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x02B8						/* total bytes       */


	.half	    1,    0 	/* chn14translate x */
	.half	    1,    1 	/* chn14 translate y */
	.half	    6,    2 	/* chn14 translate z */
	.half	    1,    0 	/* chn14 rotate x */
	.half	    1,    8 	/* chn14 rotate y */
	.half	    1,    0 	/* chn14 rotate z */
	.half	    1,    0 	/* <m_waist>1_3 rotate x */
	.half	    1,    0 	/* <m_waist>1_3 rotate y */
	.half	    1,  209 	/* <m_waist>1_3 rotate z */
	.half	    6,  191 	/* <m_body>1 rotate x */
	.half	    6,  197 	/* <m_body>1 rotate y */
	.half	    6,  203 	/* <m_body>1 rotate z */
	.half	    6,  173 	/* <m_head>2 rotate x */
	.half	    6,  179 	/* <m_head>2 rotate y */
	.half	    6,  185 	/* <m_head>2 rotate z */
	.half	    1,  169 	/* chn6 rotate x */
	.half	    1,  170 	/* chn6 rotate y */
	.half	    1,  171 	/* chn6 rotate z */
	.half	    6,   33 	/* <m_larmA>1 rotate x */
	.half	    6,   39 	/* <m_larmA>1 rotate y */
	.half	    6,   45 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1 rotate x */
	.half	    1,    0 	/* <m_larmB>1 rotate y */
	.half	    6,   27 	/* <m_larmB>1 rotate z */
	.half	    6,    9 	/* <m_lhand>1 rotate x */
	.half	    6,   15 	/* <m_lhand>1 rotate y */
	.half	    6,   21 	/* <m_lhand>1 rotate z */
	.half	    1,  165 	/* chn10 rotate x */
	.half	    1,  166 	/* chn10 rotate y */
	.half	    1,  167 	/* chn10 rotate z */
	.half	    6,   75 	/* <m_rarmA>1 rotate x */
	.half	    6,   81 	/* <m_rarmA>1 rotate y */
	.half	    6,   87 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1 rotate y */
	.half	    6,   69 	/* <m_rarmB>1 rotate z */
	.half	    6,   51 	/* <m_rhand>1 rotate x */
	.half	    6,   57 	/* <m_rhand>1 rotate y */
	.half	    6,   63 	/* <m_rhand>1 rotate z */
	.half	    1,    0 	/* chn15 rotate x */
	.half	    1,    0 	/* chn15 rotate y */
	.half	    1,  168 	/* chn15 rotate z */
	.half	    6,  147 	/* <m_llegA>1 rotate x */
	.half	    6,  153 	/* <m_llegA>1 rotate y */
	.half	    6,  159 	/* <m_llegA>1 rotate z */
	.half	    1,    0 	/* <m_llegB>1 rotate x */
	.half	    1,    0 	/* <m_llegB>1 rotate y */
	.half	    6,  141 	/* <m_llegB>1 rotate z */
	.half	    1,    0 	/* <m_ltoot>1 rotate x */
	.half	    1,    0 	/* <m_ltoot>1 rotate y */
	.half	    6,  135 	/* <m_ltoot>1 rotate z */
	.half	    1,    0 	/* chn17 rotate x */
	.half	    1,    0 	/* chn17 rotate y */
	.half	    1,  172 	/* chn17 rotate z */
	.half	    6,  117 	/* <m_rlegA>1 rotate x */
	.half	    6,  123 	/* <m_rlegA>1 rotate y */
	.half	    6,  129 	/* <m_rlegA>1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1 rotate y */
	.half	    6,  111 	/* <m_rlegB>1 rotate z */
	.half	    6,   93 	/* <m_rfoot>1 rotate x */
	.half	    6,   99 	/* <m_rfoot>1 rotate y */
	.half	    6,  105 	/* <m_rfoot>1 rotate z */

	.half	0x0000,0x00AA,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x3FFF,0x0000,0xFFA8,0xFED3,0xFDD4,0xFCF9,0xFC93,0xF97C
	.half	0xF989,0xF9AE,0xF9E5,0xFA29,0xFA73,0xED8C,0xEFDC,0xF564,0xFC08,0x01AA,0x042F,0xE67B,0xE759,0xE95B,0xEBA0,0xED45
	.half	0xED69,0xCA25,0xCEFD,0xDA95,0xE884,0xF460,0xF9C2,0xD6DA,0xD8DD,0xDDCA,0xE3F9,0xE9C0,0xED76,0xFDA5,0xF973,0xEF38
	.half	0xE27A,0xD6BF,0xCF8D,0x020B,0x0220,0x0251,0x028A,0x02B6,0x02C2,0x03A9,0x039B,0x0379,0x0352,0x0332,0x0328,0xF578
	.half	0xF670,0xF8B6,0xFB5C,0xFD73,0xFE0B,0xDAE3,0xDCF7,0xE205,0xE847,0xEDF9,0xF156,0x3BED,0x36EC,0x2B11,0x1D19,0x11BE
	.half	0x0DBD,0x29A2,0x274A,0x21AE,0x1AEB,0x1520,0x126D,0xF54C,0xF147,0xE7C3,0xDC8E,0xD377,0xD04D,0xFFC5,0xFFBE,0xFFAC
	.half	0xFF96,0xFF81,0xFF76,0xFF39,0xFF3D,0xFF47,0xFF53,0xFF5E,0xFF65,0xD5A9,0xD752,0xDB5C,0xE05B,0xE4E8,0xE797,0x41BC
	.half	0x3E24,0x355B,0x2A60,0x2031,0x19CE,0x0768,0x07C0,0x089A,0x09B3,0x0AC7,0x0B92,0x0954,0x08B1,0x0726,0x0543,0x0396
	.half	0x02AE,0xA3E7,0xA667,0xAC7E,0xB40E,0xBAFA,0xBF23,0xCB11,0xCBBF,0xCD66,0xCF71,0xD14D,0xD266,0x4CBC,0x49E6,0x4324
	.half	0x3B11,0x344B,0x3170,0xF894,0xF837,0xF753,0xF631,0xF51C,0xF460,0xF606,0xF6AC,0xF832,0xF9F6,0xFB53,0xFBA8,0x98E7
	.half	0x9B68,0xA15A,0xA860,0xAE20,0xB03E,0x7FFF,0x7FFF,0x4171,0xBF5F,0x7FFF,0x7FFF,0x4171,0xBF5F,0x007C,0x005E,0x0018
	.half	0xFFC6,0xFF80,0xFF63,0x0215,0x0214,0x0212,0x020F,0x020D,0x020C,0x02F2,0x00BE,0xFB7F,0xF539,0xEFF6,0xEDBC,0xFFB1
	.half	0xFFC0,0xFFE5,0x0010,0x0036,0x0049,0xFF67,0xFF67,0xFF66,0xFF66,0xFF65,0xFF64,0x211B,0x1D60,0x146A,0x0991,0x0030
	.half	0xFB9F,0x40A1
