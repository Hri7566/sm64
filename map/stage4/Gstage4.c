/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage4 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 18, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#include "teresa/teresa_ext_texture.h"
#include "teresa/kanoke_texture.h"

#include "teresa/tere_a_shape.sou"
#include "teresa/tere_a_a_shape.sou"
#include "teresa/tere_a_s_shape.sou"
#include "teresa/tere_a_ia_shape.sou"
#include "teresa/tere_a_kn_shape.sou"
#include "teresa/tere_a_mr_shape.sou"
#include "teresa/tere_b_shape.sou"
#include "teresa/tere_b_s_shape.sou"
#include "teresa/tere_b_ia_shape.sou"
#include "teresa/tere_b_kn_shape.sou"
#include "teresa/tere_b_mr_shape.sou"
#include "teresa/tere_c_shape.sou"
#include "teresa/tere_c_a_shape.sou"
#include "teresa/tere_c_s_shape.sou"
#include "teresa/tere_c_mr_shape.sou"
#include "teresa/tere_d_shape.sou"
#include "teresa/tere_d_a_shape.sou"
#include "teresa/tere_e_shape.sou"
#include "teresa/tere_f_shape.sou"
#include "teresa/tere_f_r_shape.sou"
#include "teresa/tere_f_cr_shape.sou"
#include "teresa/tere_g_shape.sou"
#include "teresa/tere_g_ia_shape.sou"
#include "teresa/tere_h_shape.sou"
#include "teresa/tere_h_a_shape.sou"
#include "teresa/tere_h_ia_shape.sou"
#include "teresa/tere_h_kn_shape.sou"
#include "teresa/tere_i_shape.sou"
#include "teresa/tere_i_s_shape.sou"
#include "teresa/tere_i_mr_shape.sou"
#include "teresa/tere_j_shape.sou"
#include "teresa/tere_j_a_shape.sou"
#include "teresa/tere_k_shape.sou"
#include "teresa/tere_l_shape.sou"
#include "teresa/tere_m_shape.sou"
#include "teresa/tere_m_a_shape.sou"
#include "teresa/tere_m_s_shape.sou"
#include "teresa/tere_m_ia_shape.sou"
#include "teresa/tere_m_kn_shape.sou"

#include "teresa/a_trap_shape.sou"
#include "teresa/b_trap_shape.sou"
#include "teresa/d_trap_a_shape.sou"
#include "teresa/d_trap_b_shape.sou"
#include "teresa/f_trap_shape.sou"
#include "teresa/l_trap_shape.sou"
#include "teresa/j_trap_shape.sou"
#include "teresa/kanoke_shape.sou"

#include "teresa/cx0401.flk"
#include "teresa/cx0401.ara"
#include "teresa/cx0401.tag"

#include "teresa/a_trap_check.flk"
#include "teresa/b_trap_check.flk"
#include "teresa/d_trap_check.flk"
#include "teresa/f_trap_check.flk"
#include "teresa/l_trap_check.flk"
#include "teresa/j_trap_check.flk"
#include "teresa/kanoke_check.flk"


/********************************************************************************/
/*	Water surface records in 4 - 1.												*/
/********************************************************************************/
static short waterSurf_04_01_00[] = {
	1,
	0,	0,	20,	-4812, 1485,   -4812, 7270,     640, 7270,     640, 1485,	0,	150,	0,
};

static short waterSurf_04_01_01[] = {
	1,
	0,	0,	20,	1536,-1637,    1536, 2662,    3789, 2662,    3789,-1637,	0,	150,	0,
};

WaterSurfRec waterSurf_04_00[] = {
		{	 0,	waterSurf_04_01_00	 },
		{   -1,	NULL	}
};

WaterSurfRec waterSurf_04_01[] = {
		{	 1,	waterSurf_04_01_01	 },
		{   -1,	NULL	}
};
