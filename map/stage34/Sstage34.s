/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 34 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage34


/* ================================================================================
		: Move BGs sequence.
===================================================================================	*/
SEQ_Stage34_MoveBGs:

		seqActor(S_movebg01,  0,0,0,  0,0,0,  0, 1,0,  e_kopa3_moveBG)
		seqActor(S_movebg02,  0,0,0,  0,0,0,  0, 2,0,  e_kopa3_moveBG)
		seqActor(S_movebg03,  0,0,0,  0,0,0,  0, 3,0,  e_kopa3_moveBG)
		seqActor(S_movebg04,  0,0,0,  0,0,0,  0, 4,0,  e_kopa3_moveBG)
		seqActor(S_movebg05,  0,0,0,  0,0,0,  0, 5,0,  e_kopa3_moveBG)
		seqActor(S_movebg06,  0,0,0,  0,0,0,  0, 6,0,  e_kopa3_moveBG)
		seqActor(S_movebg07,  0,0,0,  0,0,0,  0, 7,0,  e_kopa3_moveBG)
		seqActor(S_movebg08,  0,0,0,  0,0,0,  0, 8,0,  e_kopa3_moveBG)
		seqActor(S_movebg09,  0,0,0,  0,0,0,  0, 9,0,  e_kopa3_moveBG)
		seqActor(S_movebg10,  0,0,0,  0,0,0,  0,10,0,  e_kopa3_moveBG)

		seqActor(S_kirai,  -2122,512,-2912,   0,0,0,   0,0,0,   e_kirai)
		seqActor(S_kirai,  -3362,512, 1121,   0,0,0,   0,0,0,   e_kirai)
		seqActor(S_kirai,      0,512, 3584,   0,0,0,   0,0,0,   e_kirai)
		seqActor(S_kirai,   3363,512, 1121,   0,0,0,   0,0,0,   e_kirai)
		seqActor(S_kirai,   2123,512,-2912,   0,0,0,   0,0,0,   e_kirai)

		seqReturn


/* ================================================================================
		: Stage 34 main sequence.
===================================================================================	*/
SEQ_DoStage34:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage34SegmentRomStart , _GfxStage34SegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_aSegmentRomStart, _GfxEnemy2_aSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_aSegmentRomStart, _HmsEnemy2_aSegmentRomEnd	)
	seqLoadPres(SEGMENT_BACK	  ,	_BackKuppa3SegmentRomStart , _BackKuppa3SegmentRomEnd	)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetEnemy2_a)

		seqHmsShape(S_movebg01, RCP_HmsKopa3A)
		seqHmsShape(S_movebg02, RCP_HmsKopa3B)
		seqHmsShape(S_movebg03, RCP_HmsKopa3C)
		seqHmsShape(S_movebg04, RCP_HmsKopa3D)
		seqHmsShape(S_movebg05, RCP_HmsKopa3E)
		seqHmsShape(S_movebg06, RCP_HmsKopa3F)
		seqHmsShape(S_movebg07, RCP_HmsKopa3G)
		seqHmsShape(S_movebg08, RCP_HmsKopa3H)
		seqHmsShape(S_movebg09, RCP_HmsKopa3I)
		seqHmsShape(S_movebg10, RCP_HmsKopa3J)

		seqHmsShape(S_bg01, RCP_HmsKopa3Buoy)

		seqBeginScene(1, RCP_Stage34Scene1)
			seqActor(S_NULL,   0,307+START_H,0,  0,183,0,  0,10,0,  e_player_rolling)
			seqPort(10, 34, 1, 10)
			seqCall(SEQ_Stage34_MoveBGs)
/*			seqGameClear(??, ?, ??)		To the ending.		*/
			seqGameOver (21, 1, 12)
			seqMapInfo(cx3401_info)
			seqSetMusic(NA_STG_KUPPA, NA_LAST_KUPPA_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 183,  0,307,0)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
