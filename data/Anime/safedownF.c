/********************************************************************************
						Ultra 64 MARIO Brothers

						 mario safe fore down data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

						   March 16, 1996
 ********************************************************************************/

#include "../headers.h"


static short mario_safedown_F_prm[]={
	    0,    0,    0,    0,    0,    0,    0,    0,
	   -1,   -3,   -2,   -1,    0,    1,    1,   -5,
	   -4,    0,    0,    0,    0,    0,    0,    0,
	    0,  180,  181,  182,  176,  172,  177,  181,
	  178,  174,  175,  178,  179,  180,  178,  173,
	  166,  159,  155,  156,  159,  163,  166,  169,
	  170,    0,    0,    0,    0,    0,    0,    0,
	    0,    0,    4,   11,   16,   21,   23,   21,
	   12,    1,   -2,   -6,   -5,   -3,   -2,    0,
	    0,16383,    0,-1106,-3558,-6058,-7304,-7345,
	-7126,-6695,-6103,-5398,-4630,-3846,-3096,-2429,
	-1583, -602,    0,  178,  247,  238,  178,   99,
	   29,    0,-1668,-2491,-4311,-6150,-7034,-6995,
	-6743,-6324,-5779,-5155,-4493,-3839,-3235,-2726,
	-2276,-1890,-1668,-1591,-1560,-1565,-1591,-1625,
	-1655,-1668,-4724,-4984,-5560,-6146,-6434,-6436,
	-6375,-6263,-6113,-5936,-5746,-5554,-5373,-5215,
	-5035,-4842,-4724,-4688,-4674,-4676,-4688,-4704,
	-4718,-4724,-7904,-7332,-6123,-5035,-4826,-5808,
	-7488,-9421,-11159,-12257,-12335,-11689,-10883,-10480,
	-10838,-11579,-12164,-12491,-12810,-13106,-13362,-13565,
	-13697,-13745,-2660,-2725,-2876,-3045,-3166,-3173,
	-2997,-2666,-2256,-1845,-1508,-1324,-1368,-1718,
	-3846,-7237,-8983,-8520,-7402,-5894,-4260,-2765,
	-1674,-1251,-6865,-6827,-6758,-6723,-6789,-7023,
	-7440,-7983,-8605,-9259,-9900,-10481,-10956,-11278,
	-10931,-10048,-9565,-9696,-9988,-10376,-10793,-11173,
	-11449,-11556,-10490,-9351,-6638,-3404, -704,  407,
	  -48,-1207,-2853,-4774,-6756,-8585,-10046,-10926,
	-9694,-6865,-5297,-5629,-6460,-7590,-8818,-9943,
	-10766,-11084,    0, -104, -278, -312,    0,  819,
	 1992, 3255, 4347, 5006, 5138, 4919, 4488, 3984,
	 3546, 3142, 2666, 2150, 1626, 1125,  681,  323,
	   86,    0,-2743,-3192,-4211,-5303,-5976,-6136,
	-6076,-5881,-5635,-5422,-5227,-4994,-4745,-4503,
	-4289,-4085,-3863,-3633,-3408,-3199,-3016,-2872,
	-2777,-2743,-8008,-7522,-6423,-5254,-4555,-4424,
	-4537,-4807,-5144,-5460,-5730,-6013,-6346,-6766,
	-7311,-8214,-9393,-10407,-11245,-12132,-12977,-13692,
	-14186,-14371, 4219, 4756, 6027, 7517, 8713, 9500,
	10157,10761,11389,12119,13242,14707,16077,16912,
	16775,15639,13899,11753, 9396, 7026, 4841, 3036,
	 1808, 1355, 5090, 5417, 6232, 7282, 8314, 9261,
	10236,11262,12357,13544,15109,17037,18900,20266,
	20707,20250,19309,18027,16547,15013,13571,12362,
	11531,11222,-9778,-8508,-5580,-2316,  -39, 1065,
	 1759, 2133, 2282, 2298, 2197, 1918, 1435,  725,
	 -237,-1544,-3184,-5027,-6942,-8800,-10472,-11827,
	-12735,-13066,   76, -214, -754,-1041,   99, -480,
	-1007, -365,  565, 1258, 1768, 1932, 1877, 1688,
	 1322,  648,    0,   62,  -81,  -79,  -60,  -33,
	  -10,    0, -193, -205, -225, -234, -183, -268,
	 -334, -250, -161, -208, -266, -219, -190, -326,
	 -440, -335, -190, -197, -148, -155, -170, -188,
	 -202, -208,-17828,-18311,-19267,-19958,-19080,-15647,
	-13019,-14266,-16262,-16500,-16200,-15578,-15142,-15726,
	-16564,-17154,-17533,-17095,-16637,-16004,-15273,-14581,
	-14066,-13864, 9214,10701,13765,16312,15576,10041,
	 5500, 6890, 9109, 8631, 7511, 6350, 5748, 6680,
	 8375,10156,11979,12777,12357,11413,10213, 9025,
	 8116, 7754, 6602, 5809, 4072, 2358, 1071, 3721,
	 6339, 6497, 6114, 6392, 6799, 7233, 7768, 8307,
	 8501, 7888, 6993, 6440, 6353, 6408, 6549, 6718,
	 6861, 6921,  729,  782,  902, 1032, 1197, 2026,
	 2951, 3850, 4522, 4767, 4832, 4846, 4652, 4369,
	 4275, 4694, 5279, 5584, 5509, 5296, 5014, 4729,
	 4509, 4420,-14105,-15457,-18391,-21220,-22967,-22613,
	-22124,-22632,-23084,-22791,-22247,-21705,-21255,-20887,
	-20951,-22081,-23550,-24309,-24134,-23612,-22913,-22205,
	-21656,-21436,    0, -530,-1275,-1606,-1715,-1685,
	-1632,-1561,-1477,-1385,-1289,-1194,-1106,-1018,
	 -924, -826, -726, -625, -503, -383, -250, -127,
	  -35,    0,    0,    1,  -16,  -90, -159, -191,
	 -227, -262, -294, -318, -330, -327, -305, -240,
	 -128,   10,  151,  273,  323,  260,  176,   91,
	   26,    0,-15846,-16980,-18734,-20098,-20804,-20659,
	-20394,-20040,-19626,-19183,-18740,-18327,-17975,-17688,
	-17438,-17210,-16984,-16743,-16448,-16444,-16473,-16517,
	-16558,-16576, 6815, 6874, 7059, 7384, 7863, 8608,
	 9587,10592,11414,11847,11220, 9895, 9206, 9587,
	10387,11322,12104,12448,12252,11716,11009,10297,
	 9747, 9527,-4407,-3788,-2442,-1134, -631,-1424,
	-3009,-4657,-5641,-5603,-4985,-4260,-3899,-3949,
	-4135,-4451,-4892,-5452,-6267,-7354,-8527,-9597,
	-10376,-10678,-1518,-1423,-1232,-1087,-1131,-1515,
	-2125,-2707,-3007,-2789,-2244,-1761,-1727,-2318,
	-3281,-4366,-5321,-5897,-6045,-5948,-5703,-5408,
	-5160,-5055,-19849,-19391,-18338,-17174,-16380,-15983,
	-15758,-15816,-16267,-17358,-18956,-20607,-21851,-22714,
	-23468,-24033,-24329,-24275,-23710,-22691,-21467,-20285,
	-19394,-19042,-32767,-32767,16753,-16545,-32767,-32767,
	16753,-16545,-2276,-1727, -445, 1024, 2139, 2436,
	 2255, 1997, 1658, 1288,  938,  602,  272,    0,
	 -203, -350, -419, -403, -333, -242, -164,  -95,
	  -29,    0, 1844, 1526,  787,  -49, -660, -706,
	 -501, -374, -231,  -98,    0,   31,   15,    0,
	   -9,  -19,    0,   74,  178,  272,  312,  231,
	   81,    0, 5650, 4640, 2302, -331,-2225,-2341,
	-1431, -393,  833, 2037, 3008, 3643, 3986, 4046,
	 3743, 3156, 2527, 1914, 1256,  618,   62, -413,
	 -780, -926, 2528, 1463, -811,-2921,-3746,-3131,
	-2293,-1429, -739, -227,  223,  601,  896, 1096,
	 1055,  771,  453,  102, -283, -500, -458, -282,
	  -90,    0,-1135, -865, -286,  257,  510,  578,
	  643,  640,  508,  183, -283, -783,-1207,-1447,
	-1342, -925, -373,  457, 1423, 1916, 1647,  984,
	  309,    0, 1475,  138,-2597,-4815,-3810,-1842,
	  379, 2345, 3546, 3874, 3690, 3191, 2573, 2031,
	 1357,  534,   45,  171,  631, 1068, 1413, 1776,
	 2063, 2180,    0,   -6,  -19,  -33,  -39,  -38,
	  -33,  -27,  -19,  -12,   -5,    0,    2,    3,
	    3,    3,    3,    2,    2,    1,    0,    0,
	    0,    0,    0,   35,  123,  241,  361,  500,
	  674,  859, 1032, 1171, 1252, 1251,  895,  265,
	 -127, -205, -237, -233, -203, -156, -102,  -52,
	  -14,    0,16545,16460,16310,16253,16445,16914,
	17537,18247,18976,19658,20226,20613,20438,19764,
	19181,18823,18438,18046,17663,17309,17002,16760,
	16602,16545
};
static short mario_safedown_F_tbl[]={
	   24,    1,	/* chn14_4translate x */
	   24,   25,	/* chn14_4 translate y */
	   24,   49,	/* chn14_4 translate z */
	    1,    0,	/* chn14_4 rotate x */
	    1,   73,	/* chn14_4 rotate y */
	    1,    0,	/* chn14_4 rotate z */
	   24,  874,	/* <m_waist>1_3 rotate x */
	   24,  898,	/* <m_waist>1_3 rotate y */
	   24,  922,	/* <m_waist>1_3 rotate z */
	   24,  802,	/* <m_body>1 rotate x */
	   24,  826,	/* <m_body>1 rotate y */
	   24,  850,	/* <m_body>1 rotate z */
	   24,  730,	/* <m_head>2 rotate x */
	   24,  754,	/* <m_head>2 rotate y */
	   24,  778,	/* <m_head>2 rotate z */

	    1,  726,	/* chn6 rotate x */
	    1,  727,	/* chn6 rotate y */
	    1,  728,	/* chn6 rotate z */
	   24,  170,	/* <m_larmA>1 rotate x */
	   24,  194,	/* <m_larmA>1 rotate y */
	   24,  218,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	   24,  146,	/* <m_larmB>1 rotate z */
	   24,   74,	/* <m_lhand>1 rotate x */
	   24,   98,	/* <m_lhand>1 rotate y */
	   24,  122,	/* <m_lhand>1 rotate z */

	    1,  722,	/* chn10 rotate x */
	    1,  723,	/* chn10 rotate y */
	    1,  724,	/* chn10 rotate z */
	   24,  314,	/* <m_rarmA>1 rotate x */
	   24,  338,	/* <m_rarmA>1 rotate y */
	   24,  362,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	   24,  290,	/* <m_rarmB>1 rotate z */
	   24,  242,	/* <m_rhand>1 rotate x */
	    1,    0,	/* <m_rhand>1 rotate y */
	   24,  266,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  725,	/* chn15 rotate z */
	   24,  650,	/* <m_llegA>1 rotate x */
	   24,  674,	/* <m_llegA>1 rotate y */
	   24,  698,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	   24,  626,	/* <m_llegB>1 rotate z */
	   24,  554,	/* <m_ltoot>1 rotate x */
	   24,  578,	/* <m_ltoot>1 rotate y */
	   24,  602,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  729,	/* chn17 rotate z */
	   24,  482,	/* <m_rlegA>1 rotate x */
	   24,  506,	/* <m_rlegA>1 rotate y */
	   24,  530,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	   24,  458,	/* <m_rlegB>1 rotate z */
	   24,  386,	/* <m_rfoot>1 rotate x */
	   24,  410,	/* <m_rfoot>1 rotate y */
	   24,  434,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioSafeForeDown = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	24,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_safedown_F_prm,
	mario_safedown_F_tbl
};
