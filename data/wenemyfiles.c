/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: wenemyfiles.c
					Description	: 
					Date		: 1995.1.8
					Author		: 

 ********************************************************************************/

#include	"wp/pathwlib.c"
#include	"wp/pathnoko.c"
#include	"wp/pathsanbo.c"
#include	"wp/pathbat.c"
#include	"wp/pathheyho.c"
#include	"wp/pathkuribo.c"
#include	"wp/pathwanwan.c"
#include	"wp/pathhanachan.c"
#include	"wp/pathjugem.c"
#include	"wp/pathindy.c"
#include	"wp/pathlinelift.c"
#include	"wp/pathseesaw.c"
#include	"wp/pathpedallift.c"
#include	"wp/pathwaterbom.c"
#include	"wp/pathtokeiobj.c"
#include	"wp/pathsnowman.c"
#include	"wp/pathderublock.c"
#include	"wp/pathrotstand.c"
#include	"wp/pathstepslope.c"
#include	"wp/pathridestartlift.c"
#include	"wp/pathhart.c"
#include	"wp/pathwatercanon.c"
#include	"wp/pathubboo.c"
#include	"wp/pathnessy.c"
#include	"wp/pathchair.c"
#include	"wp/pathpiano.c"
#include	"wp/pathbook.c"
#include	"wp/pathfirepakun.c"
#include	"wp/pathmucho.c"
#include	"wp/pathjiro.c"
#include	"wp/pathhandman.c"
#include	"wp/pathcondor.c"
#include	"wp/pathminibird.c"
#include	"wp/pathracepengin.c"
#include	"wp/pathkanoke.c"
#include	"wp/pathbigshell.c"
#include	"wp/pathamembow.c"
#include	"wp/pathswinglift.c"
#include	"wp/pathchikuwa.c"
#include	"wp/pathmovebar.c"
#include	"wp/pathstarpanel.c"
#include	"wp/pathfly.c"
#include	"wp/pathbuku.c"

/*===============================================================================
		end end end end end end end end 
===============================================================================*/


