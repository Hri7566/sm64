/********************************************************************************
	shapes.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1995 Nintendo co., ltd.  All rights reserved

	May 11, 1995
 ********************************************************************************/

#ifndef SHAPES_H
#define	SHAPES_H

/********************************************************************************/
/*	Polygon shape numbers														*/
/********************************************************************************/

/*	Look at "include/pathmacs.h"	*/	
/*	Changed By H.Yajima 1995.3.23	*/

/********************************************************************************/
/*	Mario & Luigi skeleton animation numbers									*/
/********************************************************************************/

#define	SYNC_RUNNING	(1<<0)

enum {
	animeHumanBase,
	animeStandup,
	animeWait,
	animeSleep,
	animeWakeup,
	animeRunSlow,
	animeRunNormal,
	animeRunFast,
	animeDashSlow,
	animeDashNormal,
	animeDashFast,
	animeBrakeStart,
	animeBrakeEnd,
	animeUTurnStart,
	animeUTurnEnd,
	animeUTurnCont,
	animeJumpStart,
	animeJumpEnd,
	animeLandStart,
	animeLandEnd,
	animeRolling,
	animeSlipStart,
	animeSlipEnd,
	animeFrontDown,
	animeBackDown,
	animeShortDown,
	animeJumpDown,
	animePush,
	animePull,
	animeSwimWait,
	animeSwimStop,
	animeSwim1,
	animeSwim2,
	animeSwim3,
	animeStayLog,
	animeWalkLogL,
	animeWalkLogR,
	animeWaverLogL,
	animeWaverLogR,
	animeLandLog,
	animeTakeBurden,
	animeThrowBurden,
	animeHoldBurden,
	animeCarryBurden,
	animeJpThrowBurden,
	animeJumpingBurden,
	animeJumpEndBurden,
	animeLandingBurden,
	animeLandEndBurden,
	animeCannonJump,
	animeFlying,
	animeTitleDemo
};

/********************************************************************************/
/*	Shape datas.																*/
/********************************************************************************/

extern Gfx RCP_mtx[];
extern Hierarchy RCP_HmsFireA[];
extern Hierarchy RCP_HmsVanime[];

extern Hierarchy RCP_HmsEnemyTeresa[];
extern Hierarchy RCP_HmsEnemyKiller[];

extern Gfx RCP_otoshi1[];
extern Gfx RCP_otoshiL[];
extern Gfx RCP_otoshiR[];
extern Gfx RCP_seesawd[];
extern Gfx RCP_seesaw[];
extern Gfx RCP_bridge[];
extern Gfx RCP_haguru[];
extern Gfx RCP_roll[];
extern Gfx RCP_hito[];
extern Gfx RCP_pukucube[];

extern Hierarchy RCP_Hmsflag[];
extern Hierarchy RCP_HmsBtree[];

extern Gfx RCP_key[];
extern Gfx RCP_keyR[];
extern Gfx RCP_keyG[];
extern Gfx RCP_keyB[];

extern Gfx RCP_kabebig[];
extern Gfx RCP_kabesm[];
extern Gfx RCP_lift1[];
extern Gfx RCP_door1[];
extern Gfx RCP_door2[];
extern Hierarchy RCP_MTXita[];

extern Hierarchy RCP_MTXtestswing[];
extern Gfx 		 RCP_testswing[];
extern Hierarchy RCP_MTXtestcube[];
extern Gfx 		 RCP_testcube[];

/* player shapes */

extern Hierarchy RCP_MarioHierarchy[];
extern Hierarchy RCP_LuigiHierarchy[];
extern Hierarchy RCP_jet[];
extern Hierarchy RCP_truck[];
extern Hierarchy RCP_box[];

/* effect shapes */

extern Gfx			RCP_GfxEffectCrash[];
extern Gfx		 	RCP_GfxEffectZZZ1[];
extern Gfx			RCP_GfxEffectZZZ2[];
extern Gfx			RCP_GfxEffectZZZ3[];
extern Hierarchy	RCP_HmsEffectDust[];
extern Hierarchy	RCP_HmsEffectSpark[];
extern Hierarchy	RCP_HmsEffectSplash[];
extern Hierarchy	RCP_HmsEffectBubble[];

/* item shapes */

extern Hierarchy	RCP_HmsItemCoin[];
extern Hierarchy	RCP_HmsItemStar[];
extern Gfx			RCP_dokan[];
extern Gfx			RCP_t_box1[];
extern Gfx			RCP_t_box2[];




/* enemy shapes */

extern Gfx		 RCP_GfxGreenNoko[];
extern Gfx		 RCP_GfxGRedNoko[];

extern Hierarchy RCP_HmsEnemyShark[];
extern Hierarchy RCP_HmsEnemyfish[];
extern Hierarchy RCP_HmsEnemyfish_shadow[];
extern Hierarchy RCP_rabbitHierarchy[];
extern Hierarchy RCP_butterflyHierarchy[];


/* xxxxx shapes */

extern Gfx	RCP_puku[];
extern Gfx	RCP_cursor[];
extern Gfx	RCP_noko_h[];
extern Gfx	RCP_pakkun[];
extern Gfx	RCP_dossun[];

/* alphabet shapes */

extern Gfx	RCP_a[];
extern Gfx	RCP_b[];
extern Gfx	RCP_c[];
extern Gfx	RCP_d[];
extern Gfx	RCP_e[];
extern Gfx	RCP_f[];
extern Gfx	RCP_g[];
extern Gfx	RCP_h[];
extern Gfx	RCP_i[];
extern Gfx	RCP_j[];
extern Gfx	RCP_k[];
extern Gfx	RCP_l[];
extern Gfx	RCP_m[];
extern Gfx	RCP_n[];
extern Gfx	RCP_o[];
extern Gfx	RCP_p[];
extern Gfx	RCP_q[];
extern Gfx	RCP_r[];
extern Gfx	RCP_s[];
extern Gfx	RCP_t[];
extern Gfx	RCP_u[];
extern Gfx	RCP_v[];
extern Gfx	RCP_w[];
extern Gfx	RCP_x[];
extern Gfx	RCP_y[];
extern Gfx	RCP_z[];
extern Gfx	RCP_0n[];
extern Gfx	RCP_1n[];
extern Gfx	RCP_2n[];
extern Gfx	RCP_3n[];
extern Gfx	RCP_4n[];
extern Gfx	RCP_5n[];
extern Gfx	RCP_6n[];
extern Gfx	RCP_7n[];
extern Gfx	RCP_8n[];
extern Gfx	RCP_9n[];

extern Gfx	RCP_A[];
extern Gfx	RCP_B[];
extern Gfx	RCP_C[];
extern Gfx	RCP_D[];
extern Gfx	RCP_E[];
extern Gfx	RCP_F[];
extern Gfx	RCP_G[];
extern Gfx	RCP_H[];
extern Gfx	RCP_I[];
extern Gfx	RCP_J[];
extern Gfx	RCP_K[];
extern Gfx	RCP_L[];
extern Gfx	RCP_M[];
extern Gfx	RCP_N[];
extern Gfx	RCP_O[];
extern Gfx	RCP_P[];
extern Gfx	RCP_Q[];
extern Gfx	RCP_R[];
extern Gfx	RCP_S[];
extern Gfx	RCP_T[];
extern Gfx	RCP_U[];
extern Gfx	RCP_V[];
extern Gfx	RCP_W[];
extern Gfx	RCP_X[];
extern Gfx	RCP_Y[];
extern Gfx	RCP_Z[];
extern Gfx	RCP_0[];
extern Gfx	RCP_1[];
extern Gfx	RCP_2[];
extern Gfx	RCP_3[];
extern Gfx	RCP_4[];
extern Gfx	RCP_5[];
extern Gfx	RCP_6[];
extern Gfx	RCP_7[];
extern Gfx	RCP_8[];
extern Gfx	RCP_9[];

extern Gfx	RCP_tfont_on[];
extern Gfx	RCP_tfont_off[];
extern Gfx	RCP_tfont_main[];
extern Gfx	RCP_tfont1[];
extern Gfx	RCP_tfont2[];

extern Gfx RCP_RedBall[];
extern Gfx RCP_GreenBall[];
extern Gfx RCP_BlueBall[];
extern Gfx RCP_hatena[];
extern Gfx RCP_ita[];

extern Gfx RCP_horizon[];
extern Gfx RCP_cube[];
extern Gfx RCP_boxdata[];

extern Hierarchy RCP_HmsEffectRippleA[];
extern Hierarchy RCP_HmsEffectRippleB[];
extern Hierarchy RCP_HmsEffectWave[];

extern Hierarchy RCP_HmsEffectWaterColumn[];
extern Gfx gfx_waterdrop[];



#endif



