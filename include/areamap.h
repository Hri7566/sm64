/********************************************************************************
						Ultra 64 MARIO Brothers

					   Header file of "areamap.c".

			Copyright 1995 Nintendo co., ltd.  All rights reserved

				  This module was programmed by Y.Tanimoto.

							Sept 11, 1995
*********************************************************************************/

#ifndef	_AREAMAP_PROT
#define	_AREAMAP_PROT

extern ulong AreaMap(int code, MapNode *node, void *data);

typedef struct {
	ushort*		image_upper	;
	ushort*		image_lower	;
	float		xo_g		;
	float		zo_g		;
	float		side_g		;
} AreaMapData ;

#endif
