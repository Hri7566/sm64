/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage6 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE6_FOG_R		0
#define		STAGE6_FOG_G		0
#define		STAGE6_FOG_B		0
#define		STAGE6_FOG_START	950

#include "seltexture/seltexdef.h"					//		Texture.
#include "seltexture/sel_ext_texture.h"
#include "seltexture/auto_door_texture.h"
#include "seltexture/mipmap_texture.h"
#include "seltexture/mirror_texture.h"
#include "seltexture/wave_texture.h"

#include "wavedata/selectroom_wavedat.sou"			//		Wave data.

#include "selroom1f/mipmap_shape.sou"				//		Select room 1F.
#include "selroom1f/sel_a_shape.sou"
#include "selroom1f/sel_a_a_shape.sou"
#include "selroom1f/sel_a_g_shape.sou"
#include "selroom1f/sel_a_ia_shape.sou"
#include "selroom1f/sel_a_s_shape.sou"
#include "selroom1f/sel_a_p_shape.sou"
#include "selroom1f/sel_b_shape.sou"
#include "selroom1f/sel_c_shape.sou"
#include "selroom1f/sel_d_shape.sou"
#include "selroom1f/sel_d_p_shape.sou"
#include "selroom1f/sel_d_a_shape.sou"
#include "selroom1f/sel_e_shape.sou"
#include "selroom1f/sel_e_a_shape.sou"
#include "selroom1f/sel_f_shape.sou"
#include "selroom1f/sel_f_a_shape.sou"
#include "selroom1f/sel_f_aa_shape.sou"
#include "selroom1f/sel_g_shape.sou"
#include "selroom1f/sel_g_an_shape.sou"
#include "selroom1f/sel_g_g_shape.sou"
#include "selroom1f/sel_g_k_shape.sou"
#include "selroom1f/sel_g_ia_shape.sou"
#include "selroom1f/sel_g_a_shape.sou"
#include "selroom1f/sel_h_shape.sou"
#include "selroom1f/sel_h_a_shape.sou"
#include "selroom1f/sel_i_shape.sou"
#include "selroom1f/sel_c_trap_shape.sou"
#include "selroom1f/auto_door_shape.sou"

#include "selroom2f/sel2_a_shape.sou"				//		Select room 2F.
#include "selroom2f/sel2_b_shape.sou"
#include "selroom2f/sel2_b_s_shape.sou"
#include "selroom2f/sel2_b_a_shape.sou"
#include "selroom2f/sel2_c_shape.sou"
#include "selroom2f/sel2_c_k_shape.sou"
#include "selroom2f/sel2_c_s_shape.sou"
#include "selroom2f/sel2_d_shape.sou"
#include "selroom2f/sel2_e_shape.sou"
#include "selroom2f/sel2_e_furi_shape.sou"
#include "selroom2f/sel2_e_a_shape.sou"
#include "selroom2f/sel2_e_ia_shape.sou"
#include "selroom2f/sel2_e_gr_shape.sou"
#include "selroom2f/sel2_f_shape.sou"
#include "selroom2f/sel2_f_s_shape.sou"
#include "selroom2f/sel2_f_ng_shape.sou"
#include "selroom2f/hari_a_shape.sou"
#include "selroom2f/hari_b_shape.sou"

#include "selroom1b/selt_b_shape.sou"				//		Select room B1.
#include "selroom1b/selt_b_a_shape.sou"
#include "selroom1b/selt_b_ia_shape.sou"
#include "selroom1b/selt_c_shape.sou"
#include "selroom1b/selt_c_ia_shape.sou"
#include "selroom1b/selt_d_shape.sou"
#include "selroom1b/selt_d_a_shape.sou"
#include "selroom1b/selt_e_shape.sou"
#include "selroom1b/selt_e_ht_shape.sou"
#include "selroom1b/selt_e_a_shape.sou"
#include "selroom1b/selt_f_shape.sou"
#include "selroom1b/selt_sss_shape.sou"

#include "selroom1f/sel1f.flk"						//		Check file.
#include "selroom2f/sel2f.flk"
#include "selroom1b/sel1b.flk"
#include "selroom1b/sel1b_c2.flk"
#include "selroom1b/sel1b_c3.flk"

#include "selroom1f/sel1f.tag"						//		Tag file.
#include "selroom2f/sel2f.tag"
#include "selroom1b/sel1b.tag"

#include "selroom1f/sel1f.ara"						//		Area file.
#include "selroom2f/sel2f.ara"
#include "selroom1b/sel1b.ara"

#include "selroom1f/sel1ftrap_check.flk"
#include "selroom1f/auto_door_check.flk"
#include "selroom1b/selt_sss_c.flk"

#include "selroom1b/rabbit.rail"

/********************************************************************************/
/*	Water surface records in 6 - 1. 1B water.									*/
/********************************************************************************/
static short waterSurf_06_01_00[] = {
	1,
	0,   10, 10, -3225,-4146,   -3225, -255,     870, -255,     870,-4146,  1,  150,  2,
};

static short waterSurf_06_01_01[] = {
	1,
	0,    5,  7,  973,-1279,     973,  256,    5786,  256,    5786,-1279,  1,  150,  0,
};

static short waterSurf_06_01_02[] = {
	1,
	0,    3,  5, 5786,-1330,    5786,  410,    8038,  410,    8038,-1330,  1,  150,  0,
};

WaterSurfRec waterSurf_06_00[] = {
		{	 0, waterSurf_06_01_00	 },
		{	-1,	NULL	}					//	End of WaterSurfRec data.
};

WaterSurfRec waterSurf_06_12[] = {
		{	 1, waterSurf_06_01_01	 },
		{	 2, waterSurf_06_01_02	 },
		{	-1,	NULL	}					//	End of WaterSurfRec data.
};
