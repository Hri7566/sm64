/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage27 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"


#define		STAGE27_CLEAR_R		0
#define		STAGE27_CLEAR_G		0
#define		STAGE27_CLEAR_B		0

extern Gfx gfx_mini_s3[]		;
extern Gfx gfx_mini_s3flat[]	;
extern Gfx gfx_mini_s3uraomote[];
extern Gfx gfx_mini_s3nonfog[]	;
extern Gfx gfx_mini_s3_ia[]		;
extern Gfx gfx_mini_s3_daiza[]	;
extern Gfx gfx_mini_s3_gate[]	;

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage27Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(STAGE27_CLEAR_R, STAGE27_CLEAR_G, STAGE27_CLEAR_B, 1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_BACK,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF , gfx_mini_s3)
					hmsGfx(RM_SURF , gfx_mini_s3flat)
					hmsGfx(RM_SURF , gfx_mini_s3uraomote)
					hmsGfx(RM_SURF , gfx_mini_s3nonfog)
					hmsGfx(RM_XSURF, gfx_mini_s3_ia)
					hmsGfx(RM_SURF , gfx_mini_s3_daiza)
					hmsGfx(RM_DECAL, gfx_mini_s3_gate)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
