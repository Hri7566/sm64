/********************************************************************************
						Ultra 64 MARIO Brothers

					mario swim taking animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							February 1, 1996
 ********************************************************************************/

#include "../headers.h"


static short mario_swim_hold_base_prm[]={
	    0,  189,16383, 5724, 4207,  797,-2787,-4833,
	 -451, -915,-1944,-2994,-3520,-5722,-5908,-6353,
	-6884,-7332,-16331,-14480,-10325,-5966,-3502,24605,
	26750,31719,-28222,-24205,-25792,-25972,-26510,-27402,
	-28644,13312, 9833, 2048,-6065,-10529, -362, 1821,
	 5487, 9442,11989, 3231, 5835, 9071, 9181, 8495,
	-6815,-5857,-4231,-2365,-1241,-11532,-10641,-8648,
	-6573,-5434,-18474,-18214,-17706,-17360,-17583,14206,
	13069,10361, 7135, 4442,19833,19680,19445,19463,
	20069, -192, -269, -262,  142,  506,  495,  421,
	  324,  293,  292,-17623,-20325,-21272,-11751,-3003,
	20918,18741,13809, 8518, 5266, 2789, 3032, 3566,
	 4098, 4335, 4395, 4360, 4286, 4216, 4195,-20719,
	-19969,-18251,-16364,-15108,  216,  228,  206,   60,
	  -67, -160, -121,  -97, -186, -272,-17211,-19022,
	-19371,-11890,-5035,21355,18884,13296, 7328, 3718,
	-2289,-2798,-3932,-5100,-5712,-5202,-5030,-4642,
	-4228,-3978,-22624,-21598,-19251,-16680,-14981,-32616,
	32360,30938,29020,26976,32765,-32763,-32752,-32736,
	-32719,16717,16849,17189,17646,18134,-16545,-32580,
	-32063,-30743,-28961,-27064,32756,32725,32648,32544,
	32433,16801,16934,17276,17736,18226,-16545, 4209,
	 3643, 2349,  932,    0,-2072,-1806,-1192, -499,
	    0,-9140,-9269,-9578,-9949,-10263,   55,   69,
	   87,   75,    0, 5519, 4759, 3034, 1172,    0,
	 5574, 4824, 3150, 1419,  499,   -6,   -8,  -12,
	  -11,    0,   -6,   -8,   -9,   -8,    0,22313,
	23390,25815,28379,29871
};
static short mario_swim_hold_base_tbl[]={
	    1,    0,	/* chn14_2_3translate x */
	    1,    1,	/* chn14_2_3 translate y */
	    1,    0,	/* chn14_2_3 translate z */
	    1,    0,	/* chn14_2_3 rotate x */
	    1,    2,	/* chn14_2_3 rotate y */
	    1,    0,	/* chn14_2_3 rotate z */
	    5,  205,	/* <m_waist>1_3 rotate x */
	    5,  210,	/* <m_waist>1_3 rotate y */
	    5,  215,	/* <m_waist>1_3 rotate z */
	    5,  190,	/* <m_body>1 rotate x */
	    5,  195,	/* <m_body>1 rotate y */
	    5,  200,	/* <m_body>1 rotate z */
	    5,  175,	/* <m_head>2 rotate x */
	    5,  180,	/* <m_head>2 rotate y */
	    5,  185,	/* <m_head>2 rotate z */

	    5,  159,	/* chn6 rotate x */
	    5,  164,	/* chn6 rotate y */
	    5,  169,	/* chn6 rotate z */
	    5,   23,	/* <m_larmA>1 rotate x */
	    5,   28,	/* <m_larmA>1 rotate y */
	    5,   33,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	    5,   18,	/* <m_larmB>1 rotate z */
	    5,    3,	/* <m_rhand2>2 rotate x */
	    5,    8,	/* <m_rhand2>2 rotate y */
	    5,   13,	/* <m_rhand2>2 rotate z */

	    5,  143,	/* chn10 rotate x */
	    5,  148,	/* chn10 rotate y */
	    5,  153,	/* chn10 rotate z */
	    5,   58,	/* <m_rarmA>1 rotate x */
	    5,   63,	/* <m_rarmA>1 rotate y */
	    5,   68,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	    5,   53,	/* <m_rarmB>1 rotate z */
	    5,   38,	/* <m_rhand2>1 rotate x */
	    5,   43,	/* <m_rhand2>1 rotate y */
	    5,   48,	/* <m_rhand2>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  158,	/* chn15 rotate z */
	    5,  128,	/* <m_llegA>1 rotate x */
	    5,  133,	/* <m_llegA>1 rotate y */
	    5,  138,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	    5,  123,	/* <m_llegB>1 rotate z */
	    5,  108,	/* <m_ltoot>1 rotate x */
	    5,  113,	/* <m_ltoot>1 rotate y */
	    5,  118,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  174,	/* chn17 rotate z */
	    5,   93,	/* <m_rlegA>1 rotate x */
	    5,   98,	/* <m_rlegA>1 rotate y */
	    5,  103,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	    5,   88,	/* <m_rlegB>1 rotate z */
	    5,   73,	/* <m_rfoot>1 rotate x */
	    5,   78,	/* <m_rfoot>1 rotate y */
	    5,   83,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioSwimTaking = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	 5,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_swim_hold_base_prm,
	mario_swim_hold_base_tbl
};
