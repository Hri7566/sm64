/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Strategy

					April 4 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/*==============================================================================
						Path Data
 ===============================================================================*/
e_wd_pillar:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON | stf_playerangleON)
	p_save_nowpos
	p_program(s_wd_pillar_init)

	p_while
		p_program(s_wd_pillar_main)
	p_loop


e_pillar_hit:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON )
	p_save_nowpos

	p_while
		p_program(s_pillar_hit_main)
	p_loop


e_wd_pillarbase:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON)
	p_setshapeinfo(hasiradai_c_info)

	p_while
		p_program(stMainMoveBG)
	p_loop

/*=============================================
		kibako(stage 12)
===============================================*/
e_wd_kibako:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON)
	p_setshapeinfo(kibakocheck_info)
	p_save_nowpos

	p_while
		p_program(s_wd_kibako_main)
		p_program(stMainMoveBG)
	p_loop


/*################*/
#else
/*################*/


/*==================================================================================================
		C Program
 ===================================================================================================*/
#define pillar_accel	(execstp->s[stw_work0].f)

static HitCheckRecord wd_pillar_hit = {
	OBJNAME_DAMAGE,
	 150,3,0,0,		/* flag,ap,hp,coin 		 */
	 150, 300,		/* player attack hitarea */
	   0,   0,		/* player damage hitarea */
};
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_wd_pillar_init(void)
{
	execstp->s[stw_gravity].f   = 0.5;
	execstp->s[stw_friction].f  = 0.91;
	execstp->s[stw_specificG].f = 1.3;

}
/***************************************************************************************************
						    Main
***************************************************************************************************/
static void PillarMakeHit(void)
{
	int i;

	for (i=0; i<4; i++) {
		s_makeobj_chain(i,0,i*400+300,0,execstp,S_NULL,e_pillar_hit);
	}

}
/***************************************************************************************************
						    Main
***************************************************************************************************/
static short pillar_targetON(void)
{
	float posx = sin(player_angleY)*(500) + player_posX;
	float posz = cos(player_angleY)*(500) + player_posZ;

	return(atan((posz - obj_worldZ),(posx - obj_worldX)) );

}
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_wd_pillar_main(void)
{
	short target_angle;

	switch (obj_mode) {
		case 0: if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,1300)) {
					obj_angleY = obj_targetangle;
					obj_speedF = 1;
					PillarMakeHit();
					obj_mode = 1;
					objsound(NA_SE2_PILLAR_MOVE);
				}
				break;

		case 1: ObjMoveEvent_noInc();
				target_angle = pillar_targetON();
				obj_animeangleY = s_chase_angle(obj_animeangleY,target_angle,0x400);
				if (obj_timer > 10) {
					obj_mode = 2;
				}
				break;


		case 2: ObjMoveEvent_noInc();
				pillar_accel += 4;
				obj_anglespeedX += pillar_accel;
				obj_animeangleX += obj_anglespeedX;
				if (obj_animeangleX > 0x3900) {
					obj_worldX += sin(obj_animeangleY)*500;
					obj_worldZ += cos(obj_animeangleY)*500;
					Viewshaking(VS_MIDDLE,obj_worldX,obj_worldY,obj_worldZ);
					s_burneffect(0,0,92);
					execstp->status = 0;
					obj_remove_sound(NA_SE2_PILLAR_BOUND);
				}
				break;


	}



}

/*-------------------------------------------------------------------------------------------------*/

extern void s_pillar_hit_main(void)
{
	long angleX = (execstp->motherobj)->s[stw_animeangleX].d;	
	long angleY = (execstp->motherobj)->s[stw_animeangleY].d;	
	float posx =  (execstp->motherobj)->s[stw_worldX].f;
	float posy =  (execstp->motherobj)->s[stw_worldY].f;
	float posz =  (execstp->motherobj)->s[stw_worldZ].f;
	float oft = obj_programselect*400+300;

	obj_worldX = sin(angleX)*sin(angleY)*(oft) + posx;
	obj_worldY = cos(angleX)*(oft) + posy;
	obj_worldZ = sin(angleX)*cos(angleY)*(oft) + posz;

	s_set_hitparam(execstp,&wd_pillar_hit);		/* Set HitCheck Param */


	if( (execstp->motherobj)->status == 0 ) execstp->status = 0;
}


/*==================================================================================================
		Kibako	
 ===================================================================================================*/
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_wd_kibako_main(void)
{


	obj_worldY = obj_attY + sin(obj_timer*0x400)*10;


}
/*################*/
#endif
/*################*/


