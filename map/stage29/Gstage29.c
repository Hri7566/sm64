/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage29 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE29_FOG_R		0
#define		STAGE29_FOG_G		0
#define		STAGE29_FOG_B		0
#define		STAGE29_FOG_START	980

#include "ext6_flying_mario/ext6_ext_texture.h"

#include "ext6_flying_mario/e6_hontai_shape.sou"
#include "ext6_flying_mario/e6_niji_shape.sou"
#include "ext6_flying_mario/e6_shita_shape.sou"
#include "ext6_flying_mario/e6_kumo_shape.sou"

#include "ext6_flying_mario/cx2901.flk"
#include "ext6_flying_mario/cx2901.tag"
