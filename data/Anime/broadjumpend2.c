/********************************************************************************
						Ultra 64 MARIO Brothers

				mario short broad junmp end animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							January 20, 1995
 ********************************************************************************/

#include "../headers.h"


static short mario_long_jump2_end_prm[]={
	    0,   99,   96,   94,   95,   97,   99,  101,
	  102,  103,  104,  105,  106,  106,  106,  106,
	  105,  105,  105,  -35,16383,-4272,-5899,-7686,
	-9497,-11196,-12648,-13715,-14509,-15228,-15873,-16444,
	-16940,-17360,-17704,-17973,-18165,-18281,-18320,-4404,
	-5018,-5680,-6332,-6917,-7379,-7660,-7783,-7820,
	-7786,-7696,-7567,-7415,-7255,-7104,-6976,-6887,
	-6854,-5407,-4453,-3432,-2437,-1559, -892, -527,
	 -440, -523, -740,-1060,-1447,-1868,-2288,-2674,
	-2992,-3208,-3287,-7444,-8318,-9263,-10201,-11053,
	-11738,-12178,-12411,-12541,-12583,-12556,-12477,-12362,
	-12230,-12097,-11981,-11899,-11867, 7591, 4663,  741,
	-3748,-8380,-12729,-16369,-19692,-23289,-27037,-30811,
	31047,27595,24492,21861,19827,18516,18051,-8657,
	-8862,-8964,-9021,-9087,-9218,-9470,-9854,-10328,
	-10866,-11440,-12024,-12591,-13115,-13568,-13926,-14159,
	-14243,32702,-30159,-26683,-22801,-18909,-15405,-12688,
	-10460,-8197,-5951,-3773,-1716,  166, 1825, 3205,
	 4256, 4925, 5159, 8502, 9126, 9824,10548,11250,
	11884,12402,12864,13350,13845,14336,14807,15246,
	15636,15965,16218,16380,16437, 4152, 3855, 3554,
	 3283, 3080, 2980, 3019, 3213, 3533, 3949, 4430,
	 4946, 5466, 5961, 6398, 6748, 6981, 7065,-1350,
	-1158, -954, -757, -587, -464, -406, -410, -457,
	 -535, -638, -756, -880,-1001,-1111,-1200,-1260,
	-1282,-9388,-9282,-9180,-9094,-9041,-9033,-9087,
	-9212,-9402,-9640,-9909,-10194,-10480,-10749,-10986,
	-11175,-11301,-11346,-13074,-16850,-20393,-23500,-25964,
	-27583,-28151,-27678,-26429,-24576,-22292,-19751,-17126,
	-14590,-12316,-10478,-9247,-8799, 9659,11914,14376,
	16788,18894,20439,21166,21145,20682,19870,18802,
	17573,16276,15004,13852,12913,12281,12049,25117,
	19797,14757,10227, 6436, 3615, 1993, 1478, 1726,
	 2586, 3908, 5541, 7336, 9141,10807,12183,13119,
	13464, -461,  -22,  279,  214,  -36, -371, -605,
	 -598, -546, -464, -369, -278, -206, -146,  -86,
	  -33,    4,   18, -691, -710, -720, -718, -699,
	 -667, -627, -585, -542, -499, -456, -415, -375,
	 -334, -289, -248, -219, -207,-16307,-16629,-16987,
	-17670,-18034,-17136,-16197,-15990,-15900,-15880,-15886,
	-15872,-15793,-15609,-15353,-15090,-14885,-14803,10238,
	16493,20891,21728,21245,20266,19613,19390,19155,
	18915,18678,18450,18238,18049,17890,17768,17690,
	17662, -424, -341, -244, -137,  -14,   99,  185,
	  238,  275,  303,  327,  353,  388,  442,  508,
	  574,  623,  643,  416,  521,  661,  803,  950,
	 1144, 1430, 1819, 2273, 2762, 3253, 3717, 4122,
	 4494, 4852, 5157, 5369, 5449,-30619,-31813,-32606,
	-32749,-32572,-32357,-32386,-32708,32383,31874,31352,
	30868,30477,30169,29909,29709,29581,29535, -962,
	 -799, -643, -515, -415, -337, -335, -423, -532,
	 -654, -780, -903,-1014,-1125,-1244,-1352,-1430,
	-1460,  374,  249,  122,   -2, -108, -164, -182,
	 -181, -174, -164, -151, -135, -119,  -96,  -65,
	  -34,   -9,    0,-15040,-16063,-17203,-18710,-19974,
	-20049,-19741,-19613,-19491,-19367,-19233,-19081,-18904,
	-18646,-18312,-17978,-17721,-17619, 9603,16560,21552,
	22781,22641,21923,21419,21264,21075,20863,20637,
	20407,20185,19980,19803,19663,19572,19540,  983,
	  847,  686,  503,  284,   97,    8,   25,   99,
	  206,  325,  434,  511,  555,  582,  595,  600,
	  601,-1344,-1006, -763, -628, -525, -517, -673,
	 -998,-1428,-1920,-2433,-2922,-3347,-3742,-4140,
	-4487,-4733,-4827,-32072,-32727,32579,-32573,-31813,
	-31089,-30817,-31056,-31526,-32136,32740,32128,31653,
	31300,31004,30777,30632,30580,-32767,-32767,16753,
	-16545,-32767,-32767,16753,-16545,  112,  196,  284,
	  369,  440,  490,  509,  499,  467,  418,  358,
	  291,  221,  154,   93,   44,   11,    0, -683,
	 -550, -406, -264, -134,  -30,   36,   73,   93,
	  101,   98,   87,   71,   52,   33,   16,    4,
	    0,-3614,-3656,-3726,-3831,-3978,-4175,-4428,
	-4776,-5231,-5763,-6344,-6943,-7531,-8078,-8555,
	-8933,-9181,-9270,  -13, -137, -231, -292, -335,
	 -362, -375, -369, -347, -314, -279, -238, -188,
	 -135,  -84,  -41,  -11,    0,  289,  293,  299,
	  313,  335,  350,  346,  311,  255,  195,  150,
	  119,   88,   60,   36,   16,    4,    0, 9786,
	10165,10809,11952,13360,14609,15273,15023,14157,
	13194,12653,12588,12671,12853,13082,13309,13482,
	13551,16108,16069,16093,16468,16873,16656,16582,
	17077,17700,18384,19063,19669,20137,20438,20613,
	20695,20719,20719
};
static short mario_long_jump2_end_tbl[]={
	    1,    0,	/* chn14_3_3translate x */
	   18,    1,	/* chn14_3_3 translate y */
	    1,   19,	/* chn14_3_3 translate z */
	    1,    0,	/* chn14_3_3 rotate x */
	    1,   20,	/* chn14_3_3 rotate y */
	    1,    0,	/* chn14_3_3 rotate z */
	    1,    0,	/* <m_waist>1_3_1 rotate x */
	    1,    0,	/* <m_waist>1_3_1 rotate y */
	   18,  641,	/* <m_waist>1_3_1 rotate z */
	   18,  587,	/* <m_body>1_1 rotate x */
	   18,  605,	/* <m_body>1_1 rotate y */
	   18,  623,	/* <m_body>1_1 rotate z */
	   18,  533,	/* <m_head>2_1 rotate x */
	   18,  551,	/* <m_head>2_1 rotate y */
	   18,  569,	/* <m_head>2_1 rotate z */

	    1,  529,	/* chn6_1 rotate x */
	    1,  530,	/* chn6_1 rotate y */
	    1,  531,	/* chn6_1 rotate z */
	   18,   93,	/* <m_larmA>1_1 rotate x */
	   18,  111,	/* <m_larmA>1_1 rotate y */
	   18,  129,	/* <m_larmA>1_1 rotate z */
	    1,    0,	/* <m_larmB>1_1 rotate x */
	    1,    0,	/* <m_larmB>1_1 rotate y */
	   18,   75,	/* <m_larmB>1_1 rotate z */
	   18,   21,	/* <m_lhand>1_1 rotate x */
	   18,   39,	/* <m_lhand>1_1 rotate y */
	   18,   57,	/* <m_lhand>1_1 rotate z */

	    1,  525,	/* chn10_1 rotate x */
	    1,  526,	/* chn10_1 rotate y */
	    1,  527,	/* chn10_1 rotate z */
	   18,  219,	/* <m_rarmA>1_1 rotate x */
	   18,  237,	/* <m_rarmA>1_1 rotate y */
	   18,  255,	/* <m_rarmA>1_1 rotate z */
	    1,    0,	/* <m_rarmB>1_1 rotate x */
	    1,    0,	/* <m_rarmB>1_1 rotate y */
	   18,  201,	/* <m_rarmB>1_1 rotate z */
	   18,  147,	/* <m_rhand>1_1 rotate x */
	   18,  165,	/* <m_rhand>1_1 rotate y */
	   18,  183,	/* <m_rhand>1_1 rotate z */

	    1,    0,	/* chn15_1 rotate x */
	    1,    0,	/* chn15_1 rotate y */
	    1,  528,	/* chn15_1 rotate z */
	   18,  471,	/* <m_llegA>1_1 rotate x */
	   18,  489,	/* <m_llegA>1_1 rotate y */
	   18,  507,	/* <m_llegA>1_1 rotate z */
	    1,    0,	/* <m_llegB>1_1 rotate x */
	    1,    0,	/* <m_llegB>1_1 rotate y */
	   18,  453,	/* <m_llegB>1_1 rotate z */
	   18,  399,	/* <m_ltoot>1_1 rotate x */
	   18,  417,	/* <m_ltoot>1_1 rotate y */
	   18,  435,	/* <m_ltoot>1_1 rotate z */

	    1,    0,	/* chn17_1 rotate x */
	    1,    0,	/* chn17_1 rotate y */
	    1,  532,	/* chn17_1 rotate z */
	   18,  345,	/* <m_rlegA>1_1 rotate x */
	   18,  363,	/* <m_rlegA>1_1 rotate y */
	   18,  381,	/* <m_rlegA>1_1 rotate z */
	    1,    0,	/* <m_rlegB>1_1 rotate x */
	    1,    0,	/* <m_rlegB>1_1 rotate y */
	   18,  327,	/* <m_rlegB>1_1 rotate z */
	   18,  273,	/* <m_rfoot>1_1 rotate x */
	   18,  291,	/* <m_rfoot>1_1 rotate y */
	   18,  309,	/* <m_rfoot>1_1 rotate z */
};
AnimeRecord animMarioBroadJumpEnd2 = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	18,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_long_jump2_end_prm,
	mario_long_jump2_end_tbl
};
