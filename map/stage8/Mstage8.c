/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage8 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx sandcone0802_lod[]	;

extern Gfx gfx_sabaku_f[]		;				//	Sabaku.
extern Gfx gfx_sabaku_g[]		;
extern Gfx gfx_sabaku_r[]		;
extern Gfx gfx_sabaku_shadow[]	;
extern Gfx gfx_onimasu[]		;
extern Gfx gfx_sa_mobj_01[]		;

extern Gfx gfx_py[]				;				//	Pyramid.
extern Gfx gfx_py_ami[]			;
extern Gfx gfx_py_amikege[]		;
extern Gfx gfx_py_sn3[]			;
extern Gfx gfx_py_sn3_alp[]		;
extern Gfx gfx_py_obj01[]		;
extern Gfx gfx_py_obj01_ah[]	;
extern Gfx gfx_py_obj02[]		;
extern Gfx gfx_py_obj02_ah[]	;
extern Gfx gfx_py_obj03[]		;
extern Gfx gfx_py_obj04[]		;
extern Gfx gfx_py_obj04_ah[]	;


/* ----------------------------------------------------------------------------------------	*/
/* ----------------------------------------------------------------------------------------	*/


/********************************************************************************/
/*	Hierarchy map data of sand cone 1 in Sabaku.								*/
/********************************************************************************/
Hierarchy RCP_HmsSunaBG01[] = {
	hmsHeader(2000)
	hmsBegin()
		hmsCProg(0x0801, SandCone)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of sand cone 2 in pyramid.								*/
/********************************************************************************/
Hierarchy RCP_HmsSunaBG02[] = {
	hmsHeader(2000)
	hmsBegin()
		hmsLOD(-1000, 4000)
		hmsBegin()
			hmsCProg(0x0802, SandCone)
		hmsEnd()
		hmsLOD(4000, 12800)
		hmsBegin()
			hmsGfx(RM_SURF, sandcone0802_lod)
		hmsEnd()
    hmsEnd()
    hmsExit()
};


/* ----------------------------------------------------------------------------------------	*/
/* ----------------------------------------------------------------------------------------	*/


/********************************************************************************/
/*	Hierarchy map data of PyTop.												*/
/********************************************************************************/
Hierarchy RCP_HmsPyTop[] = {
    hmsHeader(1000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_sa_mobj_01)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of Onimasu in sabaku.									*/
/********************************************************************************/
Hierarchy RCP_onimasu[] = {
	hmsHeader(4000)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_onimasu)
	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1 [ Sabaku ].											*/
/********************************************************************************/
Hierarchy RCP_Stage8Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(5, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, 20000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_FIELD,  0,2000,6000,  -2048,0,-1024, GameCamera)
				hmsBegin()

					hmsGfx(RM_SURF	, gfx_sabaku_f)
					hmsGfx(RM_SURF	, gfx_sabaku_g)
					hmsGfx(RM_SPRITE, gfx_sabaku_r)
					hmsGfx(RM_XDECAL, gfx_sabaku_shadow)

					hmsCProg(0x0801, SandConeInit)

					hmsCProg(0, WaterInit)

					hmsCProg(0x0801, WaterDraw)
					hmsCProg(0x0851, WaterDraw)

					hmsCProg(0x0801, WaterFall_L)
					hmsCProg(0x0802, WaterFall_L)
					hmsCProg(0x0803, WaterFall_L)
					hmsObject()

					hmsCProg(0, WeatherProc)

				hmsEnd()
			hmsEnd()
		hmsEnd()

		hmsLayer(0)
		hmsBegin()
			hmsCProg(0, CannonEdge)
		hmsEnd()

	hmsEnd()
	hmsExit()
};


/* ----------------------------------------------------------------------------------------	*/
/* ----------------------------------------------------------------------------------------	*/


/* ===============================================================================
        : Hierarchy map data of PyObj01.
================================================================================== */
Hierarchy RCP_HmsPyObj01[] = {
    hmsHeader(700)
	hmsBegin()
		hmsShadow(550, 180, 11)
	    hmsBegin()
	        hmsGfx(RM_SURF  , gfx_py_obj01)
	        hmsGfx(RM_SPRITE, gfx_py_obj01_ah)
	    hmsEnd()
	hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of PyObj02.
================================================================================== */
Hierarchy RCP_HmsPyObj02[] = {
    hmsHeader(550)
    hmsBegin()
		hmsShadow(0, 180, 50)
	    hmsBegin()
	        hmsGfx(RM_SURF  , gfx_py_obj02)
	        hmsGfx(RM_SPRITE, gfx_py_obj02_ah)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of PyObj03.
================================================================================== */
Hierarchy RCP_HmsPyObj03[] = {
    hmsHeader(700)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_py_obj03)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of PyObj04.
================================================================================== */
Hierarchy RCP_HmsPyObj04[] = {
    hmsHeader(900)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_py_obj04)
        hmsGfx(RM_SPRITE, gfx_py_obj04_ah)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 2.														*/
/********************************************************************************/
Hierarchy RCP_Stage8Scene2[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0, 0, 0, 1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()

					hmsGfx(RM_SURF  , gfx_py)
			        hmsGfx(RM_SPRITE, gfx_py_ami)
			        hmsGfx(RM_XDECAL, gfx_py_amikege)

					hmsCProg(0x0802, SandConeInit)

					hmsCProg(0, WaterInit)
					hmsCProg(0x0801, WaterFall)
					hmsCProg(0x0802, WaterFall)
					hmsCProg(0x0803, WaterFall)

					hmsObject()
					hmsCProg(0, WeatherProc)

				hmsEnd()
			hmsEnd()
		hmsEnd()

	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 3.														*/
/********************************************************************************/
Hierarchy RCP_Stage8Scene3[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0, 0, 0, 1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF , gfx_py_sn3)
					hmsGfx(RM_XSURF, gfx_py_sn3_alp)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

	hmsEnd()
	hmsExit()
};
