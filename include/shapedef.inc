/********************************************************************************
	shapes.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1995 Nintendo co., ltd.  All rights reserved

	March 22, 1995
 ********************************************************************************/

#ifndef SHAPES_H
#define	SHAPES_H

/********************************************************************************/
/*	Polygon shape numbers														*/
/********************************************************************************/

enum {
	S_Mario,
	S_Luigi,
	S_PukuPuku,
	S_Marker,
	S_NokoNoko,
	S_Pakkun,
	S_0,
	S_1,
	S_2,
	S_3,
	S_4,
	S_5,
	S_6,
	S_7,
	S_8,
	S_9,
	S_A,
	S_B,
	S_C,
	S_D,
	S_E,
	S_F,
	S_G,
	S_H,
	S_I,
	S_J,
	S_K,
	S_L,
	S_M,
	S_N,
	S_O,
	S_P,
	S_Q,
	S_R,
	S_S,
	S_T,
	S_U,
	S_V,
	S_W,
	S_X,
	S_Y,
	S_Z,
	S_Dokan,
	S_Zushin,
	S_dust1,
	S_dust2,
	S_dust3,
	S_dust4,
	S_dust5,
	S_splash1,
	S_splash2,
	S_splash3,
	S_coin1,
	S_coin2,
	S_coin3,
	S_coin4,
	S_spark1,
	S_spark2,
	S_spark3,
	S_spark4,
	S_spark5,
	S_spark6,
	S_crash,
	S_zzz,
	S_star1,
	S_star2,
	S_star3,
	S_star4,
	S_star5,
	S_star6,
	S_star7,
	S_star8
};

/********************************************************************************/
/*	Mario & Luigi skeleton animation numbers									*/
/********************************************************************************/

enum {
	animeHumanBase,

	animeStandup,
	animeWait,
	animeSleep,
	animeWakeup,

	animeRunSlow,
	animeRunNormal,
	animeRunFast,

	animeDashSlow,
	animeDashNormal,
	animeDashFast,

	animeBrakeStart,
	animeBrakeEnd,
	animeUTurnStart,
	animeUTurnEnd,
	animeUTurnCont,

	animeJumpStart,
	animeJumpEnd,
	animeLandStart,
	animeLandEnd,
	animeRolling,
	animeSlipStart,
	animeSlipEnd,

	animeFrontDown,
	animeBackDown,
	animeShortDown,
	animeJumpDown,

	animePush,
	animePull
};

/********************************************************************************/
/*	Shape datas.																*/
/********************************************************************************/

extern Hierarchy RCP_MarioHierarchy[];
extern Hierarchy RCP_LuigiHierarchy[];

extern Gfx	RCP_puku[];
extern Gfx	RCP_cursor[];
extern Gfx	RCP_noko_h[];
extern Gfx	RCP_pakkun[];
extern Gfx	RCP_dokan[];
extern Gfx	RCP_dossun[];
extern Gfx	RCP_dust1[];
extern Gfx	RCP_dust2[];
extern Gfx	RCP_dust3[];
extern Gfx	RCP_dust4[];
extern Gfx	RCP_dust5[];
extern Gfx	RCP_splash1[];
extern Gfx	RCP_splash2[];
extern Gfx	RCP_splash3[];
extern Gfx	RCP_coin1[];
extern Gfx	RCP_coin2[];
extern Gfx	RCP_coin3[];
extern Gfx	RCP_coin4[];
extern Gfx	RCP_spark1[];
extern Gfx	RCP_spark2[];
extern Gfx	RCP_spark3[];
extern Gfx	RCP_spark4[];
extern Gfx	RCP_spark5[];
extern Gfx	RCP_spark6[];
extern Gfx	RCP_crash[];
extern Gfx	RCP_zzz[];
extern Gfx	RCP_star1[];
extern Gfx	RCP_star2[];
extern Gfx	RCP_star3[];
extern Gfx	RCP_star4[];
extern Gfx	RCP_star5[];
extern Gfx	RCP_star6[];
extern Gfx	RCP_star7[];
extern Gfx	RCP_star8[];

extern Gfx	RCP_a[];
extern Gfx	RCP_b[];
extern Gfx	RCP_c[];
extern Gfx	RCP_d[];
extern Gfx	RCP_e[];
extern Gfx	RCP_f[];
extern Gfx	RCP_g[];
extern Gfx	RCP_h[];
extern Gfx	RCP_i[];
extern Gfx	RCP_j[];
extern Gfx	RCP_k[];
extern Gfx	RCP_l[];
extern Gfx	RCP_m[];
extern Gfx	RCP_n[];
extern Gfx	RCP_o[];
extern Gfx	RCP_p[];
extern Gfx	RCP_q[];
extern Gfx	RCP_r[];
extern Gfx	RCP_s[];
extern Gfx	RCP_t[];
extern Gfx	RCP_u[];
extern Gfx	RCP_v[];
extern Gfx	RCP_w[];
extern Gfx	RCP_x[];
extern Gfx	RCP_y[];
extern Gfx	RCP_z[];
extern Gfx	RCP_0n[];
extern Gfx	RCP_1n[];
extern Gfx	RCP_2n[];
extern Gfx	RCP_3n[];
extern Gfx	RCP_4n[];
extern Gfx	RCP_5n[];
extern Gfx	RCP_6n[];
extern Gfx	RCP_7n[];
extern Gfx	RCP_8n[];
extern Gfx	RCP_9n[];

extern Gfx	RCP_A[];
extern Gfx	RCP_B[];
extern Gfx	RCP_C[];
extern Gfx	RCP_D[];
extern Gfx	RCP_E[];
extern Gfx	RCP_F[];
extern Gfx	RCP_G[];
extern Gfx	RCP_H[];
extern Gfx	RCP_I[];
extern Gfx	RCP_J[];
extern Gfx	RCP_K[];
extern Gfx	RCP_L[];
extern Gfx	RCP_M[];
extern Gfx	RCP_N[];
extern Gfx	RCP_O[];
extern Gfx	RCP_P[];
extern Gfx	RCP_Q[];
extern Gfx	RCP_R[];
extern Gfx	RCP_S[];
extern Gfx	RCP_T[];
extern Gfx	RCP_U[];
extern Gfx	RCP_V[];
extern Gfx	RCP_W[];
extern Gfx	RCP_X[];
extern Gfx	RCP_Y[];
extern Gfx	RCP_Z[];
extern Gfx	RCP_0[];
extern Gfx	RCP_1[];
extern Gfx	RCP_2[];
extern Gfx	RCP_3[];
extern Gfx	RCP_4[];
extern Gfx	RCP_5[];
extern Gfx	RCP_6[];
extern Gfx	RCP_7[];
extern Gfx	RCP_8[];
extern Gfx	RCP_9[];

#endif
