/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage36 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx gfx_dk_body[];
extern Gfx gfx_dk_ki[];
extern Gfx gfx_dk_enoki1_a[];
extern Gfx gfx_dk_enoki1_b[];
extern Gfx gfx_dk_enoki2_a[];
extern Gfx gfx_dk_enoki2_b[];
extern Gfx gfx_dk_enoki3_a[];
extern Gfx gfx_dk_enoki3_b[];
extern Gfx gfx_dk_enoki4_a[];
extern Gfx gfx_dk_enoki4_b[];
extern Gfx gfx_dk_start[];
extern Gfx gfx_dk_habatobi[];
extern Gfx gfx_dk_taihou[];
extern Gfx gfx_dk_hana1[];
extern Gfx gfx_dk_hana2[];
extern Gfx gfx_dk_maruta1[];
extern Gfx gfx_dk_maruta2[];
extern Gfx gfx_dk_gururi[];
extern Gfx gfx_dk_kozou[];
extern Gfx gfx_dk_top_a[];
extern Gfx gfx_dk_top_b[];
extern Gfx gfx_dk_fuji_a[];
extern Gfx gfx_dk_fuji_b[];
extern Gfx gfx_dk_asiba1_a[];
extern Gfx gfx_dk_asiba1_b[];
extern Gfx gfx_dk_asiba2[];
extern Gfx gfx_dk_asiba3[];
extern Gfx gfx_dk_kakusi_a[];
extern Gfx gfx_dk_kakusi_b[];
extern Gfx gfx_maruta[]	 ;
extern Gfx gfx_dk_kago1[];
extern Gfx gfx_dk_kago2[];

extern Gfx gfx_abcda_2[] ;
extern Gfx gfx_a_2_geta[];
extern Gfx gfx_abcdb_2[] ;
extern Gfx gfx_abcdb_ya[];
extern Gfx gfx_b_2_geta[];
extern Gfx gfx_abcdc_2[] ;
extern Gfx gfx_c_2_geta[];
extern Gfx RCP_endoor[]	 ;
extern Gfx gfx_kazari_1[];
extern Gfx gfx_kazari_2[];
extern Gfx gfx_kazari_3[];
extern Gfx gfx_kazari_4[];


/* ------------------------------------------------------------------------------------	*/
/* ------------------------------------------------------------------------------------	*/


/* ===============================================================================
        : Hierarchy map data of DkKago.
================================================================================== */
Hierarchy RCP_HmsDkKago[] = {
    hmsHeader(300)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_dk_kago1)
        hmsGfx(RM_SPRITE, gfx_dk_kago2)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DonkeyMaruta.
================================================================================== */
Hierarchy RCP_HmsDonkeyMaruta[] = {
    hmsHeader(1200)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_maruta)
    hmsEnd()
    hmsExit()
};


/* ------------------------------------------------------------------------------------	*/
/* ------------------------------------------------------------------------------------	*/


/* ===============================================================================
        : Hierarchy map data of DkEnoki1.
================================================================================== */
Hierarchy RCP_HmsDkEnoki1[] = {
    hmsHeader(1800)
    hmsBegin()
		hmsLOD(-2800, 10000)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_enoki1_a)
	        hmsGfx(RM_XSURF , gfx_dk_enoki1_b)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkEnoki2.
================================================================================== */
Hierarchy RCP_HmsDkEnoki2[] = {
    hmsHeader(1700)
    hmsBegin()
		hmsLOD(-2700, 6500)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_enoki2_a)
	        hmsGfx(RM_XSURF , gfx_dk_enoki2_b)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkEnoki3.
================================================================================== */
Hierarchy RCP_HmsDkEnoki3[] = {
    hmsHeader(1700)
    hmsBegin()
		hmsLOD(-2700, 6500)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_enoki3_a)
	        hmsGfx(RM_XSURF , gfx_dk_enoki3_b)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkEnoki4.
================================================================================== */
Hierarchy RCP_HmsDkEnoki4[] = {
    hmsHeader(1700)
    hmsBegin()
		hmsLOD(-2700, 6500)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_enoki4_a)
	        hmsGfx(RM_XSURF , gfx_dk_enoki4_b)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkStart.
================================================================================== */
Hierarchy RCP_HmsDkStart[] = {
    hmsHeader(650)
    hmsBegin()
		hmsLOD(-1650, 9500)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_start)
		hmsEnd()
	
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkHabatobi.
================================================================================== */
Hierarchy RCP_HmsDkHabatobi[] = {
    hmsHeader(1900)
    hmsBegin()
		hmsLOD(-2900, 8000)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_habatobi)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkTaihou.
================================================================================== */
Hierarchy RCP_HmsDkTaihou[] = {
    hmsHeader(450)
    hmsBegin()
		hmsLOD(-1450, 5500)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_taihou)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkHana1.
================================================================================== */
Hierarchy RCP_HmsDkHana1[] = {
    hmsHeader(2200)
    hmsBegin()
		hmsLOD(-3200, 10000)
		hmsBegin()
	        hmsGfx(RM_SURF, gfx_dk_hana1)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkHana2.
================================================================================== */
Hierarchy RCP_HmsDkHana2[] = {
    hmsHeader(750)
    hmsBegin()
		hmsLOD(-1750, 7000)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_hana2)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkMaruta1.
================================================================================== */
Hierarchy RCP_HmsDkMaruta1[] = {
    hmsHeader(1200)
    hmsBegin()
		hmsLOD(-2200, 8000)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_maruta1)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkMaruta2.
================================================================================== */
Hierarchy RCP_HmsDkMaruta2[] = {
    hmsHeader(1500)
    hmsBegin()
		hmsLOD(-2100, 8000)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_maruta2)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkGururi.
================================================================================== */
Hierarchy RCP_HmsDkGururi[] = {
    hmsHeader(1650)
    hmsBegin()
		hmsLOD(-2650, 5500)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_gururi)
		hmsBegin()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkKozou.
================================================================================== */
Hierarchy RCP_HmsDkKozou[] = {
    hmsHeader(350)
    hmsBegin()
		hmsLOD(-1350, 3000)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_kozou)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkTop.
================================================================================== */
Hierarchy RCP_HmsDkTop[] = {
    hmsHeader(3200)
    hmsBegin()
		hmsGfx(RM_SURF  , gfx_dk_top_a)
		hmsGfx(RM_SPRITE, gfx_dk_top_b)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkFuji.
================================================================================== */
Hierarchy RCP_HmsDkFuji[] = {
    hmsHeader(780)
    hmsBegin()
		hmsLOD(-1780, 5500)
		hmsBegin()
	        hmsGfx(RM_SPRITE, gfx_dk_fuji_a)
	        hmsGfx(RM_SURF  , gfx_dk_fuji_b)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkAsiba1.
================================================================================== */
Hierarchy RCP_HmsDkAsiba1[] = {
    hmsHeader(1700)
    hmsBegin()
		hmsLOD(-2700, 5000)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_asiba1_a)
	        hmsGfx(RM_SPRITE, gfx_dk_asiba1_b)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkAsiba2.
================================================================================== */
Hierarchy RCP_HmsDkAsiba2[] = {
    hmsHeader(920)
    hmsBegin()
		hmsLOD(-1920, 5000)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_asiba2)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkAsiba3.
================================================================================== */
Hierarchy RCP_HmsDkAsiba3[] = {
    hmsHeader(1100)
    hmsBegin()
		hmsLOD(-2100, 5000)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_asiba3)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of DkKakusi.
================================================================================== */
Hierarchy RCP_HmsDkKakusi[] = {
    hmsHeader(900)
    hmsBegin()
		hmsLOD(-1900, 5000)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_dk_kakusi_a)
	        hmsGfx(RM_SPRITE, gfx_dk_kakusi_b)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage36Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(0, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_FIELD,  0,2000,6000,  0,-2200,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF, gfx_dk_body)
					hmsGfx(RM_SURF, gfx_dk_ki  )
					hmsObject()
					hmsCProg(0, WaveInit)
					hmsCProg(0x0200, WaveMove)
					hmsCProg(0, WaterInit)
					hmsCProg(0x3601, WaterFall)
					hmsCProg(0x3602, WaterFall)
					hmsCProg(0x3603, WaterFall)
					hmsCProg(0x3604, WaterFall)
					hmsCProg(0x3605, WaterFall)
					hmsCProg(0x3601, WaterDraw)
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

		hmsLayer(0)
		hmsBegin()
			hmsCProg(0, CannonEdge)
		hmsEnd()

	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 2.														*/
/********************************************************************************/
Hierarchy RCP_Stage36Scene2[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0, 0, 0, 1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_BACK,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF, gfx_abcda_2)
					hmsGfx(RM_SURF, gfx_a_2_geta)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 3.														*/
/********************************************************************************/
Hierarchy RCP_Stage36Scene3[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0, 0, 0, 1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_BACK,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF, gfx_abcdb_2)
					hmsGfx(RM_SURF, gfx_abcdb_ya)
					hmsGfx(RM_SURF, gfx_b_2_geta)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 4.														*/
/********************************************************************************/
Hierarchy RCP_Stage36Scene4[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0, 0, 0, 1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_BACK,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF, gfx_abcdc_2)
					hmsGfx(RM_SURF, gfx_c_2_geta)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Hierarchy data of slider decorations.										*/
/********************************************************************************/
Hierarchy RCP_HmsSlider2Dec1[] = {
	hmsHeader(1000)
	hmsBegin()
		hmsBboard(0,0,0)
		hmsBegin()
			hmsScale(0.5f)
			hmsBegin()
				hmsGfx(RM_SPRITE, gfx_kazari_1)
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};

Hierarchy RCP_HmsSlider2Dec2[] = { 
	hmsHeader(1000)
	hmsBegin()
		hmsBboard(0,0,0)
		hmsBegin()
			hmsScale(0.5f)
			hmsBegin()
				hmsGfx(RM_SPRITE, gfx_kazari_2)
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};

Hierarchy RCP_HmsSlider2Dec3[] = { 
	hmsHeader(1000)
	hmsBegin()
		hmsBboard(0,0,0)
		hmsBegin()
			hmsScale(0.5f)
			hmsBegin()
				hmsGfx(RM_SPRITE, gfx_kazari_3)
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};

Hierarchy RCP_HmsSlider2Dec4[] = { 
	hmsHeader(1000)
	hmsBegin()
		hmsBboard(0,0,0)
		hmsBegin()
			hmsScale(0.5f)
			hmsBegin()
				hmsGfx(RM_SPRITE, gfx_kazari_4)
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of Endoor.												*/
/********************************************************************************/
Hierarchy RCP_HmsEndoor[] = {
	hmsHeader(8000)
	hmsBegin()
		hmsGfx(RM_SURF, RCP_endoor)
	hmsEnd()
  	hmsExit()
};
