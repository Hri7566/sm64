/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: pathhagetaka.s
					Description	: 
					Date		: 1995.
					Author		: H.yajima

 ********************************************************************************/


#define ANM_hagetaka_fly  		 	0
#define ANM_hagetaka_attack  		2

/*################*/
#ifdef ASSEMBLER
/*################*/

e_hagetaka:
	p_initialize(option)
	p_setbit(flag,stf_moveON | stf_YangleSAME | stf_playerdistON | stf_playerangleON )
	p_set_pointer(skelanime,hagetaka_anime)
	p_set_skelanime_number(0)
	p_setmovedata(30,-400,-50,0,0,200,0,0)
	p_save_nowpos
	p_while
		p_program(s_hagetaka)
	p_loop


/*################*/
#else
/*################*/

#define		HAGETAKA_OFF			0
#define		HAGETAKA_HAVE_HAT		1
#define		HAGETAKA_HAVE_POLYSTAR	2
#define		HAGETAKA_HAVE_SHADESTAR	3

#define			hagetaka_dy	(execstp->s[stw_work0].f)

/********************************************************************************
		----------------------
		 C Program (hagetaka)
		----------------------
 ********************************************************************************/

extern unsigned long CtrlCoord(int code,MapNode *node, void *data)
{

	if (code == MAP_CBACK_EXEC) {
#if 0
		((MapCoord *)(node->next))->position[0] = effect_p0;
		((MapCoord *)(node->next))->position[1] = effect_p1;
		((MapCoord *)(node->next))->position[2] = effect_p2;
#endif
		((MapCoord *)(node->next))->position[0] = 300;
		((MapCoord *)(node->next))->position[1] = 300;
		((MapCoord *)(node->next))->position[2] = 0;

	}

	return(0);

}

/********************************************************************************
		hagetaka waitting
 ********************************************************************************/

static void hagetaka_waitloop(void)
{
	short angleY = s_calc_returnangle();
	float dst = s_distanceXZ_obj2obj(execstp,player1stp);

	obj_gravity 		= 0;	/* G ... 0 */
	obj_animecounter 	= 0;
	obj_speedF		 	= 10;

	obj_animecounter = effect_p0;
	s_set_skelanimeNo(ANM_hagetaka_fly);
	s_chase_angleY(angleY,0x200);

}


/********************************************************************************
		----------
		 hagetaka
		----------
 ********************************************************************************/

static void *hagetaka_modejmp[] = {
	
	 hagetaka_waitloop
	
};

extern void s_hagetaka(void)
{

	s_enemybgcheck();
	s_modejmp(hagetaka_modejmp);
	s_3Dmove();

}

/*################*/
#endif
/*################*/
/*===============================================================================
		end end end end end end end end 
===============================================================================*/



