/********************************************************************************
						Ultra 64 MARIO Brothers

					texture buffer definition module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							May 3, 1995
 ********************************************************************************/

unsigned short textureMemory[26][1024];
