/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage31 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
*********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#include "ext8/ext8_ext_texture.h"

#include "ext8/sky_1_shape.sou"
#include "ext8/sky_clear_shape.sou"
#include "ext8/sky_niji_shape.sou"
#include "ext8/cx3101.flk"
#include "ext8/cx3101.tag"
