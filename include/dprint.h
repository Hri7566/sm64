
/********************************************************************************
		 		Iwamoto Prototype Function
*********************************************************************************/

/********************************************************************************
		 		Function prototypes
*********************************************************************************/

extern	void	dprintf(int,int,const char *,int);	
extern	void	dmprintf(int,int,const char *);			//Message only
extern	void	dcprintf(int,int,const char *);			//Offset Center

/********************************************************************************
		 		Call Message Function 
*********************************************************************************/

extern int		messageAnsNo;

extern void     CallMessage(short);
extern void     CallMessageInt(short,int);
extern void 	CallMessageFuda(short);
extern void     CallSelectMessage(short);
extern void     CallMessageYesNo(short);
extern short    GetMessageNo(void);

/********************************************************************************
		 		Object Function 
*********************************************************************************/

extern void s_enemyset_star(float,float,float);
extern void s_extraset_star(float,float,float);


/********************************************************************************
		 		Ending Message Function 
*********************************************************************************/
extern void ClearEndingMessage(void);
extern void BeginEndingMessage(void);
extern void EndingMessage(short,short,char*);
extern void EndEndingMessage(void);
extern void CallEndingDemoMessage(short,short,short,short);
extern void EndingDemoMessageEvent(void);

/********************************************************************************
		 		Pause & Save Message Function 
*********************************************************************************/
extern void iwaStageInit(void);
extern void MessageEnforcedDestroy(void);
extern void iwa_StratInit(void);

/********************************************************************************
		 		Camera Icon  Function 
*********************************************************************************/
extern void CameraIconSet(short);
#define CAMICON_MARIO	0x0001
#define CAMICON_JUGEM	0x0002
#define CAMICON_STOP	0x0004
#define CAMICON_LONG	0x0008
#define CAMICON_UP		0x0010




