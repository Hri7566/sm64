/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: pathwaterdive.s
					Description	: 
					Date		: 1995.
					Author		: H.yajima

 ********************************************************************************/


/*################*/
#ifdef ASSEMBLER
/*################*/

e_waterdive:
	p_initialize(option)
	p_setbit(flag,stf_moveON)
	p_softspritemodeON

	p_animereset

	p_do(3)
		p_animeinc
		p_program(s_waterdive_main)
		p_wait(1)
		p_program(s_waterdive_main)
	p_next

	p_do(5)
		p_animeinc
		p_wait(1)
	p_next

	p_mother_clrbit(effect,stf_diving)
	p_killshape

/*################*/
#else
/*################*/

MoveInitRecord waterdive_init[] = {
{
 	MOVEINIT_RANDOMANGLE,
	S_waterdrop,		/* shape name			*/
	e_smallwaterdrop,	/* path pointer			*/
	0,  				/* angle Y 				*/
	0,					/* random offset (XZ) 	*/
	5,3,				/* speed F 				*/
	30,20,				/* speed Y				*/
	0.5,1,				/* scale				*/
}
};


/********************************************************************************
		water dive initialize
 ********************************************************************************/


extern void s_waterdive_main(void)
{
	int	i;

	if ( obj_timer == 0 ){
		obj_worldY = mcWaterCheck(obj_worldX,obj_worldZ);
	}

	if ( obj_worldY > -10000 ){
		for(i=0;i<3;i++) s_makeobj_initdata(execstp,waterdive_init);
	}

}


/*################*/
#endif
/*################*/
/*===============================================================================
		end end end end end end end end 
===============================================================================*/



