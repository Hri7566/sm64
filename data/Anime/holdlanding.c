/********************************************************************************
						Ultra 64 MARIO Brothers

					mario hold landing animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 26, 1995
 ********************************************************************************/

#include "../headers.h"


static short mario_carry_landing_prm[]={
	    0,  170,16383,-6194,-1660,-5781,-3002, 4834,
	-3610,-29073, 2767,  525,-4654,-7069,-3085, 5354,
	-26304, -513, -164,-10171, 7279, 7698, 2180,-16667,
	 -106,  219,-8708,22107,-1942,-5307,-30669,-32767,
	-32767,16753,-16545,-32767,-32767,16753,-16545, 5761,
	 2676,13949
};
static short mario_carry_landing_tbl[]={
	    1,    0,	/* chn14_1translate x */
	    1,    1,	/* chn14_1 translate y */
	    1,    0,	/* chn14_1 translate z */
	    1,    0,	/* chn14_1 rotate x */
	    1,    2,	/* chn14_1 rotate y */
	    1,    0,	/* chn14_1 rotate z */
	    1,    0,	/* <m_waist>1 rotate x */
	    1,    0,	/* <m_waist>1 rotate y */
	    1,   41,	/* <m_waist>1 rotate z */
	    1,    0,	/* <m_body>1_4 rotate x */
	    1,    0,	/* <m_body>1_4 rotate y */
	    1,   40,	/* <m_body>1_4 rotate z */
	    1,    0,	/* <m_head>1 rotate x */
	    1,    0,	/* <m_head>1 rotate y */
	    1,   39,	/* <m_head>1 rotate z */

	    1,   35,	/* chn6_4 rotate x */
	    1,   36,	/* chn6_4 rotate y */
	    1,   37,	/* chn6_4 rotate z */
	    1,    7,	/* <m_larmA>1 rotate x */
	    1,    8,	/* <m_larmA>1 rotate y */
	    1,    9,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1_1 rotate x */
	    1,    0,	/* <m_larmB>1_1 rotate y */
	    1,    6,	/* <m_larmB>1_1 rotate z */
	    1,    3,	/* <m_lhand>1_1 rotate x */
	    1,    4,	/* <m_lhand>1_1 rotate y */
	    1,    5,	/* <m_lhand>1_1 rotate z */

	    1,   31,	/* chn10_4 rotate x */
	    1,   32,	/* chn10_4 rotate y */
	    1,   33,	/* chn10_4 rotate z */
	    1,   14,	/* <m_rarmA>1 rotate x */
	    1,   15,	/* <m_rarmA>1 rotate y */
	    1,   16,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1_4 rotate x */
	    1,    0,	/* <m_rarmB>1_4 rotate y */
	    1,   13,	/* <m_rarmB>1_4 rotate z */
	    1,   10,	/* <m_rhand>1_1 rotate x */
	    1,   11,	/* <m_rhand>1_1 rotate y */
	    1,   12,	/* <m_rhand>1_1 rotate z */

	    1,    0,	/* chn15_4 rotate x */
	    1,    0,	/* chn15_4 rotate y */
	    1,   34,	/* chn15_4 rotate z */
	    1,   28,	/* <m_llegA>1_4 rotate x */
	    1,   29,	/* <m_llegA>1_4 rotate y */
	    1,   30,	/* <m_llegA>1_4 rotate z */
	    1,    0,	/* <m_llegB>1_4 rotate x */
	    1,    0,	/* <m_llegB>1_4 rotate y */
	    1,   27,	/* <m_llegB>1_4 rotate z */
	    1,   24,	/* <m_ltoot>1_4 rotate x */
	    1,   25,	/* <m_ltoot>1_4 rotate y */
	    1,   26,	/* <m_ltoot>1_4 rotate z */

	    1,    0,	/* chn17_4 rotate x */
	    1,    0,	/* chn17_4 rotate y */
	    1,   38,	/* chn17_4 rotate z */
	    1,   21,	/* <m_rlegA>1_4 rotate x */
	    1,   22,	/* <m_rlegA>1_4 rotate y */
	    1,   23,	/* <m_rlegA>1_4 rotate z */
	    1,    0,	/* <m_rlegB>1_4 rotate x */
	    1,    0,	/* <m_rlegB>1_4 rotate y */
	    1,   20,	/* <m_rlegB>1_4 rotate z */
	    1,   17,	/* <m_rtoot>1 rotate x */
	    1,   18,	/* <m_rtoot>1 rotate y */
	    1,   19,	/* <m_rtoot>1 rotate z */
};
AnimeRecord animMarioHoldLanding = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	 2,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_carry_landing_prm,
	mario_carry_landing_tbl
};
