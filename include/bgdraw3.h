/********************************************************************************
						Ultra 64 MARIO Brothers

					   Header file of "bgdraw3.c".

			Copyright 1995 Nintendo co., ltd.  All rights reserved

				  This module was programmed by Y.Tanimoto

								1995.12.6
*********************************************************************************/

#ifndef	_BGDRAW3_HEAD
#define	_BGDRAW3_HEAD

extern Gfx* BGDrawFunc(char, char, float, float, float, float, float, float, float);

typedef struct	{
	ushort*	 tile_number[80];
} BGTextureRec ;


#endif
