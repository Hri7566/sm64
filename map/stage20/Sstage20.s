/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 20 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage20


/* ====================================================================================	
		: Stage 20 [ Ext9 ] Enemies sequence.
=======================================================================================	*/
SEQ_Stage20_Enemies:

		seqActor(S_NULL,  0,-1000,0,  0,0,0,  0,0,0,	e_sakanamother 	)
		seqActor(S_NULL,  0,-1000,0,  0,0,0,  0,2,0,	e_sakanamother 	)

		seqReturn


/* ====================================================================================	
		: Stage 20 [ Ext9 ] Stars sequence.
=======================================================================================	*/
SEQ_Stage20_Stars:

		seqActor(S_NULL, 0, -4250, 0,  0,0,0,  0,0,0,  e_tripstar_getcoins) /* 8 coins  */

		seqReturn


/* ====================================================================================	
		: Stage 20 [ Ext9 ] main sequence.
=======================================================================================	*/
SEQ_DoStage20:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage20SegmentRomStart		 , _GfxStage20SegmentRomEnd			)
	seqLoadText(SEGMENT_TEXTURE	  , _LCastleOutTextureSegmentRomStart, _LCastleOutTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_BACK	  , _BackMountainSegmentRomStart	 , _BackMountainSegmentRomEnd 		)
	seqLoadPres(SEGMENT_WEATHER	  , _WeatherSegmentRomStart			 , _WeatherSegmentRomEnd			)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_dSegmentRomStart	  	 , _GfxEnemy1_dSegmentRomEnd		)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_dSegmentRomStart	  	 , _HmsEnemy1_dSegmentRomEnd		)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_dSegmentRomStart	  	 , _GfxEnemy2_dSegmentRomEnd		)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_dSegmentRomStart	  	 , _HmsEnemy2_dSegmentRomEnd		)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetEnemy1_d)
		seqCall(SetEnemy2_d)

		seqBeginScene(1, RCP_Stage20Scene1)
			seqActor(S_NULL,  0,-1535,0,  0,90,0,  0,10,0,  e_player_swimming)
			seqPort(10, 20, 1, 10)					/*	MARIO stage in.	*/
			seqGameClear(6, 1, 39)
			seqGameOver (6, 1, 40)
			seqCall(SEQ_Stage20_Enemies)
			seqCall(SEQ_Stage20_Stars)
			seqMapInfo(cx2001_info)
			seqTagInfo(cx2001_info_tag)
			seqSetMusic(NA_STG_WATER, NA_SUISOU_BGM)
			seqEnvironment(ENV_WATER)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 90,  0,-1535,0)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
