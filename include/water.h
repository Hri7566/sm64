/********************************************************************************
						Ultra 64 MARIO Brothers

			  Prototype define used for water surface drawing.

			Copyright 1995 Nintendo co., ltd.  All rights reserved

				This module was programmed by Y.Tanimoto

							1996.4.10
*********************************************************************************/

#ifndef	_WATER_PROTFUNC
#define	_WATER_PROTFUNC


typedef	struct	{
	short	surface_code ;
	short*	surface_data ;
} WaterSurfRec ;

extern ulong WaterInit(int, MapNode*, void*);

extern ulong WaterDraw(int, MapNode*, void*);

extern ulong WaterFall(int, MapNode*, void*);
extern ulong WaterFall_L(int, MapNode*, void*);

extern ulong SandConeInit(int, MapNode*, void*);
extern ulong SandCone(int, MapNode*, void*);


#endif
