HRCH: Softimage 4D Creative Environment v3.00


model
{
  name         "chn8"
  scaling      1.000 1.000 1.000
  rotation     0.000 0.000 0.000
  translation  0.000 0.000 0.000

  actor_data
    articulated_chain    chain_root 
    envelope_type        0

    inverse_kinematics 
      ik_type          10 
      ik_nbjnts        1 
      ik_pref_angles   0 0 0 


  model
  {
    name         "jnt8_1"
    scaling      1.000 1.000 1.000
    rotation     0.000 0.000 90.000
    translation  0.000 0.000 0.000

    actor_data
      articulated_chain    joint 
      envelope_type        1

      inverse_kinematics 
        ik_type          0 
        ik_nbjnts        0 
        ik_pref_angles   0 0 1.5708 


    model
    {
      name         "eff8"
      scaling      1.000 1.000 1.000
      rotation     0.000 0.000 0.000
      translation  45.000 0.000 0.000

      actor_data
        articulated_chain    chain_leaf 
        envelope_type        0

        inverse_kinematics 
          ik_type          0 
          ik_nbjnts        0 
          ik_pref_angles   0 0 0 

    }

    model
    {
      name         "chn7"
      scaling      1.000 1.000 1.000
      rotation     90.000 0.000 -90.000
      translation  51.000 0.000 0.000

      actor_data
        articulated_chain    chain_root 
        envelope_type        0

        inverse_kinematics 
          ik_type          30 
          ik_nbjnts        5 
          ik_pref_angles   0 0 0 


      model
      {
        name         "jnt7_1"
        scaling      1.000 1.000 1.000
        rotation     0.000 0.000 -4.452
        translation  0.000 0.000 0.000

        actor_data
          articulated_chain    joint 
          envelope_type        1

          inverse_kinematics 
            ik_type          0 
            ik_nbjnts        0 
            ik_pref_angles   0 0 -0.05 


        model
        {
          name         "jnt7_2"
          scaling      1.000 1.000 1.000
          rotation     0.000 0.000 10.580
          translation  11.500 0.000 0.000

          actor_data
            articulated_chain    joint 
            envelope_type        1

            inverse_kinematics 
              ik_type          0 
              ik_nbjnts        0 
              ik_pref_angles   0 0 -0.05 


          model
          {
            name         "jnt7_3"
            scaling      1.000 1.000 1.000
            rotation     0.000 0.000 18.131
            translation  11.500 0.000 0.000

            actor_data
              articulated_chain    joint 
              envelope_type        1

              inverse_kinematics 
                ik_type          0 
                ik_nbjnts        0 
                ik_pref_angles   0 0 -0.05 


            model
            {
              name         "jnt7_4"
              scaling      1.000 1.000 1.000
              rotation     0.000 0.000 -1.034
              translation  8.600 0.000 0.000

              actor_data
                articulated_chain    joint 
                envelope_type        1

                inverse_kinematics 
                  ik_type          0 
                  ik_nbjnts        0 
                  ik_pref_angles   0 0 -0.05 


              model
              {
                name         "jnt7_5"
                scaling      1.000 1.000 1.000
                rotation     0.000 0.000 -4.464
                translation  7.200 0.000 0.000

                actor_data
                  articulated_chain    joint 
                  envelope_type        1

                  inverse_kinematics 
                    ik_type          0 
                    ik_nbjnts        0 
                    ik_pref_angles   0 0 -0.05 


                model
                {
                  name         "eff7"
                  scaling      1.000 1.000 1.000
                  rotation     0.000 0.000 0.000
                  translation  7.200 0.000 0.000

                  actor_data
                    articulated_chain    chain_leaf 
                    envelope_type        0

                    inverse_kinematics 
                      ik_type          0 
                      ik_nbjnts        0 
                      ik_pref_angles   0 0 0 

                }

                model
                {
                  name         "face11"
                  scaling      1.000 1.000 1.000
                  rotation     0.000 0.000 0.000
                  translation  0.000 0.000 0.000

                  mesh
                  {
                    flag    ( PROCESS AVERAGED )
                    discontinuity  90.000

                    vertices   3
                    {
                      [0] position  7.200  0.000  0.000
                      [1] position  0.000  0.000  2.800
                      [2] position  0.000  0.000  -2.800
                    }

                    polygons   1
                    {
                      [0] nodes  3
                          {
                            [0] vertex  1
                                normal  0.000  1.000  0.000
                                uvTexture  0.000  0.000
                            [1] vertex  0
                                normal  0.000  1.000  0.000
                                uvTexture  0.000  0.000
                            [2] vertex  2
                                normal  0.000  1.000  0.000
                                uvTexture  0.000  0.000
                          }
                          material  0
                    }

                    edges   3
                    {
                      [1] vertices  0  2
                      [2] vertices  2  1
                      [3] vertices  1  0
                    }
                  }

                  material     [0]
                  {
                    name          "green1"
                    type          LAMBERT
                    ambient       0.250  0.250  0.250
                    diffuse       0.000  0.435  0.000
                    specular      1.000  1.000  1.000
                    exponent      50.000
                    reflectivity  0.000
                    transparency  0.000
                    refracIndex   1.000
                    glow   0
                    coc   0.000
                  }
                }
              }

              model
              {
                name         "face10"
                scaling      1.000 1.000 1.000
                rotation     0.000 0.000 0.000
                translation  0.000 0.000 0.000

                mesh
                {
                  flag    ( PROCESS AVERAGED )
                  discontinuity  90.000

                  vertices   4
                  {
                    [0] position  0.000  0.000  5.600
                    [1] position  0.000  0.000  -5.600
                    [2] position  7.200  0.000  2.800
                    [3] position  7.200  0.000  -2.800
                  }

                  polygons   2
                  {
                    [0] nodes  3
                        {
                          [0] vertex  0
                              normal  0.000  1.000  0.000
                              uvTexture  0.000  0.000
                          [1] vertex  2
                              normal  0.000  1.000  0.000
                              uvTexture  0.000  0.000
                          [2] vertex  3
                              normal  0.000  1.000  0.000
                              uvTexture  0.000  0.000
                        }
                        material  0
                    [1] nodes  3
                        {
                          [0] vertex  0
                              normal  0.000  1.000  0.000
                              uvTexture  0.000  0.000
                          [1] vertex  3
                              normal  0.000  1.000  0.000
                              uvTexture  0.000  0.000
                          [2] vertex  1
                              normal  0.000  1.000  0.000
                              uvTexture  0.000  0.000
                        }
                        material  0
                  }

                  edges   5
                  {
                    [1] vertices  2  3
                    [2] vertices  3  0
                        flag    ( HIDDEN )
                    [3] vertices  0  2
                    [4] vertices  3  1
                    [5] vertices  1  0
                  }
                }

                material     [0]
                {
                  name          "green2"
                  type          LAMBERT
                  ambient       0.250  0.250  0.250
                  diffuse       0.000  0.435  0.000
                  specular      1.000  1.000  1.000
                  exponent      50.000
                  reflectivity  0.000
                  transparency  0.000
                  refracIndex   1.000
                  glow   0
                  coc   0.000
                }
              }
            }

            model
            {
              name         "face9"
              scaling      1.000 1.000 1.000
              rotation     0.000 0.000 0.000
              translation  0.000 0.000 0.000

              mesh
              {
                flag    ( PROCESS AVERAGED )
                discontinuity  90.000

                vertices   4
                {
                  [0] position  0.000  0.000  -9.000
                  [1] position  0.000  0.000  9.000
                  [2] position  8.600  0.000  5.600
                  [3] position  8.600  0.000  -5.600
                }

                polygons   2
                {
                  [0] nodes  3
                      {
                        [0] vertex  1
                            normal  0.000  1.000  0.000
                            uvTexture  0.000  0.000
                        [1] vertex  2
                            normal  0.000  1.000  0.000
                            uvTexture  0.000  0.000
                        [2] vertex  3
                            normal  0.000  1.000  0.000
                            uvTexture  0.000  0.000
                      }
                      material  0
                  [1] nodes  3
                      {
                        [0] vertex  1
                            normal  0.000  1.000  0.000
                            uvTexture  0.000  0.000
                        [1] vertex  3
                            normal  0.000  1.000  0.000
                            uvTexture  0.000  0.000
                        [2] vertex  0
                            normal  0.000  1.000  0.000
                            uvTexture  0.000  0.000
                      }
                      material  0
                }

                edges   5
                {
                  [1] vertices  2  3
                  [2] vertices  3  1
                      flag    ( HIDDEN )
                  [3] vertices  1  2
                  [4] vertices  3  0
                  [5] vertices  0  1
                }
              }

              material     [0]
              {
                name          "green3"
                type          LAMBERT
                ambient       0.250  0.250  0.250
                diffuse       0.000  0.435  0.000
                specular      1.000  1.000  1.000
                exponent      50.000
                reflectivity  0.000
                transparency  0.000
                refracIndex   1.000
                glow   0
                coc   0.000
              }
            }
          }

          model
          {
            name         "face8"
            scaling      1.000 1.000 1.000
            rotation     0.000 0.000 0.000
            translation  0.000 0.000 0.000

            mesh
            {
              flag    ( PROCESS AVERAGED )
              discontinuity  90.000

              vertices   4
              {
                [0] position  11.500  0.000  -9.000
                [1] position  11.500  0.000  9.000
                [2] position  0.000  0.000  13.500
                [3] position  0.000  0.000  -13.500
              }

              polygons   2
              {
                [0] nodes  3
                    {
                      [0] vertex  2
                          normal  0.000  1.000  0.000
                          uvTexture  0.000  0.000
                      [1] vertex  1
                          normal  0.000  1.000  0.000
                          uvTexture  0.000  0.000
                      [2] vertex  0
                          normal  0.000  1.000  0.000
                          uvTexture  0.000  0.000
                    }
                    material  0
                [1] nodes  3
                    {
                      [0] vertex  2
                          normal  0.000  1.000  0.000
                          uvTexture  0.000  0.000
                      [1] vertex  0
                          normal  0.000  1.000  0.000
                          uvTexture  0.000  0.000
                      [2] vertex  3
                          normal  0.000  1.000  0.000
                          uvTexture  0.000  0.000
                    }
                    material  0
              }

              edges   5
              {
                [1] vertices  1  0
                [2] vertices  0  2
                    flag    ( HIDDEN )
                [3] vertices  2  1
                [4] vertices  0  3
                [5] vertices  3  2
              }
            }

            material     [0]
            {
              name          "green4"
              type          LAMBERT
              ambient       0.250  0.250  0.250
              diffuse       0.000  0.435  0.000
              specular      1.000  1.000  1.000
              exponent      50.000
              reflectivity  0.000
              transparency  0.000
              refracIndex   1.000
              glow   0
              coc   0.000
            }
          }
        }

        model
        {
          name         "face7"
          scaling      1.000 1.000 1.000
          rotation     0.000 0.000 0.000
          translation  0.000 0.000 0.000

          mesh
          {
            flag    ( PROCESS AVERAGED )
            discontinuity  90.000

            vertices   5
            {
              [0] position  0.001  0.000  -17.997
              [1] position  0.001  0.000  18.003
              [2] position  11.500  0.000  13.500
              [3] position  11.500  0.000  -13.500
              [4] position  0.001  0.000  0.003
            }

            polygons   3
            {
              [0] nodes  3
                  {
                    [0] vertex  3
                        normal  0.000  1.000  0.000
                        uvTexture  0.000  0.000
                    [1] vertex  0
                        normal  0.000  1.000  0.000
                        uvTexture  0.000  0.000
                    [2] vertex  4
                        normal  0.000  1.000  0.000
                        uvTexture  0.000  0.000
                  }
                  material  0
              [1] nodes  3
                  {
                    [0] vertex  4
                        normal  0.000  1.000  0.000
                        uvTexture  0.000  0.000
                    [1] vertex  1
                        normal  0.000  1.000  0.000
                        uvTexture  0.000  0.000
                    [2] vertex  2
                        normal  0.000  1.000  0.000
                        uvTexture  0.000  0.000
                  }
                  material  0
              [2] nodes  3
                  {
                    [0] vertex  2
                        normal  0.000  1.000  0.000
                        uvTexture  0.000  0.000
                    [1] vertex  3
                        normal  0.000  1.000  0.000
                        uvTexture  0.000  0.000
                    [2] vertex  4
                        normal  0.000  1.000  0.000
                        uvTexture  0.000  0.000
                  }
                  material  1
            }

            edges   7
            {
              [1] vertices  0  4
              [2] vertices  4  3
              [3] vertices  3  0
              [4] vertices  1  2
              [5] vertices  2  4
              [6] vertices  4  1
              [7] vertices  2  3
            }
          }

          material     [0]
          {
            name          "mat1"
            type          LAMBERT
            ambient       0.250  0.250  0.250
            diffuse       1.000  0.904  0.345
            specular      0.000  0.000  0.000
            exponent      50.000
            reflectivity  0.000
            transparency  0.000
            refracIndex   1.000
            glow   0
            coc   0.000
          }

          material     [1]
          {
            name          "green5"
            type          LAMBERT
            ambient       0.250  0.250  0.250
            diffuse       0.000  0.435  0.000
            specular      1.000  1.000  1.000
            exponent      50.000
            reflectivity  0.000
            transparency  0.000
            refracIndex   1.000
            glow   0
            coc   0.000
          }
        }
      }
    }

    model
    {
      name         "cyl9"
      scaling      1.000 1.000 1.000
      rotation     0.000 0.000 0.000
      translation  0.000 0.000 0.000

      mesh
      {
        flag    ( PROCESS AVERAGED )
        discontinuity  90.000

        vertices   10
        {
          [0] position  70.000  -0.464  1.427
          [1] position  0.000  -1.214  -0.882
          [2] position  70.000  -1.500  0.000
          [3] position  0.000  0.464  -1.427
          [4] position  70.000  -0.464  -1.427
          [5] position  0.000  1.500  0.000
          [6] position  70.000  1.214  -0.882
          [7] position  0.000  0.464  1.427
          [8] position  70.000  1.214  0.882
          [9] position  0.000  -1.214  0.882
        }

        polygons   13
        {
          [0] nodes  3
              {
                [0] vertex  7
                    normal  -0.002  0.309  0.951
                    uvTexture  0.000  0.000
                [1] vertex  9
                    normal  -0.002  -0.809  0.588
                    uvTexture  0.000  0.000
                [2] vertex  0
                    normal  0.754  -0.203  0.625
                    uvTexture  0.000  0.000
              }
              material  0
          [1] nodes  3
              {
                [0] vertex  1
                    normal  -0.002  -0.809  -0.588
                    uvTexture  0.000  0.000
                [1] vertex  3
                    normal  -0.002  0.309  -0.951
                    uvTexture  0.000  0.000
                [2] vertex  4
                    normal  0.608  -0.245  -0.755
                    uvTexture  0.000  0.000
              }
              material  0
          [2] nodes  3
              {
                [0] vertex  5
                    normal  -0.002  1.000  0.000
                    uvTexture  0.000  0.000
                [1] vertex  7
                    normal  -0.002  0.309  0.951
                    uvTexture  0.000  0.000
                [2] vertex  8
                    normal  0.358  0.755  0.549
                    uvTexture  0.000  0.000
              }
              material  0
          [3] nodes  3
              {
                [0] vertex  9
                    normal  -0.002  -0.809  0.588
                    uvTexture  0.000  0.000
                [1] vertex  1
                    normal  -0.002  -0.809  -0.588
                    uvTexture  0.000  0.000
                [2] vertex  2
                    normal  0.358  -0.934  0.000
                    uvTexture  0.000  0.000
              }
              material  0
          [4] nodes  3
              {
                [0] vertex  3
                    normal  -0.002  0.309  -0.951
                    uvTexture  0.000  0.000
                [1] vertex  5
                    normal  -0.002  1.000  0.000
                    uvTexture  0.000  0.000
                [2] vertex  6
                    normal  0.608  0.642  -0.467
                    uvTexture  0.000  0.000
              }
              material  0
          [5] nodes  3
              {
                [0] vertex  6
                    normal  0.608  0.642  -0.467
                    uvTexture  0.000  0.000
                [1] vertex  8
                    normal  0.358  0.755  0.549
                    uvTexture  0.000  0.000
                [2] vertex  0
                    normal  0.754  -0.203  0.625
                    uvTexture  0.000  0.000
              }
              material  0
          [6] nodes  3
              {
                [0] vertex  6
                    normal  0.608  0.642  -0.467
                    uvTexture  0.000  0.000
                [1] vertex  0
                    normal  0.754  -0.203  0.625
                    uvTexture  0.000  0.000
                [2] vertex  4
                    normal  0.608  -0.245  -0.755
                    uvTexture  0.000  0.000
              }
              material  0
          [7] nodes  3
              {
                [0] vertex  0
                    normal  0.754  -0.203  0.625
                    uvTexture  0.000  0.000
                [1] vertex  2
                    normal  0.358  -0.934  0.000
                    uvTexture  0.000  0.000
                [2] vertex  4
                    normal  0.608  -0.245  -0.755
                    uvTexture  0.000  0.000
              }
              material  0
          [8] nodes  3
              {
                [0] vertex  4
                    normal  0.608  -0.245  -0.755
                    uvTexture  0.000  0.000
                [1] vertex  2
                    normal  0.358  -0.934  0.000
                    uvTexture  0.000  0.000
                [2] vertex  1
                    normal  -0.002  -0.809  -0.588
                    uvTexture  0.000  0.000
              }
              material  0
          [9] nodes  3
              {
                [0] vertex  6
                    normal  0.608  0.642  -0.467
                    uvTexture  0.000  0.000
                [1] vertex  4
                    normal  0.608  -0.245  -0.755
                    uvTexture  0.000  0.000
                [2] vertex  3
                    normal  -0.002  0.309  -0.951
                    uvTexture  0.000  0.000
              }
              material  0
          [10] nodes  3
              {
                [0] vertex  8
                    normal  0.358  0.755  0.549
                    uvTexture  0.000  0.000
                [1] vertex  6
                    normal  0.608  0.642  -0.467
                    uvTexture  0.000  0.000
                [2] vertex  5
                    normal  -0.002  1.000  0.000
                    uvTexture  0.000  0.000
              }
              material  0
          [11] nodes  3
              {
                [0] vertex  0
                    normal  0.754  -0.203  0.625
                    uvTexture  0.000  0.000
                [1] vertex  8
                    normal  0.358  0.755  0.549
                    uvTexture  0.000  0.000
                [2] vertex  7
                    normal  -0.002  0.309  0.951
                    uvTexture  0.000  0.000
              }
              material  0
          [12] nodes  3
              {
                [0] vertex  2
                    normal  0.358  -0.934  0.000
                    uvTexture  0.000  0.000
                [1] vertex  0
                    normal  0.754  -0.203  0.625
                    uvTexture  0.000  0.000
                [2] vertex  9
                    normal  -0.002  -0.809  0.588
                    uvTexture  0.000  0.000
              }
              material  0
        }

        edges   22
        {
          [1] vertices  9  0
          [2] vertices  0  7
          [3] vertices  7  9
          [4] vertices  3  4
          [5] vertices  4  1
          [6] vertices  1  3
          [7] vertices  7  8
          [8] vertices  8  5
          [9] vertices  5  7
          [10] vertices  1  2
          [11] vertices  2  9
          [12] vertices  9  1
          [13] vertices  5  6
          [14] vertices  6  3
          [15] vertices  3  5
          [16] vertices  8  0
          [17] vertices  0  6
          [18] vertices  6  8
          [19] vertices  0  4
          [20] vertices  4  6
          [21] vertices  2  4
          [22] vertices  0  2
        }
      }

      material     [0]
      {
        name          "mat2"
        type          LAMBERT
        ambient       0.250  0.250  0.250
        diffuse       0.678  0.333  0.023
        specular      1.000  1.000  1.000
        exponent      50.000
        reflectivity  0.000
        transparency  0.000
        refracIndex   1.000
        glow   0
        coc   0.000
      }
    }
  }
}
