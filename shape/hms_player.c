/********************************************************************************
						Ultra 64 MARIO Brothers

					  player hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 12, 1995
 ********************************************************************************/

#include "../headers.h"

#include "Player/hms_effect.sou"
#include "Player/hms_mario.sou"
