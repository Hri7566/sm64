/********************************************************************************
	physics.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	March 26, 1996
 ********************************************************************************/

#ifndef PHYSICS_H
#define	PHYSICS_H


#define	PWALK_LANDING		0x00
#define	PWALK_RUNNING		0x01
#define	PWALK_CLASHED		0x02
#define	PWALK_WALL			0x03
#define	PWALK_FIREDMG		0x04

#define	PJUMP_JUMPING		0x00
#define	PJUMP_JUMPEND		0x01
#define	PJUMP_CLASHED		0x02
#define	PJUMP_HANGING		0x03
#define	PJUMP_HANGRUF		0x04
#define	PJUMP_FIREDMG		0x06

#define	PHANG_DISABLE		0x00
#define	PHANG_ENABLE		0x01
#define	PHANG_WALL			0x01
#define	PHANG_ROOF			0x02

typedef struct {
	float	pow, radius;
	float	posx, posz;
	float	spdx, spdz;
} BallRecord, *BallPtr;

extern BGCheckData skatingWaterSurface;

/********************************************************************************/
/*																				*/
/*	Define function prototypes.													*/
/*																				*/
/********************************************************************************/

extern void
DoTrampoline(void);

extern float
PL_GetTrampolinePower(void);

extern void
PL_CheckTrampolineJump(PlayerPtr player);

extern int
PL_CollideBalls(BallPtr pick, BallPtr ball);

extern int
PL_SetBallRecord(BallPtr ball, float posx, float posz, float speed, short angle, float pow, float radius);



extern void
PL_PlayerRefrection(PlayerPtr player, int negative);

extern int
PL_CheckPlayerSinking(PlayerPtr player, float power);

extern int
PL_FallFromHeavySlope(PlayerPtr player, ulong status, ulong auxinfo);

extern int
PL_QuicksandProcess(PlayerPtr player);

extern int
PL_GustProcess(PlayerPtr player);

extern void
PL_StopProcess(PlayerPtr player);

extern int
PL_WaitProcess(PlayerPtr player);

extern int
PL_WalkProcess(PlayerPtr player);

extern int
PL_JumpProcess(PlayerPtr player, int hangflag);

extern void
PL_Set3DSpeed(PlayerPtr player);

#endif
