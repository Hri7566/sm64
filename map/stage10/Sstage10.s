/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 10 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage10


/* ================================================================================
		: Scene 1 Stars sequence.
===================================================================================	*/
SEQ_Stage1001_Stars:

		seqLevelActor(0x003f,S_polystar,  700, 4500,   690, 0,0,0,  0,0,0,  e_tripstar)			 /* snowmans_head 			*/
/*		seqLevelActor(0x003f,S_polystar,  130, 1600, -4300, 0,0,0,  1,0,0,  e_tripstar) 		 /* big_motos		 		*/
		seqLevelActor(0x003f,S_polystar, 4350, 1350,  4350, 0,0,0,  2,0,0,  e_tripstar)    		 /*	in_the_ice_tower		*/
/*		seqLevelActor(0x003f,S_polystar,-4700, 1400,  5850, 0,0,0,  3,0,0,  e_tripstar) 		 /* fura fura de		 	*/
		seqLevelActor(0x003f,S_NULL    , 5000, 1200,     0, 0,0,0,  4,0,0,  e_tripstar_getcoins) /* 8_coin 					*/

		seqReturn


/* ================================================================================
		: Scene 1 Move BGs sequence.
===================================================================================	*/
SEQ_Stage1001_MoveBGs:

		seqActor(S_NULL,  	   977,1024,2075,  0,0,0,  0,0,0,  e_iwanami)
/*		seqActor(S_movebg02,  4377,1843,4361,  0,0,0,  0,0,0,  e_ice)		*/

		seqReturn


/* ================================================================================
		: Scene 1 Enemies sequence.
===================================================================================	*/
SEQ_Stage1001_Enemies:

		seqActor(S_ping,  	 	   1715,3328,  518,  0,-51,0,   0,0,0,   e_wind_ping	)
		seqActor(S_NULL,   	 	    700,3428,  700,  0, 30,0,   0,0,0,   e_wind			)
		seqActor(S_NULL,   	 	    480,2300, 1370,  0,  0,0,   0,0,0,   e_kamakura		)
		seqActor(S_Big_ice_otos,  	315,1331,-4852,  0,  0,0,   1,0,0,   e_big_ice_otos	)
		seqActor(S_snowman,  		2954,970,  750,  0,  0,0,   0,2,0,   e_snowman		)

		seqReturn


/* ================================================================================
		: Scene 2 Stars sequence.
===================================================================================	*/
SEQ_Stage1002_Stars:

		seqLevelActor(0x003f,S_polystar,   0,  500,   1000, 0,0,0,  5,0,0,  e_tripstar) 		 /* in kamakura 			*/

		seqReturn


/* ================================================================================
		: Stage 10 main sequence.
===================================================================================	*/
SEQ_DoStage10:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage10SegmentRomStart	  , _GfxStage10SegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE	  , _FSnowMtTextureSegmentRomStart, _FSnowMtTextureSegmentRomEnd)
	seqLoadPres(SEGMENT_WEATHER	  , _WeatherSegmentRomStart		  , _WeatherSegmentRomEnd		)
	seqLoadPres(SEGMENT_BACK   	  , _BackSnowtowerSegmentRomStart , _BackSnowtowerSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_gSegmentRomStart	  , _GfxEnemy1_gSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_gSegmentRomStart	  , _HmsEnemy1_gSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_gSegmentRomStart	  , _GfxEnemy2_gSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_gSegmentRomStart	  , _HmsEnemy2_gSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_g)
		seqCall(SetEnemy2_g)

		seqHmsShape(S_movebg01, RCP_HmsSf2Nami	)
		seqHmsShape(S_movebg02, RCP_HmsSf2Hip	)
		seqHmsShape(S_movebg03, RCP_HmsSf2Hahen	)

		seqHmsShape(S_tree3	  , RCP_HmsTree03 	)

		seqBeginScene(1, RCP_Stage10Scene1)
			seqActor(S_NULL,   5541,1024+START_H,  443,  0,270,0,  0,10,0,  e_player_entpict)
			seqActor(S_NULL,    257,		2150, 1399,  0,290,0,  0,11,0,  e_player_waiting)
			seqActor(S_NULL,    569,		2150, 1336,  0,  0,0,  6,12,0,  e_tripchimney2	)
			seqActor(S_NULL,   5468,	    1056,-5400,  0,-20,0,  0,13,0,  e_warp)
			seqActor(S_NULL,  -3698,		1024,-1237,  0,  6,0,  0,14,0,  e_warp)
			seqPort(10, 10, 1, 10)				/*	MARIO stage in.					*/
			seqPort(11, 10, 1, 11)				/*	From the scene 2 [ Kamakura ].	*/
			seqPort(12, 10, 2, 10)				/*	To   the scene 2 [ Kamakura ].	*/
			seqPort(13, 10, 1, 14)				/*	Warp.							*/
			seqPort(14, 10, 1, 13)				/*	Warp.							*/
			seqCall(SEQ_Stage1001_Stars)
			seqCall(SEQ_Stage1001_MoveBGs)
			seqCall(SEQ_Stage1001_Enemies)
			seqGameClear(6, 2,  54)
			seqGameOver (6, 2, 104)
			seqMapInfo(cx1001_info)
			seqTagInfo(cx1001_info_tag)
			seqSetMusic(NA_STG_MOUNTAIN, NA_SNOW_BGM)
			seqEnvironment(ENV_SNOWMT)
		seqEndScene()

		seqBeginScene(2, RCP_Stage10Scene2)
			seqActor(S_NULL,   0,0,2867,  0,180,0,    0,10,0,  e_player_waiting	)
			seqActor(S_NULL,   0,0,3277,  0,  0,0,   20,11,0,  e_tripchimney2	)
			seqPort(10, 10, 2, 10)
			seqPort(11, 10, 1, 11)
			seqCall(SEQ_Stage1002_Stars)
			seqGameClear(6, 2,  54)
			seqGameOver (6, 2, 104)
			seqMapInfo(cx1002_info)
			seqTagInfo(cx1002_info_tag)
			seqSetMusic(NA_STG_DUNGEON, NA_DUNGEON_BGM)
			seqEnvironment(ENV_SNOWMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 270,  5541,1024,443)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
