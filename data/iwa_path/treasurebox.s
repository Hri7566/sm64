/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Strategy

					April 4 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/*==============================================================================
						Path Data
 ===============================================================================*/

e_tbox_quize:
	p_initialize(option)
	p_setbit(flag,stf_moveON)
	p_BGcheckYset
	p_program(s_tbox_quize_init)

	p_while
		p_program(s_tbox_quize_main)
	p_loop


e_tbox_quize_ground:
	p_initialize(option)
	p_setbit(flag,stf_moveON)
	p_BGcheckYset
	p_program(s_tbox_quize_ground_init)

	p_while
		p_program(s_tbox_quize_ground_main)
	p_loop

e_tbox_quize23:
	p_initialize(option)
	p_setbit(flag,stf_moveON)
	p_BGcheckYset
	p_program(s_tbox_quize23_init)

	p_while
		p_program(s_tbox_quize23_main)
	p_loop

e_trebox:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON)
	p_BGcheckYset
	p_program(s_trebox_init)
	p_hitOFF

	p_while
		p_program(s_trebox_main)
	p_loop

e_trebox_futa:
	p_initialize(option)
	p_setbit(flag,stf_moveON)
	p_save_nowpos

	p_while
		p_program(s_trebox_futa_main)
	p_loop

/*################*/
#else
/*################*/


#if 0
 s_makeobj_absolute(StrategyRecord 
	*mother,					execstp
	short name,					0
	int shape,					S_xxx
	unsigned long *pathpointer,	e_xxx
	short wx,short wy,short wz,	
	short ax,short ay,short az


extern	StrategyRecord *s_makeobj_chain(
	short mode,					0    ... programselect 
	short ox,					0	
	short oy,					30
	short oz,					100
	StrategyRecord *mother,		execstp
	int shape,					S_xxx
	unsigned long *pathpointer)	e_Xxx

#endif

/*==================================================================================================
		C Program
 ===================================================================================================*/
#define TRE_INWATER		0
#define TRE_ONGROUND	1


/*==================================================================================================
		Treasure Box 
 ===================================================================================================*/
static HitCheckRecord treasure_hit = {
	OBJNAME_BIRIBIRI,
	 0,1,0,0,			/* flag,ap,hp,coin 		 */
	 300,   300,		/* player attack hitarea */
	 300+10,300+10,		/* player damage hitarea */
};

/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_trebox_futa_main(void)
{

	StrategyRecord* mainp = motherp->motherobj;

	switch (obj_mode){
		case 0:	if ( mother_mode == 1 )	obj_mode = 1;
				break;

		case 1:
				if ( obj_timer == 0 ){
					
					if (mainp->s[stw_work2].d == TRE_INWATER) {
						s_makeobj_chain(0,0,-80,120,execstp,S_bubble,e_random_bubble);
						AudStartSound(execstp,NA_SE2_BOXOPEN+NA_IN_WATER);
					}
					else {
						AudStartSound(execstp,NA_SE2_BOXOPEN);
					}
				}
				obj_animeangleX -= 0x200;
				if ( obj_animeangleX < -0x4000)	{
					 obj_animeangleX = -0x4000;
					 obj_mode++;
					 if (motherp->s[stw_programselect].d != 4) AppNumber( (char) (motherp->s[stw_programselect].d),0,-40,0 );
				}
				break;

		case 2: if ( mother_mode == 0 )	obj_mode = 3;
				break;		

		case 3:	obj_animeangleX += 0x800;
				if ( obj_animeangleX > 0)	{
					 obj_animeangleX = 0;
					 obj_mode = 0;
				}
	}

}
/*==================================================================================================
		Treasure Box 
 ===================================================================================================*/

/***************************************************************************************************
						   Init
***************************************************************************************************/
extern void s_trebox_init(void)
{

	s_makeobj_chain(0,0,102,-77,execstp,S_t_box2,e_trebox_futa);
	
	s_set_hitparam(execstp,&treasure_hit);		/* Set HitCheck Param */

}

/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_trebox_main(void)
{

	switch (obj_mode){
		case 0 :	if (ShapeSameAngle(obj_angleY,player_angleY+0x8000,0x3000) && PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,150) && mother_work1 == 0) {
						if (mother_work0 == obj_programselect) {
							Na_FixSeFlagEntry(NA_SE2_CORRECT_CHIME);
							mother_work0++;
							obj_mode = 1;
						}
						else	{
							mother_work0 = 1;		// junban
							mother_work1 = 1;		//Danger Flag
							obj_mode = 2;
							s_hitON();
							Na_FixSeFlagEntry(NA_SYS_BUZZER);
						}
					}
					break;

		case 1 :	if (mother_work1 == 1) {
						obj_mode = 0;
					}
					break;

		case 2:		s_hitOFF();
					if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,500)==0) {
						mother_work1 = 0;
						obj_mode = 0;
					} 

	}

	s_player_slideout_RH(150,150);

	obj_mail = 0;

}
















/*==================================================================================================
		Treasure Box Quiz (In Water)
 ===================================================================================================*/
#define	treasure_boxNo		(execstp->s[stw_work0].d)
#define	treasure_dame		(execstp->s[stw_work1].d)
#define	treasure_attFlag	(execstp->s[stw_work2].d)

/***************************************************************************************************
						   Make Treasure Box
***************************************************************************************************/
extern void tBoxMake(char mode, int posx, int posy, int posz, short angy)
{
	StrategyRecord* stp = s_makeobj_absolute(execstp,0,S_t_box1,e_trebox,posx,posy,posz,0,angy,0);

	stp->s[stw_programselect].d = mode;

}
/***************************************************************************************************
						   Init
***************************************************************************************************/
extern void s_tbox_quize_init(void)
{

	tBoxMake(1, 400,-350,-2700,0);
	tBoxMake(2,650,-350,  -940,degree(225));
	tBoxMake(3,-550,-350, -770,degree(135));
	tBoxMake(4, 100,-350,-1700,0);

	treasure_boxNo = 1;

	treasure_attFlag = TRE_INWATER;
}


/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_tbox_quize_main(void)
{

	switch(obj_mode) {
		case 0: if (treasure_boxNo == 5) {
					Na_NazoClearBgm();
					Na_WaterBgmFull();
					obj_mode = 1;
				}
				break;

		case 1:	if (waterline != NULL) {
					*(waterline+6*1) -= 5;
					Na_FixSeFlagEntry(NA_LSE2_WATERLEVEL_DOWN);
					Viewshake(VS_CHINBOTSU);
					if (*(waterline+6*1) < -335) { 
						*(waterline+6*1) = -335;	
						execstp->status = 0;
					}
					SendMotorVib(2);		/* MOTOR 1997.5.30 */
				}
				break;
	}

}


/*==================================================================================================
		Treasure Box Quiz (On the Ground)
 ===================================================================================================*/

/***************************************************************************************************
						   Init
***************************************************************************************************/
extern void s_tbox_quize_ground_init(void)
{

	tBoxMake(1,-1700,-2812,-1150,degree(180));
	tBoxMake(2,-1150,-2812,-1550,degree(180));
	tBoxMake(3,-2400,-2812,-1800,degree(180));
	tBoxMake(4,-1800,-2812,-2100,degree(180));

	treasure_boxNo = 1;

	treasure_attFlag = TRE_ONGROUND;
}


/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_tbox_quize_ground_main(void)
{

	switch (obj_mode) {
		case 0: if (treasure_boxNo == 5) {
					Na_NazoClearBgm();
					obj_mode = 1;
				} 
				break;

		case  1: if (obj_timer == FRAME*2) {
					s_kemuri();
					s_enemyset_star(-1800,-2500,-1700);
					obj_mode = 2;
				 }
				break;
		case 2: break;
	}

}

/*==================================================================================================
		Treasure Box Quiz (stage23)
 ===================================================================================================*/

/***************************************************************************************************
						   Init
***************************************************************************************************/
extern void s_tbox_quize23_init(void)
{

	tBoxMake(1,-4500,-5119, 1300,degree(225));
	tBoxMake(2,-1800,-5119, 1050,degree( 45));
	tBoxMake(3,-4500,-5119,-1100,degree( 50));
	tBoxMake(4,-2400,-4607,  125,degree( 88));

	treasure_boxNo = 1;

	treasure_attFlag = TRE_INWATER;
}


/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_tbox_quize23_main(void)
{

	switch (obj_mode) {
		case 0: if (treasure_boxNo == 5) {
					Na_NazoClearBgm();
					obj_mode = 1;
				} 
				break;

		case 1: if (obj_timer == FRAME*2) {
					s_kemuri();
					s_enemyset_star(-1900,-4000,-1400);
					obj_mode = 2;
				}
				 break;
		case 2:  break;
	}

}




/*################*/
#endif
/*################*/


