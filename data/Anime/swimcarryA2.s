/********************************************************************************
						Ultra 64 MARIO Brothers

					mario swim carry A animation data 2

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							February 1, 1996
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioSwimCarryA2:
	.half	 MAP_ANIM_ONETIME 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	10 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x029A						/* total bytes       */


	.half	    1,    0 	/* chn14translate x */
	.half	    1,    1 	/* chn14 translate y */
	.half	    1,    0 	/* chn14 translate z */
	.half	    1,    0 	/* chn14 rotate x */
	.half	    1,    2 	/* chn14 rotate y */
	.half	    1,    0 	/* chn14 rotate z */
	.half	    1,    0 	/* <m_waist>1_3 rotate x */
	.half	    1,    0 	/* <m_waist>1_3 rotate y */
	.half	   10,  185 	/* <m_waist>1_3 rotate z */
	.half	    1,    0 	/* <m_body>1 rotate x */
	.half	    1,    0 	/* <m_body>1 rotate y */
	.half	   10,  175 	/* <m_body>1 rotate z */
	.half	    1,    0 	/* <m_head>2 rotate x */
	.half	    1,    0 	/* <m_head>2 rotate y */
	.half	   10,  165 	/* <m_head>2 rotate z */
	.half	    1,  161 	/* chn6 rotate x */
	.half	    1,  162 	/* chn6 rotate y */
	.half	    1,  163 	/* chn6 rotate z */
	.half	    1,    7 	/* <m_larmA>1 rotate x */
	.half	    1,    8 	/* <m_larmA>1 rotate y */
	.half	    1,    9 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1 rotate x */
	.half	    1,    0 	/* <m_larmB>1 rotate y */
	.half	    1,    6 	/* <m_larmB>1 rotate z */
	.half	    1,    3 	/* <m_rhand2>2 rotate x */
	.half	    1,    4 	/* <m_rhand2>2 rotate y */
	.half	    1,    5 	/* <m_rhand2>2 rotate z */
	.half	    1,  157 	/* chn10 rotate x */
	.half	    1,  158 	/* chn10 rotate y */
	.half	    1,  159 	/* chn10 rotate z */
	.half	    1,   14 	/* <m_rarmA>1 rotate x */
	.half	    1,   15 	/* <m_rarmA>1 rotate y */
	.half	    1,   16 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1 rotate y */
	.half	    1,   13 	/* <m_rarmB>1 rotate z */
	.half	    1,   10 	/* <m_rhand2>1 rotate x */
	.half	    1,   11 	/* <m_rhand2>1 rotate y */
	.half	    1,   12 	/* <m_rhand2>1 rotate z */
	.half	    1,    0 	/* chn15 rotate x */
	.half	    1,    0 	/* chn15 rotate y */
	.half	    1,  160 	/* chn15 rotate z */
	.half	   10,  127 	/* <m_llegA>1 rotate x */
	.half	   10,  137 	/* <m_llegA>1 rotate y */
	.half	   10,  147 	/* <m_llegA>1 rotate z */
	.half	    1,    0 	/* <m_llegB>1 rotate x */
	.half	    1,    0 	/* <m_llegB>1 rotate y */
	.half	   10,  117 	/* <m_llegB>1 rotate z */
	.half	   10,   87 	/* <m_ltoot>1 rotate x */
	.half	   10,   97 	/* <m_ltoot>1 rotate y */
	.half	   10,  107 	/* <m_ltoot>1 rotate z */
	.half	    1,    0 	/* chn17 rotate x */
	.half	    1,    0 	/* chn17 rotate y */
	.half	    1,  164 	/* chn17 rotate z */
	.half	   10,   57 	/* <m_rlegA>1 rotate x */
	.half	   10,   67 	/* <m_rlegA>1 rotate y */
	.half	   10,   77 	/* <m_rlegA>1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1 rotate y */
	.half	   10,   47 	/* <m_rlegB>1 rotate z */
	.half	   10,   17 	/* <m_rfoot>1 rotate x */
	.half	   10,   27 	/* <m_rfoot>1 rotate y */
	.half	   10,   37 	/* <m_rfoot>1 rotate z */

	.half	0x0000,0x00BD,0x3FFF,0xFEDE,0xFE52,0xD86E,0xDA85,0xD85D,0x791B,0x077F,0x123D,0x154D,0xE450,0xDC26,0x8942,0xE972
	.half	0x84B7,0x01EB,0x01EE,0x01E9,0x01C4,0x017F,0x012F,0x00D6,0x0079,0x001C,0xFFC4,0x0186,0x0185,0x0188,0x0190,0x019F
	.half	0x01B0,0x01C3,0x01D6,0x01EA,0x01FD,0xEEE4,0xEF19,0xEE9B,0xEBC5,0xE6AC,0xE0B2,0xDA22,0xD346,0xCC6B,0xC5DA,0x0EA9
	.half	0x0E70,0x0EF9,0x1211,0x179F,0x1E23,0x254B,0x2CC4,0x343E,0x3B65,0x1B98,0x1B58,0x1AC7,0x1A30,0x19AD,0x1927,0x18A0
	.half	0x1819,0x1791,0x170A,0x0320,0x0348,0x03C8,0x04A8,0x05D1,0x071F,0x0885,0x09F7,0x0B68,0x0CCE,0xD40B,0xD3EA,0xD35F
	.half	0xD230,0xD077,0xCE80,0xCC60,0xCA2C,0xC7F8,0xC5D8,0xFF43,0xFF42,0xFF46,0xFF5B,0xFF7F,0xFFA9,0xFFD8,0x0008,0x0038
	.half	0x0067,0xFF12,0xFF12,0xFF12,0xFF13,0xFF16,0xFF1A,0xFF1E,0xFF22,0xFF26,0xFF2A,0xFF3D,0xFF4B,0xFE6E,0xFB21,0xF58A
	.half	0xEF08,0xE7EB,0xE07F,0xD914,0xD1F7,0x05FD,0x05D4,0x0682,0x09C7,0x0F83,0x1638,0x1D92,0x253F,0x2CEC,0x3446,0xE121
	.half	0xE157,0xE1D5,0xE267,0xE2F4,0xE389,0xE423,0xE4BF,0xE55B,0xE5F5,0xFEED,0xFEC7,0xFE3E,0xFD2B,0xFBA7,0xF9EF,0xF815
	.half	0xF629,0xF43D,0xF263,0xD552,0xD548,0xD4E3,0xD3B5,0xD1D1,0xCFA1,0xCD3E,0xCAC3,0xC848,0xC5E6,0x19CB,0x80D8,0x59D2
	.half	0xBF5F,0xE02F,0x7A60,0x5A4B,0xBF5F,0xD17C,0xD1E8,0xD314,0xD4D9,0xD711,0xD995,0xDC40,0xDEEA,0xE16E,0xE3A6,0x109A
	.half	0x1053,0x0F8D,0x0E62,0x0CEB,0x0B42,0x097F,0x07BD,0x0614,0x049D,0x6843,0x680F,0x6781,0x66AB,0x659E,0x646D,0x632A
	.half	0x61E6,0x60B5,0x5FA8
