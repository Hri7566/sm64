/********************************************************************************
	backup.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	May 11, 1986
 ********************************************************************************/

#ifndef BACKUP_H
#define	BACKUP_H


#define	BuGetCourseNumber(stage)			(buCourseTable[(stage)-1])


#define	BuGetHiScore(course)				((ushort)(BuGetWinScore(course) & 0xffff))
#define	BuGetWinner(course)					((ushort)(BuGetWinScore(course) >> 16))

#define	BuGetCourseStars(player,course)		BuGetNumStars(player,course)
#define	BuGetTotalStars(player)				BuGetSumStars(player, 0,24)
#define	BuGetExtraStars(player)				BuGetSumStars(player,15,24)

#define	BuSetStartingStar()					(buStartingStarFlag=BuGetStarFlag(activePlayerNo-1,activeCourseNo-1))
#define	BuGetStartingStar()					buStartingStarFlag



#define	BuGetLastCourseNo()					buLastBackupCourseNo
#define	BuGetLastStarNo()					buLastBackupStarNo
#define	BuIsHiScore()						buLastBackupHiScore
#define	BuIsMyHiScore()						buLastBackupMyHiScore


extern char  buCourseTable[38];
extern uchar buLastBackupCourseNo;
extern uchar buLastBackupStarNo;
extern uchar buLastBackupHiScore;
extern uchar buLastBackupMyHiScore;
extern uchar buStartingStarFlag;
extern uchar buYosshiJump;

/********************************************************************************/
/*																				*/
/*	item flags.																	*/
/*																				*/
/********************************************************************************/

#define	BU_ITEM_ACTIVE			0x00000001

#define	BU_ITEM_REDSW			0x00000002
#define	BU_ITEM_GREENSW			0x00000004
#define	BU_ITEM_BLUESW			0x00000008

#define	BU_ITEM_KOOPAKEY1		0x00000010
#define	BU_ITEM_KOOPAKEY2		0x00000020
#define	BU_ITEM_KEYDOOR1		0x00000040
#define	BU_ITEM_KEYDOOR2		0x00000080

#define	BU_ITEM_WATERWALL		0x00000100
#define	BU_ITEM_DRAINSW			0x00000200

#define	BU_ITEM_STARDOOR0		0x00000400
#define	BU_ITEM_STARDOOR1		0x00000800
#define	BU_ITEM_STARDOOR2		0x00001000
#define	BU_ITEM_STARDOOR3		0x00002000
#define	BU_ITEM_STARDOOR4		0x00004000
#define	BU_ITEM_STARDOOR5		0x00008000
#define	BU_ITEM_STARDOOR6		0x00100000

#define	BU_ITEM_LOSTHAT			0x00010000
#define	BU_ITEM_CONDORHAT		0x00020000
#define	BU_ITEM_MONKEYHAT		0x00040000
#define	BU_ITEM_DARUMAHAT		0x00080000
#define	BU_ITEM_YOSSHI			0x00200000

#define	BU_ITEM_KINOPIO1		0x01000000
#define	BU_ITEM_KINOPIO2		0x02000000
#define	BU_ITEM_KINOPIO3		0x04000000
#define	BU_ITEM_RABBIT1			0x08000000
#define	BU_ITEM_RABBIT2			0x10000000


#define	BU_ITEM_HATFLAGS		0x000F0000
#define	BU_ITEM_ALLFLAGS		0x000003FF
#define	BU_ITEM_KILLKOOPA1		0x00000050
#define	BU_ITEM_KILLKOOPA2		0x000000A0

/********************************************************************************/
/*																				*/
/*	function prototype definition.												*/
/*																				*/
/********************************************************************************/

extern void
BuWriteStorage(int player);

extern void
BuClearStorage(int player);

extern int
BuCopyStorage(int source, int destination);



extern void
BuInitBackUp(void);

extern void
BuRestoreBackUp(void);

extern void
BuStoreBackUp(short score, short starno);



extern int
BuIsActive(int player);

extern ulong
BuGetWinScore(int course);

extern int
BuGetNumStars(int player, int course);

extern int
BuGetSumStars(int player, int course, int end_course);

extern int
BuGetStarFlag(int player, int course);

extern void
BuSetStarFlag(int player, int course, int starflag);

extern int
BuGetNumCoins(int player, int course);



extern void
BuSetItemFlag(ulong itemflag);

extern void
BuClrItemFlag(ulong itemflag);

extern ulong
BuGetItemFlag(void);

extern int
BuGetCannonFlag(void);

extern void
BuSetCannonFlag(void);

extern void
BuSetHatPosition(short posx, short posy, short posz);

extern int
BuGetHatPosition(SVector position);

extern void
BuSetSoundMode(ushort mode);

extern ushort
BuGetSoundMode(void);

extern void
BuRestoreHat(void);


extern void
BuClearRamStorage(void);

extern void
BuSetMidPoint(BGPortPtr bgport);

extern int
BuGetMidPoint(BGPortPtr bgport);

#endif
