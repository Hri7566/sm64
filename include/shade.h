/********************************************************************************
						Ultra 64 MARIO Brothers

			Shade drawing function header file(prototype function).

			Copyright 1995 Nintendo co., ltd.  All rights reserved

				  This module was programmed Y.Tanimoto

								1996.2.1
*********************************************************************************/
#ifndef _SHADE_HEAD
#define	_SHADE_HEAD

extern Gfx* ShadeDrawFunc(float, float, float, short, unsigned char, char);

extern char shade_Wflag ;
extern char shade_Tflag ;

#endif
