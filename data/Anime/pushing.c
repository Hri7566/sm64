/********************************************************************************
						Ultra 64 MARIO Brothers

					  mario pushing animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							February 10, 1996
 ********************************************************************************/

#include "../headers.h"


static short mario_push_prm[]={
	    0,  169,  173,  178,  181,  183,  185,  187,
	  188,  188,  188,  186,  185,  182,  180,  179,
	  177,  177,  179,  182,  185,  187,  187,  185,
	  180,  175,  170,  169,16383,-10222,-10070,-9675,
	-9126,-8511,-7920,-7121,-6222,-5820,-5962,-6265,
	-6677,-7145,-7617,-8040,-8361,-8583,-8751,-8883,
	-8994,-9101,-9221,-9370,-9599,-9881,-10120,-10222,
	-11394,-11524,-11853,-12287,-12734,-13101,-13424,-13697,
	-13755,-13591,-13331,-13017,-12693,-12400,-12182,-12080,
	-12137,-12327,-12597,-12889,-13149,-13321,-13349,-13009,
	-12340,-11688,-11394, 2896, 2813, 2596, 2293, 1950,
	 1617, 1177,  673,  395,  358,  376,  439,  537,
	  659,  796,  936, 1094, 1284, 1494, 1716, 1938,
	 2149, 2341, 2528, 2708, 2843, 2896,-11473,14758,
	-11902,32191, 8136, 8189, 8328, 8524, 8746, 8964,
	 9147, 9266, 9290, 9195, 8997, 8730, 8428, 8122,
	 7848, 7636, 7474, 7329, 7210, 7127, 7088, 7104,
	 7240, 7500, 7794, 8035, 8136, 9020, 9092, 9277,
	 9533, 9819,10092,10308,10426,10404,10168, 9731,
	 9174, 8582, 8035, 7618, 7412, 7493, 7814, 8272,
	 8767, 9197, 9460, 9508, 9411, 9245, 9090, 9020,
	-3931,-3972,-4081,-4239,-4423,-4613,-4789,-4930,
	-5015,-5026,-4975,-4887,-4789,-4704,-4659,-4678,
	-4812,-5051,-5332,-5592,-5767,-5794,-5557,-5089,
	-4554,-4114,-3931,-7613,-14455,13402,31773, 1152,
	 1058,  966,  983, 1016, 1061, 1111, 1159, 1200,
	 1237, 1270, 1282, 1254, 1205, 1177, 1176, 1180,
	 1188, 1198, 1209, 1220, 1229, 1234, 1235, 1213,
	 1173, 1152, -645, -775, -900, -875, -828, -766,
	 -694, -620, -551, -459, -358, -315, -389, -521,
	 -598, -603, -596, -578, -555, -529, -504, -482,
	 -469, -466, -514, -599, -645,-16047,-14773,-13550,
	-13785,-14241,-14849,-15537,-16237,-16878,-17666,-18500,
	-18847,-18219,-17110,-16463,-16423,-16493,-16643,-16844,
	-17068,-17284,-17465,-17581,-17604,-17185,-16443,-16047,
	 8901, 6934, 4833, 4668, 4701, 4928, 5343, 5939,
	 6711, 8171,10358,12551,14651,16582,17662,18070,
	18426,18709,18897,18969,18904,18678,18272,17662,
	15072,11013, 8901,  463,  862, 1346, 1490, 1591,
	 1663, 1722, 1781, 1856, 1974, 2095, 2133, 2052,
	 1891, 1682, 1439, 1166,  874,  577,  287,   18,
	 -218, -410, -543, -326,  179,  463, 2442, 2292,
	 2094, 2009, 1937, 1874, 1811, 1742, 1661, 1516,
	 1347, 1283, 1419, 1658, 1837, 1928, 2010, 2084,
	 2151, 2213, 2271, 2325, 2376, 2426, 2452, 2449,
	 2442,-26190,-24377,-22146,-21428,-20904,-20502,-20151,
	-19780,-19317,-18529,-17660,-17352,-17971,-19135,-20359,
	-21524,-22795,-24123,-25459,-26750,-27948,-29003,-29864,
	-30480,-29581,-27409,-26190,    0,    0,    0,    0,
	    0,    0,    0,    0,    0,   45,   91,    0,
	 -339, -797,-1147,-1319,-1384,-1327,-1132, -842,
	 -528, -260,  -87,    0,   23,   11,    0,    0,
	    0,    0,    0,    0,    0,    0,    0,    0,
	    0,    0,    0,   -4,   -9,    0,   41,   96,
	  125,  112,   80,   42,   13,    1,    0,   -1,
	    0,    0,-16576,-16391,-15933,-15349,-14784,-14385,
	-14363,-14519,-14385,-13496,-12316,-11778,-12479,-13813,
	-14907,-15366,-15585,-15867,-16326,-16843,-17382,-17903,
	-18578,-18870,-18157,-17112,-16576,14324,14818,16026,
	17540,18952,19852,20179,19865,18497,15256,10961,
	 7649, 6352, 6227, 6502, 6739, 7017, 7338, 7703,
	 8114, 8573, 9082, 9643,10257,11596,13412,14324,
	 -876, -797, -600, -349, -107,   63,  131,  121,
	   63,  -27, -168, -371, -696,-1062,-1278,-1340,
	-1380,-1400,-1403,-1393,-1372,-1343,-1311,-1278,
	-1159, -973, -876,-2090,-2105,-2141,-2187,-2232,
	-2263,-2274,-2270,-2263,-2269,-2271,-2234,-2116,
	-1965,-1874,-1848,-1831,-1822,-1819,-1822,-1830,
	-1842,-1857,-1874,-1938,-2038,-2090,-20910,-21288,
	-22224,-23418,-24571,-25386,-25704,-25658,-25386,-24971,
	-24331,-23357,-21737,-19887,-18797,-18480,-18277,-18175,
	-18157,-18208,-18313,-18456,-18623,-18797,-19422,-20401,
	-20910,-32767,-32767,16753,-13520,-13654,-14042,-14665,
	-15506,-16545,-18439,-20969,-22803,-23267,-23035,-22803,
	-22934,-23066,-22803,-22168,-21456,-20683,-19867,-19024,
	-18170,-17322,-16497,-15711,-14805,-13919,-13520,-32767,
	-32767,16753,-16545,-8110, 3003,  129,  747,  805,
	  958, 1176, 1427, 1681, 1908, 2075, 2153, 2092,
	 1905, 1649, 1384, 1168, 1060, 1064, 1130, 1237,
	 1365, 1494, 1604, 1675, 1686, 1525, 1204,  889,
	  747,18356
};
static short mario_push_tbl[]={
	    1,    0,	/* chn14translate x */
	   27,    1,	/* chn14 translate y */
	    1,    0,	/* chn14 translate z */
	    1,    0,	/* chn14 rotate x */
	    1,   28,	/* chn14 rotate y */
	    1,    0,	/* chn14 rotate z */
	    1,    0,	/* <m_waist>1_3 rotate x */
	    1,    0,	/* <m_waist>1_3 rotate y */
	    1,  641,	/* <m_waist>1_3 rotate z */
	    1,    0,	/* <m_body>1 rotate x */
	    1,    0,	/* <m_body>1 rotate y */
	   27,  614,	/* <m_body>1 rotate z */
	    1,  611,	/* <m_head>2 rotate x */
	    1,  612,	/* <m_head>2 rotate y */
	    1,  613,	/* <m_head>2 rotate z */

	    1,  607,	/* chn6 rotate x */
	    1,  608,	/* chn6 rotate y */
	    1,  609,	/* chn6 rotate z */
	    1,  111,	/* <m_larmA>1 rotate x */
	    1,  112,	/* <m_larmA>1 rotate y */
	    1,  113,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	    1,  110,	/* <m_larmB>1 rotate z */
	   27,   29,	/* <m_lhand>1 rotate x */
	   27,   56,	/* <m_lhand>1 rotate y */
	   27,   83,	/* <m_lhand>1 rotate z */

	    1,  577,	/* chn10 rotate x */
	    1,  578,	/* chn10 rotate y */
	    1,  579,	/* chn10 rotate z */
	    1,  196,	/* <m_rarmA>1 rotate x */
	    1,  197,	/* <m_rarmA>1 rotate y */
	    1,  198,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	    1,  195,	/* <m_rarmB>1 rotate z */
	   27,  114,	/* <m_rhand>1 rotate x */
	   27,  141,	/* <m_rhand>1 rotate y */
	   27,  168,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	   27,  580,	/* chn15 rotate z */
	   27,  496,	/* <m_llegA>1 rotate x */
	   27,  523,	/* <m_llegA>1 rotate y */
	   27,  550,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	   27,  469,	/* <m_llegB>1 rotate z */
	   27,  388,	/* <m_ltoot>1 rotate x */
	   27,  415,	/* <m_ltoot>1 rotate y */
	   27,  442,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  610,	/* chn17 rotate z */
	   27,  307,	/* <m_rlegA>1 rotate x */
	   27,  334,	/* <m_rlegA>1 rotate y */
	   27,  361,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	   27,  280,	/* <m_rlegB>1 rotate z */
	   27,  199,	/* <m_rfoot>1 rotate x */
	   27,  226,	/* <m_rfoot>1 rotate y */
	   27,  253,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioPushing = {
	 MAP_ANIM_NORMAL,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	27,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_push_prm,
	mario_push_tbl
};
