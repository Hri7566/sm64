/********************************************************************************
						Ultra 64 MARIO Brothers

				   mario cold wait end animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							January 12, 1996
 ********************************************************************************/

#include "../headers.h"


static short mario_cold_wait_end_prm[]={
	    0,  100,  101,  105,  110,  117,  124,  131,
	  137,  143,  148,  153,  157,  161,  164,  167,
	  169,  170,    0,    1,    3,    6,   10,   14,
	   18,   21,   23,   23,   21,   17,   14,   10,
	    5,    1,    0,16383,-1668,-4724,-15291,-15417,
	-15573,-15746,-15924,-16094,-16244,-16361,-16432,-16445,
	-16388,-16125,-15617,-14994,-14387,-13927,-13745,11371,
	11295,11293,11330,11371,11381,11325,11167,10872,
	10406, 9732, 8370, 6183, 3648, 1245, -547,-1251,
	-3253,-3388,-3487,-3568,-3649,-3748,-3883,-4072,
	-4333,-4684,-5143,-5978,-7267,-8739,-10124,-11153,
	-11556,-27239,-27110,-27071,-27080,-27093,-27066,-26958,
	-26724,-26322,-25708,-24840,-23120,-20377,-17205,-14202,
	-11963,-11084,  417,  418,  408,  390,  365,  336,
	  303,  269,  234,  202,  174,  143,  105,   67,
	   33,    9,    0,-1208,-1210,-1182,-1129,-1058,
	 -972, -877, -778, -679, -586, -504, -414, -306,
	 -195,  -97,  -26,    0,-5191,-5193,-5136,-5031,
	-4886,-4712,-4520,-4319,-4119,-3931,-3764,-3582,
	-3363,-3138,-2939,-2797,-2743,-16248,-16250,-16206,
	-16125,-16014,-15881,-15734,-15579,-15426,-15282,-15153,
	-15014,-14846,-14674,-14521,-14412,-14371,-7719,-7836,
	-8069,-8373,-8705,-9019,-9272,-9420,-9419,-9223,
	-8790,-7644,-5655,-3291,-1022,  683, 1355, 1597,
	 1790, 2063, 2406, 2812, 3271, 3776, 4317, 4886,
	 5474, 6073, 6872, 7943, 9099,10155,10924,11222,
	-28574,-28536,-28493,-28422,-28303,-28112,-27828,-27430,
	-26896,-26204,-25332,-23750,-21290,-18472,-15815,-13840,
	-13066,  140,  211,  353,  541,  750,  953, 1127,
	 1247, 1286, 1224, 1075,  869,  634,  400,  197,
	   54,    0, -143, -253, -474, -766,-1091,-1411,
	-1689,-1885,-1961,-1889,-1692,-1411,-1089, -766,
	 -484, -284, -208,-21918,-21875,-21806,-21699,-21544,
	-21330,-21047,-20684,-20229,-19570,-18668,-17625,-16543,
	-15524,-14670,-14082,-13864,22913,22574,21968,21134,
	20113,18944,17667,16322,14950,13589,12280,11063,
	 9978, 9065, 8363, 7913, 7754, 2340, 2443, 2626,
	 2878, 3187, 3540, 3926, 4332, 4747, 5158, 5553,
	 5921, 6249, 6525, 6737, 6873, 6921, 7440, 7373,
	 7252, 7086, 6883, 6650, 6395, 6127, 5854, 5583,
	 5322, 5080, 4864, 4682, 4542, 4452, 4420,-29219,
	-29045,-28734,-28306,-27781,-27181,-26526,-25835,-25131,
	-24432,-23760,-23135,-22578,-22109,-21749,-21518,-21436,
	    0, -117, -376, -727,-1123,-1516,-1856,-2095,
	-2186,-2092,-1844,-1494,-1093, -691, -341,  -93,
	    0,    0,   23,   75,  146,  225,  304,  372,
	  420,  438,  420,  370,  300,  219,  138,   68,
	   18,    0,-22120,-22069,-21967,-21816,-21621,-21386,
	-21113,-20809,-20475,-20043,-19476,-18835,-18179,-17566,
	-17055,-16705,-16576,22588,22296,21773,21055,20175,
	19168,18068,16909,15727,14554,13427,12378,11443,
	10656,10052, 9664, 9527,-4518,-4656,-4902,-5241,
	-5656,-6131,-6650,-7196,-7754,-8307,-8838,-9333,
	-9774,-10145,-10430,-10613,-10678,-10291,-10174,-9965,
	-9677,-9324,-8920,-8479,-8015,-7541,-7071,-6619,
	-6198,-5824,-5508,-5266,-5110,-5055,-27246,-27062,
	-26734,-26283,-25730,-25097,-24406,-23679,-22936,-22200,
	-21491,-20833,-20246,-19751,-19372,-19128,-19042,-32767,
	-32767,16753,-16545,-32767,-32767,16753,-16545,-7226,
	-7266,-7070,-6684,-6155,-5528,-4851,-4168,-3527,
	-2972,-2408,-1799,-1282, -843, -428, -120,    0,
	 -344, -273, -185,  -84,   23,  136,  247,  355,
	  453,  537,  597,  613,  571,  439,  247,   74,
	    0,-2978,-2870,-2645,-2325,-1933,-1491,-1021,
	 -546,  -87,  330,  905, 1528, 1722, 1207,  307,
	 -546, -926,    0,    8,   13,   15,   16,   16,
	   15,   14,   13,   13,   11,    9,    6,    4,
	    2,    0,    0, 7471, 7625, 7880, 8187, 8494,
	 8748, 8899, 8895, 8685, 8152, 7296, 6239, 5101,
	 4005, 3072, 2423, 2180,16545,16702,17048,17517,
	18046,18570,19025,19344,19465,19340,19009,18541,
	18005,17469,17001,16671,16545
};
static short mario_cold_wait_end_tbl[]={
	    1,    0,	/* chn14_3translate x */
	   17,    1,	/* chn14_3 translate y */
	   17,   18,	/* chn14_3 translate z */
	    1,    0,	/* chn14_3 rotate x */
	    1,   35,	/* chn14_3 rotate y */
	    1,    0,	/* chn14_3 rotate z */
	    1,    0,	/* <m_waist>1_3 rotate x */
	    1,    0,	/* <m_waist>1_3 rotate y */
	   17,  556,	/* <m_waist>1_3 rotate z */
	    1,    0,	/* <m_body>1 rotate x */
	   17,  522,	/* <m_body>1 rotate y */
	   17,  539,	/* <m_body>1 rotate z */
	   17,  471,	/* <m_head>2 rotate x */
	   17,  488,	/* <m_head>2 rotate y */
	   17,  505,	/* <m_head>2 rotate z */

	    1,  467,	/* chn6 rotate x */
	    1,  468,	/* chn6 rotate y */
	    1,  469,	/* chn6 rotate z */
	   17,   55,	/* <m_larmA>1 rotate x */
	   17,   72,	/* <m_larmA>1 rotate y */
	   17,   89,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	   17,   38,	/* <m_larmB>1 rotate z */
	    1,    0,	/* <m_lhand>1 rotate x */
	    1,   36,	/* <m_lhand>1 rotate y */
	    1,   37,	/* <m_lhand>1 rotate z */

	    1,  463,	/* chn10 rotate x */
	    1,  464,	/* chn10 rotate y */
	    1,  465,	/* chn10 rotate z */
	   17,  174,	/* <m_rarmA>1 rotate x */
	   17,  191,	/* <m_rarmA>1 rotate y */
	   17,  208,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	   17,  157,	/* <m_rarmB>1 rotate z */
	   17,  106,	/* <m_rhand>1 rotate x */
	   17,  123,	/* <m_rhand>1 rotate y */
	   17,  140,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  466,	/* chn15 rotate z */
	   17,  412,	/* <m_llegA>1 rotate x */
	   17,  429,	/* <m_llegA>1 rotate y */
	   17,  446,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	   17,  395,	/* <m_llegB>1 rotate z */
	   17,  344,	/* <m_ltoot>1 rotate x */
	   17,  361,	/* <m_ltoot>1 rotate y */
	   17,  378,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  470,	/* chn17 rotate z */
	   17,  293,	/* <m_rlegA>1 rotate x */
	   17,  310,	/* <m_rlegA>1 rotate y */
	   17,  327,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	   17,  276,	/* <m_rlegB>1 rotate z */
	   17,  225,	/* <m_rfoot>1 rotate x */
	   17,  242,	/* <m_rfoot>1 rotate y */
	   17,  259,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioColdWaitEnd = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	17,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_cold_wait_end_prm,
	mario_cold_wait_end_tbl
};
