/********************************************************************************
						Ultra 64 MARIO Brothers

				  mario wall junmping animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							August 23, 1995
 ********************************************************************************/

#include "../headers.h"


static short mario_wall_jump_prm[]={
	    0,  195,  -67,16383, 4754, 4776, 4830, 4897,
	 4957, 4992, 4983, 4910, 4754, 4458, 4016, 3485,
	 2922, 2385, 1930, 1615, 1498,  866,  892,  955,
	 1033, 1104, 1145, 1134, 1048,  866,  519,    1,
	 -620,-1279,-1909,-2442,-2811,-2949,-9186,-9173,
	-9142,-9105,-9070,-9051,-9056,-9097,-9186,-9353,
	-9603,-9904,-10223,-10527,-10785,-10963,-11030,-3741,
	-3898,-4323,-4948,-5703,-6519,-7327,-8058,-8643,
	-9143,-9649,-10140,-10592,-10983,-11290,-11491,-11563,
	-56823,-56816,-56799,-56778,-56759,-56748,-56751,-56774,
	-56823,-56917,-57056,-57224,-57401,-57571,-57714,-57814,
	-57851, 1627, 1667, 1765, 1886, 1996, 2060, 2042,
	 1910, 1627, 1090,  288, -674,-1695,-2670,-3495,
	-4066,-4280,34525,34514,34488,34457,34428,34411,
	34415,34450,34525,34666,34878,35132,35401,35658,
	35875,36026,36082,-7432,-7389,-7316,-7302,-7432,
	-7891,-8579,-9156,-9282,-8930,-8355,-7639,-6867,
	-6121,-5486,-5043,-4877, 5247, 5376, 5590, 5633,
	 5247, 4077, 2355,  707, -239, -502, -550, -438,
	 -221,   43,  302,  498,  575,-9045,-9136,-9288,
	-9318,-9045,-8188,-6920,-5746,-5167,-5189,-5434,
	-5832,-6314,-6810,-7251,-7567,-7687,-10113,-10883,
	-12462,-13753,-13663,-11108,-6936,-2946, -938, -950,
	-1675,-2891,-4380,-5921,-7294,-8279,-8656,17997,
	17673,16927,16103,15544,15421,15524,15625,15497,
	15091,14549,13930,13291,12693,12193,11850,11722,
	10148, 8612, 5149, 1478, -683, -642,  606, 2249,
	 3474, 4249, 5057, 5856, 6604, 7259, 7779, 8122,
	 8246,-11424,-11432,-11456,-11495,-11548,-11742,-12035,
	-12171,-11893,-11147,-10127,-8948,-7725,-6574,-5609,
	-4945,-4698,-5426,-5131,-4399,-3462,-2548,-1888,
	-1665,-1722,-1777,-1741,-1710,-1679,-1641,-1590,
	-1521,-1306,-1132,  575,  468,  207, -113, -403,
	 -569, -448, -155,  -12,  -79, -193, -345, -524,
	 -720, -921,-1319,-1616,-17922,-17376,-15999,-14182,
	-12316,-10792,-9492,-8419,-8150,-8621,-9348,-10277,
	-11351,-12516,-13716,-16030,-17744,24225,21722,15968,
	 9597, 5242, 3613, 3301, 3768, 4472, 5478, 6994,
	 8695,10258,11357,11669, 7261, 2948, 2925, 2980,
	 3123, 3319, 3537, 3743, 4073, 4430, 4431, 3990,
	 3351, 2623, 1915, 1334,  989, 1902, 2909,  482,
	  527,  643,  802,  974, 1131, 1283, 1446, 1598,
	 1762, 1956, 2147, 2302, 2389, 2373, 1657,  979,
	-25348,-24723,-23129,-20989,-18727,-16765,-14724,-12734,
	-11980,-12626,-13860,-15403,-16977,-18304,-19106,-17443,
	-15557,-2838,-2701,-2379,-2007,-1720,-1511,-1288,
	-1057, -824, -596, -378, -177,    0,  149,  263,
	  336,  362,-3603,-3339,-2751,-2145,-1828,-1781,
	-1778,-1811,-1871,-1953,-2047,-2148,-2247,-2337,
	-2410,-2460,-2478,-11134,-10766,-10003,-9361,-9354,
	-9945,-10724,-11647,-12666,-13737,-14814,-15851,-16802,
	-17621,-18263,-18682,-18831, 8188, 9848,13557,17411,
	19504,19947,20150,20151,19988,19700,19326,18903,
	18471,18068,17732,17503,17418, 3172, 2823, 1947,
	  801, -357,-1274,-1686,-1797,-1905,-2011,-2113,
	-2208,-2291,-2362,-2416,-2450,-2462, 2206, 2044,
	 1636, 1095,  535,   70, -194, -446, -842,-1301,
	-1795,-2293,-2766,-3185,-3521,-3744,-3825,-17136,
	-18325,-21287,-25119,-28914,-31767,-32272,-31704,-31628,
	-31538,-31441,-31342,-31247,-31162,-31095,-31050,-31033,
	32767,32767,16753,-16542,32767,32767,32768,32770,
	32772,32774,32776,32777,32778,32779,32779,32779,
	32779,32779,32779,32779,32778,32767,31964,29816,
	26714,23047,19207,15584,12568,10550, 9487, 8988,
	 8923, 9162, 9574,10030,10399,10550,16753,16754,
	16759,16766,16773,16781,16789,16796,16800,16802,
	16803,16803,16803,16802,16801,16800,16800, -823,
	 1355,-18285, 7652, 7454, 6931, 6192, 5345, 4500,
	 3764, 3247, 2900, 2602, 2351, 2144, 1979, 1854,
	 1768, 1718, 1702, 3850, 3663, 3171, 2478, 1689,
	  909,  242, -206, -476, -680, -829, -929, -992,
	-1025,-1037,-1038,-1036,-5092,-4975,-4662,-4206,
	-3663,-3087,-2534,-2057,-1597,-1081, -538,    3,
	  512,  960, 1317, 1553, 1638, -700, -646, -504,
	 -305,  -80,  140,  324,  442,  502,  538,  553,
	  553,  542,  525,  507,  493,  487,-1177,-1104,
	 -913, -648, -353,  -71,  154,  280,  313,  295,
	  240,  159,   66,  -28, -110, -169, -191,13064,
	12208, 9989, 6929, 3549,  372,-2080,-3287,-3275,
	-2547,-1294,  290, 2017, 3693, 5128, 6129, 6506,
	-12161,-11722,-10572,-8964,-7149,-5381,-3912,-2994,
	-2560,-2346,-2307,-2398,-2573,-2788,-2998,-3156,
	-3219,-1287,-1174, -873, -432,   94,  657, 1203,
	 1683, 1407, 1062,  695,  328,  -19, -325, -570,
	 -732, -790,20722,20669,20527,20318,20070,19804,
	19548,19324,18541,17647,16701,15755,14861,14074,
	13446,13030,12879
};
static short mario_wall_jump_tbl[]={
	    1,    0,	/* chn14_2translate x */
	    1,    1,	/* chn14_2 translate y */
	    1,    2,	/* chn14_2 translate z */
	    1,    0,	/* chn14_2 rotate x */
	    1,    3,	/* chn14_2 rotate y */
	    1,    0,	/* chn14_2 rotate z */
	   17,  640,	/* <m_waist>1_3 rotate x */
	   17,  657,	/* <m_waist>1_3 rotate y */
	   17,  674,	/* <m_waist>1_3 rotate z */
	   17,  589,	/* <m_body>1 rotate x */
	   17,  606,	/* <m_body>1 rotate y */
	   17,  623,	/* <m_body>1 rotate z */
	   17,  538,	/* <m_head>2 rotate x */
	   17,  555,	/* <m_head>2 rotate y */
	   17,  572,	/* <m_head>2 rotate z */

	   17,  484,	/* chn6 rotate x */
	   17,  501,	/* chn6 rotate y */
	   17,  518,	/* chn6 rotate z */
	   17,   72,	/* <m_larmA>1 rotate x */
	   17,   89,	/* <m_larmA>1 rotate y */
	   17,  106,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	   17,   55,	/* <m_larmB>1 rotate z */
	   17,    4,	/* <m_lhand>1 rotate x */
	   17,   21,	/* <m_lhand>1 rotate y */
	   17,   38,	/* <m_lhand>1 rotate z */

	    1,  480,	/* chn10 rotate x */
	    1,  481,	/* chn10 rotate y */
	    1,  482,	/* chn10 rotate z */
	   17,  191,	/* <m_rarmA>1 rotate x */
	   17,  208,	/* <m_rarmA>1 rotate y */
	   17,  225,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	   17,  174,	/* <m_rarmB>1 rotate z */
	   17,  123,	/* <m_rhand>1 rotate x */
	   17,  140,	/* <m_rhand>1 rotate y */
	   17,  157,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  483,	/* chn15 rotate z */
	   17,  429,	/* <m_llegA>1 rotate x */
	   17,  446,	/* <m_llegA>1 rotate y */
	   17,  463,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	   17,  412,	/* <m_llegB>1 rotate z */
	   17,  361,	/* <m_ltoot>1 rotate x */
	   17,  378,	/* <m_ltoot>1 rotate y */
	   17,  395,	/* <m_ltoot>1 rotate z */

	    1,  535,	/* chn17 rotate x */
	    1,  536,	/* chn17 rotate y */
	    1,  537,	/* chn17 rotate z */
	   17,  310,	/* <m_rlegA>1 rotate x */
	   17,  327,	/* <m_rlegA>1 rotate y */
	   17,  344,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	   17,  293,	/* <m_rlegB>1 rotate z */
	   17,  242,	/* <m_rfoot>1 rotate x */
	   17,  259,	/* <m_rfoot>1 rotate y */
	   17,  276,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioWallJump = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	17,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_wall_jump_prm,
	mario_wall_jump_tbl
};
AnimeRecord animMarioWallStay = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	 1,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_wall_jump_prm,
	mario_wall_jump_tbl
};
