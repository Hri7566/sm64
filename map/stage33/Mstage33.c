/********************************************************************************
						Ultra 64 MARIO Brothers

					 stage33 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							Decemver 11, 1995
*********************************************************************************/
#include "../../headers.h"

extern Gfx gfx_kopa2_A[];
extern Gfx gfx_kopa2_B[];

/* ===============================================================================
        : Hierarchy map data of Kopa2A.
================================================================================== */
Hierarchy RCP_HmsKopa2A[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kopa2_A)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage33Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(1, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, 20000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_KUPPA,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF, gfx_kopa2_B)
					hmsObject()
					hmsCProg(12, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
