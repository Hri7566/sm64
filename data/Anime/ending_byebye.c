/********************************************************************************
						Ultra 64 MARIO Brothers

				mario ending demo bye bye animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							April 22, 1996
 ********************************************************************************/

#include "../headers.h"
#include "../include/anime.h"


static short mario_demo_byebye_prm[]={
	    0,  180,16383,-6357,-6322,-6220,-6061,-5855,
	-5609,-5332,-5033,-4721,-4405,-4093,-3794,-3517,
	-3271,-3064,-2906,-2804,-2768,-2803,-2903,-3059,
	-3263,-3505,-3779,-4074,-4383,-4696,-5006,-5304,
	-5581,-5829,-6039,-6202,-6311,-6356,-4199,-4179,
	-4123,-4036,-3922,-3786,-3634,-3470,-3298,-3124,
	-2952,-2787,-2635,-2499,-2386,-2298,-2242,-2223,
	-2242,-2297,-2383,-2495,-2629,-2779,-2942,-3112,
	-3284,-3455,-3619,-3771,-3908,-4023,-4113,-4173,
	-4198,-6253,-6257,-6269,-6287,-6310,-6338,-6370,
	-6404,-6439,-6475,-6510,-6544,-6576,-6604,-6627,
	-6645,-6657,-6661,-6657,-6646,-6628,-6605,-6577,
	-6546,-6513,-6478,-6442,-6407,-6373,-6341,-6313,
	-6289,-6271,-6258,-6253,-13745,-13738,-13721,-13693,
	-13657,-13614,-13566,-13514,-13459,-13404,-13350,-13298,
	-13249,-13207,-13171,-13143,-13125,-13119,-13125,-13142,
	-13170,-13205,-13247,-13295,-13347,-13400,-13455,-13509,
	-13561,-13609,-13653,-13689,-13718,-13737,-13745,-1251,
	-1251,-1251,-1251,-1262,-1293,-1341,-1404,-1479,
	-1564,-1655,-1751,-1849,-1945,-2038,-2125,-2203,
	-2269,-2309,-2309,-2269,-2203,-2126,-2041,-1949,
	-1853,-1757,-1662,-1571,-1487,-1411,-1348,-1298,
	-1265,-1251,-11556,-11556,-11556,-11556,-11521,-11422,
	-11267,-11064,-10822,-10550,-10255,-9947,-9633,-9323,
	-9024,-8744,-8494,-8280,-8152,-8152,-8280,-8492,
	-8739,-9015,-9310,-9617,-9928,-10233,-10526,-10798,
	-11040,-11245,-11405,-11511,-11555,-11084,-11084,-11084,
	-11084,-11074,-11042,-10994,-10931,-10855,-10770,-10678,
	-10581,-10483,-10386,-10293,-10205,-10127,-10060,-10020,
	-10020,-10060,-10126,-10204,-10290,-10382,-10478,-10575,
	-10671,-10762,-10847,-10923,-10987,-11037,-11070,-11084,
	 -197,  593,-6086,-5401,-5365,-5265,-5108,-4904,
	-4661,-4387,-4092,-3784,-3471,-3163,-2868,-2594,
	-2351,-2147,-1990,-1890,-1854,-1889,-1988,-2142,
	-2343,-2583,-2853,-3145,-3449,-3759,-4065,-4360,
	-4633,-4878,-5086,-5247,-5355,-5400,-12382,-12382,
	-12382,-12382,-12382,-12382,-12334,-12200,-11994,-11730,
	-11422,-11084,-10731,-10377,-10036,-9721,-9448,-9230,
	-9082,-9009,-9009,-9083,-9230,-9446,-9715,-10025,
	-10361,-10710,-11059,-11394,-11702,-11967,-12178,-12321,
	-12381, 9712, 9712, 9712, 9712, 9712, 9712, 9756,
	 9880,10070,10314,10599,10910,11236,11564,11879,
	12169,12421,12623,12759,12827,12827,12759,12623,
	12424,12175,11889,11578,11256,10933,10624,10340,
	10095, 9900, 9768, 9713,19533,19533,19533,19533,
	19533,19533,19587,19738,19970,20268,20615,20995,
	21392,21791,22175,22529,22837,23082,23249,23332,
	23332,23248,23082,22840,22537,22188,21809,21416,
	21023,20645,20299,20000,19763,19602,19535,  954,
	 -491,-14204, 5563, 7715, 3573,-18658, -834,   22,
	-15429, 6502, 6501, 6500, 6498, 6494, 6490, 6486,
	 6481, 6476, 6470, 6464, 6458, 6452, 6446, 6440,
	 6434, 6429, 6424, 6420, 6416, 6413, 6411, 6410,
	 6410, 6411, 6413, 6416, 6421, 6427, 6435, 6445,
	 6456, 6469, 6485, 6502,-5671,-5671,-5672,-5672,
	-5673,-5673,-5674,-5675,-5676,-5677,-5679,-5680,
	-5681,-5682,-5683,-5685,-5686,-5687,-5687,-5688,
	-5689,-5689,-5689,-5689,-5689,-5689,-5688,-5687,
	-5686,-5684,-5682,-5680,-5678,-5675,-5671,-2680,
	-2680,-2679,-2678,-2677,-2675,-2674,-2671,-2669,
	-2667,-2664,-2662,-2659,-2656,-2654,-2651,-2649,
	-2647,-2645,-2644,-2642,-2642,-2641,-2641,-2641,
	-2642,-2644,-2646,-2649,-2652,-2656,-2661,-2666,
	-2673,-2680,-19130,-19129,-19128,-19126,-19124,-19121,
	-19118,-19114,-19110,-19106,-19101,-19097,-19092,-19088,
	-19084,-19079,-19075,-19072,-19069,-19066,-19064,-19062,
	-19061,-19061,-19062,-19063,-19066,-19070,-19074,-19080,
	-19087,-19096,-19106,-19117,-19130,-32767,-32767,16753,
	-16545,-32767,-32767,16753,-16545,    0,    9,   35,
	   77,  131,  195,  268,  346,  427,  510,  592,
	  670,  742,  807,  861,  902,  929,  938,  929,
	  903,  862,  809,  745,  674,  597,  516,  434,
	  353,  275,  203,  138,   83,   40,   12,    0,
	 -613, -613, -613, -613, -613, -614, -614, -614,
	 -614, -614, -614, -614, -614, -614, -614, -614,
	 -614, -614, -614, -614, -614, -614, -614, -614,
	 -614, -614, -614, -614, -614, -614, -613, -613,
	 -613, -613, -613,    0,  -12,  -47, -103, -175,
	 -261, -358, -462, -571, -682, -790, -895, -992,
	-1078,-1150,-1205,-1241,-1253,-1241,-1206,-1151,
	-1080, -996, -900, -797, -689, -580, -471, -367,
	 -271, -184, -111,  -54,  -16,    0,  -10,  -10,
	  -10,  -10,   -9,   -9,   -9,   -9,   -9,   -9,
	   -9,   -9,   -9,   -9,   -8,   -8,   -8,   -8,
	   -8,   -8,   -8,   -9,   -9,   -9,   -9,   -9,
	   -9,   -9,   -9,   -9,   -9,  -10,  -10,  -10,
	  -10,16545
};
static unsigned short mario_demo_byebye_tbl[]={
	    1,    0,	/* chn14_3translate x */
	    1,    1,	/* chn14_3 translate y */
	    1,    0,	/* chn14_3 translate z */
	    1,    0,	/* chn14_3 rotate x */
	    1,    2,	/* chn14_3 rotate y */
	    1,    0,	/* chn14_3 rotate z */
	    1,    0,	/* <m_waist>1_3 rotate x */
	    1,    0,	/* <m_waist>1_3 rotate y */
	    1,  689,	/* <m_waist>1_3 rotate z */
	    1,    0,	/* <m_body>1 rotate x */
	   35,  619,	/* <m_body>1 rotate y */
	   35,  654,	/* <m_body>1 rotate z */
	    1,    0,	/* <m_head>2 rotate x */
	   35,  549,	/* <m_head>2 rotate y */
	   35,  584,	/* <m_head>2 rotate z */

	    1,  545,	/* chn6 rotate x */
	    1,  546,	/* chn6 rotate y */
	    1,  547,	/* chn6 rotate z */
	   35,  143,	/* <m_larmA>1 rotate x */
	   35,  178,	/* <m_larmA>1 rotate y */
	   35,  213,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	   35,  108,	/* <m_larmB>1 rotate z */
	   35,    3,	/* <m_lhand>1 rotate x */
	   35,   38,	/* <m_lhand>1 rotate y */
	   35,   73,	/* <m_lhand>1 rotate z */

	    1,  541,	/* chn10 rotate x */
	    1,  542,	/* chn10 rotate y */
	    1,  543,	/* chn10 rotate z */
	   35,  286,	/* <m_rarmA>1 rotate x */
	   35,  321,	/* <m_rarmA>1 rotate y */
	   35,  356,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	   35,  251,	/* <m_rarmB>1 rotate z */
	    1,  248,	/* <m_rhand>1 rotate x */
	    1,  249,	/* <m_rhand>1 rotate y */
	    1,  250,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  544,	/* chn15 rotate z */
	   35,  436,	/* <m_llegA>1 rotate x */
	   35,  471,	/* <m_llegA>1 rotate y */
	   35,  506,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	   35,  401,	/* <m_llegB>1 rotate z */
	    1,  398,	/* <m_ltoot>1 rotate x */
	    1,  399,	/* <m_ltoot>1 rotate y */
	    1,  400,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  548,	/* chn17 rotate z */
	    1,  395,	/* <m_rlegA>1 rotate x */
	    1,  396,	/* <m_rlegA>1 rotate y */
	    1,  397,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	    1,  394,	/* <m_rlegB>1 rotate z */
	    1,  391,	/* <m_rfoot>1 rotate x */
	    1,  392,	/* <m_rfoot>1 rotate y */
	    1,  393,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioEndingByeBye = {
	 MAP_ANIM_NORMAL,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	35,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_demo_byebye_prm,
	mario_demo_byebye_tbl
};
