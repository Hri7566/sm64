/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 37 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							  1996.1.8
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage37

/********************************************************************************
		stage 37 Seq
 ********************************************************************************/

SEQ_DoStage37:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1, _GfxStage37SegmentRomStart, _GfxStage37SegmentRomEnd)


	seqBeginConstruction()
		seqHmsMario(S_Mario, ShapePlayer1, e_mario)

		/*------ test enemy's -----------------------------------------------*/
		seqHmsShape(S_nokonoko		 , RCP_HmsEnemynokonoko	 		)
		seqHmsShape(S_nokonoko_nude	 , RCP_HmsEnemynokonoko_nude		)
		seqHmsShape(S_koura			 , RCP_HmsGreenNoko 			)
		seqHmsShape(S_sanbo_head	 , RCP_HmsSanboHead 			)
		seqHmsShape(S_sanbo_body	 , RCP_HmsSanboBody		 	)
		seqHmsShape(S_bat			 , RCP_HmsEnemybat	 		)
		seqHmsShape(S_heyho		 	 , RCP_HmsEnemyheyho_fly		)
		seqHmsShape(S_kuribo		 , RCP_HmsEnemykuribo	 		)
		seqHmsShape(S_wanwan		 , RCP_HmsEnemywanwan	 		)
		seqHmsShape(S_hanachan_head	 , RCP_HmsEnemyhanahead	 		)
		seqHmsShape(S_hanachan_body	 , RCP_HmsEnemyhanabody	 		)
		seqHmsShape(S_jugem			 , RCP_HmsEnemyjygem	 		)
		seqGfxShape(S_NULL			 , gfx_cloud, RM_XSURF 			)
		seqHmsShape(S_indy			 , RCP_HmsEnemyindy	 		)
		seqGfxShape(S_poohole		 , RCP_hole, RM_XDECAL	 		)
		seqGfxShape(S_stone			 , RCP_stone, RM_SPRITE	 		)
		seqGfxShape(S_lift0			 , gfx_testlift, RM_SURF 		)
		seqHmsShape(S_togezo		 , RCP_HmsEnemytogezo	 		)
		seqHmsShape(S_pipo			 , RCP_HmsEnemypipo	 		)
		seqHmsShape(S_wan_ball		 , RCP_wan_ball		 		)
		/*-------------------------------------------------------------------*/

		seqBeginScene(1, RCP_Stage37Scene1)

			/*------ test enemy's -----------------------------------------------*/
			seqActor( S_koura		,    1000, 0,0	, 0,0,0, 0,0,0, 	e_kame2		 )
			seqActor( S_NULL		,    3500, 0,   -2000, 0,0,0, 0,0,0, 	e_sanbo		 )
			seqActor( S_bat			,    1500, 500,0, 0,0,0, 0,1,0, 	e_bat		 )
			seqActor( S_bat			,    1500, 500,500, 0,0,0, 0,0,0, 	e_bat		 )
			seqActor( S_bat			,    2000, 500,0, 0,0,0, 0,0,0, 	e_bat		 )
			seqActor( S_bat			,    2000, 500,500, 0,0,0, 0,0,0, 	e_bat		 )
			seqActor( S_bat			,    -1500, 500,0, 0,0,0, 0,0,0, 	e_bat		 )
			seqActor( S_bat			,    -1500, 500,500, 0,0,0, 0,0,0, 	e_bat		 )
			seqActor( S_bat			,    -2000, 500,0, 0,0,0, 0,0,0, 	e_bat		 )
			seqActor( S_bat			,    -2000, 500,500, 0,0,0, 0,0,0, 	e_bat		 )
			seqActor( S_heyho		,    2000, 300,0, 0,0,0, 0,0,0, 	e_heyho		 )
			seqActor( S_wanwan		,   -2500, 0,-1500, 0,0,0, 0,0,0, 	e_wanwan	 )
			seqActor( S_wanwan		,    0,    0,-2000, 0,0,0, 0,0,0, 	e_wanwan	 )
			seqActor( S_wanwan		,   -2500, 0,1000, 0,0,0, 0,0,0, 	e_wanwan	 )
			seqActor( S_jugem		,    4000, 100,4000, 0,0,0, 0,0,0, 	e_jugem		 )
			seqActor( S_indy		,    0, 0,  4000, 0,0,0, 0,0,0, 	e_indy		 )
			seqActor( S_indy		,    0, 0,  4200, 0,0,0, 0,1,0, 	e_indy		 )
			seqActor( S_poohole		,    0, 0,  3800, 0,0,0, 0,0,0, 	e_hole		 )
			seqActor( S_poohole		,    200, 0,  4000, 0,0,0, 0,0,0, 	e_hole		 )
			seqActor( S_poohole		,    0, 0,  4200, 0,0,0, 0,0,0, 	e_hole		 )
			seqActor( S_poohole		,    -200, 0,  4000, 0,0,0, 0,0,0, 	e_hole		 )
			seqActor( S_lift0		,    3000, 300, -1000, 0,0,0, 0,0,0, 	e_linelift	 )
			seqActor( S_lift0		,    -4000, 300, 4000, 0,0,0, 0,0,0, 	e_seesaw	 )
			seqActor( S_nokonoko		,    3500, 0, 200, 0,0,0, 0,1,0, 	e_noko		 )
			seqActor( S_nokonoko_nude	,    3500, 0, 200, 0,0,0, 0,0,0, 	e_noko		 )
			seqActor( S_kuribo		,    3500, 0, 200, 0,0,0, 0,0,0, 	e_kuribo	 )
			seqActor( S_hanachan_head	,    3500, 0, 200, 0,0,0, 0,0,0, 	e_hanachan	 )
			seqActor( S_NULL		,    -3000, 700, 4000, 0,0,0, 0,0,0, 	e_pedallift	 )
			/*-------------------------------------------------------------------*/

			seqMapInfo(map37_info)

		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 0,  0,0,0)		/* mario start position (code,Yangle,Xpos,Ypos,Zpos) */
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit

/*===============================================================================
		End
===============================================================================*/


