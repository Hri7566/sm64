/********************************************************************************
						Ultra 64 MARIO Brothers

				   mario hold jump end animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 26, 1995
 ********************************************************************************/

#include "../headers.h"


static short mario_carry_jump_end_prm[]={
	    0,  154,  145,  134,  126,  125,  129,  136,
	  143,  147,  151,  154,  157,  157,  157,  157,
	  156,  155,  155,  154,  -12,  -14,  -16,  -16,
	  -15,  -12,  -10,   -7,   -4,   -1,    0,    1,
	    3,    4,    5,    6,    6,    7,    7,16383,
	-6194,-1660,-5781,-3002,-3016,-3030,-3002,-2931,
	-2850,-2759,-2663,-2562,-2459,-2358,-2259,-2166,
	-2081,-2006,-1943,-1895,-1865,-1854, 6481, 5844,
	 4696, 4025, 3942, 3899, 3890, 3909, 3953, 4016,
	 4093, 4179, 4270, 4360, 4445, 4520, 4579, 4619,
	 4633,-4235,-3902,-3306,-2910,-2773,-2642,-2518,
	-2401,-2291,-2190,-2098,-2014,-1941,-1877,-1824,
	-1782,-1751,-1732,-1726,28846,29730,31744,-32469,
	-32125,-31814,-31537,-31291,-31076,-30888,-30727,-30590,
	-30478,-30387,-30316,-30263,-30228,-30208,-30202, 2767,
	  525,-4654,-7069,-7107,-7145,-7069,-6884,-6668,
	-6430,-6175,-5909,-5639,-5371,-5111,-4866,-4641,
	-4443,-4278,-4153,-4073,-4045,-4291,-3781,-2871,
	-2356,-2321,-2322,-2354,-2411,-2489,-2582,-2687,
	-2798,-2909,-3017,-3117,-3203,-3270,-3314,-3330,
	 5732, 5291, 4540, 4070, 3944, 3833, 3736, 3653,
	 3582, 3522, 3473, 3433, 3401, 3377, 3359, 3347,
	 3340, 3336, 3335,30564,31578,-31665,-30207,-29896,
	-29637,-29425,-29255,-29123,-29025,-28957,-28913,-28889,
	-28882,-28885,-28896,-28909,-28920,-28925, -468, -661,
	 -573, -435, -329, -285, -264, -248, -220, -176,
	 -119,  -45,   62,  192,  307,  371,  323,  206,
	  140, -517, -472, -516, -575, -607, -598, -570,
	 -534, -502, -477, -452, -426, -413, -411, -387,
	 -310,  -85,  215,  365,-16154,-15354,-16715,-18436,
	-19448,-19364,-18759,-18000,-17453,-17244,-17195,-17218,
	-17295,-17429,-17568,-17655,-17638,-17562,-17516,14230,
	15327,16693,18056,18577,17916,16620,15172,14058,
	13475,13197,13073,13001,12973,12977,13001,13033,
	13061,13073, 5171, 4338, 4183, 4163, 4250, 4436,
	 4697, 4990, 5272, 5580, 5894, 6081, 6141, 6164,
	 6160, 6141, 6114, 6091, 6081, 5857, 6686, 6913,
	 7019, 7001, 6854, 6618, 6351, 6114, 5897, 5697,
	 5580, 5541, 5526, 5528, 5541, 5559, 5574, 5580,
	-24181,-26055,-26412,-26465,-26194,-25591,-24756,-23839,
	-22991,-22144,-21330,-20850,-20694,-20633,-20642,-20694,
	-20763,-20824,-20850, 1162,  843,  601,  415,  382,
	  298, -121,  -92,  -31, -109, -225, -300, -320,
	 -327, -326, -320, -311, -303, -300,   56,   26,
	   31,   33,    7,  -28,  -47,  -59,  -69,  -78,
	  -88,  -94,  -96,  -96,  -96,  -96,  -95,  -94,
	  -94,-16948,-17595,-18150,-18557,-18593,-18547,-18719,
	-18941,-19159,-19366,-19576,-19702,-19741,-19757,-19754,
	-19741,-19724,-19708,-19702,14517,16376,17337,17975,
	18064,17661,16890,15993,15282,14802,14428,14220,
	14143,14112,14117,14143,14177,14207,14220,-8397,
	-7821,-7370,-7044,-7024,-7311,-7892,-8564,-9122,
	-9570,-9969,-10201,-10280,-10310,-10306,-10280,-10245,
	-10214,-10201,-7165,-8490,-9246,-9805,-10031,-9815,
	-9147,-8318,-7624,-7073,-6575,-6286,-6188,-6150,
	-6155,-6188,-6231,-6269,-6286,-22339,-23268,-23965,
	-24392,-24217,-23515,-22362,-21083,-20012,-19100,-18261,
	-17772,-17608,-17544,-17553,-17608,-17681,-17744,-17772,
	-32767,-32767,18943,-16545,-32767,-32767,16753,16723,
	16693,16753,16900,17071,17260,17462,17673,17887,
	18099,18306,18500,18679,18836,18966,19066,19129,
	19152,-16545,-3158,-4293,-5383,-5307,-3485, -701,
	 2123, 4067, 4764, 4747, 4436, 4250, 4262, 4267,
	 4266, 4262, 4257, 4252, 4250, 5916, 7536, 9323,
	 9688, 8532, 6920, 5005, 2941,  883,-1015,-2599,
	-3716,-4274,-4378,-4160,-3757,-3302,-2931,-2777,
	15538,15630,15694,15725,15701,15612,15475,15305,
	15118,14929,14755,14611,14513,14477,14477,14477,
	14477,14477,14477
};
static short mario_carry_jump_end_tbl[]={
	    1,    0,	/* chn14translate x */
	   19,    1,	/* chn14 translate y */
	   19,   20,	/* chn14 translate z */
	    1,    0,	/* chn14 rotate x */
	    1,   39,	/* chn14 rotate y */
	    1,    0,	/* chn14 rotate z */
	    1,    0,	/* <m_waist>1 rotate x */
	    1,    0,	/* <m_waist>1 rotate y */
	   19,  528,	/* <m_waist>1 rotate z */
	    1,    0,	/* <m_body>1_2 rotate x */
	    1,    0,	/* <m_body>1_2 rotate y */
	   19,  509,	/* <m_body>1_2 rotate z */
	    1,    0,	/* <m_head>1 rotate x */
	    1,    0,	/* <m_head>1 rotate y */
	   19,  490,	/* <m_head>1 rotate z */

	    1,  468,	/* chn6_2 rotate x */
	    1,  469,	/* chn6_2 rotate y */
	   19,  470,	/* chn6_2 rotate z */
	   19,   62,	/* <m_larmA>1 rotate x */
	   19,   81,	/* <m_larmA>1 rotate y */
	   19,  100,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1_1 rotate x */
	    1,    0,	/* <m_larmB>1_1 rotate y */
	   19,   43,	/* <m_larmB>1_1 rotate z */
	    1,   40,	/* <m_lhand>1_1 rotate x */
	    1,   41,	/* <m_lhand>1_1 rotate y */
	    1,   42,	/* <m_lhand>1_1 rotate z */

	    1,  464,	/* chn10_2 rotate x */
	    1,  465,	/* chn10_2 rotate y */
	    1,  466,	/* chn10_2 rotate z */
	   19,  141,	/* <m_rarmA>1 rotate x */
	   19,  160,	/* <m_rarmA>1 rotate y */
	   19,  179,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1_2 rotate x */
	    1,    0,	/* <m_rarmB>1_2 rotate y */
	   19,  122,	/* <m_rarmB>1_2 rotate z */
	    1,  119,	/* <m_rhand>1_1 rotate x */
	    1,  120,	/* <m_rhand>1_1 rotate y */
	    1,  121,	/* <m_rhand>1_1 rotate z */

	    1,    0,	/* chn15_2 rotate x */
	    1,    0,	/* chn15_2 rotate y */
	    1,  467,	/* chn15_2 rotate z */
	   19,  407,	/* <m_llegA>1_2 rotate x */
	   19,  426,	/* <m_llegA>1_2 rotate y */
	   19,  445,	/* <m_llegA>1_2 rotate z */
	    1,    0,	/* <m_llegB>1_2 rotate x */
	    1,    0,	/* <m_llegB>1_2 rotate y */
	   19,  388,	/* <m_llegB>1_2 rotate z */
	   19,  331,	/* <m_ltoot>1_2 rotate x */
	   19,  350,	/* <m_ltoot>1_2 rotate y */
	   19,  369,	/* <m_ltoot>1_2 rotate z */

	    1,    0,	/* chn17_2 rotate x */
	    1,    0,	/* chn17_2 rotate y */
	    1,  489,	/* chn17_2 rotate z */
	   19,  274,	/* <m_rlegA>1_2 rotate x */
	   19,  293,	/* <m_rlegA>1_2 rotate y */
	   19,  312,	/* <m_rlegA>1_2 rotate z */
	    1,    0,	/* <m_rlegB>1_2 rotate x */
	    1,    0,	/* <m_rlegB>1_2 rotate y */
	   19,  255,	/* <m_rlegB>1_2 rotate z */
	   19,  198,	/* <m_rtoot>1 rotate x */
	   19,  217,	/* <m_rtoot>1 rotate y */
	   19,  236,	/* <m_rtoot>1 rotate z */
};
AnimeRecord animMarioHoldJumpEnd = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	19,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_carry_jump_end_prm,
	mario_carry_jump_end_tbl
};
