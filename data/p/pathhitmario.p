/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: pathhitmario
					Description	: 
					Date		: 1995.
					Author		: H.yajima

 ********************************************************************************/


/*################*/
#ifdef ASSEMBLER
/*################*/

/*################*/
#else
/*################*/

/********************************************************************************
		C Program
 ********************************************************************************/

extern unsigned long MarioProc1(int code,MapNode *node, void *data)
{


	if (code == MAP_CBACK_EXEC) {

		AffineMtx	 modelmtx;
	  	StrategyRecord	*stp = (StrategyRecord *)hmsActiveShape;

		if ( stp == player1stp && stp->childobj != NULL ){
			s_calc_skeleton_glbmtx(&modelmtx,(AffineMtx *)data,hmsActiveCamera->matrix);
			s_calc_skeleton_glbpos(modelmtx,stp->childobj);
			s_copy_worldXYZmappos(stp->childobj);
		}
	}

	return(0);

}

/*################*/
#endif
/*################*/
/*===============================================================================
		end end end end end end end end 
===============================================================================*/






