/********************************************************************************
						Ultra 64 MARIO Brothers

				  mario punching base 2 animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							February 8, 1996
 ********************************************************************************/

#include "../headers.h"


static short mario_2nd_punch_base_prm[]={
	    0,   -7,   -8,   -9,  -10,  158,  161,  164,
	  167,   32,   33,   35,   36,16383,-3203,-6097,
	-5663,-4168,-2376,-3005,-3591,-3800,-5449,-5355,
	-5246,-5022,-16332,-11426,-6456,-2920,-15180,-17978,
	28343,19070,-4304,-5077,-12432,-9965,-3276,-7854,
	15191,23795, -542, -510, -477, -444,  -83,  -78,
	  -75,  -77,-3893,-3825,-3742,-3636,-15963,-18200,
	-19266,-18470,15531, 9286, 7941, 7866, 9288, 7680,
	 6030, 6348,-9941,-16817,-16707,-14285,  924,  943,
	  953,  964, -400, -545, -694, -838,-20239,-19920,
	-19522,-19081,17335,17187,16973,16664, 4881, 5063,
	 5280, 5549, 4275, 3993, 3884, 4085,-17911,-17422,
	-16937,-16360, -169, -142, -197, -398,  194,  178,
	  163,  146,-19370,-19137,-18975,-18950,12971,12695,
	12452,12278,-7198,-7559,-7377,-6338,-4294,-4632,
	-4610,-3990,-23750,-23371,-22952,-22405,-32767,-32767,
	16753,-16545,-32767,-32767,16753,-16545,  625, 6165,
	10049,11793,  787, 1432, 1788, 1702,-3564,-4170,
	-4474,-4303,-2431,-7210,-10638,-12366, -361, 1201,
	 2515, 3595, 5175, 3080, 1486,  402, 1453,  666,
	  121,  -83, -426, -411, -281,   98,19678,19433,
	19140,18764
};
static short mario_2nd_punch_base_tbl[]={
	    4,    1,	/* chn14_2_1translate x */
	    4,    5,	/* chn14_2_1 translate y */
	    4,    9,	/* chn14_2_1 translate z */
	    1,    0,	/* chn14_2_1 rotate x */
	    1,   13,	/* chn14_2_1 rotate y */
	    1,    0,	/* chn14_2_1 rotate z */
	    4,  158,	/* <m_waist>1_3 rotate x */
	    4,  162,	/* <m_waist>1_3 rotate y */
	    4,  166,	/* <m_waist>1_3 rotate z */
	    4,  146,	/* <m_body>1 rotate x */
	    4,  150,	/* <m_body>1 rotate y */
	    4,  154,	/* <m_body>1 rotate z */
	    4,  134,	/* <m_head>2 rotate x */
	    4,  138,	/* <m_head>2 rotate y */
	    4,  142,	/* <m_head>2 rotate z */

	    1,  130,	/* chn6 rotate x */
	    1,  131,	/* chn6 rotate y */
	    1,  132,	/* chn6 rotate z */
	    4,   30,	/* <m_larmA>1 rotate x */
	    4,   34,	/* <m_larmA>1 rotate y */
	    4,   38,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	    4,   26,	/* <m_larmB>1 rotate z */
	    4,   14,	/* <m_lhand>1 rotate x */
	    4,   18,	/* <m_lhand>1 rotate y */
	    4,   22,	/* <m_lhand>1 rotate z */

	    1,  126,	/* chn10 rotate x */
	    1,  127,	/* chn10 rotate y */
	    1,  128,	/* chn10 rotate z */
	    4,   58,	/* <m_rarmA>1 rotate x */
	    4,   62,	/* <m_rarmA>1 rotate y */
	    4,   66,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	    4,   54,	/* <m_rarmB>1 rotate z */
	    4,   42,	/* <m_rhand>1 rotate x */
	    4,   46,	/* <m_rhand>1 rotate y */
	    4,   50,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  129,	/* chn15 rotate z */
	    4,  114,	/* <m_llegA>1 rotate x */
	    4,  118,	/* <m_llegA>1 rotate y */
	    4,  122,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	    4,  110,	/* <m_llegB>1 rotate z */
	    4,   98,	/* <m_ltoot>1 rotate x */
	    4,  102,	/* <m_ltoot>1 rotate y */
	    4,  106,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  133,	/* chn17 rotate z */
	    4,   86,	/* <m_rlegA>1 rotate x */
	    4,   90,	/* <m_rlegA>1 rotate y */
	    4,   94,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	    4,   82,	/* <m_rlegB>1 rotate z */
	    4,   70,	/* <m_rfoot>1 rotate x */
	    4,   74,	/* <m_rfoot>1 rotate y */
	    4,   78,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioSecPunchBase = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	 4,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_2nd_punch_base_prm,
	mario_2nd_punch_base_tbl
};
