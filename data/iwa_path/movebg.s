/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Strategy

				Feburary 14 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
						Move Bg Path Data
 ********************************************************************************/

/********************************************************************************/
/*		motos BG BRIGE path Program											    */
/********************************************************************************/
e_motos_bridge1:
	p_initialize(option)
	p_shapeOFF
	p_while
		p_program(s_motosbridge_main)
	p_loop

e_motos_bridge_parts:
	p_initialize(moveBG)
	p_setbit( flag,stf_moveON | stf_YangleSAME)
	p_setshapeinfo(motos_obj03_info)
	p_while
		p_program(s_motos_bridge)
		p_program(stMainMoveBG)
	p_loop

/*==============================================================================================*/
/*		Castle Move BG  path Program										    				*/
/*==============================================================================================*/

/********************************************************************************/
/*		dosuun bar path Program											    */
/********************************************************************************/
e_dossun_bar:
	p_initialize(moveBG)
	p_setbit( flag,stf_moveON | stf_FspeedON)
	p_setshapeinfo(dossun_bar_info)
	p_program(s_dossun_bar_init)

	p_while
		p_program(s_dossun_bar_main)
		p_program(stMainMoveBG)
	p_loop

/********************************************************************************/
/*		dosuun bar 2 path Program											    */
/********************************************************************************/
e_dossun_bar2:
	p_initialize(moveBG)
	p_setbit( flag,stf_moveON | stf_FspeedON)
	p_setshapeinfo(dossun_bar2_info)
	p_program(s_dossun_bar2_init)

	p_while
		p_program(s_dossun_bar2_main)
		p_program(stMainMoveBG)
	p_loop

/********************************************************************************/
/*		dosuun bar path Program											    */
/********************************************************************************/
e_trans_bar:
	p_initialize(moveBG)
	p_setbit( flag,stf_moveON | stf_FspeedON)
	p_setshapeinfo(transbar_50_info)
	p_program(s_trans_bar_init)

	p_while
		p_program(s_trans_bar_main)
		p_program(stMainMoveBG)
	p_loop





/*################*/
#else
/*################*/


/***************************************************************************************************
		C Program
 ****************************************************************************************************/

/***************************************************************************************************
						Motos Bridge Main
***************************************************************************************************/
extern void s_motosbridge_main(void)
{

	StrategyRecord *stratp1;
	StrategyRecord *stratp2;

		stratp1 = s_makeobj_nowpos(execstp,S_motos_obj03,e_motos_bridge_parts);
		stratp1->s[stw_angleY].d = execstp->s[stw_angleY].d;
		stratp1->s[stw_worldX].f += cos(execstp->s[stw_angleY].d)*(640);
		stratp1->s[stw_worldZ].f += sin(execstp->s[stw_angleY].d)*(640);

		stratp2 = s_makeobj_nowpos(execstp,S_motos_obj03,e_motos_bridge_parts);
		stratp2->s[stw_angleY].d = execstp->s[stw_angleY].d + 0x8000;
		stratp2->s[stw_worldX].f += cos(execstp->s[stw_angleY].d)*(-640);
		stratp2->s[stw_worldZ].f += sin(execstp->s[stw_angleY].d)*(-640);

		execstp->status = 0;		/* s_removeobj */	/* object remove */

}
/***************************************************************************************************
						Motos Bridge1
***************************************************************************************************/
extern void s_motos_bridge(void)
{
	
	ulong f_counter = frameCounter;

	switch( obj_mode ) {
		case 0 : execstp->s[stw_animeangleZ].d += 0x100; break;
		case 1 : execstp->s[stw_animeangleZ].d -= 0x100; break;
	}

	if ((short)execstp->s[stw_animeangleZ].d < -8189) {
		execstp->s[stw_animeangleZ].d = degree(315);
		if (obj_timer > 50 && (f_counter & 0x0000007)==0) {
			obj_mode = 0;
			objsound(NA_SE2_BRIDGEUP);
		}
	}	
		

	if ((short) execstp->s[stw_animeangleZ].d >= 0) {
		execstp->s[stw_animeangleZ].d = degree(0);
		if (obj_timer > 50 && (f_counter & 0x0000007)==0) {
			obj_mode = 1;
			objsound(NA_SE2_BRIDGEDOWN);
		}
	}	


}

/*==================================================================================================
								Castle BG Event
====================================================================================================*/

/*+++++++++++++++++++++++++++++++++ Dossun Bar ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/***************************************************************************************************
								Dossun Bar Event
***************************************************************************************************/
extern void s_dossun_bar_init(void)
{

	execstp->s[stw_animeangleY].d -= 0x4000;

	execstp->s[stw_work3].f = execstp->s[stw_worldX].f;


	obj_timer = (int)(Randomf()*100);

}
/***************************************************************************************************
								Dossun Bar Event
***************************************************************************************************/
extern void s_dossun_bar_main(void)
{

	switch (obj_mode) {
		case 0: if (obj_timer > 100) {
					obj_mode = 1;
					execstp->s[stw_speedF].f = 30;
				}
				break;
		case 1:	if (execstp->s[stw_worldX].f > 3450)  {	execstp->s[stw_worldX].f = 3450; execstp->s[stw_speedF].f = 0;}
				if (obj_timer == FRAMENUM*0.5) {
					obj_mode = 2;
					execstp->s[stw_speedF].f = 40;
					objsound(NA_SE3_OSIDASI);
				}
				break;

		case 2:	if (execstp->s[stw_worldX].f > 3830)  {execstp->s[stw_worldX].f = 3830; execstp->s[stw_speedF].f = 0;}
				if (obj_timer == FRAMENUM*2) {
					obj_mode = 3;
					execstp->s[stw_speedF].f = 10;
					execstp->s[stw_angleY].d -= 0x8000;
					objsound(NA_SE3_OSIDASI);
				}
				break;

		case 3:	if (execstp->s[stw_worldX].f < 3330)  {execstp->s[stw_worldX].f = 3330;execstp->s[stw_speedF].f = 0;}
				if (obj_timer == FRAMENUM*3) {
					obj_mode = 1;
					execstp->s[stw_speedF].f = 25;
					execstp->s[stw_angleY].d -= 0x8000;
				}
				break;
	}



}
/*+++++++++++++++++++++++++++++++++ Dossun Bar2 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/***************************************************************************************************
								Dossun Bar Event
***************************************************************************************************/
extern void s_dossun_bar2_init(void)
{


	execstp->s[stw_angleY].d += 0x4000;
	obj_timer = (int)(Randomf()*100);

}
/***************************************************************************************************
								Dossun Bar Event
***************************************************************************************************/
extern void s_dossun_bar2_main(void)
{


	switch (obj_mode) {
		case 0: if (obj_timer > 100) {
					obj_mode = 1;
					execstp->s[stw_speedF].f = 30;
				}
				break;
		case 1:	if (execstp->s[stw_worldX].f > 3450)  {execstp->s[stw_worldX].f = 3450;execstp->s[stw_speedF].f = 0;}
				if (obj_timer == FRAMENUM*0.5) {
					obj_mode = 2;
					execstp->s[stw_speedF].f = 10;
					objsound(NA_SE3_OSIDASI);
				}
				break;

		case 2:	if (execstp->s[stw_worldX].f > 3830)  {execstp->s[stw_worldX].f = 3830;execstp->s[stw_speedF].f = 0;}
				if (obj_timer == FRAMENUM*2) {
					obj_mode = 3;
					execstp->s[stw_speedF].f = 10;
					execstp->s[stw_angleY].d -= 0x8000;
					objsound(NA_SE3_OSIDASI);
				}
				break;

		case 3:	if (execstp->s[stw_worldX].f < 3330)  {execstp->s[stw_worldX].f = 3330;execstp->s[stw_speedF].f = 0;}
				if (obj_timer == FRAMENUM*3) {
					obj_mode = 1;
					execstp->s[stw_speedF].f = 25;
					execstp->s[stw_angleY].d -= 0x8000;
				}
				break;
	}

}


/*+++++++++++++++++++++++++++++++++ trans Bar2 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define trans_speed		(execstp->s[stw_work0].f)
/***************************************************************************************************
								Dossun Bar Event
***************************************************************************************************/
extern void s_trans_bar_init(void)
{

	execstp->s[stw_animeangleY].d -= 0x4000;

	obj_worldX += 2;
	obj_attX = obj_worldX;

	switch (obj_programselect) {
		case 1:	trans_speed = 10.;
				break;
		case 2:	trans_speed = 15.;
				break;
		case 3:	trans_speed = 20.;
				break;
	}


	obj_timer = (int)(Randomf()*100);

}
/***************************************************************************************************
								trans Bar Event
***************************************************************************************************/
extern void s_trans_bar_main(void)
{


	switch (obj_mode) {
		case 0: if (obj_timer > 100) {
					obj_mode = 1;
					execstp->s[stw_speedF].f = trans_speed;
				}
				break;
		case 1:	if (obj_timer >=  500/trans_speed) {
					obj_speedF = 0;
					obj_worldX = obj_attX+510;
				}
				if (obj_timer == FRAMENUM*2) {
					obj_mode = 2;
					execstp->s[stw_speedF].f = trans_speed;
					execstp->s[stw_angleY].d -= 0x8000;
				}
				break;

		case 2:	if (obj_timer >= 500/trans_speed) {
					 obj_speedF = 0;
					 obj_worldX = obj_attX;
				}
				if (obj_timer == FRAMENUM*3) {
					obj_mode = 1;
					execstp->s[stw_speedF].f = trans_speed;
					execstp->s[stw_angleY].d -= 0x8000;
				}
				break;
	}

}




/*################*/
#endif
/*################*/


