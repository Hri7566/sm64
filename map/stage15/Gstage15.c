/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage15 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#include "fire_bubble2/fb2_ext_texture.h"

#include "fire_bubble2/e8_base_shape.sou"

#include "fire_bubble2/e8_bg01_shape.sou"
#include "fire_bubble2/e8_bg02_shape.sou"
#include "fire_bubble2/e8_bg03_shape.sou"
#include "fire_bubble2/e8_bg04_shape.sou"
#include "fire_bubble2/e8_bg05_shape.sou"
#include "fire_bubble2/e8_bg06_shape.sou"
#include "fire_bubble2/e8_bg07_shape.sou"
#include "fire_bubble2/e8_bg08_shape.sou"
#include "fire_bubble2/e8_bg09_shape.sou"
#include "fire_bubble2/e8_bg10_shape.sou"
#include "fire_bubble2/e8_bg10_ana_shape.sou"
#include "fire_bubble2/e8_bg11_shape.sou"
#include "fire_bubble2/e8_bg12_shape.sou"
#include "fire_bubble2/e8_bg13_shape.sou"
#include "fire_bubble2/e8_bg14_shape.sou"
#include "fire_bubble2/e8_bg15_shape.sou"
#include "fire_bubble2/e8_bg16_shape.sou"
#include "fire_bubble2/e8_bg17_shape.sou"
#include "fire_bubble2/e8_bg18_shape.sou"
#include "fire_bubble2/e8_bg19_shape.sou"
#include "fire_bubble2/e8_bg20_shape.sou"

#include "fire_bubble2/e8_arajin_shape.sou"
#include "fire_bubble2/e8_buranko_shape.sou"
#include "fire_bubble2/e8_guruguru_shape.sou"
#include "fire_bubble2/e8_hane_shape.sou"
#include "fire_bubble2/e8_kaiten_shape.sou"
#include "fire_bubble2/e8_rifuto01_shape.sou"
#include "fire_bubble2/e8_rifuto02_shape.sou"
#include "fire_bubble2/e8_rifuto03_shape.sou"
#include "fire_bubble2/e8_shiso_shape.sou"
#include "fire_bubble2/e8_tikuwa01_shape.sou"
#include "fire_bubble2/e8_tikuwa02_shape.sou"
#include "fire_bubble2/e8_yokoshiso_shape.sou"
#include "fire_bubble2/e8_dorifu00_shape.sou"
#include "fire_bubble2/e8_dorifu01_shape.sou"
#include "fire_bubble2/e8_dorifu02_shape.sou"
#include "fire_bubble2/e8_dorifu03_shape.sou"
#include "fire_bubble2/e8_dorifu04_shape.sou"

#include "fire_bubble2/e8_arajin_check.flk"
#include "fire_bubble2/e8_buranko_check.flk"
#include "fire_bubble2/e8_guruguru_check.flk"
#include "fire_bubble2/e8_kaiten_check.flk"
#include "fire_bubble2/e8_rifuto01_check.flk"
#include "fire_bubble2/e8_rifuto02_check.flk"
#include "fire_bubble2/e8_rifuto03_check.flk"
#include "fire_bubble2/e8_shiso_check.flk"
#include "fire_bubble2/e8_tikuwa_check.flk"
#include "fire_bubble2/e8_yokoshiso_check.flk"
#include "fire_bubble2/e8_dorifu00_check.flk"
#include "fire_bubble2/e8_dorifu01_check.flk"
#include "fire_bubble2/e8_dorifu02_check.flk"
#include "fire_bubble2/e8_dorifu03_check.flk"
#include "fire_bubble2/e8_dorifu04_check.flk"

#include "fire_bubble2/cx1501.flk"
#include "fire_bubble2/cx1501.tag"

#include "fire_bubble2/niji_root1.ral"
#include "fire_bubble2/niji_root2.ral"
#include "fire_bubble2/niji_root3.ral"
#include "fire_bubble2/niji_root4.ral"
