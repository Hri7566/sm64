/********************************************************************************
						Ultra 64 MARIO Brothers

						  frame buffer module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							April 30, 1995
 ********************************************************************************/

#include "../headers.h"

Pixmap	frameMemoryA;
Pixmap	frameMemoryB;
Pixmap	frameMemoryC;
