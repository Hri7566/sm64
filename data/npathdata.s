/********************************************************************************
						Ultra 64 MARIO Brothers

							path data module

			Copyright 1996 Nintendo co., ltd.  All rights reserved

						   April 26, 1996
 ********************************************************************************/


#define ASSEMBLER

#include "../headers.h"

/********************************************************************************/
/*		Start AsmCode															*/
/********************************************************************************/

	.data
	.align	2
	.align	0

/********************************************************************************/
/*		globl labels															*/
/********************************************************************************/

	.globl	e_mario
	.globl	e_decor_kinopio
	.globl	e_stardoor_effect

	.globl	e_player_waiting
	.globl	e_player_landing
	.globl	e_player_falling
	.globl	e_player_rolling
	.globl	e_player_downing
	.globl	e_player_entpict
	.globl	e_player_flight
	.globl	e_player_entwinner
	.globl	e_player_entloser
	.globl	e_player_landloser
	.globl	e_player_landwinner
	.globl	e_player_pushout
	.globl	e_player_pushdown
	.globl	e_player_swimming

/********************************************************************************/
/*																				*/
/* << PLAYER >> Mario(Player1) PathData											*/
/*																				*/
/********************************************************************************/

e_mario:
	p_initialize(player)
	p_hitON
	p_setbit( flag,stf_playerON )
	p_setbit( my_status,code_player )
	p_sethitbox(150/4,640/4)
	p_while
		p_program(s_playerdebug)
		p_program(s_playermove)
		p_program(s_debug)
	p_loop

/********************************************************************************/
/*																				*/
/* << KINOPIO >> Decor kinopio													*/
/*																				*/
/********************************************************************************/

e_decor_kinopio:
	p_initialize(enemyA)
	p_setbit(flag, stf_moveON | stf_playerdistON | stf_YangleSAME | stf_NOramsave)
	p_set_pointer(skelanime, kinopio_anime)
	p_set_skelanime_number(6)
	p_setobjname(FRIEND)
/*	p_setd(enemyinfo,ENEMYINFO_MESSAGE)	*/
	p_sethitbox(80,100)
/*	p_setdamagebox(80,100)				*/
	p_hitON
	p_program(s_areastage_init)
	p_program(InitDecorKinopio)
	p_while
		p_program(ExecDecorKinopio)
	p_loop

/********************************************************************************/
/*																				*/
/* << KINOPIO >> Decor kinopio													*/
/*																				*/
/********************************************************************************/

e_stardoor_effect:
	p_initialize(item)
	p_setbit(flag,stf_moveON | stf_YangleSAME )
	p_program(InitStarDoorEffect)

	p_while
		p_program(ExecStarDoorEffect)
	p_loop

/********************************************************************************/
/*																				*/
/* Dummy path data for mario entrant											*/
/*																				*/
/********************************************************************************/

e_player_waiting:		p_end
e_player_landing:		p_end
e_player_falling:		p_end
e_player_rolling:		p_end
e_player_downing:		p_end
e_player_entpict:		p_end
e_player_flight:		p_end
e_player_entwinner:		p_end
e_player_entloser:		p_end
e_player_landloser:		p_end
e_player_landwinner:	p_end
e_player_pushout:		p_end
e_player_pushdown:		p_end
e_player_swimming:		p_end

/*==============================================================================*/
/*==============================================================================*/
/*==============================================================================*/
/*==============================================================================*/
/*==============================================================================*/
