/********************************************************************************
						Ultra 64 MARIO Brothers

					  mario rolling animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							July 11, 1995
 ********************************************************************************/

#include "../headers.h"


static short mario_rolling_1_0_prm[]={
	    0,  144,16383,  168,-1660,-5781,-14184, 4358,
	-6510,-19417,-4724,-14706,-3062, 7783,-19267,   27,
	  603,-13531,23294,  141, -309,-31828,  149, -247,
	-13246,22320,  -70,   81,-31279,32767,32767,16753,
	-16545,32767,32767,16753,-16545,10859, 3953,16383,
	22856,29328,35800,42273,48745,55218,61690,68163,
	74635
};

static short mario_rolling_1_0_tbl[]={
	    1,    0,	/* chn14translate x */
	    1,    1,	/* chn14 translate y */
	    1,    0,	/* chn14 translate z */
	    1,    0,	/* chn14 rotate x */
	    1,    2,	/* chn14 rotate y */
	    1,    0,	/* chn14 rotate z */
	    1,    0,	/* <m_waist>1_3 rotate x */
	    1,    0,	/* <m_waist>1_3 rotate y */
	   10,   39,	/* <m_waist>1_3 rotate z */
	    1,    0,	/* <m_body>1 rotate x */
	    1,    0,	/* <m_body>1 rotate y */
	    1,   38,	/* <m_body>1 rotate z */
	    1,    0,	/* <m_head>2 rotate x */
	    1,    0,	/* <m_head>2 rotate y */
	    1,   37,	/* <m_head>2 rotate z */

	    1,   33,	/* chn6 rotate x */
	    1,   34,	/* chn6 rotate y */
	    1,   35,	/* chn6 rotate z */
	    1,    7,	/* <m_larmA>1 rotate x */
	    1,    8,	/* <m_larmA>1 rotate y */
	    1,    9,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	    1,    6,	/* <m_larmB>1 rotate z */
	    1,    3,	/* <m_lhand>1 rotate x */
	    1,    4,	/* <m_lhand>1 rotate y */
	    1,    5,	/* <m_lhand>1 rotate z */

	    1,   29,	/* chn10 rotate x */
	    1,   30,	/* chn10 rotate y */
	    1,   31,	/* chn10 rotate z */
	    1,   12,	/* <m_rarmA>1 rotate x */
	    1,   13,	/* <m_rarmA>1 rotate y */
	    1,   14,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	    1,   11,	/* <m_rarmB>1 rotate z */
	    1,    0,	/* <m_rhand>1 rotate x */
	    1,    0,	/* <m_rhand>1 rotate y */
	    1,   10,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,   32,	/* chn15 rotate z */
	    1,   26,	/* <m_llegA>1 rotate x */
	    1,   27,	/* <m_llegA>1 rotate y */
	    1,   28,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	    1,   25,	/* <m_llegB>1 rotate z */
	    1,   22,	/* <m_ltoot>1 rotate x */
	    1,   23,	/* <m_ltoot>1 rotate y */
	    1,   24,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,   36,	/* chn17 rotate z */
	    1,   19,	/* <m_rlegA>1 rotate x */
	    1,   20,	/* <m_rlegA>1 rotate y */
	    1,   21,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	    1,   18,	/* <m_rlegB>1 rotate z */
	    1,   15,	/* <m_rfoot>1 rotate x */
	    1,   16,	/* <m_rfoot>1 rotate y */
	    1,   17,	/* <m_rfoot>1 rotate z */
};

AnimeRecord animMarioRolling = {
	 MAP_ANIM_NORMAL,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	10,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_rolling_1_0_prm,
	mario_rolling_1_0_tbl
};
AnimeRecord animMarioBackRolling = {
	 MAP_ANIM_REVERSE,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	10,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_rolling_1_0_prm,
	mario_rolling_1_0_tbl
};
