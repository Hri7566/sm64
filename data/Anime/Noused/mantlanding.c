/********************************************************************************
						Ultra 64 MARIO Brothers

					mario mant landing animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

						   September 19, 1995
 ********************************************************************************/

#include "../headers.h"


static short mario_mant_fly_landon_prm[]={
	    0,  167,  166,  162,  156,  150,  142,  134,
	  125,  117,  111,16383,  138,  135,  126,  112,
	   96,   76,   56,   36,   16,    0,-3019,-2987,
	-2900,-2769,-2604,-2417,-2218,-2020,-1833,-1668,
	-5588,-5568,-5512,-5428,-5323,-5203,-5076,-4949,
	-4830,-4724,-1773,-1884,-2192,-2657,-3240,-3902,
	-4602,-5303,-5965,-6548,-18270,-18155,-17836,-17355,
	-16752,-16067,-15342,-14617,-13932,-13329,-15276,-15449,
	-15928,-16651,-17558,-18587,-19678,-20768,-21797,-22704,
	-13507,-13260,-12575,-11541,-10245,-8774,-7215,-5657,
	-4186,-2890,   58,   57,   53,   47,   40,   32,
	   23,   15,    7,    0, 1354, 1323, 1235, 1103,
	  938,  750,  551,  353,  165,    0,-5138,-5082,
	-4928,-4694,-4402,-4070,-3719,-3367,-3035,-2743,
	-2398,-2495,-2763,-3167,-3673,-4248,-4857,-5466,
	-6041,-6548,18783,19468,21364,24226,27815,31887,
	36202,40516,44588,48177,14822,14754,14566,14283,
	13927,13523,13095,12667,12263,11908,-13276,-12244,
	-9390,-5079,  324, 6456,12953,19450,25583,30987,
	    3,  -17,  -75, -163, -273, -398, -531, -664,
	 -789, -899, -238, -238, -236, -234, -231, -227,
	 -224, -220, -217, -214,-16001,-15997,-15987,-15972,
	-15953,-15932,-15909,-15886,-15865,-15846, 3994, 4325,
	 5240, 6622, 8354,10320,12403,14485,16451,18184,
	 1299, 1306, 1324, 1351, 1386, 1425, 1466, 1508,
	 1547, 1581, 2154, 2253, 2526, 2939, 3457, 4044,
	 4667, 5289, 5877, 6395,-23087,-23270,-23774,-24537,
	-25493,-26578,-27727,-28876,-29961,-30917,   14,   14,
	   13,   12,   10,    8,    6,    3,    1,    0,
	    6,    3,   -3,  -14,  -27,  -42,  -59,  -75,
	  -90, -104,-16274,-16361,-16603,-16967,-17424,-17943,
	-18492,-19042,-19561,-20018, 5434, 5787, 6764, 8239,
	10088,12187,14411,16634,18733,20583,-1607,-1673,
	-1854,-2129,-2473,-2863,-3277,-3690,-4080,-4424,
	-1732,-1913,-2413,-3170,-4118,-5194,-6334,-7474,
	-8550,-9499,-23778,-23866,-24109,-24477,-24938,-25460,
	-26014,-26568,-27091,-27552,32767,32767,16753,-16545,
	32767,32767,16753,-16545, 3171, 3151, 3095, 3011,
	 2905, 2785, 2659, 2532, 2412, 2306, 2282, 2370,
	 2612, 2978, 3436, 3957, 4508, 5060, 5580, 6039,
	13652,13720,13906,14188,14541,14942,15367,15791,
	16192,16545
};
static short mario_mant_fly_landon_tbl[]={
	    1,    0,	/* chn14translate x */
	   10,    1,	/* chn14 translate y */
	    1,    0,	/* chn14 translate z */
	    1,    0,	/* chn14 rotate x */
	    1,   11,	/* chn14 rotate y */
	    1,    0,	/* chn14 rotate z */
	    1,    0,	/* <m_waist>1_3 rotate x */
	    1,    0,	/* <m_waist>1_3 rotate y */
	   10,  320,	/* <m_waist>1_3 rotate z */
	    1,    0,	/* <m_body>1 rotate x */
	    1,    0,	/* <m_body>1 rotate y */
	   10,  310,	/* <m_body>1 rotate z */
	    1,    0,	/* <m_head>2 rotate x */
	    1,    0,	/* <m_head>2 rotate y */
	   10,  300,	/* <m_head>2 rotate z */

	    1,  296,	/* chn6 rotate x */
	    1,  297,	/* chn6 rotate y */
	    1,  298,	/* chn6 rotate z */
	   10,   52,	/* <m_larmA>1 rotate x */
	   10,   62,	/* <m_larmA>1 rotate y */
	   10,   72,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	   10,   42,	/* <m_larmB>1 rotate z */
	   10,   12,	/* <m_lhand>1 rotate x */
	   10,   22,	/* <m_lhand>1 rotate y */
	   10,   32,	/* <m_lhand>1 rotate z */

	    1,  292,	/* chn10 rotate x */
	    1,  293,	/* chn10 rotate y */
	    1,  294,	/* chn10 rotate z */
	   10,  122,	/* <m_rarmA>1 rotate x */
	   10,  132,	/* <m_rarmA>1 rotate y */
	   10,  142,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	   10,  112,	/* <m_rarmB>1 rotate z */
	   10,   82,	/* <m_rhand>1 rotate x */
	   10,   92,	/* <m_rhand>1 rotate y */
	   10,  102,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  295,	/* chn15 rotate z */
	   10,  262,	/* <m_llegA>1 rotate x */
	   10,  272,	/* <m_llegA>1 rotate y */
	   10,  282,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	   10,  252,	/* <m_llegB>1 rotate z */
	   10,  222,	/* <m_ltoot>1 rotate x */
	   10,  232,	/* <m_ltoot>1 rotate y */
	   10,  242,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  299,	/* chn17 rotate z */
	   10,  192,	/* <m_rlegA>1 rotate x */
	   10,  202,	/* <m_rlegA>1 rotate y */
	   10,  212,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	   10,  182,	/* <m_rlegB>1 rotate z */
	   10,  152,	/* <m_rfoot>1 rotate x */
	   10,  162,	/* <m_rfoot>1 rotate y */
	   10,  172,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioMantLanding = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	10,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_mant_fly_landon_prm,
	mario_mant_fly_landon_tbl
};
