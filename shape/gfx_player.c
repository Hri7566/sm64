/********************************************************************************
						Ultra 64 MARIO Brothers

				   player graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 12, 1995
 ********************************************************************************/

#include "../headers.h"


#include "Player/gfx_mario.sou"
#include "Player/gfx_effect.sou"
