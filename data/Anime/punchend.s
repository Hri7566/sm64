/********************************************************************************
						Ultra 64 MARIO Brothers

					 mario punch end animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							February 6, 1996
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioPunchEnd:
	.half	 MAP_ANIM_ONETIME 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	10 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x0410						/* total bytes       */


	.half	   10,    1 	/* chn14_2_2translate x */
	.half	   10,   11 	/* chn14_2_2 translate y */
	.half	   10,   21 	/* chn14_2_2 translate z */
	.half	    1,    0 	/* chn14_2_2 rotate x */
	.half	    1,   31 	/* chn14_2_2 rotate y */
	.half	    1,    0 	/* chn14_2_2 rotate z */
	.half	   10,  352 	/* <m_waist>1_3 rotate x */
	.half	   10,  362 	/* <m_waist>1_3 rotate y */
	.half	   10,  372 	/* <m_waist>1_3 rotate z */
	.half	   10,  322 	/* <m_body>1 rotate x */
	.half	   10,  332 	/* <m_body>1 rotate y */
	.half	   10,  342 	/* <m_body>1 rotate z */
	.half	   10,  292 	/* <m_head>2 rotate x */
	.half	   10,  302 	/* <m_head>2 rotate y */
	.half	   10,  312 	/* <m_head>2 rotate z */
	.half	    1,  288 	/* chn6 rotate x */
	.half	    1,  289 	/* chn6 rotate y */
	.half	    1,  290 	/* chn6 rotate z */
	.half	   10,   44 	/* <m_larmA>1 rotate x */
	.half	   10,   54 	/* <m_larmA>1 rotate y */
	.half	   10,   64 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1 rotate x */
	.half	    1,    0 	/* <m_larmB>1 rotate y */
	.half	   10,   34 	/* <m_larmB>1 rotate z */
	.half	    1,    0 	/* <m_lhand>1 rotate x */
	.half	    1,   32 	/* <m_lhand>1 rotate y */
	.half	    1,   33 	/* <m_lhand>1 rotate z */
	.half	    1,  284 	/* chn10 rotate x */
	.half	    1,  285 	/* chn10 rotate y */
	.half	    1,  286 	/* chn10 rotate z */
	.half	   10,  114 	/* <m_rarmA>1 rotate x */
	.half	   10,  124 	/* <m_rarmA>1 rotate y */
	.half	   10,  134 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1 rotate y */
	.half	   10,  104 	/* <m_rarmB>1 rotate z */
	.half	   10,   74 	/* <m_rhand>1 rotate x */
	.half	   10,   84 	/* <m_rhand>1 rotate y */
	.half	   10,   94 	/* <m_rhand>1 rotate z */
	.half	    1,    0 	/* chn15 rotate x */
	.half	    1,    0 	/* chn15 rotate y */
	.half	    1,  287 	/* chn15 rotate z */
	.half	   10,  254 	/* <m_llegA>1 rotate x */
	.half	   10,  264 	/* <m_llegA>1 rotate y */
	.half	   10,  274 	/* <m_llegA>1 rotate z */
	.half	    1,    0 	/* <m_llegB>1 rotate x */
	.half	    1,    0 	/* <m_llegB>1 rotate y */
	.half	   10,  244 	/* <m_llegB>1 rotate z */
	.half	   10,  214 	/* <m_ltoot>1 rotate x */
	.half	   10,  224 	/* <m_ltoot>1 rotate y */
	.half	   10,  234 	/* <m_ltoot>1 rotate z */
	.half	    1,    0 	/* chn17 rotate x */
	.half	    1,    0 	/* chn17 rotate y */
	.half	    1,  291 	/* chn17 rotate z */
	.half	   10,  184 	/* <m_rlegA>1 rotate x */
	.half	   10,  194 	/* <m_rlegA>1 rotate y */
	.half	   10,  204 	/* <m_rlegA>1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1 rotate y */
	.half	   10,  174 	/* <m_rlegB>1 rotate z */
	.half	   10,  144 	/* <m_rfoot>1 rotate x */
	.half	   10,  154 	/* <m_rfoot>1 rotate y */
	.half	   10,  164 	/* <m_rfoot>1 rotate z */

	.half	0x0000,0x0007,0x000E,0x000E,0x000E,0x000C,0x0006,0x0002,0x0000,0x0000,0x0000,0x0098,0x009A,0x009A,0x009A,0x009A
	.half	0x009A,0x009D,0x00A1,0x00A6,0x00AA,0x0014,0x001B,0x001A,0x0018,0x0015,0x000A,0x0004,0x0000,0x0000,0x0000,0x3FFF
	.half	0xF97C,0xED8C,0xD2B6,0xCFBD,0xD140,0xD4A2,0xD887,0xDB96,0xDC74,0xD772,0xCEDD,0xCA4F,0xF645,0xD708,0xD37F,0xD533
	.half	0xDAAB,0xE26F,0xEB07,0xF2FC,0xF8D6,0xFB1D,0xD798,0xD633,0xD588,0xD4E8,0xD457,0xD3D8,0xD36F,0xD320,0xD2EE,0xD2DC
	.half	0xD968,0xFA3D,0xFDFE,0xFC42,0xF693,0xEE7A,0xE581,0xDD32,0xD715,0xD4B4,0x08E5,0x00EB,0xFE2C,0xFDDE,0xFE1D,0xFE3C
	.half	0xFEB2,0xFF48,0xFFC9,0x0000,0x06E6,0x0211,0x0056,0x0000,0xFFF5,0xFFCC,0xFFCA,0xFFDD,0xFFF5,0x0000,0xE8B6,0xEC09
	.half	0xEDCB,0xEEDB,0xEFD7,0xF126,0xF297,0xF3ED,0xF4E8,0xF549,0xE9D6,0xE3FA,0xDE97,0xD8F9,0xD476,0xD0E7,0xCD7F,0xCA9D
	.half	0xC89D,0xC7DD,0x78CB,0x9272,0xA705,0xB91B,0xC674,0xBB5C,0x46D3,0x2E34,0x144A,0x054B,0x22C7,0x1F5A,0x1AEB,0x169C
	.half	0x1746,0x326B,0x2CC5,0x2691,0x2920,0x2BD6,0x3B60,0x5099,0x65DD,0x7AC5,0x8A2A,0x7C2C,0xFFFA,0xEB74,0xD7F5,0xCCF6
	.half	0x000B,0x002D,0x0108,0x0216,0x0334,0x043F,0x0516,0x0480,0x01D3,0x0000,0xFF37,0xFF3C,0xFF48,0xFF55,0xFF63,0xFF6F
	.half	0xFF75,0xFF56,0xFF3D,0xFF30,0xC435,0xC09B,0xBB79,0xB5E3,0xAFEC,0xAAC4,0xA79E,0xB2B9,0xC123,0xC9D8,0x39F8,0x34AF
	.half	0x3E0F,0x41EE,0x4483,0x42FA,0x3AB0,0x2E5D,0x232B,0x1E4A,0x0971,0x0C05,0x0C46,0x0E78,0x111D,0x13EC,0x169A,0x18E0
	.half	0x1A72,0x1B09,0x119B,0x1175,0x1379,0x1355,0x1303,0x1295,0x121E,0x11B2,0x1163,0x1144,0xC39C,0xC525,0xBF0B,0xBBB8
	.half	0xB826,0xB49B,0xB15F,0xAEB8,0xACED,0xAC44,0xFDDC,0xFE36,0xFE6B,0xFEA2,0xFEDC,0xFF17,0xFF50,0xFF98,0xFFE0,0x0000
	.half	0x00A1,0x00E0,0x00EE,0x00EA,0x00D8,0x00BE,0x00A1,0x006B,0x0023,0x0000,0xB7A0,0xB311,0xB218,0xB1D7,0xB234,0xB319
	.half	0xB46F,0xB7EA,0xBCC9,0xBF40,0x2C93,0x2C73,0x2E2D,0x30C1,0x335D,0x352C,0x355B,0x30C5,0x2933,0x2537,0xF5CF,0xF371
	.half	0xF06A,0xECB8,0xE8A9,0xE48B,0xE0AD,0xDC5D,0xD82C,0xD64A,0x05E2,0x046A,0x00C0,0xFB4D,0xF52B,0xEF77,0xEB4E,0xEA35
	.half	0xEB56,0xEC41,0x97F3,0x9A06,0x9BFE,0x9E41,0xA0D5,0xA3BF,0xA703,0xAC55,0xB299,0xB59E,0x8001,0x8001,0x4171,0xBF5F
	.half	0x8001,0x8001,0x4171,0xBF5F,0xC6A6,0xC470,0xC84C,0xD0DA,0xD9A3,0xE186,0xE989,0xF2C5,0xFBE2,0x0000,0xFC03,0xF9D2
	.half	0xFA69,0xFC48,0xFDD9,0xFE25,0xFE37,0xFEDF,0xFFA2,0x0000,0xF089,0xEF8A,0xF149,0xF478,0xF593,0xEF30,0xEA08,0xEF65
	.half	0xF7ED,0xFC62,0x1FB6,0x232F,0x212A,0x1C80,0x1669,0x101C,0x0ACE,0x060C,0x01D3,0x0000,0xEBE8,0xEDC3,0xEEF8,0xF050
	.half	0xF1DB,0xF3A6,0xF5C1,0xF96C,0xFDDA,0x0000,0x1071,0x1154,0x1370,0x1603,0x1860,0x19D9,0x19C1,0x14CC,0x0CBD,0x0884
	.half	0x17F9,0x1588,0x131A,0x0FF5,0x0C6B,0x08CD,0x056E,0x02A0,0x00B5,0x0000,0x00F9,0xFED9,0xFE7B,0xFE6B,0xFE94,0xFEE2
	.half	0xFF41,0xFF9E,0xFFE5,0x0000,0x5670,0x582E,0x5664,0x535C,0x4F8A,0x4B64,0x475E,0x43EE,0x4188,0x40A1
