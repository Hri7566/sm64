/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 11 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage11


/* ================================================================================
		: Scene 1 Move BGs sequence.
===================================================================================	*/
SEQ_Stage1101_MoveBGs:

		seqActor(S_movebg01,   3390,   0,  384,   0,180,0,   0,0,0,   e_po_floatingbord)
		seqActor(S_movebg01,   -767, 384, 3584,   0,  0,0,   0,0,0,   e_po_floatingbord)
		seqActor(S_movebg01,   -767, 384, 1536,   0,  0,0,   0,0,0,   e_po_floatingbord)
		seqActor(S_movebg01,   -767,2304,-1279,   0,  0,0,   0,0,0,   e_po_floatingbord)

		seqActor(S_movebg02,    -578,2177,3009,   0,  0,0,   0,0,0,   e_po_moveblock)
		seqActor(S_movebg02,   -1474,2177,3393,   0,270,0,   0,0,0,   e_po_moveblock)
		seqActor(S_movebg02,   -1602,2177,3009,   0,  0,0,   0,0,0,   e_po_moveblock)
		seqActor(S_movebg02,   -1090,2177,3521,   0,  0,0,   0,0,0,   e_po_moveblock)
		seqActor(S_movebg02,    -962,2177,3137,   0, 90,0,   0,0,0,   e_po_moveblock)

		seqActor(S_NULL,  	      0,   0,    0,   0,0,0,   0,0,0,   e_watermove  )
		seqActor(S_movebg03,   1920,2560,-3583,   0,0,0,   0,0,0,   e_switchwater)
		seqActor(S_movebg03,   3328, 256, 2918,   0,0,0,   0,0,0,   e_switchwater)
		seqActor(S_movebg03,   2048,1792, 2176,   0,0,0,   0,0,0,   e_switchwater)
		seqActor(S_movebg03,    640,1024, 3712,   0,0,0,   0,0,0,   e_switchwater)
		seqActor(S_movebg03,   1810,  40,-3118,   0,0,0,   0,0,0,   e_switchwater)

		seqActor(S_hanbutton,  3360,1280,3420,   0,0,0,   0,0,0,   e_timeblock_switch)
		seqActor(S_movebg04,   2239,1126,3391,   0,0,0,   0,1,0,   e_timeblock)
		seqActor(S_movebg04,   1215,1357,2751,   0,0,0,   0,1,0,   e_timeblock)
		seqActor(S_movebg04,   1215,1229,3391,   0,0,0,   0,1,0,   e_timeblock)
		seqActor(S_movebg04,   1599,1101,3391,   0,0,0,   0,1,0,   e_timeblock)
		seqActor(S_movebg04,   2879,1152,3391,   0,0,0,   0,1,0,   e_timeblock)

		seqActor(S_movebg05,   1024,3277,-2112,   0,0,0,   0,50,0,   e_poollift_stop)
		seqActor(S_movebg05,   1024,3277,-1663,   0,0,0,   0,50,0,   e_poollift)

		seqActor(S_movebg06,   -767,1152,  128,   0,0,0,   0,0,0,   e_po_floatingbord2)
		seqActor(S_movebg06,   -767,2304,-2687,   0,0,0,   0,0,0,   e_po_floatingbord2)

		seqActor(S_movebg07,   734,3840,84,   0,0,0,   70,1,0,   e_rotland)

		seqActor(S_amembow,    2956,288,-468,   0,0,0,   0,0,0,   e_amembow)
		seqActor(S_amembow,     184,384, 621,   0,0,0,   0,0,0,   e_amembow)

		seqReturn


/* ================================================================================
		: Scene 2 Move BGs sequence.
===================================================================================	*/
SEQ_Stage1102_MoveBGs:

		seqActor(S_NULL,  	      0,    0,    0,   0,0,0,   0,0,0,   e_watermove  )
		seqActor(S_movebg03,  -3583,-2508,-2047,   0,0,0,   0,0,0,   e_switchwater)
		seqActor(S_movebg03,   -767, -127, 1792,   0,0,0,   0,0,0,   e_switchwater)

		seqActor(S_NULL,  -768,-665,3584,  0,0,0,  0,92,0,  e_bar)

		seqReturn


/* ================================================================================
		: Scene 1 Stars sequence.
===================================================================================	*/
SEQ_Stage1101_Stars:

/*		seqLevelActor(0x003f,S_polystar,-2200,  2600, 3500,  0,0,0,  0,0,0,  e_tripstar)			/*ukiita	*/
/*		seqLevelActor(0x003f,S_polystar, 1550,  4350,  100,  0,0,0,  1,0,0,  e_tripstar)	/*bridge	*/
		seqLevelActor(0x003f,S_NULL	   , 3360,  1580, 2660,  0,0,0,  2,0,0,  e_tripstar_getdummy)	/*push block*/
		seqLevelActor(0x003f,S_polystar,  890,  3400,-2040,  0,0,0,  3,0,0,  e_tripstar) 	/*ami		*/

		seqReturn


/* ================================================================================
		: Scene 2 Stars sequence.
===================================================================================	*/
SEQ_Stage1102_Stars:

		seqLevelActor(0x003f,S_NULL    , -770, -1600, 3600,  0,0,0,  4,0,0,  e_tripstar_getcoins) /* 8_coin 				*/
		seqLevelActor(0x003f,S_polystar, 2180,  -840, 3720,  0,0,0,  5,0,0,  e_tripstar) 		  /* no_water_under_town 	*/

		seqReturn


/* ================================================================================
		: Stage 11 main sequence.
===================================================================================	*/
SEQ_DoStage11:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage11SegmentRomStart	  , _GfxStage11SegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE	  , _JFortTextureSegmentRomStart  , _JFortTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_BACK      , _BackWaterlandSegmentRomStart , _BackWaterlandSegmentRomEnd )
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_aSegmentRomStart	  , _GfxEnemy1_aSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_aSegmentRomStart	  , _HmsEnemy1_aSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_dSegmentRomStart	  , _GfxEnemy2_dSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_dSegmentRomStart	  , _HmsEnemy2_dSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_a)
		seqCall(SetEnemy2_d) 

		seqHmsShape(S_tree1, RCP_HmsMainTree)

		seqHmsShape(S_movebg01, RCP_HmsPoMobj01)
		seqHmsShape(S_movebg02, RCP_HmsPoMobj02)
		seqHmsShape(S_movebg03, RCP_HmsPoMobj03)
		seqHmsShape(S_movebg04, RCP_HmsPoMobj04)
		seqHmsShape(S_movebg05, RCP_HmsPoMobj05)
		seqHmsShape(S_movebg06, RCP_HmsPoMobj06)
		seqHmsShape(S_movebg07, RCP_HmsPoMobj07)
 
		seqBeginScene(1, RCP_Stage11Scene1)
			seqActor(S_NULL,  3395,2580+START_H, 384,  0,180,0,  0,10,0,  e_player_entpict)
			seqActor(S_NULL,   818,			  0,3634,  0, 45,0,  0,11,0,  e_warp)
			seqActor(S_NULL, -2865,		   3328,3065,  0,  0,0,  0,12,0,  e_warp)
			seqPort(10, 11, 1, 10)					/*	MARIO stage in.	*/
			seqPort(11, 11, 1, 12)					/*	Warp.			*/
			seqPort(12, 11, 1, 11)					/*	Warp.			*/
			seqCall(SEQ_Stage1101_Stars)
			seqCall(SEQ_Stage1101_MoveBGs)
			seqGameClear(6, 2,  50)
			seqGameOver (6, 2, 100)
			seqConnect(1, 2, 0,0,0)
			seqMapInfo(cx1101_info)
			seqTagInfo(cx1101_info_tag)
			seqSetMusic(NA_STG_WATER, NA_DUNGEON_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

		seqBeginScene(2, RCP_Stage11Scene2)
			seqCall(SEQ_Stage1102_Stars)
			seqCall(SEQ_Stage1102_MoveBGs)
			seqGameClear(6, 2,  50)
			seqGameOver (6, 2, 100)
			seqConnect(0, 1, 0,0,0)
			seqMapInfo(cx1102_info)
			seqTagInfo(cx1102_info_tag)
			seqSetMusic(NA_STG_WATER, NA_DUNGEON_BGM)
			seqEnvironment(ENV_WATER)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 180,  3395,2580,384)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
