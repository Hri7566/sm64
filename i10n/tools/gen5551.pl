#!/usr/bin/perl -w 

die "Usage: gen5551.pl <input Photoshop raw format> <output> <texture name>\n" unless $#ARGV==2;

$in = shift;
$out = shift;
$name = shift;
$idx=0;

open(INFILE, "<$in") or die "Can't open $in: $!";
open(OUTFILE, ">$out") or die "Can't open $out: $!";
printf OUTFILE "static unsigned short %s[] = { \n", $name;

while( read(INFILE, $byte, 4) != 0 ) {
    $r = 31.0 / 255.0 * vec($byte, 0, 8);
    $g = 31.0 / 255.0 * vec($byte, 1, 8);
    $b = 31.0 / 255.0 * vec($byte, 2, 8);
    $a = vec($byte, 3, 8);

#    if ($r<4.0 && $g<4.0 && $b<4.0) { 
#        $rgb = 0; 
#    } else {
if ($a==0) {
        $rgb = ($r<<11 & 0xf800) | ($g<<6 & 0x07c0) | ($b<<1 & 0x003e) | 0;
} else {
        $rgb = ($r<<11 & 0xf800) | ($g<<6 & 0x07c0) | ($b<<1 & 0x003e) | 1;
}
#    } 

    printf OUTFILE "0x%04x, ", $rgb;
    $idx++;
    if ($idx%80==0) {
        printf OUTFILE "\n";
    }
}
printf OUTFILE "}; ";
close INFILE;
close OUTFILE;

