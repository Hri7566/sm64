/***************************************************************************************************
			8*8 -> 16bit Font Texture    (for Ending)

			programed by Iwamoto Daiki     junuary 18 1996 
****************************************************************************************************/
static unsigned short sg_3_txt[] = {
	  0x0001,  0x0941,  0x3e83,  0x3fc1,  0x3681,  0x0901,  0x0001,  0x0001,
	  0x0001,  0x1a81,  0x2c81,  0x1241,  0x4f81,  0x2c01,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x0001,  0x0041,  0x66c3,  0x33c1,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x1101,  0x8703,  0x7f01,  0x08c3,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x0041,  0x3b01,  0x96c1,  0x53c1,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x0001,  0x0001,  0x7d01,  0x9601,  0x0001,  0x0001,
	  0x0001,  0x7c43,  0x9501,  0x5b03,  0xd741,  0x7c81,  0x0001,  0x0001,
	  0x0001,  0x52c3,  0xef43,  0xffc3,  0xb5c3,  0x18c1,  0x0001,  0x0001,
};
static unsigned short sg_4_txt[] = {
	  0x0001,  0x0001,  0x0001,  0x0d03,  0x0503,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x09c1,  0x1fc1,  0x1681,  0x0041,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x3503,  0x3541,  0x3601,  0x0041,  0x0001,  0x0001,
	  0x0001,  0x19c1,  0x4d83,  0x2281,  0x5641,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x6d41,  0x5403,  0x3b01,  0x86c1,  0x3241,  0x0001,  0x0001,
	  0x0001,  0x8dc1,  0xaf01,  0xaf01,  0xc7c1,  0x7d41,  0x0001,  0x0001,
	  0x0001,  0x0841,  0x3181,  0x8c83,  0xd701,  0x3a01,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x73c3,  0xe701,  0xe701,  0x9cc3,  0x0001,  0x0001,
};
static unsigned short sg_6_txt[] = {
	  0x0001,  0x0001,  0x01c1,  0x0641,  0x0781,  0x0403,  0x0001,  0x0001,
	  0x0001,  0x0101,  0x1f41,  0x0c41,  0x00c1,  0x0041,  0x0001,  0x0001,
	  0x0001,  0x2441,  0x3ec1,  0x1103,  0x0081,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x4d81,  0x6f41,  0x6741,  0x6701,  0x1181,  0x0001,  0x0001,
	  0x0001,  0x7601,  0x7ec1,  0x2a43,  0x7e81,  0x6541,  0x0001,  0x0001,
	  0x0001,  0x8e01,  0x74c1,  0x0001,  0x6401,  0x9e81,  0x0001,  0x0001,
	  0x0001,  0x6bc1,  0xcf01,  0x4243,  0xc681,  0x8d01,  0x0001,  0x0001,
	  0x0001,  0x0841,  0xad81,  0xf7c3,  0xc603,  0x18c1,  0x0001,  0x0001,
};

static unsigned short sag_A_txt[] = {
	  0x0001,  0x04c1,  0x0741,  0x0741,  0x0381,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0281,  0x1e81,  0x1f81,  0x1e41,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x3e01,  0x23c1,  0x4741,  0x0981,  0x0001,  0x0001,
	  0x0001,  0x1a01,  0x5e81,  0x0041,  0x5ec1,  0x33c1,  0x0001,  0x0001,
	  0x0001,  0x4c01,  0x9f81,  0x75c1,  0x9781,  0x6dc1,  0x0001,  0x0001,
	  0x0001,  0x9601,  0x7d41,  0x7501,  0x7d01,  0xb781,  0x0881,  0x0001,
	  0x6341,  0xefc1,  0x73c1,  0x0001,  0x6b81,  0xefc1,  0x9501,  0x0001,
	  0xad41,  0xef01,  0xbdc1,  0x0841,  0xb5c1,  0xef01,  0xd681,  0x0001,
};
static unsigned short sag_B_txt[] = {
	  0x0281,  0x0741,  0x0741,  0x0781,  0x0681,  0x0281,  0x0001,  0x0001,
	  0x0141,  0x1f01,  0x1601,  0x0341,  0x1641,  0x1f81,  0x0081,  0x0001,
	  0x0001,  0x3601,  0x2cc1,  0x0001,  0x23c1,  0x47c1,  0x00c1,  0x0001,
	  0x0001,  0x4dc1,  0x6f41,  0x5e81,  0x77c1,  0x4541,  0x0001,  0x0001,
	  0x0001,  0x6e01,  0x7641,  0x3b81,  0x5d01,  0x8f01,  0x21c1,  0x0001,
	  0x0001,  0x8e01,  0x6c81,  0x0001,  0x0001,  0xc7c1,  0x6441,  0x0001,
	  0x2941,  0xd701,  0xbe81,  0x7c01,  0x9d41,  0xf7c1,  0x4a81,  0x0001,
	  0x4a41,  0xe701,  0xef41,  0xf781,  0xe701,  0x8401,  0x0001,  0x0001,
};
static unsigned short sag_C_txt[] = {
	  0x0001,  0x00c1,  0x0601,  0x07c1,  0x0781,  0x06c1,  0x0281,  0x0001,
	  0x0001,  0x1dc1,  0x1f41,  0x02c1,  0x0b81,  0x1fc1,  0x0ac1,  0x0001,
	  0x0981,  0x47c1,  0x1b01,  0x0001,  0x0001,  0x2d41,  0x1ac1,  0x0001,
	  0x2b01,  0x6fc1,  0x1181,  0x0001,  0x0001,  0x0041,  0x0041,  0x0001,
	  0x3b01,  0x97c1,  0x1981,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,
	  0x29c1,  0xc7c1,  0x4301,  0x0001,  0x0001,  0x10c1,  0x10c1,  0x0001,
	  0x0001,  0xa601,  0xdf41,  0x5b01,  0x6341,  0xdf41,  0x6bc1,  0x0001,
	  0x0001,  0x18c1,  0xbdc1,  0xffc1,  0xffc1,  0xad41,  0x18c1,  0x0001,
};
static unsigned short sag_D_txt[] = {
	  0x0281,  0x0741,  0x0741,  0x0741,  0x0581,  0x0141,  0x0001,  0x0001,
	  0x0141,  0x1f01,  0x1641,  0x0c01,  0x1f41,  0x1e81,  0x0081,  0x0001,
	  0x0001,  0x3601,  0x2c81,  0x0001,  0x1241,  0x4fc1,  0x1ac1,  0x0001,
	  0x0001,  0x5601,  0x44c1,  0x0001,  0x0041,  0x6fc1,  0x3401,  0x0001,
	  0x0001,  0x7601,  0x5cc1,  0x0001,  0x0841,  0x97c1,  0x4c01,  0x0001,
	  0x0001,  0x8e01,  0x6c81,  0x0001,  0x3a81,  0xc7c1,  0x3a81,  0x0001,
	  0x2941,  0xd701,  0xbe41,  0x8c81,  0xe781,  0xbe81,  0x0841,  0x0001,
	  0x5281,  0xe701,  0xef41,  0xe701,  0xad41,  0x2101,  0x0001,  0x0001,
};
static unsigned short sag_E_txt[] = {
	  0x0201,  0x0701,  0x0741,  0x0781,  0x0781,  0x0701,  0x0241,  0x0001,
	  0x0101,  0x1ec1,  0x1e41,  0x0301,  0x0301,  0x1f01,  0x0b01,  0x0001,
	  0x0001,  0x3581,  0x2c81,  0x0901,  0x1a41,  0x2c81,  0x1281,  0x0001,
	  0x0001,  0x4d01,  0x6f41,  0x6f41,  0x4d81,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x6541,  0x7e41,  0x6581,  0x6501,  0x0841,  0x0001,  0x0001,
	  0x0001,  0x8581,  0x6c41,  0x0001,  0x08c1,  0x7d41,  0x42c1,  0x0001,
	  0x2101,  0xcec1,  0xc681,  0x7c01,  0x73c1,  0xd701,  0x5b01,  0x0001,
	  0x39c1,  0xe701,  0xef41,  0xf781,  0xf781,  0xef41,  0x4201,  0x0001,
};
static unsigned short sag_F_txt[] = {
	  0x0181,  0x0701,  0x0741,  0x0781,  0x0781,  0x0741,  0x02c1,  0x0001,
	  0x00c1,  0x1e81,  0x1e81,  0x0301,  0x0301,  0x2701,  0x13c1,  0x0001,
	  0x0001,  0x2cc1,  0x3541,  0x08c1,  0x1a41,  0x2c81,  0x1ac1,  0x0001,
	  0x0001,  0x4481,  0x7781,  0x6f41,  0x5601,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x5cc1,  0x8681,  0x6d81,  0x6dc1,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x74c1,  0x7d01,  0x0001,  0x2181,  0x0001,  0x0001,  0x0001,
	  0x18c1,  0xc681,  0xd6c1,  0x4201,  0x0001,  0x0001,  0x0001,  0x0001,
	  0x3181,  0xe701,  0xef41,  0x8401,  0x0001,  0x0001,  0x0001,  0x0001,
};
static unsigned short sag_G_txt[] = {
	  0x0001,  0x01c1,  0x0701,  0x07c1,  0x0741,  0x06c1,  0x0101,  0x0001,
	  0x00c1,  0x1f01,  0x1601,  0x02c1,  0x14c1,  0x27c1,  0x0101,  0x0001,
	  0x1b41,  0x47c1,  0x0901,  0x0001,  0x0001,  0x3601,  0x0901,  0x0001,
	  0x44c1,  0x6741,  0x0001,  0x0001,  0x0041,  0x08c1,  0x0041,  0x0001,
	  0x54c1,  0x8781,  0x0001,  0x3b01,  0x9781,  0x8f41,  0x5d01,  0x0001,
	  0x4b81,  0xbfc1,  0x1901,  0x08c1,  0x5c01,  0xb7c1,  0x3ac1,  0x0001,
	  0x10c1,  0xc701,  0xb641,  0x4a81,  0x6bc1,  0xdfc1,  0x18c1,  0x0001,
	  0x0001,  0x39c1,  0xdec1,  0xffc1,  0xf7c1,  0xad41,  0x0881,  0x0001,
};
static unsigned short sag_H_txt[] = {
	  0x0281,  0x0741,  0x0701,  0x0301,  0x06c1,  0x0741,  0x0301,  0x0001,
	  0x0141,  0x1f01,  0x1641,  0x0141,  0x15c1,  0x1f81,  0x01c1,  0x0001,
	  0x0001,  0x3601,  0x2cc1,  0x0001,  0x2401,  0x3ec1,  0x0001,  0x0001,
	  0x0001,  0x4dc1,  0x6fc1,  0x6f41,  0x6f81,  0x5641,  0x0001,  0x0001,
	  0x0001,  0x6dc1,  0x7e81,  0x4401,  0x7601,  0x7e81,  0x0001,  0x0001,
	  0x0001,  0x8e01,  0x6c81,  0x0001,  0x5bc1,  0xa6c1,  0x0001,  0x0001,
	  0x2941,  0xd701,  0xbe41,  0x3181,  0xadc1,  0xdf81,  0x31c1,  0x0001,
	  0x5281,  0xef01,  0xe701,  0x6301,  0xdec1,  0xef01,  0x6301,  0x0001,
};
static unsigned short sag_I_txt[] = {
	  0x0001,  0x04c1,  0x0781,  0x0701,  0x0781,  0x0541,  0x0001,  0x0001,
	  0x0001,  0x0281,  0x0cc1,  0x1fc1,  0x0d41,  0x02c1,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x0941,  0x47c1,  0x11c1,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x1181,  0x6fc1,  0x1a41,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x1981,  0x8fc1,  0x2a41,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x1941,  0xbfc1,  0x29c1,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x5281,  0x9cc1,  0xefc1,  0xa541,  0x5ac1,  0x0001,  0x0001,
	  0x0001,  0x9cc1,  0xf781,  0xe701,  0xf781,  0xad41,  0x0001,  0x0001,
};
static unsigned short sag_J_txt[] = {
	  0x0001,  0x0001,  0x03c1,  0x07c1,  0x07c1,  0x07c1,  0x03c1,  0x0001,
	  0x0001,  0x0001,  0x0181,  0x0b81,  0x1f81,  0x1541,  0x0181,  0x0001,
	  0x0001,  0x0001,  0x0001,  0x0001,  0x4741,  0x1b41,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x0001,  0x0041,  0x6741,  0x3381,  0x0001,  0x0001,
	  0x2181,  0x4c01,  0x0001,  0x0041,  0x8f41,  0x4381,  0x0001,  0x0001,
	  0x3a81,  0xa6c1,  0x0001,  0x0001,  0xbfc1,  0x5381,  0x0001,  0x0001,
	  0x4241,  0xefc1,  0x7c01,  0x94c1,  0xf7c1,  0x4241,  0x0001,  0x0001,
	  0x18c1,  0xad41,  0xffc1,  0xf781,  0x9441,  0x0001,  0x0001,  0x0001,
};
static unsigned short sag_K_txt[] = {
	  0x0301,  0x0741,  0x06c1,  0x02c1,  0x06c1,  0x0741,  0x0281,  0x0001,
	  0x01c1,  0x1781,  0x0dc1,  0x0141,  0x1f81,  0x1601,  0x0141,  0x0001,
	  0x0001,  0x36c1,  0x1bc1,  0x23c1,  0x3641,  0x0081,  0x0001,  0x0001,
	  0x0001,  0x5641,  0x5f01,  0x6fc1,  0x2281,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x7641,  0x8781,  0x8701,  0x7e81,  0x0041,  0x0001,  0x0001,
	  0x0001,  0x9ec1,  0x5c01,  0x2181,  0xc7c1,  0x4301,  0x0001,  0x0001,
	  0x31c1,  0xdf81,  0xadc1,  0x0841,  0x9d81,  0xcec1,  0x3a01,  0x0001,
	  0x6301,  0xef01,  0xe701,  0x2101,  0x39c1,  0xe701,  0x7381,  0x0001,
};
static unsigned short sag_L_txt[] = {
	  0x0301,  0x0741,  0x0701,  0x0701,  0x00c1,  0x0001,  0x0001,  0x0001,
	  0x0181,  0x0d81,  0x1fc1,  0x0401,  0x0041,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x1a81,  0x3f81,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x22c1,  0x6781,  0x0081,  0x0001,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x32c1,  0x8f81,  0x0881,  0x0001,  0x1981,  0x2a41,  0x0001,
	  0x0001,  0x3a81,  0xb781,  0x0001,  0x0001,  0x5381,  0x85c1,  0x0001,
	  0x3181,  0xad81,  0xe7c1,  0x8441,  0x7c01,  0xb601,  0x9d41,  0x0001,
	  0x5b01,  0xef41,  0xe701,  0xf781,  0xf781,  0xf741,  0x8c41,  0x0001,
};
static unsigned short sag_M_txt[] = {
	  0x0501,  0x0701,  0x0241,  0x0001,  0x01c1,  0x0701,  0x0541,  0x0001,
	  0x0301,  0x1fc1,  0x1501,  0x0001,  0x1481,  0x1fc1,  0x0b81,  0x0001,
	  0x0901,  0x47c1,  0x3e41,  0x0081,  0x35c1,  0x4781,  0x0941,  0x0001,
	  0x19c1,  0x6701,  0x4d41,  0x3bc1,  0x4d01,  0x66c1,  0x2241,  0x0001,
	  0x21c1,  0x86c1,  0x4b81,  0x9f81,  0x4bc1,  0x8641,  0x3241,  0x0001,
	  0x3201,  0xae81,  0x1941,  0xae81,  0x2141,  0x9e01,  0x3a41,  0x0001,
	  0x9481,  0xef81,  0x4a41,  0x0841,  0x39c1,  0xe741,  0xa501,  0x0001,
	  0xce41,  0xef41,  0x7bc1,  0x0001,  0x6b41,  0xef41,  0xce41,  0x0001,
};
static unsigned short sag_N_txt[] = {
	  0x03c1,  0x0701,  0x0381,  0x00c1,  0x0701,  0x0701,  0x03c1,  0x0001,
	  0x0201,  0x1f81,  0x1f41,  0x0101,  0x0cc1,  0x1fc1,  0x0241,  0x0001,
	  0x0001,  0x3ec1,  0x4fc1,  0x1b41,  0x1181,  0x4781,  0x0041,  0x0001,
	  0x0041,  0x5e81,  0x4d81,  0x5e81,  0x22c1,  0x6741,  0x0881,  0x0001,
	  0x0841,  0x86c1,  0x3281,  0x7641,  0x75c1,  0x8701,  0x0881,  0x0001,
	  0x0001,  0xa6c1,  0x2181,  0x42c1,  0xcfc1,  0xb741,  0x0841,  0x0001,
	  0x4a41,  0xe781,  0x9d01,  0x1901,  0xcec1,  0xe7c1,  0x0841,  0x0001,
	  0x7381,  0xef41,  0xe701,  0x2101,  0x6b41,  0xf781,  0x0841,  0x0001,
};
static unsigned short sag_O_txt[] = {
	  0x0001,  0x0141,  0x0601,  0x07c1,  0x0641,  0x0181,  0x0001,  0x0001,
	  0x0041,  0x1681,  0x1701,  0x0401,  0x16c1,  0x16c1,  0x0081,  0x0001,
	  0x1281,  0x3fc1,  0x1201,  0x0001,  0x09c1,  0x3fc1,  0x1b01,  0x0001,
	  0x3401,  0x67c1,  0x0041,  0x0001,  0x0041,  0x6781,  0x3c81,  0x0001,
	  0x4c01,  0x8fc1,  0x0841,  0x0001,  0x0001,  0x8781,  0x54c1,  0x0001,
	  0x4301,  0xbfc1,  0x21c1,  0x0001,  0x1901,  0xbfc1,  0x5381,  0x0001,
	  0x1081,  0xcf01,  0xb641,  0x52c1,  0xadc1,  0xd781,  0x1901,  0x0001,
	  0x0001,  0x41c1,  0xef41,  0xffc1,  0xf781,  0x4a41,  0x0001,  0x0001,
};
static unsigned short sag_P_txt[] = {
	  0x0181,  0x0701,  0x0741,  0x0781,  0x0681,  0x02c1,  0x0001,  0x0001,
	  0x00c1,  0x1e41,  0x1ec1,  0x0c01,  0x1e41,  0x27c1,  0x0981,  0x0001,
	  0x0001,  0x2cc1,  0x3541,  0x0001,  0x0981,  0x4fc1,  0x1b01,  0x0001,
	  0x0001,  0x44c1,  0x5e41,  0x2ac1,  0x5541,  0x77c1,  0x19c1,  0x0001,
	  0x0001,  0x5481,  0x9fc1,  0x97c1,  0x8f41,  0x43c1,  0x0001,  0x0001,
	  0x0001,  0x6c81,  0x8dc1,  0x08c1,  0x0841,  0x0001,  0x0001,  0x0001,
	  0x18c1,  0xc681,  0xcec1,  0x5b01,  0x0001,  0x0001,  0x0001,  0x0001,
	  0x3181,  0xe701,  0xef41,  0xbdc1,  0x0001,  0x0001,  0x0001,  0x0001,
};
static unsigned short sag_Q_txt[] = {
	  0x0001,  0x01c1,  0x0701,  0x07c1,  0x0701,  0x0201,  0x0001,  0x0001,
	  0x0081,  0x1f01,  0x1681,  0x0b41,  0x1641,  0x1f01,  0x00c1,  0x0001,
	  0x1ac1,  0x4fc1,  0x11c1,  0x0001,  0x0941,  0x4fc1,  0x1b41,  0x0001,
	  0x3c01,  0x6fc1,  0x0041,  0x0001,  0x0001,  0x6781,  0x4481,  0x0001,
	  0x4c01,  0x97c1,  0x0041,  0x1901,  0x4b81,  0x9781,  0x5481,  0x0001,
	  0x42c1,  0xc7c1,  0x2981,  0x1941,  0xbfc1,  0xc7c1,  0x4b01,  0x0001,
	  0x0881,  0xc6c1,  0xcec1,  0x6b41,  0xcf01,  0xffc1,  0x6341,  0x0001,
	  0x0001,  0x3181,  0xde81,  0xffc1,  0xd681,  0xce41,  0x8c41,  0x0001,
};
static unsigned short sag_R_txt[] = {
	  0x0301,  0x0701,  0x0741,  0x0f81,  0x0641,  0x0201,  0x0001,  0x0001,
	  0x01c1,  0x2781,  0x1dc1,  0x0b81,  0x1ec1,  0x2701,  0x0041,  0x0001,
	  0x0001,  0x46c1,  0x23c1,  0x0001,  0x2c41,  0x4fc1,  0x0041,  0x0001,
	  0x0001,  0x5e81,  0x6681,  0x4d01,  0x6f01,  0x3c81,  0x0001,  0x0001,
	  0x0001,  0x7e41,  0x86c1,  0x8f41,  0x8f41,  0x0841,  0x0001,  0x0001,
	  0x0001,  0xa6c1,  0x5381,  0x2141,  0xcfc1,  0x5341,  0x0001,  0x0001,
	  0x31c1,  0xe781,  0xb601,  0x0841,  0x9d41,  0xdf41,  0x5281,  0x0001,
	  0x6301,  0xef01,  0xe701,  0x2101,  0x2101,  0xe6c1,  0x9cc1,  0x0001,
};
static unsigned short sag_S_txt[] = {
	  0x0001,  0x01c1,  0x0701,  0x07c1,  0x0741,  0x0681,  0x0041,  0x0001,
	  0x0001,  0x1ec1,  0x15c1,  0x0181,  0x1481,  0x2781,  0x0041,  0x0001,
	  0x0081,  0x4fc1,  0x3541,  0x0041,  0x0041,  0x1301,  0x0001,  0x0001,
	  0x0001,  0x4d81,  0x7fc1,  0x6f41,  0x4d41,  0x1941,  0x0001,  0x0001,
	  0x0001,  0x0881,  0x3b81,  0x7641,  0x9fc1,  0x8741,  0x10c1,  0x0001,
	  0x2141,  0x85c1,  0x0001,  0x0001,  0x4b81,  0xbfc1,  0x3241,  0x0001,
	  0x31c1,  0xe7c1,  0x8481,  0x2141,  0x7c01,  0xdfc1,  0x2141,  0x0001,
	  0x2941,  0xe741,  0xef81,  0xf7c1,  0xef81,  0x6b81,  0x0001,  0x0001,
};
static unsigned short sag_T_txt[] = {
	  0x0281,  0x0741,  0x0781,  0x0701,  0x0781,  0x0741,  0x0301,  0x0001,
	  0x0b81,  0x1ec1,  0x0cc1,  0x1fc1,  0x1501,  0x1e81,  0x1441,  0x0001,
	  0x2c41,  0x3501,  0x08c1,  0x47c1,  0x0941,  0x2c41,  0x2d01,  0x0001,
	  0x1a41,  0x2281,  0x1141,  0x6fc1,  0x1a01,  0x1a01,  0x2281,  0x0001,
	  0x0001,  0x0001,  0x21c1,  0x97c1,  0x2a41,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x1941,  0xbfc1,  0x29c1,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x5281,  0x9cc1,  0xefc1,  0xa541,  0x5ac1,  0x0001,  0x0001,
	  0x0001,  0x9481,  0xf781,  0xe701,  0xf781,  0xa541,  0x0001,  0x0001,
};
static unsigned short sag_U_txt[] = {
	  0x0401,  0x0741,  0x0741,  0x0301,  0x0201,  0x0781,  0x0741,  0x04c1,
	  0x0201,  0x1f41,  0x1f41,  0x0181,  0x0101,  0x1681,  0x1fc1,  0x0281,
	  0x0001,  0x3641,  0x3641,  0x0001,  0x0001,  0x2cc1,  0x3f41,  0x0001,
	  0x0001,  0x5641,  0x5641,  0x0001,  0x0001,  0x3d01,  0x5f41,  0x0041,
	  0x0001,  0x6e41,  0x6e41,  0x0001,  0x0001,  0x5501,  0x7f41,  0x0041,
	  0x0001,  0x8641,  0x96c1,  0x0001,  0x0001,  0x7541,  0x9f41,  0x0041,
	  0x0001,  0x7c81,  0xdfc1,  0x63c1,  0x5b41,  0xcf81,  0x8d81,  0x0001,
	  0x0001,  0x2101,  0xbe41,  0xffc1,  0xffc1,  0xcec1,  0x2981,  0x0001,
};
static unsigned short sag_V_txt[] = {
	  0x0501,  0x0701,  0x06c1,  0x0081,  0x04c1,  0x0701,  0x05c1,  0x0001,
	  0x0b01,  0x27c1,  0x1541,  0x0041,  0x02c1,  0x1fc1,  0x0c01,  0x0001,
	  0x0001,  0x3ec1,  0x2c41,  0x0001,  0x11c1,  0x3ec1,  0x0041,  0x0001,
	  0x0001,  0x3c41,  0x66c1,  0x0001,  0x3c41,  0x4d81,  0x0001,  0x0001,
	  0x0001,  0x2201,  0x97c1,  0x1941,  0x7601,  0x3b41,  0x0001,  0x0001,
	  0x0001,  0x0841,  0xaf01,  0x7d01,  0xa6c1,  0x1901,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x94c1,  0xf7c1,  0xadc1,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x4201,  0xe701,  0x6b41,  0x0001,  0x0001,  0x0001,
};
static unsigned short sag_W_txt[] = {
	  0x05c1,  0x0701,  0x0501,  0x0001,  0x0341,  0x0741,  0x06c1,  0x0001,
	  0x0bc1,  0x27c1,  0x02c1,  0x0041,  0x01c1,  0x1f41,  0x1541,  0x0001,
	  0x08c1,  0x4701,  0x1141,  0x4681,  0x1201,  0x3e01,  0x1a81,  0x0001,
	  0x0881,  0x66c1,  0x33c1,  0x6f41,  0x3c01,  0x5e01,  0x1a01,  0x0001,
	  0x0041,  0x7e01,  0x75c1,  0x4bc1,  0x6d41,  0x86c1,  0x1941,  0x0001,
	  0x0001,  0x9e01,  0xb741,  0x10c1,  0x95c1,  0xbf81,  0x1081,  0x0001,
	  0x0001,  0xb5c1,  0xc641,  0x0001,  0x8c81,  0xef81,  0x0841,  0x0001,
	  0x0001,  0x8c41,  0x7bc1,  0x0001,  0x4a41,  0xbd81,  0x0001,  0x0001,
};
static unsigned short sag_X_txt[] = {
	  0x0201,  0x0741,  0x0701,  0x0341,  0x0681,  0x0741,  0x0201,  0x0001,
	  0x0101,  0x15c1,  0x1fc1,  0x01c1,  0x1ec1,  0x15c1,  0x0101,  0x0001,
	  0x0001,  0x0081,  0x3ec1,  0x34c1,  0x3e41,  0x0081,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x2b01,  0x7fc1,  0x2b01,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x4341,  0xa7c1,  0x5c41,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x1901,  0x9e41,  0x4b41,  0xaf41,  0x31c1,  0x0001,  0x0001,
	  0x2941,  0xbe41,  0xc6c1,  0x2941,  0xd701,  0xcec1,  0x3a01,  0x0001,
	  0x4a41,  0xef41,  0xd681,  0x5281,  0xde81,  0xef41,  0x7381,  0x0001,
};
static unsigned short sag_Y_txt[] = {
	  0x0101,  0x0f41,  0x0741,  0x0541,  0x0301,  0x0f41,  0x0f81,  0x0241,
	  0x0081,  0x1501,  0x2fc1,  0x1401,  0x0181,  0x2fc1,  0x1e01,  0x0101,
	  0x0001,  0x0041,  0x4741,  0x2c81,  0x1201,  0x4fc1,  0x0901,  0x0001,
	  0x0001,  0x0001,  0x2b41,  0x6fc1,  0x66c1,  0x4501,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x0041,  0x7ec1,  0x8fc1,  0x1101,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x0001,  0x7501,  0xa741,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x18c1,  0x7441,  0xbec1,  0xd7c1,  0x8481,  0x31c1,  0x0001,
	  0x0001,  0x3181,  0xdf41,  0xe741,  0xdf01,  0xf781,  0x6301,  0x0001,
};
static unsigned short sag_Z_txt[] = {
	  0x0001,  0x0341,  0x0f41,  0x0fc1,  0x0f81,  0x0741,  0x0501,  0x0001,
	  0x0001,  0x1481,  0x2781,  0x0c01,  0x1dc1,  0x2fc1,  0x1401,  0x0001,
	  0x0001,  0x2cc1,  0x3e01,  0x0001,  0x4641,  0x46c1,  0x0081,  0x0001,
	  0x0001,  0x0901,  0x0901,  0x4481,  0x7fc1,  0x19c1,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x2181,  0x9fc1,  0x5481,  0x1941,  0x2a41,  0x0001,
	  0x0001,  0x0841,  0x9e81,  0x9e81,  0x0001,  0x6481,  0x9681,  0x0001,
	  0x0001,  0x6bc1,  0xefc1,  0xae01,  0x7c41,  0xbe81,  0xa601,  0x0001,
	  0x0001,  0x94c1,  0xe701,  0xe741,  0xef81,  0xef41,  0x8c41,  0x0001,
};
static unsigned short sag_ten_txt[] = {
	  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x0841,  0x5301,  0x0881,  0x0001,  0x0001,  0x0001,
	  0x0001,  0x0001,  0x10c1,  0xd6c1,  0x2141,  0x0001,  0x0001,  0x0001,
};
