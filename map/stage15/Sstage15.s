/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 15 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage15


/* ================================================================================
		: Move BGs sequence.
===================================================================================	*/
SEQ_Stage15_MoveBGs:

		seqActor(S_movebg01,   -3400,-2038,6564,   0,0,0,   2,86,0,   e_derublock)
		seqActor(S_movebg01,   -2684,-1423, -36,   0,0,0,   2,89,0,   e_derublock)

		seqActor(S_movebg02,   4571,-1782, 2036,   0,180,0,   0,0,0,   e_linelift)
		seqActor(S_movebg02,    580, -963,-3659,   0,180,0,   0,1,0,   e_linelift)
		seqActor(S_movebg02,   1567,  880, -184,   0,180,0,   0,7,0,   e_linelift)
		seqActor(S_movebg02,    646,  880, -184,   0,  0,0,   0,8,0,   e_linelift)

		seqActor(S_movebg03,    644,-1321,-1301,   0,180,0,   3,1,0,   e_rotstand)
		seqActor(S_movebg03,   1797,-1321,  -56,   0,  0,0,   0,1,0,   e_rotstand)
		seqActor(S_movebg03,    663,-1321, 1179,   0,180,0,   3,1,0,   e_rotstand)
		seqActor(S_movebg03,   -502,-1321,  -51,   0,  0,0,   0,1,0,   e_rotstand)

		seqActor(S_movebg04,   5043,1342,300,   0,0,0,   0,1,0,   e_fireskybar)

		seqActor(S_movebg06,   3473,2422,-1821,   0,  0,  0,   0,0,0,   e_shipoar)
		seqActor(S_movebg06,   4084,2431,-2883,  45,180,180,   0,1,0,   e_shipoar)
		seqActor(S_movebg06,   3470,2420,-2869,  45,180,180,   0,1,0,   e_shipoar)
		seqActor(S_movebg06,   2856,2410,-2855,  45,180,180,   0,1,0,   e_shipoar)
		seqActor(S_movebg06,   4101,2435,-1813,   0,  0,  0,   0,0,0,   e_shipoar)
		seqActor(S_movebg06,   2859,2411,-1834,   0,  0,  0,   0,0,0,   e_shipoar)

		seqActor(S_movebg07,    -6013,-2857,6564,   0,270,0,   0,5,0,   e_seesaw)
		seqActor(S_movebg07,      614,-3574,6564,   0,270,0,   0,5,0,   e_seesaw)

		seqActor(S_movebg09,   -3557, -809,4619,   0,0,0,   0,0,0,   e_swinglift)
		seqActor(S_movebg09,   -2213,-2582,6257,   0,0,0,   0,0,0,   e_swinglift)

		seqActor(S_NULL,    0,0,0,   0,0,0,   0,0,0,   e_chikuwa)

		seqActor(S_movebg11,   -2684,1546,-36,   0,0,0,   0,5,0,   e_downlift)

		seqActor(S_movebg12,   5862,-1347,6564,   0,0,0,   0,2,0,  e_stepslope	  )
		seqActor(S_hanbutton,  4428,-1936,6564,   0,0,0,   0,0,0,  e_onoff_switch )

		seqActor(S_NULL,   614,-2857, 3671,  0,0,0,  0,204,0,  e_bar)
		seqActor(S_NULL,   621,-4598, 7362,  0,0,0,  0,117,0,  e_bar)
		seqActor(S_NULL,  5119, 3819, 3325,  0,0,0,  0, 97,0,  e_bar)
		seqActor(S_NULL,  3554, 2891,-2327,  0,0,0,  0,193,0,  e_bar)
		seqActor(S_NULL,  2680,  214,  295,  0,0,0,  0, 98,0,  e_bar)
		seqActor(S_NULL,  3811, 1033,  295,  0,0,0,  0, 98,0,  e_bar)

		seqReturn


/* ================================================================================
		: Enemies sequence.
===================================================================================	*/
SEQ_Stage15_Enemies:

		seqActor(S_NULL,  -5809,-1834, 5719,   0,0,0,   0,0,0,   e_firebigbar)
		seqActor(S_NULL,  -4838,-1015, 4081,   0,0,0,   0,0,0,   e_firebigbar)
		seqActor(S_NULL,   3301,-1834, 5617,   0,0,0,   0,0,0,   e_firebigbar)
		seqActor(S_NULL,   6772, -757, -606,   0,0,0,   0,0,0,   e_firebigbar)
		seqActor(S_NULL,  -4187, 3213,-6630,   0,0,0,   0,0,0,   e_firebigbar)

		seqReturn


/* ================================================================================
		: Stars sequence.
===================================================================================	*/
SEQ_Stage15_Stars:

		seqLevelActor(0x003f,S_polystar,1450,   3400,-2352,  0,0,0,  0,0,0,  e_tripstar)			/*on the ship	*/
		seqLevelActor(0x003f,S_polystar,-4200,  6700,-4450,  0,0,0,  1,0,0,  e_tripstar) 			/*on castle 	*/
		seqLevelActor(0x003f,S_NULL    ,-5150, -1400,    0,  0,0,0,  2,0,0,  e_tripstar_getcoins) 	/* 8_coin 		*/
		seqLevelActor(0x003f,S_polystar,-5850,  -700, 4950,  0,0,0,  3,0,0,  e_tripstar)			/*hade athretic	*/
		seqLevelActor(0x003f,S_polystar, 3700,  -400, 6600,  0,0,0,  4,0,0,  e_tripstar)			/*jimi athretic */
/*		seqLevelActor(0x003f,S_polystar, 5120,  4125, 4045,  0,0,0,  5,0,0,  e_tripstar) 			/*on island		*/

		seqReturn


/* ================================================================================
		: Stage 15 main sequence.
===================================================================================	*/
SEQ_DoStage15:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage15SegmentRomStart	  , _GfxStage15SegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE	  , _EMtTextureSegmentRomStart	  , _EMtTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_BACK	  , _BackMountainSegmentRomStart  , _BackMountainSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_kSegmentRomStart	  , _GfxEnemy1_kSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_kSegmentRomStart	  , _HmsEnemy1_kSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetEnemy1_k)
		seqCall(SetBasicEnemy)

		seqHmsShape(S_bg01, RCP_HmsExt8Bg01)
		seqHmsShape(S_bg02, RCP_HmsExt8Bg02)
		seqHmsShape(S_bg03, RCP_HmsExt8Bg03)
		seqHmsShape(S_bg04, RCP_HmsExt8Bg04)
		seqHmsShape(S_bg05, RCP_HmsExt8Bg05)
		seqHmsShape(S_bg06, RCP_HmsExt8Bg06)
		seqHmsShape(S_bg07, RCP_HmsExt8Bg07)
		seqHmsShape(S_bg08, RCP_HmsExt8Bg08)
		seqHmsShape(S_bg09, RCP_HmsExt8Bg09)
		seqHmsShape(S_bg10, RCP_HmsExt8Bg10)
		seqHmsShape(S_bg11, RCP_HmsExt8Bg11)
		seqHmsShape(S_bg12, RCP_HmsExt8Bg12)
		seqHmsShape(S_bg13, RCP_HmsExt8Bg13)
		seqHmsShape(S_bg14, RCP_HmsExt8Bg14)
		seqHmsShape(S_bg15, RCP_HmsExt8Bg15)
		seqHmsShape(S_bg16, RCP_HmsExt8Bg16)
		seqHmsShape(S_bg17, RCP_HmsExt8Bg17)
		seqHmsShape(S_bg18, RCP_HmsExt8Bg18)
		seqHmsShape(S_bg19, RCP_HmsExt8Bg19)
		seqHmsShape(S_bg20, RCP_HmsExt8Bg20)

		seqHmsShape(S_movebg01, RCP_HmsExt8Rifuto01	)
		seqHmsShape(S_movebg02, RCP_HmsExt8Arajin	)
		seqHmsShape(S_movebg03, RCP_HmsExt8Kaiten	)
		seqHmsShape(S_movebg04, RCP_HmsExt8Guruguru	)
		seqHmsShape(S_movebg05, RCP_HmsExt8Rifuto02	)
		seqHmsShape(S_movebg06, RCP_HmsExt8Hane		)
		seqHmsShape(S_movebg07, RCP_HmsExt8Shiso	)
		seqHmsShape(S_movebg08, RCP_HmsExt8Yokoshiso)
		seqHmsShape(S_movebg09, RCP_HmsExt8Buranko	)
		seqHmsShape(S_movebg10, RCP_HmsExt8Tikuwa01	)
		seqHmsShape(S_movebg11, RCP_HmsExt8Rifuto03	)
		seqHmsShape(S_movebg12, RCP_HmsExt8Dorifu00 )
		seqHmsShape(S_movebg13, RCP_HmsExt8Dorifu01 )
		seqHmsShape(S_movebg14, RCP_HmsExt8Dorifu02 )
		seqHmsShape(S_movebg15, RCP_HmsExt8Dorifu03 )
		seqHmsShape(S_movebg16, RCP_HmsExt8Dorifu04 )

		seqBeginScene(1, RCP_Stage15Scene1)
			seqActor(S_NULL,  2599,-1833+START_H, 2071,  0, 90,0,  0,10,0,	e_player_entpict)
			seqActor(S_NULL, -7092,			2364,  -63,  0, 90,0,  0,11,0,  e_warp)
			seqActor(S_NULL, -4213,			3379,-2815,  0,180,0,  0,12,0,  e_warp)
			seqPort(10, 15, 1, 10)				/*	Mario stage in.		*/
			seqPort(11, 15, 1, 12)				/*	Warp.				*/
			seqPort(12, 15, 1, 11)				/*	Warp.				*/
			seqGameClear(6, 2,  58)
			seqGameOver (6, 2, 108)
			seqCall(SEQ_Stage15_MoveBGs)
			seqCall(SEQ_Stage15_Enemies)
			seqCall(SEQ_Stage15_Stars)
			seqMapInfo(cx1501_info)
			seqTagInfo(cx1501_info_tag)
			seqSetMusic(NA_STG_GROUND, NA_SLIDER_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 90,  2599,-1833,2071)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
