/********************************************************************************
						Ultra 64 MARIO Brothers

				 	Metal mario hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 16, 1995
 ********************************************************************************/

extern ulong CtrlMarioAlpha(int code, MapNode *node, void *data);
extern ulong CtrlMarioRenderMode(int code, MapNode *node);

extern Gfx RCP_mario_head_metal[]	  ;
extern Gfx RCP_mario_far_head_metal[] ;

extern Gfx RCP_mario_nohat_metal[]	   ;
extern Gfx RCP_mario_far_nohat_metal[] ;

extern Gfx RCP_swim_hand_l_draw0[]		;
extern Gfx RCP_swim_hand_r_metal[]		;
extern Gfx RCP_hat_hand_r_metal[]		;
extern Gfx RCP_hat_hand_wing_metal[]	;
extern Gfx RCP_mario_vsign_hand_draw0[]	;

extern Gfx RCP_mario_near_body_metal[] ;
extern Gfx RCP_mario_mid_body_metal[]  ;
extern Gfx RCP_mario_far_body_metal[]  ;

extern Gfx RCP_mario_near0_draw0[]	;
extern Gfx RCP_mario_near1[]		;
extern Gfx RCP_mario_near2_draw0[]	;
extern Gfx RCP_mario_near3_metal[]	;
extern Gfx RCP_mario_near4[]		;
extern Gfx RCP_mario_near5_draw0[]	;
extern Gfx RCP_mario_near6_metal[]	;
extern Gfx RCP_mario_near7[]		;
extern Gfx RCP_mario_near8_draw0[]	;
extern Gfx RCP_mario_near9_draw0[]	;
extern Gfx RCP_mario_near10[]		;
extern Gfx RCP_mario_near11_metal[]	;
extern Gfx RCP_mario_near14_metal[]	;

extern Gfx RCP_mario_mid0_draw0[]	;
extern Gfx RCP_mario_mid1[]			;
extern Gfx RCP_mario_mid2_draw0[]	;
extern Gfx RCP_mario_mid3_metal[]	;
extern Gfx RCP_mario_mid4[]			;
extern Gfx RCP_mario_mid5_draw0[]	;
extern Gfx RCP_mario_mid6_metal[]	;
extern Gfx RCP_mario_mid7[]			;
extern Gfx RCP_mario_mid8_draw0[]	;
extern Gfx RCP_mario_mid9_draw0[]	;
extern Gfx RCP_mario_mid10[]		;
extern Gfx RCP_mario_mid11_metal[]	;
extern Gfx RCP_mario_mid14_metal[]	;

extern Gfx RCP_mario_far0_draw0[]	;
extern Gfx RCP_mario_far1[]			;
extern Gfx RCP_mario_far2_draw0[]	;
extern Gfx RCP_mario_far3_metal[]	;
extern Gfx RCP_mario_far4[]			;
extern Gfx RCP_mario_far5_draw0[]	;
extern Gfx RCP_mario_far6_metal[]	;
extern Gfx RCP_mario_far7[]			;
extern Gfx RCP_mario_far8_draw0[]	;
extern Gfx RCP_mario_far9_draw0[]	;
extern Gfx RCP_mario_far10[]		;
extern Gfx RCP_mario_far11_metal[]	;
extern Gfx RCP_mario_far14_metal[]	;


// ============================================================================================	
//
//
//		Metal Mario Hierarchy data
//
//
/********************************************************************************/
/*	Mario metal right hand with metal Wing-hat hms data [ Opaque ].				*/
/********************************************************************************/
static Hierarchy Hms_WingHat_hand_r_metal[] = {
	hmsGroup()
	hmsBegin()
		hmsGfx(RM_SURF,   RCP_hat_hand_r_metal)
		hmsGfx(RM_SPRITE, RCP_hat_hand_wing_metal)
	hmsEnd()
	hmsReturn()
};

/********************************************************************************/
/*	Mario metal right hand with metal Wing-hat hms data [ Trans ].				*/
/********************************************************************************/
static Hierarchy Hms_WingHat_hand_r_metal_xlu[] = {
	hmsGroup()
	hmsBegin()
		hmsGfx(RM_XSURF, RCP_hat_hand_r_metal)
		hmsGfx(RM_XSURF, RCP_hat_hand_wing_metal)
	hmsEnd()
	hmsReturn()
};

/********************************************************************************/
/*	Near metal mario hierarchy map data [ Opaque ].								*/
/********************************************************************************/
static Hierarchy RCP_NearMetalMarioHierarchy[] = {
	hmsJoint(RM_SURF, NULL, 0, 0, 0)											/* chn14		*/
	hmsBegin()
		hmsJoint(RM_SURF, RCP_mario_near14_metal, 0, 0, 0)							/* m_waist1_3	*/
		hmsBegin()
			hmsCProg(0, MarioProc1)
			hmsJoint(RM_SURF, RCP_mario_near_body_metal, 68, 0, 0)					/* m_body1		*/
			hmsBegin()

				hmsJoint(RM_SURF, NULL, 87, 0, 0)							/* m_head2		*/
				hmsBegin()
					hmsCProg(0, CtrlMarioHead)
					hmsRotate(0, 0, 0)								/* for control by program	*/
					hmsBegin()
						hmsGfx(RM_SURF, RCP_mario_head_metal)
					hmsEnd()
				hmsEnd()

				hmsJoint(RM_SURF, NULL, 67, -10, 79)							/* chn6			*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_near2_draw0, 0, 0, 0)				/* m_larmA1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_near1, 65, 0, 0)			/* m_larmB1		*/
						hmsBegin()
							hmsSelect(0, CtrlMarioHand)
							hmsBegin()
								hmsJoint(RM_SURF, RCP_mario_near0_draw0, 60, 0, 0)		/* m_lhamd1		*/
								hmsJoint(RM_SURF, RCP_swim_hand_l_draw0, 60, 0, 0)
								hmsJoint(RM_SURF, RCP_swim_hand_l_draw0, 60, 0, 0)
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()

				hmsJoint(RM_SURF, NULL, 68, -10, -79)						/* chn10		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_near5_draw0, 0, 0, 0)				/* m_rarmA1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_near4, 65, 0, 0)			/* m_rarmB1		*/
						hmsBegin()
							hmsSelect(0, CtrlMarioHand)
							hmsBegin()
								hmsJoint(RM_SURF, RCP_mario_near3_metal, 60, 0, 0)		/* m_rhand1		*/
								hmsBegin()
									hmsSucker(0,0,0, 0,CtrlMarioTaking)
								hmsEnd()
								hmsJoint(RM_SURF, RCP_swim_hand_r_metal, 60, 0, 0)
								hmsBegin()
									hmsSucker(0,0,0, 0,CtrlMarioTaking)
								hmsEnd()
								hmsJoint(RM_SURF, RCP_mario_vsign_hand_draw0, 60, 0, 0)
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()

			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, 42)								/* chn15		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_near11_metal, 0, 0, 0)					/* m_llegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_near10, 89, 0, 0)			/* m_llegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_near9_draw0, 67, 0, 0)			/* m_lfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, -42)								/* chn17		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_near8_draw0, 0, 0, 0)					/* m_rlegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_near7, 89, 0, 0)				/* m_rlegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_near6_metal, 67, 0, 0)			/* m_rfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

		hmsEnd()
	hmsEnd()
	hmsReturn()
};

/********************************************************************************/
/*	Near metal mario hierarchy map data [ Translucent & Transparent ].			*/
/********************************************************************************/
static Hierarchy RCP_NearMetalMarioHierarchy_XLU[] = {
	hmsJoint(RM_XSURF, NULL, 0, 0, 0)											/* chn14		*/
	hmsBegin()
		hmsJoint(RM_XSURF, RCP_mario_near14_metal, 0, 0, 0)							/* m_waist1_3	*/
		hmsBegin()
			hmsCProg(0, MarioProc1)
			hmsJoint(RM_XSURF, RCP_mario_near_body_metal, 68, 0, 0)					/* m_body1		*/
			hmsBegin()

				hmsJoint(RM_XSURF, NULL, 87, 0, 0)							/* m_head2		*/
				hmsBegin()
					hmsCProg(0, CtrlMarioHead)
					hmsRotate(0, 0, 0)								/* for control by program	*/
					hmsBegin()
						hmsGfx(RM_XSURF, RCP_mario_head_metal)
					hmsEnd()
				hmsEnd()

				hmsJoint(RM_XSURF, NULL, 67, -10, 79)							/* chn6			*/
				hmsBegin()
					hmsJoint(RM_XSURF, RCP_mario_near2_draw0, 0, 0, 0)				/* m_larmA1		*/
					hmsBegin()
						hmsJoint(RM_XSURF, RCP_mario_near1, 65, 0, 0)			/* m_larmB1		*/
						hmsBegin()
							hmsSelect(0, CtrlMarioHand)
							hmsBegin()
								hmsJoint(RM_XSURF, RCP_mario_near0_draw0, 60, 0, 0)		/* m_lhamd1		*/
								hmsJoint(RM_XSURF, RCP_swim_hand_l_draw0, 60, 0, 0)
								hmsJoint(RM_XSURF, RCP_swim_hand_l_draw0, 60, 0, 0)
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()

				hmsJoint(RM_XSURF, NULL, 68, -10, -79)						/* chn10		*/
				hmsBegin()
					hmsJoint(RM_XSURF, RCP_mario_near5_draw0, 0, 0, 0)				/* m_rarmA1		*/
					hmsBegin()
						hmsJoint(RM_XSURF, RCP_mario_near4, 65, 0, 0)			/* m_rarmB1		*/
						hmsBegin()
							hmsSelect(0, CtrlMarioHand)
							hmsBegin()
								hmsJoint(RM_XSURF, RCP_mario_near3_metal, 60, 0, 0)		/* m_rhand1		*/
								hmsBegin()
									hmsSucker(0,0,0, 0,CtrlMarioTaking)
								hmsEnd()
								hmsJoint(RM_XSURF, RCP_swim_hand_r_metal, 60, 0, 0)
								hmsBegin()
									hmsSucker(0,0,0, 0,CtrlMarioTaking)
								hmsEnd()
								hmsJoint(RM_XSURF, RCP_mario_vsign_hand_draw0, 60, 0, 0)
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()

			hmsEnd()

			hmsJoint(RM_XSURF, NULL, 13, -8, 42)								/* chn15		*/
			hmsBegin()
				hmsJoint(RM_XSURF, RCP_mario_near11_metal, 0, 0, 0)					/* m_llegA1		*/
				hmsBegin()
					hmsJoint(RM_XSURF, RCP_mario_near10, 89, 0, 0)			/* m_llegB1		*/
					hmsBegin()
						hmsJoint(RM_XSURF, RCP_mario_near9_draw0, 67, 0, 0)			/* m_lfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

			hmsJoint(RM_XSURF, NULL, 13, -8, -42)								/* chn17		*/
			hmsBegin()
				hmsJoint(RM_XSURF, RCP_mario_near8_draw0, 0, 0, 0)					/* m_rlegA1		*/
				hmsBegin()
					hmsJoint(RM_XSURF, RCP_mario_near7, 89, 0, 0)				/* m_rlegB1		*/
					hmsBegin()
						hmsJoint(RM_XSURF, RCP_mario_near6_metal, 67, 0, 0)			/* m_rfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

		hmsEnd()
	hmsEnd()
	hmsReturn()
};

/********************************************************************************/
/*	Middle metal mario hierarchy map data [ Opaque ].							*/
/********************************************************************************/
static Hierarchy RCP_MidMetalMarioHierarchy[] = {
	hmsJoint(RM_SURF, NULL, 0, 0, 0)										/* chn14		*/
	hmsBegin()
		hmsJoint(RM_SURF, RCP_mario_mid14_metal, 0, 0, 0)						/* m_waist1_3	*/
		hmsBegin()
			hmsCProg(0, MarioProc1)
			hmsJoint(RM_SURF, RCP_mario_mid_body_metal, 68, 0, 0)					/* m_body1		*/
			hmsBegin()

				hmsJoint(RM_SURF, NULL, 87, 0, 0)						/* m_head2		*/
				hmsBegin()
					hmsCProg(0, CtrlMarioHead)
					hmsRotate(0, 0, 0)							/* for control by program	*/
					hmsBegin()
						hmsGfx(RM_SURF, RCP_mario_head_metal)
					hmsEnd()
				hmsEnd()

				hmsJoint(RM_SURF, NULL, 67, -10, 79)							/* chn6			*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_mid2_draw0, 0, 0, 0)				/* m_larmA1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_mid1, 65, 0, 0)			/* m_larmB1		*/
						hmsBegin()
							hmsSelect(0, CtrlMarioHand)
							hmsBegin()
								hmsJoint(RM_SURF, RCP_mario_mid0_draw0 , 60, 0, 0)		/* m_lhamd1		*/
								hmsJoint(RM_SURF, RCP_swim_hand_l_draw0, 60, 0, 0)
								hmsJoint(RM_SURF, RCP_swim_hand_l_draw0, 60, 0, 0)
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()

				hmsJoint(RM_SURF, NULL, 68, -10, -79)						/* chn10		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_mid5_draw0, 0, 0, 0)				/* m_rarmA1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_mid4, 65, 0, 0)			/* m_rarmB1		*/
						hmsBegin()
							hmsSelect(0, CtrlMarioHand)
							hmsBegin()
								hmsJoint(RM_SURF, RCP_mario_mid3_metal, 60, 0, 0)		/* m_rhand1		*/
								hmsBegin()
									hmsSucker(0,0,0, 0,CtrlMarioTaking)
								hmsEnd()
								hmsJoint(RM_SURF, RCP_swim_hand_r_metal, 60, 0, 0)
								hmsBegin()
									hmsSucker(0,0,0, 0,CtrlMarioTaking)
								hmsEnd()
								hmsJoint(RM_SURF, RCP_mario_vsign_hand_draw0, 60, 0, 0)
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()

			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, 42)								/* chn15		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_mid11_metal, 0, 0, 0)					/* m_llegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_mid10, 89, 0, 0)				/* m_llegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_mid9_draw0, 67, 0, 0)			/* m_lfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, -42)								/* chn17		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_mid8_draw0, 0, 0, 0)					/* m_rlegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_mid7, 89, 0, 0)				/* m_rlegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_mid6_metal, 67, 0, 0)			/* m_rfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

		hmsEnd()
	hmsEnd()
	hmsReturn()
};

/********************************************************************************/
/*	Middle metal mario hierarchy map data [ Translucent & Transparent ].		*/
/********************************************************************************/
static Hierarchy RCP_MidMetalMarioHierarchy_XLU[] = {
	hmsJoint(RM_XSURF, NULL, 0, 0, 0)										/* chn14		*/
	hmsBegin()
		hmsJoint(RM_XSURF, RCP_mario_mid14_metal, 0, 0, 0)						/* m_waist1_3	*/
		hmsBegin()
			hmsCProg(0, MarioProc1)
			hmsJoint(RM_XSURF, RCP_mario_mid_body_metal, 68, 0, 0)					/* m_body1		*/
			hmsBegin()

				hmsJoint(RM_XSURF, NULL, 87, 0, 0)						/* m_head2		*/
				hmsBegin()
					hmsCProg(0, CtrlMarioHead)
					hmsRotate(0, 0, 0)							/* for control by program	*/
					hmsBegin()
						hmsGfx(RM_XSURF, RCP_mario_head_metal)
					hmsEnd()
				hmsEnd()

				hmsJoint(RM_XSURF, NULL, 67, -10, 79)							/* chn6			*/
				hmsBegin()
					hmsJoint(RM_XSURF, RCP_mario_mid2_draw0, 0, 0, 0)				/* m_larmA1		*/
					hmsBegin()
						hmsJoint(RM_XSURF, RCP_mario_mid1, 65, 0, 0)			/* m_larmB1		*/
						hmsBegin()
							hmsSelect(0, CtrlMarioHand)
							hmsBegin()
								hmsJoint(RM_XSURF, RCP_mario_mid0_draw0 , 60, 0, 0)		/* m_lhamd1		*/
								hmsJoint(RM_XSURF, RCP_swim_hand_l_draw0, 60, 0, 0)
								hmsJoint(RM_XSURF, RCP_swim_hand_l_draw0, 60, 0, 0)
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()

				hmsJoint(RM_XSURF, NULL, 68, -10, -79)						/* chn10		*/
				hmsBegin()
					hmsJoint(RM_XSURF, RCP_mario_mid5_draw0, 0, 0, 0)				/* m_rarmA1		*/
					hmsBegin()
						hmsJoint(RM_XSURF, RCP_mario_mid4, 65, 0, 0)			/* m_rarmB1		*/
						hmsBegin()
							hmsSelect(0, CtrlMarioHand)
							hmsBegin()
								hmsJoint(RM_XSURF, RCP_mario_mid3_metal, 60, 0, 0)		/* m_rhand1		*/
								hmsBegin()
									hmsSucker(0,0,0, 0,CtrlMarioTaking)
								hmsEnd()
								hmsJoint(RM_XSURF, RCP_swim_hand_r_metal, 60, 0, 0)
								hmsBegin()
									hmsSucker(0,0,0, 0,CtrlMarioTaking)
								hmsEnd()
								hmsJoint(RM_XSURF, RCP_mario_vsign_hand_draw0, 60, 0, 0)
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()

			hmsEnd()

			hmsJoint(RM_XSURF, NULL, 13, -8, 42)								/* chn15		*/
			hmsBegin()
				hmsJoint(RM_XSURF, RCP_mario_mid11_metal, 0, 0, 0)					/* m_llegA1		*/
				hmsBegin()
					hmsJoint(RM_XSURF, RCP_mario_mid10, 89, 0, 0)				/* m_llegB1		*/
					hmsBegin()
						hmsJoint(RM_XSURF, RCP_mario_mid9_draw0, 67, 0, 0)			/* m_lfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

			hmsJoint(RM_XSURF, NULL, 13, -8, -42)								/* chn17		*/
			hmsBegin()
				hmsJoint(RM_XSURF, RCP_mario_mid8_draw0, 0, 0, 0)					/* m_rlegA1		*/
				hmsBegin()
					hmsJoint(RM_XSURF, RCP_mario_mid7, 89, 0, 0)				/* m_rlegB1		*/
					hmsBegin()
						hmsJoint(RM_XSURF, RCP_mario_mid6_metal, 67, 0, 0)			/* m_rfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

		hmsEnd()
	hmsEnd()
	hmsReturn()
};

/********************************************************************************/
/*	Far metal mario hierarchy map data [ Opaque ].								*/
/********************************************************************************/
static Hierarchy RCP_FarMetalMarioHierarchy[] = {
	hmsJoint(RM_SURF, NULL, 0, 0, 0)										/* chn14		*/
	hmsBegin()
		hmsJoint(RM_SURF, RCP_mario_far14_metal, 0, 0, 0)						/* m_waist1_3	*/
		hmsBegin()
  			hmsCProg(0, MarioProc1)
			hmsJoint(RM_SURF, RCP_mario_far_body_metal, 68, 0, 0)					/* m_body1		*/
			hmsBegin()

				hmsJoint(RM_SURF, NULL, 87, 0, 0)						/* m_head2		*/
				hmsBegin()
					hmsCProg(0, CtrlMarioHead)
					hmsRotate(0, 0, 0)							/* for control by program	*/
					hmsBegin()
						hmsGfx(RM_SURF, RCP_mario_far_head_metal)
					hmsEnd()
				hmsEnd()

				hmsJoint(RM_SURF, NULL, 67, -10, 79)							/* chn6			*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_far2_draw0, 0, 0, 0)				/* m_larmA1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_far1, 65, 0, 0)			/* m_larmB1		*/
						hmsBegin()
							hmsSelect(0, CtrlMarioHand)
							hmsBegin()
								hmsJoint(RM_SURF, RCP_mario_far0_draw0, 60, 0, 0)		/* m_lhamd1		*/
								hmsJoint(RM_SURF, RCP_swim_hand_l_draw0, 60, 0, 0)
								hmsJoint(RM_SURF, RCP_swim_hand_l_draw0, 60, 0, 0)
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()

				hmsJoint(RM_SURF, NULL, 68, -10, -79)						/* chn10		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_far5_draw0, 0, 0, 0)				/* m_rarmA1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_far4, 65, 0, 0)			/* m_rarmB1		*/
						hmsBegin()
							hmsSelect(0, CtrlMarioHand)
							hmsBegin()
								hmsJoint(RM_SURF, RCP_mario_far3_metal, 60, 0, 0)		/* m_rhand1		*/
								hmsBegin()
									hmsSucker(0,0,0, 0,CtrlMarioTaking)
								hmsEnd()
								hmsJoint(RM_SURF, RCP_swim_hand_r_metal, 60, 0, 0)
								hmsBegin()
									hmsSucker(0,0,0, 0,CtrlMarioTaking)
								hmsEnd()
								hmsJoint(RM_SURF, RCP_mario_vsign_hand_draw0, 60, 0, 0)
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()

			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, 42)								/* chn15		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_far11_metal, 0, 0, 0)					/* m_llegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_far10, 89, 0, 0)				/* m_llegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_far9_draw0, 67, 0, 0)			/* m_lfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, -42)								/* chn17		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_far8_draw0, 0, 0, 0)					/* m_rlegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_far7, 89, 0, 0)				/* m_rlegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_far6_metal, 67, 0, 0)			/* m_rfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

		hmsEnd()
	hmsEnd()
	hmsReturn()
};

/********************************************************************************/
/*	Far metal mario hierarchy map data [ Translucent & Transparent ].			*/
/********************************************************************************/
static Hierarchy RCP_FarMetalMarioHierarchy_XLU[] = {
	hmsJoint(RM_XSURF, NULL, 0, 0, 0)										/* chn14		*/
	hmsBegin()
		hmsJoint(RM_XSURF, RCP_mario_far14_metal, 0, 0, 0)						/* m_waist1_3	*/
		hmsBegin()
  			hmsCProg(0, MarioProc1)
			hmsJoint(RM_XSURF, RCP_mario_far_body_metal, 68, 0, 0)					/* m_body1		*/
			hmsBegin()

				hmsJoint(RM_XSURF, NULL, 87, 0, 0)						/* m_head2		*/
				hmsBegin()
					hmsCProg(0, CtrlMarioHead)
					hmsRotate(0, 0, 0)							/* for control by program	*/
					hmsBegin()
						hmsGfx(RM_XSURF, RCP_mario_far_head_metal)
					hmsEnd()
				hmsEnd()

				hmsJoint(RM_XSURF, NULL, 67, -10, 79)							/* chn6			*/
				hmsBegin()
					hmsJoint(RM_XSURF, RCP_mario_far2_draw0, 0, 0, 0)				/* m_larmA1		*/
					hmsBegin()
						hmsJoint(RM_XSURF, RCP_mario_far1, 65, 0, 0)			/* m_larmB1		*/
						hmsBegin()
							hmsSelect(0, CtrlMarioHand)
							hmsBegin()
								hmsJoint(RM_XSURF, RCP_mario_far0_draw0, 60, 0, 0)		/* m_lhamd1		*/
								hmsJoint(RM_XSURF, RCP_swim_hand_l_draw0, 60, 0, 0)
								hmsJoint(RM_XSURF, RCP_swim_hand_l_draw0, 60, 0, 0)
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()

				hmsJoint(RM_XSURF, NULL, 68, -10, -79)						/* chn10		*/
				hmsBegin()
					hmsJoint(RM_XSURF, RCP_mario_far5_draw0, 0, 0, 0)				/* m_rarmA1		*/
					hmsBegin()
						hmsJoint(RM_XSURF, RCP_mario_far4, 65, 0, 0)			/* m_rarmB1		*/
						hmsBegin()
							hmsSelect(0, CtrlMarioHand)
							hmsBegin()
								hmsJoint(RM_XSURF, RCP_mario_far3_metal, 60, 0, 0)		/* m_rhand1		*/
								hmsBegin()
									hmsSucker(0,0,0, 0,CtrlMarioTaking)
								hmsEnd()
								hmsJoint(RM_XSURF, RCP_swim_hand_r_metal, 60, 0, 0)
								hmsBegin()
									hmsSucker(0,0,0, 0,CtrlMarioTaking)
								hmsEnd()
								hmsJoint(RM_XSURF, RCP_mario_vsign_hand_draw0, 60, 0, 0)
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()

			hmsEnd()

			hmsJoint(RM_XSURF, NULL, 13, -8, 42)								/* chn15		*/
			hmsBegin()
				hmsJoint(RM_XSURF, RCP_mario_far11_metal, 0, 0, 0)					/* m_llegA1		*/
				hmsBegin()
					hmsJoint(RM_XSURF, RCP_mario_far10, 89, 0, 0)				/* m_llegB1		*/
					hmsBegin()
						hmsJoint(RM_XSURF, RCP_mario_far9_draw0, 67, 0, 0)			/* m_lfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

			hmsJoint(RM_XSURF, NULL, 13, -8, -42)								/* chn17		*/
			hmsBegin()
				hmsJoint(RM_XSURF, RCP_mario_far8_draw0, 0, 0, 0)					/* m_rlegA1		*/
				hmsBegin()
					hmsJoint(RM_XSURF, RCP_mario_far7, 89, 0, 0)				/* m_rlegB1		*/
					hmsBegin()
						hmsJoint(RM_XSURF, RCP_mario_far6_metal, 67, 0, 0)			/* m_rfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

		hmsEnd()
	hmsEnd()
	hmsReturn()
};

/********************************************************************************/
/*	Metal Mario hierarchy map data.												*/
/********************************************************************************/
Hierarchy RCP_MetalMarioHierarchy[] = {
 	hmsShadow(100, 180, 99)
	hmsBegin()
		hmsScale(0.25f)
		hmsBegin()
			hmsCProg(0, CtrlMarioAlpha)
			hmsSelect(0, CtrlMarioLOD)
			hmsBegin()

				hmsSelect(0, CtrlMarioRenderMode)
				hmsBegin()
					hmsCall(RCP_NearMetalMarioHierarchy)
					hmsCall(RCP_NearMetalMarioHierarchy_XLU)
				hmsEnd()					

				hmsGroup()
				hmsBegin()
					hmsLOD(-2048,600)
					hmsBegin()
						hmsSelect(0, CtrlMarioRenderMode)
						hmsBegin()
							hmsCall(RCP_NearMetalMarioHierarchy)
							hmsCall(RCP_NearMetalMarioHierarchy_XLU)
						hmsEnd()					
					hmsEnd()

					hmsLOD(600,1600)
					hmsBegin()
						hmsSelect(0, CtrlMarioRenderMode)
						hmsBegin()
							hmsCall(RCP_MidMetalMarioHierarchy)
							hmsCall(RCP_MidMetalMarioHierarchy_XLU)
						hmsEnd()					
					hmsEnd()

					hmsLOD(1600,32767)
					hmsBegin()
						hmsSelect(0, CtrlMarioRenderMode)
						hmsBegin()
							hmsCall(RCP_FarMetalMarioHierarchy)
							hmsCall(RCP_FarMetalMarioHierarchy_XLU)
						hmsEnd()					
					hmsEnd()
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
