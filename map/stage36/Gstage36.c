/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage36 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define GFX_SHAPE

#define		STAGE36_FOG_R		0
#define		STAGE36_FOG_G		0
#define		STAGE36_FOG_B		0
#define		STAGE36_FOG_START	980

#include "donkey/donkey_ext_texture.h"
#include "donkey/dkwave0_texture.h"

#include "donkey/dk_body_shape.sou"
#include "donkey/dk_ki_shape.sou"
#include "donkey/dk_enoki1_a_shape.sou"
#include "donkey/dk_enoki1_b_shape.sou"
#include "donkey/dk_enoki2_a_shape.sou"
#include "donkey/dk_enoki2_b_shape.sou"
#include "donkey/dk_enoki3_a_shape.sou"
#include "donkey/dk_enoki3_b_shape.sou"
#include "donkey/dk_enoki4_a_shape.sou"
#include "donkey/dk_enoki4_b_shape.sou"
#include "donkey/dk_start_shape.sou"
#include "donkey/dk_habatobi_shape.sou"
#include "donkey/dk_taihou_shape.sou"
#include "donkey/dk_hana1_shape.sou"
#include "donkey/dk_hana2_shape.sou"
#include "donkey/dk_maruta1_shape.sou"
#include "donkey/dk_maruta2_shape.sou"
#include "donkey/dk_gururi_shape.sou"
#include "donkey/dk_kozou_shape.sou"
#include "donkey/dk_top_a_shape.sou"
#include "donkey/dk_top_b_shape.sou"
#include "donkey/dk_fuji_a_shape.sou"
#include "donkey/dk_fuji_b_shape.sou"
#include "donkey/dk_asiba1_a_shape.sou"
#include "donkey/dk_asiba1_b_shape.sou"
#include "donkey/dk_asiba2_shape.sou"
#include "donkey/dk_asiba3_shape.sou"
#include "donkey/dk_kakusi_a_shape.sou"
#include "donkey/dk_kakusi_b_shape.sou"

#include "donkey/maruta_shape.sou"
#include "donkey/donkey_wavedat.sou"
#include "donkey/dk_kago1_shape.sou"
#include "donkey/dk_kago2_shape.sou"

#include "donkey/maruta_check.flk"
#include "donkey/dk_kago_check.flk"

#include "donkey/cx3601.flk"
#include "donkey/cx3601.tag"

#include "donkey/don_ironball.ral"


/********************************************************************************/
/*	Water surface records in 36 - 1.											*/
/********************************************************************************/
static short waterSurf_36_01_00[] = {
	1,
	0,  100,  3, -2047, 2765,   -2047, 5069,    1946, 5069,    1946, 2765,  0,  150,  0,
};

WaterSurfRec waterSurf_36_01[] = {
		{	 0, waterSurf_36_01_00	 },
		{	-1,	NULL	},					//	End of WaterSurfRec data.
};

/********************************************************************************/
/*	Donkey waterfall point data.												*/
/********************************************************************************/
short donkey_wfA_pt[] = {
	35,
	2191, -1638, 1188,		0,	0,
	2165,  1638, 1145,		3,	0,
	1625,  2286,  757,		4,	0,
	1756, -1638, 1457,		0,	1,
	1729,  1638, 1413,		3,	1,
	1221,  2286, 1077,		4,	1,
};

short donkey_wfC_pt[] = {
	30,
	3583, -1638, 2751,		0,	0,
	3583, -1638,  898,		5,	0,
	1586, -1638, 2751,		0,	2,
	1586, -1638,  898, 		5,	2,
};

short donkey_wfB_pt[] = {
	25,
	3583, -3840, 2913,		0,	0,
	3583, -1889, 2913,		2,	0,
	3583, -1638, 2751,		3,	0,
	2559, -3840, 2913,		0,	1,
	2559, -1889, 2913,		2,	1,
	2457, -1638, 2751,		3,	1,
};

short donkey_wfD_pt[] = {
	20,
	3640, -3840, 3422,		0,	0,
	3792, -3840, 2849,		1,	0,
	2358, -3840, 3422,		0,	1,
	2358, -3840, 2849,		1,	1,
};

short donkey_wfE_pt[] = {
	15,
	1946, -4403, 4520,		0,	0,
	1992, -4096, 4520,		1,	0,
	3640, -4096, 3496,		3,	0,
	3640, -3840, 3422,		4,	0,
	1946, -4403, 4008,		0,	1,
	1992, -4096, 4008,		1,	1,
	2358, -4096, 3496,		3,	1,
	2358, -3840, 3422,		4,	1,
};

/********************************************************************************/
/*	Donkey waterfall draw data.													*/
/********************************************************************************/
Gfx donkey_wfAB_draw[] = {
	gsSP1Triangle(0, 1, 3, 0),
	gsSP1Triangle(3, 1, 4, 0),
	gsSP1Triangle(1, 2, 4, 0),
	gsSP1Triangle(4, 2, 5, 0),
	gsSPEndDisplayList()
};

Gfx donkey_wfCD_draw[] = {
	gsSP1Triangle(0, 1, 2, 0),
	gsSP1Triangle(2, 1, 3, 0),
	gsSPEndDisplayList()
};

Gfx donkey_wfE_draw[] = {
	gsSP1Triangle(0, 1, 4, 0),
	gsSP1Triangle(4, 1, 5, 0),
	gsSP1Triangle(1, 2, 5, 0),
	gsSP1Triangle(5, 2, 6, 0),
	gsSP1Triangle(2, 3, 6, 0),
	gsSP1Triangle(6, 3, 7, 0),
	gsSPEndDisplayList()
};


/* ----------------------------------------------------------------------------	*/
/* ----------------------------------------------------------------------------	*/


#define		STAGE3602_FOG_R			15
#define		STAGE3602_FOG_G			0
#define		STAGE3602_FOG_B			0
#define		STAGE3602_FOG_START		980

#include "slider2/abcda_2_shape.sou"
#include "slider2/a_2_geta_shape.sou"
#include "slider2/abcdb_2_shape.sou"
#include "slider2/abcdb_ya_shape.sou"
#include "slider2/b_2_geta_shape.sou"
#include "slider2/abcdc_2_shape.sou"
#include "slider2/c_2_geta_shape.sou"
#include "slider2/kazari_1_shape.sou"
#include "slider2/kazari_2_shape.sou"
#include "slider2/kazari_3_shape.sou"
#include "slider2/kazari_4_shape.sou"
#include "slider2/endoor.sou"

#include "slider2/cx3602.flk"
#include "slider2/cx3603.flk"
#include "slider2/cx3604.flk"
#include "slider2/endoor.flk"

#include "slider2/cx3602.tag"
#include "slider2/cx3603.tag"
#include "slider2/cx3604.tag"
