/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage28 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 28, 1995
 ********************************************************************************/

#include "../../headers.h"

extern ulong SelectSceneMessProc(int , MapNode*, void*);
extern ulong StarSelectSceneProc(int , MapNode*, void*);

#include "selectShape/button2.hms"
#include "selectShape/button.hms"

/********************************************************************************/
/*	Hierarchy map scene 1.	( File Select Scene)								*/
/********************************************************************************/
Hierarchy RCP_SelectScene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0,0,0,1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPersp(45, 100, 25000)
			hmsBegin()
				hmsCamera(0,  0,0,1000,  0,0,0, NULL)
				hmsBegin()
					hmsObject()
				hmsEnd()
			hmsEnd()
		hmsEnd()

		hmsLayer(0)
		hmsBegin()
			hmsCProg(0,SelectSceneMessProc)
		hmsEnd()

	hmsEnd()

	hmsExit()
};
/********************************************************************************/
/*	Hierarchy map scene 2.( Star Course Select Scene)							*/
/********************************************************************************/
Hierarchy RCP_SelectScene2[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(255,255,255,1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPersp(45, 100, 25000)
			hmsBegin()
				hmsCamera(0,  0,0,1000,  0,0,0, NULL)
				hmsBegin()
/*					hmsGfx(RM_SURF, gfx_aka_button)*/
					hmsObject()
				hmsEnd()
			hmsEnd()
		hmsEnd()

		hmsLayer(0)
		hmsBegin()
			hmsCProg(0,StarSelectSceneProc)
		hmsEnd()

	hmsEnd()

	hmsExit()
};
