/********************************************************************************
						Ultra 64 MARIO Brothers

						 game sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							May 11, 1995
 ********************************************************************************/

#define	ASSEMBLER

#include "../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_GameSequence
		.globl	SetBasicEnemy
		.globl	SetEnemy1_a
		.globl	SetEnemy1_b
		.globl	SetEnemy1_c
		.globl	SetEnemy1_d
		.globl	SetEnemy1_e
		.globl	SetEnemy1_f
		.globl	SetEnemy1_g
		.globl	SetEnemy1_h
		.globl	SetEnemy1_i
		.globl	SetEnemy1_j
		.globl	SetEnemy1_k
		.globl	SetEnemy2_a
		.globl	SetEnemy2_d
		.globl	SetEnemy2_e
		.globl	SetEnemy2_f
		.globl	SetEnemy2_g
		.globl	SetEnemy2_h


/********************************************************************************/
/*																				*/
/*	Game sequence.																*/
/*																				*/
/********************************************************************************/

SEQ_GameSequence:

	seqLoadPres(SEGMENT_PLAYER    , _GfxPlayerSegmentRomStart, _GfxPlayerSegmentRomEnd)
	seqLoadPres(SEGMENT_KEEP	  ,   _GfxKeepSegmentRomStart,   _GfxKeepSegmentRomEnd)
	seqLoadData(SEGMENT_PLAYDATA  , _HmsPlayerSegmentRomStart, _HmsPlayerSegmentRomEnd)
	seqLoadData(SEGMENT_KEEPDATA  ,   _HmsKeepSegmentRomStart,   _HmsKeepSegmentRomEnd)
	seqLoadData(SEGMENT_PATHDATA  ,      _PathSegmentRomStart,      _PathSegmentRomEnd)


	seqBeginConstruction()

		seqHmsShape(S_Mario				, RCP_MarioHierarchy				)
		seqHmsShape(S_dust				, RCP_HmsEffectDust					)
		seqHmsShape(S_spark				, RCP_HmsEffectSpark				)
		seqHmsShape(S_bubble			, RCP_HmsEffectBubble				)
		seqHmsShape(S_rippleA			, RCP_HmsEffectRippleA				)
		seqHmsShape(S_rippleB			, RCP_HmsEffectRippleB				)
		seqHmsShape(S_watercolumn		, RCP_HmsEffectWaterColumn			)
		seqHmsShape(S_wave				, RCP_HmsEffectWave					)
		seqHmsShape(S_coin		  	    , RCP_HmsItemCoin					)
		seqHmsShape(S_polystar			, RCP_HmsPolygonStar				)
		seqHmsShape(S_shadestar			, RCP_HmsShadeStar					)
		seqHmsShape(S_tatefuda			, RCP_HmsTatefuda					)
		seqGfxShape(S_waterdrop			, gfx_waterdrop			, RM_SPRITE )	
		seqHmsShape(S_fireball_yellow	, RCP_HmsFireD_Yellow				)
		seqHmsShape(S_fireball_blue		, RCP_HmsFireD_Blue					)
		seqHmsShape(S_firesmoke	 		, RCP_HmsGasB						)
		seqHmsShape(S_grass		 		, RCP_HmsGrassPiece					)
		seqHmsShape(S_syabon	 		, RCP_HmsEffectSyabon				)
		seqHmsShape(S_fish				, RCP_HmsEnemyfish					)
		seqHmsShape(S_fishshadow		, RCP_HmsEnemyfish_shadow			)
		seqHmsShape(S_mini_twinkle		, RCP_HmsStarFlash					)
		seqGfxShape(S_sand_dust			, RCP_sand_dust			,RM_SPRITE	)
        seqHmsShape(S_butterfly	 		, RCP_ButterflyHierarchy			)
		seqHmsShape(S_gas_b				, RCP_HmsGasB						)
		seqGfxShape(S_stone			 	, RCP_stone, RM_SPRITE	 			)
		seqHmsShape(S_hiteffect			, RCP_HmsAttackEffect				)
		seqHmsShape(S_blackeffect		, RCP_HmsBlackEffect				)
		seqGfxShape(S_snowdust			, RCP_snow_ball,RM_SPRITE			)
		seqHmsShape(S_snowball			, RCP_HmsSnowBall					)
		seqHmsShape(S_coin_noshadow		, RCP_HmsItemCoinNoShadow			)
		seqHmsShape(S_bluecoin		  	, RCP_HmsItemBlueCoin					)
		seqHmsShape(S_bluecoin_noshadow	, RCP_HmsItemBlueCoinNoShadow			)
		seqHmsShape(S_itemhat_metalwing	, Hms_MetalWingItemHatHierarchy			)
		seqHmsShape(S_itemhat_metal		, Hms_MetalItemHatHierarchy				)
		seqHmsShape(S_itemhat_wing		, Hms_WingItemHatHierarchy				)
		seqHmsShape(S_itemhat_hat		, Hms_ItemHatHierarchy					)
		seqHmsShape(S_itemhat_hat		, Hms_ItemHatHierarchy					)
		seqHmsShape(S_doorkey_demo		, RCP_HmsEnemydoorkey					)
		seqHmsShape(S_kopakey           , RCP_HmsKopaKey   						)
		seqHmsShape(S_fireball_yellow_shadow	, RCP_HmsFireD_Yellow_shadow	)
		seqHmsShape(S_oneup_kinoko		, RCP_HmsOneUpKinoko					)
		seqHmsShape(S_redcoin		  	, RCP_HmsItemRedCoin					)
		seqHmsShape(S_redcoin_noshadow	, RCP_HmsItemRedCoinNoShadow			)
		seqHmsShape(S_numbers			, RCP_HmsItemNumber						)
		seqHmsShape(S_bombfire  		, RCP_HmsItembombfire					)
		seqHmsShape(S_sankaku	 		, RCP_HmsSankaku					)
		seqHmsShape(S_polygon_hoshi		, RCP_HmsHoshi						)

	seqEndConstruction()




/********************************************************************************/

	seqCProgram(GameInitialize, 0)
	seqRepeat
		seqExecute(SEGMENT_DEMODATA, _HmsSelectSegmentRomStart, _HmsSelectSegmentRomEnd, SEQ_CourseMenu)
		seqCall(SEQ_BranchSequence)
		seqWait(1)
	seqUntil(SEQ_GT, 0)


/********************************************************************************/
/*																				*/
/*	Branch each title sequence.													*/
/*																				*/
/********************************************************************************/

	seqTstJump(SEQ_EQ,  -1, SEQ_GoFin   )
	seqTstJump(SEQ_EQ,  -2, SEQ_GoTitle )
	seqTstJump(SEQ_EQ,  -3, SEQ_GoGmOver)
	seqTstJump(SEQ_EQ,  -8, SEQ_GoLogo  )
	seqTstJump(SEQ_EQ,  -9, SEQ_GoDebug )

SEQ_GoLogo:		seqChain(SEGMENT_DEMODATA  ,	  _TitleSegmentRomStart,	  _TitleSegmentRomEnd, SEQ_LogoSequence  )
SEQ_GoFin:		seqChain(SEGMENT_STAGEDATA1, _MapStage25SegmentRomStart, _MapStage25SegmentRomEnd, SEQ_DoStage25	 )
SEQ_GoTitle:	seqChain(SEGMENT_DEMODATA  ,	  _TitleSegmentRomStart,	  _TitleSegmentRomEnd, SEQ_TitleSequence )
SEQ_GoGmOver:	seqChain(SEGMENT_DEMODATA  ,	  _TitleSegmentRomStart,	  _TitleSegmentRomEnd, SEQ_GmOverSequence)
SEQ_GoDebug:	seqChain(SEGMENT_DEMODATA  ,	  _TitleSegmentRomStart,	  _TitleSegmentRomEnd, SEQ_DebugSequence )


/********************************************************************************/
/*																				*/
/*	Branch each stage sequence.													*/
/*																				*/
/********************************************************************************/

SEQ_BranchSequence:
	seqGetGameMode(GMODE_STAGE)
/*	seqTstJump(SEQ_EQ,  1, SEQ_GoStage01)	*/
/*	seqTstJump(SEQ_EQ,  2, SEQ_GoStage02)	*/
/*	seqTstJump(SEQ_EQ,  3, SEQ_GoStage03)	*/
	seqTstJump(SEQ_EQ,  4, SEQ_GoStage04)
	seqTstJump(SEQ_EQ,  5, SEQ_GoStage05)
	seqTstJump(SEQ_EQ,  6, SEQ_GoStage06)
	seqTstJump(SEQ_EQ,  7, SEQ_GoStage07)
	seqTstJump(SEQ_EQ,  8, SEQ_GoStage08)
	seqTstJump(SEQ_EQ,  9, SEQ_GoStage09)
	seqTstJump(SEQ_EQ, 10, SEQ_GoStage10)
	seqTstJump(SEQ_EQ, 11, SEQ_GoStage11)
	seqTstJump(SEQ_EQ, 12, SEQ_GoStage12)
	seqTstJump(SEQ_EQ, 13, SEQ_GoStage13)
	seqTstJump(SEQ_EQ, 14, SEQ_GoStage14)
	seqTstJump(SEQ_EQ, 15, SEQ_GoStage15)
	seqTstJump(SEQ_EQ, 16, SEQ_GoStage16)
	seqTstJump(SEQ_EQ, 17, SEQ_GoStage17)
	seqTstJump(SEQ_EQ, 18, SEQ_GoStage18)
	seqTstJump(SEQ_EQ, 19, SEQ_GoStage19)
	seqTstJump(SEQ_EQ, 20, SEQ_GoStage20)
	seqTstJump(SEQ_EQ, 21, SEQ_GoStage21)
	seqTstJump(SEQ_EQ, 22, SEQ_GoStage22)
	seqTstJump(SEQ_EQ, 23, SEQ_GoStage23)
	seqTstJump(SEQ_EQ, 24, SEQ_GoStage24)
	seqTstJump(SEQ_EQ, 25, SEQ_GoStage25)
	seqTstJump(SEQ_EQ, 26, SEQ_GoStage26)
	seqTstJump(SEQ_EQ, 27, SEQ_GoStage27)
	seqTstJump(SEQ_EQ, 28, SEQ_GoStage28)
	seqTstJump(SEQ_EQ, 29, SEQ_GoStage29)
	seqTstJump(SEQ_EQ, 30, SEQ_GoStage30)
	seqTstJump(SEQ_EQ, 31, SEQ_GoStage31)
/*	seqTstJump(SEQ_EQ, 32, SEQ_GoStage32)	*/
	seqTstJump(SEQ_EQ, 33, SEQ_GoStage33)
	seqTstJump(SEQ_EQ, 34, SEQ_GoStage34)
/*	seqTstJump(SEQ_EQ, 35, SEQ_GoStage35)	*/
	seqTstJump(SEQ_EQ, 36, SEQ_GoStage36)
/*	seqTstJump(SEQ_EQ, 37, SEQ_GoStage37)	*/
/*	seqTstJump(SEQ_EQ, 38, SEQ_GoStage38)	*/
	seqExit

/*
SEQ_GoStage01:	seqExecute(SEGMENT_STAGEDATA1,  _MapStage1SegmentRomStart,  _MapStage1SegmentRomEnd, SEQ_DoStage01)
				seqReturn
SEQ_GoStage02:	seqExecute(SEGMENT_STAGEDATA1,  _MapStage2SegmentRomStart,  _MapStage2SegmentRomEnd, SEQ_DoStage02)
				seqReturn
SEQ_GoStage03:	seqExecute(SEGMENT_STAGEDATA1,  _MapStage3SegmentRomStart,  _MapStage3SegmentRomEnd, SEQ_DoStage03)
				seqReturn
*/

SEQ_GoStage04:	seqExecute(SEGMENT_STAGEDATA1,  _MapStage4SegmentRomStart,  _MapStage4SegmentRomEnd, SEQ_DoStage04)
				seqReturn
SEQ_GoStage05:	seqExecute(SEGMENT_STAGEDATA1,  _MapStage5SegmentRomStart,  _MapStage5SegmentRomEnd, SEQ_DoStage05)
				seqReturn
SEQ_GoStage06:	seqExecute(SEGMENT_STAGEDATA1,  _MapStage6SegmentRomStart,  _MapStage6SegmentRomEnd, SEQ_DoStage06)
				seqReturn
SEQ_GoStage07:	seqExecute(SEGMENT_STAGEDATA1,  _MapStage7SegmentRomStart,  _MapStage7SegmentRomEnd, SEQ_DoStage07)
				seqReturn
SEQ_GoStage08:	seqExecute(SEGMENT_STAGEDATA1,  _MapStage8SegmentRomStart,  _MapStage8SegmentRomEnd, SEQ_DoStage08)
				seqReturn
SEQ_GoStage09:	seqExecute(SEGMENT_STAGEDATA1,  _MapStage9SegmentRomStart,  _MapStage9SegmentRomEnd, SEQ_DoStage09)
				seqReturn
SEQ_GoStage10:	seqExecute(SEGMENT_STAGEDATA1, _MapStage10SegmentRomStart, _MapStage10SegmentRomEnd, SEQ_DoStage10)
				seqReturn
SEQ_GoStage11:	seqExecute(SEGMENT_STAGEDATA1, _MapStage11SegmentRomStart, _MapStage11SegmentRomEnd, SEQ_DoStage11)
				seqReturn
SEQ_GoStage12:	seqExecute(SEGMENT_STAGEDATA1, _MapStage12SegmentRomStart, _MapStage12SegmentRomEnd, SEQ_DoStage12)
				seqReturn
SEQ_GoStage13:	seqExecute(SEGMENT_STAGEDATA1, _MapStage13SegmentRomStart, _MapStage13SegmentRomEnd, SEQ_DoStage13)
				seqReturn
SEQ_GoStage14:	seqExecute(SEGMENT_STAGEDATA1, _MapStage14SegmentRomStart, _MapStage14SegmentRomEnd, SEQ_DoStage14)
				seqReturn
SEQ_GoStage15:	seqExecute(SEGMENT_STAGEDATA1, _MapStage15SegmentRomStart, _MapStage15SegmentRomEnd, SEQ_DoStage15)
				seqReturn
SEQ_GoStage16:	seqExecute(SEGMENT_STAGEDATA1, _MapStage16SegmentRomStart, _MapStage16SegmentRomEnd, SEQ_DoStage16)
				seqReturn
SEQ_GoStage17:	seqExecute(SEGMENT_STAGEDATA1, _MapStage17SegmentRomStart, _MapStage17SegmentRomEnd, SEQ_DoStage17)
				seqReturn
SEQ_GoStage18:	seqExecute(SEGMENT_STAGEDATA1, _MapStage18SegmentRomStart, _MapStage18SegmentRomEnd, SEQ_DoStage18)
				seqReturn
SEQ_GoStage19:	seqExecute(SEGMENT_STAGEDATA1, _MapStage19SegmentRomStart, _MapStage19SegmentRomEnd, SEQ_DoStage19)
				seqReturn
SEQ_GoStage20:	seqExecute(SEGMENT_STAGEDATA1, _MapStage20SegmentRomStart, _MapStage20SegmentRomEnd, SEQ_DoStage20)
				seqReturn
SEQ_GoStage21:	seqExecute(SEGMENT_STAGEDATA1, _MapStage21SegmentRomStart, _MapStage21SegmentRomEnd, SEQ_DoStage21)
				seqReturn
SEQ_GoStage22:	seqExecute(SEGMENT_STAGEDATA1, _MapStage22SegmentRomStart, _MapStage22SegmentRomEnd, SEQ_DoStage22)
				seqReturn
SEQ_GoStage23:	seqExecute(SEGMENT_STAGEDATA1, _MapStage23SegmentRomStart, _MapStage23SegmentRomEnd, SEQ_DoStage23)
				seqReturn
SEQ_GoStage24:	seqExecute(SEGMENT_STAGEDATA1, _MapStage24SegmentRomStart, _MapStage24SegmentRomEnd, SEQ_DoStage24)
				seqReturn
SEQ_GoStage25:	seqExecute(SEGMENT_STAGEDATA1, _MapStage25SegmentRomStart, _MapStage25SegmentRomEnd, SEQ_DoStage25)
				seqReturn
SEQ_GoStage26:	seqExecute(SEGMENT_STAGEDATA1, _MapStage26SegmentRomStart, _MapStage26SegmentRomEnd, SEQ_DoStage26)
				seqReturn
SEQ_GoStage27:	seqExecute(SEGMENT_STAGEDATA1, _MapStage27SegmentRomStart, _MapStage27SegmentRomEnd, SEQ_DoStage27)
				seqReturn
SEQ_GoStage28:	seqExecute(SEGMENT_STAGEDATA1, _MapStage28SegmentRomStart, _MapStage28SegmentRomEnd, SEQ_DoStage28)
				seqReturn
SEQ_GoStage29:	seqExecute(SEGMENT_STAGEDATA1, _MapStage29SegmentRomStart, _MapStage29SegmentRomEnd, SEQ_DoStage29)
				seqReturn
SEQ_GoStage30:	seqExecute(SEGMENT_STAGEDATA1, _MapStage30SegmentRomStart, _MapStage30SegmentRomEnd, SEQ_DoStage30)
				seqReturn
SEQ_GoStage31:	seqExecute(SEGMENT_STAGEDATA1, _MapStage31SegmentRomStart, _MapStage31SegmentRomEnd, SEQ_DoStage31)
				seqReturn
/*
SEQ_GoStage32:	seqExecute(SEGMENT_STAGEDATA1, _MapStage32SegmentRomStart, _MapStage32SegmentRomEnd, SEQ_DoStage32)
				seqReturn
*/

SEQ_GoStage33:	seqExecute(SEGMENT_STAGEDATA1, _MapStage33SegmentRomStart, _MapStage33SegmentRomEnd, SEQ_DoStage33)
				seqReturn
SEQ_GoStage34:	seqExecute(SEGMENT_STAGEDATA1, _MapStage34SegmentRomStart, _MapStage34SegmentRomEnd, SEQ_DoStage34)
				seqReturn

/*
SEQ_GoStage35:	seqExecute(SEGMENT_STAGEDATA1, _MapStage35SegmentRomStart, _MapStage35SegmentRomEnd, SEQ_DoStage35)
				seqReturn
*/

SEQ_GoStage36:	seqExecute(SEGMENT_STAGEDATA1, _MapStage36SegmentRomStart, _MapStage36SegmentRomEnd, SEQ_DoStage36)
				seqReturn

/*
SEQ_GoStage37:	seqExecute(SEGMENT_STAGEDATA1, _MapStage37SegmentRomStart, _MapStage37SegmentRomEnd, SEQ_DoStage37)
				seqReturn
SEQ_GoStage38:	seqExecute(SEGMENT_STAGEDATA1, _MapStage38SegmentRomStart, _MapStage38SegmentRomEnd, SEQ_DoStage38 )
				seqReturn
*/


/********************************************************************************/
/*																				*/
/*	Branch each stage sequence.													*/
/*																				*/
/********************************************************************************/

/*------------------------------------------------------------------------------*/
/*			Basic Enemy															*/
/*------------------------------------------------------------------------------*/
SetBasicEnemy:
		seqHmsShape(S_BCbutton			, RCP_HmsBCbutton					)
		seqHmsShape(S_biribiri			, RCP_HmsEnemybiribiri				)
		seqHmsShape(S_hanbutton			, RCP_HmsHanButton					)
		seqHmsShape(S_commonlift		, RCP_HmsHanLift					)
		seqHmsShape(S_hibiblock			 , RCP_HmsHibiBlock					)
		seqHmsShape(S_hibiblock_noshadow , RCP_HmsHibiBlock_noshadow		)
		seqHmsShape(S_qbox_box	 		, RCP_HmsQBox						)
		seqHmsShape(S_itembox	 		, RCP_HmsItemBox					)
		seqHmsShape(S_kuribo			 , RCP_HmsEnemykuribo				)
		seqGfxShape(S_qbox_mark 		, gfx_q_mark	,RM_SPRITE			)
		seqHmsShape(S_koura				, RCP_HmsSellBoard 					)
		seqHmsShape(S_pushblock			, RCP_HmsPushBlock					)
		seqGfxShape(S_pushblock_noshadow, gfx_push_block ,RM_SURF			)
		seqHmsShape(S_blackbom			, RCP_HmsEnemyNewBlackBom 			)
		seqHmsShape(S_redbom			, RCP_HmsEnemyNewRedBom 			)
		seqGfxShape(S_bom_futa			, gfx_bom_futa , RM_SURF			)
		seqHmsShape(S_ironball 			, RCP_HmsIronBall					)			
		seqHmsShape(S_cannon_body		, RCP_HmsCannonBody					)
		seqHmsShape(S_cannon_base		, RCP_HmsCannonBase					)
		seqHmsShape(S_hart				, RCP_HmsHart						)
		seqHmsShape(S_heyho		 		, RCP_HmsEnemyheyho_fly				)
		seqHmsShape(S_carryboy			, RCP_HmsEnemycarryboy				)

		seqHmsShape(S_ironball_noshadow		, RCP_HmsIronBall_noshadow			)	

		seqReturn

/*------------------------------------------------------------------------------*/
/*			EnemyBank (1)														*/
/*------------------------------------------------------------------------------*/
SetEnemy1_a:
		seqHmsShape(S_killer			, RCP_HmsEnemyKiller   		 		)
		seqHmsShape(S_yellowball  		, RCP_HmsYellowBall					)
		seqHmsShape(S_bird       		, RCP_birdHierarchy					)
		seqHmsShape(S_bird_egg   		, RCP_bird_eggHierarchy				)
		seqHmsShape(S_dossun			, RCP_HmsDosun						)
		seqHmsShape(S_horiage			, RCP_HmsEnemyomurobo				)
		seqReturn

/*------------------------------------------------------------------------------*/
SetEnemy1_b:
		seqHmsShape(S_unbaba			, RCP_HmsEnemyunbaba  			 	)
		seqHmsShape(S_otos				, RCP_HmsEnemyotos_basedata		 	)	
		seqHmsShape(S_big_otos			, RCP_HmsEnemyBigotos_basedata		)	
		seqReturn

/*------------------------------------------------------------------------------*/
SetEnemy1_c:
		seqHmsShape(S_water_bom			, RCP_HmsWaterBom					)
		seqHmsShape(S_water_bom_shadow	, RCP_HmsWaterBomShadow				)
		seqHmsShape(S_bomking			, RCP_HmsEnemybomking				)
		seqReturn

/*------------------------------------------------------------------------------*/
SetEnemy1_d:
		seqHmsShape(S_manta				, RCP_HmsEnemymanta_anim_swim		)	
		seqHmsShape(S_ubboo				, RCP_HmsEnemyubboo					)
		seqHmsShape(S_shark				, RCP_HmsEnemyShark					)
		seqGfxShape(S_w_tornade			, gfx_water_tornado		,RM_XSURF	)
		seqHmsShape(S_big_shell			, RCP_HmsEnemybig_shell				)
		seqReturn

/*------------------------------------------------------------------------------*/
SetEnemy1_e:
		seqHmsShape(S_sanbo_head	 	, RCP_HmsSanboHead 					)
		seqHmsShape(S_sanbo_body	 	, RCP_HmsSanboBody				 	)
		seqHmsShape(S_tornedo 		 	, RCP_HmsTornado 					)
		seqHmsShape(S_hagetaka		 	, RCP_HmsEnemyhagetaka_anim_fly		)
		seqHmsShape(S_handman_Lhand	 	, RCP_HmsEnemyhandman_Lhand			)
		seqHmsShape(S_handman_Rhand	 	, RCP_HmsEnemyhandman_Rhand			)
		seqReturn

/*------------------------------------------------------------------------------*/
SetEnemy1_f:
		seqGfxShape(S_poohole			, RCP_hole, RM_XDECAL	 			)
		seqHmsShape(S_indy		 		, RCP_HmsEnemyindy	 				)
		seqHmsShape(S_monky	 	 		, RCP_HmsEnemymonky_F_run			)
		seqHmsShape(S_windface		 	, RCP_HmsWindFace					)
		seqReturn

/*------------------------------------------------------------------------------*/
SetEnemy1_g:															
		seqHmsShape(S_furafura			, RCP_HmsEnemyfurafura				)
		seqHmsShape(S_snowman			, RCP_HmsEnemysnowman				)
		seqHmsShape(S_shadow_snowman	, RCP_HmsEnemysnowman_shadow		)
		seqHmsShape(S_ping	 			, RCP_HmsEnemyping_base				)
		seqReturn

/*------------------------------------------------------------------------------*/
SetEnemy1_h:
		seqGfxShape(S_bikkuri_mark	 	, gfx_button_mk,RM_SPRITE		)
		seqHmsShape(S_bikkuri_button 	, RCP_HmsBikkuriButton			)
		seqGfxShape(S_bikkuri_dodai	 	, gfx_dodai	,RM_SURF			)
		seqReturn

/*------------------------------------------------------------------------------*/
SetEnemy1_i:
		seqHmsShape(S_teresa 		 	, RCP_HmsEnemyTeresa 			)
		seqHmsShape(S_key			 	, RCP_HmsKey					)
		seqHmsShape(S_chair		 	 	, RCP_HmsEnemychair 			)
		seqHmsShape(S_piano		 	 	, RCP_HmsEnemypiano 			)
		seqHmsShape(S_book		 	 	, RCP_HmsEnemybook 				)
		seqHmsShape(S_booklight		 	, RCP_HmsBookLight 				)
		seqHmsShape(S_teresakago 	 	, RCP_HmsTeresaKago				)
		seqReturn

/*------------------------------------------------------------------------------*/
SetEnemy1_j:
		seqHmsShape(S_minibird		 	 , RCP_HmsEnemyminibird 	 	)
		seqHmsShape(S_peach				 , RCP_HmsEnemypeach			)
		seqHmsShape(S_yoshi				 , RCP_HmsEnemyyoshi			)
		seqReturn

/*------------------------------------------------------------------------------*/
SetEnemy1_k:															
		seqHmsShape(S_jugem				, RCP_HmsEnemyjygem	 			)
		seqHmsShape(S_pipo				, RCP_HmsEnemypipo	 			)
		seqHmsShape(S_togezo			, RCP_HmsEnemytogezo			)
		seqHmsShape(S_hanachan_head		, RCP_HmsEnemyhanahead	 		)
		seqHmsShape(S_hanachan_body		, RCP_HmsEnemyhanabody	 		)
		seqHmsShape(S_buku				, RCP_buku		 				)
		seqReturn

/*------------------------------------------------------------------------------*/
/*			EnemyBank (2)														*/
/*------------------------------------------------------------------------------*/
SetEnemy2_a:
		seqHmsShape(S_kopa				, RCP_HmsEnemykopa_basedata		)
		seqHmsShape(S_kopafire  		, RCP_HmsKopaKirai				)
		seqHmsShape(S_kirai 	 		, RCP_HmsKopaKirai 				)
		seqHmsShape(S_kopakirai_smoke 	, RCP_HmsMowan 					)
		seqHmsShape(S_kopakirai_fire 	, RCP_HmsBlowUp 				)
		seqHmsShape(S_firering 			, RCP_HmsFireRing 				)
		seqHmsShape(S_kopa_noshadow		, RCP_HmsEnemykopa_noshadow		)
		seqReturn

/*------------------------------------------------------------------------------*/
SetEnemy2_d:
		seqHmsShape(S_PukuPuku			, RCP_HmsEnemypuku_anim			)
		seqHmsShape(S_t_box1			, RCP_HmsT_BoxA					)
		seqHmsShape(S_t_box2			, RCP_HmsT_BoxB					)
		seqHmsShape(S_pirania			, RCP_HmsEnemypirania			)
		seqHmsShape(S_ring				, RCP_HmsEnemyRing				)	
		seqHmsShape(S_kirai				, RCP_HmsMine					)
		seqHmsShape(S_kombu				, RCP_HmsEnemykomubu			)
		seqHmsShape(S_amembow			, RCP_HmsEnemyamembow			)
		seqReturn

/*------------------------------------------------------------------------------*/
SetEnemy2_e:
		seqHmsShape(S_pakun	 			, RCP_HmsEnemypakun_angry		)
		seqHmsShape(S_wallman	 		, RCP_HmsEnemywallman			)
		seqHmsShape(S_nokonoko			, RCP_HmsEnemynokonoko	 		)
		seqHmsShape(S_nokonoko_nude	 	, RCP_HmsEnemynokonoko_nude		)
		seqHmsShape(S_wan_ball			, RCP_HmsEnemywan_ball		 	)
		seqHmsShape(S_wanwan  			, RCP_HmsEnemywanwan	 		)
		seqHmsShape(S_goal_flag			, RCP_HmsEnemygoal_flag	 		)
		seqHmsShape(S_kui				, RCP_HmsKui	 				)
		seqReturn

/*------------------------------------------------------------------------------*/
SetEnemy2_f:
		seqHmsShape(S_rabbit		 	, RCP_rabbitHierarchy	 		)
		seqHmsShape(S_michi_teresa	 	, RCP_HmsEnemyTeresa2			)
		seqHmsShape(S_camera_jugem	 	, RCP_HmsEnemycamerajugem		)
		seqHmsShape(S_kinopio		 	, RCP_HmsEnemykinopio	 		)
		seqReturn

/*------------------------------------------------------------------------------*/
SetEnemy2_g:
		seqHmsShape(S_ice_otos		 	, RCP_HmsEnemyIce_otos_basedata		)
		seqHmsShape(S_Big_ice_otos	 	, RCP_HmsEnemyBigIce_otos_basedata	)
		seqHmsShape(S_kuramochi		 	, RCP_HmsEnemykuramochi				) 
		seqReturn

/*------------------------------------------------------------------------------*/
SetEnemy2_h:
		seqHmsShape(S_bat				, RCP_HmsEnemybat	 			 	)
		seqHmsShape(S_kumo				, RCP_HmsEnemywalker				)
		seqHmsShape(S_ballooneye		, RCP_HmsBalloonEye				 	)
		seqHmsShape(S_balloonbody		, RCP_HmsBalloonBody			 	)
		seqHmsShape(S_nessy				, RCP_HmsEnemynessy 			 	)
		seqHmsShape(S_mucho				, RCP_HmsEnemymucho					)
		seqReturn







