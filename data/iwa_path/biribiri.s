/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Biri Biri Strategy

				Junuary 31 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
		Biri Biri Pathdata
 ********************************************************************************/
e_biribiri:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON|stf_FspeedON| stf_YangleSAME | stf_playerdistON | stf_playerangleON)
	p_set_pointer(skelanime,biribiri_anime)
	p_set_skelanime_number(0)
	p_setf(animepositionY,40)
	p_hitON
	p_program(s_biribiri_init)

	p_while
		p_program(s_biribiri_main)
	p_loop


e_gurubiri:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON|stf_FspeedON | stf_playerdistON | stf_playerangleON)
	p_set_pointer(skelanime,biribiri_anime)
	p_set_skelanime_number(0)
	p_setf(animepositionY,40)
	p_hitON
	p_program(s_gurubiri_init)

	p_while
		p_program(s_gurubiri_main)
	p_loop





/*################*/
#else
/*################*/


/***************************************************************************************************
		C Program
 ****************************************************************************************************/
#define biri_attack_flag (execstp->s[stw_work0].d)
#define biri_counter	 (execstp->s[stw_work1].d)
#define biri_WorldY 	 (execstp->s[stw_work2].f)


static HitCheckRecord biribiri_hit = {
	OBJNAME_BIRIBIRI,
	40,1,0,0,			/* flag,ap,hp,coin 		 */
	 40,       50,		/* player attack hitarea */
	 40+10, 50+10,		/* player damage hitarea */
};


/***************************************************************************************************
						    Initial
***************************************************************************************************/
extern void s_biribiri_init(void)
{
	obj_attX = obj_worldX;
	obj_attY = obj_worldY;
	obj_attZ = obj_worldZ;

	execstp->s[stw_gravity].f   = 0.;
	execstp->s[stw_friction].f  = 1.0;
	execstp->s[stw_specificG].f = 1.0;

	biri_WorldY = obj_attY;

	s_set_scale(0.1);

	MapHideShape(&execstp->map);

}
/***************************************************************************************************
						    Biribiri Attack
***************************************************************************************************/
static void biri_PlayerTouch(void)
{

	s_set_hitparam(execstp,&biribiri_hit);		/* Set HitCheck Param */


	if ( obj_mail & EMAIL_PLAYERHIT ){
		if ( obj_mail & EMAIL_PLAYERHIT ) obj_mode = 4;

		Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
	}


}
/***************************************************************************************************
			Biri Appear
***************************************************************************************************/
static void biri_Apear(void)
{
	float dx = camWork.Sposition[_AX] - obj_worldX;
	float dz = camWork.Sposition[_AZ] - obj_worldZ;
	short targetAngleY = atan(dz,dx);

 	obj_angleY = s_chase_angle(obj_angleY,targetAngleY,(0x1000));


	if (obj_timer < FRAME) {
		s_set_scale(0.1+((float)obj_timer/FRAME)*0.9);
	}
	else {
		obj_animecounter = 1;
	}

	if (obj_timer > FRAME*3) {
		s_set_scale(1.0);
		obj_mode = 2;
		biri_counter=0;
	}

}
/***************************************************************************************************
			Player Attack
***************************************************************************************************/
static void biri_PlayerAttack(void)
{
	


	if (obj_angleY > obj_targetangle-0x400 && obj_angleY < obj_targetangle+0x400) {
		biri_attack_flag = 1;
		obj_timer = 0;
	}


	if (biri_attack_flag == 1) {
		obj_speedF = 15;
		if (biri_WorldY > player_posY+150) biri_WorldY -= 10;
		else							   biri_WorldY = player_posY+150;

		if (obj_timer > FRAME) biri_attack_flag = 0;
	}
	else {
		obj_speedF = 10;
		s_chase_obj_angle(execstp,player1stp,stw_angleY,0x400);
		if (biri_WorldY < player_posY+(250)) biri_WorldY += 10;

	}
	obj_worldY = biri_WorldY + sin(biri_counter*0x400)*20;


	biri_PlayerTouch();

	if (PlayerApproach(obj_attX,obj_attY,obj_attZ,1500)==0) obj_mode = 3;
}
/***************************************************************************************************
			Player Remove
***************************************************************************************************/
static void biri_Remove(void)
{
	StrategyRecord *stratp;

	obj_speedF = 15.;
	
	if (obj_timer > FRAME*5) {
		obj_worldX = obj_attX;
		obj_worldY = obj_attY;
		obj_worldZ = obj_attZ;
		MapHideShape(&execstp->map);
		obj_mode = 0;
		obj_animecounter = 0;
		obj_speedF = 0.;
		biri_WorldY = obj_attY;
	}


}
/***************************************************************************************************
		 biribiri Win
***************************************************************************************************/
static void biri_Win(void)
{
	execstp->map.skelanim.frame += 2;

	obj_speedF = 0.;
	s_hitOFF();

	if (obj_timer > FRAME) {
		obj_animecounter = 0;
	}

	if (obj_timer > FRAME*3) {
		obj_animecounter = 1;
		s_hitON();
		obj_mode = 2;
	}

}
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_biribiri_main(void)
{

	switch(obj_mode) {
		case 0: if (PlayerApproach(obj_attX,obj_attY,obj_attZ,800)==1) {
					obj_mode = 1;
					MapDispShape(&execstp->map);
				}
				break;
		case 1: biri_Apear(); break;

		case 2: biri_PlayerAttack(); 
				objsound_level(NA_LSE3_SPARK);
				break;

		case 3: biri_Remove(); break;

		case 4: biri_Win(); break;

	}

	ObjMoveEvent();

	biri_counter++;
}

/*==================================================================================================*/
/*						GuruGuru BiriBiri 
/*==================================================================================================*/
#define gurubiri_R			(execstp->s[stw_work0].f)
#define gurubiri_counter	(execstp->s[stw_work1].d)

/***************************************************************************************************
						    Init
***************************************************************************************************/
extern void s_gurubiri_init(void)
{

	obj_attX = obj_worldX;
	obj_attY = obj_worldY;
	obj_attZ = obj_worldZ;

	obj_animecounter = 1;

	switch (obj_programselect) {
		case 0:	gurubiri_R = 200;
				break;
		case 1:	gurubiri_R = 300;
				break;
		case 2:	gurubiri_R = 400;
				break;

		case 3:	break;		// Stop
	}

	obj_angleY = Randomd();
	obj_mode = 2;

}
/***************************************************************************************************
						    stopbiri
***************************************************************************************************/
static void stopbiri_Event(void)
{

	float dx = player_posX - obj_worldX;
	float dy = (player_posY+120) - obj_worldY;
	float dz = player_posZ - obj_worldZ;
	short targetAngleX = atan(sqrtf(dx*dx + dz*dz),-dy);
	
	s_chase_obj_angle(execstp,player1stp,stw_animeangleY,0x1000);
 	obj_animeangleX = s_chase_angle(obj_animeangleX,targetAngleX,0x1000);


	obj_worldY = obj_attY + cos(gurubiri_counter*(0x458))*20; 	

	biri_PlayerTouch();

	gurubiri_counter++;

}
/***************************************************************************************************
						    gurubiri Move
***************************************************************************************************/
static void gurubiri_Move(void)
{
	obj_worldX = obj_attX + sin(obj_angleY)*(gurubiri_R);	
	obj_worldZ = obj_attZ + cos(obj_angleY)*(gurubiri_R);	

	obj_worldY = obj_attY + cos(gurubiri_counter*(0x8b0))*30; 	

	obj_angleY += 0x400;
	obj_animeangleY = 0x4000 + obj_angleY;

	biri_PlayerTouch();

	gurubiri_counter++;
	objsound_level(NA_LSE3_SPARK);


}
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_gurubiri_main(void)
{

	
	switch (obj_mode) {
		case 2:	if (obj_programselect == 3) stopbiri_Event();
				else						gurubiri_Move();
				break;

		case 4:	biri_Win();
				break;
	}


}





/*################*/
#endif
/*################*/





