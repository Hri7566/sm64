/********************************************************************************
	project.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	June 12, 1996
 ********************************************************************************/

#ifndef PROJECT_H
#define	PROJECT_H


/*-------------------------------------------------------------------------------
 *	Switch.
 */

#define	DEBUGSW			0
#define	DEVELOP			0
#define	VERSION_E3		0

#define LOCALE_US 0
#define LOCALE_ZH 1

#define	ENGLISH			0
#define CHINA			1

#define	PRESS			0
#define	FORCE_MPAL		0

#define	mariosound


/********************************************************************************/
/*	Segment definition															*/
/********************************************************************************/


#define	SEGMENT_CODE			0x00
#define	SEGMENT_DYNAMIC			0x01
#define	SEGMENT_BASIC			0x02
#define	SEGMENT_KEEP			0x03
#define	SEGMENT_PLAYER			0x04
#define	SEGMENT_FACE			0x04
#define	SEGMENT_ENEMY1			0x05
#define	SEGMENT_ENEMY2			0x06
#define	SEGMENT_STAGE1			0x07
#define	SEGMENT_ENEMY3			0x08
#define	SEGMENT_TEXTURE			0x09
#define	SEGMENT_BACK			0x0A
#define	SEGMENT_WEATHER			0x0B
#define	SEGMENT_ENEMYDATA1		0x0C
#define	SEGMENT_ENEMYDATA2		0x0D
#define	SEGMENT_STAGEDATA1		0x0E
#define	SEGMENT_ENEMYDATA3		0x0F
#define	SEGMENT_BOOTSEQ			0x10
#define	SEGMENT_MARIOANIM		0x11
#define	SEGMENT_LUIGIANIM		0x12
#define	SEGMENT_PATHDATA		0x13
#define	SEGMENT_DEMODATA		0x14
#define	SEGMENT_GAMEDATA		0x15
#define	SEGMENT_KEEPDATA		0x16
#define	SEGMENT_PLAYDATA		0x17
#define	SEGMENT_CONTDATA		0x18


#define	ADDRESS_CODE			0x00000000
#define	ADDRESS_DYNAMIC			0x01000000
#define	ADDRESS_BASIC			0x02000000
#define	ADDRESS_KEEP			0x03000000
#define	ADDRESS_PLAYER			0x04000000
#define	ADDRESS_FACE			0x04000000
#define	ADDRESS_ENEMY1			0x05000000
#define	ADDRESS_ENEMY2			0x06000000
#define	ADDRESS_STAGE1			0x07000000
#define	ADDRESS_ENEMY3			0x08000000
#define	ADDRESS_TEXTURE			0x09000000
#define	ADDRESS_BACK			0x0A000000
#define	ADDRESS_WEATHER			0x0B000000
#define	ADDRESS_ENEMYDATA1		0x0C000000
#define	ADDRESS_ENEMYDATA2		0x0D000000
#define	ADDRESS_STAGEDATA1		0x0E000000
#define	ADDRESS_ENEMYDATA3		0x0F000000
#define	ADDRESS_BOOTSEQ			0x10000000
#define	ADDRESS_MARIOANIM		0x11000000
#define	ADDRESS_LUIGIANIM		0x12000000
#define	ADDRESS_PATHDATA		0x13000000
#define	ADDRESS_DEMODATA		0x14000000
#define	ADDRESS_GAMEDATA		0x15000000
#define	ADDRESS_KEEPDATA		0x16000000
#define	ADDRESS_PLAYDATA		0x17000000
#define	ADDRESS_CONTDATA		0x18000000


/********************************************************************************/
/*	memory																		*/
/********************************************************************************/

#define	APPL_MEMORY_START	0x80000400
#define	APPL_MEMORY_END		0x801C1000


#define	DEPTHBUF_START		0x80000400
#define	FREEZONE_START		0x8005C000
#define	FREEZONE_END		0x801C1000

#define	DEMOCODE_START		0x8016F000
#define	TEXTBUFF_START		0x801C1000
#define	CPROGBUF_START		0x801CE000
#define	FIFOBUFF_START		0x8022A000		/* 0x80227000	*/
#define	CPROGRAM_START		0x80249000		/* 0x80246000	*/
#define	ULIBCODE_START		0x80378800
#define	CPROGRAM_END		0x8038F800
#define	FRAMEBUF_START		0x8038F800

/********************************************************************************/
/*	buffer size																	*/
/********************************************************************************/

#define	BOOT_STACKSIZE			0x0400
#define	IDLE_STACKSIZE			0x0800
#define	MAIN_STACKSIZE			0x2000

#define	BOOT_STACKSIZE64		(BOOT_STACKSIZE	>> 3)
#define	IDLE_STACKSIZE64		(IDLE_STACKSIZE	>> 3)
#define	MAIN_STACKSIZE64		(MAIN_STACKSIZE	>> 3)
#define	RMON_STACKSIZE64		(RMON_STACKSIZE	>> 3)

#define	DAT_YIELD_SIZE64		(OS_YIELD_DATA_SIZE  >> 3)
#define	AUD_YIELD_SIZE64		(OS_YIELD_AUDIO_SIZE >> 3)

#define	RSP_FIFO_SIZE8			0x1F000
#define	RSP_FIFO_SIZE64			(RSP_FIFO_SIZE8 >> 3)

#define	BACKUP_EEROM_SIZE8		512
#define	BACKUP_EEROM_SIZE64		(BACKUP_EEROM_SIZE8 >> 3)

#define	HEAP_SIZE				0x4000
#define	DYNAMIC_BLKSIZE			6400

#define	MAP_NUMSHAPES			256
#define	MAP_NUMSCENES			8
#define	MAP_NUMLIGHTS			8
#define	MAP_NUMCAMERAS			8


/********************************************************************************/
/*	Geometry render mode.														*/
/********************************************************************************/

#define	NUM_RENDERS			8

#define	RM_BACK				0
#define	RM_SURF				1
#define	RM_DECAL			2
#define	RM_INTER			3
#define	RM_SPRITE			4
#define	RM_XSURF			5
#define	RM_XDECAL			6
#define	RM_XINTER			7

#define	RM_SOFT_SPRITE		RM_SPRITE


/********************************************************************************/
/*	Mario start height.															*/
/********************************************************************************/

#define	START_H				1000


#endif
