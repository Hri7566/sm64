/********************************************************************************
						Ultra 64 MARIO Brothers

					move wing hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 16, 1995
 ********************************************************************************/

extern Gfx RCP_move_wing[];
extern Gfx RCP_move_wing_xlu[];

extern Gfx RCP_move_metal_wing[];
extern Gfx RCP_move_metal_wing_xlu[];


// ============================================================================================	
//
//
//		Move Wing Hierarchy data.
//
//
/*********************************************************************************
    : Move Wing hierarchy [ Opaque ].
**********************************************************************************/
Hierarchy Hms_MoveWing[] = {
	hmsGroup()
	hmsBegin()
		hmsGfx(RM_SPRITE, RCP_move_wing)
	hmsEnd()
	hmsReturn()
};

/*********************************************************************************
    : Move Wing hierarchy [ Trans ].
**********************************************************************************/
Hierarchy Hms_MoveWing_xlu[] = {
	hmsGroup()
	hmsBegin()
		hmsGfx(RM_XSURF, RCP_move_wing_xlu)
	hmsEnd()
	hmsReturn()
};

/*********************************************************************************
    : Move metal wing hierarchy [ Opaque ].
**********************************************************************************/
Hierarchy Hms_MoveMetalWing[] = {
	hmsGroup()
	hmsBegin()
		hmsGfx(RM_SPRITE, RCP_move_metal_wing)
	hmsEnd()
	hmsReturn()
};

/*********************************************************************************
    : Move metal wing hierarchy [ Trans ].
**********************************************************************************/
Hierarchy Hms_MoveMetalWing_xlu[] = {
	hmsGroup()
	hmsBegin()
		hmsGfx(RM_XSURF, RCP_move_metal_wing_xlu)
	hmsEnd()
	hmsReturn()
};
