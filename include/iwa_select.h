/***************************************************************************************************
			Iwamoto Select Scene Program headder file

****************************************************************************************************/
#include "fontdef.h"

extern unsigned char Se_scenePtr[];
extern unsigned short* texDataPtr[];
extern unsigned char s88fontPtr[];
extern unsigned short* s88ColfontPtr[];
extern unsigned char *crsNamePtr[];
extern unsigned char *starnamePtr[];
extern unsigned short* MessTexDataPtr[];
extern unsigned short* camIconPtr[];

extern char select_starNo;			// Select Star No
extern char getStar_flag;			// Get Star Flag for SaveScene & Gauge

extern void iwa_LoadImatrix(void);
extern void iTranslate(char ,float,float,float);
extern void iScale(char ,float,float,float);
extern void iRotate(char ,float,float,float,float);

#if CHINA
extern void DrawMessageTexture(unsigned short);
extern void DrawMessageTexture_zh(unsigned short);
extern void I_itochar_16(int,unsigned char* ); 
#else
extern void DrawMessageTexture(unsigned char);
#endif

extern void MakeSelectProjection(void);
extern void ContCursorEvent(char ,char*,char,char);

extern void Draw8bitFont(short ,short , unsigned char* ); 
extern void Draw16bitFont(char ,short ,short , unsigned char* ); 
extern void DrawS88Font(short ,short , unsigned char* ); 
extern void I_itochar(int,unsigned char* ); 
extern short CharCentering(short,unsigned char*,float);
extern short MessageLength(unsigned char* font);
extern void CannonSightEvent(void);



extern Gfx RCP_mess_font_on[];
extern Gfx RCP_mess_font_main[];
extern Gfx RCP_mess_font_off[];
extern Gfx RCP_tfont2_on[];
extern Gfx RCP_tfont2_main[];
extern Gfx RCP_tfont2_off[];
extern Gfx RCP_s88font_on[];
extern Gfx RCP_s88font_off[];
extern Gfx RCP_se_cursolA[];
extern Gfx RCP_se_cursolB[];

extern Gfx RCP_Mess_window[];
extern Gfx RCP_mess_cursor[];

extern void DrawCoinScore(int,char,char,short,short); 
#define DrawHiScore(player,course,posx,posy)	DrawCoinScore(0,player,course,posx,posy) 
#define DrawMyScore(player,course,posx,posy)	DrawCoinScore(1,player,course,posx,posy) 


/*-----------------------------------*/

extern char ContCommand(short*);
#define IWAMOTO_DBUG 1





