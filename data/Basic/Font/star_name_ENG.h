/**************************************************************************************************
					Message Data 	for ENGLISH															   
								programed by Iwamoto Daiki										   
**************************************************************************************************/

static unsigned char star_name_00[] = { 0x0b, 0x12, 0x10, 0x9e, 0x0b, 0x18, 0x0b, 0x9f, 0x18, 0x16, 0x0b, 0x9e, 0x18, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1c, 0x1e, 0x16, 0x16, 0x12, 0x1d, 0xff,};
static unsigned char star_name_01[] = { 0x0f, 0x18, 0x18, 0x1d, 0x1b, 0x0a, 0x0c, 0x0e, 0x9e, 0x20, 0x12, 0x1d, 0x11, 0x9e, 0x14, 0x18, 0x18, 0x19, 0x0a, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1a, 0x1e, 0x12, 0x0c, 0x14, 0xff,};
static unsigned char star_name_02[] = { 0x1c, 0x11, 0x18, 0x18, 0x1d, 0x9e, 0x1d, 0x18, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x12, 0x1c, 0x15, 0x0a, 0x17, 0x0d, 0x9e, 0x12, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1c, 0x14, 0x22, 0xff,};
static unsigned char star_name_03[] = { 0x0f, 0x12, 0x17, 0x0d, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x08, 0x9e, 0x1b, 0x0e, 0x0d, 0x9e, 0x0c, 0x18, 0x12, 0x17, 0x1c, 0xff,};
static unsigned char star_name_04[] = { 0x16, 0x0a, 0x1b, 0x12, 0x18, 0x9e, 0x20, 0x12, 0x17, 0x10, 0x1c, 0x9e, 0x1d, 0x18, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1c, 0x14, 0x22, 0xff,};
static unsigned char star_name_05[] = { 0x0b, 0x0e, 0x11, 0x12, 0x17, 0x0d, 0x9e, 0x0c, 0x11, 0x0a, 0x12, 0x17, 0x9e, 0x0c, 0x11, 0x18, 0x16, 0x19, 0x3e, 0x1c, 0x9e, 0x10, 0x0a, 0x1d, 0x0e, 0xff,};
static unsigned char star_name_06[] = { 0x0c, 0x11, 0x12, 0x19, 0x9e, 0x18, 0x0f, 0x0f, 0x9e, 0x20, 0x11, 0x18, 0x16, 0x19, 0x3e, 0x1c, 0x9e, 0x0b, 0x15, 0x18, 0x0c, 0x14, 0xff,};
static unsigned char star_name_07[] = { 0x1d, 0x18, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1d, 0x18, 0x19, 0x9e, 0x18, 0x0f, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0f, 0x18, 0x1b, 0x1d, 0x1b, 0x0e, 0x1c, 0x1c, 0xff,};
static unsigned char star_name_08[] = { 0x1c, 0x11, 0x18, 0x18, 0x1d, 0x9e, 0x12, 0x17, 0x1d, 0x18, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x20, 0x12, 0x15, 0x0d, 0x9e, 0x0b, 0x15, 0x1e, 0x0e, 0xff,};
static unsigned char star_name_09[] = { 0x1b, 0x0e, 0x0d, 0x9e, 0x0c, 0x18, 0x12, 0x17, 0x1c, 0x9e, 0x18, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0f, 0x15, 0x18, 0x0a, 0x1d, 0x12, 0x17, 0x10, 0x9e, 0x12, 0x1c, 0x15, 0x0e, 0xff,};
static unsigned char star_name_10[] = { 0x0f, 0x0a, 0x15, 0x15, 0x9e, 0x18, 0x17, 0x1d, 0x18, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0c, 0x0a, 0x10, 0x0e, 0x0d, 0x9e, 0x12, 0x1c, 0x15, 0x0a, 0x17, 0x0d, 0xff,};
static unsigned char star_name_11[] = { 0x0b, 0x15, 0x0a, 0x1c, 0x1d, 0x9e, 0x0a, 0x20, 0x0a, 0x22, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x20, 0x0a, 0x15, 0x15, 0xff,};
static unsigned char star_name_12[] = { 0x19, 0x15, 0x1e, 0x17, 0x0d, 0x0e, 0x1b, 0x9e, 0x12, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1c, 0x1e, 0x17, 0x14, 0x0e, 0x17, 0x9e, 0x1c, 0x11, 0x12, 0x19, 0xff,};
static unsigned char star_name_13[] = { 0x0c, 0x0a, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0e, 0x0e, 0x15, 0x9e, 0x0c, 0x18, 0x16, 0x0e, 0x9e, 0x18, 0x1e, 0x1d, 0x9e, 0x1d, 0x18, 0x9e, 0x19, 0x15, 0x0a, 0x22, 0xf4, 0xff,};
static unsigned char star_name_14[] = { 0x1d, 0x1b, 0x0e, 0x0a, 0x1c, 0x1e, 0x1b, 0x0e, 0x9e, 0x18, 0x0f, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x18, 0x0c, 0x0e, 0x0a, 0x17, 0x9e, 0x0c, 0x0a, 0x1f, 0x0e, 0xff,};
static unsigned char star_name_15[] = { 0x1b, 0x0e, 0x0d, 0x9e, 0x0c, 0x18, 0x12, 0x17, 0x1c, 0x9e, 0x18, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1c, 0x11, 0x12, 0x19, 0x9e, 0x0a, 0x0f, 0x15, 0x18, 0x0a, 0x1d, 0xff,};
static unsigned char star_name_16[] = { 0x0b, 0x15, 0x0a, 0x1c, 0x1d, 0x9e, 0x1d, 0x18, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1c, 0x1d, 0x18, 0x17, 0x0e, 0x9e, 0x19, 0x12, 0x15, 0x15, 0x0a, 0x1b, 0xff,};
static unsigned char star_name_17[] = { 0x1d, 0x11, 0x1b, 0x18, 0x1e, 0x10, 0x11, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x13, 0x0e, 0x1d, 0x9e, 0x1c, 0x1d, 0x1b, 0x0e, 0x0a, 0x16, 0xff,};
static unsigned char star_name_18[] = { 0x1c, 0x15, 0x12, 0x19, 0x9e, 0x1c, 0x15, 0x12, 0x0d, 0x12, 0x17, 0x3e, 0x9e, 0x0a, 0x20, 0x0a, 0x22, 0xff,};
static unsigned char star_name_19[] = { 0x15, 0x12, 0x3e, 0x15, 0x9e, 0x19, 0x0e, 0x17, 0x10, 0x1e, 0x12, 0x17, 0x9e, 0x15, 0x18, 0x1c, 0x1d, 0xff,};
static unsigned char star_name_20[] = { 0x0b, 0x12, 0x10, 0x9e, 0x19, 0x0e, 0x17, 0x10, 0x1e, 0x12, 0x17, 0x9e, 0x1b, 0x0a, 0x0c, 0x0e, 0xff,};
static unsigned char star_name_21[] = { 0x0f, 0x1b, 0x18, 0x1c, 0x1d, 0x22, 0x9e, 0x1c, 0x15, 0x12, 0x0d, 0x0e, 0x9e, 0x0f, 0x18, 0x1b, 0x9e, 0x08, 0x9e, 0x1b, 0x0e, 0x0d, 0x9e, 0x0c, 0x18, 0x12, 0x17, 0x1c, 0xff,};
static unsigned char star_name_22[] = { 0x1c, 0x17, 0x18, 0x20, 0x16, 0x0a, 0x17, 0x3e, 0x1c, 0x9e, 0x15, 0x18, 0x1c, 0x1d, 0x9e, 0x11, 0x12, 0x1c, 0x9e, 0x11, 0x0e, 0x0a, 0x0d, 0xff,};
static unsigned char star_name_23[] = { 0x20, 0x0a, 0x15, 0x15, 0x9e, 0x14, 0x12, 0x0c, 0x14, 0x1c, 0x9e, 0x20, 0x12, 0x15, 0x15, 0x9e, 0x20, 0x18, 0x1b, 0x14, 0xff,};
static unsigned char star_name_24[] = { 0x10, 0x18, 0x9e, 0x18, 0x17, 0x9e, 0x0a, 0x9e, 0x10, 0x11, 0x18, 0x1c, 0x1d, 0x9e, 0x11, 0x1e, 0x17, 0x1d, 0xff,};
static unsigned char star_name_25[] = { 0x1b, 0x12, 0x0d, 0x0e, 0x9e, 0x0b, 0x12, 0x10, 0x9e, 0x0b, 0x18, 0x18, 0x3e, 0x1c, 0x9e, 0x16, 0x0e, 0x1b, 0x1b, 0x22, 0x9f, 0x10, 0x18, 0x9f, 0x1b, 0x18, 0x1e, 0x17, 0x0d, 0xff,};
static unsigned char star_name_26[] = { 0x1c, 0x0e, 0x0c, 0x1b, 0x0e, 0x1d, 0x9e, 0x18, 0x0f, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x11, 0x0a, 0x1e, 0x17, 0x1d, 0x0e, 0x0d, 0x9e, 0x0b, 0x18, 0x18, 0x14, 0x1c, 0xff,};
static unsigned char star_name_27[] = { 0x1c, 0x0e, 0x0e, 0x14, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x08, 0x9e, 0x1b, 0x0e, 0x0d, 0x9e, 0x0c, 0x18, 0x12, 0x17, 0x1c, 0xff,};
static unsigned char star_name_28[] = { 0x0b, 0x12, 0x10, 0x9e, 0x0b, 0x18, 0x18, 0x3e, 0x1c, 0x9e, 0x0b, 0x0a, 0x15, 0x0c, 0x18, 0x17, 0x22, 0xff,};
static unsigned char star_name_29[] = { 0x0e, 0x22, 0x0e, 0x9e, 0x1d, 0x18, 0x9e, 0x0e, 0x22, 0x0e, 0x9e, 0x12, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1c, 0x0e, 0x0c, 0x1b, 0x0e, 0x1d, 0x9e, 0x1b, 0x18, 0x18, 0x16, 0xff,};
static unsigned char star_name_30[] = { 0x1c, 0x20, 0x12, 0x16, 0x16, 0x12, 0x17, 0x10, 0x9e, 0x0b, 0x0e, 0x0a, 0x1c, 0x1d, 0x9e, 0x12, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0c, 0x0a, 0x1f, 0x0e, 0x1b, 0x17, 0xff,};
static unsigned char star_name_31[] = { 0x0e, 0x15, 0x0e, 0x1f, 0x0a, 0x1d, 0x0e, 0x9e, 0x0f, 0x18, 0x1b, 0x9e, 0x08, 0x9e, 0x1b, 0x0e, 0x0d, 0x9e, 0x0c, 0x18, 0x12, 0x17, 0x1c, 0xff,};
static unsigned char star_name_32[] = { 0x16, 0x0e, 0x1d, 0x0a, 0x15, 0x9f, 0x11, 0x0e, 0x0a, 0x0d, 0x9e, 0x16, 0x0a, 0x1b, 0x12, 0x18, 0x9e, 0x0c, 0x0a, 0x17, 0x9e, 0x16, 0x18, 0x1f, 0x0e, 0xf2, 0xff,};
static unsigned char star_name_33[] = { 0x17, 0x0a, 0x1f, 0x12, 0x10, 0x0a, 0x1d, 0x12, 0x17, 0x10, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1d, 0x18, 0x21, 0x12, 0x0c, 0x9e, 0x16, 0x0a, 0x23, 0x0e, 0xff,};
static unsigned char star_name_34[] = { 0x0a, 0x9f, 0x16, 0x0a, 0x23, 0x0e, 0x9f, 0x12, 0x17, 0x10, 0x9e, 0x0e, 0x16, 0x0e, 0x1b, 0x10, 0x0e, 0x17, 0x0c, 0x22, 0x9e, 0x0e, 0x21, 0x12, 0x1d, 0xff,};
static unsigned char star_name_35[] = { 0x20, 0x0a, 0x1d, 0x0c, 0x11, 0x9e, 0x0f, 0x18, 0x1b, 0x9e, 0x1b, 0x18, 0x15, 0x15, 0x12, 0x17, 0x10, 0x9e, 0x1b, 0x18, 0x0c, 0x14, 0x1c, 0xff,};
static unsigned char star_name_36[] = { 0x0b, 0x18, 0x12, 0x15, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0b, 0x12, 0x10, 0x9e, 0x0b, 0x1e, 0x15, 0x15, 0x22, 0xff,};
static unsigned char star_name_37[] = { 0x0b, 0x1e, 0x15, 0x15, 0x22, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0b, 0x1e, 0x15, 0x15, 0x12, 0x0e, 0x1c, 0xff,};
static unsigned char star_name_38[] = { 0x08, 0x9f, 0x0c, 0x18, 0x12, 0x17, 0x9e, 0x19, 0x1e, 0x23, 0x23, 0x15, 0x0e, 0x9e, 0x20, 0x12, 0x1d, 0x11, 0x9e, 0x01, 0x05, 0x9e, 0x19, 0x12, 0x0e, 0x0c, 0x0e, 0x1c, 0xff,};
static unsigned char star_name_39[] = { 0x1b, 0x0e, 0x0d, 0x9f, 0x11, 0x18, 0x1d, 0x9e, 0x15, 0x18, 0x10, 0x9e, 0x1b, 0x18, 0x15, 0x15, 0x12, 0x17, 0x10, 0xff,};
static unsigned char star_name_40[] = { 0x11, 0x18, 0x1d, 0x9f, 0x0f, 0x18, 0x18, 0x1d, 0x9f, 0x12, 0x1d, 0x9e, 0x12, 0x17, 0x1d, 0x18, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1f, 0x18, 0x15, 0x0c, 0x0a, 0x17, 0x18, 0xff,};
static unsigned char star_name_41[] = { 0x0e, 0x15, 0x0e, 0x1f, 0x0a, 0x1d, 0x18, 0x1b, 0x9e, 0x1d, 0x18, 0x1e, 0x1b, 0x9e, 0x12, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1f, 0x18, 0x15, 0x0c, 0x0a, 0x17, 0x18, 0xff,};
static unsigned char star_name_42[] = { 0x12, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1d, 0x0a, 0x15, 0x18, 0x17, 0x1c, 0x9e, 0x18, 0x0f, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0b, 0x12, 0x10, 0x9e, 0x0b, 0x12, 0x1b, 0x0d, 0xff,};
static unsigned char star_name_43[] = { 0x1c, 0x11, 0x12, 0x17, 0x12, 0x17, 0x10, 0x9e, 0x0a, 0x1d, 0x18, 0x19, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x19, 0x22, 0x1b, 0x0a, 0x16, 0x12, 0x0d, 0xff,};
static unsigned char star_name_44[] = { 0x12, 0x17, 0x1c, 0x12, 0x0d, 0x0e, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0a, 0x17, 0x0c, 0x12, 0x0e, 0x17, 0x1d, 0x9e, 0x19, 0x22, 0x1b, 0x0a, 0x16, 0x12, 0x0d, 0xff,};
static unsigned char star_name_45[] = { 0x1c, 0x1d, 0x0a, 0x17, 0x0d, 0x9e, 0x1d, 0x0a, 0x15, 0x15, 0x9e, 0x18, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0f, 0x18, 0x1e, 0x1b, 0x9e, 0x19, 0x12, 0x15, 0x15, 0x0a, 0x1b, 0x1c, 0xff,};
static unsigned char star_name_46[] = { 0x0f, 0x1b, 0x0e, 0x0e, 0x9e, 0x0f, 0x15, 0x22, 0x12, 0x17, 0x10, 0x9e, 0x0f, 0x18, 0x1b, 0x9e, 0x08, 0x9e, 0x1b, 0x0e, 0x0d, 0x9e, 0x0c, 0x18, 0x12, 0x17, 0x1c, 0xff,};
static unsigned char star_name_47[] = { 0x19, 0x22, 0x1b, 0x0a, 0x16, 0x12, 0x0d, 0x9e, 0x19, 0x1e, 0x23, 0x23, 0x15, 0x0e, 0xff,};
static unsigned char star_name_48[] = { 0x0b, 0x18, 0x0a, 0x1b, 0x0d, 0x9e, 0x0b, 0x18, 0x20, 0x1c, 0x0e, 0x1b, 0x3e, 0x1c, 0x9e, 0x1c, 0x1e, 0x0b, 0xff,};
static unsigned char star_name_49[] = { 0x0c, 0x11, 0x0e, 0x1c, 0x1d, 0x1c, 0x9e, 0x12, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0c, 0x1e, 0x1b, 0x1b, 0x0e, 0x17, 0x1d, 0xff,};
static unsigned char star_name_50[] = { 0x19, 0x18, 0x15, 0x0e, 0x9f, 0x13, 0x1e, 0x16, 0x19, 0x12, 0x17, 0x10, 0x9e, 0x0f, 0x18, 0x1b, 0x9e, 0x1b, 0x0e, 0x0d, 0x9e, 0x0c, 0x18, 0x12, 0x17, 0x1c, 0xff,};
static unsigned char star_name_51[] = { 0x1d, 0x11, 0x1b, 0x18, 0x1e, 0x10, 0x11, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x13, 0x0e, 0x1d, 0x9e, 0x1c, 0x1d, 0x1b, 0x0e, 0x0a, 0x16, 0xff,};
static unsigned char star_name_52[] = { 0x1d, 0x11, 0x0e, 0x9e, 0x16, 0x0a, 0x17, 0x1d, 0x0a, 0x9e, 0x1b, 0x0a, 0x22, 0x3e, 0x1c, 0x9e, 0x1b, 0x0e, 0x20, 0x0a, 0x1b, 0x0d, 0xff,};
static unsigned char star_name_53[] = { 0x0c, 0x18, 0x15, 0x15, 0x0e, 0x0c, 0x1d, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0c, 0x0a, 0x19, 0x1c, 0x3f, 0x3f, 0x3f, 0xff,};
static unsigned char star_name_54[] = { 0x1c, 0x17, 0x18, 0x20, 0x16, 0x0a, 0x17, 0x3e, 0x1c, 0x9e, 0x0b, 0x12, 0x10, 0x9e, 0x11, 0x0e, 0x0a, 0x0d, 0xff,};
static unsigned char star_name_55[] = { 0x0c, 0x11, 0x12, 0x15, 0x15, 0x9e, 0x20, 0x12, 0x1d, 0x11, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0b, 0x1e, 0x15, 0x15, 0x22, 0xff,};
static unsigned char star_name_56[] = { 0x12, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0d, 0x0e, 0x0e, 0x19, 0x9e, 0x0f, 0x1b, 0x0e, 0x0e, 0x23, 0x0e, 0xff,};
static unsigned char star_name_57[] = { 0x20, 0x11, 0x12, 0x1b, 0x15, 0x9e, 0x0f, 0x1b, 0x18, 0x16, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0f, 0x1b, 0x0e, 0x0e, 0x23, 0x12, 0x17, 0x10, 0x9e, 0x19, 0x18, 0x17, 0x0d, 0xff,};
static unsigned char star_name_58[] = { 0x1c, 0x11, 0x0e, 0x15, 0x15, 0x9e, 0x1c, 0x11, 0x1b, 0x0e, 0x0d, 0x0d, 0x12, 0x17, 0x3e, 0x9e, 0x0f, 0x18, 0x1b, 0x9e, 0x1b, 0x0e, 0x0d, 0x9e, 0x0c, 0x18, 0x12, 0x17, 0x1c, 0xff,};
static unsigned char star_name_59[] = { 0x12, 0x17, 0x1d, 0x18, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x12, 0x10, 0x15, 0x18, 0x18, 0xff,};
static unsigned char star_name_60[] = { 0x1c, 0x11, 0x18, 0x0c, 0x14, 0x12, 0x17, 0x10, 0x9e, 0x0a, 0x1b, 0x1b, 0x18, 0x20, 0x9e, 0x15, 0x12, 0x0f, 0x1d, 0x1c, 0xf2, 0xff,};
static unsigned char star_name_61[] = { 0x1d, 0x18, 0x19, 0x9e, 0x18, 0x3e, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1d, 0x18, 0x20, 0x17, 0xff,};
static unsigned char star_name_62[] = { 0x1c, 0x0e, 0x0c, 0x1b, 0x0e, 0x1d, 0x1c, 0x9e, 0x12, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1c, 0x11, 0x0a, 0x15, 0x15, 0x18, 0x20, 0x1c, 0x9e, 0xe5, 0x9e, 0x1c, 0x14, 0x22, 0xff,};
static unsigned char star_name_63[] = { 0x0e, 0x21, 0x19, 0x1b, 0x0e, 0x1c, 0x1c, 0x9e, 0x0e, 0x15, 0x0e, 0x1f, 0x0a, 0x1d, 0x18, 0x1b, 0x9f, 0x9f, 0x11, 0x1e, 0x1b, 0x1b, 0x22, 0x9e, 0x1e, 0x19, 0xf2, 0xff,};
static unsigned char star_name_64[] = { 0x10, 0x18, 0x9e, 0x1d, 0x18, 0x9e, 0x1d, 0x18, 0x20, 0x17, 0x9e, 0x0f, 0x18, 0x1b, 0x9e, 0x1b, 0x0e, 0x0d, 0x9e, 0x0c, 0x18, 0x12, 0x17, 0x1c, 0xff,};
static unsigned char star_name_65[] = { 0x1a, 0x1e, 0x12, 0x0c, 0x14, 0x9e, 0x1b, 0x0a, 0x0c, 0x0e, 0x9e, 0x1d, 0x11, 0x1b, 0x18, 0x1e, 0x10, 0x11, 0x9e, 0x0d, 0x18, 0x20, 0x17, 0x1d, 0x18, 0x20, 0x17, 0xf2, 0xff,};
static unsigned char star_name_66[] = { 0x1c, 0x0c, 0x0a, 0x15, 0x0e, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x16, 0x18, 0x1e, 0x17, 0x1d, 0x0a, 0x12, 0x17, 0xff,};
static unsigned char star_name_67[] = { 0x16, 0x22, 0x1c, 0x1d, 0x0e, 0x1b, 0x22, 0x9e, 0x18, 0x0f, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x16, 0x18, 0x17, 0x14, 0x0e, 0x22, 0x9e, 0x0c, 0x0a, 0x10, 0x0e, 0xff,};
static unsigned char star_name_68[] = { 0x1c, 0x0c, 0x0a, 0x1b, 0x22, 0x9e, 0x3e, 0x1c, 0x11, 0x1b, 0x18, 0x18, 0x16, 0x1c, 0x6f, 0x9e, 0x1b, 0x0e, 0x0d, 0x9e, 0x0c, 0x18, 0x12, 0x17, 0x1c, 0xff,};
static unsigned char star_name_69[] = { 0x16, 0x22, 0x1c, 0x1d, 0x0e, 0x1b, 0x12, 0x18, 0x1e, 0x1c, 0x9e, 0x16, 0x18, 0x1e, 0x17, 0x1d, 0x0a, 0x12, 0x17, 0x1c, 0x12, 0x0d, 0x0e, 0xff,};
static unsigned char star_name_70[] = { 0x0b, 0x1b, 0x0e, 0x0a, 0x1d, 0x11, 0x1d, 0x0a, 0x14, 0x12, 0x17, 0x10, 0x9e, 0x1f, 0x12, 0x0e, 0x20, 0x9e, 0x0f, 0x1b, 0x18, 0x16, 0x9e, 0x0b, 0x1b, 0x12, 0x0d, 0x10, 0x0e, 0xff,};
static unsigned char star_name_71[] = { 0x0b, 0x15, 0x0a, 0x1c, 0x1d, 0x9e, 0x1d, 0x18, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x15, 0x18, 0x17, 0x0e, 0x15, 0x22, 0x9e, 0x16, 0x1e, 0x1c, 0x11, 0x1b, 0x18, 0x18, 0x16, 0xff,};
static unsigned char star_name_72[] = { 0x19, 0x15, 0x1e, 0x0c, 0x14, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x19, 0x12, 0x1b, 0x0a, 0x17, 0x11, 0x0a, 0x9e, 0x0f, 0x15, 0x18, 0x20, 0x0e, 0x1b, 0xff,};
static unsigned char star_name_73[] = { 0x1d, 0x11, 0x0e, 0x9e, 0x1d, 0x12, 0x19, 0x9e, 0x1d, 0x18, 0x19, 0x9e, 0x18, 0x0f, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x11, 0x1e, 0x10, 0x0e, 0x9e, 0x12, 0x1c, 0x15, 0x0a, 0x17, 0x0d, 0xff,};
static unsigned char star_name_74[] = { 0x1b, 0x0e, 0x16, 0x0a, 0x1d, 0x0c, 0x11, 0x9e, 0x20, 0x12, 0x1d, 0x11, 0x9e, 0x14, 0x18, 0x18, 0x19, 0x0a, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1a, 0x1e, 0x12, 0x0c, 0x14, 0xff,};
static unsigned char star_name_75[] = { 0x0f, 0x12, 0x1f, 0x0e, 0x9e, 0x12, 0x1d, 0x1d, 0x22, 0x9e, 0x0b, 0x12, 0x1d, 0x1d, 0x22, 0x9e, 0x1c, 0x0e, 0x0c, 0x1b, 0x0e, 0x1d, 0x1c, 0xff,};
static unsigned char star_name_76[] = { 0x20, 0x12, 0x10, 0x10, 0x15, 0x0e, 0x1b, 0x3e, 0x1c, 0x9e, 0x1b, 0x0e, 0x0d, 0x9e, 0x0c, 0x18, 0x12, 0x17, 0x1c, 0xff,};
static unsigned char star_name_77[] = { 0x16, 0x0a, 0x14, 0x0e, 0x9e, 0x20, 0x12, 0x10, 0x10, 0x15, 0x0e, 0x1b, 0x9e, 0x1c, 0x1a, 0x1e, 0x12, 0x1b, 0x16, 0xff,};
static unsigned char star_name_78[] = { 0x1b, 0x18, 0x15, 0x15, 0x9e, 0x12, 0x17, 0x1d, 0x18, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0c, 0x0a, 0x10, 0x0e, 0xff,};
static unsigned char star_name_79[] = { 0x1d, 0x11, 0x0e, 0x9e, 0x19, 0x12, 0x1d, 0x9e, 0x0a, 0x17, 0x0d, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x19, 0x0e, 0x17, 0x0d, 0x1e, 0x15, 0x1e, 0x16, 0x1c, 0xff,};
static unsigned char star_name_80[] = { 0x10, 0x0e, 0x1d, 0x9e, 0x0a, 0x9e, 0x11, 0x0a, 0x17, 0x0d, 0xff,};
static unsigned char star_name_81[] = { 0x1c, 0x1d, 0x18, 0x16, 0x19, 0x9e, 0x18, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1d, 0x11, 0x20, 0x18, 0x16, 0x19, 0xff,};
static unsigned char star_name_82[] = { 0x1d, 0x12, 0x16, 0x0e, 0x0d, 0x9e, 0x13, 0x1e, 0x16, 0x19, 0x1c, 0x9e, 0x18, 0x17, 0x9e, 0x16, 0x18, 0x1f, 0x12, 0x17, 0x10, 0x9e, 0x0b, 0x0a, 0x1b, 0x1c, 0xff,};
static unsigned char star_name_83[] = { 0x1c, 0x1d, 0x18, 0x19, 0x9e, 0x1d, 0x12, 0x16, 0x0e, 0x9e, 0x0f, 0x18, 0x1b, 0x9e, 0x1b, 0x0e, 0x0d, 0x9e, 0x0c, 0x18, 0x12, 0x17, 0x1c, 0xff,};
static unsigned char star_name_84[] = { 0x0c, 0x1b, 0x1e, 0x12, 0x1c, 0x0e, 0x1b, 0x9e, 0x0c, 0x1b, 0x18, 0x1c, 0x1c, 0x12, 0x17, 0x10, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1b, 0x0a, 0x12, 0x17, 0x0b, 0x18, 0x20, 0xff,};
static unsigned char star_name_85[] = { 0x1d, 0x11, 0x0e, 0x9e, 0x0b, 0x12, 0x10, 0x9e, 0x11, 0x18, 0x1e, 0x1c, 0x0e, 0x9e, 0x12, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1c, 0x14, 0x22, 0xff,};
static unsigned char star_name_86[] = { 0x0c, 0x18, 0x12, 0x17, 0x1c, 0x9e, 0x0a, 0x16, 0x0a, 0x1c, 0x1c, 0x0e, 0x0d, 0x9e, 0x12, 0x17, 0x9e, 0x0a, 0x9e, 0x16, 0x0a, 0x23, 0x0e, 0xff,};
static unsigned char star_name_87[] = { 0x1c, 0x20, 0x12, 0x17, 0x10, 0x12, 0x17, 0x3e, 0x9e, 0x12, 0x17, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0b, 0x1b, 0x0e, 0x0e, 0x23, 0x0e, 0xff,};
static unsigned char star_name_88[] = { 0x1d, 0x1b, 0x12, 0x0c, 0x14, 0x22, 0x9e, 0x1d, 0x1b, 0x12, 0x0a, 0x17, 0x10, 0x15, 0x0e, 0x1c, 0xf2, 0xff,};
static unsigned char star_name_89[] = { 0x1c, 0x18, 0x16, 0x0e, 0x20, 0x11, 0x0e, 0x1b, 0x0e, 0x9e, 0x18, 0x1f, 0x0e, 0x1b, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x1b, 0x0a, 0x12, 0x17, 0x0b, 0x18, 0x20, 0xff,};
static unsigned char star_name_90[] = { 0x18, 0x17, 0x0e, 0x9e, 0x18, 0x0f, 0x9e, 0x1d, 0x11, 0x0e, 0x9e, 0x0c, 0x0a, 0x1c, 0x1d, 0x15, 0x0e, 0x3e, 0x1c, 0x9e, 0x1c, 0x0e, 0x0c, 0x1b, 0x0e, 0x1d, 0x9e, 0x1c, 0x1d, 0x0a, 0x1b, 0x1c, 0xf2, 0xff,};
static unsigned char star_name_91[] = { 0xff,};
static unsigned char star_name_92[] = { 0xff,};
static unsigned char star_name_93[] = { 0xff,};
static unsigned char star_name_94[] = { 0xff,};
static unsigned char star_name_95[] = { 0xff,};
static unsigned char star_name_96[] = { 0xff,};


unsigned char *starnamePtr[] = {star_name_00,star_name_01,star_name_02,star_name_03,star_name_04,star_name_05,star_name_06,star_name_07,star_name_08,star_name_09,star_name_10,star_name_11,star_name_12,star_name_13,star_name_14,star_name_15,
								star_name_16,star_name_17,star_name_18,star_name_19,star_name_20,star_name_21,star_name_22,star_name_23,star_name_24,star_name_25,star_name_26,star_name_27,star_name_28,star_name_29,star_name_30,star_name_31,
								star_name_32,star_name_33,star_name_34,star_name_35,star_name_36,star_name_37,star_name_38,star_name_39,star_name_40,star_name_41,star_name_42,star_name_43,star_name_44,star_name_45,star_name_46,star_name_47,
								star_name_48,star_name_49,star_name_50,star_name_51,star_name_52,star_name_53,star_name_54,star_name_55,star_name_56,star_name_57,star_name_58,star_name_59,star_name_60,star_name_61,star_name_62,star_name_63,
								star_name_64,star_name_65,star_name_66,star_name_67,star_name_68,star_name_69,star_name_70,star_name_71,star_name_72,star_name_73,star_name_74,star_name_75,star_name_76,star_name_77,star_name_78,star_name_79,
								star_name_80,star_name_81,star_name_82,star_name_83,star_name_84,star_name_85,star_name_86,star_name_87,star_name_88,star_name_89,star_name_90,star_name_91,star_name_92,star_name_93,star_name_94,star_name_95,
								star_name_96,NULL};

