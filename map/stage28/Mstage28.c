/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage28 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

#define		STAGE28_CLEAR_R		0
#define		STAGE28_CLEAR_G		0
#define		STAGE28_CLEAR_B		0

extern Gfx gfx_taki[]		;
extern Gfx gfx_taki_metal1[];
extern Gfx gfx_taki_kage[]	;

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage28Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(STAGE28_CLEAR_R, STAGE28_CLEAR_G, STAGE28_CLEAR_B, 1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON_O,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF , gfx_taki)
					hmsGfx(RM_SURF , gfx_taki_metal1)
					hmsCProg(0, 	 WaterInit)
					hmsCProg(0x2801, WaterFall)
					hmsGfx(RM_XSURF, gfx_taki_kage)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
