/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 8 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage08


/* ================================================================================
		: 8 - 1 [ Sabaku ] Move BGs sequence.
===================================================================================	*/
SEQ_Stage0801_MoveBGs:

		seqActor(S_movebg05,  -2047,1536,-1023,  0,0,0,  0,0,0,  e_sb_pytop)

		seqReturn


/* ================================================================================
		: 8 - 1 [ Sabaku ] Enemies sequence.
===================================================================================	*/
SEQ_Stage0801_Enemies:

		seqActor(S_onimasu,  -3327+2043, 0, -6395+500 ,  0,0,0,   0,0,0,   e_onimasu)
		seqActor(S_onimasu,  -3327+4610, 0, -6395+1530,  0,0,0,   0,1,0,   e_onimasu)
		seqActor(S_onimasu,  -3327+8200, 0, -6395+3060,  0,0,0,   0,2,0,   e_onimasu)
		seqActor(S_tornedo,  -3600,   -200,  2940,       0,0,0,   0,18,0,  e_tornedo)

		seqLevelActor(0x0038,S_tornedo,   1017,   -200,  3832,       0,0,0,   0,25,0,  e_tornedo)
		seqLevelActor(0x0038,S_tornedo,   3066,   -200,   400,       0,0,0,   0,25,0,  e_tornedo)

		seqLevelActor(0x0001,S_hagetaka,   2200,1174,-2820,  0,0,0,   0,1,0,   e_condor)
		seqLevelActor(0x003e,S_hagetaka,  -5963, 573,-4784,  0,0,0,   0,0,0,   e_condor)

		seqReturn


/* ================================================================================
		: 8 - 1 [ Sabaku ] Stars sequence.
===================================================================================	*/
SEQ_Stage0801_Stars:

/*		seqLevelActor(0x003e,S_polystar, -5580, 1380,-2557,  0,0,0,  0,0,0,  e_tripstar)			/*	hagetaka		*/
		seqLevelActor(0x003f,S_polystar, -2050, 1200, -580,  0,0,0,  1,0,0,  e_tripstar) 			/*	top_the_pyramid	*/			
		seqLevelActor(0x003f,S_NULL    ,  6000,  800, 3500,  0,0,0,  4,0,0,  e_tripstar_getcoins)	/*	8_coin 			*/

		seqReturn


/* ================================================================================
		: 8 - 2 [ Pyramid ] Enemies sequence.
===================================================================================	*/
SEQ_Stage0802_Enemies:

		seqActor(S_NULL,       2867, 640, 2867,  0,  0,0,  0,77,0,  e_bar			)
		seqActor(S_NULL,          0,3200, 1331,  0,  0,0,  0,92,0,  e_bar			)
		seqActor(S_movebg01,   3297,   0,   95,  0,  0,0,  0,28,0,  e_sabaku_dosun	)
		seqActor(S_movebg01,   -870,3840,  105,  0,180,0,  0, 0,0,  e_jiro			)
		seqActor(S_movebg01,  -3362,   0,-1385,  0,  0,0,  0, 0,0,  e_jiro			)
		seqActor(S_movebg02,  -2458,2109,-1430,  0,  0,0,  0, 0,0,  e_py_pencil		)
		seqActor(S_movebg03,    858,1927,-2307,  0,  0,0,  0, 0,0,  e_py_wall		)
		seqActor(S_movebg03,    730,1927,-2307,  0,  0,0,  0, 1,0,  e_py_wall		)
		seqActor(S_movebg03,   1473,2567,-2307,  0,  0,0,  0, 1,0,  e_py_wall		)
		seqActor(S_movebg03,   1345,2567,-2307,  0,  0,0,  0, 2,0,  e_py_wall		)
		seqActor(S_movebg04,      0,4966,  256,  0,  0,0,  0, 0,0,  e_py_gondola	)
		seqActor(S_NULL, 	   1198,-133, 2396,  0,	 0,0,  0, 0,0,  e_sandfall		)
		seqActor(S_NULL,          7,1229, -708,  0,	 0,0,  0, 0,0,  e_sandfall		)
		seqActor(S_NULL,          7,4317, -708,  0,	 0,0,  0, 0,0,  e_sandfall		)

		seqReturn


/* ================================================================================
		: 8 - 2 [ Pyramid ] Stars sequence.
===================================================================================	*/
SEQ_Stage0802_Stars:

		seqLevelActor(0x003f,S_polystar, 500, 5050, -500,  0,0,0,  2,0,0,  e_tripstar)			/* top in the py */
		seqLevelActor(0x003f,S_NULL,     900, 1400, 2350,  0,0,0,  5,4,0,  e_tripstar_getdummy) /* secret		 */

		seqReturn


/* ================================================================================
		: 8 - 3 [ Pyramid B1 ] Enemies sequence.
===================================================================================	*/
SEQ_Stage0803_Enemies:

		seqActor(S_NULL,  0,-1534,-3693,   0,0,0,  3,0,0,  e_handman)

		seqReturn


/* ================================================================================
		: Stage 8 main sequence.
===================================================================================	*/
SEQ_DoStage08:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage8SegmentRomStart	  , _GfxStage8SegmentRomEnd		)
	seqLoadPres(SEGMENT_BACK	  , _BackSabakuSegmentRomStart	  , _BackSabakuSegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE	  , _CFieldTextureSegmentRomStart , _CFieldTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_eSegmentRomStart	  , _GfxEnemy1_eSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_eSegmentRomStart	  , _HmsEnemy1_eSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_e)

		seqHmsShape(S_tree5, RCP_HmsTree05)

		seqHmsShape(S_bg01, RCP_HmsSunaBG01)
		seqHmsShape(S_bg02, RCP_HmsSunaBG02)

		seqHmsShape(S_movebg05, RCP_HmsPyTop  )

		seqHmsShape(S_movebg01, RCP_HmsPyObj01)
		seqHmsShape(S_movebg02, RCP_HmsPyObj02)
		seqHmsShape(S_movebg03, RCP_HmsPyObj03)
		seqHmsShape(S_movebg04, RCP_HmsPyObj04)

		seqHmsShape(S_onimasu, RCP_onimasu)

		seqBeginScene(1, RCP_Stage8Scene1)
			seqActor(S_NULL,    653,38+START_H, 6566,  0, 90,0,   0,10,0,  e_player_entpict)
			seqActor(S_NULL,  -2048,  		 0,   56,  0,  0,0,   0,20,0,  e_tripchimney2  )
			seqActor(S_NULL,  -2048,	   768,-1024,  0,  0,0,  15,30,0,  e_tripchimney2  )
			seqActor(S_NULL,   6930,		 0,-4871,  0,159,0,   0,31,0,  e_warp)
			seqActor(S_NULL,  -5943,		 0,-4903,  0, 49,0,   0,32,0,  e_warp)
			seqPort(10, 8, 1, 10)				/*	MARIO stage in.				*/
			seqMidPort(20, 8, 2, 10)			/*	To scene2 [ In pyramid ].	*/
			seqMidPort(30, 8, 2, 20)			/*	To scene2 [ Gondra ].		*/
			seqPort(31, 8, 1, 32)				/*	Warp.						*/
			seqPort(32, 8, 1, 31)				/*	Warp.						*/
			seqGameClear(6, 3,  51)
			seqGameOver (6, 3, 101)
			seqCall(SEQ_Stage0801_MoveBGs)
			seqCall(SEQ_Stage0801_Enemies)
			seqCall(SEQ_Stage0801_Stars)
			seqMapInfo(cx0801_info)
			seqTagInfo(cx0801_info_tag)
			seqSetMusic(NA_STG_GROUND, NA_MOTOS_BGM)
			seqEnvironment(ENV_DESERT)
		seqEndScene()

		seqBeginScene(2, RCP_Stage8Scene2)
			seqActor(S_NULL,  	  0, 300, 6451,  0,180,0,  0,10,0,  e_player_landing)
			seqActor(S_NULL,  	  0,5500,  256,  0,180,0,  0,20,0,  e_player_landing)
			seqActor(S_NULL,   3070,1280, 2900,  0,180,0,  0,21,0,  e_warp)
			seqActor(S_NULL,   2546,1150,-2647,  0, 78,0,  0,22,0,  e_warp)
			seqPort(10, 8, 2, 10)				/*	MARIO scene in [ Lower ].	*/
			seqPort(20, 8, 2, 20)				/*	MARIO scene in [ Upper ].	*/
			seqPort(21, 8, 2, 22)				/*	Warp.						*/
			seqPort(22, 8, 2, 21)				/*	Warp.						*/
			seqGameClear(6, 3,  51)
			seqGameOver (6, 3, 101)
			seqCall(SEQ_Stage0802_Enemies)
			seqCall(SEQ_Stage0802_Stars)
			seqConnect(3, 3, 0,0,0)
			seqMapInfo(cx0802_info)
			seqTagInfo(cx0802_info_tag)
			seqSetMusic(NA_STG_DUNGEON, NA_DUNGEON_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

		seqBeginScene(3, RCP_Stage8Scene3)
			seqGameClear(6, 3,  51)
			seqGameOver (6, 3, 101)
			seqCall(SEQ_Stage0803_Enemies)
			seqMapInfo(cx0803_info)
			seqTagInfo(cx0803_info_tag)
			seqConnect(2, 2, 0,0,0)
			seqSetMusic(NA_STG_DUNGEON, NA_DUNGEON_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 88, 653,  38, 6566)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
