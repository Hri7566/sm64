/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage28 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"


#if ENGLISH || CHINA
	#include "selectShape/button2.sou"
	#include "selectShape/button.sou"
	#include "selectShape/cursol.sou"
	#include "selectShape/selectFont.h"
	#include "selectShape/font88_ENG_txt.h"
	#include "selectShape/selectpict.sou"
	#include "selectShape/dummy.flk"
#else
	#include "selectShape/button2.sou"
	#include "selectShape/button.sou"
	#include "selectShape/cursol.sou"
	#include "selectShape/selectFont.h"
	#include "selectShape/font88_txt.h"
	#include "selectShape/selectpict.sou"
	#include "selectShape/dummy.flk"
#endif


