/********************************************************************************
	camera.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1995 Nintendo co., ltd.  All rights reserved

	August 24, 1995
*********************************************************************************/
#define	CAMDEBUGSW	0
#define	CAMVER2		1	// 0: original version 1:English version

#define	BVD_A	45	//shido pak no timing no chosei  camdemo.c to autodemo.c ni tukatteiru
#define	BVD_B	30
#define	BVD_C	0
#define	BVD_D	0
#define	BVD_E	10
#ifndef CAMERA_H

#include "camno.h"

#define	SPNUM		32

#define	CAMERA_H

#define	CAM_FIELD	1
#define	CAM_INSIDE	2
#define	CAM_WATER	3
#define	CAM_DUNGEON	4
#define	CAM_TWOPLAY	5
#define	CAM_VIEWING	6
#define	CAM_BRIDGE	7
#define	CAM_RIVER	8
#define	CAM_BACK	9
#define	CAM_CANNON	10
#define	CAM_KUPPA	11
#define	CAM_RAIL	12
#define	CAM_FIX		13
#define	CAM_PARALLEL	14
#define	CAM_SLIDER	15
#define	CAM_DUNGEON_O	16
#define	CAM_SPIRAL	17

#define	CAM_DEMO_OFF		0
#define	CAM_DEMO_TEST0		128
#define	CAM_DEMO_TEST1		129
#define	CAM_DEMO_DOOR1		130
#define	CAM_DEMO_DOOR2		131
//#define	CAM_DEMO_TRAP_FALL	132
#define	CAM_DEMO_CANNON		133
#define	CAM_DEMO_RIPPLE		134
#define	CAM_DEMO_RIPPLEOUT	135
#define	CAM_OPENDEMO_0		136
//#define	CAM_DEMO_DOORIN1	137
//#define	CAM_DEMO_DOORIN2	138
#define	CAM_DEMO_TRIPDOOR	139
#define	CAM_DEMO_DOOR_MAINROOM1	140
#define	CAM_DEMO_DOOR_MAINROOM2	141
#define	CAM_DEMO_MAINMAP 	142
#define	CAM_DEMO_WINDEMO	143
#define	CAM_DEMO_KUPPA		144
#define	CAM_DEMO_VIEWTEST	145
#define	CAM_DEMO_VIEWTEST	145
#define	CAM_DEMO_TURN		146
#define	CAM_DEMO_RIPPLEPUSH	147
#define	CAM_DEMO_PUTONHAT	148
#define	CAM_DEMO_AUTODOOR	149
#define	CAM_DEMO_CANNONDOOR	150
#define	CAM_DEMO_OPENKDOOR	151
#define	CAM_DEMO_GIDDYDOWN	152
#define	CAM_DEMO_FOREDOWNE	153
#define	CAM_DEMO_BACKDOWNE	154
#define	CAM_DEMO_SANDDOWN	155
#define	CAM_DEMO_GASDOWN	156
#define	CAM_DEMO_BEATKUPPA	157
#define	CAM_DEMO_LOSEDKUPPA	158
#define	CAM_DEMO_SWIMDOWNE	159
#define	CAM_DEMO_RIPPLEWIN	160
#define	CAM_DEMO_ITEM		161
#define	CAM_DEMO_TALK		162
#define	CAM_DEMO_TALKYESNO	163
#define	CAM_DEMO_PYLTOP		164
#define	CAM_DEMO_WINDEMOOVERLOOK	165
#define	CAM_DEMO_WINDEMOCLOSEUP	166
#define	CAM_DEMO_WINDEMOSTOROBO	167
#define	CAM_DEMO_PYLEXP		168
#define	CAM_DEMO_PUSHOUT	169
#define	CAM_DEMO_PUSHDOWN	170
#define	CAM_DEMO_LOOKBOARD	171
#define	CAM_DEMO_ENDB		172
#define	CAM_DEMO_LOOKOBJ	173
#define	CAM_DEMO_ENDA		174
#define	CAM_DEMO_WINDEMO2	175
#define	CAM_DEMO_STAR8		176
#define	CAM_DEMO_SAYONARA	177
#define	CAM_DEMO_CREDITS	178
#define	CAM_DEMO_INFALL		179
#define	CAM_DEMO_BLUESKY	180
#define	CAM_DEMO_METALPOOL	181

#define	CAM_FLAG_TURNCENTER		0x0001	/* field camera long viewing	*/
#define	CAM_FLAG_VIEWLONG		0x0002	/* field camera long viewing	*/
#define	CAM_FLAG_TURNLEFT		0x0004	/* field camera turn left		*/
#define	CAM_FLAG_TURNRIGHT		0x0008	/* field camera turn right		*/
#define	CAM_FLAG_TURNCODE		0x0010	/* field camera turn left		*/
#define	CAM_FLAG_DUNGEONWATER		0x0020	/* Dungeon camera suityu-mode */
#define	CAM_FLAG_LOCKPOS		0x0040	/* Dungeon camera suityu-mode */

#define	CAM_FLAG_MDCHANGE		0x0100	/* camera mode changing			*/
#define	CAM_FLAG_VIEWMODEOFF		0x0200	/* map camera					*/
#define	CAM_FLAG_VIEWMODE_ex		0x0400	// player igai ga viewmode wo ON ni sitatoki set suru. 
#define	CAM_FLAG_INIT			0x0800	/* init map camera(in CtrlGameCamera)					*/
#define	CAM_FLAG_MAPMODE		0x1000	/* map camera					*/
#define	CAM_FLAG_VIEWMODE		0x2000
#define	CAM_FLAG_INWATER		0x4000	/* camera in water				*/
#define	CAM_FLAG_PAUSE			0x8000	/* camera pause mode			*/

// Windemo
#define	wdLong		0
#define	wdNormal	1
#define	wdClose		2
#define	wdKuppa		3
#define	wdSonoba	4

#define	gSF_SPECIAL		0x0001	/* Do Special near camera */
#define	gSF_VIEWLONG		0x0002	/* View long no back up*/
#define	gSF_SPECIALMODE		0x0004	/* 1:Special camera 0:Stop camera*/

#define	SC_CHECK	0
#define	SC_ON		1
#define	SC_OFF		2

#define	SM_CHECK	0
#define	SM_SPECIAL	1
#define	SM_STOP		2

#define	gCF_ACHASEON		0x0001
#define	gCF_ACHASEDISABLE	0x0002
#define	gCF_1STFRAME		0x0004
#define	gCF_RAILCHANGED		0x0008
#define	gCF_CAMTOCLOSE		0x0010  // Back camera no toki camera tono kyori wo chijimeru
#define	gCF_HITWALL		0x0020  // Kabe ni atatta toki no flag
#define	gCF_ZZZZ		0x0040  // Mario ga neteiru toki no flag
#define	gCF_CAMTURN		0x0080  // Mario ga Cameara ni atatta toki syori wo tomeru
#define	gCF_CAMTURNINIG		0x0100  // Mario ga Cameara ni atatta toki syori wo tomeru
#define	gCF_DRIFT		0x0200  // Kabe no mawarikomi-cyu
#define	gCF_INBETON		0x0400	// Camera Mode Change no tokino inbetweener no flag
#define	gCF_INBETING		0x0800	// Camera Mode Change no tokino inbetween wo siteirutokino flag
#define	gCF_LOCKMODE		0x1000	// Camera Mode no Lock(1 frame dake)
#define	gCF_DEMO_TURN		0x2000	// Mawarikomi demo no ON/OFF  1=ON
#define	gCF_DEMO_INBETACTIVE	0x4000	// demo cyu- no Inbetween wo on ni suru
#define	gCF_DOOREND		0x8000	// door wo kugutta tokini CAM_MIN no check wo OFF ni suru.

#define	gCS_FIX		0x1	// MainHall no genkan etc..
#define	gCS_PAUSE	0x2
#define	gCS_TURNLEFTmax		0x4
#define	gCS_TURNRIGHTmax	0x8

#define	CDMF_MODETESTON		0x0001
#define	CDMF_JUGEMON		0x0002

#define	gCFd_camAngleyAuto		0x0001	/*AUTO de camAngley wo inc,dec suruka */

#define	gCFSE_VIEWTRIG	0x0001
#define	gCFSE_SPECIAL_ON 	0x0002
#define	gCFSE_SPECIAL_OFF	0x0004
#define	gCFSE_SPECIALMODE_ON 	0x0008
#define	gCFSE_SPECIALMODE_OFF	0x0010
#define	gCFSE_SHUTTER		0x0020


//#define ZOOM_TEST	255
#define ZOOM_OFF	0
#define ZOOM_45	1
#define ZOOM_NORMAL	2
#define ZOOM_CLEAR	3
#define ZOOM_MAINROOM	4
#define ZOOM_DOOR	5
#define ZOOM_RIPPLE	6
#define ZOOM_OBAKE	7
#define ZOOM_UP	8
#define ZOOM_WINDEMOCLOSEUP	9
#define ZOOM_PYLEXPUP	10
#define ZOOM_PYLEXPDOWN	11
#define ZOOM_PEACH	12
#define ZOOM_KISS	13

#define ZS_BEEEN	1
#define ZS_GATYOOON	2
#define ZS_MIDDLE	3
#define ZS_LARGE	4

#define	DI_OUTDOOR	1 // obake yashi no soto he derutoki
#define	DI_RAIL		2 // Main room no Fix -> Rail no toki
#define	DI_15A		3 // Map15 no Fix camera no tame no coard
#define	DI_15B		4 // Map15 no Fix camera no tame no coard

#define VS_SMALL	1
#define VS_MIDDLE	2
#define VS_LARGE	3
#define VS_GOROGORO	4
#define VS_PUNCH	5
#define VS_KEY		6
#define VS_CANNON	7
#define VS_PYLAMID	8
#define VS_CHINBOTSU	9
#define VS_QUAKE	10

#define	VE_PUNCH	1
#define	VE_HIPDROP	2
#define	VE_DAMAGE_S	3
#define	VE_DAMAGE_M	4
#define	VE_DAMAGE_L	5
#define	VE_DAMAGE_WATER	6
#define	VE_DEAD		7
#define	VE_HEADATK	8
#define	VE_LANDDMG	9
#define	VE_ELEC		10

#define	VIEW_ON		1
#define	VIEW_OFF	2

#define	SW_OFF	0	// swing mode
#define	SW_NORMAL	1	// swing mode
#define	SW_SHIP		2	// swing mode
#define	SW_UNTEI	3	// swing mode
#define	SW_GIDDY	4	// swing mode
#define	SW_OVERLOOK	5	// swing mode
#define	SW_MAINMAP	6	// swing mode

// BG code list
#define	CAMCODE_FIELD		13
#define	CAMCODE_DUNGEON		11
#define	CAMCODE_DUNGEON_O	102
#define	CAMCODE_DUNGEON_SLOPE	121
#define	CAMCODE_WALLROTOFF	40
#define	CAMCODE_HEIKOU		105
#define	CAMCODE_TURNCENTER	110
#define	CAMCODE_TURNRIGHT	111
#define	CAMCODE_TURNLEFT	112
#define	CAMCODE_RIPPLE_START	211
#define	CAMCODE_RIPPLE_END	252
#define	CAMCODE_DEATH	10	// ku-cyu- shibou
#define	CAMCODE_KUPPA	101

// Camera Parameter

//#define BIG_DIST		400.0f
//#define FIELD_DIST_MM		 796.0f /* Main Map */
//#define	FIELD_LONG		 400.0f	/*400.0f*/
//#define	FIELD_LONG_MM		 400.0f	/*400.0f*/
//#define	FIELD_HEIGHT	 100.0f
//#define	FIELD_HEIGHT_MM	 100.0f	/*125.0f*/
//#define	FIELD_PITCH		0x200	//ANGLE8
//#define	FIELD_DISTANGLEX	0x900
//#define	FIELD_DISTANGLEX_MM	0x900

#define CAMINIT_HEIGHT		125.0f

#define FIELD_DIST		1000.0f
#define	FIELD_LONG		 400.0f	/*400.0f*/
#define	FIELD_HEIGHT	 125.0f
#define	FIELD_PITCH		ANGLE8
#define	FIELD_DISTANGLEX	0x900

#define PARALLEL_DIST		1000.0f
#define	PARALLEL_LONG		400.0f
#define	PARALLEL_HEIGHT	 	125.0f
#define	PARALLEL_PITCH		ANGLE8
#define	PARALLEL_DISTANGLEX	0x900

#define BIG_DIST	600.0f
#define	BIG_LONG	400.0f
#define	BIG_HEIGHT	125.0f
#define	BIG_PITCH	ANGLE8
#define	BIG_DISTANGLEX	0x900
#define SMALL_DIST	1000.0f
#define	SMALL_LONG	400.0f
#define	SMALL_HEIGHT	125.0f
#define	SMALL_PITCH	ANGLE8
#define	SMALL_DISTANGLEX	0x900

#define	WATER_DIST		800.0f
#define	WATER_MIN		300.0f
#define	WATER_LONG		 50.0f
#define	WATER_HEIGHT	125.0f

#define	RIVER_DIST		800.0f
#define	RIVER_LONG		 50.0f
#define	RIVER_HEIGHT	125.0f
#define	RIVER_LOOKHEIGHT	125.0f;

#define RAIL_HEIGHT	150.0f

#define BACK_DIST	800.0f;
#define BACK_DIST_MAP6	1000.0f;

#define	VIEW_DIST		250.0f
#define	VIEW_HEIGHT		125.0f

#define	BIRD_DIST	1000.0f
#define	BIRD_ANGX	0x2800


#define	SPECIAL_WATER_DIST	350.0f
#define	SPECIAL_WATER_HEIGHT	120.0f

#define	SPECIAL_DIST	350.0f
#define	SPECIAL_HEIGHT	90.0f
#define	SPECIAL_LOOKHEIGHT	100.0f
#define	SPECIAL_HEIGHTLONG	300.0f+400.0f
#define	SPECIAL_LONG		 1050.0f


#define	DUNGEON_DIST	800.0f
#define	DUNGEON_HEIGHT	125.0f
#define	DUNGEON_O_HEIGHT	225.0f
#define	DUNGEON_HEIGHTLONG	500.0f
#define	DUNGEON_LONG		 400.0f
#define	DUNGEON_MIN		 200.0f
#define	DUNGEON_MIN_CAM		 250.0f

#define	SMOG_HEIGHT	130.0f

#define	FIX_MIN		 300.0f	/*200.0f*/
#define	FIX_HIGHTLONG	 400.0f	/*200.0f*/
#define	RAIL_MIN		 300.0f	/*200.0f*/
#define	RAIL_HIGHTLONG	 400.0f	/*200.0f*/

#define	TWOPLAY_HEIGHT	125.0f
#define	TWOPLAY_DIST	375.0f

#define	KUPPA_HEIGHT	300.0f
#define	KUPPA_LOOKHEIGHT	100.0f
#define	KUPPA_DIST	375.0f
#define	KUPPA_LONG	400.0f
#define	KUPPA_ANGLEX	0x1000
#define	KUPPA_DISTANGLEX	0x1800

#define	SPIRAL_HEIGHT	125.0f

#define	CANNON_DEPTH	335.0f+50.0f	/* 50.0f = offset */

// Winning Demo

#define WINDEMO_DIST	600.0f
#define WINDEMO_HEIGHT	100.0f

//end of Camera Parameter

#define	ANGLE8		0x05B0
#define	ANGLE30		0x1555
#define	ANGLE45		0x2000
#define	ANGLE60		0x2AAA
#define	ANGLE90		0x4000
#define	ANGLE180	0x8000
#define	ANGLE359	0x8000

// Demo producer's code

#define	PRODUCE_WINDEMO	1
#define	PRODUCE_KUPPA	2
#define	PRODUCE_ENDB	3
#define	PRODUCE_MAINMAP	4
#define	PRODUCE_PEACH	5
#define	PRODUCE_JUGEM	6
#define	PRODUCE_BIRD	7
#define	PRODUCE_BIRD2	8
#define	PRODUCE_BIRD_B	9

/********************************************************************************/
/*	Global veriable.															*/
/********************************************************************************/

/* player information record	*/

typedef struct {
	ulong	status;					/* player's status			*/
	FVector	position;				/* player's position		*/
	SVector	angle;					/* player's angle			*/
	short	headAngx;				/* player's head angle x	*/
	short	headAngy;				/* player's head angle y	*/
	short	eyeCtrl;				/* player's eye control		*/
	short	handCtrl;
	short	demostatus;
	StrategyRecord	*Stp;

} CamPLInfo, *CamPLInfoPtr;



/* camera parameter record	*/

typedef struct {

	FVector		lookat;			/* camera look at point							*/
	FVector		position;		/* camera position	     						*/
	float		distance;		/* camera distance from a look at point			*/
	short		anglex;			/* camera rotation angle x						*/
	short		angley;			/* camera rotation angle y						*/

} CamParam;

/* camera mode change record	*/

typedef struct {
	short		mode;			/* change camera mode							*/
	short		retmode; 
	short		nframes;		/* mode change frames							*/
	short		counter;		/* mode change conter							*/
	CamParam	origin;
	CamParam	change;
} CamMDChange;


/* Demo actor structure	*/

typedef struct {

	FVector		position;			/* camera look at point							*/
	FVector		lookat;			/* camera look at point							*/

} DemoParam;

/* Camera joint Data structure */

typedef struct	{

	FVector	position;
	short	flag;

}JointData;



typedef struct	{
	int	flag;
	short	ang;
}cwr;


/* Data structure for Demo */


typedef struct	{

	ulong		status;
	FVector		position;
	FVector		vec;
	SVector		angle;
	short		timer;

}Demodata;



/* camera zoom record	*/

typedef struct {
	uchar	mode;	 /* zoom mode									*/
	uchar	oldmode; /* zoom mode(old)									*/
	float	angle;
	float	ofs;
	long	count;
	float	shakeang;	/* shake value */
	short	shakefreq;	/* shake frequency */
	short	shakefreqvel;	/* shake velocity */
	short	shakeangdec;	/* shake decriment value */

} ZoomRecord, *ZoomPtr;

/* camera information record	*/

typedef struct {
	uchar		mode;			/* camera mode									*/
	uchar		orgmode;		/* original camera mode							*/
	short		panning;		/* base angle for player control				*/
	FVector		lookat;			/* current look at point						*/
	FVector		position;		/* current camera position						*/
	SVector		angle;
	SVector		angleoffset;
	float		centerx;		/* camera rotation center position x			*/
	float		centerz;		/* camera rotation center position z			*/
	uchar		demomode;			/* camera mode									*/
	float		persp;			/* camera mode									*/

	char		BGareabuf;

	short		pan;		/* base angle for player control(buffer)				*/
	short		panofs;		/* base angle for player control(offset)				*/

	ZoomRecord	zoom;

	float		DIST;
	float		LONG;

	uchar		doorinfo;	/* camera mode */

	float		centery;		/* camera rotation center position y			*/

} CameraRecord, *CameraPtr;


extern	CameraPtr cameraOut;

/* camera information record buffer */

typedef struct {

	FVector	lookat;		/* current look at point(Main)						*/
	FVector	position;	/* current camera position(Main)						*/
	FVector	Slookat;	/* current look at point(Sub)						*/
	FVector	Sposition;	/* current camera position(Sub)						*/

	FVector	center;		/* Map center */

	uchar	mode;		/* camera mode									*/
	uchar	orgmode;	/* original camera mode							*/

	uchar	perspmode;	/* perspective mode									*/
	float	persp;		/* perspective */
	float	perspofs;	/* perspective offset */

	float	dist;
	SVector	ang;
	SVector	shakeang;	/* shake value */
	short	shakefreqX;	/* shake frequency */
	short	shakefreqvelX;	/* shake velocity */
	short	shakeangdecX;	/* shake decriment value */

	FVector	Wallofs;	// Wall ni atatta toki no inbetween wo yaru //

	SVector	swingang;	/* swing angle */
	char	swingpnt;	/* data pointer */
	short	swingfreq;	/* swing frequency */
	short	swingfreqvel;	/* swing velocity */
	short	swingangdec;	/* swing decriment value */

//	FVector	Oposition;	/* old camera position(Sub) */
//	FVector	Olookat;	/* old camera lookat(Sub)   */


	short	anglez;		// rotation z

	short	panning;
	short	pan;

	FVector	Flookat;	/* final output data */
	FVector	Fposition;	/* final output data */


	short	shakefreqZ;	/* shake frequency */
	short	shakefreqvelZ;	/* shake velocity */
	short	shakeangdecZ;	/* shake decriment value */

	short	shakefreqY;	/* shake frequency */
	short	shakefreqvelY;	/* shake velocity */
	short	shakeangdecY;	/* shake decriment value */

	float	lookrate;	/* camera->look chase rate (default 0.8f)*/
	float	lookrateY;	/* camera->look[_AY] chase rate (default 0.3f)*/
	float	posrate;	/* camera->position chase rate(default 0.3f )*/
	float	posrateY;	/* camera->position[_AY] chase rate(default 0.3f )*/

	short	anglezofs;		// rotation z offset

	ulong	lastmInfo_status;

	short	scrollrot;	// BG scroll no offset


} CameraBuffer, *CameraBuf;



extern	CameraBuffer	camWork;



/* Data structure for Camera Inbetween system */

typedef struct	{
	short	angx; //rotation x
	short	angy; //rotation y
	float	dist  ;
	short	Langx; //rotation x
	short	Langy; //rotation y
	float	Ldist  ;
	int	frame ;

	FVector Omario;	//Mario no old position

	int	Lframe ;

	}InbetRecord , *InbetPtr;

/* Data structure for Camera Control Block */

typedef struct	{
	char 	sceneNo;
	short 	(*prog)(CameraPtr camera);
	SVector		center; //center pos
	SVector		width ; //width
	short		angley; //rotation y
	}CamCtlBlock , *CamCtlBlockPtr;

// Data structure for Viewmode no buffer

typedef struct	{
	FVector	position;
	FVector	lookat;
	float		camLookatxofs;
	float		camLookatyofs;
	}Viewbakdat , *ViewbakdatPtr;

/* Data structure for Spline */

typedef struct	{
	char	code;	// Code
	float	T;	// T no sokudo (0<Tvel<1.0)
	SVector	pos;
	}Splinedat , *SplinedatPtr;


typedef struct	{
	char	code;	// Code
	uchar	T;	// T no sokudo (0<Tvel<1.0)
	SVector	pos;
	}SplinedatZ , *SplinedatPtrZ;

/* Data structure for Camera Rail */

typedef struct	{
	short	code;	// 0:end code
	FVector	 pos;
	float	 width;
	float	 xper ;
	}Raildat , *RaildatPtr;


typedef struct	{

	BGCheckData	*Gplane;
	float		ground;
	short		Gattr;
	BGCheckData	*Rplane;
	short		Rattr;
	float		roof;

	BGCheckData	*GplaneOld;
	float		groundOld;
	short		GattrOld;
	BGCheckData	*RplaneOld;
	float		roofOld;
	short		RattrOld;

	float		water;

}C_marioBGInfo ,*C_marioBGInfoPtr;



typedef struct	{

	short No;
	float dist;
	SVector	Pang;
	SVector	Lang;
	float	frame;
	float	T;

}Jg , *JgPtr;


/********************************************************************************/
/*	Global veriable.															*/
/********************************************************************************/

extern CamPLInfo	camPlayerInfo[2];		/* player information for camera control	*/
extern short	gameCameraControl;		/* game camera control flags				*/
extern short	gameCameraFlag;		/* game camera control flags				*/
extern short	gameCameraCounter;	/* Demo tokatde tsukau...*/



extern SplinedatZ	Cspline[SPNUM];		// spline test
extern SplinedatZ	CsplineL[SPNUM];		// spline test
#if CAMDEBUGSW
extern	CamDebugModeFlag;
#endif

extern short	Csplineno;		// spline test
extern float	CsplineT;		// spline test
extern short	Csplinecount;		// spline test

//camdemo.c

extern short	DemoCameraPointer;
extern short	DemoCameraCounter;

extern SplinedatZ       JugemP[];
extern SplinedatZ       JugemL[];
extern SplinedatZ       JugemP2[];
extern SplinedatZ       JugemL2[];

/* For Demo */

extern	Demodata	camDemo[10];

//extern	StrategyRecord	*demostratstp;
extern	StrategyRecord	*demoposstp;
extern	StrategyRecord	*demolookstp;
extern	StrategyRecord	*camBOSSstp;

extern	int	demoseqcode;
extern	uchar	ENDdemomode;

extern	int		producecode;

extern	CameraPtr Camerawork;

/* Slider no block kirikae */

extern	FVector	camBlockofs;

/********************************************************************************/
/*	Function prototype declaration.												*/
/********************************************************************************/

extern void
InitCamera_1st(void);

extern ulong
ZoomControl(int code, MapNode *node, void *data);

extern ulong
GameCamera(int code, MapNode *node, void *data);

extern void
CtrlGameCamera(CameraPtr camera);

extern void
InitGameCamera(CameraPtr camera);

extern void
InitCameraParam(CameraPtr camera);

extern void
ChangeGameCamera(CameraPtr camera, short new_mode, short frames);

extern void
InbetGameCamera(CameraPtr camera, short newMode, short nframes);

extern short
cameraDemo(uchar mode);

extern short
cameraDemoMsg(uchar mode);

extern short
cameraDemoStratMsgNum(uchar mode,StrategyRecord *stp,short No);

extern short
cameraDemoStratMsg(uchar mode,StrategyRecord *stp);

extern short
cameraDemoStrat(uchar mode,StrategyRecord *stp);

extern void
Viewshake(short mode);

extern void
Vieweffect(short mode);

extern void
Viewshaking(short mode,float x,float y,float z);

extern void
InbetCamera(CameraPtr camera, short nframes);

extern int
InitViewingCamera(CameraPtr camera);

extern int
FinishViewingCamera(CameraPtr camera);


extern SplinedatZ       xxxxP[];
extern SplinedatZ       xxxxL[];
extern SplinedatZ       sayoP[];
extern SplinedatZ       sayoL[];

//camlib.c								

extern	void
CCW_Parallel(CameraPtr camera);

extern int
SpecialMode(int mode);

extern int
SpecialCamera(int mode);

extern	int
Crou_CamAngLimit(FVector position,FVector lookat,short maxX,short minX);

extern	short
Crou_CameraIconSet(CameraPtr camera);

//extern short
//GetSpline(FVector POS,FVector LOOK,float FRAME,short *Prx,short *Pry,short *Lrx,short *Lry);

extern int
CamWallCheck(FVector position, float ofs, float R);

extern	int
Crou_sameFV(FVector p,float x,float y,float z);

extern	int
Crou_mariosamepos(float	x,float y,float z);

extern void
SubFVector(FVector org, FVector sub);

extern	void
Crou_SeekViewoff_pos(CameraPtr camera);

extern short
Crou_XZbarrier(FVector position,FVector lookat,float Xl,float Xs,float Zl,float Zs);

extern void
Srou_pos2FVector(FVector pos, StrategyRecord  *stp );

extern void
Srou_FVector2pos(StrategyRecord  *stp,FVector pos);

extern void
Srou_rot2SVector(SVector rot, StrategyRecord  *stp);

//extern void
//Srou_SVector2rot( StrategyRecord  *stp ,SVector rot);

extern short
Crou_AnglePerCamDist(short angle,float offdist,float x,float y,float z);

extern int
ViewingCam(int sw);

extern void
Crou_BGM_Peach(void);

extern void
Crou_BGM_Openning(void);

extern void
Crou_SE_SHUTTER(void);

extern void
Crou_SE_SPECIALCAMERA(void);

extern void
Crou_SE_ZOOM_IN (void);

extern void
Crou_SE_ZOOM_OUT(void);

extern void
Crou_SE_CAMERAMOVE(void);

extern void
Crou_SE_BUZZER(void);

extern void
Crou_DebugCommand(CameraPtr camera);

extern short
Crou_Forcerot_s(short org , short obj );

extern short
Crou_camAngley_INSIDE(CameraPtr camera,short fieldang);

extern int
Crou_CalcPlaneArea(BGCheckData *plane,float Lx, float Ly,float Lz);

extern int
Crou_CheckPlaneFace(FVector lookat,FVector position,BGCheckData *plane,short ofs,short OFFCODE);

extern ushort
Crou_Key(ushort key ,ushort trigger,ushort status);

extern int
c_c_achase_f(float *org , float obj , float rate );

extern int
c_c_fchase_s(short *org , short obj , short rate );

extern int
c_fchase_s(short *org , short obj , short rate );

extern int
c_achase_f(float *org , float obj , float rate );

extern int
c_achase_s(short *org , short obj , short rate );

extern int
c_fchase_f(float *org , float obj , float rate );

extern short
c_Fchase_s(short org , short obj , short rate );

extern float
c_Achase_f(float org , float obj , float rate );

extern short
c_Achase_s(short org , short obj , short rate );

extern float
c_Fchase_f(float org , float obj , float rate );

extern void
c_achase_FVector(FVector org , FVector obj , float ratex,float ratey,float ratez );

extern void
c_c_achase_FVector(FVector org , FVector obj , float ratex,float ratey,float ratez );

extern void
c_achase_SVector(SVector org , SVector obj , short ratex,short ratey,short ratez );

//extern void
//c_fchase_FVector(FVector org , FVector obj , float ratex,float ratey,float ratez );

extern void
Crou_roffs2pos( FVector pos, FVector ofs , FVector org , SVector angle);

extern	void
Crou_roffs2posvalue( FVector pos,FVector org, SVector angle,float x,float y,float z);

//extern	void
//Crou_roffs2obj( FVector pos,StrategyRecord *stp, float ofsx,float ofsy,float ofsz);

//extern void
//c_fchase_SVector(SVector org , SVector obj , short ratex,short ratey,short ratez );

extern short
Crou_CalcAnglex(FVector center, FVector point);

extern short
Crou_CalcAngley(FVector center, FVector point);

extern void
Crou_CalcAngle(FVector center, FVector point,short *angx,short *angy);

extern float
Crou_CalcDistXZ(FVector center, FVector point);

extern float
Crou_CalcDist  (FVector center, FVector point);

//extern void
//Crou_RotatePos(FVector pos, FVector org , short anglex ,short angley);

extern void
Crou_YRotatePos(FVector pos, FVector pnt , short angley);

extern void
Crou_XRotatePos(FVector pos, FVector pnt , short anglex);

extern void
Crou_Set_ViewshakeX(short angle,short angledec,short freqvel);

extern void
Crou_Set_ViewshakeY(short angle,short angledec,short freqvel);

extern void
Crou_Set_ViewshakeZ(short angle,short angledec,short freqvel);

extern void
Crou_Set_ViewshakingX(short angle,short angledec,short freqvel,float offdist,float x,float y,float z);

extern void
Crou_Set_ViewshakingY(short angle,short angledec,short freqvel,float offdist,float x,float y,float z);

static	void
Crou_ViewshakeX(FVector position,FVector lookat);

static	void
Crou_ViewshakeY(FVector position,FVector lookat);

static	void
Crou_ViewshakeZ(short *rotz);

extern	void
Crou_Viewswing(FVector position,FVector lookat);

extern void
Viewswing(uchar mode);

extern short
Crou_ViewLong(CameraPtr camera, float distoff);

extern void
Crou_SE_ViewDdisable(void);

extern void
Crou_SE_ViewLRdisable(void);

extern void
Crou_SE_ViewUDLRdisable(void);

extern	void
Crou_SetDemoMode(CameraPtr camera,uchar mode);

extern void
Crou_MidPnt(FVector pos0,FVector pos1,FVector pos2,float rate);

extern	uchar
Crou_DemoMode(CameraPtr camera);

extern void
Crou_camBlockofs(float x,float y,float z);

extern void
Crou_traceCamHeight(CameraPtr camera,float height,float rate);

extern	void
Crou_test_campos(FVector position ,SVector angle,float vx,float vy,float vz);

extern void
Crou_ViewMode(CameraPtr camera);

extern short
Crou_CameraWizard(CameraPtr camera);

extern void
Crou_WaterWallCheck(FVector position,FVector oldpos);

extern void
Crou_ChengeGameMode_test(CameraPtr camera);

extern void
Crou_InitFixCampos(short mode);

//extern void
//Crou_TurnDemo(CameraPtr camera , FVector lookat , FVector position ,float MIN);

extern int
Crou_CalcWallRoty(CameraPtr camera ,FVector position,short *angley,short ofs);

//extern int
//Crou_CalcWallRotx(CameraPtr camera,short *anglex);

extern short
Crou_Inbet(FVector ipos,FVector ilook,FVector position,FVector lookat,FVector Oposition,FVector Olookat,short pan);

extern void
Crou_marioBG(C_marioBGInfoPtr m);

extern void
Crou_InitRailCamera(CameraPtr camera,RaildatPtr ptr);

//extern int
//Crou_Dospline(FVector pos,SplinedatPtr ptr,short *pointer,float *T);

extern int
Crou_DosplineZ(FVector pos,SplinedatPtrZ ptr,short *pointer,float *T);

//extern void
//Crou_SetRandom_FVector(FVector pos,float Rx,float Ry,float Rz);

extern void
Crou_SetRandom_SVector(SVector pos,short Rx,short Ry,short Rz);
//camdemo.c								

extern void
DoDemoCamera( CameraPtr camera );

extern int
DoDemoRou(short (*prog)(CameraPtr camera) ,CameraPtr camera,short StartTime ,short EndTime);

extern int
DoProduce(int code, short time);

extern	Raildat RD6_Mainroom[];

//zoom.c
extern ulong
ZoomControl(int code, MapNode *node, void *data);

extern ulong
ZoomControl_Dungeon(int code, MapNode *node, void *data);

extern void
SetZoomMode(uchar mode);

extern void
Zoomshake(uchar mode);

extern void
Zoomshaking(uchar mode,float x,float y,float z);

extern void
Zrou_Set_Zoomshake(short angle,short angledec,short freqvel);

// windemo.c

extern
uchar	wintbl[27][4];

extern
uchar	pauseinfo[19];





// BATTLE                               
extern SplinedatZ	spBattleP[];
extern SplinedatZ       spBattleL[];
// MOUNTAIN                               
extern SplinedatZ       spMountainP[];
extern SplinedatZ       spMountainL[];
//WaterDungeon
extern SplinedatZ       spWaterDungeonP[];
extern SplinedatZ       spWaterDungeonL[];
// Snow Slider                                        
extern SplinedatZ       spSnowsliderP[];
extern SplinedatZ       spSnowsliderL[];
// HorrorHouse                                        
extern SplinedatZ       spHorrorHouseP[];
extern SplinedatZ       spHorrorHouseL[];
// HorrorDungeon                                        
extern SplinedatZ       spHorrorDungeonP[];
extern SplinedatZ       spHorrorDungeonL[];
// Hanacyan                                       
extern SplinedatZ       spHanacyanP[];
extern SplinedatZ       spHanacyanL[];
// ChikaLava                                       
extern SplinedatZ       spChikaLavaP[];
extern SplinedatZ       spChikaLavaL[];
// Sabaku                                       
extern SplinedatZ       spSabakuP[];
extern SplinedatZ       spSabakuL[];
// WaterLand                                    
extern SplinedatZ       spWaterLandP[];
extern SplinedatZ       spWaterLandL[];
// Snowman                                
extern SplinedatZ       spSnowmanP[];
extern SplinedatZ       spSnowmanL[];
// Pool
extern SplinedatZ       spPoolP[];
extern SplinedatZ       spPoolL[];
// Donkey
extern SplinedatZ       spDonkeyP[];
extern SplinedatZ       spDonkeyL[];
// Big world  
extern SplinedatZ       spBigWorldP[];
extern SplinedatZ       spBigWorldL[];
// Clock
extern SplinedatZ       spClockP[];
extern SplinedatZ       spClockL[];
// Rainbow
extern SplinedatZ       spRainbowP[];
extern SplinedatZ       spRainbowL[];
// Suisou
extern SplinedatZ       spSuisouP[];
extern SplinedatZ       spSuisouL[];
// Fall
extern SplinedatZ       spFallP[];
extern SplinedatZ       spFallL[];
// Submarine
extern SplinedatZ       spSubmarineP[];
extern SplinedatZ       spSubmarineL[];
// Yukiyama
extern SplinedatZ       spYukiyamaP[];
extern SplinedatZ       spYukiyamaL[];

// kobject.c


extern unsigned long e_opening_jugem[];
extern unsigned long e_ending_bird[];
extern unsigned long e_ending_bird2[];
extern unsigned long e_opening_peach[];
extern unsigned long e_opening_jugem[];
extern unsigned	long e_ENDB_peach[];
extern unsigned	long e_ENDB_director[];
extern unsigned	long e_demo_producer[];

#if	CAMDEBUGSW
extern unsigned long e_tool_jugem[];
#endif


#endif
