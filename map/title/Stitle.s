/********************************************************************************
						Ultra 64 MARIO Brothers

						 title sequence module

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							April 11, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

#define	FADEOUT_TIME		190

	.data
	.align	2
	.align	0

	.globl	SEQ_LogoSequence
	.globl	SEQ_TitleSequence
	.globl	SEQ_GmOverSequence
	.globl	SEQ_DebugSequence


/********************************************************************************/
/*																				*/
/*	Nintendo logo display sequence.												*/
/*																				*/
/********************************************************************************/

SEQ_LogoSequence:

/*************** stage construction *********************************************/

	seqInitStage()
	seqLoadCode(DEMOCODE_START	, _TCodeSegmentRomStart, _TCodeSegmentRomEnd)
	seqLoadData(SEGMENT_PATHDATA,  _PathSegmentRomStart,  _PathSegmentRomEnd)
	seqLoadPres(SEGMENT_STAGE1	, _TLogoSegmentRomStart, _TLogoSegmentRomEnd)

	seqBeginConstruction()
		seqBeginScene(1, RCP_NintendoLogo)
		seqEndScene()
	seqEndConstruction()

/*************** display Nintendo logo ******************************************/

	seqOpenScene(1)
	seqCProgram(TitleProcess, 0)
	seqWait(75)
	seqWipe(WIPE_FADE_OUT, 16, 0, 0, 0)
	seqWait(16)
	seqCloseScene(1)
	seqDestroyStage()
	seqWait(2)
	seqChain(SEGMENT_DEMODATA, _TitleSegmentRomStart, _TitleSegmentRomEnd, SEQ_TitleSequence)


/********************************************************************************/
/*																				*/
/*	Title sequence.																*/
/*																				*/
/********************************************************************************/

SEQ_TitleSequence:

/*************** stage construction *********************************************/

	seqInitStage()
	seqBlanking(1)
	seqLoadCode(DEMOCODE_START	, _TCodeSegmentRomStart, _TCodeSegmentRomEnd)
	seqLoadFace(2)
	seqLoadData(SEGMENT_PATHDATA,  _PathSegmentRomStart,  _PathSegmentRomEnd)
	seqLoadText(SEGMENT_BACK	, _TWallSegmentRomStart, _TWallSegmentRomEnd)

	seqBeginConstruction()
		seqBeginScene(1, RCP_MapTitle)
		seqEndScene()
	seqEndConstruction()

/*************** display mario face demo ***************************************/

	seqWait(2)
	seqBlanking(0)
	seqOpenScene(1)
	seqPlayMusic(NA_TITLE_BGM, 0)
	seqWipe(WIPE_STAR_WIN_OPEN, 20, 0, 0, 0)
	seqWait(20)
	seqRunning(TitleProcess, 1)

	seqTstJump(SEQ_EQ, 100, SEQ_ChainSelect)
	seqTstJump(SEQ_EQ, 101, SEQ_ChainDebug )
	seqJump(SEQ_ChainDemo)


/********************************************************************************/
/*																				*/
/*	Continue sequence.															*/
/*																				*/
/********************************************************************************/

SEQ_GmOverSequence:

/*************** stage construction *********************************************/

	seqInitStage()
	seqBlanking(1)
	seqLoadCode(DEMOCODE_START	, _TCodeSegmentRomStart, _TCodeSegmentRomEnd)
	seqLoadFace(3)
	seqLoadData(SEGMENT_PATHDATA,  _PathSegmentRomStart,  _PathSegmentRomEnd)
	seqLoadText(SEGMENT_BACK	, _TWallSegmentRomStart, _TWallSegmentRomEnd)

	seqBeginConstruction()
		seqBeginScene(1, RCP_MapGmOver)
		seqEndScene()
	seqEndConstruction()

/*************** display mario face demo ***************************************/

	seqWait(2)
	seqBlanking(0)
	seqOpenScene(1)
	seqPlayMusic(NA_OVER_TITLE_BGM, 0)
	seqWipe(WIPE_STAR_WIN_OPEN, 20, 0, 0, 0)
	seqWait(20)
	seqRunning(TitleProcess, 2)

	seqTstJump(SEQ_EQ, 100, SEQ_ChainSelect)
	seqTstJump(SEQ_EQ, 101, SEQ_ChainDebug )
	seqJump(SEQ_ChainDemo)


/********************************************************************************/
/*																				*/
/*	Debug sequence.																*/
/*																				*/
/********************************************************************************/

SEQ_DebugSequence:

/*************** stage construction *********************************************/

	seqInitStage()
	seqLoadData(SEGMENT_PATHDATA,   _PathSegmentRomStart,   _PathSegmentRomEnd)
	seqLoadText(SEGMENT_BACK	,  _TWallSegmentRomStart,  _TWallSegmentRomEnd)
	seqLoadPres(SEGMENT_STAGE1	,  _DebugSegmentRomStart,  _DebugSegmentRomEnd)
	seqLoadCode(DEMOCODE_START	,  _TCodeSegmentRomStart,  _TCodeSegmentRomEnd)

	seqBeginConstruction()
		seqBeginScene(1, RCP_MapSelect)
		seqEndScene()
	seqEndConstruction()

/*************** display select screen ******************************************/

	seqOpenScene(1)
	seqPlayMusic(NA_TITLE_BGM, 0)
	seqWipe(WIPE_FADE_IN, 16, 255, 255, 255)
	seqWait(16)
	seqRunning(TitleProcess, 3)

	seqTstJump(SEQ_EQ, -1, SEQ_ChainLogo)
	seqJump(SEQ_ChainGame)


/********************************************************************************/
/*	Chain select																*/
/********************************************************************************/

SEQ_ChainSelect:

	seqStopMusic(FADEOUT_TIME)
	seqWipe(WIPE_FADE_OUT, 16, 255, 255, 255)
	seqWait(16)
	seqDestroyStage()
	seqWait(2)

	seqSetResult(16)
	seqChain(SEGMENT_DEMODATA, _HmsSelectSegmentRomStart, _HmsSelectSegmentRomEnd, SEQ_SelectSequence)

/********************************************************************************/
/*	Chain auto demo																*/
/********************************************************************************/

SEQ_ChainDebug:

	seqWipe(WIPE_FADE_OUT, 16, 255, 255, 255)
	seqWait(16)
	seqDestroyStage()
	seqWait(2)
	seqChain(SEGMENT_DEMODATA, _TitleSegmentRomStart, _TitleSegmentRomEnd, SEQ_DebugSequence)

/********************************************************************************/
/*	Chain game																	*/
/********************************************************************************/

SEQ_ChainGame:

	seqStopMusic(FADEOUT_TIME)
	seqWipe(WIPE_FADE_OUT, 16, 255, 255, 255)
	seqWait(16)
	seqDestroyStage()
	seqWait(2)
	seqChain(SEGMENT_GAMEDATA, _GameSegmentRomStart, _GameSegmentRomEnd, SEQ_GameSequence)

/********************************************************************************/
/*	Chain auto demo																*/
/********************************************************************************/

SEQ_ChainDemo:

	seqWipe(WIPE_FADE_OUT, 16, 255, 255, 255)
	seqWait(16)
	seqDestroyStage()
	seqWait(2)
	seqChain(SEGMENT_GAMEDATA, _GameSegmentRomStart, _GameSegmentRomEnd, SEQ_GameSequence)

/********************************************************************************/
/*	Chain title logo															*/
/********************************************************************************/

SEQ_ChainLogo:

	seqStopMusic(FADEOUT_TIME)
	seqWipe(WIPE_FADE_OUT, 16, 0, 0, 0)
	seqWait(16)
	seqDestroyStage()
	seqWait(2)
	seqChain(SEGMENT_DEMODATA, _TitleSegmentRomStart, _TitleSegmentRomEnd, SEQ_LogoSequence)
