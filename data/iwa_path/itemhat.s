/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Strategy

					March 31 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/*==============================================================================
						Path Data
 ===============================================================================*/
e_itemhat_wing:
	p_initialize(item)
	p_setbit(flag,stf_moveON | stf_playerangleON)
	p_program(s_itemhat_wing_init)

	p_while
		p_program(s_itemhat_wing_main)
	p_loop



e_itemhat_metal:
	p_initialize(item)
	p_setbit(flag,stf_moveON | stf_playerangleON)
	p_program(s_itemhat_metal_init)

	p_while
		p_program(s_itemhat_metal_main)
	p_loop



e_itemhat_blow:
	p_initialize(item)
	p_setbit(flag,stf_moveON | stf_playerangleON)
	p_program(s_itemhat_blow_init)

	p_while
		p_hitON
		p_program(s_itemhat_blow_main)
	p_loop

e_itemhat_erase:
	p_initialize(item)
	p_setbit(flag,stf_moveON | stf_playerangleON)
	p_program(s_itemhat_erase_init)

	p_while
		p_program(s_itemhat_wing_main)
	p_loop



/*################*/
#else
/*################*/


/*==================================================================================================
		C Program
 ===================================================================================================*/
#define hat_scale_flag	(execstp->s[stw_work0].d)
#define hat_scale		(execstp->s[stw_work1].d)

#define SABAKU_F	2


static HitCheckRecord item_hat_hit = {
	OBJNAME_ITEMHAT,
	 0,0,  0,0,		/* flag,hp,ap,coin 		 */
	 80,    80,		/* player damage hitarea */
  80+10, 80+10,		/* player attack hitarea */
};


/***************************************************************************************************
						  Hit Check
***************************************************************************************************/
static char hat_HitCheckEvent(void)
{

	s_set_hitparam(execstp,&item_hat_hit);		/* Set HitCheck Param */

	if ( obj_mail & EMAIL_PLAYERHIT ){
		execstp->status = 0;
		Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
		return(1);
	}

	return(0);
}
/***************************************************************************************************
						   Remove Check
***************************************************************************************************/
static void hat_RemoveCheck(void)
{
	if (obj_timer > ITEMHAT_TIME) {
		
		iwa_TimerRemove(execstp,ITEMHAT_TIME);
	}


}
/***************************************************************************************************
						    Hat Lost Check
***************************************************************************************************/
static void HatLostCheck(void)
{


	 if (moveobj_bgcp == NULL) { return; }

	switch ( bgdata_attr) {
		case 10  : 	execstp->status = 0;     // air dead
					break;

		case 33: 
		case 38: 
		case 34: obj_mode = 10;
				 obj_speedF = 0;
				 break;

		case 36:
		case 37:
		case 39: obj_mode = 11;
				 obj_angleY =  bgdata_angleY;
				 obj_speedF =  SABAKU_F*(3-bgdata_wspeed)+SABAKU_F;
				 break;


		case 35: obj_mode = 12;
				 obj_speedF = 0;
				 break;

		case 45: obj_mode = 13;
				 obj_angleY =  bgdata_angleY;
				 obj_speedF =  SABAKU_F*(3-bgdata_wspeed)+SABAKU_F;
				 break;

	}

 
}
/***************************************************************************************************
						    Hat Good By
***************************************************************************************************/
static void itemhat_Goodby(void)
{

	switch (obj_mode) {
		case 10: if (obj_timer < 10) { 						//no Dead
				    obj_animepositionY += -1;
					obj_animeangleX = 0x2000;
				 }
				 break;

		case 11: 											// Water tag & no Dead
				 if (obj_timer < 10) { 
					obj_animepositionY += -3;
				 }
				 obj_animeangleX = 0x2000;
				 break;			 

		case 12: obj_animepositionY += -1;							// Dead
				 if (obj_timer > 20) { execstp->status = 0; }
				 break;

		case 13: 											//Watertag & Dead
				 obj_animepositionY +=  -6;
				 if (obj_timer > 20) { execstp->status = 0; }
				 obj_animeangleX = 0x2000;
				 break;
	}


	HatLostCheck();

}
/***************************************************************************************************
						    Initial
***************************************************************************************************/
extern void s_itemhat_wing_init(void)
{

	execstp->s[stw_gravity].f   = 1.2;
	execstp->s[stw_friction].f  = 0.999;
	execstp->s[stw_specificG].f = 0.9;

	obj_alpha = 255;
}

/***************************************************************************************************
						    Hat Scale Event
***************************************************************************************************/
static void hat_ScaleEvent(void)
{
	hat_scale += 0x2000;

	execstp->map.scale[1] = cos(hat_scale)*0.3 + 0.7;

	if (hat_scale == 0x10000) {
		hat_scale = 0;
		hat_scale_flag = 2;
	}
}
/***************************************************************************************************
						 Wing hat Move
***************************************************************************************************/
extern void itemhat_wing_Move(void)
{

	short move_flag;

	obj_animeangleY += obj_speedF*0x80;

	if ((move_flag = ObjMoveEvent()) & 0x0001) {
		HatLostCheck();
		if (obj_speedY != 0) {
			hat_scale_flag = 1;
			obj_speedY = 0;
		}
	}

	if (hat_scale_flag == 1) hat_ScaleEvent();

}
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_itemhat_wing_main(void)
{

	switch (obj_mode) {
		case 0: itemhat_wing_Move();
				break;

		default : ObjMoveEvent();
				  itemhat_Goodby();
				  break;


	}
	
	if (obj_timer > 20) { s_hitON(); }

	hat_RemoveCheck();
	hat_HitCheckEvent();

}
/*-------------------------------------------------------------------------------------------------*/
/***************************************************************************************************
						    Initial
***************************************************************************************************/
extern void s_itemhat_metal_init(void)
{

	execstp->s[stw_gravity].f   = 2.4;
	execstp->s[stw_friction].f  = 0.999;
   	execstp->s[stw_specificG].f = 1.5;

//	obj_angleY = player_angleY;
//	obj_speedY = 15.;
//	obj_speedF = 20;

	obj_alpha = 255;

}

/***************************************************************************************************
						    Move
***************************************************************************************************/
extern void itemhat_metal_Move(void)
{

	short move_flag;

	obj_animeangleY += obj_speedF*0x80;

	if ((move_flag = ObjMoveEvent()) & 0x0001) {
		HatLostCheck();


	}


}

/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_itemhat_metal_main(void)
{

	switch (obj_mode) {
		case 0: itemhat_metal_Move();
				break;

		default : ObjMoveEvent();
				  itemhat_Goodby();
				  break;


	}

	if (obj_timer > 20) { s_hitON(); }

	hat_HitCheckEvent();
	hat_RemoveCheck();

}
/*-------------------------------------------------------------------------------------------------*/
/*							Item hat 	Blow														*/
/*-------------------------------------------------------------------------------------------------*/
/***************************************************************************************************
						    Initial
***************************************************************************************************/
extern void s_itemhat_blow_init(void)
{

	execstp->s[stw_gravity].f   = 0.7;
	execstp->s[stw_friction].f  = 0.89;
	execstp->s[stw_specificG].f = 0.9;


	obj_alpha = 255;

	BuSetHatPosition( (short)obj_worldX,(short)obj_worldY,(short)obj_worldZ);

}

/***************************************************************************************************
						   Lost Event
***************************************************************************************************/
static void hat_LostEvent(void)
{

	BuClrItemFlag(BU_ITEM_LOSTHAT);			// No Back Up
	
	switch (activeCourseNo) {
		case 8:		BuSetItemFlag(BU_ITEM_CONDORHAT);
					break;

		case 10:	BuSetItemFlag(BU_ITEM_DARUMAHAT);
					break;

		case 12:	BuSetItemFlag(BU_ITEM_MONKEYHAT);
					break;

		default:	BuSetItemFlag(BU_ITEM_CONDORHAT);
					break;
	}

}
/***************************************************************************************************
						    Move
***************************************************************************************************/
extern void itemhat_blow_Move(void)
{

	short move_flag;

	obj_animeangleY += obj_speedF*0x80;
	obj_animeangleX += obj_speedF*0x50;

	if ((move_flag = ObjMoveEvent()) & 0x0001) {
		HatLostCheck();
		if (obj_speedY != 0) {
			hat_scale_flag = 1;
			obj_speedY = 0;
			obj_animeangleX = 0;
		}
	}

	if (hat_scale_flag == 1) hat_ScaleEvent();

	

}
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_itemhat_blow_main(void)
{


	switch (obj_mode) {
		case 0: itemhat_blow_Move();
				break;

		default : ObjMoveEvent();
				  itemhat_Goodby();
				  break;


	}

	if ((int)obj_speedF != 0) { 
		BuSetHatPosition( (short)obj_worldX,(short)obj_worldY,(short)obj_worldZ);
	}
	
	if (execstp->status == 0) hat_LostEvent();

	if (hat_HitCheckEvent()==1) {
		BuClrItemFlag(BU_ITEM_LOSTHAT);			// No Back Up
	}
}


/*-------------------------------------------------------------------------------------------------*/
/*							Item hat 	Erase	    												*/
/*-------------------------------------------------------------------------------------------------*/
/***************************************************************************************************
						    Initial
***************************************************************************************************/
extern void s_itemhat_erase_init(void)
{

	execstp->s[stw_gravity].f   = 1.2;
	execstp->s[stw_friction].f  = 0.999;
	execstp->s[stw_specificG].f = 0.9;

	obj_alpha = 150;


}



/*################*/
#endif
/*################*/


