/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 22 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage22


/* ================================================================================
		: 22 - 1 [ Motos ] BG parts sequence.
===================================================================================	*/
SEQ_Stage2201_BGparts:

		seqActor(S_bg01,   3840,  0,-5631,   0,  0,0,   0,0,0,   e_stop		)
		seqActor(S_bg02,   4992,  0, -639,   0,  0,0,   0,0,0,   e_stop		)
		seqActor(S_bg03,   7168,  0, 1408,   0,  0,0,   0,0,0,   e_stop		)
		seqActor(S_bg04,      0,  0, 3712,   0,  0,0,   0,0,0,   e_stop		)
		seqActor(S_bg05,  -3199,  0, 3456,   0,  0,0,   0,0,0,   e_stop		)
		seqActor(S_bg06,  -5119,  0,-2047,   0,  0,0,   0,0,0,   e_stop		)
		seqActor(S_bg08,      0,  0,    0,   0,  0,0,   0,0,0,   e_stop		)
		seqActor(S_bg09,      0,  0, 6272,   0,  0,0,   0,0,0,   e_stop		)
		seqActor(S_bg10,   5632,  0, 1408,   0,270,0,   0,0,0,   e_stop		)
		seqActor(S_bg10,   2048,  0, 3456,   0,180,0,   0,0,0,   e_stop		)
		seqActor(S_bg10,  -4607,  0, 3456,   0,270,0,   0,0,0,   e_stop		)
		seqActor(S_bg10,  -5119,  0, -511,   0,  0,0,   0,0,0,   e_stop		)
		seqActor(S_bg11,      0,  0,-2047,   0,  0,0,   0,0,0,   e_stop		)
		seqActor(S_bg12,  -5115,300,-3200,   0, 90,0,   0,0,0,   e_patch_0	)

		seqReturn


/* ================================================================================
		: 22 - 1 [ Motos ] Move BG parts sequence.
===================================================================================	*/
SEQ_Stage2201_MoveBGs:

		seqActor(S_motos_obj03,  -1919, 307, 3648,   0, 0,0,   0,0,0,   e_motos_bridge1	 )
		seqActor(S_motos_obj05,  -5119, 307,-4095,   0, 0,0,   0,0,0,   e_mapobj_motos_05)
		seqActor(S_motos_obj08,      0,	  0,	0,   0, 0,0,   0,0,0,   e_mapobj_motos_08)
		seqActor(S_motos_obj09,   3968,   0, 1408,   0,90,0,   0,0,0,   e_mapobj_motos09 )
		seqActor(S_motos_obj09,  -5759,   0, 3072,   0, 0,0,   0,0,0,   e_mapobj_motos09 )
		seqActor(S_motos_obj09,   2816,   0,  512,   0,90,0,   0,0,0,   e_mapobj_motos09 )
		seqActor(S_motos_obj09,  -1791,   0,-4095,   0,90,0,   0,0,0,   e_mapobj_motos09 )
		seqActor(S_motos_obj10,   3840,   0,-3199,   0, 0,0,   0,0,0,   e_mapobj_motos10 ) 
		seqActor(S_motos_obj11,    922,-153, 2150,   0, 0,0,   0,0,0,   e_mapobj_motos11 )
		seqActor(S_motos_obj11,   1741,-153, 1741,   0, 0,0,   0,0,0,   e_mapobj_motos11 )
		seqActor(S_motos_obj11,   1741,-153, 2560,   0, 0,0,   0,0,0,   e_mapobj_motos11 )
		seqActor(S_motos_obj11,   2099,-153, -306,   0, 0,0,   0,0,0,   e_mapobj_motos11 )
		seqActor(S_NULL,		 -5119, 102, 1024,   0, 0,0,   0,0,0,   e_mapobj_motos12 )

		seqReturn


/* ================================================================================
		: 22 - 1 [ Motos ] New Move BG parts sequence.
===================================================================================	*/
SEQ_Stage2201_NewMoveBGs:

		seqActor(S_motos_obj01,   1124,  0,-4607,   0, 0,0,   0,0,0,   e_mapobj_motos_01	)	/*	Moving Kanaami.					*/
		seqActor(S_motos_obj01,   7168,  0, 2432,   0, 0,0,   0,1,0,   e_mapobj_motos_01 	)	/*		  "							*/
		seqActor(S_movebg02	  ,   7168,  0, 7296,   0, 0,0,   0,0,0,   e_sizumu_yuka		)	/*	Up-down ground [ New ].			*/
		seqActor(S_movebg04	  ,   6144,307, 6016,   0,90,0,   0,0,0,   e_fm_maruta			)	/*	Maruta		   [ New ].			*/
		seqActor(S_bg07		  ,  -5119,  0,-4095,   0, 0,0,   0,0,0,   e_rotate_record		)	/*	Base of the rotate fire bar.	*/
		seqActor(S_NULL		  ,  -3583,  0,-4095,   0, 0,0,   0,0,0,   e_mapobj_motos_06	)	/*	Up-down bridge.					*/
/*		seqActor(S_NULL		  , 	 0,154,-5631,   0, 0,0,   0,0,0,   e_mapobj_motos_07	)		Broken  bridge.					*/

		seqReturn


/* ================================================================================
		: 22 - 1 [ Motos ] Enemies sequence.
===================================================================================	*/
SEQ_Stage2201_Enemies:

		seqActor(S_NULL, 	   -3199,307, 3456,   0,  0,0,   0,0,0,		e_balloon		)
		seqActor(S_big_otos,       0,307,-4385,   0,  0,0,   0,0,0,		e_big_otos		)
		seqActor(S_big_otos,    4046,2234,-5521,  0,  0,0,   1,0,0,		e_big_otos2		)
		seqActor(S_otos,       -5119,307,-2482,	  0,  0,0,	 0,0,0,		e_otos			)
		seqActor(S_otos,	       0,307, 3712,	  0,  0,0,	 0,0,0,		e_otos			)
		seqActor(S_otos,   		6813,307, 1613,	  0,  0,0,	 0,0,0,		e_otos			)
		seqActor(S_otos,   		7168,307,  998,	  0,  0,0,	 0,0,0,		e_otos			)
		seqActor(S_otos,	   -5130,285,-1663,	  0,  0,0,	 0,0,0,		e_otos			)
		seqActor(S_NULL,   		   0,200,-2048,   0,  0,0,   0,0,0,  	e_firebigbar	)
		seqActor(S_NULL,  		 500,  2, 5000,   0,270,0,   0,0,0,  	e_firebubble_1	)
		seqActor(S_NULL, 		-700,  2, 4500,   0, 90,0,   0,0,0,  	e_firebubble_1	)
		seqActor(S_NULL,  	   -6300,  2, 2625,   0, 90,0,   0,0,0,  	e_firebubble_1	)
		seqActor(S_NULL,  	   -3280,  2,-4854,   0,  0,0,   0,0,0,	 	e_firebubble_1	)
		seqActor(S_NULL,   		5996,  2, -390,   0,315,0,   0,0,0,  	e_firebubble_1	)
		seqActor(S_NULL,   		5423,  2,-1991,   0,315,0,   0,0,0,  	e_firebubble_1	)
		seqActor(S_NULL,   		4921,  2,-1504,   0, 90,0,   0,0,0,  	e_firebubble_1	)

		seqLevelActor(0x0030,S_itembox, 1050, 550, 6200,  0,0,0,  0,3,0,  e_itembox)
		seqReturn


/* ================================================================================
		: 22 - 1 [ Motos ] Stars sequence.
===================================================================================	*/
SEQ_Stage2201_Stars:

/*		seqLevelActor(0x003f,S_polystar, -1450,  500, -6700,  0,0,0,  0,0,0,  e_tripstar)		      yama			 		 */
/*		seqLevelActor(0x003f,S_polystar,  3850,  323, -5600,  0,0,0,  1,0,0,  e_tripstar)		   	  bigmotos       		 */
		seqLevelActor(0x003f,S_NULL,     -4400,  350,   250,  0,0,0,  2,0,0,  e_tripstar_getcoins) 	/* 8 coins on the puzzle. */
		seqLevelActor(0x003f,S_polystar,  3100,  400,  7900,  0,0,0,  3,0,0,  e_tripstar)		 	/* secret coin			 */

		seqReturn


/* ================================================================================
		: 22 - 2 [ In valcano ] Move BG parts sequence.
===================================================================================	*/
SEQ_Stage2202_MoveBGs:

		seqActor(S_NULL,   728,2606,-2754,   0,0,0,   0, 56,0,   e_bar)
		seqActor(S_NULL,  1043,2972,-2679,   0,0,0,   0, 78,0,   e_bar)
		seqActor(S_NULL,  1078,3078,-2269,   0,0,0,   0,102,0,   e_bar)
		seqActor(S_NULL,  1413,3222,-2190,   0,0,0,   0, 82,0,   e_bar)
		seqActor(S_NULL,   783,1126,  -47,   0,0,0,   0,102,0,   e_bar)
		seqActor(S_NULL,   662,2150,  708,   0,0,0,   0,102,0,   e_bar)

		seqActor(S_NULL,   2943, 476,   10,   0,270,0,   0,2,0,   e_firebigbar)
		seqActor(S_NULL,  -2759,2350,-1108,   0, 60,0,   0,2,0,   e_firebigbar)
		seqActor(S_NULL,  -2472,2350,-1605,   0, 60,0,   0,2,0,   e_firebigbar)

		seqActor(S_movebg30,     -485,1203,2933,  0,0,0,  0,0,0,  e_fm_battan)
		seqActor(S_motos_obj05,   417,2150, 583,  0,0,0,  0,0,0,  e_mapobj_motos_05)

		seqActor(S_commonlift,  -764,  0, 1664,  0,180,0,  8,128+(2*16)+5,0,  e_linelift)
		seqActor(S_commonlift,   184,980,-1366,  0,180,0,  8,128+(2*16)+6,0,  e_linelift)

		seqActor(S_NULL,  -26,103,-2649,  0,0,0,  0,0,0,  e_magumafalls)

		seqReturn


/* ================================================================================
		: 22 - 2 [ In valcano ] Stars sequence.
===================================================================================	*/
SEQ_Stage2202_Stars:

		seqLevelActor(0x003f,S_polystar, 2523, 3850, -901,  0,0,0,  4,0,0,  e_tripstar)		/* kazan 1 */
		seqLevelActor(0x003f,S_polystar, 1800, 3400, 1450,  0,0,0,  5,0,0,  e_tripstar)		/* kazan lift	*/

		seqReturn


/* ================================================================================
		: Stage22 main sequence.
===================================================================================	*/
SEQ_DoStage22:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1 , 	_GfxStage22SegmentRomStart,   _GfxStage22SegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE, 	_ALavaTextureSegmentRomStart, _ALavaTextureSegmentRomEnd)
	seqLoadPres(SEGMENT_BACK,  		_BackMotosSegmentRomStart,    _BackMotosSegmentRomEnd	)
	seqLoadPres(SEGMENT_WEATHER,    _WeatherSegmentRomStart,      _WeatherSegmentRomEnd		)
	seqLoadPres(SEGMENT_ENEMY1,  	_GfxEnemy1_bSegmentRomStart,  _GfxEnemy1_bSegmentRomEnd )
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_bSegmentRomStart,  _HmsEnemy1_bSegmentRomEnd )
	seqLoadPres(SEGMENT_ENEMY2,  	_GfxEnemy2_hSegmentRomStart,  _GfxEnemy2_hSegmentRomEnd )
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_hSegmentRomStart,  _HmsEnemy2_hSegmentRomEnd )
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()
		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_b)
		seqCall(SetEnemy2_h)

		seqHmsShape(S_bg01, RCP_HmsMotosBG01)
		seqHmsShape(S_bg02, RCP_HmsMotosBG02)
		seqHmsShape(S_bg03, RCP_HmsMotosBG03)
		seqHmsShape(S_bg04, RCP_HmsMotosBG04)
		seqHmsShape(S_bg05, RCP_HmsMotosBG05)
		seqHmsShape(S_bg06, RCP_HmsMotosBG06)
		seqHmsShape(S_bg08, RCP_HmsMotosBG08)
		seqHmsShape(S_bg09, RCP_HmsMotosBG09)
		seqHmsShape(S_bg10, RCP_HmsMotosBG10)
		seqHmsShape(S_bg11, RCP_HmsMotosBG11)
		seqHmsShape(S_bg12, RCP_HmsMotosBG12)

		seqHmsShape(S_motos_obj03, RCP_HmsMotosOBJ03)		/*	London bridge.			*/
		seqHmsShape(S_motos_obj05, RCP_HmsMotosOBJ05)		/*	Rotate firebar cannon.	*/
		seqHmsShape(S_motos_obj08, RCP_HmsMotosOBJ08)		/*	Mery gourand.			*/
		seqHmsShape(S_motos_obj09, RCP_HmsMotosOBJ09)		/*	Seesaw.					*/
		seqHmsShape(S_motos_obj10, RCP_HmsMotosOBJ10)		/*	Up down ground.			*/
		seqHmsShape(S_motos_obj11, RCP_HmsMotosOBJ11)		/*	Gura gura cone.			*/

		seqHmsShape(S_pazz00, RCP_HmsMotosOBJ12_00)			/*	15 puzzles.				*/
		seqHmsShape(S_pazz01, RCP_HmsMotosOBJ12_01)
		seqHmsShape(S_pazz02, RCP_HmsMotosOBJ12_02)
		seqHmsShape(S_pazz03, RCP_HmsMotosOBJ12_03)
		seqHmsShape(S_pazz04, RCP_HmsMotosOBJ12_04)
		seqHmsShape(S_pazz05, RCP_HmsMotosOBJ12_05)
		seqHmsShape(S_pazz06, RCP_HmsMotosOBJ12_06)
		seqHmsShape(S_pazz07, RCP_HmsMotosOBJ12_07)
		seqHmsShape(S_pazz08, RCP_HmsMotosOBJ12_08)
		seqHmsShape(S_pazz09, RCP_HmsMotosOBJ12_09)
		seqHmsShape(S_pazz10, RCP_HmsMotosOBJ12_10)
		seqHmsShape(S_pazz11, RCP_HmsMotosOBJ12_11)
		seqHmsShape(S_pazz12, RCP_HmsMotosOBJ12_12)
		seqHmsShape(S_pazz13, RCP_HmsMotosOBJ12_13)

		seqHmsShape(S_motos_obj01	  , RCP_HmsMotosOBJ01		)		/*	Moving Kanaami.					*/
		seqHmsShape(S_movebg02		  , RCP_HmsMotosOBJ02		)		/*	Up-down ground [ New ].			*/
		seqHmsShape(S_movebg04		  , RCP_HmsMotosOBJ04		)		/*	Maruta		   [ New ].			*/
		seqHmsShape(S_bg07			  , RCP_HmsMotosBG07		)		/*	Base of the rotate fire bar.	*/
		seqHmsShape(S_motos_obj06_near, RCP_HmsMotosOBJ06_Near	)		/*	Up-down bridge [ Near ].		*/
		seqHmsShape(S_motos_obj06_far , RCP_HmsMotosOBJ06_Far	)		/*			"	   [ Far  ].		*/
		seqHmsShape(S_motos_obj07_near, RCP_HmsMotosOBJ07_Near	)		/*	Broken bridge. [ Near ].		*/
		seqHmsShape(S_motos_obj07_far , RCP_HmsMotosOBJ07_Far	)		/*			"	   [ Far  ].		*/

		seqHmsShape(S_movebg30, RCP_HmsFmBattan)

		seqBeginScene(1, RCP_Stage22Scene1)
			seqActor(S_NULL,  -3839,154+START_H,6272,  0,  90,0,   0,10,0,  e_player_entpict)
			seqActor(S_NULL,      0,		105,   0,  0,   0,0,  25,11,0,  e_tripchimney2	)
			seqActor(S_NULL,  -3200,		 11,3456,  0,-100,0,   0,12,0,  e_warp)
			seqActor(S_NULL,  -5888,		154,6656,  0, 100,0,   0,13,0,  e_warp)
			seqPort   (10, 22, 1, 10)		/*	MARIO stage in.				*/
			seqMidPort(11, 22, 2, 10)		/*	To scene2 [ In valcano ].	*/
			seqPort   (12, 22, 1, 13)		/*	Warp.						*/
			seqPort   (13, 22, 1, 12)		/*	Warp.						*/
			seqGameClear(6, 3,  50)
			seqGameOver (6, 3, 100)
			seqCall(SEQ_Stage2201_BGparts)
			seqCall(SEQ_Stage2201_MoveBGs)
			seqCall(SEQ_Stage2201_NewMoveBGs)
			seqCall(SEQ_Stage2201_Enemies)
			seqCall(SEQ_Stage2201_Stars)
			seqMapInfo(cx2201_info)
			seqTagInfo(cx2201_info_tag)	
			seqMessage(SEQ_MESG_ENTRANT, 97)
			seqSetMusic(NA_STG_GROUND, NA_MOTOS_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

		seqBeginScene(2, RCP_Stage22Scene2)
			seqActor(S_NULL,  -955,103+START_H,-1029,  0,118,0,  0,10,0,  e_player_landing)
			seqPort(10, 22, 2, 10)
			seqGameClear(6, 3,  50)
			seqGameOver (6, 3, 100)
			seqCall(SEQ_Stage2202_MoveBGs)
			seqCall(SEQ_Stage2202_Stars)
			seqMapInfo(cx2202_info)
			seqTagInfo(cx2202_info_tag)
			seqSetMusic(NA_STG_DUNGEON, NA_MOTOS_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 90,  -3839,154,6272)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
