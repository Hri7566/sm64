/********************************************************************************
	anime.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	May 9, 1996
 ********************************************************************************/

#ifndef ANIME_H
#define	ANIME_H

extern char _MarioAnimeSegmentRomStart[];
extern char _MarioAnimeSegmentRomEnd[];

enum {
	amAscend,
	amBackDown,
	amJumpBackDown,
	amBackDownEnd,
	amBackJumping,
	amBarClimb,
	amBarHang,
	amBarJumping,
	amBarJumpEnd,
	amBarStanding,
	amBarStandJump,
	amBarStandStart,
	amBarStandEnd,
	amBarWaiting,
	amBaseData,
	amBraking,
	amBrakeEnd,
	amBroadJumpEnd1,
	amBroadJumpEnd2,
	amBroadJumping1,
	amBroadJumping2,
	amCannonFly,
	amCarryJogging,
	amCarryRunning,
	amCarryWalking,
	amColdWaitStart,
	amColdWaitEnd,
	amColdWaiting,
	amDescend,
	amEndingByeBye,
	amEndingHatWait,
	amEndingLookDown,
	amEndingLookUp1,
	amEndingLookUp2,
	amEndingPutOff,
	amEndingWalkA,
	amEndingWalkB,
	amEndingWinFlyA,
	amEndingWinFlyB,
	amEndingYaaha,
	amFireJumpEnd,
	amFireJumping,
	amFlight,
	amFlying,
	amForeDown,
	amJumpForeDown,
	amForeDownEnd,
	amGasDowning,
	amGasWaiting,
	amGetDoorKey,
	amGiddyDown,
	amHanging,
	amHangJump,
	amHangRoof,
	amHatTaking,
	amHatWaiting,
	amHatWaitEnd,
	amHeadDowning,
	amHipAttackEnd,
	amHipAttackFly,
	amHipAttackStart,
	amHipAttacking,
	amHipDowning,
	amHolding,
	amHoldJumpEnd,
	amHoldJumping,
	amHoldLandEnd,
	amHoldLanding,
	amHoldSlipLanding,
	amHoldSlipping,
	amHoldSlipEnd,
	amBoardJumpend,
	amJogging,
	amJumpBack,
	amBoardJumping,
	amJumpEnd2,
	amJumpFall,
	amJumping,
	amJumpEnd,
	amJumpKick,
	amJumpStep2,
	amJumpStep3,
	amJumpThrow,
	amKickLanding,
	amLandBoard,
	amLandDowning,
	amLanding,
	amLandEnd,
	amLifted,
	amLifting,
	amLostStandup,
	amMantFlying,
	amMoveRoofL,
	amMoveRoofR,
	amNoHatWaiting,
	amOpenDoor1,
	amOpenDoor2,
	amOpenKeyDoor,
	amOshinWaiting1,
	amOshinWaiting2,
	amOshinWaiting3,
	amPitching,
	amPowerKick,
	amPunchBase,
	amSecPunchBase,
	amPunchEnd,
	amSecPunchEnd,
	amPunchTake,
	amPushing,
	amPutonBoard,
	amPutting,
	amRolling,
	amBackRolling,
	amRollingKick,
	amRunning,
	amTurnCont,
	amSafeBackDown,
	amSafeForeDown,
	amSandDowning,
	amSandWaiting,
	amSandWalking,
	amShockDowning,
	amShocking,
	amShortBackDown,
	amShortForeDown,
	amShoulder,
	amSideWait,
	amSideWalkL,
	amSideWalkR,
	amSleeping1,
	amSleeping2,
	amSleeping3,
	amSleeping4,
	amSleeping5,
	amSleeping6,
	amSleeping7,
	amSlideCatch,
	amSlipBack,
	amSlideDown,
	amSlideStandup,
	amSlidingKick,
	amSlidingKickEnd,
	amSlip,
	amSlipEnd,
	amSlipLanding,
	amSlipping,
	amSoftStep,
	amSpinJpEnd,
	amSpinJumping,
	amSpinJpStart,
	amSquatEnd,
	amSquatStart,
	amSquatWaiting,
	amSquatWalking,
	amSquatWalkEnd,
	amSquatWalkStart,
	amStarDoorA,
	amStarDoorB,
	amSwimBackDown,
	amSwimCarryA1,
	amSwimCarryA2,
	amSwimCarryB,
	amSwimCarryStopA,
	amSwimCarryStopB,
	amSwimCarryWait,
	amSwimDown1,
	amSwimDown2,
	amSwimDownEnd,
	amSwimForeDown,
	amSwimLanding,
	amSwimming1,
	amSwimming2,
	amSwimming3,
	amSwimStop,
	amSwimTakeEnd,
	amSwimTakeMiss,
	amSwimTaking,
	amSwimThrow,
	amSwimWait,
	amSwimWinDemo,
	amSwimWinEnd,
	amSwingStart,
	amSwingAttack,
	amSwingDown,
	amSwingWait,
	amThrowing,
	amTiredWaiting,
	amTransfer,
	amTurning,
	amTurnEnd,
	amUJumpEnd,
	amUJumping,
	amUltraJumpEnd,
	amUltraJumping,
	amViewing,
	amWaiting1,
	amWaiting2,
	amWaiting3,
	amWaitRoofL,
	amWaitRoofR,
	amWakeup,
	amWakeup2,
	amWalking,
	amWallJump,
	amWallStay,
	amWinDemoA,
	amWinDemoAEnd,
	amWingJumpEnd,
	amWingJumping
};

#endif
