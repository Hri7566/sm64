/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage10 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx gfx_sf2[]	 	;
extern Gfx gfx_sf2_p1[]	 	;
extern Gfx gfx_sf2_p2[]	 	;
extern Gfx gfx_sf2_p3[]	 	;
extern Gfx gfx_sf2_p4[]	 	;
extern Gfx gfx_sf2_p5[]	 	;
extern Gfx gfx_sf2_p6[]		;
extern Gfx gfx_sf2_nami[]	;
extern Gfx gfx_sf2_hip[]	;
extern Gfx gfx_sf2_hahen[]	;

extern Gfx gfx_sf2_kama1[];
extern Gfx gfx_sf2_kama2[];
extern Gfx gfx_sf2_kama3[];
extern Gfx gfx_sf2_kama4[];


/* ===============================================================================
        : Hierarchy map data of Sf2Hip.
================================================================================== */
Hierarchy RCP_HmsSf2Hip[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_XSURF, gfx_sf2_hip)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Sf2Hahen.
================================================================================== */
Hierarchy RCP_HmsSf2Hahen[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_XSURF, gfx_sf2_hahen)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Sf2Nami.
================================================================================== */
Hierarchy RCP_HmsSf2Nami[] = {
    hmsHeader(350)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_sf2_nami)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1 [ Yukiyama2 ].										*/
/********************************************************************************/
Hierarchy RCP_Stage10Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(4, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, 20000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON_O,  0,2000,6000,  0,4400,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF  , gfx_sf2)
					hmsGfx(RM_XSURF , gfx_sf2_p1)
					hmsGfx(RM_SURF  , gfx_sf2_p2)
					hmsGfx(RM_SPRITE, gfx_sf2_p3)
					hmsGfx(RM_XSURF , gfx_sf2_p4)
					hmsGfx(RM_XDECAL, gfx_sf2_p5)
					hmsGfx(RM_SURF  , gfx_sf2_p6)

					hmsCProg(0	   , WaterInit)
					hmsCProg(0x1001, WaterDraw)

					hmsObject()
					hmsCProg(1, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

		hmsLayer(0)
		hmsBegin()
			hmsCProg(0, CannonEdge)
		hmsEnd()

	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 2 [ In kamakura ].										*/
/********************************************************************************/
Hierarchy RCP_Stage10Scene2[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0,0,0,1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF , gfx_sf2_kama1)
					hmsGfx(RM_SURF , gfx_sf2_kama2)
					hmsGfx(RM_XSURF, gfx_sf2_kama3)
					hmsGfx(RM_XSURF, gfx_sf2_kama4)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

	hmsEnd()
	hmsExit()
};
