/********************************************************************************
					-------------------------
					 Ultra 64 MARIO Brothers
   					-------------------------

					File		: yextern.h
					Description	: Function Prototypes
					Date		: 1995.4.5
					Author		: H.yajima

 ********************************************************************************/

#include "ext/cfile.ext"
#include "ext/pathdata.ext" 

extern ulong frameCounter;

extern unsigned long	emode_kame_wait[];
extern unsigned long	emode_kame_catch[];
extern unsigned long	emode_kame_drop[];
extern unsigned long	emode_kame_throw[];
extern unsigned long	emode_kame_next[];

extern unsigned long	e_falls[];

extern short	kopadata[];


/*==============================================================================*/
/*				End End End End End End											*/
/*==============================================================================*/



