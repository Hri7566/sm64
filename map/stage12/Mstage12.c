/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage12 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

#define		STAGE12_CLEAR_R		 9
#define		STAGE12_CLEAR_G		22
#define		STAGE12_CLEAR_B		28

extern Gfx gfx_tin_a[]		;
extern Gfx gfx_tin_b[]		;
extern Gfx gfx_tin_c[]		;
extern Gfx gfx_tin_d[]		;
extern Gfx gfx_tin_e[]		;
extern Gfx gfx_tin_f[]		;
extern Gfx gfx_tin_g[]		;
extern Gfx gfx_tin_iwa[]	;
extern Gfx gfx_tin_ita[]	;
extern Gfx gfx_kibako[]		;
extern Gfx gfx_fune_fl_opa[];
extern Gfx gfx_fune_fl_xlu[];
extern Gfx gfx_fune_bl_opa[];
extern Gfx gfx_fune_bl_xlu[];
extern Gfx gfx_fune_fr[]	;
extern Gfx gfx_fune_br[]	;
extern Gfx gfx_hasira[]		;
extern Gfx gfx_hasiradai[]	;

extern Gfx gfx_miyafune[]	 ;
extern Gfx gfx_miyafune_alp[];
extern Gfx gfx_miyafune_uo[] ;


/* ===============================================================================
        : Hierarchy map data of WdHasira.
================================================================================== */
Hierarchy RCP_HmsWdHasira[] = {
    hmsHeader(1600)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_hasira)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of WdHasiradai.
================================================================================== */
Hierarchy RCP_HmsWdHasiradai[] = {
    hmsHeader(300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_hasiradai)
    hmsEnd()
    hmsExit()
};


/* ===============================================================================
        : Hierarchy map data of HmsTinIwa.
================================================================================== */
Hierarchy RCP_HmsTinIwa[] = {
    hmsHeader(1100)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_tin_iwa)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of HmsTinIta.
================================================================================== */
Hierarchy RCP_HmsHmsTinIta[] = {
    hmsHeader(900)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tin_ita)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of WDKibako.
================================================================================== */
Hierarchy RCP_HmsWDKibako[] = {
    hmsHeader(300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kibako)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Fune_FrontLeft_opa.
================================================================================== */
Hierarchy RCP_HmsFune_FrontLeft_opa[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_fune_fl_opa)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Fune_FrontLeft_xlu.
================================================================================== */
Hierarchy RCP_HmsFune_FrontLeft_xlu[] = {
    hmsHeader(5000)
    hmsBegin()
		hmsCProg(0, AlphaControl)
        hmsGfx(RM_XSURF, gfx_fune_fl_xlu)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Fune_BackLeft_opa.
================================================================================== */
Hierarchy RCP_HmsFune_BackLeft_opa[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_fune_bl_opa)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Fune_BackLeft_xlu.
================================================================================== */
Hierarchy RCP_HmsFune_BackLeft_xlu[] = {
    hmsHeader(5000)
    hmsBegin()
		hmsCProg(0, AlphaControl)
        hmsGfx(RM_XSURF, gfx_fune_bl_xlu)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Fune_FrontRight.
================================================================================== */
Hierarchy RCP_HmsFune_FrontRight[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_fune_fr)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Fune_BackRight.
================================================================================== */
Hierarchy RCP_HmsFune_BackRight[] = {
    hmsHeader(5000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_fune_br)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage12Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(8, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, 25000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON_O,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SPRITE, gfx_tin_a)
					hmsGfx(RM_SURF  , gfx_tin_b)
					hmsGfx(RM_SURF  , gfx_tin_c)
					hmsGfx(RM_SURF  , gfx_tin_d)
					hmsGfx(RM_SURF  , gfx_tin_e)
					hmsGfx(RM_SURF  , gfx_tin_f)
					hmsGfx(RM_SPRITE, gfx_tin_g)
					hmsCProg(0	   , WaterInit)
					hmsCProg(0x1201, WaterDraw)
					hmsCProg(0x1205, WaterDraw)
					hmsObject()
					hmsCProg(14, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

		hmsLayer(0)
		hmsBegin()
			hmsCProg(0, CannonEdge)
		hmsEnd()

	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 2.														*/
/********************************************************************************/
Hierarchy RCP_Stage12Scene2[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0,0,0,1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, 10000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON_O,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF  , gfx_miyafune)
					hmsGfx(RM_SPRITE, gfx_miyafune_alp)
					hmsGfx(RM_SURF  , gfx_miyafune_uo)
					hmsCProg(0	   , WaterInit)
					hmsCProg(0x1202, WaterDraw)
					hmsObject()
					hmsCProg(2, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

	hmsEnd()
	hmsExit()
};
