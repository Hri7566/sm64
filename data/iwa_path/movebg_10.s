/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					 stage10 MoveBg Strategy

				April 2 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
						Path Data
 ********************************************************************************/
e_iwanami_parts:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON)
	p_setshapeinfo(sf2_nami_check_info)
	p_save_nowpos

	p_while
		p_program(s_iwanami_parts_main)
		p_program(stMainMoveBG)
	p_loop

e_iwanami:
	p_initialize(option)

	p_while
		p_program(s_iwanami_main)
	p_loop



/*################*/
#else
/*################*/


/***************************************************************************************************
		C Program
 ****************************************************************************************************/
#define S_iwanami	S_movebg01

/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_iwanami_parts_main(void)
{

	switch (obj_mode) {
		case 0:	obj_speedX = -40;
				obj_worldX += obj_speedX;
				if(obj_timer > 117) {
					obj_mode = 1;
				}
				objsound_level(NA_LSE2_TRIANGLE_SLIDE);
				break;
				
		case 1:	obj_speedX = -5;
				obj_worldX += obj_speedX;
				obj_speedY = -10;
				obj_worldY += obj_speedY;
				obj_worldZ = obj_attZ - 2;
				if(obj_timer > 50) {
					execstp->status = 0;
				}
				break;
	}

}
/***************************************************************************************************
						    Gush
***************************************************************************************************/
extern void s_iwanami_main(void)
{
	StrategyRecord *stp;

	if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,6000)==0 || (obj_worldY+1000 < player_posY) ) {
		return;
	}


	if (obj_timer==64 || obj_timer==128 || obj_timer==192 || obj_timer==224 || obj_timer==256) {	
		stp = s_makeobj_nowpos(execstp,S_iwanami,e_iwanami_parts);
	}


	if (obj_timer == 256) {	
		stp->map.scale[0] = 2.0;
		stp->map.scale[1] = 2.0;
	}

	if (obj_timer >= 256) obj_timer = 0;

}



/*################*/
#endif
/*################*/


