/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Yoshi Strategy

					May 9 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/*==============================================================================
						Path Data
 ===============================================================================*/
e_yoshi:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON| stf_YangleSAME | stf_playerangleON)
	p_set_pointer(skelanime,yoshi_anime)
	p_setobjname(FRIEND)
	p_BGcheckYset
	p_sethitbox(160,150)
	p_set_skelanime_number(0)
	p_save_nowpos
	p_program(s_yoshi_init)

	p_while
		p_hitON
		p_program(s_yoshi_main)
	p_loop



/*################*/
#else
/*################*/


/*==================================================================================================
		C Program
 ===================================================================================================*/
#define yoshi_eyetimer  (execstp->s[stw_work0].d)
#define yoshi_mess_flag	(execstp->s[stw_work1].d)
#define yoshi_ral_flag	(execstp->s[stw_work2].d)
#define yoshi_tangle	(execstp->s[stw_work3].d)


short yoshi_rail[] = {
	    0, -5625,
	-1364, -5912,
	-1403, -4609,
	-1004, -5308,
};

/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_yoshi_init(void)
{
	execstp->s[stw_gravity].f   = 2.0;
	execstp->s[stw_friction].f  = 0.9;
	execstp->s[stw_specificG].f = 1.3;

	execstp->s[stw_enemyinfo].d = ENEMYINFO_MESSAGE;


	if ((BuGetTotalStars(activePlayerNo-1) < 120)|| yoshi_destFlag == 1) {
		execstp->status = 0;
	}

}
/***************************************************************************************************
						    Main
***************************************************************************************************/
static  void yoshi_WalkEvent(void)
{
	short bg_flag;
	short anim_frame = execstp->map.skelanim.frame;
	
	obj_speedF = 10;
	bg_flag = ObjMoveEvent();	

	obj_angleY = s_chase_angle(obj_angleY,yoshi_tangle,0x500);

	if (ObjApproach(execstp,obj_attX,3174,obj_attZ,200)) {
		obj_mode = 0;
	}

	stSetSkelAnimeNumber(1);
	if (anim_frame == 0 || anim_frame == 15) objsound(NA_SE2_YOSSY_WALK);

	if (execstp->s[stw_mail].d == EMAIL_PLAYERHIT) obj_mode = 2;


	if (obj_worldY < 2100) { 
		Obj_reset(S_yoshi,e_yoshi,3000); 
		execstp->status = 0; 
	}
}
/***************************************************************************************************
						    Main
***************************************************************************************************/
static  void yoshi_WaitEvent(void)
{
	short flag;
	short anim_frame = execstp->map.skelanim.frame;


	if (obj_timer > FRAME*3) {
		flag = (short) (Randomf()*3.99);
		if (flag == yoshi_ral_flag) return;
		else	yoshi_ral_flag = flag;

		obj_attX = yoshi_rail[yoshi_ral_flag*2];
		obj_attZ = yoshi_rail[yoshi_ral_flag*2+1];
		yoshi_tangle = atan(obj_attZ - obj_worldZ,obj_attX - obj_worldX);
		obj_mode = 1;
	}

	stSetSkelAnimeNumber(0);

	if (execstp->s[stw_mail].d == EMAIL_PLAYERHIT) obj_mode = 2;



	if (camPlayerInfo[0].demostatus == DEMO_ENDING_B || camPlayerInfo[0].demostatus == DEMO_SAYONARA) {
		obj_mode = 10;
		obj_worldX = -1798;
		obj_worldY =  3174;
		obj_worldZ = -3644;
	}

}
/***************************************************************************************************
						    Main
***************************************************************************************************/
static  void yoshi_MessageEvent(void)
{

	if ((short) obj_angleY == (short) obj_targetangle) {
		stSetSkelAnimeNumber(0);

		if (CtrlPlayerDialog(DLOG_LOOKFRONT)==DLOG_RESULT_READY) {
			Mbitset( execstp->status,OBJECT_DEMOMOVEENTRY );				//kaijo only me
			if (cameraDemoStratMsgNum(CAM_DEMO_TALK,execstp,161) != 0) {
				Mbitclr( execstp->status,OBJECT_DEMOMOVEENTRY );
				execstp->s[stw_mail].d = 0;

				obj_attX = yoshi_rail[2];
				obj_attZ = yoshi_rail[2+1];
				yoshi_tangle = atan(obj_attZ - obj_worldZ,obj_attX - obj_worldX);
				obj_mode = 5;
			}
		}
	}

	else {

		stSetSkelAnimeNumber(1);
		Na_NazoClearBgm();
		obj_angleY = s_chase_angle(obj_angleY,obj_targetangle,0x500);

	}


}
/***************************************************************************************************
						    Main
***************************************************************************************************/
static  void yoshi_GoHome(void)
{
	short anim_frame = execstp->map.skelanim.frame;

	obj_speedF = 10;
	ObjMoveEvent();	

	stSetSkelAnimeNumber(1);

	if (obj_timer == 0) cameraDemoStrat(CAM_DEMO_LOOKOBJ,execstp);

	obj_angleY = s_chase_angle(obj_angleY,yoshi_tangle,0x500);

	if (ObjApproach(execstp,obj_attX,3174,obj_attZ,200)) {
		stSetSkelAnimeNumber(2);
		objsound(NA_SE2_YOSSY_JUMP);
		obj_speedF = 50;
		obj_speedY = 40;
		obj_angleY = degree(-90);
		obj_mode = 4;
	}

	if (anim_frame == 0 || anim_frame == 15) objsound(NA_SE2_YOSSY_WALK);

}
/***************************************************************************************************
						    Main
***************************************************************************************************/
static  void yoshi_GoHome2(void)
{

	s_stop_animeend();

	ObjSpeedOn(execstp);
	obj_speedY -= 2.0;


	if (obj_worldY < 2100) {
		CtrlPlayerDialog(DLOG_DONE);
		demoseqcode = 1;				//CAMERA DEMO END
		yoshi_destFlag = 1;
		execstp->status = 0;
	}

}
/***************************************************************************************************
						    Main
***************************************************************************************************/
static  void yoshi_100up(void)
{
	ulong counter = frameCounter;

	if (playerMeter.life == 100) {
		Na_FixSeFlagEntry(NA_SE2_ONEUP);
		buYosshiJump = 1;
		obj_mode = 3;
		return;
	}

	if ((counter & 0x03)==0) {
		Na_FixSeFlagEntry(NA_SYS_COUNTUP);
		(marioWorks->nlifes)++;
	}


}
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_yoshi_main(void)
{

	switch (obj_mode) {

		case 0: yoshi_WaitEvent();
				break;

		case 1: yoshi_WalkEvent();
				break;

		case 2: yoshi_MessageEvent();
				break;

		case 3: yoshi_GoHome();
				break;

		case 4: yoshi_GoHome2();
				break;

		case 5: yoshi_100up();
				break;

		case 10: stSetSkelAnimeNumber(0);
				 break;


	}


	ObjEyeEvent(&yoshi_eyetimer);



}




/*################*/
#endif
/*################*/


