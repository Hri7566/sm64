/**********************************************************************
 *                                                                    *
 *  Application Data for model:                                       *
 *     ./newmario/nin/display/mario64_logo.nin                        *
 *                                                                    *
 *  THIS FILE WAS CREATED BY NINGEN                                   *
 *                                                                    *
 *                                                                    *
 **********************************************************************/


#include <mbi.h>
#include "model.h"

extern Vtx mario64_logo_v[];
extern Material mario64_logo_material[];
extern Mtx mario64_logo_matrix[];



extern Gfx mario64_logo_model[];
extern Gfx mario64_logo_grp_g27[];



static NinStruc mario64_logo_grp_g27_structure = {
	NULL,		/* matrix */ 
	0,	/* num children */
	NULL,	/* no children */
};

static NinDL mario64_logo_grp_g27_dlist = {
	713,
	mario64_logo_grp_g27,
	0,
	NULL,
};

static Nin mario64_logo_grp_g27_data = {
	NIN_GROUP,
	&mario64_logo_grp_g27_dlist,
	NULL,	/* no custom data */
	&mario64_logo_grp_g27_structure,
};

static Nin *mario64_logo_model_children[] = {
	&mario64_logo_grp_g27_data,
};

static NinStruc mario64_logo_model_structure = {
	NULL,		/* matrix */ 
	1,	/* num children */
	mario64_logo_model_children,
};

static NinDL mario64_logo_model_dlist = {
	2,
	mario64_logo_model,
	0,
	NULL,
};

Nin mario64_logo_model_data = {
	NIN_HEADER,
	&mario64_logo_model_dlist,
	NULL,	/* no custom data */
	&mario64_logo_model_structure,
};

float mario64_logo_box[][3] = {
	{ -2424.094530, -1995.015015, -819.200012 }, 
	{ 2484.239063, 2075.211182, 209.199772 } 
}; 
