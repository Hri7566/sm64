/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    NEWS lift Strategy

					March 18 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/*==============================================================================
						Path Data
 ===============================================================================*/
e_NEWSlift:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON  | stf_matrixON | stf_createON  )
	p_setshapeinfo( NEWSliftcheck_info)
	p_save_nowpos
	p_program(s_NEWSlift_init)

	p_while
		p_program(s_NEWSlift_main)
		p_program(stMainMoveBG)
	p_loop

e_NEWSlift_button:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON )
	p_setshapeinfo( NEWSbutcheck_info)

	p_while
		p_program(s_NEWSbut_main)
		p_program(stMainMoveBG)
	p_loop



/*################*/
#else
/*################*/


/*==================================================================================================
		C Program
 ===================================================================================================*/
#define S_NEWSlift		S_movebg01
#define S_NEWSbutton	S_movebg02

static char NEWSliftButton_flag = 0;

/*===========================      NEWSbutton        =====================================================*/
/***************************************************************************************************
						    Button Click
***************************************************************************************************/
static void NEWSbutton_ON(void)
{

	obj_skeletonY -= 4;

	if (obj_skeletonY < 41) {
		obj_skeletonY = 41;
		obj_mode = 2;
	}

}
/***************************************************************************************************
						    Button Click
***************************************************************************************************/
static void NEWSbutton_OFF(void)
{
	if (NEWSliftButton_flag == obj_programselect) return;

	obj_skeletonY += 4;

	if (obj_skeletonY > 51) {
		obj_skeletonY = 51;
		obj_mode = 0;
	}

}
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_NEWSbut_main(void)
{
	switch (obj_mode) {
		case 0:	if (obj_timer < FRAME) { break;  }
				if (player1stp->ride_strat == execstp) {
					NEWSliftButton_flag = obj_programselect;
                    MapDispShape(&((execstp->motherobj)->map));
					obj_mode = 1;
					objsound(NA_SE2_RAFTSWITCH);
				}
				break;

		case 1: NEWSbutton_ON();
				break;

		case 2: NEWSbutton_OFF();
				break;
	}

	obj_speedX = (execstp->motherobj)->s[stw_speedX].f;
	obj_speedZ = (execstp->motherobj)->s[stw_speedZ].f;

	if (motherp->status == 0) execstp->status = 0;	

//	s_set_scale(1.0);


}









/*===========================      NEWSlift        =====================================================*/
#define NEWS_ReflectBuf		(execstp->s[stw_work1].d)
#define NEWS_oftY			(execstp->s[stw_work2].f)
#define NEWS_flag			(execstp->s[stw_work3].d)


/***************************************************************************************************
						    Init
***************************************************************************************************/
extern void s_NEWSlift_init(void)
{
	StrategyRecord* stratp;

	stratp = s_makeobj_relative(execstp,S_NEWSbutton,e_NEWSlift_button,   0, 51,  204,     0,     0,0);
	stratp->s[stw_programselect].d = 1;

	stratp = s_makeobj_relative(execstp,S_NEWSbutton,e_NEWSlift_button,   0, 51, -204,     0,0x8000,0);
	stratp->s[stw_programselect].d = 2;

	stratp = s_makeobj_relative(execstp,S_NEWSbutton,e_NEWSlift_button, 204, 51,    0,     0,0x4000,0);
	stratp->s[stw_programselect].d = 3;

	stratp = s_makeobj_relative(execstp,S_NEWSbutton,e_NEWSlift_button,-204, 51,    0,     0,0xc000,0);
	stratp->s[stw_programselect].d = 4;


	NEWSliftButton_flag = 0;
	NEWS_oftY = obj_worldY;
}
/***************************************************************************************************
						Wall Check
***************************************************************************************************/
static void NEWSliftReflect(char code)
{
	NEWS_ReflectBuf = code;
	obj_timer = 0;
	NEWSliftButton_flag = 5;
	objsound(NA_SE2_RAFT_COLLISION);
	SendMotorEvent(50,80);		/* MOTOR 1997.6.2 */

}
/***************************************************************************************************
						Wall Check
***************************************************************************************************/
static void NEWSliftWallCheck(char code,char* codeNo,float* check1,float* check2,float* check3)
{
	if (codeNo[1] == 1 || (codeNo[0]==1 && codeNo[2]==1)) {
		NEWSliftReflect(code);
	}
	else {
		if (codeNo[0] ==1) {
			if ( ((code==1||code==2) && (int)check1[2]!=0) || ((code==3||code==4) && (int)check1[0]!=0) ){
				NEWSliftReflect(code);
			}
			else {
				obj_worldX += check1[0];
				obj_worldZ += check1[2];
			}
		}

		if (codeNo[2] == 1) {
			if ( ((code==1||code==2) && (int)check3[2]!=0) || ((code==3||code==4) && (int)check3[0]!=0) ){
				NEWSliftReflect(code);
			}
			else {
				obj_worldX += check3[0];
				obj_worldZ += check3[2];
			}
		}
	}


	if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,400)==0) { 
		NEWSliftButton_flag = 6;
		NEWS_flag = 1;
		obj_timer = 0;
	}

}
/***************************************************************************************************
						Wall Check
***************************************************************************************************/
static void NEWSlift_Shock(void)
{

	if (NEWS_ReflectBuf == 1 || NEWS_ReflectBuf == 2) {
		obj_animeangleX = sin(obj_timer*(0x1000) )*degree(10);
		obj_worldY      = NEWS_oftY + sin(obj_timer*(0x2000))*(20);
	}
	else {
		obj_animeangleZ = sin(obj_timer*(0x1000) )*degree(10);
		obj_worldY      = NEWS_oftY + sin(obj_timer*(0x2000))*(20);
	}



//	obj_animeangleX += obj_anglespeedX;
//	obj_animeangleZ += obj_anglespeedZ;

	if (obj_timer == 0x20) {
		NEWSliftButton_flag = NEWS_ReflectBuf;
		obj_animeangleX = 0;
		obj_animeangleZ = 0;
		obj_worldY      = NEWS_oftY;
	}

}
/***************************************************************************************************
						Wall Check
***************************************************************************************************/
static void NEWSlift_PlayerRideCheck(void)
{

	short p_posx = (player_posX - obj_worldX);
	short p_posz = (player_posZ - obj_worldZ);
	
	if (player1stp->ride_strat == execstp || player1stp->ride_strat == (StrategyRecord*) s_find_obj(e_NEWSlift_button) )	{
		obj_animeangleX =  p_posz*4;
		obj_animeangleZ = -p_posx*4;
		if (NEWSliftButton_flag == 6) {
			NEWSliftButton_flag = 0;
			obj_timer = 0;
			MapDispShape(&execstp->map);
		}
	}
	else {

	}

}
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_NEWSlift_main(void)
{
	char ch[3];
	float check1[3];
	float check2[3];
	float check3[3];

	obj_anglespeedZ = 0;
	obj_anglespeedX = 0;
	obj_speedX = 0;
	obj_speedZ = 0;

	switch (NEWSliftButton_flag) {
		case 0: obj_animeangleX /= 2;
				obj_animeangleZ /= 2;
				if (NEWS_flag == 1 && obj_timer > FRAME) {
					NEWSliftButton_flag = 6;
					obj_timer = 0;
				}
				break;

		case 1:	obj_speedZ =  10;
				ch[0] = PositionWallCheck(check1,obj_worldX+250.,obj_worldY,obj_worldZ+300.,50);
				ch[1] = PositionWallCheck(check2,obj_worldX     ,obj_worldY,obj_worldZ+300.,50);
				ch[2] = PositionWallCheck(check3,obj_worldX-250.,obj_worldY,obj_worldZ+300.,50);
				NEWSliftWallCheck(2,ch,check1,check2,check3);
				break;

		case 2:	obj_speedZ = -10;
				ch[0] = PositionWallCheck(check1,obj_worldX+250.,obj_worldY,obj_worldZ-300.,50);
				ch[1] = PositionWallCheck(check2,obj_worldX     ,obj_worldY,obj_worldZ-300.,50);
				ch[2] = PositionWallCheck(check3,obj_worldX-250.,obj_worldY,obj_worldZ-300.,50);
				NEWSliftWallCheck(1,ch,check1,check2,check3);
				break;

		case 3:	obj_speedX =  10;
				ch[0] = PositionWallCheck(check1,obj_worldX+300.,obj_worldY,obj_worldZ+250.,50);
				ch[1] = PositionWallCheck(check2,obj_worldX+300.,obj_worldY,obj_worldZ     ,50);
				ch[2] = PositionWallCheck(check3,obj_worldX+300.,obj_worldY,obj_worldZ-250.,50);
				NEWSliftWallCheck(4,ch,check1,check2,check3);
				break;

		case 4:	obj_speedX = -10;
				ch[0] = PositionWallCheck(check1,obj_worldX-300.,obj_worldY,obj_worldZ+250.,50);
				ch[1] = PositionWallCheck(check2,obj_worldX-300.,obj_worldY,obj_worldZ     ,50);
				ch[2] = PositionWallCheck(check3,obj_worldX-300.,obj_worldY,obj_worldZ-250.,50);
				NEWSliftWallCheck(3,ch,check1,check2,check3);
				break;

		case 5:	NEWSlift_Shock();
				return;
				break;

		case 6:	if (iwa_TimerRemove(execstp,FRAME*5)) {
					s_makeobj_absolute(execstp,0,S_NEWSlift,e_NEWSlift,obj_attX,obj_attY,obj_attZ,0,0,0);
				} 
				break;
	}

	NEWSlift_PlayerRideCheck();



	obj_worldX += obj_speedX;
	obj_worldZ += obj_speedZ;

	if (NEWSliftButton_flag != 0 && NEWSliftButton_flag != 6)  objsound_level(NA_LSE2_RAFT_MOVE);

//dprintf(20,20,"flag %d",NEWSliftButton_flag);

}




/*################*/
#endif
/*################*/


