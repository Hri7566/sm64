/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
   					-------------------------

					File		: yalcs.h
					Description	: yajima "structure"
					Date		: 1995.7.18
					Author		: H.yajima

 ********************************************************************************/




/********************************************************************************
			=========================
				StrategySystem
			=========================
 ********************************************************************************/

typedef unsigned long	Path;


/*============= Strategy Work (Assemble) =======================================*/

typedef union {
	float			f;				/* float	work	*/
	long			d;				/* long		work	*/
	void			*pointer;		/* function	pointer	*/
	short			word[2];
	unsigned long	flag32;			/* 32 bit flags		*/
	unsigned short	flag16[2];		/* short flag		*/
	unsigned char	flag8[4];		/* char	flag		*/
	short			*shortptr;		/* short pointer	*/
} stratwork;

/*============= Strategy List ==================================================*/

typedef struct _stratlist{
	MapShape			map;  									/* maplist structure			*/
	struct	_stratlist	*next;
	struct	_stratlist	*prev;

} StrategyList;

/*============= Linked List ====================================================*/

typedef struct _linklist	{
	struct	_linklist	*next;		/* next */
	struct	_linklist	*prev;		/* prev */
} Linklist;

/********************************************************************************
			===================================
			 delay object stratetgy structure
			===================================
 ********************************************************************************/

#define	MAX_DELAY	20

typedef struct {
	float	positionX;
	float	positionY;
	float	positionZ;
	short	angleX;
	short	angleY;
	short	angleZ;
} DelayRecord;

/********************************************************************************
			===================
			 Strategy Record
			===================
 ********************************************************************************/

typedef struct _strat{
	MapShape			map; 									/* maplist structure			*/
	struct	_strat		*next;
	struct	_strat		*prev;

	struct	_strat		*motherobj;								/* mothershape pointer			*/
	struct	_strat		*childobj;								/* childshape pointer			*/

	unsigned long		hit_names;
	short				status;									/* remove only			ON/OFF	*/
	short				hit_counter;							/* hitting counter		(INIT)	*/
	struct	_strat		*hit_shape[size_hit_counter];			/* hit shapes					*/

	stratwork			s[size_path_work];						/* strategy work		(INIT)	*/

	void				(*func)();								/* strategy function address	*/
	unsigned long 		*path_pointer;					  		/* path ProgramCounter			*/
	unsigned long		path_stackpointer;				  		/* path StackPointer	(INIT)	*/
	unsigned long		path_stack[size_path_stack];	  		/* path Stack			(INIT)	*/
	short				path_waittimer;					  		/* wait time counter	(INIT)	*/
	short				actortype;

	float				myobj_R;								/* hitcheck 					*/
	float				myobj_height;							/* hitcheck						*/
	float				myobj_damage_R;							/* hitcheck 					*/
	float				myobj_damage_H;							/* hitcheck						*/
	float				myobj_kuikomi;							/* hitcheck						*/

	unsigned long		*pathname;	  							/* strategy name				*/
	struct	_strat		*rootobj;								/* skeleton root object			*/
	struct	_strat 		*ride_strat;
	short				*shapeinfo;								/* shape infomation				*/

	AffineMtx			matrix;									/* matrix						*/

	void				*actoraddr;								/* actor address				*/

} StrategyRecord, *StrategyPtr;

/*============= Screen Strategy ==============================================*/

typedef struct {
	StrategyList	slist_buffer[16];
} ScreenStrategy;




/********************************************************************************
			=========
			 BGcheck 
			=========
 ********************************************************************************/




/********************************************************************************
		BG check data format
 ********************************************************************************/

typedef struct {
	short	flag;
	short	attr;
	short	x1,z1;
	short	x2,z2;
	short	x3,z3;
	float	a,b,c,d;
	short	xmin,xmax;
	short	zmin,zmax;
} Plane;

/*------------------------------------------------------------------------------*/

typedef struct {
	short		BGattr;			/* BGcheck number 		*/
	short		BGdata1;		/* BGcheck data	(1)		*/
	char		BGstatus;		/* BGcheck data (2)		*/
	char		BGarea;			/* area Number			*/
	short		Ymin,Ymax;		/* square MAX			*/
	short		x1,y1,z1;		/* point data 	(1)		*/
	short		x2,y2,z2;		/* point data 	(2)		*/
	short		x3,y3,z3;		/* point data 	(3)		*/
	float		a,b,c,d;		/* Plane Normal Vector	*/
	StrategyRecord *stratp;	
} BGCheckData;

/*		flags		*/
/*		BGarea ... area number 0 ... 255	*/

/*------------------------------------------------------------------------------*/

typedef struct _BGCheckList {
	struct	_BGCheckList	*next;		/* next 		*/
  	BGCheckData				*data;		/* BGcheck Data */
} BGCheckList;

/*------------------------------------------------------------------------------*/

#define	_CHECK_GROUND	0
#define	_CHECK_ROOF		1
#define	_CHECK_WALL		2

typedef struct {
	BGCheckList		root[3];		/* ground 	pointer		*/
} BGCheckHead;


/*------------------------------------------------------------------------------*/







/********************************************************************************
				mapcheck Record
 ********************************************************************************/

#define	WALLPLANELIST_MAX		4

typedef struct {
	float		wx;								/* world position X	*/
	float		wy;								/* world position Y */
	float		wz;								/* world position Z */
	float		offsetY;	  					/* check offset Y	*/
	float		r;			  					/* check radius		*/
	short		flag;		  					/* flag				*/
	short		walllistptr;  					/* walllistptr		*/
	BGCheckData	*wall[WALLPLANELIST_MAX];		/* check wall		*/
} BGWallCheckRecord;

/********************************************************************************
				moving BG Record
 ********************************************************************************/

typedef struct {

	short	flag;
	float	cx,cy,cz;
	short	angleY;
	StrategyRecord *stratp;
  	
} RideONRecord;

/********************************************************************************
				Stage Record
 ********************************************************************************/

typedef struct {
	short	ground;
	short	roof;
	short	wall;
} BGDebugInfo;

/********************************************************************************
				JumpBG Record 
 ********************************************************************************/

typedef struct {
	FVector	position;
	SVector	angle;
	short	flag;
} JumpBGRecord;

/********************************************************************************
		enemy parameter record
 ********************************************************************************/

typedef struct {
	short	search_speed;
	short	search_angle;
	short	data2;
	short	data3;
	short	data4;
	short	data5;
	short	data6;
	short	anime_Y;
} EnemyParam;

/********************************************************************************
		effect parameter 
 ********************************************************************************/

typedef struct {
	char  flag;					/* flag         	*/
	char  total;				/* total			*/
	uchar shapename;			/* shape			*/
	char  offsetY;				/* offset Y pos 	*/
	char  speedF_min;			/* min speed-F		*/
	char  speedF_random;		/* + random speed-F	*/
	char  speedY_min;			/* min speed-Y		*/
	char  speedY_random;		/* + random speed-Y	*/
	char  gravity;				/* gravity      	*/
	char  air_fric;				/* air friction 	*/
	float scale_min;			/* min scale		*/
	float scale_random;			/* max scale		*/
} EffectRecord;



#define		MOVEINIT_RANDOMANGLE		(1<<1)
#define		MOVEINIT_RANDOM_XZPOS		(1<<2)	
#define		MOVEINIT_RANDOM_XYZPOS		(1<<3)
#define		MOVEINIT_SETSCALE			(1<<4)
#define		MOVEINIT_POSWATER			(1<<5)
#define		MOVEINIT_ANGLE_BACK			(1<<6)
#define		MOVEINIT_ANGLE_FOR			(1<<7)

typedef struct {
	short			flag; 				/* command 				 */
	short			shape;				/* shapename 			 */
	unsigned long	*path;				/* program name			 */
	short			random_angleY;		/* random angle			 */
	short			random_offset;		/* start offset position */
	float			speedF_default;		/* speed (F) default	 */
	float			speedF_random;		/* speed (F) random		 */
	float			speedY_default;		/* speed (Y) default	 */
	float			speedY_random;		/* speed (Y) random		 */
	float			scale_default;		/* scale default		 */
	float			scale_random;		/* scale random			 */
} MoveInitRecord;


/********************************************************************************
		OBJ HitCheck Parameter
 ********************************************************************************/

typedef struct {
	unsigned long	type;			/* object type				*/
	unsigned char	kuikomi;		/* kuikomi data				*/
	char			ap;				/* enemy attack point 		*/
	char			hp;				/* enemy hit point			*/
	char			coin;			/* enemy have coin			*/
	short			attack_R;		/* enemy  damage 			*/
	short			attack_H;
	short			damage_R;		/* player damage */
	short			damage_H;
} HitCheckRecord;

/********************************************************************************
			debug print structure
 ********************************************************************************/

typedef	struct {
	short	flag;
 	short 	posX;
	short	posY;
	short	minY;
	short	maxY;
	short	height;
} dbPrintRecord;

/********************************************************************************
		player effect record
 ********************************************************************************/

typedef struct {
	unsigned long	effect;
	unsigned long	stf;
	unsigned char	shape;
	unsigned long	*path;
} PlayerEffectRecord;

/********************************************************************************
		make object index jump
 ********************************************************************************/

typedef struct {
 	unsigned char	code;
 	unsigned char	flag;
 	unsigned char	status;
	unsigned char	shape;
	unsigned long	*path;
} MakeIDJMP;

/********************************************************************************
		Enemy Making Star
 ********************************************************************************/

typedef struct {
	unsigned long 	*path;
	short			starNo;
} EnemyStarRecord;

typedef struct {
 	short	Xpos;
 	short	Ypos;
 	short	Zpos;
} PositionRecord;

/********************************************************************************
		Enemy Walk Sound
 ********************************************************************************/

typedef struct {

	short			flag;
	char			time1;
	char			time2;
	unsigned long	sound;	

} EnemySoundRecord;


/********************************************************************************
		Rotation MoveBG Record
 ********************************************************************************/

typedef struct {
	short	flag;
	short	scale;
	short	*funlike;	
	short	checkdist;
} RotateMoveBGRecord;



/*===============================================================================
		End End 
================================================================================*/

