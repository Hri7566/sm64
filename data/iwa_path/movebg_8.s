/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Stage8 MoveBg Strategy

				Feburary 26 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
						Path Data
 ********************************************************************************/

/********************************************************************************
						pyramid Pencil Path Data
 ********************************************************************************/
e_py_pencil:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON | stf_ALLangleSAME)
	p_setshapeinfo(py_obj02_check_info)
	p_program(s_pencil_init)

	p_while
		p_program(s_pencil_main)
		p_program(stMainMoveBG)
	p_loop

/********************************************************************************
						pyramid Wall Path Data
 ********************************************************************************/
e_py_wall:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON | stf_ALLangleSAME)
	p_setshapeinfo(py_obj03_check_info)
	p_program(s_py_wall_init)

	p_while
		p_program(s_py_wall_main)
		p_program(stMainMoveBG)
	p_loop


/********************************************************************************
						pyramid Gondola Path Data
 ********************************************************************************/
e_py_gondola:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON)
	p_setshapeinfo(py_obj04_check_info)
	p_save_nowpos
	p_setf(erasedist,20000)
	p_program(s_py_gondola_init)

	p_while
		p_program(s_py_gondola_main)
		p_program(stMainMoveBG)
	p_loop

/*---------------------------------------------------------------------------------*/

e_py_gondola_dot:
	p_initialize(option)
	p_setbit(flag,stf_moveON)
	p_softspritemodeON

	p_while
		p_program(s_gondola_dot_main)
	p_loop


/********************************************************************************
						Sabaku Pyramid Top Path Data
 ********************************************************************************/
e_sb_pytop:
	p_initialize(moveBG)
	p_setbit(flag,stf_moveON)
	p_setshapeinfo(sa_mobj_01_c_info)
	p_save_nowpos
	p_setf(erasedist,20000)
	p_program(s_sb_pytop_init)

	p_while
		p_program(s_sb_pytop_main)
		p_program(stMainMoveBG)
	p_loop



/*---------------------------------------------------------------------------------*/
e_sb_py_hahen:
	p_initialize(option)
	p_setbit(flag,stf_moveON)
	p_program(s_sb_py_hahen_init)

	p_while
		p_program(s_sb_py_hahen_main)
	p_loop

/*---------------------------------------------------------------------------------*/
e_pytop_dummy:
	p_initialize(item)
	p_setbit(flag,stf_moveON)
	p_sethitbox(50,50)
	p_hitON

	p_while
		p_program(s_pytop_dummy_main)
	p_loop


/*################*/
#else
/*################*/


/***************************************************************************************************
						C Program
 ****************************************************************************************************/


/*==================================================================================================
						Pyramid Pencil
====================================================================================================*/
#define pencil_count	(execstp->s[stw_work0].d)
#define pencil_flag		(execstp->s[stw_work1].d)

/***************************************************************************************************
							Init
****************************************************************************************************/
extern void s_pencil_init()
{
	obj_attY = obj_worldY;
	pencil_count = 0;
	pencil_flag = 0;

}
/***************************************************************************************************
							Main
****************************************************************************************************/
extern void s_pencil_main()
{
	float offsetY;
	int wait;

	if (pencil_count == -1) {
		if (obj_timer == 0x20) { pencil_count = 0; obj_timer=0; }
		else {
			obj_speedZ = 0; obj_anglespeedX = 0;
			return;
		}
	}


	wait = (10-pencil_count);
	if (wait < 0) wait*=-1;
	wait = wait - 6;
	if (wait < 0) wait = 0;

	if (obj_timer == (0x8 + wait)) { 
		obj_timer = 0; 
		pencil_count++;

		if (pencil_count == 20) {
			if (pencil_flag==0) pencil_flag = 1;
			else				pencil_flag = 0;
			pencil_count = -1;
		}
	}

	if (wait==4 || wait==3)      wait = 4;
	else if (wait==2 || wait==1) wait = 2;
	else if (wait==0) 			 wait = 1;


	if (obj_timer < 0x08*wait) {

		if (pencil_flag == 0){  obj_speedZ =  20/wait; obj_anglespeedX =  0x400/wait; }
		else			     {  obj_speedZ = -20/wait; obj_anglespeedX = -0x400/wait; }

		obj_worldZ += obj_speedZ; 
		obj_angleX += obj_anglespeedX;

		if (I_abs(obj_angleX & 0x1fff) < 0x320 && obj_anglespeedX != 0) {
			objsound(NA_SE2_ROLLER_ROLL);
		}

		offsetY = sin(obj_angleX*(0x8000/0x2000))*23.;
		if (offsetY < 0) offsetY *= -1;
		obj_worldY = obj_attY + offsetY;
		if (obj_timer == 0x08*wait-1) Viewshaking(VS_SMALL,obj_worldX,obj_worldY,obj_worldZ);
	}

	

}

/*==================================================================================================
						Pyramid Wall0
====================================================================================================*/
#define py_wall_TIME	100
#define py_wall_set		(execstp->s[stw_programselect].d)

/***************************************************************************************************
							Init
****************************************************************************************************/
extern void s_py_wall_init()
{
	switch (py_wall_set) {
		case 0: break;
		case 1: obj_worldY -= 256;
				obj_timer += 50;
				break; 	

		case 2: obj_worldY -= 512;
				obj_mode = 1;
				break; 	
	}

}
/***************************************************************************************************
							Main
****************************************************************************************************/
extern void s_py_wall_main()
{
	switch (obj_mode) {
		case 0: obj_speedY = -512./py_wall_TIME;
				if (obj_timer == py_wall_TIME) obj_mode = 1;
				break;
				
		case 1: obj_speedY = +512./py_wall_TIME;
				if (obj_timer == py_wall_TIME) obj_mode = 0;
				break;
	}

	obj_worldY += obj_speedY;
}






/*==================================================================================================
						Pyramid Gondola
====================================================================================================*/
/***************************************************************************************************
							Main
****************************************************************************************************/
extern void s_py_gondola_init()
{
	int i;

	for (i=0; i<10; i++) {
		StrategyRecord* stratp = s_makeobj_nowpos(execstp,S_ironball_noshadow,e_py_gondola_dot);
		stratp->s[stw_worldY].f = (4600 - 460*i);
	}




}
/***************************************************************************************************
							Main
****************************************************************************************************/
extern void s_py_gondola_main()
{

	switch (obj_mode) {
		case 0:	if (player1stp->ride_strat == execstp) {
					obj_mode = 1;
				}
				break;

		case 1: obj_worldY = obj_attY - sin(obj_timer*0x1000)*10;
				if (obj_timer == 8) {obj_mode = 2; }
				break;

		case 2: obj_speedY = -10;
				obj_worldY += obj_speedY;
				if (obj_worldY < 128) {
					obj_worldY = 128;
					obj_mode = 3;
				}
				break;

		case 3: obj_worldY = 128 + sin(obj_timer*0x1000)*10;
				if (obj_timer >= 8) { 
					obj_speedY = 0;
					obj_worldY = 128;
				}
				break;
	}


}

/*==================================================================================================
						Pyramid Gondola Dot
====================================================================================================*/
/***************************************************************************************************
							Main
****************************************************************************************************/
extern void s_gondola_dot_main()
{
	StrategyRecord* stratp;

	s_set_scale(0.15);

	stratp = (StrategyRecord*) s_find_obj(e_py_gondola);

	if(stratp != 0) {
		if (stratp->s[stw_mode].d != 0) {
			execstp->status = 0;
		}

	}


}




/*==================================================================================================
						Pyramid Top
====================================================================================================*/
#define pytop_bombflag	(execstp->s[stw_work0].d)

/***************************************************************************************************
							Main
****************************************************************************************************/
extern void s_sb_pytop_init()
{
	s_makeobj_absolute(execstp,0,S_NULL,e_pytop_dummy, 1789, 1024,  764,0,0,0);
	s_makeobj_absolute(execstp,0,S_NULL,e_pytop_dummy, 1789,  896,-2579,0,0,0);
	s_makeobj_absolute(execstp,0,S_NULL,e_pytop_dummy,-5883, 1024,-2579,0,0,0);
	s_makeobj_absolute(execstp,0,S_NULL,e_pytop_dummy,-5883, 1024,  764,0,0,0);

}
/***************************************************************************************************
						Pyramid Shake
****************************************************************************************************/
static void pytop_Shake(void)
{
	StrategyRecord* stratp;
	

	obj_worldX = obj_attX + sin(obj_timer*0x4000)*40;

	if (obj_timer < FRAME*2) {
		obj_worldY = obj_attY + I_abs(sin(obj_timer*0x2000)*10);
	}
	else {
		obj_anglespeedY += 0x100;
		if (obj_anglespeedY > 0x1800) {
			obj_anglespeedY = 0x1800;
			obj_speedY = 5;
		}
		obj_animeangleY += obj_anglespeedY;
		obj_worldY += obj_speedY;

	}


//	Viewshaking(VS_SMALL,player_posX+2000,player_posY,player_posZ+2000);

	if (obj_timer < FRAME*3 ) {
		stratp = s_makeobj_nowpos(execstp,S_sankaku,e_sb_py_hahen);
		stratp->s[stw_speedF].f = 20+(Randomf()*10);
		stratp->s[stw_angleY].d = Randomd();
		stratp->s[stw_work0].f  = 0.8;
		stratp->s[stw_gravity].f   = 2 + Randomf();
	}
	
	if (obj_timer == FRAME*5) {
		obj_mode = 2;
	}

}
/***************************************************************************************************
						Pyramid Bomb
****************************************************************************************************/
static void pytop_Bomb(void)
{
	StrategyRecord* stratp;
	short i;
	
	s_burneffect(0,0,690);

	for (i=0; i<30; i++) {
		stratp = s_makeobj_nowpos(execstp,S_sankaku,e_sb_py_hahen);
		stratp->s[stw_speedF].f = 80+(Randomf()*50);
		stratp->s[stw_speedY].f = 20+(Randomf()*80);
		stratp->s[stw_angleY].d = Randomd();
		stratp->s[stw_work0].f  = 3.;
		stratp->s[stw_gravity].f   = 5 + Randomf()*2;
	}


	execstp->status = 0;


}
/***************************************************************************************************
							Main
****************************************************************************************************/
extern void s_sb_pytop_main()
{

	switch (obj_mode) {
		case 0:	if (pytop_bombflag == 4) { 
					Na_NazoClearBgm();
					obj_mode = 1; 
				}
				break;

		case 1: if ( obj_timer == 0 )	objsound(NA_SE2_PYRAMID_SHAKING);
				pytop_Shake();
				break;

		case 2:  if ( obj_timer == 0 )	obj_remove_sound(NA_SE2_PYRAMID_EXPLODE);
				 pytop_Bomb();
				 break;

	}


}
/*====================================================================================================*/
/***************************************************************************************************
					Pyramid Top Hahen Init
****************************************************************************************************/
extern void s_sb_py_hahen_init()
{
	execstp->s[stw_friction].f  = 0.999;
	execstp->s[stw_specificG].f = 2.0;
	obj_animecounter = 3;


	s_set_scale(execstp->s[stw_work0].f);
}
/***************************************************************************************************
					Pyramid Top Hahen	Main
****************************************************************************************************/
extern void s_sb_py_hahen_main()
{

	ObjMoveEvent();

	obj_animeangleY += 0x1000;
	obj_animeangleX += 0x1000;


	if (obj_timer == FRAME*2) {
		execstp->status = 0;

	}	

}
/*====================================================================================================*/
/***************************************************************************************************
					Pyramid Top Dummy Main
****************************************************************************************************/
extern void s_pytop_dummy_main()
{

	s_hitON();

	if ( s_hitcheck(execstp,player1stp)==1 ){

		mother_work0++;
		  
		execstp->status = 0;
	}


}






/*################*/
#endif
/*################*/


