/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 36 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage36


/* ====================================================================================
		: 36 - 1 [ Donkey ] move BGs sequence.
=======================================================================================	*/
SEQ_Stage3601_MoveBGs:

		seqActor(S_movebg00,   4360,-1722, 4001,  0, 48,0,  0,			0,0,  e_don_maruta	)

		seqReturn


/* ====================================================================================
		: 36 - 1 [ Donkey ] enemies sequence.
=======================================================================================	*/
SEQ_Stage3601_Enemies:

		seqActor(S_NULL,  	  	   -1639, 1146,-1742,  0,0,0,  0,1,0,  	e_gush_ironball	)
		seqActor(S_NULL,  	  	 	3295,-3692, 2928,  0,0,0,  0,0,0,   e_falls			)
		seqActor(S_NULL,  	  	 	2004,-1580, 1283,  0,0,0,  0,0,0,   e_falls			)

		seqActor( S_poohole,      -2077,-1023,-1969,  0,0,0,  0,0,0,  e_hole		 )
		seqActor( S_poohole,      -2500,-1023,-2157,  0,0,0,  0,0,0,  e_hole		 )
		seqActor( S_poohole,      -2048,-1023,-2307,  0,0,0,  0,0,0,  e_hole		 )
		seqActor( S_poohole,      -2351,-1023,-2416,  0,0,0,  0,0,0,  e_hole		 )
		seqActor( S_poohole,      -2400,-2559,-2185,  0,0,0,  0,0,0,  e_hole		 )
		seqActor( S_poohole,      -1435,-2559,-3118,  0,0,0,  0,0,0,  e_hole		 )
		seqActor( S_poohole,      -1677,-2559,-3507,  0,0,0,  0,0,0,  e_hole		 )
		seqActor( S_poohole,      -1869,-2559,-2704,  0,0,0,  0,0,0,  e_hole		 )
		seqActor( S_poohole,      -2525,-2559,-2626,  0,0,0,  0,0,0,  e_hole		 )

		seqActor( S_indy,    	      0,    0,    0,  0,0,0,  0,0,0,  e_indy		 )
		seqActor( S_indy,    	      0,    0,    0,  0,0,0,  0,1,0,  e_indy		 )

		seqActor( S_NULL,    	   3625,  560,  165,  0,330,0,  0,0,0,  e_cloud		 )

		seqLevelActor(0x0002,S_monky, 729, 2307,  335,  0,0,0,  0,0,0, e_event_monky )		
		seqLevelActor(0x003f,S_monky, 1992, -1548, 2944,  0,0,0,  0,1,0, e_event_monky )

		seqReturn


/* ====================================================================================
		: 36 - 1 [ Donkey ] Stars sequence.
=======================================================================================	*/
SEQ_Stage3601_Stars:

		seqLevelActor(0x0001,S_polystar,  1200,  2600,  150,  0,0,0,  0,0,0,  e_tripstar) /* top_of_the_mountain */
		seqLevelActor(0x0002,S_movebg01,  2496,  1670, 1492,  0,0,0,  1,0,0,  e_donky_kago	)		/* monky's star 		*/
		seqLevelActor(0x003f,S_NULL	   , -3250, -2500,-3700,  0,0,0,  2,0,0,  e_tripstar_getcoins) /* 8_coin_in_the_slider */
		seqLevelActor(0x003f,S_polystar, -2900, -2700, 3650,  0,0,0,  3,0,0,  e_tripstar) /* slider_exit 		 */
		seqLevelActor(0x003f,S_polystar,  1800,  1200, 1050,  0,0,0,  4,0,0,  e_tripstar) /* waterfall 			 */
		seqLevelActor(0x003f,S_polystar,  7300, -3100, 1300,  0,0,0,  5,0,0,  e_tripstar) /* on_the_mushroom     */

		seqReturn


/* ====================================================================================
		: 36 - 2 [ Slider 2 - 1 ] BG parts.
=======================================================================================	*/
SEQ_Stage3602_BGParts:

		seqActor(S_movebg02,  4389,3620,  624,  0,0,0,  0,0,0,  e_stop)
		seqActor(S_movebg03, -1251,2493, 2224,  0,0,0,  0,0,0,  e_stop)
		seqActor(S_movebg04, -2547,1365, -520,  0,0,0,  0,0,0,  e_stop)
		seqActor(S_movebg05,  -324, 989,-4090,  0,0,0,  0,0,0,  e_stop)

		seqReturn


/* ====================================================================================
		: 36 - 3 [ Slider 2 - 2 ] BG parts.
=======================================================================================	*/
SEQ_Stage3603_BGParts:

		seqActor(S_movebg02,  7867, -959,-6085,  0,0,0,  0,0,0,  e_stop)
		seqActor(S_movebg02, -5241, 5329, 9466,  0,0,0,  0,0,0,  e_stop)
		seqActor(S_movebg03, -1869,-5311, 7358,  0,0,0,  0,0,0,  e_stop)
		seqActor(S_movebg04, -9095, 4262, 5348,  0,0,0,  0,0,0,  e_stop)
		seqActor(S_movebg05, -8477,  730,-7122,  0,0,0,  0,0,0,  e_stop)
		seqActor(S_movebg05,  6160,-6076, 7861,  0,0,0,  0,0,0,  e_stop)

		seqReturn


/* ====================================================================================
		: 36 - 4 [ Slider 2 - 3 ] BG parts.
=======================================================================================	*/
SEQ_Stage3604_BGParts:

		seqActor(S_movebg03,  5157,1974,-8292,  0,0,0,  0,0,0,  e_stop)
		seqActor(S_movebg04, 11106,2588,  381,  0,0,0,  0,0,0,  e_stop)
		seqActor(S_movebg05,    37,1974,-1124,  0,0,0,  0,0,0,  e_stop)

		seqReturn


/* ====================================================================================
		: 36 - 4 [ Donkey ] Stars sequence.
=======================================================================================	*/
SEQ_Stage3604_Stars:

		seqReturn


/* ====================================================================================
		: Stage 36 main sequence.
=======================================================================================	*/
SEQ_DoStage36:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage36SegmentRomStart	  , _GfxStage36SegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE	  , _ISliderTextureSegmentRomStart, _ISliderTextureSegmentRomEnd)
	seqLoadPres(SEGMENT_BACK	  , _BackMainmapSegmentRomStart	  , _BackMainmapSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_fSegmentRomStart	  , _GfxEnemy1_fSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_fSegmentRomStart	  , _HmsEnemy1_fSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()
		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_f)

		seqHmsShape(S_chimney , RCP_HmsEndoor)
		seqHmsShape(S_movebg00, RCP_HmsDonkeyMaruta)
		seqHmsShape(S_movebg01, RCP_HmsDkKago)

		seqHmsShape(S_movebg02, RCP_HmsSlider2Dec1)
		seqHmsShape(S_movebg03, RCP_HmsSlider2Dec2)
		seqHmsShape(S_movebg04, RCP_HmsSlider2Dec3)
		seqHmsShape(S_movebg05, RCP_HmsSlider2Dec4)

		seqHmsShape(S_bg01, RCP_HmsDkEnoki1)
		seqHmsShape(S_bg02, RCP_HmsDkEnoki2)
		seqHmsShape(S_bg03, RCP_HmsDkEnoki3)
		seqHmsShape(S_bg04, RCP_HmsDkEnoki4)
		seqHmsShape(S_bg05, RCP_HmsDkStart)
		seqHmsShape(S_bg06, RCP_HmsDkHabatobi)
		seqHmsShape(S_bg07, RCP_HmsDkTaihou)
		seqHmsShape(S_bg08, RCP_HmsDkHana1)
		seqHmsShape(S_bg09, RCP_HmsDkHana2)
		seqHmsShape(S_bg10, RCP_HmsDkMaruta1)
		seqHmsShape(S_bg11, RCP_HmsDkMaruta2)
		seqHmsShape(S_bg13, RCP_HmsDkGururi)
		seqHmsShape(S_bg14, RCP_HmsDkKozou)
		seqHmsShape(S_bg15, RCP_HmsDkTop)
		seqHmsShape(S_bg16, RCP_HmsDkFuji)
		seqHmsShape(S_bg17, RCP_HmsDkAsiba1)
		seqHmsShape(S_bg18, RCP_HmsDkAsiba2)
		seqHmsShape(S_bg19, RCP_HmsDkAsiba3)
		seqHmsShape(S_bg20, RCP_HmsDkKakusi)

		seqBeginScene(1, RCP_Stage36Scene1)
			seqActor(S_NULL,   102,-4332+START_H, 5734,  0,  45,0,  0,10,0,  e_player_entpict)
			seqActor(S_NULL, -2447,		   -2457, 3952,  0,-105,0,  0,20,0,  e_player_landing)
			seqActor(S_NULL,  2267,		   -3006,-3788,  0, 148,0,  0,21,0,  e_warp)
			seqActor(S_NULL,  -557,		   -3448,-4146,  0,-168,0,  0,22,0,  e_warp)
			seqPort(10, 36, 1, 10)					/*	MARIO stage in.				*/
			seqPort(20, 36, 1, 20)					/*	From the slider exit.		*/
			seqPort(21, 36, 1, 22)					/*	Warp.						*/
			seqPort(22, 36, 1, 21)					/*	Warp.						*/
			seqMidBGPort(0, 36, 2, 10)				/*	To the scene 2 [ slider ].	*/
			seqMidBGPort(1, 36, 2, 10)				/*				"				*/
			seqMidBGPort(2, 36, 2, 10)				/*				"				*/
			seqGameClear(6, 2,  52)
			seqGameOver (6, 2, 102)
			seqCall(SEQ_Stage3601_MoveBGs)
			seqCall(SEQ_Stage3601_Enemies)
			seqCall(SEQ_Stage3601_Stars)
			seqMapInfo(cx3601_info)
			seqTagInfo(cx3601_info_tag)
			seqSetMusic(NA_STG_MOUNTAIN, NA_MAINMAP_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

		seqBeginScene(2, RCP_Stage36Scene2)
			seqActor(S_NULL, 7000, 5381, 6750,  0,225,0,  0,10,0,  e_player_landing)
			seqPort(10, 36, 2, 10)
			seqGameClear(6, 2,  52)
			seqGameOver (6, 2, 102)
			seqCall(SEQ_Stage3602_BGParts)
			seqMapInfo(cx3602_info)
			seqTagInfo(cx3602_info_tag)
			seqConnect(2, 3, 10*1024, 7*1024, +10*1024)
			seqSetMusic(NA_STG_CASTLE, NA_SLIDER_BGM)
			seqEnvironment(ENV_SLIDER)
		seqEndScene()

		seqBeginScene(3, RCP_Stage36Scene3)
			seqGameClear(6, 2,  52)
			seqGameOver (6, 2, 102)
			seqCall(SEQ_Stage3603_BGParts)
			seqMapInfo(cx3603_info)
			seqTagInfo(cx3603_info_tag)
			seqConnect(3, 4, -11*1024, 13*1024,   3*1024)
			seqSetMusic(NA_STG_CASTLE, NA_SLIDER_BGM)
			seqEnvironment(ENV_SLIDER)
		seqEndScene()

		seqBeginScene(4, RCP_Stage36Scene4)
			seqActor(S_chimney, -7285,-1866,-4812,  0,0,0,  0,10,0,    e_tripchimney)
			seqPort(10, 36, 1, 20)
			seqGameClear(6, 2,  52)
			seqGameOver (6, 2, 102)
			seqCall(SEQ_Stage3604_BGParts)
			seqCall(SEQ_Stage3604_Stars)
			seqMapInfo(cx3604_info)
			seqTagInfo(cx3604_info_tag)
			seqSetMusic(NA_STG_CASTLE, NA_SLIDER_BGM)
			seqEnvironment(ENV_SLIDER)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 45,  102,-4332,5734)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
