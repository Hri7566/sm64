/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 13 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							  Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage13


/* ====================================================================================
		: Scene 1 [ Big world ] Stars sequence.
=======================================================================================	*/
SEQ_Stage1301_Stars:

/*		seqLevelActor(0x003f,S_NULL,     -6300,-1750, -6300,  0,0,0,  0,4,0,  e_tripstar_getdummy)		big Pakkun	*/
/*		seqLevelActor(0x003f,S_polystar,  2600, 3550, -2400,  0,0,0,  1,0,0,  e_tripstar)			/*	big top		*/

		seqReturn


/* ====================================================================================
		: Scene 2 [ Small world ] Stars sequence.
=======================================================================================	*/
SEQ_Stage1302_Stars:

/*		seqLevelActor(0x003f,S_polystar, -150, -500, 2000,  0,0,0,  2,0,0,  e_tripstar)					on small 	 	*/
		seqLevelActor(0x003f,S_NULL    ,    0, -700,-4500,  0,0,0,  3,0,0,  e_tripstar_getdummy)	/*	far island		*/

		seqReturn


/* ====================================================================================
		: Scene 3 [ In the Hole ] Stars sequence.
=======================================================================================	*/
SEQ_Stage1303_Stars:

		seqLevelActor(0x003f, S_NULL,  -1800,800,-1500,  0,0,0,  4,0,0,  e_tripstar_getcoins)	/* 8 coin	 */

		seqActor(S_hanachan_head,  17,1843,-62,  0,0,0,  5,0,0,  e_hanachan)

		seqReturn


/* ====================================================================================
		: Scene 1 [ Big world ] Enemies sequence.
=======================================================================================	*/
SEQ_Stage1301_Enemies:

		seqLevelActor(0x0004,S_nokonoko,   -1900, -511, 2400,  0,-30,0, 2,3,0,  e_noko)		/* Kame */
		seqLevelActor(0x0004,S_NULL,        7400,-1537,-6300,  0,  0,0, 0,0,0,  e_kamegoal)	/* Kame */

		seqActor(S_NULL,   -6556,-2969, 6565,  0,0,0,  0,1,0,  e_kuribo_set)
		seqActor(S_kuribo,  6517,-2559, 4327,  0,0,0,  0,1,0,  e_kuribo)

		seqActor(S_pakun,  -6336,-2047,-3861,   0,0,0,  0,1,0,  e_firepakun)
		seqActor(S_pakun,  -5740,-2047,-6578,   0,0,0,  0,1,0,  e_firepakun)
		seqActor(S_pakun,  -6481,-2047,-5998,   0,0,0,  0,1,0,  e_firepakun)
		seqActor(S_pakun,  -5577,-2047,-4961,   0,0,0,  0,1,0,  e_firepakun)
		seqActor(S_pakun,  -6865,-2047,-4568,   0,0,0,  0,1,0,  e_firepakun)

		seqActor(S_NULL,  -4413,204,-2140,  0,0,0,  0,3,0,  e_gush_ironball2)

		seqActor(S_buku,  -6241,-3300, -716,  0,0,0,  0,0,0,  e_buku)
		seqActor(S_buku,   1624,-3300, 8142,  0,0,0,  0,0,0,  e_buku)

		seqReturn


/* ====================================================================================
		: Scene 1 [ Big world ] Move BGs sequence.
=======================================================================================	*/
SEQ_Stage1301_MoveBGs:

		seqActor(S_movebg01,  0,3891,-1533,  0,0,0,  0,0,0,  e_big_futa)

		seqReturn


/* ====================================================================================
		: Scene 2 [ Small world ] Move BGs sequence.
=======================================================================================	*/
SEQ_Stage1302_MoveBGs:

		seqActor(S_movebg02,  0,1167,-460,  0,0,0,  0,0,0,  e_small_futa)
		seqActor(S_NULL,  -1382,  80,-649,  0,0,0,  0,4,0,  e_gush_ironball2)

		seqReturn


/* ====================================================================================
		: Scene 1 [ Big world ] Dokan sequence.
=======================================================================================	*/
SEQ_Stage1301_Dokan:

		seqActor(S_bg20,   6656,-1536,-5632,  0,0,0,  0,50,0,  e_dokan)
		seqActor(S_bg20,  -5888,-2048,-5888,  0,0,0,  0,51,0,  e_dokan)
		seqActor(S_bg20,  -3072,  512,-3840,  0,0,0,  0,52,0,  e_dokan)
		seqPort(50, 13, 2, 50)
		seqPort(51, 13, 2, 51)
		seqPort(52, 13, 2, 52)

		seqReturn


/* ====================================================================================
		: Scene 2 [ Small world ] Dokan sequence.
=======================================================================================	*/
SEQ_Stage1302_Dokan:

		seqActor(S_bg20,   1997,-461,-1690,  0,0,0,  0,50,0,  e_dokan)
		seqActor(S_bg20,  -1766,-614,-1766,  0,0,0,  0,51,0,  e_dokan)
		seqActor(S_bg20,   -922, 154,-1152,  0,0,0,  0,52,0,  e_dokan)
		seqPort(50, 13, 1, 50)
		seqPort(51, 13, 1, 51)
		seqPort(52, 13, 1, 52)

		seqReturn


/* ====================================================================================
		: Stage 13 main sequence.
=======================================================================================	*/
SEQ_DoStage13:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage13SegmentRomStart	  , _GfxStage13SegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE	  , _JFortTextureSegmentRomStart  , _JFortTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_BACK      , _BackMainmapSegmentRomStart   , _BackMainmapSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_kSegmentRomStart	  , _GfxEnemy1_kSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_kSegmentRomStart	  , _HmsEnemy1_kSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_eSegmentRomStart	  , _GfxEnemy2_eSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_eSegmentRomStart	  , _HmsEnemy2_eSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_k)
		seqCall(SetEnemy2_e)

		seqHmsShape(S_tree1, RCP_HmsMainTree)

		seqHmsShape(S_bg01, RCP_HmsBSSima)
		seqHmsShape(S_bg20, RCP_HmsDokan )

		seqHmsShape(S_movebg01, RCP_HmsBSfutaBig  )
		seqHmsShape(S_movebg02, RCP_HmsBSfutaSmall)

		seqBeginScene(1, RCP_Stage13Scene1)
			seqActor(S_NULL,  -7372,-2969+START_H, 7373,  0,149,0,    0,10,0,	e_player_entpict)
			seqActor(S_NULL,    410,		 -512,  922,  0,  0,0,    0,11,0,	e_player_waiting)
			seqActor(S_NULL,    410,		 -512,  717,  0,  0,0,    5,12,0,	e_tripchimney2	)
			seqActor(S_NULL,      0,		 3170,-1570,  0,  0,0,   10,13,0,	e_tripchimney2	)
			seqPort(10, 13, 1, 10)				/*	MARIO stage in.				*/
			seqPort(11, 13, 1, 11)				/*	From the scene3.			*/
			seqPort(12, 13, 3, 10)				/*	To   the scene3. [ lower ]	*/
			seqPort(13, 13, 3, 11)				/*	To   the scene3. [ upper ]	*/
			seqGameClear(6, 2,  55)
			seqGameOver (6, 2, 105)
			seqCall(SEQ_Stage1301_Dokan)
			seqCall(SEQ_Stage1301_Stars)
			seqCall(SEQ_Stage1301_MoveBGs)
			seqCall(SEQ_Stage1301_Enemies)
			seqMapInfo(cx1301_info)
			seqTagInfo(cx1301_info_tag)
			seqSetMusic(NA_STG_GROUND, NA_MAINMAP_BGM)
			seqEnvironment(ENV_PLAIN)
		seqEndScene()

		seqBeginScene(2, RCP_Stage13Scene2)
			seqActor(S_NULL,  -2211,-890+START_H, 2212,  0, 149,0,  0,10,0,  e_player_entpict)
			seqActor(S_NULL,    280,		-767,-4180,  0,   0,0,  0,11,0,  e_warp)
			seqActor(S_NULL,  -1638,		   0,-1988,  0,-126,0,  0,12,0,  e_warp)
			seqPort(10, 13, 2, 10)					/*	MARIO stage in.	*/
			seqPort(11, 13, 2, 12)					/*	Warp.			*/
			seqPort(12, 13, 2, 11)					/*	Warp.			*/
			seqGameClear(6, 2,  51)
			seqGameOver (6, 2, 101)
			seqCall(SEQ_Stage1302_Dokan)
			seqCall(SEQ_Stage1302_Stars)
			seqCall(SEQ_Stage1302_MoveBGs)
			seqMapInfo(cx1302_info)
			seqTagInfo(cx1302_info_tag)	
			seqSetMusic(NA_STG_GROUND, NA_MAINMAP_BGM)
			seqEnvironment(ENV_PLAIN)
		seqEndScene()

		seqBeginScene(3, RCP_Stage13Scene3)
			seqActor(S_NULL,	512,1024,2150,	 0,180,0,	 0,10,0,	e_player_waiting)
			seqActor(S_NULL,	  0,3277,	0,	 0,  0,0,	 0,11,0,	e_player_landing)
			seqActor(S_NULL,	512,1024,2355,	 0,  0,0,	 5,12,0,	e_tripchimney2	)
			seqPort(10, 13, 3, 10)					/*	From the scene1. [ lower ]	*/
			seqPort(11, 13, 3, 11)					/*	From the scene1. [ upper ]	*/
			seqPort(12, 13, 1, 11)					/*	To	 the scene1. [ lower ]	*/
			seqGameClear(6, 2,  55)
			seqGameOver (6, 2, 105)
			seqCall(SEQ_Stage1303_Stars)
			seqMapInfo(cx1303_info)
			seqTagInfo(cx1303_info_tag)	
			seqSetMusic(NA_STG_DUNGEON, NA_DUNGEON_BGM)
			seqEnvironment(ENV_PLAIN)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 149,  -7372,-2969,7373)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
