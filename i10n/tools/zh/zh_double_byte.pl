#!/usr/bin/perl -w 

die "Usage: zh_double_byte.pl <input>\n       inserts an extra byte for each letter of the string table\n       in <input> and write out to <input>.zh\n" unless $#ARGV>=0;

foreach $file (@ARGV) {
    open(INFILE, "< $file") or die "Can't open $file: $!\n";
    open(OUTFILE, "> $file.zh") or die "Can't open $file.zh: $!\n";
    while (<INFILE>) {
        if (/^static/) {
            @words = split /\s+/;
            foreach $word (@words) {
                if ($word =~ /(0xff)|(0xfe)|(0x9e)|(0xf0)|(0xf1)|(0xbe)|(0xd0)|(0xd1)|(0xd2)|(0xe0)/) {
                    printf OUTFILE "0xff, %s", $word;
                } elsif ($word =~ /0x/) {
                    printf OUTFILE "0x00, %s", $word;
                } else {
                    printf OUTFILE "%s", $word;
                }
                if ($word =~ /};$/) {
                    printf OUTFILE "\n";
                } else {
                    printf OUTFILE " ";
                }
            }
        } else {
            printf OUTFILE "%s", $_;
        }
    }
    close INFILE;
    close OUTFILE;
}
