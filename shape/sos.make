enemyshape: \
hms_basic_enemy.c \
hms_enemy1_a.c \
hms_enemy1_b.c \
hms_enemy1_c.c \
hms_enemy1_d.c \
hms_enemy1_e.c \
hms_enemy1_f.c \
hms_enemy1_g.c \
hms_enemy1_h.c \
hms_enemy1_i.c \
hms_enemy1_j.c \
hms_enemy1_k.c \
hms_enemy2_a.c \
hms_enemy2_b.c \
hms_enemy2_c.c \
hms_enemy2_d.c \
hms_enemy2_e.c \
hms_enemy2_f.c \
hms_enemy2_g.c \
hms_enemy2_h.c \
hms_enemy2_i.c \
hms_keep.c 
	touch enemyshape

hms_enemy1_a.c: \
enemy1_a/ball.shape \
enemy1_a/bird/bird.shape \
enemy1_a/bird_egg.shape \
enemy1_a/dosun/dosun.shape \
enemy1_a/killer.shape \
enemy1_a/omurobo/omurobo.shape 
	mkshape enemy1_a

hms_enemy1_b.c: \
enemy1_b/otos/otos.shape \
enemy1_b/unbaba.shape 
	mkshape enemy1_b

hms_enemy1_c.c: \
enemy1_c/bom_king/bomking.shape \
enemy1_c/water_bom.shape 
	mkshape enemy1_c

hms_enemy1_d.c: \
enemy1_d/big_shell/big_shell.shape \
enemy1_d/manta/manta.shape \
enemy1_d/shark/shark.shape \
enemy1_d/ubboo/ubboo.shape \
enemy1_d/w_tornado.shape 
	mkshape enemy1_d

hms_enemy1_e.c: \
enemy1_e/hagetaka/hagetaka.shape \
enemy1_e/handman/handman.shape \
enemy1_e/sanbo.shape \
enemy1_e/tornedo.shape 
	mkshape enemy1_e

hms_enemy1_f.c: \
enemy1_f/gfx_hole.shape \
enemy1_f/indy/indy.shape \
enemy1_f/kazekozo.shape \
enemy1_f/monky/monky.shape \
enemy1_f/windface.shape 
	mkshape enemy1_f

hms_enemy1_g.c: \
enemy1_g/furafura/furafura.shape \
enemy1_g/ping/ping.shape \
enemy1_g/snow_man/snow_man.shape 
	mkshape enemy1_g

hms_enemy1_h.c: \
enemy1_h/bane.shape \
enemy1_h/bikkuri_button.shape 
	mkshape enemy1_h

hms_enemy1_i.c: \
enemy1_i/book/book.shape \
enemy1_i/booklight.shape \
enemy1_i/chair/chair.shape \
enemy1_i/key.shape \
enemy1_i/piano/piano.shape \
enemy1_i/teresa.shape \
enemy1_i/teresakago.shape 
	mkshape enemy1_i

hms_enemy1_j.c: \
enemy1_j/minibird.shape \
enemy1_j/peach/peach.shape \
enemy1_j/yoshi/yoshi.shape 
	mkshape enemy1_j

hms_enemy1_k.c: \
enemy1_k/buku/buku.shape \
enemy1_k/hanachan.shape \
enemy1_k/jugem/jugem.shape \
enemy1_k/pipo.shape \
enemy1_k/togezo.shape 
	mkshape enemy1_k

hms_enemy2_a.c: \
enemy2_a/blowup.shape \
enemy2_a/firering.shape \
enemy2_a/guide_ball.shape \
enemy2_a/kopa.shape \
enemy2_a/kopakirai.shape \
enemy2_a/mowan.shape 
	mkshape enemy2_a

hms_enemy2_d.c: \
enemy2_d/amembow/amembow.shape \
enemy2_d/kombu.shape \
enemy2_d/mine.shape \
enemy2_d/pirania/pirania.shape \
enemy2_d/puku.shape \
enemy2_d/ring.shape \
enemy2_d/treasurebox.shape 
	mkshape enemy2_d

hms_enemy2_e.c: \
enemy2_e/goal_flag/goal_flag.shape \
enemy2_e/kui.shape \
enemy2_e/nokonoko/nokonoko.shape \
enemy2_e/pakun.shape \
enemy2_e/wallman/wallman.shape \
enemy2_e/wan_ball.shape \
enemy2_e/wanwan/wanwan.shape 
	mkshape enemy2_e

hms_enemy2_f.c: \
enemy2_f/jugem2/jugem2.shape \
enemy2_f/kinopio/kinopio.shape \
enemy2_f/rabbit/rabbit.shape \
enemy2_f/teresa2.shape 
	mkshape enemy2_f

hms_enemy2_g.c: \
enemy2_g/ice_otos.shape \
enemy2_g/kuramochi/kuramochi.shape 
	mkshape enemy2_g

hms_enemy2_h.c: \
enemy2_h/balloonbody.shape \
enemy2_h/ballooneye.shape \
enemy2_h/bat/bat.shape \
enemy2_h/mucho/mucho.shape \
enemy2_h/nessy/nessy.shape \
enemy2_h/walker/walker.shape 
	mkshape enemy2_h

hms_keep.c: \
keep/attack_effect.shape \
keep/bombfire.shape \
keep/butterfly.shape \
keep/coin.shape \
keep/dokan.shape \
keep/door.shape \
keep/doorkey.shape \
keep/fireD.shape \
keep/fish.shape \
keep/gfx_stone.shape \
keep/grass.shape \
keep/hitdatas.shape \
keep/itemhat.shape \
keep/meter.shape \
keep/numbers.shape \
keep/one_up_kinoko.shape \
keep/polygon_star.shape \
keep/sand_dust.shape \
keep/sankaku.shape \
keep/shadestar.shape \
keep/snow_ball.shape \
keep/tatefuda.shape \
keep/tree.shape 
	mkshape keep

hms_basic_enemy.c: \
basic_enemy/BCbutton.shape \
basic_enemy/biribiri/biribiri.shape \
basic_enemy/bom_futa.shape \
basic_enemy/cannon_base.shape \
basic_enemy/cannon_body.shape \
basic_enemy/carryboy/carryboy.shape \
basic_enemy/hanbutton.shape \
basic_enemy/hanlift.shape \
basic_enemy/hart.shape \
basic_enemy/heyho.shape \
basic_enemy/hibi_block.shape \
basic_enemy/ironball.shape \
basic_enemy/itembox.shape \
basic_enemy/kuribo/kuribo.shape \
basic_enemy/newbomhei/newbomhei.shape \
basic_enemy/pushblock.shape \
basic_enemy/qbox.shape \
basic_enemy/testlift.shape \
basic_enemy/xkoura.shape 
	mkshape basic_enemy

