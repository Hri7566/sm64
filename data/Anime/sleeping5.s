/********************************************************************************
						Ultra 64 MARIO Brothers

					 mario sleeping animation data 5

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							July 19, 1995
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioSleeping5:
	.half	 MAP_ANIM_NORMAL 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	40 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x0258						/* total bytes       */


	.half	    1,    0 	/* chn14_4translate x */
	.half	    1,    1 	/* chn14_4 translate y */
	.half	    1,    2 	/* chn14_4 translate z */
	.half	    1,    0 	/* chn14_4 rotate x */
	.half	    1,    3 	/* chn14_4 rotate y */
	.half	    1,    0 	/* chn14_4 rotate z */
	.half	    1,    0 	/* <m_waist>1_3 rotate x */
	.half	    1,    0 	/* <m_waist>1_3 rotate y */
	.half	    1,  161 	/* <m_waist>1_3 rotate z */
	.half	    1,  158 	/* <m_body>1 rotate x */
	.half	    1,  159 	/* <m_body>1 rotate y */
	.half	    1,  160 	/* <m_body>1 rotate z */
	.half	   40,   38 	/* <m_head>2 rotate x */
	.half	   40,   78 	/* <m_head>2 rotate y */
	.half	   40,  118 	/* <m_head>2 rotate z */
	.half	    1,   34 	/* chn6 rotate x */
	.half	    1,   35 	/* chn6 rotate y */
	.half	    1,   36 	/* chn6 rotate z */
	.half	    1,    8 	/* <m_larmA>1 rotate x */
	.half	    1,    9 	/* <m_larmA>1 rotate y */
	.half	    1,   10 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1 rotate x */
	.half	    1,    0 	/* <m_larmB>1 rotate y */
	.half	    1,    7 	/* <m_larmB>1 rotate z */
	.half	    1,    4 	/* <m_lhand>1 rotate x */
	.half	    1,    5 	/* <m_lhand>1 rotate y */
	.half	    1,    6 	/* <m_lhand>1 rotate z */
	.half	    1,   30 	/* chn10 rotate x */
	.half	    1,   31 	/* chn10 rotate y */
	.half	    1,   32 	/* chn10 rotate z */
	.half	    1,   15 	/* <m_rarmA>1 rotate x */
	.half	    1,   16 	/* <m_rarmA>1 rotate y */
	.half	    1,   17 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1 rotate y */
	.half	    1,   14 	/* <m_rarmB>1 rotate z */
	.half	    1,   11 	/* <m_rhand>1 rotate x */
	.half	    1,   12 	/* <m_rhand>1 rotate y */
	.half	    1,   13 	/* <m_rhand>1 rotate z */
	.half	    1,    0 	/* chn15 rotate x */
	.half	    1,    0 	/* chn15 rotate y */
	.half	    1,   33 	/* chn15 rotate z */
	.half	    1,   27 	/* <m_llegA>1 rotate x */
	.half	    1,   28 	/* <m_llegA>1 rotate y */
	.half	    1,   29 	/* <m_llegA>1 rotate z */
	.half	    1,    0 	/* <m_llegB>1 rotate x */
	.half	    1,    0 	/* <m_llegB>1 rotate y */
	.half	    1,   26 	/* <m_llegB>1 rotate z */
	.half	    1,    0 	/* <m_ltoot>1 rotate x */
	.half	    1,    0 	/* <m_ltoot>1 rotate y */
	.half	    1,   25 	/* <m_ltoot>1 rotate z */
	.half	    1,    0 	/* chn17 rotate x */
	.half	    1,    0 	/* chn17 rotate y */
	.half	    1,   37 	/* chn17 rotate z */
	.half	    1,   22 	/* <m_rlegA>1 rotate x */
	.half	    1,   23 	/* <m_rlegA>1 rotate y */
	.half	    1,   24 	/* <m_rlegA>1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1 rotate y */
	.half	    1,   21 	/* <m_rlegB>1 rotate z */
	.half	    1,   18 	/* <m_rfoot>1 rotate x */
	.half	    1,   19 	/* <m_rfoot>1 rotate y */
	.half	    1,   20 	/* <m_rfoot>1 rotate z */

	.half	0x0000,0x0035,0xFF5A,0x3FFF,0x0816,0xE285,0xEA2F,0xF5E7,0x6E83,0x9FAC,0x5146,0xF345,0x2351,0xDF86,0xFA63,0x131C
	.half	0xEC4C,0xE0B0,0xFFB8,0xFF3D,0xD81B,0x0366,0x0CE2,0x0EFE,0x961D,0xCEBC,0x0710,0xF169,0xF279,0x973B,0x9524,0x5D21
	.half	0x475E,0xBF5F,0x90BE,0x7987,0x4B8C,0xBF5F,0xFD76,0xFD7A,0xFD7F,0xFD85,0xFD8B,0xFD92,0xFD99,0xFDA0,0xFDA7,0xFDAE
	.half	0xFDB4,0xFDBA,0xFDBF,0xFDC3,0xFDC7,0xFDC9,0xFDC9,0xFDC9,0xFDC8,0xFDC6,0xFDC3,0xFDC1,0xFDBD,0xFDB9,0xFDB5,0xFDB1
	.half	0xFDAC,0xFDA7,0xFDA2,0xFD9E,0xFD99,0xFD95,0xFD90,0xFD8D,0xFD89,0xFD86,0xFD84,0xFD82,0xFD81,0xFD80,0xFEEE,0xFEE6
	.half	0xFEDE,0xFED4,0xFECB,0xFEC0,0xFEB6,0xFEAC,0xFEA2,0xFE99,0xFE90,0xFE88,0xFE81,0xFE7B,0xFE76,0xFE74,0xFE73,0xFE73
	.half	0xFE75,0xFE79,0xFE7D,0xFE83,0xFE89,0xFE90,0xFE98,0xFEA0,0xFEA8,0xFEB1,0xFEBA,0xFEC2,0xFECB,0xFED3,0xFEDA,0xFEE1
	.half	0xFEE8,0xFEED,0xFEF2,0xFEF5,0xFEF7,0xFEF8,0x26D8,0x2656,0x25C1,0x251C,0x2469,0x23AF,0x22F0,0x2231,0x2174,0x20C0
	.half	0x2016,0x1F7C,0x1EF5,0x1E85,0x1E30,0x1DFB,0x1DE8,0x1DF4,0x1E19,0x1E53,0x1EA0,0x1EFE,0x1F6A,0x1FE3,0x2067,0x20F2
	.half	0x2183,0x2217,0x22AC,0x2341,0x23D1,0x245D,0x24E0,0x2559,0x25C6,0x2624,0x2671,0x26AB,0x26CF,0x26DC,0xFF6C,0x00A3
	.half	0x0F98,0x2968
