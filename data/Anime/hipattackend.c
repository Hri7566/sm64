/********************************************************************************
						Ultra 64 MARIO Brothers

				   mario squat waiting animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							November 2, 1995
 ********************************************************************************/

#include "../headers.h"


static short mario_hip_attack_end_prm[]={
	    0,   52,   53,   53,   53,   54,   54,   -2,
	   -2,   -1,    0,    0,    0,16383, -118,-1664,
	-3985,-16688,-16069,-14595,-12835,-11361,-10742,-15360,
	-16284,-18486,-21115,-23317,-24240,-10868,-11107,-11676,
	-12355,-12924,-13163, 5481, 6912,10323,14395,17807,
	19238,  173,  155,  112,   61,   18,    0, -816,
	 -731, -528, -287,  -84,    0,-6922,-6693,-6148,
	-5498,-4953,-4724,-14810,-14235,-12864,-11228,-9857,
	-9282,19365,18660,16978,14970,13289,12583, 8150,
	 8623, 9751,11097,12225,12698,13327,12596,10855,
	 8777, 7035, 6305,   27,  603,-13531,14429,13095,
	 9913, 6116, 2934, 1600,-2065,-1857,-1359, -765,
	 -267,  -59, 2006, 1762, 1181,  488,  -91, -335,
	31435,32322,-31095,-28570,-26454,-25567,  149, -247,
	-13246,13350,12222, 9532, 6321, 3631, 2503,  -53,
	  -47,  -34,  -18,   -5,    0,-3126,-2790,-1988,
	-1030, -228,  108,-31135,-30420,-28717,-26683,-24980,
	-24265,-32767,-32767,16753,-16545,-32767,-32767,16753,
	-16545,  286,  256,  185,  100,   29,    0,  783,
	  789,  801,  816,  829,  834, 9826, 9446, 8538,
	 7454, 6546, 6165,   -7,   -6,   -4,   -2,    0,
	    0, -836, -836, -835, -835, -834, -834, 5197,
	 5187, 5163, 5134, 5110, 5100, 6949, 7036, 7243,
	 7490, 7697, 7784
};
static short mario_hip_attack_end_tbl[]={
	    1,    0,	/* chn14translate x */
	    6,    1,	/* chn14 translate y */
	    6,    7,	/* chn14 translate z */
	    1,    0,	/* chn14 rotate x */
	    1,   13,	/* chn14 rotate y */
	    1,    0,	/* chn14 rotate z */
	    1,    0,	/* <m_waist>1_3 rotate x */
	    1,    0,	/* <m_waist>1_3 rotate y */
	    6,  181,	/* <m_waist>1_3 rotate z */
	    6,  163,	/* <m_body>1 rotate x */
	    6,  169,	/* <m_body>1 rotate y */
	    6,  175,	/* <m_body>1 rotate z */
	    6,  145,	/* <m_head>2 rotate x */
	    6,  151,	/* <m_head>2 rotate y */
	    6,  157,	/* <m_head>2 rotate z */

	    1,  141,	/* chn6 rotate x */
	    1,  142,	/* chn6 rotate y */
	    1,  143,	/* chn6 rotate z */
	    6,   23,	/* <m_larmA>1 rotate x */
	    6,   29,	/* <m_larmA>1 rotate y */
	    6,   35,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	    6,   17,	/* <m_larmB>1 rotate z */
	    1,   14,	/* <m_lhand>1 rotate x */
	    1,   15,	/* <m_lhand>1 rotate y */
	    1,   16,	/* <m_lhand>1 rotate z */

	    1,  137,	/* chn10 rotate x */
	    1,  138,	/* chn10 rotate y */
	    1,  139,	/* chn10 rotate z */
	    6,   65,	/* <m_rarmA>1 rotate x */
	    6,   71,	/* <m_rarmA>1 rotate y */
	    6,   77,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	    6,   59,	/* <m_rarmB>1 rotate z */
	    6,   41,	/* <m_rhand>1 rotate x */
	    6,   47,	/* <m_rhand>1 rotate y */
	    6,   53,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  140,	/* chn15 rotate z */
	    6,  119,	/* <m_llegA>1 rotate x */
	    6,  125,	/* <m_llegA>1 rotate y */
	    6,  131,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	    6,  113,	/* <m_llegB>1 rotate z */
	    1,  110,	/* <m_ltoot>1 rotate x */
	    1,  111,	/* <m_ltoot>1 rotate y */
	    1,  112,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  144,	/* chn17 rotate z */
	    6,   92,	/* <m_rlegA>1 rotate x */
	    6,   98,	/* <m_rlegA>1 rotate y */
	    6,  104,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	    6,   86,	/* <m_rlegB>1 rotate z */
	    1,   83,	/* <m_rfoot>1 rotate x */
	    1,   84,	/* <m_rfoot>1 rotate y */
	    1,   85,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioHipAttackEnd = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	 6,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_hip_attack_end_prm,
	mario_hip_attack_end_tbl
};
