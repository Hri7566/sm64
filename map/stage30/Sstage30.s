/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 30 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage30


/* ================================================================================
		: Stage 30 main sequence.
===================================================================================	*/
SEQ_DoStage30:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage30SegmentRomStart	, _GfxStage30SegmentRomEnd	)
	seqLoadPres(SEGMENT_BACK	  , _BackYokoScrlSegmentRomStart, _BackYokoScrlSegmentRomEnd)
	seqLoadPres(SEGMENT_ENEMY2	  ,  _GfxEnemy2_aSegmentRomStart, _GfxEnemy2_aSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2,  _HmsEnemy2_aSegmentRomStart, _HmsEnemy2_aSegmentRomEnd	)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetEnemy2_a)

		seqHmsShape(S_bg01, RCP_HmsGuideBall )

		seqBeginScene(1, RCP_Stage30Scene1)
			seqActor(S_NULL,  0,307+START_H,0,  0,180,0,  0,10,0,  e_player_rolling)
			seqPort(10, 30, 1, 10)
			seqGameClear( 6, 1, 36)
			seqGameOver (17, 1, 12)
			seqMapInfo(cx3001_info)
			seqSetMusic(NA_STG_KUPPA, NA_KUPPA_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 180,  0,307,0)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
