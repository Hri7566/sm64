/********************************************************************************
	camera.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1995 Nintendo co., ltd.  All rights reserved

	Jan 29, 1996
*********************************************************************************/

#define	cSTG1_1	1*16 + 1
#define	cSTG1_2	1*16 + 2
#define	cSTG1_3	1*16 + 3

#define	cSTG2_1	2*16 + 1
#define	cSTG2_2	2*16 + 2
#define	cSTG2_3	2*16 + 3

#define	cSTG3_1	3*16 + 1
#define	cSTG3_2	3*16 + 2
#define	cSTG3_3	3*16 + 3

#define	cSTG4_1	4*16 + 1
#define	cSTG4_2	4*16 + 2
#define	cSTG4_3	4*16 + 3

#define	cSTG5_1	5*16 + 1
#define	cSTG5_2	5*16 + 2
#define	cSTG5_3	5*16 + 3

#define	cSTG6_1	6*16 + 1
#define	cSTG6_2	6*16 + 2
#define	cSTG6_3	6*16 + 3

#define	cSTG7_1	7*16 + 1
#define	cSTG7_2	7*16 + 2
#define	cSTG7_3	7*16 + 3

#define	cSTG8_1	8*16 + 1
#define	cSTG8_2	8*16 + 2
#define	cSTG8_3	8*16 + 3

#define	cSTG9_1	9*16 + 1
#define	cSTG9_2	9*16 + 2
#define	cSTG9_3	9*16 + 3

#define	cSTG10_1	10*16 + 1
#define	cSTG10_2	10*16 + 2
#define	cSTG10_3	10*16 + 3

#define	cSTG11_1	11*16 + 1
#define	cSTG11_2	11*16 + 2
#define	cSTG11_3	11*16 + 3

#define	cSTG12_1	12*16 + 1
#define	cSTG12_2	12*16 + 2
#define	cSTG12_3	12*16 + 3

#define	cSTG13_1	13*16 + 1
#define	cSTG13_2	13*16 + 2
#define	cSTG13_3	13*16 + 3

#define	cSTG14_1	14*16 + 1
#define	cSTG14_2	14*16 + 2
#define	cSTG14_3	14*16 + 3

#define	cSTG15_1	15*16 + 1
#define	cSTG15_2	15*16 + 2
#define	cSTG15_3	15*16 + 3

#define	cSTG16_1	16*16 + 1
#define	cSTG16_2	16*16 + 2
#define	cSTG16_3	16*16 + 3

#define	cSTG17_1	17*16 + 1
#define	cSTG17_2	17*16 + 2
#define	cSTG17_3	17*16 + 3

#define	cSTG18_1	18*16 + 1
#define	cSTG18_2	18*16 + 2
#define	cSTG18_3	18*16 + 3

#define	cSTG19_1	19*16 + 1
#define	cSTG19_2	19*16 + 2
#define	cSTG19_3	19*16 + 3

#define	cSTG20_1	20*16 + 1
#define	cSTG20_2	20*16 + 2
#define	cSTG20_3	20*16 + 3

#define	cSTG21_1	21*16 + 1
#define	cSTG21_2	21*16 + 2
#define	cSTG21_3	21*16 + 3

#define	cSTG22_1	22*16 + 1
#define	cSTG22_2	22*16 + 2
#define	cSTG22_3	22*16 + 3

#define	cSTG23_1	23*16 + 1
#define	cSTG23_2	23*16 + 2
#define	cSTG23_3	23*16 + 3

#define	cSTG24_1	24*16 + 1
#define	cSTG24_2	24*16 + 2
#define	cSTG24_3	24*16 + 3

#define	cSTG25_1	25*16 + 1
#define	cSTG25_2	25*16 + 2
#define	cSTG25_3	25*16 + 3

#define	cSTG26_1	26*16 + 1
#define	cSTG26_2	26*16 + 2
#define	cSTG26_3	26*16 + 3

#define	cSTG27_1	27*16 + 1
#define	cSTG27_2	27*16 + 2
#define	cSTG27_3	27*16 + 3

#define	cSTG28_1	28*16 + 1
#define	cSTG28_2	28*16 + 2
#define	cSTG28_3	28*16 + 3

#define	cSTG29_1	29*16 + 1
#define	cSTG29_2	29*16 + 2
#define	cSTG29_3	29*16 + 3

#define	cSTG30_1	30*16 + 1
#define	cSTG30_2	30*16 + 2
#define	cSTG30_3	30*16 + 3

#define	cSTG31_1	31*16 + 1
#define	cSTG31_2	31*16 + 2
#define	cSTG31_3	31*16 + 3

#define	cSTG32_1	32*16 + 1
#define	cSTG32_2	32*16 + 2
#define	cSTG32_3	32*16 + 3

#define	cSTG33_1	33*16 + 1
#define	cSTG33_2	33*16 + 2
#define	cSTG33_3	33*16 + 3

#define	cSTG34_1	34*16 + 1
#define	cSTG34_2	34*16 + 2
#define	cSTG34_3	34*16 + 3

#define	cSTG35_1	35*16 + 1
#define	cSTG35_2	35*16 + 2
#define	cSTG35_3	35*16 + 3

#define	cSTG36_1	36*16 + 1
#define	cSTG36_2	36*16 + 2
#define	cSTG36_3	36*16 + 3

#define	cSTG37_1	37*16 + 1
#define	cSTG37_2	37*16 + 2
#define	cSTG37_3	37*16 + 3

#define	cSTG38_1	38*16 + 1
#define	cSTG38_2	38*16 + 2
#define	cSTG38_3	38*16 + 3

#define	cSTG39_1	39*16 + 1
#define	cSTG39_2	39*16 + 2
#define	cSTG39_3	39*16 + 3

#define	cSTG40_1	40*16 + 1
#define	cSTG40_2	40*16 + 2
#define	cSTG40_3	40*16 + 3

#define	cSTG41_1	41*16 + 1
#define	cSTG41_2	41*16 + 2
#define	cSTG41_3	41*16 + 3

#define	cSTG42_1	42*16 + 1
#define	cSTG42_2	42*16 + 2
#define	cSTG42_3	42*16 + 3

#define	cSTG43_1	43*16 + 1
#define	cSTG43_2	43*16 + 2
#define	cSTG43_3	43*16 + 3

#define	cSTG44_1	44*16 + 1
#define	cSTG44_2	44*16 + 2
#define	cSTG44_3	44*16 + 3

#define	cSTG45_1	45*16 + 1
#define	cSTG45_2	45*16 + 2
#define	cSTG45_3	45*16 + 3

#define	cSTG46_1	46*16 + 1
#define	cSTG46_2	46*16 + 2
#define	cSTG46_3	46*16 + 3


