/********************************************************************************
	memory.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	February 27, 1986
 ********************************************************************************/

#ifndef MEMORY_H
#define	MEMORY_H

#define	MM_ALLOC_FORE	0
#define	MM_ALLOC_BACK	1

#define	MM_DATA_SEGMENT	0
#define	MM_BOOT_SEGMENT	1

#define	MM_RSRC_COMMON	0
#define	MM_RSRC_EACH	1



#ifndef ASSEMBLER

/********************************************************************************
 *
 *	Type definition.
 *
 ********************************************************************************/


/*-------------------------------------------------------------------------------
 *	Arena memory managiment record.
 */
typedef struct {
	int			size;					/* size of the arena memory				*/
	int			used;					/* size of used memory					*/
	MemPtr		buff;					/* pointer to the arena memory			*/
	MemPtr		free;					/* pointer to the free memory			*/
} ArenaRecord, *ArenaPtr;

/*-------------------------------------------------------------------------------
 *	Heap memory managiment record.
 */
typedef struct {
	int		size;						/* size of the heap memory				*/
	MemPtr	buff;						/* pointer to the heap memory			*/
	MemPtr	free;						/* pointer to the free memory block		*/
	long	reserved;
} HeapRecord, *HeapPtr;

/*-------------------------------------------------------------------------------
 *	Segment information record.
 */
typedef struct {
	short	flags;						/* flags								*/
	short	number;						/* segment number						*/			
	MemPtr	romtop;						/* top address on ROM					*/
	MemPtr	rombtm;						/* bottom address +1 on ROM				*/
	MemPtr	memory;						/* top address on virtual memory		*/
} SegmentInfo;

/*-------------------------------------------------------------------------------
 *	Memory partition table.
 */
typedef struct {
	ulong	nitems;						/* number of items						*/
	MemPtr	topaddr;					/* pointer to the top of data			*/
	struct {
		ulong	offset;					/* data offset							*/
		ulong	length;					/* data length							*/
	} item[1];							/* item table (variable length)			*/
} Partition;

/*-------------------------------------------------------------------------------
 *	Partial memory management record.
 */
typedef struct {
	Partition	*ptable;				/* pointer to the partition table		*/
	MemPtr		last;					/* pointer to the last access memory	*/
	MemPtr		buffer;					/* pointer to the buffer memory			*/
} PartialRecord, *PartialPtr;

/*-------------------------------------------------------------------------------
 *	Resource management record.
 */
typedef struct {
	Partition	*ptable;				/* pointer to the partition table		*/
	MemPtr		loadmap[1];				/* pointer to the load map				*/
} ResourceRecord, *ResourcePtr;



extern HeapPtr		systemHeap;			/* pointer to the system heap memory	*/


/********************************************************************************
 *
 *	Function prototypes
 *
 ********************************************************************************/

/* RCP segment management */

extern uint	 SetSegment(int number, void *cpuAddr);
extern void *GetSegment(int number);
extern void *SegmentToVirtual(void *cpuAddr);
extern void *VirtualToSegment(int number, void *cpuAddr);
extern void  StoreSegments(void);

/* Free zone management */

extern void  InitFreeZone(MemPtr topAddr, MemPtr btmAddr);
extern void *AllocFreeZone(uint size, int mode);
extern uint  PurgeFreeZone(void *address);
extern void *ResizeFreeZone(void *address, uint size);
extern uint	 FreeZoneSize(void);

extern uint LinkFreeZone(void);
extern uint UnlinkFreeZone(void);

/* Static data loader */

extern MemPtr
LoadSegment(int number, char *romStart, char *romEnd, int mode);

extern MemPtr
LoadData(char *romStart, char *romEnd, int mode);

extern MemPtr
LoadProgram(char *ramAddr, char *romStart, char *romEnd);

extern MemPtr
LoadCompressed(int number, char *romStart, char *romEnd);

extern MemPtr
LoadPressTexture(int number, char *romStart, char *romEnd);

extern void
LoadUserLibrary(void);


/* Arena memory management */

extern ArenaPtr InitArena(int size, int mode);
extern void		*AllocArena(ArenaPtr arena, int size);
extern void		*ResizeArena(ArenaPtr arena, int size);


/* Heap memory management */

extern HeapPtr InitHeap(int size, int mode);
extern void	   *AllocHeap(HeapPtr heap, int memsize);
extern void	   *FreeHeap(HeapPtr heap, void *memory);


/* Dynamic memory management */

extern void *AllocDynamic(int size);



extern Partition *
LoadPartitionTable(MemPtr romAddr);

extern void
InitPartial(PartialPtr partial, MemPtr romAddr, MemPtr buffer);

extern int
ReadPartialData(PartialPtr partial, int index);


#endif
#endif
