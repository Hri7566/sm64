/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage5 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"


#include "yukiyama1/yukiyama1_ext_texture.h"
#include "yukiyama1/snowman_head_texture.h"
#include "yuki_slider1/yuki_slider1_ext_texture.h"

/* ------------------------------------------------------------	*/

#include "yukiyama1/st_body_shape.sou"
#include "yukiyama1/st_yuka_shape.sou"
#include "yukiyama1/st_ft_shape.sou"
#include "yukiyama1/st_saku_shape.sou"
#include "yukiyama1/st_kage_shape.sou"
#include "yukiyama1/st_ie1a_shape.sou"
#include "yukiyama1/st_ie1b_shape.sou"
#include "yukiyama1/st_ie2a_shape.sou"
#include "yukiyama1/st_ie2b_shape.sou"
#include "yukiyama1/st_ie2c_shape.sou"
#include "yukiyama1/st_dai_shape.sou"
#include "yukiyama1/st_hasira_shape.sou"
#include "yukiyama1/st_ura_a_shape.sou"
#include "yukiyama1/st_ura_b_shape.sou"
#include "yukiyama1/st_ura_c_shape.sou"

#include "yukiyama1/rift_shape.sou"
#include "yukiyama1/rift_wa_shape.sou"
#include "yukiyama1/snow_rock.sou"
#include "yukiyama1/snow_head_shape.sou"
#include "yukiyama1/snow_eye_shape.sou"

#include "yukiyama1/cx0501.flk"
#include "yukiyama1/cx0501.tag"
#include "yukiyama1/rift_check.flk"
#include "yukiyama1/yuki_goro.ral"
#include "yukiyama1/rope_lift.ral"

/********************************************************************************/
/*	Water surface records in 5 - 1.												*/
/********************************************************************************/
static short waterSurf_05_01_00[] = {
	1,
	0,   5,  2, 3137, 4228,    3137, 4945,    3925, 4945,    3925, 4228,  1,  150,  0,
};

WaterSurfRec waterSurf_05_01[] = {
		{	 0, waterSurf_05_01_00	 },
		{	-1,	NULL	}					//	End of  WaterSurfRec data.
};


/* ------------------------------------------------------------	*/


#include "yuki_slider1/s_s_shape.sou"
#include "yuki_slider1/s_s_inout_shape.sou"
#include "yuki_slider1/s_s_gate_shape.sou"
#include "yuki_slider1/s_s_alpha_shape.sou"
#include "yuki_slider1/s_s_mado_shape.sou"
#include "yuki_slider1/s_s_clear_shape.sou"
#include "yuki_slider1/s_s_ia_shape.sou"

#include "yuki_slider1/cx0502.flk"
#include "yuki_slider1/cx0502.tag"
#include "yuki_slider1/pengin.ral"
