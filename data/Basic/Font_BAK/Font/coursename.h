/**************************************************************************************************
					Message Data 		(5/7)														   
								programed by Iwamoto Daiki										   
**************************************************************************************************/

static unsigned char course01[] = { 0x9e, 0x01, 0x9e, 0xf0, 0x8d, 0x90, 0x5c, 0x41, 0x58, 0x9e, 0x4d, 0x6d, 0xf0, 0x4b, 0xa4, 0x42, 0xff,};
static unsigned char course02[] = { 0x9e, 0x02, 0x9e, 0xf0, 0x89, 0xd1, 0x7f, 0x9d, 0x76, 0x9d, 0xf0, 0x77, 0x58, 0x9e, 0x53, 0x67, 0xf0, 0x52, 0xff,};
static unsigned char course03[] = { 0x9e, 0x03, 0x9e, 0x45, 0x41, 0xf0, 0x4e, 0x47, 0x58, 0x9e, 0x41, 0x67, 0x43, 0xff,};
static unsigned char course04[] = { 0x9e, 0x04, 0x9e, 0x4a, 0x60, 0x41, 0x4a, 0x60, 0x41, 0x9e, 0x8e, 0x72, 0x9d, 0x82, 0x9d, 0xff,};
static unsigned char course05[] = { 0x9e, 0x05, 0x9e, 0x82, 0x99, 0x7a, 0x58, 0x9e, 0x8d, 0x96, 0x9f, 0x89, 0x72, 0x7c, 0xff,};
static unsigned char course06[] = { 0x9e, 0x06, 0x9e, 0x63, 0x5f, 0x55, 0x53, 0x48, 0x68, 0x9e, 0xf0, 0x53, 0x42, 0x47, 0x51, 0xff,};
static unsigned char course07[] = { 0x9e, 0x07, 0x9e, 0x8b, 0xd5, 0x71, 0x70, 0xf0, 0x89, 0xf0, 0x8b, 0x98, 0x9e, 0x96, 0x9d, 0xf0, 0x83, 0xff,};
static unsigned char course08[] = { 0x9e, 0x08, 0x9e, 0x40, 0xa1, 0x50, 0xa1, 0x50, 0x9e, 0x4a, 0xf0, 0x59, 0x47, 0xff,};
static unsigned char course09[] = { 0x9e, 0x09, 0x9e, 0x72, 0xd8, 0x9f, 0x7f, 0x9f, 0x9e, 0x96, 0x9d, 0xf0, 0x83, 0xff,};
static unsigned char course10[] = { 0x01, 0x00, 0x9e, 0x7c, 0x88, 0x9f, 0x8e, 0x9d, 0xf0, 0x7c, 0x9e, 0x96, 0x9d, 0xf0, 0x83, 0xff,};
static unsigned char course11[] = { 0x01, 0x01, 0x9e, 0x5f, 0xf0, 0x4c, 0xf0, 0x5a, 0x4f, 0x7b, 0x82, 0xd6, 0x9f, 0xff,};
static unsigned char course12[] = { 0x01, 0x02, 0x9e, 0x4f, 0x45, 0x41, 0x4f, 0x45, 0x41, 0x8e, 0x72, 0x9d, 0x82, 0x9d, 0xff,};
static unsigned char course13[] = { 0x01, 0x03, 0x9e, 0x50, 0xf0, 0x5a, 0xf0, 0x52, 0x45, 0x9e, 0x70, 0x71, 0x96, 0x9d, 0xf0, 0x83, 0xff,};
static unsigned char course14[] = { 0x01, 0x04, 0x9e, 0x80, 0xd1, 0x77, 0x7f, 0xd1, 0x77, 0x9a, 0xd1, 0x77, 0xff,};
static unsigned char course15[] = { 0x01, 0x05, 0x9e, 0x99, 0x71, 0x9d, 0xf0, 0x8d, 0x9f, 0x9e, 0x77, 0x98, 0x9f, 0xf0, 0x7c, 0xff,};
static unsigned char course16[] = { 0x9e, 0x9e, 0x9e, 0x63, 0x5f, 0x58, 0x9e, 0x4d, 0x45, 0x41, 0x58, 0x9e, 0x77, 0xd1, 0xf1, 0x89, 0xff,};
static unsigned char course17[] = { 0x9e, 0x9e, 0x9e, 0x5d, 0x58, 0x44, 0x58, 0x9e, 0x42, 0x5f, 0x58, 0x9e, 0x77, 0xd1, 0xf1, 0x89, 0xff,};
static unsigned char course18[] = { 0x9e, 0x9e, 0x9e, 0x52, 0x6d, 0x47, 0x42, 0x58, 0x9e, 0x4f, 0x4f, 0x45, 0x41, 0xf2, 0xff,};
static unsigned char course19[] = { 0x9e, 0x9e, 0x9e, 0xf1, 0x8a, 0x9f, 0x80, 0x58, 0x45, 0x47, 0x69, 0x7c, 0x96, 0x71, 0xf0, 0x7f, 0x9f, 0xff,};
static unsigned char course20[] = { 0x9e, 0x9e, 0x9e, 0x91, 0x7f, 0x98, 0x7c, 0x71, 0xd1, 0x80, 0x58, 0x9e, 0x4f, 0x46, 0xff,};
static unsigned char course21[] = { 0x9e, 0x9e, 0x9e, 0x59, 0xf0, 0x59, 0x4f, 0x48, 0xf2, 0x59, 0x57, 0x7c, 0x71, 0xd1, 0x80, 0x5c, 0xff,};
static unsigned char course22[] = { 0x9e, 0x9e, 0x9e, 0x44, 0x5d, 0x67, 0x58, 0x53, 0x42, 0x61, 0x41, 0x7c, 0x71, 0xd1, 0x80, 0xff,};
static unsigned char course23[] = { 0x9e, 0x9e, 0x9e, 0x55, 0xf0, 0x4b, 0x9e, 0x45, 0x48, 0x68, 0x9e, 0x59, 0x57, 0x8e, 0x97, 0x74, 0xff,};
static unsigned char course24[] = { 0x9e, 0x9e, 0x9e, 0x44, 0x4a, 0x45, 0x54, 0x53, 0x9e, 0x41, 0xa1, 0x4b, 0xa4, 0xff,};
static unsigned char course25[] = { 0x9e, 0x9e, 0x9e, 0x44, 0x41, 0x4b, 0x41, 0x78, 0x9f, 0x76, 0xff,};
static unsigned char course26[] = { 0x9e, 0x9e, 0x9e, 0x44, 0x4b, 0x6a, 0x58, 0x45, 0x47, 0x69, 0x7c, 0x7f, 0x9f, 0xff,};

unsigned char *crsNamePtr[] = {
	course01, course02, course03, course04, course05, course06, course07, course08, course09, course10, course11, course12, course13, course14, course15, course16,
	course17, course18, course19, course20, course21, course22, course23, course24, course25, course26, NULL,
}; 
