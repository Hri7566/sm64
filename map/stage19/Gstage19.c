/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage19 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE19_FOG_R		0
#define		STAGE19_FOG_G		0
#define		STAGE19_FOG_B		0
#define		STAGE19_FOG_START	980

#include "ext2_yokoscroll/ext2_yokoscroll_ext_texture.h"

#include "ext2_yokoscroll/e2_ana_shape.sou"
#include "ext2_yokoscroll/e2_bg01_shape.sou"
#include "ext2_yokoscroll/e2_bg02_shape.sou"
#include "ext2_yokoscroll/e2_bg03_shape.sou"
#include "ext2_yokoscroll/e2_bg04_shape.sou"
#include "ext2_yokoscroll/e2_bg05_shape.sou"
#include "ext2_yokoscroll/e2_bg06_shape.sou"
#include "ext2_yokoscroll/e2_bg07_shape.sou"
#include "ext2_yokoscroll/e2_bg08_shape.sou"
#include "ext2_yokoscroll/e2_bg09_shape.sou"
#include "ext2_yokoscroll/e2_bg10_shape.sou"
#include "ext2_yokoscroll/e2_bg11_shape.sou"
#include "ext2_yokoscroll/e2_bg12_shape.sou"
#include "ext2_yokoscroll/e2_bg13_shape.sou"
#include "ext2_yokoscroll/e2_bg14_shape.sou"
#include "ext2_yokoscroll/e2_bg15_shape.sou"
#include "ext2_yokoscroll/e2_bg16_shape.sou"
#include "ext2_yokoscroll/e2_bg17_shape.sou"
#include "ext2_yokoscroll/e2_bg18_shape.sou"
#include "ext2_yokoscroll/e2_bg19_shape.sou"

#include "ext2_yokoscroll/e2_agaru_shape.sou"
#include "ext2_yokoscroll/e2_amiyuka_shape.sou"
#include "ext2_yokoscroll/e2_bou_shape.sou"
#include "ext2_yokoscroll/e2_gura_shape.sou"
#include "ext2_yokoscroll/e2_kuzure01_shape.sou"
#include "ext2_yokoscroll/e2_kuzure02_shape.sou"
#include "ext2_yokoscroll/e2_nobiru01_shape.sou"
#include "ext2_yokoscroll/e2_nobiru02_shape.sou"
#include "ext2_yokoscroll/e2_rifuto01_shape.sou"
#include "ext2_yokoscroll/e2_rifuto02_shape.sou"
#include "ext2_yokoscroll/e2_susumu_shape.sou"
#include "ext2_yokoscroll/e2_ukishima_shape.sou"
#include "ext2_yokoscroll/e2_yokoshiso_shape.sou"


/* ------------------------------------------------------------------------------------	*/
/* ------------------------------------------------------------------------------------	*/

#include "ext2_yokoscroll/cx1901.flk"
#include "ext2_yokoscroll/cx1901.tag"

#include "ext2_yokoscroll/e2_agaru_check.flk"
#include "ext2_yokoscroll/e2_amiyuka_check.flk"
#include "ext2_yokoscroll/e2_gura_check.flk"
#include "ext2_yokoscroll/e2_kuzure01_check.flk"
#include "ext2_yokoscroll/e2_nobiru_check.flk"
#include "ext2_yokoscroll/e2_rifuto01_check.flk"
#include "ext2_yokoscroll/e2_rifuto02_check.flk"
#include "ext2_yokoscroll/e2_susumu_check.flk"
#include "ext2_yokoscroll/e2_ukishima_check.flk"
#include "ext2_yokoscroll/e2_yokoshiso_check.flk"

#include "ext2_yokoscroll/yogan_lift.ral"


/********************************************************************************/
/*	Waterfall data in 19 - 1 [ Lava yoko scroll ].								*/
/********************************************************************************/
short waterfall_1901_pt[] = {
	 2,
	-7450,  205,  1050,		0,	0,
	 4838,  205,  1050,		6,	0,
	 4838,  205, -1108,		6,	2,
	-7450,  205, -1108,		0,	2,
};

short waterfall_1902_pt[] = {
	 -3,
	-4531, 3487,  1050,		0,	0,
	 5658, 3487,  1050,		6,	0,
	 5658, 3487, -1108,		6,	2,
	-4531, 3487, -1108,		0,	2,
};

short waterfall_1903_pt[] = {
	 -2,
	 8191,  -3067,  8192,		0,	0,
	    0,  -3067,  8192,		0,	2,
	-8191,  -3067,  8192,		0,	4,

	 8191,  -3067,     0,		2,	0,
	    0,  -3067,     0,		2,	2,
	-8191,  -3067,     0,		2,	4,

	 8191,  -3067, -8192,		4,	0,
	    0,  -3067, -8192,		4,	2,
	-8191,  -3067, -8192,		4,	4,
};

Gfx waterfall_1901_draw[] = {
	gsSP1Triangle(0, 1, 2, 0),
	gsSP1Triangle(0, 2, 3, 0),
	gsSPEndDisplayList()
};

Gfx waterfall_1903_draw[] = {
	gsSP1Triangle(0, 3, 1, 0),
	gsSP1Triangle(1, 3, 4, 0),
	gsSP1Triangle(1, 4, 2, 0),
	gsSP1Triangle(2, 4, 5, 0),
	gsSP1Triangle(3, 6, 4, 0),
	gsSP1Triangle(4, 6, 7, 0),
	gsSP1Triangle(4, 7, 5, 0),
	gsSP1Triangle(5, 7, 8, 0),
	gsSPEndDisplayList()
};
