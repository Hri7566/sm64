/********************************************************************************
	basis.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1995-1997 Nintendo co., ltd.  All rights reserved

	May 27, 1997
 ********************************************************************************/

#ifndef BASIS_H
#define	BASIS_H

#define	TRUE		1
#define	FALSE		0

#define	DBLBUF		2
#define TRIBUF		3

#define	nobreak

#if DEVELOP
#define	rmonpf(params)	osSyncPrintf params
#else
#define	rmonpf(f)		{}
#endif



#define	chkflag(flag,bits)		((flag)&(bits))
#define	setflag(flag,bits)		((flag)|=(bits))
#define	clrflag(flag,bits)		((flag)&=~(bits))
#define	negflag(flag,bits)		((flag)^=(birs))

#define	makebit(shift)			(1<<(shift))


#define	LoWord(n)				 ((n)&0x00000000ffffffff)
#define	HiWord(n)				(((n)&0xffffffff00000000)>>32)

#define	LoHalf(n)				 ((n)&0x0000ffff)
#define	HiHalf(n)				(((n)&0xffff0000)>>16)

#define	LoByte(n)				 ((n)&0x00ff)
#define	HiByte(n)				(((n)&0xff00)>>8)



typedef	unsigned char		uchar;
typedef unsigned short		ushort;
typedef unsigned int		uint;
typedef unsigned long		ulong;
typedef long long			wlong;
typedef unsigned long long	uwlong;


typedef union {
	ulong	word;
	ushort	half[2];
	uchar	byte[4];	
} Packed, UPacked;

typedef union {
	long	word;
	short	half[2];
	char	byte[4];	
} SPacked;


typedef char	*String;
typedef char	*MemPtr;		/* memory pointer				*/
typedef Gfx		*GfxPtr;
typedef Mtx		*MtxPtr;
typedef Vtx		*VtxPtr;
typedef	OSTask	*TaskPtr;
typedef Light	*LightPtr;

#endif
