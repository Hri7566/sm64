#!/usr/bin/perl -w 

die "Usage: zh_genmap16.pl <input Chinese RGB24 texture in Photoshop raw format> <output>\n       See README for more detail.\n" unless $#ARGV==1;

$in = shift;
$out = shift;
$idx=0;
$cnt=0;

open(INFILE, "<$in") or die "Can't open $in: $!";
open(OUTFILE, ">$out") or die "Can't open $out: $!";

while( read(INFILE, $byte, 4) != 0 ) {
    $r = 31.0 / 255.0 * vec($byte, 0, 8);
    $g = 31.0 / 255.0 * vec($byte, 1, 8);
    $b = 31.0 / 255.0 * vec($byte, 2, 8);
    $alpha = 31.0 / 255.0 * vec($byte, 3, 8);
    if ($alpha<1.0) { 
        $rgb = 0; 
    } else {
        $rgb = ($r<<11 & 0xf800) | ($g<<6 & 0x07c0) | ($b<<1 & 0x003e) | 1;
    } 

    if ($idx%256==0) {
        printf OUTFILE "static unsigned short c_%d_%d_txt[] = {\n", $cnt/4, $cnt%4;
        $cnt ++;
    }
    printf OUTFILE "0x%04x, ", $rgb;
    $idx++;
    if ($idx%16==0) {
        printf OUTFILE "\n";
    }
    if ($idx%256==0) {
        printf OUTFILE "};\n";
    }
}
close INFILE;
close OUTFILE;

print $cnt/4, " characters with ", $cnt*0.5, "K bytes\n";

