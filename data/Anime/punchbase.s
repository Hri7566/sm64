/********************************************************************************
						Ultra 64 MARIO Brothers

					mario punching base animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							February 6, 1996
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioPunchBase:
	.half	 MAP_ANIM_ONETIME 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	 5 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x0296						/* total bytes       */


	.half	    5,    1 	/* chn14_2translate x */
	.half	    5,    6 	/* chn14_2 translate y */
	.half	    1,   11 	/* chn14_2 translate z */
	.half	    1,    0 	/* chn14_2 rotate x */
	.half	    1,   12 	/* chn14_2 rotate y */
	.half	    1,    0 	/* chn14_2 rotate z */
	.half	    5,  178 	/* <m_waist>1_3 rotate x */
	.half	    5,  183 	/* <m_waist>1_3 rotate y */
	.half	    5,  188 	/* <m_waist>1_3 rotate z */
	.half	    5,  163 	/* <m_body>1 rotate x */
	.half	    5,  168 	/* <m_body>1 rotate y */
	.half	    5,  173 	/* <m_body>1 rotate z */
	.half	    5,  148 	/* <m_head>2 rotate x */
	.half	    5,  153 	/* <m_head>2 rotate y */
	.half	    5,  158 	/* <m_head>2 rotate z */
	.half	    1,  144 	/* chn6 rotate x */
	.half	    1,  145 	/* chn6 rotate y */
	.half	    1,  146 	/* chn6 rotate z */
	.half	    5,   20 	/* <m_larmA>1 rotate x */
	.half	    5,   25 	/* <m_larmA>1 rotate y */
	.half	    5,   30 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1 rotate x */
	.half	    1,    0 	/* <m_larmB>1 rotate y */
	.half	    5,   15 	/* <m_larmB>1 rotate z */
	.half	    1,    0 	/* <m_lhand>1 rotate x */
	.half	    1,   13 	/* <m_lhand>1 rotate y */
	.half	    1,   14 	/* <m_lhand>1 rotate z */
	.half	    1,  140 	/* chn10 rotate x */
	.half	    1,  141 	/* chn10 rotate y */
	.half	    1,  142 	/* chn10 rotate z */
	.half	    5,   55 	/* <m_rarmA>1 rotate x */
	.half	    5,   60 	/* <m_rarmA>1 rotate y */
	.half	    5,   65 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1 rotate y */
	.half	    5,   50 	/* <m_rarmB>1 rotate z */
	.half	    5,   35 	/* <m_rhand>1 rotate x */
	.half	    5,   40 	/* <m_rhand>1 rotate y */
	.half	    5,   45 	/* <m_rhand>1 rotate z */
	.half	    1,    0 	/* chn15 rotate x */
	.half	    1,    0 	/* chn15 rotate y */
	.half	    1,  143 	/* chn15 rotate z */
	.half	    5,  125 	/* <m_llegA>1 rotate x */
	.half	    5,  130 	/* <m_llegA>1 rotate y */
	.half	    5,  135 	/* <m_llegA>1 rotate z */
	.half	    1,    0 	/* <m_llegB>1 rotate x */
	.half	    1,    0 	/* <m_llegB>1 rotate y */
	.half	    5,  120 	/* <m_llegB>1 rotate z */
	.half	    5,  105 	/* <m_ltoot>1 rotate x */
	.half	    5,  110 	/* <m_ltoot>1 rotate y */
	.half	    5,  115 	/* <m_ltoot>1 rotate z */
	.half	    1,    0 	/* chn17 rotate x */
	.half	    1,    0 	/* chn17 rotate y */
	.half	    1,  147 	/* chn17 rotate z */
	.half	    5,   90 	/* <m_rlegA>1 rotate x */
	.half	    5,   95 	/* <m_rlegA>1 rotate y */
	.half	    5,  100 	/* <m_rlegA>1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1 rotate y */
	.half	    5,   85 	/* <m_rlegB>1 rotate z */
	.half	    5,   70 	/* <m_rfoot>1 rotate x */
	.half	    5,   75 	/* <m_rfoot>1 rotate y */
	.half	    5,   80 	/* <m_rfoot>1 rotate z */

	.half	0x0000,0x0000,0x0000,0xFFFF,0xFFFF,0x0000,0x00B2,0x00B2,0x0094,0x008F,0x0094,0x0016,0x3FFF,0xF97C,0xED8C,0xDB6C
	.half	0xDB6C,0xDB6C,0xDB6C,0xD702,0x4424,0x4424,0x4424,0x4424,0x2954,0xDA67,0xDA67,0xDA67,0xDA67,0xD977,0x879E,0x879E
	.half	0x879E,0x879E,0xA3C7,0x0EAF,0x1210,0x17E6,0x19C0,0x1351,0x0739,0x09DD,0x0E9A,0x10AC,0x0D1F,0xE375,0xE2C0,0xE1B6
	.half	0xE1F5,0xE4B1,0xDDDE,0xE2B6,0xEBE6,0xF16C,0xEF76,0x6826,0x64D5,0x5C28,0x4807,0x5CAA,0x2E3A,0x295B,0x2389,0x2888
	.half	0x2653,0xED13,0xFDDA,0x1515,0x13CF,0x25CC,0x0000,0x0000,0xFFFF,0x0000,0x000C,0xFF30,0xFF30,0xFF30,0xFF30,0xFF33
	.half	0xC9D8,0xC9FB,0xCA1F,0xC9D8,0xC75E,0x47D8,0x4720,0x4578,0x43A4,0x4138,0x103A,0x0FC5,0x0E83,0x0CA3,0x07B9,0x10A4
	.half	0x1041,0x0F96,0x0F61,0x11B4,0xBAF8,0xBDFC,0xC3EB,0xC81D,0xC211,0x0000,0xFB5A,0x00D4,0xFFC4,0xFDF0,0x0000,0xFFC3
	.half	0xFCF4,0xFE38,0xFFF2,0xBF40,0xBDFF,0xC680,0xC36D,0xBDF0,0x1482,0x1F55,0x2C26,0x2E3F,0x2DC8,0xED74,0xF27F,0xF80A
	.half	0xF85B,0xF782,0xEF2B,0xF728,0x00E9,0x03EB,0x05AE,0xAA21,0xA104,0x968F,0x9545,0x9617,0x8001,0x8001,0x4171,0xBF5F
	.half	0x8001,0x8001,0x4171,0xBF5F,0x1B73,0x0D8F,0xF103,0xD975,0xCD8E,0xF9F2,0xFBDC,0xFF68,0x014D,0xFF7C,0x0679,0x0329
	.half	0xFC48,0xF66B,0xF2F7,0xEA59,0xF145,0xFFEF,0x0D2B,0x1727,0x0904,0x02A1,0xF5E8,0xEC7D,0xEA8B,0xF42A,0xF969,0x0413
	.half	0x0C90,0x0F98,0xEBBC,0xF50B,0x07A7,0x159B,0x191F,0xFDDB,0xFFED,0x03CC,0x05FA,0x0453,0x3CB1,0x3F97,0x45D8,0x4BCB
	.half	0x5144
