/********************************************************************************
	skin.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1995 Nintendo co., ltd.  All rights reserved

	November 20, 1995
 ********************************************************************************/

#ifndef SKIN_H
#define	SKIN_H


extern void
gdm_init(uint *memory, uint size);

extern void
gdm_addtempheap(void *, uint);

extern void
gdm_setup(void);

extern void
gdm_maketestdl(int number);

extern GfxPtr
gdm_gettestdl(int number);

extern void
gdm_updatebuffers(void);

extern void
gdm_readcontroller(OSContPad *cont);

extern uint
gdm_getsoundflags(void);

#endif
