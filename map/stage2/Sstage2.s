/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 2 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage02

/********************************************************************************/
/*	Stage 2 construction sequence.												*/
/********************************************************************************/

SEQ_DoStage02:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1, _GfxStage2SegmentRomStart, _GfxStage2SegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)

		seqBeginScene(1, RCP_Stage2Scene1)

			seqMapInfo(test_info)

		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 0, 1000,5000,1000)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
