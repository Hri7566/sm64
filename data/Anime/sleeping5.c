/********************************************************************************
						Ultra 64 MARIO Brothers

					 mario sleeping animation data 5

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							July 19, 1995
 ********************************************************************************/

#include "../headers.h"


static short mario_sleep5_prm[]={
	    0,   53, -166,16383, 2070,-7547,-5585,-2585,
	28291,-24660,-44730,-3259, 9041,-8314,-1437, 4892,
	-5044,-8016,  -72, -195,-10213,  870, 3298, 3838,
	-27107,-12612, 1808,-3735,-3463,-26821,38180,23841,
	18270,-16545,37054,31111,19340,-16545, -650, -646,
	 -641, -635, -629, -622, -615, -608, -601, -594,
	 -588, -582, -577, -573, -569, -567, -567, -567,
	 -568, -570, -573, -575, -579, -583, -587, -591,
	 -596, -601, -606, -610, -615, -619, -624, -627,
	 -631, -634, -636, -638, -639, -640, -274, -282,
	 -290, -300, -309, -320, -330, -340, -350, -359,
	 -368, -376, -383, -389, -394, -396, -397, -397,
	 -395, -391, -387, -381, -375, -368, -360, -352,
	 -344, -335, -326, -318, -309, -301, -294, -287,
	 -280, -275, -270, -267, -265, -264, 9944, 9814,
	 9665, 9500, 9321, 9135, 8944, 8753, 8564, 8384,
	 8214, 8060, 7925, 7813, 7728, 7675, 7656, 7668,
	 7705, 7763, 7840, 7934, 8042, 8163, 8295, 8434,
	 8579, 8727, 8876, 9025, 9169, 9309, 9440, 9561,
	 9670, 9764, 9841, 9899, 9935, 9948, -148,  163,
	 3992,10600
};
static short mario_sleep5_tbl[]={
	    1,    0,	/* chn14_4translate x */
	    1,    1,	/* chn14_4 translate y */
	    1,    2,	/* chn14_4 translate z */
	    1,    0,	/* chn14_4 rotate x */
	    1,    3,	/* chn14_4 rotate y */
	    1,    0,	/* chn14_4 rotate z */
	    1,    0,	/* <m_waist>1_3 rotate x */
	    1,    0,	/* <m_waist>1_3 rotate y */
	    1,  161,	/* <m_waist>1_3 rotate z */
	    1,  158,	/* <m_body>1 rotate x */
	    1,  159,	/* <m_body>1 rotate y */
	    1,  160,	/* <m_body>1 rotate z */
	   40,   38,	/* <m_head>2 rotate x */
	   40,   78,	/* <m_head>2 rotate y */
	   40,  118,	/* <m_head>2 rotate z */

	    1,   34,	/* chn6 rotate x */
	    1,   35,	/* chn6 rotate y */
	    1,   36,	/* chn6 rotate z */
	    1,    8,	/* <m_larmA>1 rotate x */
	    1,    9,	/* <m_larmA>1 rotate y */
	    1,   10,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	    1,    7,	/* <m_larmB>1 rotate z */
	    1,    4,	/* <m_lhand>1 rotate x */
	    1,    5,	/* <m_lhand>1 rotate y */
	    1,    6,	/* <m_lhand>1 rotate z */

	    1,   30,	/* chn10 rotate x */
	    1,   31,	/* chn10 rotate y */
	    1,   32,	/* chn10 rotate z */
	    1,   15,	/* <m_rarmA>1 rotate x */
	    1,   16,	/* <m_rarmA>1 rotate y */
	    1,   17,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	    1,   14,	/* <m_rarmB>1 rotate z */
	    1,   11,	/* <m_rhand>1 rotate x */
	    1,   12,	/* <m_rhand>1 rotate y */
	    1,   13,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,   33,	/* chn15 rotate z */
	    1,   27,	/* <m_llegA>1 rotate x */
	    1,   28,	/* <m_llegA>1 rotate y */
	    1,   29,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	    1,   26,	/* <m_llegB>1 rotate z */
	    1,    0,	/* <m_ltoot>1 rotate x */
	    1,    0,	/* <m_ltoot>1 rotate y */
	    1,   25,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,   37,	/* chn17 rotate z */
	    1,   22,	/* <m_rlegA>1 rotate x */
	    1,   23,	/* <m_rlegA>1 rotate y */
	    1,   24,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	    1,   21,	/* <m_rlegB>1 rotate z */
	    1,   18,	/* <m_rfoot>1 rotate x */
	    1,   19,	/* <m_rfoot>1 rotate y */
	    1,   20,	/* <m_rfoot>1 rotate z */

};
AnimeRecord animMarioSleeping5 = {
	 MAP_ANIM_NORMAL,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	40,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_sleep5_prm,
	mario_sleep5_tbl
};
