/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 16 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
*********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage16


/* ================================================================================
		: Ports sequence.
===================================================================================	*/
SEQ_Stage16_Port:

/*	Trip door ports.	*/

		seqPort(0, 6, 1, 0)			/*	Trip door 0. To St6. Select room 1F.	*/
		seqPort(1, 6, 1, 1)			/*	Trip door 1.			"				*/
		seqPort(2, 6, 3, 2)			/*	Trip door 2. To St6. Select room B1.	*/


/*	Missed only port.	*/

		seqActor(S_NULL,   0,900,-1710,   0,180,0,   0,3,0,  e_player_downing)
		seqPort(3, 16, 1, 3)		/*	Missed by St6, St16, St26.	*/


/*	Enter only port.	*/

		seqActor(S_NULL,  -1328,260,4664,  0,180,0,  0,4,0,	 e_player_rolling)
		seqPort(4, 16, 1, 4)


/*	With St18 [ Ext7 Hori mini ].	*/

		seqActor(S_NULL,  -3379, -815,-2025,  0,  0,0,  60,5,0,  e_tripchimney2	  )
		seqActor(S_NULL,  -3379, -500,-2025,  0,180,0,   0,6,0,  e_player_pushdown)
		seqActor(S_NULL,  -3799,-1199,-5816,  0,  0,0,   0,7,0,  e_player_swimming)
		seqActor(S_NULL,  -3379, -500,-2025,  0,180,0,   0,8,0,  e_player_pushout )
		seqPort(5, 18, 1, 10)		/*	To St18 [ Ext7. Hori mini ].		*/
		seqPort(6, 16, 1,  6)		/*	St18	[ Ext7. Hori mini ] missed.	*/
		seqPort(7, 16, 1,  7)		/*	Exit from the St18.					*/
		seqPort(8, 16, 1,  8)		/*	St18	[ Ext7. Hori mini ] clear.	*/

/*	With St31 [ Ext8 Blue sky ].	*/

		seqActor(S_NULL,  5408,4500,3637,  0,225,0,  0,10,0,	e_player_landing)
		seqPort(10, 16, 1, 10)		/*	St31 [ Ext8 Blue sky ] missed.	*/


/*	With St28 [ Ext5 In Fall ].		*/

		seqActor(S_NULL,  -6901,2376,-6509,  0,230,0,  0,20,0,  e_player_landing)
		seqPort(20, 16, 1, 20)		/*	From the St28 [ Ext 5. In Fall. ].	*/




/*	With St23 [ Submarine ].		*/

		seqActor(S_NULL,  4997,-1250,2258,  0,210,0,  0,30,0,  e_player_swimming)
		seqPort(30, 16, 1, 30)		/*	From the St23 [ Submarine ].	*/

		seqReturn


/* ================================================================================
		: BG parts sequence.
===================================================================================	*/
SEQ_Stage16_BGParts:

		seqActor(S_NULL,  -5812,100,-5937,  0,0,0,  0,0,0,  e_falls)

		seqActor(S_NULL,  -7430,1500,  873,  0,0,0,  0,0,0,  e_birdsing)
		seqActor(S_NULL,    -80,1500, 5004,  0,0,0,  0,1,0,  e_birdsing)
		seqActor(S_NULL,   7131,1500,-2989,  0,0,0,  0,2,0,  e_birdsing)
		seqActor(S_NULL,  -7430,1500,-5937,  0,0,0,  0,0,0,  e_soyokaze)

		seqActor(S_movebg01,  0,0,0,  0,0,0,  0,0,0,  e_maincastle_net)
		seqActor(S_NULL,      0,0,0,  0,0,0,  0,0,0,  e_maincastle_waterswitch)

		seqActor(S_hiteffect,  -4878,-787,-5690,  0,0,0,  0,0,0,  e_fall_effect) 
		seqActor(S_hiteffect,  -4996,-787,-5548,  0,0,0,  0,1,0,  e_fall_effect) 
		seqActor(S_hiteffect,  -5114,-787,-5406,  0,0,0,  0,2,0,  e_fall_effect)
		seqActor(S_hiteffect,  -5212,-787,-5219,  0,0,0,  0,3,0,  e_fall_effect)
		seqActor(S_hiteffect,  -5311,-787,-5033,  0,0,0,  0,4,0,  e_fall_effect)
		seqActor(S_hiteffect,  -5419,-787,-4895,  0,0,0,  0,5,0,  e_fall_effect)
		seqActor(S_hiteffect,  -5527,-787,-4757,  0,0,0,  0,6,0,  e_fall_effect)
		seqActor(S_hiteffect,  -5686,-787,-4733,  0,0,0,  0,7,0,  e_fall_effect)
		seqActor(S_hiteffect,  -5845,-787,-4710,  0,0,0,  0,8,0,  e_fall_effect)

		seqActor(S_NULL,   	    5223,-975, 1667,  0,0,0,  0,0,0,  e_fishmother_many)

		seqActor(S_minibird,  -5069,850, 3221,  0,0,0,  0,1,0,  e_minibird)
		seqActor(S_minibird,  -4711,742,  433,  0,0,0,  0,1,0,  e_minibird)
		seqActor(S_minibird,   5774,913,-1114,  0,0,0,  0,1,0,  e_minibird)
		seqActor(S_NULL, -1328,260,4664,  0,180,0,  0,40,0,    e_demo_producer)

		seqActor(S_movebg03,  0,0,0,  0,0,0,  0,0,0,  e_main_net2)

		seqActor(S_camera_jugem,   11,803,-3015,  0,0,0,  0,1,0,  e_camera_jugem)

		seqReturn


/* ================================================================================
		: Move BGs sequence.
===================================================================================	*/
SEQ_Stage16_MoveBGs:

		seqActor(S_movebg02,  -3213,3348,-3011,  0,0,0,  0,0,0,  e_castle_flag)
		seqActor(S_movebg02,   3213,3348,-3011,  0,0,0,  0,0,0,  e_castle_flag)
		seqActor(S_movebg02,  -3835,3348,-6647,  0,0,0,  0,0,0,  e_castle_flag)
		seqActor(S_movebg02,   3835,3348,-6647,  0,0,0,  0,0,0,  e_castle_flag)

		seqReturn


/* ================================================================================
		: Enemys sequence.
===================================================================================	*/
SEQ_Stage16_Enemys:

		seqActor(S_butterfly,   -4508,  406,  4400,  0,0,0,  0,0,0,  e_butterfly	)
		seqActor(S_butterfly,   -4408,  406,  4500,  0,0,0,  0,0,0,  e_butterfly	)
		seqActor(S_butterfly,   -4708,  406,  4100,  0,0,0,  0,0,0,  e_butterfly	)
		seqActor(S_butterfly,   -6003,  473, -2621,  0,0,0,  0,0,0,  e_butterfly	)
		seqActor(S_butterfly,   -6003,  473, -2321,  0,0,0,  0,0,0,  e_butterfly	)
		seqActor(S_butterfly,    6543,  461,  -617,  0,0,0,  0,0,0,  e_butterfly	)
		seqActor(S_butterfly,    6143,  461,  -617,  0,0,0,  0,0,0,  e_butterfly	)
		seqActor(S_butterfly,    5773,  775, -5722,  0,0,0,  0,0,0,  e_butterfly	)
		seqActor(S_butterfly,    5873,  775, -5622,  0,0,0,  0,0,0,  e_butterfly	)
		seqActor(S_butterfly,    5473,  775, -5322,  0,0,0,  0,0,0,  e_butterfly	)
		seqActor(S_butterfly,   -1504,  326,  3196,  0,0,0,  0,0,0,  e_butterfly	)
		seqActor(S_butterfly,   -1204,  326,  3296,  0,0,0,  0,0,0,  e_butterfly	)

		seqActor(S_yoshi	,       0, 3174, -5625,  0,0,0,  0,0,0,  e_yoshi		)

		seqReturn


/* ================================================================================
		: Stage 16 main sequence.
===================================================================================	*/
SEQ_DoStage16:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  ,  _GfxStage16SegmentRomStart		,  _GfxStage16SegmentRomEnd		)
	seqLoadPres(SEGMENT_BACK  	  , _BackMainmapSegmentRomStart		, _BackMainmapSegmentRomEnd		)
	seqLoadText(SEGMENT_TEXTURE	  , _KCastleInTextureSegmentRomStart, _KCastleInTextureSegmentRomEnd)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_jSegmentRomStart	    , _GfxEnemy1_jSegmentRomEnd		)	
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_jSegmentRomStart	    , _HmsEnemy1_jSegmentRomEnd		)	
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_fSegmentRomStart	    , _GfxEnemy2_fSegmentRomEnd		)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_fSegmentRomStart	    , _HmsEnemy2_fSegmentRomEnd		)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_j)
		seqCall(SetEnemy2_f)

		seqHmsShape(S_bg01 , RCP_HmsMainTower)
		seqHmsShape(S_tree1, RCP_HmsMainTree )
		seqHmsShape(S_bg20 , RCP_HmsDokan	 )

		seqHmsShape(S_tripdoor0, RCP_HmsMainDoor )
		seqHmsShape(S_tripdoor3, RCP_HmsDoor3	 )

		seqHmsShape(S_movebg01, RCP_HmsMainNet		)
		seqHmsShape(S_movebg02, RCP_Hmscastle_flag	)
		seqHmsShape(S_movebg03, RCP_HmsMainNet2		)

		seqBeginScene(1, RCP_Stage16Scene1)
			seqGameOver(16, 1, 3)
			seqCall(SEQ_Stage16_Port)
			seqCall(SEQ_Stage16_BGParts)
			seqCall(SEQ_Stage16_MoveBGs)
			seqCall(SEQ_Stage16_Enemys)
			seqMapInfo(cx1601_info)
			seqTagInfo(cx1601_info_tag)
			seqSetMusic(NA_STG_GROUND, 0)
			seqEnvironment(ENV_PLAIN)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 180, -1328,260,4664)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
