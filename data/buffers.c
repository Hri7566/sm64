/********************************************************************************
						Ultra 64 MARIO Brothers

						buffer definition module

			Copyright 1996,1997 Nintendo co., ltd.  All rights reserved

							May 27, 1997
 ********************************************************************************/

#include "../headers.h"

uwlong	 bootThreadStack[BOOT_STACKSIZE64];		/* boot thread stack memory		*/
uwlong	 idleThreadStack[IDLE_STACKSIZE64];		/* idel thread stack memory		*/
uwlong	 mainThreadStack[MAIN_STACKSIZE64];		/* main thread stack memory		*/
uwlong	audioThreadStack[MAIN_STACKSIZE64];		/* audio thread stack memory	*/
uwlong	graphThreadStack[MAIN_STACKSIZE64];		/* graphic thread stack memory	*/
uwlong	motorThreadStack[MAIN_STACKSIZE64];		/* motor thread stack memory	*/		/* MOTOR 1997.5.27 */

uwlong  rspDramStack[SP_DRAM_STACK_SIZE64];		/* RSP tasks stack				*/
uwlong	datYieldBuffer[DAT_YIELD_SIZE64];

BackupRecord  backupMemory;						/* Backup EEROM I/O buffer		*/ 
DynamicRecord dynamicData[DBLBUF];				/* dynamic data memory			*/
