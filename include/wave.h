/********************************************************************************
						Ultra 64 MARIO Brothers

						 Header file of wave.c.

			Copyright 1995 Nintendo co., ltd.  All rights reserved

				  This module was programmed by Y.Tanimoto

							Mar 1, 1996
*********************************************************************************/

#ifndef	_WAVE_HEAD
#define	_WAVE_HEAD

#define		WAVE_ST_MV_ST	10				//	static --> move --> static type wave.
#define		WAVE_MV_MV_MV	20				//	move   --> move --> move   type wave.

#define		WAVE_XO_LMR		30
#define		WAVE_XO_CMP		40
#define		WAVE_XO_CST		50

#define		WAVE_YO_MHT		60
#define		WAVE_YO_CMP		70
#define		WAVE_YO_CST		80

#define		WAVE_T_CLEAR		100
#define		WAVE_T_NOTCLEAR		200

#define		WAVE_NRMMAP		0
#define		WAVE_ENVMAP		1

#define		WAVE_OFF		0
#define		WAVE_SMALL		1
#define		WAVE_BIG		2

#define		WAVE_IN_LEFT		0x20		//		00 100 000
#define		WAVE_IN_MIDDLE		0x10		//		00 010 000
#define		WAVE_IN_RIGHT		0x08		//		00 001 000

#define		WAVE_OUT_LEFT		0x04		//		00 000 100
#define		WAVE_OUT_MIDDLE		0x02		//		00 000 010
#define		WAVE_OUT_RIGHT		0x01		//		00 000 001

#define		WAVE_AMB_R		80
#define		WAVE_AMB_G		80
#define		WAVE_AMB_B		80

#define		WAVE_LIGHT_X	50
#define		WAVE_LIGHT_Y	50
#define		WAVE_LIGHT_Z	50


/* ============================================================================================
		: Prototype function.
===============================================================================================	*/
extern ulong WaveMove(int code, MapNode *node, void *data);
extern ulong WaveInit(int code, MapNode *node, void *data);


/* ============================================================================================
		: Extern variables.
===============================================================================================	*/
extern short wave_vp_all[];
extern short wave_pp_all[];


/* ============================================================================================
		: Define structures used only in wave routines.
===============================================================================================	*/
typedef	struct	{
	short	x  ;
	short	y  ;
	short	z  ;
	char	nx ;
	char	ny ;
	char	nz ;
} WavePointData ;		//	13[bytes]

typedef	struct	{
	float	nx ;
	float	ny ;
	float	nz ;
} WavePolyNormal ;		//	12[bytes]

typedef struct	{
	short		checkCode	 ;
	char		ndatas		 ;
	char		envMap		 ;		//	0 --> normal mapping.	1 --> environment mapping.
	char		bitSWbef	 ;
	char		bitSWnow	 ;
	char		bitSWtrig	 ;
	char		waveSW		 ;
	float		rotateX		 ;
	float		rotateY		 ;
	float		transX		 ;
	float		transY		 ;
	float		transZ		 ;
	float		waveA		 ; 
	float		waveAs		 ;
	float		waveAb		 ;
	float		waveK		 ;
	float		waveKs		 ;
	float		waveKb		 ;
	float		waveF		 ;
	float		waveFs		 ;
	float		waveFb		 ;
	float		waveV		 ;
	float		waveVs		 ;
	float		waveVb		 ;
	float		waveT		 ;
	float		waveXo		 ;
	float		waveYo		 ;
	Gfx*		steady		 ;
	short**		hexaXX_iRptr ;
	ushort**	image		 ;
	short		iwidth		 ;
	short		iheight		 ;
	Gfx*		tile_set	 ;
	char		w_type		 ;			//	See above.
	uchar		alpha		 ;			//	255 --> RM_SURF. others --> RM_XSURF.
	char		bitSW_Mh_bef ;
	char		bitSW_Mh_now ;
	char		bitSW_Mh_trg ;
	float		one_side	 ;			//	Original size = 614.0.
} WaveData ;							//	[ 121 bytes. ]

extern WaveData*	wavePtr ;
extern char			wave_dummy_backup ;


#endif
