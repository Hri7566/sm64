/********************************************************************************
						Ultra 64 MARIO Brothers

				  stage34 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							Decemver 11, 1995
*********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE34_FOG_R		0
#define		STAGE34_FOG_G		0
#define		STAGE34_FOG_B		0
#define		STAGE34_FOG_START	980

#include "kopa_round3/kopa3_ext_texture.h"

#include "kopa_round3/kopa3_A_shape.sou"
#include "kopa_round3/kopa3_B_shape.sou"
#include "kopa_round3/kopa3_C_shape.sou"
#include "kopa_round3/kopa3_D_shape.sou"
#include "kopa_round3/kopa3_E_shape.sou"
#include "kopa_round3/kopa3_F_shape.sou"
#include "kopa_round3/kopa3_G_shape.sou"
#include "kopa_round3/kopa3_H_shape.sou"
#include "kopa_round3/kopa3_I_shape.sou"
#include "kopa_round3/kopa3_J_shape.sou"
#include "kopa_round3/kopa3_star_shape.sou"
#include "kopa_round3/kopa3_buoy_shape.sou"

#include "kopa_round3/cx3401.flk"
#include "kopa_round3/kopa3_A.flk"
#include "kopa_round3/kopa3_B.flk"
#include "kopa_round3/kopa3_C.flk"
#include "kopa_round3/kopa3_D.flk"
#include "kopa_round3/kopa3_E.flk"
#include "kopa_round3/kopa3_F.flk"
#include "kopa_round3/kopa3_G.flk"
#include "kopa_round3/kopa3_H.flk"
#include "kopa_round3/kopa3_I.flk"
#include "kopa_round3/kopa3_J.flk"
