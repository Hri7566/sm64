/********************************************************************************
						Ultra 64 MARIO Brothers

				   mario electric shoking animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							January 11, 1996
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioShocking:
	.half	 MAP_ANIM_NORMAL 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	 6 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x02A6						/* total bytes       */


	.half	    1,    0 	/* chn14translate x */
	.half	    1,    1 	/* chn14 translate y */
	.half	    6,    2 	/* chn14 translate z */
	.half	    1,    0 	/* chn14 rotate x */
	.half	    1,    8 	/* chn14 rotate y */
	.half	    1,    0 	/* chn14 rotate z */
	.half	    1,    0 	/* <m_waist>1 rotate x */
	.half	    6,  189 	/* <m_waist>1 rotate y */
	.half	    6,  195 	/* <m_waist>1 rotate z */
	.half	    6,  171 	/* <m_body>1 rotate x */
	.half	    6,  177 	/* <m_body>1 rotate y */
	.half	    6,  183 	/* <m_body>1 rotate z */
	.half	    1,    0 	/* <m_head>1 rotate x */
	.half	    1,    0 	/* <m_head>1 rotate y */
	.half	    6,  165 	/* <m_head>1 rotate z */
	.half	    1,  161 	/* chn6 rotate x */
	.half	    1,  162 	/* chn6 rotate y */
	.half	    1,  163 	/* chn6 rotate z */
	.half	    6,   33 	/* <m_larmA>1 rotate x */
	.half	    6,   39 	/* <m_larmA>1 rotate y */
	.half	    6,   45 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1_1 rotate x */
	.half	    1,    0 	/* <m_larmB>1_1 rotate y */
	.half	    6,   27 	/* <m_larmB>1_1 rotate z */
	.half	    6,    9 	/* <m_lhand>1_1 rotate x */
	.half	    6,   15 	/* <m_lhand>1_1 rotate y */
	.half	    6,   21 	/* <m_lhand>1_1 rotate z */
	.half	    1,  157 	/* chn10 rotate x */
	.half	    1,  158 	/* chn10 rotate y */
	.half	    1,  159 	/* chn10 rotate z */
	.half	    6,   60 	/* <m_rarmA>1 rotate x */
	.half	    6,   66 	/* <m_rarmA>1 rotate y */
	.half	    6,   72 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1 rotate y */
	.half	    6,   54 	/* <m_rarmB>1 rotate z */
	.half	    1,   51 	/* <m_rhand>1_1 rotate x */
	.half	    1,   52 	/* <m_rhand>1_1 rotate y */
	.half	    1,   53 	/* <m_rhand>1_1 rotate z */
	.half	    1,    0 	/* chn15 rotate x */
	.half	    1,    0 	/* chn15 rotate y */
	.half	    1,  160 	/* chn15 rotate z */
	.half	    6,  139 	/* <m_llegA>1 rotate x */
	.half	    6,  145 	/* <m_llegA>1 rotate y */
	.half	    6,  151 	/* <m_llegA>1 rotate z */
	.half	    1,    0 	/* <m_llegB>1 rotate x */
	.half	    1,    0 	/* <m_llegB>1 rotate y */
	.half	    1,  138 	/* <m_llegB>1 rotate z */
	.half	    6,  120 	/* <m_ltoot>1 rotate x */
	.half	    6,  126 	/* <m_ltoot>1 rotate y */
	.half	    6,  132 	/* <m_ltoot>1 rotate z */
	.half	    1,    0 	/* chn17 rotate x */
	.half	    1,    0 	/* chn17 rotate y */
	.half	    1,  164 	/* chn17 rotate z */
	.half	    6,  102 	/* <m_rlegA>1 rotate x */
	.half	    6,  108 	/* <m_rlegA>1 rotate y */
	.half	    6,  114 	/* <m_rlegA>1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1 rotate y */
	.half	    6,   96 	/* <m_rlegB>1 rotate z */
	.half	    6,   78 	/* <m_rtoot>1 rotate x */
	.half	    6,   84 	/* <m_rtoot>1 rotate y */
	.half	    6,   90 	/* <m_rtoot>1 rotate z */

	.half	0x0000,0x00B4,0x0000,0x0020,0x001D,0x0008,0xFFF7,0xFFFF,0x3FFF,0xF43C,0xF066,0xEF29,0xEE78,0xEEC7,0xF43C,0xF0B3
	.half	0xE82E,0xE730,0xE7AF,0xE957,0xF0B3,0xEF35,0xF2F1,0xEEDE,0xE976,0xE641,0xEF35,0xE930,0xCFED,0xF16B,0xE998,0xD94C
	.half	0xE707,0xB613,0xBC17,0xC743,0xCD47,0xC1AD,0xB613,0xD647,0xD8EE,0xDDDB,0xE082,0xDB65,0xD647,0x270B,0x20D8,0x1555
	.half	0x0F22,0x1B17,0x270B,0x0523,0x1CCE,0xDE1E,0xEC72,0xCEB4,0xD181,0xDBC3,0xEAC0,0xEC72,0x3185,0x4F4E,0x75C9,0x5C8D
	.half	0x5FD9,0x3185,0x33A3,0x2452,0x3819,0x35DF,0x30D1,0x33A3,0x1462,0x34B3,0x4E41,0x3072,0x33CF,0x1462,0x01ED,0x039A
	.half	0x046C,0x03B4,0xFC22,0x01ED,0x04A0,0x0497,0x0488,0x045D,0x03AF,0x04A0,0xAEA8,0xAF10,0xB1C9,0xB4CC,0xB6F9,0xAEA8
	.half	0x2537,0x2B53,0x2A7D,0x2897,0x268D,0x254C,0x1714,0x1AE5,0x190C,0x12D1,0x0E60,0x15DE,0x112E,0x16A9,0x1B5D,0x16CB
	.half	0x1189,0x1110,0xBAFA,0xC3D2,0xBF29,0xB249,0xA937,0xB882,0xF6E3,0xF659,0xF575,0xF672,0xF77C,0xF6E3,0x03C3,0xFE06
	.half	0x00BA,0xFD60,0xFB06,0x03C3,0xD4BE,0xBCF5,0xC69F,0xB8F0,0xAFCD,0xD4BE,0x52B4,0x8AC1,0x8412,0x7C48,0x7D5F,0x81FB
	.half	0x8AC1,0x9169,0x90B7,0x8F4F,0x8CFE,0x8BBC,0x9169,0x0EAC,0x1263,0x1619,0x1A32,0x1CA4,0x0EAC,0x8001,0x8001,0x4171
	.half	0xBF5F,0x8001,0x8001,0x4171,0xBF5F,0x0074,0x1332,0x00F9,0xEFB6,0x0831,0x0074,0x0000,0x0000,0xF548,0x02DB,0x0F7A
	.half	0x0243,0x0000,0x0000,0x024B,0x0378,0x03AF,0x0076,0xF697,0xF21C,0xF95E,0xFA2A,0xF956,0xF6EB,0x0D09,0x0F47,0x1184
	.half	0x1184,0x1184,0x1184,0x40A1,0x3C5A,0x3FC9,0x448A,0x478E,0x4194
