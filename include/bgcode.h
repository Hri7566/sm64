/********************************************************************************
	player.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	January 9, 1996
 ********************************************************************************/

#ifndef BGCODE_H
#define	BGCODE_H

#define	BG_ATTR_NORMAL		 0

#define	BG_ATTR_LAKE		13
#define	BG_ATTR_RIVER		14

#define	BG_ATTR_ICE3		19
#define	BG_ATTR_ICE2		20
#define	BG_ATTR_ICE1		21

#define	BG_ATTR_SAND1		36
#define	BG_ATTR_SAND2		37
#define	BG_ATTR_SAND3		39

#endif
