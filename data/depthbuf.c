/********************************************************************************
						Ultra 64 MARIO Brothers

					 depth buffer definition module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

				This module was programmed by Y.Nishida

							October 06, 1995
 ********************************************************************************/

#include "../headers.h"

Pixmap	depthMemory;							/* depth buffer memory			*/
