/********************************************************************************
						Ultra 64 MARIO Brothers

					  title hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

				This module was programmed by Y.Nishida

							December 7, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx RCP_Nintendo[];
extern Gfx RCP_TitleSuperS[];
extern Gfx RCP_TitleSuperU[];
extern Gfx RCP_TitleSuperP[];
extern Gfx RCP_TitleSuperE[];
extern Gfx RCP_TitleSuperR[];
extern Gfx RCP_TitleMarioM[];
extern Gfx RCP_TitleMarioA[];
extern Gfx RCP_TitleMarioR[];
extern Gfx RCP_TitleMarioI[];
extern Gfx RCP_TitleMarioO[];

extern ulong TitleLGDraw(int, MapNode*, void*);
extern ulong TitleTMDraw(int, MapNode*, void*);
extern ulong TitleBGDraw(int, MapNode*, void*);
extern ulong GOverBGDraw(int, MapNode*, void*);
extern ulong VibLogoDraw(int, MapNode*, void*);

extern ulong ImageChangeFunc(int, MapNode*, void*);


/********************************************************************************/
/*	Nintendo log screen hierarchy data.											*/
/********************************************************************************/
Hierarchy RCP_NintendoLogo[] = {
	hmsScene(160, 120, 160, 120, 0)
	hmsBegin()

		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0,0,0,1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPersp(45, 128, 16384)
			hmsBegin()
				hmsCamera(0,  0,0,3200,  0,0,0, NULL)
				hmsBegin()
					hmsCProg(0, TitleLGDraw)
				hmsEnd()
			hmsEnd()
		hmsEnd()

		hmsLayer(0)
		hmsBegin()
			hmsCProg(0, TitleTMDraw)
		hmsEnd()

	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	title demo hierarchy data.													*/
/********************************************************************************/

Hierarchy RCP_MapTitle[] = {
	hmsScene(160, 120, 160, 120, 0)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsCProg(0, TitleBGDraw)
				hmsCProg(0, ImageChangeFunc)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPersp(45, 128, 16384)
			hmsBegin()
				hmsCProg(2, DoMarioFaceAnime)
			hmsEnd()
		hmsEnd()

#if !CHINA
		hmsLayer(0)
		hmsBegin()
			hmsCProg(0, VibLogoDraw)
		hmsEnd()
#endif

	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	game over demo hierarchy data.												*/
/********************************************************************************/

Hierarchy RCP_MapGmOver[] = {
	hmsScene(160, 120, 160, 120, 0)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsCProg(0, GOverBGDraw)
				hmsCProg(0, ImageChangeFunc)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPersp(45, 128, 16384)
			hmsBegin()
				hmsCProg(3, DoMarioFaceAnime)
			hmsEnd()
		hmsEnd()

#if !CHINA
		hmsLayer(0)
		hmsBegin()
			hmsCProg(1, VibLogoDraw)
		hmsEnd()
#endif

	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Debug select hierarchy map data.											*/
/********************************************************************************/

Hierarchy RCP_MapSelect[] = {
	hmsScene(160, 120, 160, 120, 0)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsCProg(0, TitleBGDraw)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPersp(45, 128, 16384)
			hmsBegin()
				hmsCamera(0,  0,0,1200,  0,0,0, NULL)
				hmsBegin()
					hmsGfxTrans(RM_SURF, RCP_TitleSuperS, -230,300,0)
					hmsGfxTrans(RM_SURF, RCP_TitleSuperU, -120,300,0)
					hmsGfxTrans(RM_SURF, RCP_TitleSuperP,  -20,300,0)
					hmsGfxTrans(RM_SURF, RCP_TitleSuperE,  100,300,0)
					hmsGfxTrans(RM_SURF, RCP_TitleSuperR,  250,300,0)
					hmsGfxTrans(RM_SURF, RCP_TitleMarioM, -310,100,0)
					hmsGfxTrans(RM_SURF, RCP_TitleMarioA,  -90,100,0)
					hmsGfxTrans(RM_SURF, RCP_TitleMarioR,   60,100,0)
					hmsGfxTrans(RM_SURF, RCP_TitleMarioI,  180,100,0)
					hmsGfxTrans(RM_SURF, RCP_TitleMarioO,  300,100,0)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
