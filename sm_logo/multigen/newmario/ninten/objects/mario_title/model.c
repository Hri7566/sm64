#include <mbi.h>
#include <gu.h>
#include <em.h>
#include <os.h>

#include "model.h"
#define NUM_MODELS	1

extern Gfx mario64_logo_model[];
extern Nin mario64_logo_model_data;
extern float mario64_logo_box[][3];

int numModels = NUM_MODELS;

Model models[] = {
	{ &mario64_logo_model_data,	(float *) mario64_logo_box },
};

