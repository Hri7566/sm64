/********************************************************************************
						Ultra 64 MARIO Brothers

					mario swim carry A animation data 2

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							February 1, 1996
 ********************************************************************************/

#include "../headers.h"


static short mario_swim_carryA_2_prm[]={
	    0,  189,16383, -290, -430,-10130,-9595,-10147,
	31003, 1919, 4669, 5453,-7088,-9178,-30398,-5774,
	-31561,  491,  494,  489,  452,  383,  303,  214,
	  121,   28,  -60,  390,  389,  392,  400,  415,
	  432,  451,  470,  490,  509,-4380,-4327,-4453,
	-5179,-6484,-8014,-9694,-11450,-13205,-14886, 3753,
	 3696, 3833, 4625, 6047, 7715, 9547,11460,13374,
	15205, 7064, 7000, 6855, 6704, 6573, 6439, 6304,
	 6169, 6033, 5898,  800,  840,  968, 1192, 1489,
	 1823, 2181, 2551, 2920, 3278,-11253,-11286,-11425,
	-11728,-12169,-12672,-13216,-13780,-14344,-14888, -189,
	 -190, -186, -165, -129,  -87,  -40,    8,   56,
	  103, -238, -238, -238, -237, -234, -230, -226,
	 -222, -218, -214, -195, -181, -402,-1247,-2678,
	-4344,-6165,-8065,-9964,-11785, 1533, 1492, 1666,
	 2503, 3971, 5688, 7570, 9535,11500,13382,-7903,
	-7849,-7723,-7577,-7436,-7287,-7133,-6977,-6821,
	-6667, -275, -313, -450, -725,-1113,-1553,-2027,
	-2519,-3011,-3485,-10926,-10936,-11037,-11339,-11823,
	-12383,-12994,-13629,-14264,-14874, 6603,-32552,22994,
	-16545,-8145,31328,23115,-16545,-11908,-11800,-11500,
	-11047,-10479,-9835,-9152,-8470,-7826,-7258, 4250,
	 4179, 3981, 3682, 3307, 2882, 2431, 1981, 1556,
	 1181,26691,26639,26497,26283,26014,25709,25386,
	25062,24757,24488
};
static short mario_swim_carryA_2_tbl[]={
	    1,    0,	/* chn14translate x */
	    1,    1,	/* chn14 translate y */
	    1,    0,	/* chn14 translate z */
	    1,    0,	/* chn14 rotate x */
	    1,    2,	/* chn14 rotate y */
	    1,    0,	/* chn14 rotate z */
	    1,    0,	/* <m_waist>1_3 rotate x */
	    1,    0,	/* <m_waist>1_3 rotate y */
	   10,  185,	/* <m_waist>1_3 rotate z */
	    1,    0,	/* <m_body>1 rotate x */
	    1,    0,	/* <m_body>1 rotate y */
	   10,  175,	/* <m_body>1 rotate z */
	    1,    0,	/* <m_head>2 rotate x */
	    1,    0,	/* <m_head>2 rotate y */
	   10,  165,	/* <m_head>2 rotate z */

	    1,  161,	/* chn6 rotate x */
	    1,  162,	/* chn6 rotate y */
	    1,  163,	/* chn6 rotate z */
	    1,    7,	/* <m_larmA>1 rotate x */
	    1,    8,	/* <m_larmA>1 rotate y */
	    1,    9,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	    1,    6,	/* <m_larmB>1 rotate z */
	    1,    3,	/* <m_rhand2>2 rotate x */
	    1,    4,	/* <m_rhand2>2 rotate y */
	    1,    5,	/* <m_rhand2>2 rotate z */

	    1,  157,	/* chn10 rotate x */
	    1,  158,	/* chn10 rotate y */
	    1,  159,	/* chn10 rotate z */
	    1,   14,	/* <m_rarmA>1 rotate x */
	    1,   15,	/* <m_rarmA>1 rotate y */
	    1,   16,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	    1,   13,	/* <m_rarmB>1 rotate z */
	    1,   10,	/* <m_rhand2>1 rotate x */
	    1,   11,	/* <m_rhand2>1 rotate y */
	    1,   12,	/* <m_rhand2>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  160,	/* chn15 rotate z */
	   10,  127,	/* <m_llegA>1 rotate x */
	   10,  137,	/* <m_llegA>1 rotate y */
	   10,  147,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	   10,  117,	/* <m_llegB>1 rotate z */
	   10,   87,	/* <m_ltoot>1 rotate x */
	   10,   97,	/* <m_ltoot>1 rotate y */
	   10,  107,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  164,	/* chn17 rotate z */
	   10,   57,	/* <m_rlegA>1 rotate x */
	   10,   67,	/* <m_rlegA>1 rotate y */
	   10,   77,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	   10,   47,	/* <m_rlegB>1 rotate z */
	   10,   17,	/* <m_rfoot>1 rotate x */
	   10,   27,	/* <m_rfoot>1 rotate y */
	   10,   37,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioSwimCarryA2 = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	10,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_swim_carryA_2_prm,
	mario_swim_carryA_2_tbl
};
