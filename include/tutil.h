/********************************************************************************
						Ultra 64 MARIO Brothers

						Header file of tutil.c.

			Copyright 1995 Nintendo co., ltd.  All rights reserved

				  This module was programmed by Y.Tanimoto

								1996.1.12
*********************************************************************************/

#ifndef	_TUTIL_HEAD
#define	_TUTIL_HEAD


#define	G_CC_DECALRGB_ENVA				     0, 	0, 			  0, TEXEL0,		 0, 0, 0, ENVIRONMENT
#define	G_CC_DECALRGB_TEXELENVA		         0, 	0,			  0, TEXEL0,	TEXEL0, 0, ENVIRONMENT, 0
#define	G_CC_MODULATERGB_ENVA			TEXEL0, 	0, 		  SHADE, 	  0,		 0, 0, 0, ENVIRONMENT
#define	G_CC_MODULATERGB_TEXELENVA		TEXEL0, 	0, 		  SHADE, 	  0,	TEXEL0, 0, ENVIRONMENT, 0
#define	G_CC_BLENDRGBA_ENVA		    	TEXEL0, SHADE, TEXEL0_ALPHA,  SHADE,    	 0, 0, 0, ENVIRONMENT
#define	G_CC_SHADE_ENVA		    		     0, 	0, 			  0,  SHADE,    	 0, 0, 0, ENVIRONMENT


extern void Tani_SetOneVtxData(Vtx*, int, short, short, short, short, short,
									 uchar, uchar, uchar, uchar);
extern short Tani_RoundOff(float);

extern ulong LoopShapeAnime(int, MapNode*);


#endif
