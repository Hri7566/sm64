/********************************************************************************
						Ultra 64 MARIO Brothers

					 stage22 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx gfx_mo_body[] ;
extern Gfx gfx_mo_plate[];
extern Gfx gfx_mo_rgba[] ;

extern Gfx gfx_motosBG01[]	;
extern Gfx gfx_motosBG02[]	;
extern Gfx gfx_motosBG03[]	;
extern Gfx gfx_motosBG04[]	;
extern Gfx gfx_motosBG05S[]	;
extern Gfx gfx_motosBG05T[]	;
extern Gfx gfx_motosBG06[]	;
extern Gfx gfx_motosBG07[]	;
extern Gfx gfx_motosBG08[]	;
extern Gfx gfx_motosBG09[]	;
extern Gfx gfx_motosBG10[]	;
extern Gfx gfx_motosBG11[]	;
extern Gfx gfx_motosBG12[]	;

extern Gfx gfx_motosOBJ01[]		;
extern Gfx gfx_motosOBJ03[]		;
extern Gfx gfx_motosOBJ05[]		;
extern Gfx gfx_motosOBJ06Near[]	;
extern Gfx gfx_motosOBJ06Far[]	;
extern Gfx gfx_motosOBJ07Near[]	;
extern Gfx gfx_motosOBJ07Far[]	;
extern Gfx gfx_motosOBJ08[]		;
extern Gfx gfx_motosOBJ09[]		;
extern Gfx gfx_motosOBJ10[]		;
extern Gfx gfx_motosOBJ11[]		;

extern Gfx gfx_motosOBJ12_00[];
extern Gfx gfx_motosOBJ12_01[];
extern Gfx gfx_motosOBJ12_02[];
extern Gfx gfx_motosOBJ12_03[];
extern Gfx gfx_motosOBJ12_04[];
extern Gfx gfx_motosOBJ12_05[];
extern Gfx gfx_motosOBJ12_06[];
extern Gfx gfx_motosOBJ12_07[];
extern Gfx gfx_motosOBJ12_08[];
extern Gfx gfx_motosOBJ12_09[];
extern Gfx gfx_motosOBJ12_10[];
extern Gfx gfx_motosOBJ12_11[];
extern Gfx gfx_motosOBJ12_12[];
extern Gfx gfx_motosOBJ12_13[];
extern Gfx gfx_motosOBJ12_sd[];

extern Gfx gfx_mo_mobj02[];
extern Gfx gfx_mo_mobj04[];


/* ------------------------------------------------------------------------------------	*/
/* ------------------------------------------------------------------------------------	*/

extern Gfx gfx_fm[]			;
extern Gfx gfx_fm_gouraud[]	;
extern Gfx gfx_fm_i_alpha[]	;
extern Gfx gfx_fm_kakou[]	;
extern Gfx gfx_fm_rgba[]	;
extern Gfx gfx_fm_battan[]	;


/********************************************************************************/
/*	Hierarchy map data of MotosBG01.											*/
/********************************************************************************/
Hierarchy RCP_HmsMotosBG01[] = { 
	hmsHeader(1700)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosBG01)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of MotosBG02.											*/
/********************************************************************************/
Hierarchy RCP_HmsMotosBG02[] = { 
	hmsHeader(2200)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosBG02)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of MotosBG03.											*/
/********************************************************************************/
Hierarchy RCP_HmsMotosBG03[] = { 
	hmsHeader(1200)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosBG03)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of MotosBG04.											*/
/********************************************************************************/
Hierarchy RCP_HmsMotosBG04[] = { 
	hmsHeader(2000)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosBG04)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of MotosBG05.											*/
/********************************************************************************/
Hierarchy RCP_HmsMotosBG05[] = { 
	hmsHeader(1700)
	hmsBegin()
		hmsGfx(RM_SURF  , gfx_motosBG05S)
		hmsGfx(RM_SPRITE, gfx_motosBG05T)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of MotosBG06.											*/
/********************************************************************************/
Hierarchy RCP_HmsMotosBG06[] = { 
	hmsHeader(1500)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosBG06)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of MotosBG07.											*/
/********************************************************************************/
Hierarchy RCP_HmsMotosBG07[] = { 
	hmsHeader(1300)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosBG07)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of MotosBG08.											*/
/********************************************************************************/
Hierarchy RCP_HmsMotosBG08[] = { 
	hmsHeader(1000)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosBG08)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of MotosBG09.											*/
/********************************************************************************/
Hierarchy RCP_HmsMotosBG09[] = { 
	hmsHeader(900)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosBG09)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of MotosBG10.											*/
/********************************************************************************/
Hierarchy RCP_HmsMotosBG10[] = { 
	hmsHeader(1000)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosBG10)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of MotosBG11.											*/
/********************************************************************************/
Hierarchy RCP_HmsMotosBG11[] = { 
	hmsHeader(500)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosBG11)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of MotosBG12.											*/
/********************************************************************************/
Hierarchy RCP_HmsMotosBG12[] = { 
	hmsHeader(500)
	hmsBegin()
		hmsGfx(RM_SPRITE, gfx_motosBG12)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of OBJ01.												*/
/********************************************************************************/
Hierarchy RCP_HmsMotosOBJ01[] = { 
	hmsHeader(550)
	hmsBegin()
		hmsGfx(RM_SPRITE, gfx_motosOBJ01)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of OBJ03.												*/
/********************************************************************************/
Hierarchy RCP_HmsMotosOBJ03[] = { 
	hmsHeader(850)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosOBJ03)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of OBJ05.												*/
/********************************************************************************/
Hierarchy RCP_HmsMotosOBJ05[] = { 
	hmsHeader(500)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosOBJ05)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of OBJ06.												*/
/********************************************************************************/
Hierarchy RCP_HmsMotosOBJ06_Near[] = { 
	hmsHeader(700)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosOBJ06Near)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsMotosOBJ06_Far[] = { 
	hmsHeader(1100)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosOBJ06Far)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of OBJ07.												*/
/********************************************************************************/
Hierarchy RCP_HmsMotosOBJ07_Near[] = {
	hmsHeader(450)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosOBJ07Near)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsMotosOBJ07_Far[] = { 
	hmsHeader(900)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosOBJ07Far)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of OBJ08.												*/
/********************************************************************************/
Hierarchy RCP_HmsMotosOBJ08[] = { 
	hmsHeader(2100)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosOBJ08)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of OBJ09.												*/
/********************************************************************************/
Hierarchy RCP_HmsMotosOBJ09[] = { 
	hmsHeader(1400)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosOBJ09)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of OBJ10.												*/
/********************************************************************************/
Hierarchy RCP_HmsMotosOBJ10[] = { 
	hmsHeader(1800)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosOBJ10)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of OBJ11.												*/
/********************************************************************************/
Hierarchy RCP_HmsMotosOBJ11[] = { 
	hmsHeader(800)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_motosOBJ11)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of OBJ12.												*/
/********************************************************************************/
Hierarchy RCP_HmsMotosOBJ12_00[] = { 
	hmsHeader(350)
	hmsBegin()
		hmsGfx(RM_SURF , gfx_motosOBJ12_00)
		hmsGfx(RM_XSURF, gfx_motosOBJ12_sd)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsMotosOBJ12_01[] = { 
	hmsHeader(350)
	hmsBegin()
		hmsGfx(RM_SURF , gfx_motosOBJ12_01)
		hmsGfx(RM_XSURF, gfx_motosOBJ12_sd)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsMotosOBJ12_02[] = { 
	hmsHeader(350)
	hmsBegin()
		hmsGfx(RM_SURF , gfx_motosOBJ12_02)
		hmsGfx(RM_XSURF, gfx_motosOBJ12_sd)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsMotosOBJ12_03[] = { 
	hmsHeader(350)
	hmsBegin()
		hmsGfx(RM_SURF , gfx_motosOBJ12_03)
		hmsGfx(RM_XSURF, gfx_motosOBJ12_sd)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsMotosOBJ12_04[] = { 
	hmsHeader(350)
	hmsBegin()
		hmsGfx(RM_SURF , gfx_motosOBJ12_04)
		hmsGfx(RM_XSURF, gfx_motosOBJ12_sd)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsMotosOBJ12_05[] = { 
	hmsHeader(350)
	hmsBegin()
		hmsGfx(RM_SURF , gfx_motosOBJ12_05)
		hmsGfx(RM_XSURF, gfx_motosOBJ12_sd)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsMotosOBJ12_06[] = { 
	hmsHeader(350)
	hmsBegin()
		hmsGfx(RM_SURF , gfx_motosOBJ12_06)
		hmsGfx(RM_XSURF, gfx_motosOBJ12_sd)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsMotosOBJ12_07[] = { 
	hmsHeader(350)
	hmsBegin()
		hmsGfx(RM_SURF , gfx_motosOBJ12_07)
		hmsGfx(RM_XSURF, gfx_motosOBJ12_sd)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsMotosOBJ12_08[] = { 
	hmsHeader(350)
	hmsBegin()
		hmsGfx(RM_SURF , gfx_motosOBJ12_08)
		hmsGfx(RM_XSURF, gfx_motosOBJ12_sd)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsMotosOBJ12_09[] = { 
	hmsHeader(350)
	hmsBegin()
		hmsGfx(RM_SURF , gfx_motosOBJ12_09)
		hmsGfx(RM_XSURF, gfx_motosOBJ12_sd)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsMotosOBJ12_10[] = { 
	hmsHeader(350)
	hmsBegin()
		hmsGfx(RM_SURF , gfx_motosOBJ12_10)
		hmsGfx(RM_XSURF, gfx_motosOBJ12_sd)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsMotosOBJ12_11[] = { 
	hmsHeader(350)
	hmsBegin()
		hmsGfx(RM_SURF , gfx_motosOBJ12_11)
		hmsGfx(RM_XSURF, gfx_motosOBJ12_sd)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsMotosOBJ12_12[] = { 
	hmsHeader(350)
	hmsBegin()
		hmsGfx(RM_SURF , gfx_motosOBJ12_12)
		hmsGfx(RM_XSURF, gfx_motosOBJ12_sd)
	hmsEnd()
  	hmsExit()
};

Hierarchy RCP_HmsMotosOBJ12_13[] = { 
	hmsHeader(350)
	hmsBegin()
		hmsGfx(RM_SURF , gfx_motosOBJ12_13)
		hmsGfx(RM_XSURF, gfx_motosOBJ12_sd)
	hmsEnd()
  	hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MotosOBJ02.
================================================================================== */
Hierarchy RCP_HmsMotosOBJ02[] = {
    hmsHeader(600)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_mo_mobj02)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of MotosOBJ04.
================================================================================== */
Hierarchy RCP_HmsMotosOBJ04[] = {
    hmsHeader(1300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_mo_mobj04)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage22Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(1, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(64, SCREEN_NEAR, 20000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_FIELD,  0,2000,6000,  0,0,-8192, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF  , gfx_mo_body)
					hmsGfx(RM_SPRITE, gfx_mo_plate)
					hmsGfx(RM_SPRITE, gfx_mo_rgba)
					hmsCProg(0, WaterInit)
					hmsCProg(0x2201, WaterFall)
					hmsObject()
					hmsCProg(12, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

	hmsEnd()
	hmsExit()
};


/* ------------------------------------------------------------------------------------	*/
/* ------------------------------------------------------------------------------------	*/


/* =============================================================================
        : Hierarchy map data of FmBattan.
================================================================================ */
Hierarchy RCP_HmsFmBattan[] = {
    hmsHeader(900)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_fm_battan)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 2.														*/
/********************************************************************************/
Hierarchy RCP_Stage22Scene2[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0, 0, 0, 1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_INSIDE,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF  , gfx_fm)
					hmsGfx(RM_SURF  , gfx_fm_gouraud)
					hmsGfx(RM_XSURF , gfx_fm_i_alpha)
					hmsGfx(RM_SURF  , gfx_fm_kakou)
					hmsGfx(RM_SPRITE, gfx_fm_rgba)
					hmsCProg(0	   , WaterInit)
					hmsCProg(0x2202, WaterFall)
					hmsCProg(0x2202, WaterDraw)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

	hmsEnd()
	hmsExit()
};
