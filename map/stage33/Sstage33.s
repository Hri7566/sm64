/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 33 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage33


/* ================================================================================
		: Move BGs sequence.
===================================================================================	*/
SEQ_Stage33_MoveBGs:

		seqActor(S_movebg01,       0,   0,    0,   0,90,0,   0,0,0,   e_kopa2_moveBG)
		seqActor(S_kirai,   	   4,1329, 3598,   0,90,0,   0,0,0,   e_kirai)
		seqActor(S_kirai,   	3584,1329,    0,   0,90,0,   0,0,0,   e_kirai)	
		seqActor(S_kirai,   	   0,1329,-3583,   0,90,0,   0,0,0,   e_kirai)
		seqActor(S_kirai,      -3583,1329,    0,   0,90,0,   0,0,0,   e_kirai)

		seqReturn


/* ================================================================================
		: Stage 33 main sequence.
===================================================================================	*/
SEQ_DoStage33:

	seqInitStage()
	seqLoadPres(SEGMENT_BACK	  , _BackMotosSegmentRomStart   , _BackMotosSegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE   , _ALavaTextureSegmentRomStart, _ALavaTextureSegmentRomEnd)
	seqLoadPres(SEGMENT_WEATHER   , _WeatherSegmentRomStart     , _WeatherSegmentRomEnd		)
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage33SegmentRomStart  , _GfxStage33SegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_aSegmentRomStart , _GfxEnemy2_aSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_aSegmentRomStart , _HmsEnemy2_aSegmentRomEnd	)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetEnemy2_a)

		seqHmsShape(S_movebg01, RCP_HmsKopa2A)

		seqBeginScene(1, RCP_Stage33Scene1)
			seqActor(S_NULL,   0,1229+START_H,0,  0,180,0,  0,10,0,  e_player_rolling)
			seqPort(10, 33, 1, 10)
			seqGameClear( 6, 3, 54)
			seqGameOver (19, 1, 12)
			seqCall(SEQ_Stage33_MoveBGs)
			seqMapInfo(cx3301_info)
			seqSetMusic(NA_STG_KUPPA, NA_KUPPA_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 180,  0,1229,0)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
