/* @@@@@@@@@@@@@@@@@@@@
    BGM NAME ALIAS
      1996. 7.22(MON)
   @@@@@@@@@@@@@@@@@@@@ */

#define NA_TEST_SE	    0x0000
#define NA_NO_MUSIC	    0x0000
#define NA_CLEAR_FANFARE    0x0F01

#define NA_TITLE_BGM	    0x0002
#define NA_OVER_TITLE_BGM   0x0082
#define NA_MAINMAP_BGM	    0x0003		
#define NA_CASTLE_BGM	    0x0004
#define NA_TOWER_BGM	    0x0009
#define NA_TEST_BGM	    0x0003
#define NA_SNOW_BGM	    0x0008
#define NA_SLIDER_BGM	    0x0009
#define NA_SEA_BGM	    0x0005
#define NA_SUISOU_BGM	    0x0085
#define NA_MOTOS_BGM	    0x0006
#define NA_KUPPA_BGM	    0x0007
#define NA_COURTYARD_BGM    0x0003
#define NA_ATHLETIC_BGM	    0x0009
#define NA_RACE_BGM	    0x0409
#define NA_OBAKE_HOUSE_BGM  0x000A

#define NA_PUKKUN_BGM	    0x000B
#define NA_DUNGEON_BGM	    0x000C
#define NA_STAGESTART_BGM   0x000D
#define NA_MUTEKI_BGM	    0x040E
#define NA_NOKO_SLIP_BGM    0x048E

#define NA_METAL_BGM	    0x040F
#define NA_K_MESSAGE_BGM    0x0F10
#define NA_EXTRA_BGM	    0x0011
#define NA_HIGH_SCORE_BGM   0x0F12
#define NA_MERRY_BGM	    0x0F13
#define NA_RACE_FANFARE_BGM 0x0F14
#define NA_STAR_APPEAR_BGM  0x0F15
#define NA_BOSS_BGM	    0x0416
#define NA_K_CLEAR_FANFARE  0x0F17
#define NA_MUGEN_KAIDAN_BGM 0x0F18
#define NA_LAST_KUPPA_BGM   0x0019
#define NA_ENDING_BGM	    0x0F1A
#define NA_NAZO_CLEAR_BGM   0x0F1B
#define NA_KINOPIO_BGM	    0x0F1C
#define NA_PEACH_BGM	    0x041D
#define NA_OPENING_BGM	    0x0F1E
#define NA_LAST_KUPPA_CLEAR 0x0F1F
#define NA_ENDING_DEMO_BGM  0x0020
#define NA_FILE_SELECT_BGM  0x0021
#define NA_JUGEMU_BGM	    0x0F22	/* USA & japanese Ver 2.0 */
/* #define NA_JUGEMU_BGM	    0x0022	/* japanese Ver 1.0 */

/* @@@@@@@@@@@@@@@@@@@@
    STAGE MODE ALIAS
   @@@@@@@@@@@@@@@@@@@@ */

#define NA_STG_DEFAULT	    0x00
#define NA_STG_GROUND	    0x00
#define NA_STG_MOUNTAIN	    0x00
#define NA_STG_CASTLE	    0x01
#define NA_STG_KUPPA	    0x02
#define NA_STG_WATER	    0x03
#define NA_STG_DUNGEON	    0x04

#define NA_STG_MOUNTAIN_24  0x05
#define NA_STG_OBAKE	    0x06

#define NA_STG_ENDING	    0x07

