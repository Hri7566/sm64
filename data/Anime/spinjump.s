/********************************************************************************
						Ultra 64 MARIO Brothers

					mario spin jumping animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

						   September 19, 1995
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioSpinJumping:
	.half	 MAP_ANIM_ONETIME 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	 1 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x0164						/* total bytes       */


	.half	    1,    0 	/* chn14translate x */
	.half	    1,    1 	/* chn14 translate y */
	.half	    1,    0 	/* chn14 translate z */
	.half	    1,    0 	/* chn14 rotate x */
	.half	    1,    2 	/* chn14 rotate y */
	.half	    1,    0 	/* chn14 rotate z */
	.half	    1,    0 	/* <m_waist>1_3 rotate x */
	.half	    1,    0 	/* <m_waist>1_3 rotate y */
	.half	    1,   39 	/* <m_waist>1_3 rotate z */
	.half	    1,    0 	/* <m_body>1 rotate x */
	.half	    1,    0 	/* <m_body>1 rotate y */
	.half	    1,   38 	/* <m_body>1 rotate z */
	.half	    1,    0 	/* <m_head>2 rotate x */
	.half	    1,    0 	/* <m_head>2 rotate y */
	.half	    1,   37 	/* <m_head>2 rotate z */
	.half	    1,   33 	/* chn6 rotate x */
	.half	    1,   34 	/* chn6 rotate y */
	.half	    1,   35 	/* chn6 rotate z */
	.half	    1,    7 	/* <m_larmA>1 rotate x */
	.half	    1,    8 	/* <m_larmA>1 rotate y */
	.half	    1,    9 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1 rotate x */
	.half	    1,    0 	/* <m_larmB>1 rotate y */
	.half	    1,    6 	/* <m_larmB>1 rotate z */
	.half	    1,    3 	/* <m_lhand>1 rotate x */
	.half	    1,    4 	/* <m_lhand>1 rotate y */
	.half	    1,    5 	/* <m_lhand>1 rotate z */
	.half	    1,   29 	/* chn10 rotate x */
	.half	    1,   30 	/* chn10 rotate y */
	.half	    1,   31 	/* chn10 rotate z */
	.half	    1,   12 	/* <m_rarmA>1 rotate x */
	.half	    1,   13 	/* <m_rarmA>1 rotate y */
	.half	    1,   14 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1 rotate y */
	.half	    1,   11 	/* <m_rarmB>1 rotate z */
	.half	    1,    0 	/* <m_rhand>1 rotate x */
	.half	    1,    0 	/* <m_rhand>1 rotate y */
	.half	    1,   10 	/* <m_rhand>1 rotate z */
	.half	    1,    0 	/* chn15 rotate x */
	.half	    1,    0 	/* chn15 rotate y */
	.half	    1,   32 	/* chn15 rotate z */
	.half	    1,   26 	/* <m_llegA>1 rotate x */
	.half	    1,   27 	/* <m_llegA>1 rotate y */
	.half	    1,   28 	/* <m_llegA>1 rotate z */
	.half	    1,    0 	/* <m_llegB>1 rotate x */
	.half	    1,    0 	/* <m_llegB>1 rotate y */
	.half	    1,   25 	/* <m_llegB>1 rotate z */
	.half	    1,   22 	/* <m_ltoot>1 rotate x */
	.half	    1,   23 	/* <m_ltoot>1 rotate y */
	.half	    1,   24 	/* <m_ltoot>1 rotate z */
	.half	    1,    0 	/* chn17 rotate x */
	.half	    1,    0 	/* chn17 rotate y */
	.half	    1,   36 	/* chn17 rotate z */
	.half	    1,   19 	/* <m_rlegA>1 rotate x */
	.half	    1,   20 	/* <m_rlegA>1 rotate y */
	.half	    1,   21 	/* <m_rlegA>1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1 rotate y */
	.half	    1,   18 	/* <m_rlegB>1 rotate z */
	.half	    1,   15 	/* <m_rfoot>1 rotate x */
	.half	    1,   16 	/* <m_rfoot>1 rotate y */
	.half	    1,   17 	/* <m_rfoot>1 rotate z */

	.half	0x0000,0x00BD,0x3FFF,0xFF8A,0xF980,0xF06F,0xF9E4,0x3E29,0xB6E4,0x9220,0xED8C,0xFB1D,0x4ED2,0x35F4,0x1B41,0x01E6
	.half	0x0166,0xEF70,0x0640,0xFEE9,0xFF3F,0xBC52,0xFF9E,0xFEF0,0xF088,0x054B,0x0040,0x0056,0xBCD3,0x7FFF,0x7FFF,0x4171
	.half	0xBF5F,0x7FFF,0x7FFF,0x4171,0xBF5F,0xF50C,0xFF26,0x3FFF
