/**************************************************************************************************
			Message Number define

			Programed by Iwamoto Daiki
***************************************************************************************************/

#define MESS_TORI 		0
#define MESS_MAIN_OUT	1
#define MESS_MAIN_IN	2
#define MESS_CRS_START	3
#define MESS_MOUNT		4
#define MESS_FAIREBAR	5
#define MESS_SNOWSLIDE	6
#define MESS_WATERLAND  7
#define MESS_STARDOOR   8
#define MESS_DANGER		9



