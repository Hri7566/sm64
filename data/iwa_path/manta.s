/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					   Manta Strategy

					April 4 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/*==============================================================================
						Path Data
 ===============================================================================*/
e_d_manta:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON| stf_ALLangleSAME)
	p_set_pointer(skelanime,manta_anime)
	p_set_skelanime_number(0)
	p_program(s_d_manta_init)

	p_while
		p_hitON
		p_program(s_d_manta_main)
	p_loop


/*################*/
#else
/*################*/


/*==================================================================================================
		C Program
 ===================================================================================================*/
#define manta_target_angX 	(execstp->s[stw_work0].d)
#define manta_target_angY 	(execstp->s[stw_work1].d)
#define manta_star_flag 	(execstp->s[stw_work8].d)

#define MANTA_STARX	-3180
#define MANTA_STARY	-3600
#define MANTA_STARZ	 120


extern short manta_raildata[]={
	 0, -4500, -1380,   -40,
	 1, -4120, -2240,   740,
	 2, -3280, -3080,  1040,
	 3, -2240, -3320,   720,
	 4, -1840, -3140,  -280,
	 5, -2320, -2480, -1100,
	 6, -3220, -1600, -1360,
	 7, -4180, -1020, -1040,
	-1,
};

static HitCheckRecord manta_hit = {
	OBJNAME_DAMAGE,
	0,0,3,0,			/* flag,hp,ap,coin 		 */
	200+10,50+10,		/* player attack hitarea */
	200   ,50   ,		/* player damege hitarea */
};


/***************************************************************************************************
						    Init
***************************************************************************************************/
extern void s_d_manta_init(void)
{
	StrategyRecord* ring_dstp = s_makeobj_nowpos(execstp,S_NULL,e_ring_dummy);
	execstp->motherobj = ring_dstp;


	s_set_hitparam(execstp,&manta_hit);		/* Set HitCheck Param */

	s_set_scale(2.5);

}

/***************************************************************************************************
						    Manta Move
***************************************************************************************************/
static void manta_Move(void)
{
	short anim_frame = execstp->map.skelanim.frame;
	int	  lift_flag;

	road_root(execstp) = manta_raildata;

	lift_flag = s_road_move(lift_flag);

	manta_target_angY = road_angleY(execstp);
	manta_target_angX = road_angleX(execstp);

	obj_speedF = 10;
	obj_angleY = s_chase_angle(obj_angleY,manta_target_angY,0x80);
	obj_angleX = s_chase_angle(obj_angleX,manta_target_angX,0x80);

	if ((short) manta_target_angY != (short)obj_angleY ) {
		obj_angleZ -= 91;
		if (obj_angleZ < degree(-30)) obj_angleZ = degree(-30);
	}
	else {
		obj_angleZ += 91;
		if (obj_angleZ > degree(30)) obj_angleZ = degree(30);
	}

	s_3Dmove();

	if (anim_frame==0) objsound(NA_SE2_FISH);
}
/***************************************************************************************************
						    Main
***************************************************************************************************/
static void manta_MakeRing(void)
{
	StrategyRecord* ring_dstp =  execstp->motherobj;


	if (obj_timer == 300) obj_timer = 0;
	if (obj_timer == 0 || obj_timer == 50 || obj_timer == 150 || obj_timer == 200 || obj_timer == 250 ) {	
		StrategyRecord* stp = s_makeobj_nowpos(execstp,S_ring,e_manta_ring);
		stp->s[stw_animeangleY].d = obj_angleY;
		stp->s[stw_animeangleX].d = 0x4000 + obj_angleX;
		stp->s[stw_worldX].f = obj_worldX + sin(obj_angleY+0x8000)*(200);
		stp->s[stw_worldY].f = (obj_worldY+10)+sin(obj_angleX)*(200);
		stp->s[stw_worldZ].f = obj_worldZ + cos(obj_angleY+0x8000)*(200);
		stp->s[stw_work7].d = ring_count;
		ring_count++;

		if (ring_count > 10000) ring_count = 0;
	}

}
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_d_manta_main(void)
{



	switch (obj_mode) {
		case 0: manta_Move();
				manta_MakeRing();

				if (manta_star_flag == 5) {
					s_kemuri();
					s_enemyset_star(MANTA_STARX,MANTA_STARY,MANTA_STARZ);
					obj_mode = 1;
				}
				break;

		case 1: manta_Move();
				break;
	}


	if ( obj_mail & EMAIL_PLAYERHIT ){

		Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
	}

}




/*################*/
#endif
/*################*/


