/****************************************************
		SUPER MARIO 64 SOUND MACRO
		Written By Yoji Inagaki
		    1996. 5. 1(WED)
*****************************************************/

extern void Na_SeFlagCheck(u8, f32 *, f32 *, u16 *);
extern void Na_SeFlagEntry(u32 , f32 *);
extern void Na_SeFlagFree(u8, u8);
extern void Na_SeBgmMuteOff(u8, u8);
extern f32 Na_SePan(f32, f32);
extern void Na_AllMusicCheck(void);
extern void Na_SeqSubVolumeTrans(u8, u8 , u8 , u16);
extern void Na_AllSeqSubVolumeTrans(u8, u8, u16);
extern void Na_SeqSubVolumeMove(u8 );

extern void Na_BgmFlagEntry(u8, u16 , u16);
extern void Na_BgmFlagFadeOut(u16, u16);
extern void Na_BgmFlagFree(u16);
extern void Na_AllBgmStackFlagFree(void);
extern u16 Na_GetPlayingBgmFlag(void);
extern void Na_Seq0TimerMuteCheck(void);
extern void Na_Seq1PlayingCheck(void);

extern void Na_SeTrackEntry(u8);
extern void Na_SeEnd(u32, f32 *);
extern void Na_ObjSeEnd(f32 *);
extern void Na_AllSeStop(u8);
extern void Na_AllSeCheck(void);
extern void Na_LevelSeAllStop(void);
extern void Na_SeqStart(u8, u8, u16);
extern void Na_SeqMixing(u8 ,u32 ,s8 );
extern void Na_SeqCrossFade(u8, u8, u8, u8, u8, u8, u16);
extern void Na_AddSeq(u8 , u8, u8,  u16);
extern void Na_SubSeq(u16);
extern void Na_SoundFadeOut(u16);
extern u8 Na_Seq0MuteVolSet(u16);

extern void Na_SeqStop(u8, u16);
extern void Na_AllSeqStop(void);
extern void Na_PauseSet(u8);
extern void Na_SeInfoGet(u8, u8 *, u8 *, u8 *);
extern void Na_SeqVolRecover(u8, u16);
extern void Na_SeqVolMute(u8, u16, u8);
extern void Na_PortLock(u8, u16);
extern void Na_PortUnlock(u8, u16);
extern void Na_OutputMode(u8);
extern void Na_StageChange(u8);
extern void Na_SetObjSpeed(u8, u8);
extern void Na_MessageSound(u8);

extern void Na_HiScoreBgm(void);
extern void Na_NewStarAppearBgm(u8);
extern void Na_RaceFanfareBgm(void);
extern void Na_NazoClearBgm(void);
extern void Na_NewDoorMessageBgm(void);
extern void Na_kinopioAppearBgm(void);
extern void Na_ClearFanfare(void);

extern u8 Na_SubTrackMute(u8 ,  u8 , u8 );
extern f32 _zeroptr[3];
extern f32 _oneptr[3];
extern u32   NA_RANDOM;
extern u8 StageFeOfs;

/********************
    Command Macro
    1996. 5. 2(THU)
********************/
#define Na_FixSeFlagEntry(s)		Na_SeFlagEntry(s, &_zeroptr[0])
#define Na_MarioJump(p)			Na_SeFlagEntry(NA_VC1_NORMALJUMP+((NA_RANDOM % 0x03)<<16), p)
#define Na_MarioSpeed(s)		Na_SetObjSpeed(_HN_PLAYER_L, s)
#define Na_MarioPant(p)			Na_SeFlagEntry(NA_VC1_HARDBREATHE+((NA_RANDOM % 0x03)<<16), p)
#define Na_MarioHighJump(p)		Na_SeFlagEntry(NA_VC1_HIGHJUMP+((NA_RANDOM % 0x05)<<16), p)

#define Na_RedCoinSe(c)			Na_SeFlagEntry(NA_SYS_REDCOIN+(((u32)(c)-1)<<16), &_zeroptr[0])
#define Na_SecretPointSe(c)		Na_SeFlagEntry(NA_SYS_SECRETPOINT+(((u32)(c-1))<<16), &_zeroptr[0])

#define Na_SystemTrgFixSeStart(s)	Na_SeFlagEntry(s,&_zeroptr[0])
#define	Na_LevelSound(stratp,flag)	Na_SeFlagEntry(flag,&(stratp)->map.viewCoord[0])

#define Na_SeSeqStart()			Na_SeqStart(NA_SE_HANDLE,_SE_SEQUENCE,0)	
#define Na_EntryMusicFree(f)		Na_BgmFlagFree(f);
#define Na_EntryMusicFadeOut(f,t)	Na_BgmFlagFadeOut(f,t);
#define Na_MusicStart(h,a,t)		Na_BgmFlagEntry(h,a,t)
#define Na_MusicStop(h,t)		Na_SeqStop(h,t)
#define Na_MusicStart2(n)		Na_SeqStart(NA_MUS_HANDLE,n,0)
#define Na_MusicFadein(n,t)		Na_SeqStop(NA_MUS_HANDLE,t)
#define Na_MusicStop2()			Na_SeqStop(NA_MUS_HANDLE,1)
#define Na_MusicFadeOut(t)		Na_SeqStop(NA_MUS_HANDLE,t)
#define Na_MusicMuteOn()		Na_SeqVolMute(NA_MUS_HANDLE, 60, NA_VIEWMODE_VOL)
#define Na_MusicMuteOff()		Na_SeqVolRecover(NA_MUS_HANDLE, 60)
#define Na_MusicVolDown(h,t,v)		Na_SeqVolMute(h, t, v)
#define Na_MusicVolRecover(h,t)		Na_SeqVolRecover(h, t)

#define Na_KuppaBgmFadeOut()		Na_SeqStop(NA_MUS_HANDLE, 1)
#define Na_StageBgmFadeOut()		Na_SeqStop(NA_MUS_HANDLE, 480)
#define Na_BossBgmStop()		Na_SeqStop(NA_MUS_HANDLE,1)
#define Na_WaterBgmFull()		Na_AllSeqSubVolumeTrans(NA_MUS_HANDLE, 127, 1000)

#define Na_PauseStart()			Na_PauseSet(1)
#define Na_PauseEnd()			Na_PauseSet(0)
#define Na_PukkunBgmStart()		Na_AddSeq(0x0b, NA_PUKKUN_VOL, 0xff, 1000)    /*NA_VIEWMODE_VOL*/
#define Na_PukkunBgmEnd()		Na_SubSeq(50)
#define Na_PukkunBgmAway()		Na_SubSeq(500)

#define Na_MerryBgmMin()		Na_AddSeq(0x13, NA_MERRY_VOL00, NA_MERRY_VOL01, 200)
#define Na_MerryBgmMax()		Na_AddSeq(0x13, NA_MERRY_VOL10, NA_MERRY_VOL11, 50)
#define Na_MerryBgmEnd()		Na_SubSeq(300)

#define Na_MugenBgmStart()		Na_AddSeq(0x18, 0, 0xff, 1000)    /*NA_VIEWMODE_VOL*/
#define Na_MugenBgmEnd()		Na_SubSeq(500)

#define Na_StarAppearBgm()		Na_NewStarAppearBgm(1)
#define Na_Star100AppearBgm()		Na_NewStarAppearBgm(1)
/*
#define Na_StarAppearBgm()		Na_NewStarAppearBgm(1)
#define Na_Star100AppearBgm()		Na_NewStarAppearBgm(0)
*/

#define	Na_BlockSwitchBgm()		Na_NazoClearBgm()

/*** seq handle define ***/
#define NA_MUS_HANDLE     0x00
#define NA_FANFARE_HANDLE 0x01
#define NA_SE_HANDLE      0x02

/*** sequence define ***/
#define _SE_SEQUENCE	0

/*** se handle define ***/
#define	_HN_PLAYER_T	0
#define	_HN_PLAYER_L	1
#define	_HN_PLAYER_V	2
#define	_HN_SCENE_T	3
#define	_HN_SCENE_L	4
#define	_HN_ENEMY_T	5
#define	_HN_ENEMY_L	6
#define	_HN_SYSTEM	7

/*** output mode ***/
#define NA_SOUT_WIDE	    0
#define NA_SOUT_HEADPHONE   1
#define NA_SOUT_STEREO	    2
#define NA_SOUT_MONO	    3




/********************
    SE LABLE Macro
    1996. 3.20(WED)
********************/

/********************
	FORMAT
oooooooo
b3b2b1b0

b3:d4-d7 handle
   d3	 dist pitch flag
   d2	 priority distanse no check
   d1	 real time random pitch
   d0	 distanse volume no check 
b2:d0-d7 number
b1:d0-d7 priority
b0:d7    0:trigger
	 1:lebel
   d0	 start flag
   d5	 no effect
   d4	 BGM mute
   d1-d6 no use
********************/





/*** Mario Play Action Trigger ***/
#define NA_ON_GRND	    0x00000000	    /* On Ground    */
#define NA_ON_GRAS	    0x00010000	    /* On Grass     */
#define NA_ON_WATRSIDE	    0x00020000	    /* On Waterside */
#define NA_ON_CONC	    0x00030000	    /* On Concrete  */
#define NA_ON_FLOOR	    0x00040000	    /* On Floor     */
#define NA_ON_SNOW	    0x00050000	    /* On Snow      */
#define NA_ON_ICE	    0x00060000	    /* On Ice       */
#define NA_ON_SAND	    0x00070000	    /* On Sand      */
#define NA_ON_MAGMA	    0x00080000	    /* On magma (kame slip only)      */

#define NA_SE1_JUMP	    0x04008081	    /* Jumping     */
#define NA_SE1_LAND	    0x04088081	    /* Landing     */
#define NA_SE1_WALK	    0x06108081      /* Walking     */
#define NA_SE1_DOWN	    0x04188081	    /* Fall Down   */
#define NA_SE1_SLOWWALK	    0x06208081      /* Slow Walking     */

#define NA_SE1_METAL_JUMP    0x04289081	    /* metal mario jump */
#define NA_SE1_METAL_LAND    0x04299081	    /* metal mario land */
#define NA_SE1_METAL_WALK    0x042a9081	    /* metal mario walk */
#define NA_SE1_METAL_DOWN    0x042b9081	    /* metal mario down */

#define NA_SE1_HANDRUB	    0x062c0081	    /* hand rub */   
#define NA_SE1_UNTEI	    0x042da081	    /* untei move */   
#define NA_SE1_DEEPSANDWALK 0x042e0081	    /* walk on deep sand */   
#define NA_SE1_METAL_SLOWWALK    0x042f9081	    /* metal mario slowwalk */


#define NA_SE1_DIVE	    0x0430C081	    /* Dive into water! */   
#define NA_SE1_FACEUP	    0x04316081	    /* face break the surface of water */
#define NA_SE1_WATJUMP	    0x04328081	    /* jump from water */
#define NA_SE1_SWIMHAND	    0x04338081	    /* Swimming with Hand */
#define NA_SE1_SWIMFOOT	    0x04348081	    /* Swimming with Foot */
#define NA_SE1_THROW	    0x04358081	    /* Throw an object */
/*
#define NA_SE1_KICK	    0x04369081	    /* Kick  an object */
#define NA_SE1_THROWUP	    0x04368081	    /* throw up key */

#define NA_SE1_JUMPROLL	    0x04378081	    /* Kick  an object */
#define NA_SE1_KURU	    0x04388081	    /* Kurukuru Jump   */
/*
#define NA_SE1_CURVE	    0x04398081	    /* slip at slope 's curve  */

#define NA_SE1_TREEUP	    0x043a8081	    /* Treeup   */
#define NA_SE1_TREEDOWN	    0x043b8081	    /* Treedown */
#define NA_SE1_HIPATTACK    0x043c8081	    /* Hip Attack */
#define NA_SE1_OFFCAP	    0x043d8081	    /* cap off */
#define NA_SE1_WEARCAP	    0x043e8081	    /* cap on */
#define NA_SE1_HITHIP	    0x043f8081	    /* hit hip */
#define NA_SE1_CATCH	    0x043f8081	    /* catch key */
#define NA_SE1_SCRATCH_HEAD 0x04408081	    /* atama wo kaku */
#define NA_SE1_POLEUP	    0x04418081	    /* climb pole */
#define NA_SE1_METAL_HITWALL	0x04428081	    /* hit wall on metal */
#define NA_SE1_GRD_ESCAPE   0x04438081	    /* SHUPON !! */
#define NA_SE1_ATTACK_HIT   0x0444B081	    /* boyoyon */
#define NA_SE1_CLEARWALL_HIT	0x0444C081	    /* boyoyon */
#define NA_SE1_PIPEJUMP	    0x0444A081	    /* jump from pipe */

#define NA_SE1_HITWALL	    0x0445A081	    /* hit wall !! */
#define NA_SE1_INTO_BIRDCAGE	0x0446A081	    /* into bird cage */
#define NA_SE1_SWIMHAND_FAST	0x0447A081	    /* into bird cage */

#define NA_SE1_SINK	    0x04488081	    /* sink ground */

#define NA_SE1_METAL_JUMP_W	0x04509081	    /* metal mario jump in water */
#define NA_SE1_METAL_LAND_W	0x04519081	    /* metal mario land in water */
#define NA_SE1_METAL_WALK_W	0x04529081	    /* metal mario walk in water */
#define NA_SE1_METAL_DOWN_W	0x04539081	    /* metal mario down in water */
#define NA_SE1_METAL_SLOWWALK_W 0x04549081	    /* metal mario slowwalk in water */

#define NA_SE1_GO_UP_WIND   0x04568081	    /* flying mario goes up */
#define NA_SE1_JUGEM_ACCEL_1   0x04568081	    /* opening jugem acceleration 1 */
#define NA_SE1_TELEPORT	    0x0457C081	    /* flying mario goes up */
#define NA_SE1_METAL_REBOUND	0x0458A081	    /* flying mario goes up */
#define NA_SE1_STEP_HIT	    0x0459B081	    /* flying mario goes up */
#define NA_SE1_SIDEJUMP_ROLL	0x045A8081	    /* flying mario goes up */
#define NA_SE1_KANBAN_READ	0x045Bff81	    /* kanban read trigger */
#define NA_SE1_PUTKEY	0x045C8081	    /* put key */
#define NA_SE1_JUGEM_ACCEL_2   0x045E8081	    /* opening jugem acceleration 2 */
#define NA_SE1_JUGEM_ACCEL_3   0x045F8081	    /* opening jugem acceleration 3 */

#define NA_SE1_HIPATTACK2    0x04608081	    /* Hip Attack new */


/*** Mario Play Action LEVEL ***/
#define NA_LSE1_SLIP	    0x14000001
#define NA_LSE1_SMOKE	    0x14100001	    /* hip fired! */
#define NA_LSE1_POLEDOWN    0x14110001	    /* get off pole */
#define NA_LSE1_TREEDOWN    0x14128001	    /* Treedown */
#define NA_LSE1_LONGFALL    0x14130001	    /* long fall */
#define NA_LSE1_SANDSINK    0x14140001	    /* into sand */
#define NA_LSE1_FLYING	    0x14150001	    /* fly in the sky */
#define NA_LSE1_SPARK	    0x14160001	    /* denki biribiri */
#define NA_LSE1_FLY	    0x14170001	    /* flying */
#define NA_LSE1_ALARM	    0x1C180001	    /* flying */
#define NA_LSE1_TAIHOU_AIM  0x1D192001	    /* flying */
#define NA_LSE1_KOURA_SLIP  0x14200001	    /* nokonoko slide */
#define NA_LSE1_KOURA_SLIP_MAGMA    0x14280001	    /* nokonoko slide */


/*** Mario Voice Trigger ***/
#define NA_VC1_NORMALJUMP   0x24008081
					    /*  Hop! 
						One of them which are from 0x00 to 0x02
						selected randomly.
						Please use "Na_PlayerJump()" for it.
					    */
#define NA_VC1_SECONDJUMP   0x24038081	    /* Step! */
#define NA_VC1_SPINJUMP	    0x24048081	    /* Jump! */
#define NA_VC1_SPECIALJUMP  0x24048081	    /* Jump! */
#define NA_VC1_FALL	    0x24058081	    /* Fall from above to bottom */
#define NA_VC1_LIFT	    0x24068081	    /* Lift up an object */
#define NA_VC1_THROW	    0x24078081	    /* Throw an object */
#define NA_VC1_HANG	    0x2408C081	    /* Hook hands above player */ 
#define NA_VC1_CLIMB	    0x24098081	    /* Climbing */
#define NA_VC1_DAMAGE	    0x240aff81	    /* Damaged */ 
#define NA_VC1_TUMBLE	    0x240bD081	    /* Fall on bottom */   
#define NA_VC1_CAUGHT	    0x240b8081	    /* Fall on bottom */   
#define NA_VC1_NO	    0x240b8081	    /* Tubusare */

#define NA_VC1_YEEAH	    0x240c8081	    /* Get Coin  */  
#define NA_VC1_JOY	    0x240c8081	    /* Get Coin  */  
#define NA_VC1_YAWN	    0x240d8081	    /* Akubi */

#define NA_VC1_ZZZ_IN	    0x240e8081	    /* Sleep breathe in */
#define NA_VC1_ZZZ_OUT	    0x240f8081	    /* Sleep breathe out */
#define NA_VC1_DOWN	    0x2410C081	    /* Player KnockDown */
#define NA_VC1_INVOLVED	    0x2410C081	    /* involved by storm */
#define NA_VC1_SPARK	    0x2410C081	    /* spark */
#define NA_VC1_LAND	    0x24118081	    /* High Jump Landing */
#define NA_VC1_LONGFALL_DIVE	0x2411f081	    /* High fall diving */
#define NA_VC1_HARDBREATHE  0x24188081	    /* HP<=4 */
#define NA_VC1_LONGJUMPLAND 0x24138081	    /* long jump land */
#define NA_VC1_CLIMB2	    0x2413D081	    /* jump climb */
#define NA_VC1_FIREONBUTT   0x2414A081	    /* fire on butt */
#define NA_VC1_DEATH	    0x2415FF81	    /* hp 0 death */
#define NA_VC1_FREEZE	    0x24168081	    /* freeze */
#define NA_VC1_COUGH	    0x24178081	    /* cough */
#define NA_VC1_COUGH0	    0x241B8081	    /* cough 0 */
#define NA_VC1_COUGH1	    0x241C8081	    /* cough 1 */
#define NA_VC1_COUGH2	    0x241D8081	    /* cough 2 */
#define NA_VC1_PUNCH	    0x241E8081	    /* punch */
#define NA_VC1_KICK	    0x241F8081	    /* kick */
#define NA_VC1_MAMMA	    0x24208081	    /* manmamia */
#define NA_VC1_OKEYDOKEY    0x24218081	    /* okeydokey */
#define NA_VC1_HIPATTACK    0x24228081	    /* hip attack */
#define NA_VC1_DEATH_IN_WATER	0x2423F081  /* death in water */
#define NA_VC1_PUNCH2	    0x24248081	    /* punch 2 */

#define NA_VC1_HIGHJUMP	    0x242B8081	    /* randomhigh jump */

#define NA_VC1_DUUUN	    0x24308081	    /* duuuun */
#define NA_VC1_GAMEOVER	    0x2431FF81	    /* game over */
#define NA_VC1_HELLO	    0x2432FF81	    /* hello */
#define NA_VC1_PRESS_START  0x2433FFA1	    /* press start */
#define NA_VC1_POIN	    0x24348081	    /* poin */
#define NA_VC1_SLEEPINGTALK 0x2435FF81	    /* sleeping talk */
#define NA_VC1_SO_LONG	    0x24368081	    /* so long bowser */
#define NA_VC1_TIRED	    0x24378081	    /* I'm tired */


#define NA_VC1_PEACH_LETTER	0x2428FF81	    /* peach letter message */
#define NA_VC1_PEACH_END_1	0x2438FF81	    /* peach ENDING message 1 */
#define NA_VC1_PEACH_END_2	0x2439FF81	    /* peach ENDING message 2 */
#define NA_VC1_PEACH_END_3	0x243AFF81	    /* peach ENDING message 3 */
#define NA_VC1_PEACH_END_4	0x243BFF81	    /* peach ENDING message 4 */
#define NA_VC1_PEACH_END_5	0x243CFF81	    /* peach ENDING message 5 */
#define NA_VC1_PEACH_END_6	0x243DFF81	    /* peach ENDING message 6 */
#define NA_VC1_PEACH_END_7	0x243EFF81	    /* peach ENDING message 7 */
#define NA_VC1_PEACH_END_8	0x243FFF81	    /* peach ENDING message 8 */

#define NA_VC1_SILENT	    0x2028FF91	    /* dummy silent SE for fanfare */
#define NA_VC1_SILENT2	    0x2029FF91	    /* dummy silent SE for fanfare */
#define NA_VC1_SILENT3	    0x202AFF91	    /* dummy silent SE for fanfare */


/*** Scene Trigger ***/
#define NA_DOOR_WOOD	    0x00000000	    /* Wood Door    */
#define NA_DOOR_METAL	    0x00020000	    /* Metal Door   */

#define NA_IN_CASTLE	    0x00000000
#define NA_IN_OUTDOOR	    0x00010000
#define NA_IN_WATER	    0x00020000

#define NA_SE2_BIGSWITCH    0x30008081	    /* BIG SWITCH */
#define NA_SE2_FLOORCRUMBLE 0x30008081	    /* big world top floor */
#define NA_SE2_BIGJUMP	    0x30018081	    /* JUMP STEPBOARD */

#define NA_SE2_PIPEWARP	    NA_SYS_PIPEWARP	    /* WARP TO ANOTHER WORLD */
/*
#define NA_SE2_PIPEWARP	    0x31028081	    /* WARP TO ANOTHER WORLD */

#define NA_SE2_FIRE_GO_OUT  0x30038081	    /* hi ga kieru */
					    /** DOORKNOB is removed. **/
#define NA_SE2_WOODOPEN	    0x3004c081
#define NA_SE2_WOODCLOSE    0x3005c081
#define NA_SE2_METALOPEN    0x3006c081
#define NA_SE2_METALCLOSE   0x3007c081


#define NA_SE2_BUBBLE	    0x30080081	    /* Water Bubble PokoPoko */
#define NA_SE2_FISH	    0x30090081	    /* Fish Swiming */

#define NA_SE2_BIGBUBBLE    0x300b0081	    /* Water Boko! */
#define NA_SE2_VOLCANO	    0x300c8081
#define NA_SE2_MAGMAAWA	    0x300d0081	    /* Magma bokoboko */
#define NA_SE2_OTOSHIANA    0x300e8081	    /* Trap */

#define NA_SE2_WALLCRASH    0x300F0081	    /* wall crush using taihou */
#define NA_SE2_WALLCRUMBLE  0x300F0081	    /* wall crush using taihou */
#define NA_SE2_GATECRUMBLE  0x300F0081	    /* wall crush using taihou */

#define NA_SE2_STAR_ROLL    0x30160091	    /* small furiko in castle */
#define NA_SE2_FURIKO_S	    0x30170081	    /* small furiko in castle */

#define NA_SE2_WOODBOX_BOUND	0x32240081	    /* log rolling */

#define NA_SE2_LOGROLL	    0x32250081	    /* log rolling */
#define NA_SE2_SHELL_OPEN   0x30264081	    /* shell open */
#define NA_SE2_SHELL_CLOSE  0x30274081	    /* shell close */
#define NA_SE2_MARIO	    0x39280081	    /* Enter the mario picture */



#define NA_SE3_OWL	    0x300a0081	    /* Shark Swiming */
#define NA_SE3_BIRDFLAP	    0x300a0081	    /* Hawk flap */
#define NA_SE2_SELECT	    0x302b0081
#define NA_SE2_SET	    NA_SYS_SET     /* 0x302cF081*/
#define NA_SE2_BRIDGEFALL   0x302d8081
#define NA_SE2_FLOOR_BURST  0x302e2081	    /* athletic floor burst */
/*
#define NA_SE2_KIRAI	    0x302e0081
*/
#define NA_SE2_KUPABOMB	    0x312f0081
#define NA_SE2_KUPPABOMB    0x312f0081
#define NA_SE2_COINFLY	    0x38302081
#define NA_SE2_WATERBOMB    0x30310081
#define NA_SE2_WATERRING    0x30320081
#define NA_SE2_BOYOYONJUMP  0x30330081
#define NA_SE2_BRIDGEUP	    0x30344081
#define NA_SE2_BRIDGEDOWN   0x30354081
#define NA_SE2_SEESAW_MOVE  0x30354081
#define NA_SE2_COINBOUND    0x30364081
#define NA_SE2_KEYGET	    0x38370081
#define NA_SE2_KEYBOUND	    0x38378081
#define NA_SE2_FURIKO	    0x30380081
#define NA_SE2_WANWAN_WALK  0x30390081
#define NA_SE2_WANWAN_ATTACK 0x303A0081
#define NA_SE2_UNLOCK	    0x303B0081
#define NA_SE2_SNOWBALLBROKEN	    0x303C0081
#define NA_SE2_WOODHIT	    0x303D0081
#define NA_SE2_WOODPOLE_BOUND	0x303D8081
#define NA_SE2_RAFTSWITCH	    0x303E0081
/*
#define NA_SE2_PLATESWITCH	    0x303E0081
*/
#define NA_SE2_METAL_GATE	    0x303Fa081
#define NA_SE2_M_BALL_BOUND	    0x30400081

#define NA_SE2_RAFT_COLLISION	    0x30404081
#define NA_SE2_BLOCK_BURST	    0x3041C081
#define NA_SE2_KEY_SET	    0x30420081
#define NA_SE2_WALL_MOVE_DOWN	    0x30430081
#define NA_SE2_WALL_HIT	    0x30440081
#define NA_SE2_PILLAR_BOUND 0x30440081
#define NA_SE2_WALL_UP	    0x30450081
#define NA_SE2_METALBOX_ROLL	    0x30468081
#define NA_SE2_TAIHOUCAP_OPEN	    0x30478081
#define NA_SE2_ROCKBOUND    0x30480081
/*
#define NA_SE2_ROLLER_ROLL  0x30482081
*/
#define NA_SE2_BIG_SBALL_BROKEN    0x30490081
/*
#define NA_SE2_PYRAMID_SHAKING  0x304BE081
#define NA_SE2_PYRAMID_EXPLODE  0x304CF081
*/
#define NA_SE2_READY_GO_SHOT  0x314D4081

#define NA_SE2_AUTO_DOOR_OPEN	0x304EC081
#define NA_SE2_AUTO_DOOR_CLOSE	0x304FC081

#define NA_SE2_PILLAR_MOVE	0x30560081
/*
#define NA_SE2_STAR_APPEAR	0x3057FF91
*/
#define NA_SE2_STAR_FINISH	0x3057FF91
#define NA_SE2_ONEUP	    0x3058FF81
#define NA_SE2_CHAIR_DOWN	0x30590081
/*
#define NA_SE2_ROLLBOX_START	0x30590081
*/
#define NA_SE2_COFFIN_UP	0x315A0081
#define NA_SE2_WOODPOLE_SWING	0x315A4081
#define NA_SE2_COFFIN_DOWN	0x315B0081
#define NA_SE2_CHAIR_ROLL	0x315C0081
#define NA_SE2_CHAIR_FALL	0x305D0081
#define NA_SE2_OBJECT_BOUND	0x305E0081
#define NA_SE2_CHAIR_SHAKE	0x305F0081
/*
#define NA_SE2_KUPPA_DISAPPEAR	0x30600081
#define NA_SE2_KUPPA_KEY_FALL	0x30610081
*/
#define NA_SE2_STAGE_CRUMBLE	0x31628081
/*
#define NA_SE2_ONEUP_APPEAR	0x3063D081
*/
#define NA_SE2_HEART_TOUCH	0x3064C081
#define NA_SE2_PILE_HIT	    0x3065C081
#define NA_SE2_WATERLEVEL_SWITCH    0x30668081
#define NA_SE2_STONE_SWITCH    0x3067A081
#define NA_SE2_GET_ALLREDCOIN    0x38689081

#define NA_SE2_BIRD_FLY	    0x30690081
/*
#define NA_SE2_CORRECT_CHIME	0x306A8081
*/
#define NA_SE2_LIFT_STOP    0x306B8081
#define NA_SE2_BOXJUMP_S    0x306C4081
#define NA_SE2_BOXJUMP_B    0x306D4081
#define NA_SE2_FLOG_JUMP     0x306D2081
#define NA_SE2_YOSSY_WALK   0x306E2081
#define NA_SE2_YOSSY_JUMP   0x306F3081
#define NA_SE2_YOSSY_MESSAGE	0x30703081
#define NA_SE2_FLOG_DOWN	0x30713081
#define NA_SE2_JUGEMU_APPEAR	0x30750081
#define NA_SE2_BIGSTAR_MOVE	0x30730081
#define NA_SE2_BIGSTAR_BOUND	0x30740081
#define NA_SE2_SHIP_ROW		0x30750081
#define NA_SE2_FLOG_APPEAR	0x30762081


#define NA_SE2_OPENDOOR	    0x3004C081
#define NA_SE2_CLOSEDOOR    0x3005C081

#define NA_SE2_COIN	    0x38108081
#define NA_SE2_BLOCK	    0x30188081
#define NA_SE2_BOXOPEN	    0x31208081



/*** Scene Level ***/
#define NA_LSE2_RIVER	    0x40000001	    /* River stream */
#define NA_LSE2_FALL	    0x40010001	    /* Water fall   */
#define NA_LSE2_MOVE	    0x40020001	    /* Motor Floor */
#define NA_LSE2_LIFT	    0x40020001	    /* Motor Floor */
#define NA_LSE2_MAGMA	    0x41030001	    /* Magma stream */
#define NA_LSE2_KIRAKIRA    0x40040001	    /* Kirakira Star */
#define NA_LSE2_SANDSTORM	    0x40050001	    /* storm */
#define NA_LSE2_SWIRL	    0x40060001	    /* arijigoku */
#define NA_LSE2_STANDSLOPE  0x40070001	    /* stand slope */
#define NA_LSE2_CONVEYOR    0x40080001	    /* belt conveyor */
#define NA_LSE2_STAND_APPEAR    0x40080001	    /* stand appear */
#define NA_LSE2_RAFT_MOVE   0x40080001	    /* raft move or mounatain*/
#define NA_LSE2_WATERTORNADO	0x40090001  /* water tornado */
#define NA_LSE2_ROCKROLL	0x400A0001  /* rock roll */
#define NA_LSE2_ROCKSLIDE	0x400A0001  /* rock slide */
#define NA_LSE2_BRIDGESLOPE	0x400B0001
#define NA_LSE2_SHIPSLOPE	0x400B0001

#define NA_LSE2_LIFTROLLER	0x400C0001
#define NA_LSE2_WOODLIFT    0x400D0001
#define NA_LSE2_RACK_SLIDE  0x410D0001	    /* book rack slide */
#define NA_LSE2_SANDFALL    0x400E0001	    /* sand fall */
#define NA_LSE2_TURNTABLE   0x400F4001	    /* wood turn table */
#define NA_LSE2_BLOWUP_WIND 0x40108001	    /* blow up wind */
#define NA_LSE2_CLOCKHAND   0x40120001	    /* clock hand move */
#define NA_LSE2_BOXSLIDE_S  0x40130001	    /* small box slide */
#define NA_LSE2_STAR_MOVE   0x40140011	    /* small box slide */
#define NA_LSE2_STAGE_SLOPE 0x41150001	    /* stage slope */
#define NA_LSE2_WATERLEVEL_UP	0x41160001	    /* water level up */
#define NA_LSE2_WATERLEVEL_DOWN	0x41160001	    /* water level down */
#define NA_LSE2_CUBE_SLIDE  0x40178001	    /* water level down */
#define NA_LSE2_TRIANGLE_SLIDE  0x40188001	    /* water level down */



/*** Enemy Trigger ***/
#define NA_SE2_SHARK	    0x50008081	    /** fukurou **/
#define NA_SE3_RABBITJUMP   0x50010081	    /**    **/
#define NA_SE3_EYESHOT	    0x50010081	    /** eye shot   **/
#define NA_SE3_PENGUINWALK  0x50020081	    /** penguin **/
#define NA_SE3_KUPAWALK	    0x50030081
/*
#define NA_SE3_KUPAGAO	    0x50040081
*/
#define NA_SE3_KUPADAMAGE   0x50050081
#define NA_SE3_KUPADOWN	    0x50060081
#define NA_SE3_KUPABUUN	    0x50070081
#define NA_SE3_KUPABREATH   0x50080081
#define NA_SE3_PENGUINOYA   0x50098081	    /** BIG penguin **/

#define NA_SE3_TERECRASH    0x500A0081

#define NA_SE3_TEREAWAY	    0x500B0081
#define NA_SE3_STONE	    0x500CA081
#define NA_SE3_MOTOS_APPEAR_B	0x500CA081
#define NA_SE3_TAIHOUSET0   0x500DF081
#define NA_SE3_TAIHOUSET1   0x500EF081
#define NA_SE3_TAIHOUSET2   0x500FF081

/*
#define NA_SE3_FLOWERVOICE  0x50105081
#define NA_SE3_FLOWERDOWN   0x50116081
*/
#define NA_SE3_FISHSPLASH   0x50120081
#define NA_SE3_OSIDASI	    0x50130081
#define NA_SE3_ENEMYDOWN    0x50140081
#define NA_SE3_NURIWALK	    0x50155081
#define NA_SE3_KATSUGI_WALK	    0x50158081
#define NA_SE3_BOMBKING_BOUND	    0x50168081
#define NA_SE3_NURIPRESS    0x50166081
#define NA_SE3_MOTOSATTACK  0x50178081
#define NA_SE3_MOTOSFALL    0x5118A081
/*
#define NA_SE3_PUZZLE	    0x50192081
*/
#define NA_SE3_TAIHOU	    0x501a5081	    /* Taihou Bomb!   */

#define NA_SE3_MOTOSWALK    0x501b3081
/*
#define NA_SE3_MOTOS_HIT_S   0x501c0081
*/
#define NA_SE3_CATCH_B	    0x501d8081 	    
#define NA_SE3_THROW_B	    0x501eA081
#define NA_SE3_PENGUINJUMP  0x501f4081	    /** penguin **/
#define NA_SE3_BOMBHEI_WALK 0x50270081	    /** bombhei **/
#define NA_SE3_MONKEYVOICE0 0x50210081
#define NA_SE3_THROW_S	    0x50222081
#define NA_SE3_MOGURA_THROW 0x50220081
#define NA_SE3_MOGURA_HIT   0x50244081

#define NA_SE3_BOMB_TAIHOU  0x50254081
#define NA_SE3_KURIOBOU_DOWN 0x50268081
#define NA_SE3_KURIOBOU_WALK 0x50200081
#define NA_SE3_WATERBOMB_FALL 0x50288081
#define NA_SE3_WATERBOMB_SPLASH 0x5029A081
#define NA_SE3_SNOWMAN_DOWN	0x502A0081
#define NA_SE3_SNOWMAN_LAND	0x502A0081

#define NA_SE3_SNOWMAN_APPEAR	0x502B0081
#define NA_SE3_DISAPPEAR    0x502C8081
#define NA_SE3_PENGUINVOICE    0x502D0081
#define NA_SE3_WATERBOMB_BOUND    0x502E8081
#define NA_SE3_BOUND_S    0x502F0081
#define NA_SE3_KURIBOU_JUMP	0x502F0081
#define NA_SE3_HANACHAN_JUMP	0x502F6081

#define NA_SE3_KURIBOU_SMASH    0x50308081
#define NA_SE3_ENEMY_SMASH    0x50308081
#define NA_SE3_BATFLAP	    0x50310081
#define NA_SE3_DIVE	    0x50324081
#define NA_SE3_PUKKEN_DISAPPEAR	    0x50334081
#define NA_SE3_NOKONOKO_WALK_B	0x50342081
#define NA_SE3_NOKONOKO_WALK_S	0x50350081
#define NA_SE3_BIGMOTOSWALK	0x50366081
#define NA_SE3_NESSIE_VOICE	0x50376081

#define NA_SE3_KUPPA_MESSAGE	0x50388081

#define NA_SE3_MONKEYVOICE1 0x50390081
#define NA_SE3_MONKEYVOICE2 0x503A0081
#define NA_SE3_MONKEYRUNNING	0x503B0081
#define NA_SE3_MONKEYSCRATCH	0x503C0081
#define NA_SE3_NOKONOKO_CRY_B	0x503DA081
#define NA_SE3_NOKONOKO_CRY_S	0x503EA081
#define NA_SE3_EAGLE_ATTACK	0x503F4081
#define NA_SE3_EAGLE_DAMAGE	0x50406081
#define NA_SE3_BOMBKING_MESSAGE	0x50410081
/*
#define NA_SE3_BOMBKING_DAMAGE	0x50424081
#define NA_SE3_KUMO_WALK	0x50434081
#define NA_SE3_KUMO_JUMP	0x50444081
#define NA_SE3_PENGUINVOICE_S	0x50450081
*/

#define NA_SE3_ENEMY_JUMP	0x50468081
#define NA_SE3_BOMBKING_DOWN	0x5147C081
#define NA_SE3_ENEMY_DOWN	0x5147C081

#define NA_SE3_TERESA_MESSAGE	0x50480081
#define NA_SE3_TERESA_APPEAR	0x50480081
/*
#define NA_SE3_BAT_VOICE	0x50490081
*/
#define NA_SE3_UTSUBO_VOICE	0x524A0081

#define NA_SE3_FINGER	    0x524B0081

#define NA_SE3_SNOWMAN_JUMP	0x504C0081
#define NA_SE3_HEIHOSHOT	0x504D0081
#define NA_SE3_AMENBO_WALK_G	0x504E0081
#define NA_SE3_AMENBO_WALK_W	0x504F0081
#define NA_SE3_PUKKUN_APPEAR	0x50542081
#define NA_SE3_PUKKUN_FIRE	0x50558081
#define NA_SE3_KUPPA_F_SHOT	0x50558081
#define NA_SE3_FIREBALL_SHOT	0x50558081
#define NA_SE3_PIANO_RAMPAGE	0x52564081
/*
#define NA_SE3_MOTOS_HIT_B  0x50570081
*/
#define NA_SE3_BOMBHEI_R_MESSAGE	0x50584081
#define NA_SE3_BOMBHEI_B_MESSAGE	NA_SE3_BOMBHEI_R_MESSAGE
#define NA_SE3_TOGEZOU_BOUND	0x50591081
/*
#define NA_SE3_HAND_DAMAGE  0x525A0081
#define NA_SE3_HAND_DOWN    0x525B0081
*/
#define NA_SE3_HANACHAN_WALK	0x505c4081
#define NA_SE3_THROWUP	    0x505d4081	    /* houri nage */

#define NA_SE3_KUPPA_MESSAGE_MUTE	0x505F8091

#define NA_SE3_KURIBOU_S_DOWN	    0x5060B081	    /* kuribou down */
#define NA_SE3_FURAFURA_DOWN	    0x5060B081	    /* furahura down */
#define NA_SE3_KURIBOU_B_DOWN	    0x5061B081	    /* kuribou down */
#define NA_SE3_BAT_DOWN		    0x5062B081	    /* bat down */
#define NA_SE3_NOKONOKO_S_DOWN	    0x5063B081	    /* nokonoko down */
#define NA_SE3_SANBO_DOWN	    0x5063C081	    /* sanbo down */
#define NA_SE3_HEIHO_DOWN	    0x5063B081	    /* heiho down */
#define NA_SE3_KUMO_DOWN	    0x5063B081	    /* kumo down */
#define NA_SE3_SNOWBODY_JUMP	0x5064C081
#define NA_SE3_SNOWBODY_LAND	0x5065D081
/*
#define NA_SE3_TEREPORT	    0x50668081
#define NA_SE3_MOGURA_APPEAR	0x50678081
*/
#define NA_SE3_HAND_BOUND   0x50684081
/*
#define NA_SE3_BATTANKING_VOICE   0x50694081
*/
#define NA_SE3_USAGI_JUMP_F	0x506A0081	    /** usagi **/
/*
#define NA_SE3_EYEROLL	    0x506B0081	    
*/
#define NA_SE3_USAGI_JUMP_W	0x506C0081	    /** usagi **/
#define NA_SE3_HAND_MOVE	0x506D0081	    /** hand start to move **/
#define NA_SE3_KATSUGINAGE_DOWN	0x516E0081	    /** katsugi nage down **/
#define NA_SE3_HANACHAN_MESSAGE	0x506F0081	    /** hanachan message  **/
#define NA_SE3_HANACHAN_DAMAGE	0x50706081	    /** hanachan down **/
#define NA_SE3_HANACHAN_SLOWWALK    0x50712081	    /** hanachan slow walk **/
#define NA_SE3_HANACHAN_DOWN    0x5072C081	    /** hanachan DOWN **/
#define NA_SE3_BIGPUKUPUKU_BITE    0x50734081	    /** hanachan DOWN **/
#define NA_SE3_DOWN_TO_SMALL	0x50744081	    /** small DOWN **/


/*** Enemy Level ***/
#define NA_LSE3_KUPAFIRE    0x60000001
#define NA_LSE3_TAIHOUSET3  NA_LSE1_TAIHOU_AIM	    /*0x6001ff01*/
#define NA_LSE3_JUGEMU_FLY  0x60028001
#define NA_LSE3_OPENING_JUGEMU_FLY  0x6002FF01

#define NA_LSE3_SPARK	    0x60034001
#define NA_LSE3_SNOWMAN_BREATH	0x60044001
#define NA_LSE3_BURNER	    0x60048001
#define NA_LSE3_PENGUIN_SLIDE	0x60050001
#define NA_LSE3_BOXSLIDE    0x60050001	    /* box slide */
#define NA_LSE3_THROWUP_MOVE    0x60064001	    /* houri nage */
#define NA_LSE3_FURAFURA_MOVE	0x60070001	    /* furafura */
#define NA_LSE3_BOMBHEI_STEAM	0x60086001	    /* steam */
#define NA_LSE3_GHOST_WIND	0x60098001	    /* steam */
#define NA_LSE3_KATSUGINAGE_MOVE    0x600A4001	    /* katugi nage */
#define NA_LSE2_PEACH_STAR  0x600B4001	    /*end demo peach kirakira */


/*** system Trigger ***/
#define NA_SYS_CURSOR	    0x7000F881
#define NA_SYS_ENTER	    0x70010081
#define NA_SYS_PAUSE_ON     0x7002FF81
#define NA_SYS_PAUSE_OFF    0x7003FF81
#define NA_SYS_WIN_OPEN	    0x70040081
#define NA_SYS_WIN_CLOSE    0x70050081
#define NA_SYS_ZOOM_IN	    0x70060081
#define NA_SYS_ZOOM_OUT     0x70070081
#define NA_SYS_PICK         0x70080081
#define NA_SYS_TREMBLE      0x70090081
#define NA_SYS_HAND_A       0x700a0081
#define NA_SYS_HAND_D       0x700b0081
#define NA_SYS_HAND_F       0x700c0081
#define NA_SYS_RECOVER      0x700d0081
#define NA_SYS_BUZZER	    0x700e0081
#define NA_SYS_LOOK         0x700f4081
#define NA_SYS_STOP00       0x70100081
#define NA_SYS_CAMERAMOVE   0x700f0081
#define NA_SYS_CLICK	    0x70110081
#define NA_SYS_ALRIGHT	    0x70120081
#define NA_SYS_MESSAGEPASS  0x70130081
#define NA_SYS_CREDIT	    0x70140081
#define NA_SYS_COUNTUP	    0x70150081
/*
#define NA_SE2_PIPEWARP	    0x70160081	    /* WARP TO ANOTHER WORLD */

#define NA_SYS_DOKAN_IN	    0x7016A081	    /*  */
#define NA_SYS_POWER_UP	    0x7017A081	    /*  */
#define NA_SYS_KUPPA_LAUGH  0x70188081	    /* mario down wipe */
#define NA_SYS_PIPEWARP	    0x71198081
#define NA_SYS_CAMERA_SHUTTER	0x701A8081  /* camera mode change */
#define NA_SYS_DOKAN_APPEAR	0x701B8081  /* opening dokan out  */
#define NA_SYS_DOKAN_DISAPPEAR	0x701C8081  /* opening dokan in */
#define NA_SYS_RETURN_CASTLE	0x701DB081  /* return to castle */
#define NA_SYS_SET		0x701EFF81  /* system kirari */
#define NA_SYS_END_MESSAGE	0x701FFF81  /* system kirari */
#define NA_SYS_KANBAN_OPEN	0x70202081  /* kanban open */
#define NA_SYS_KANBAN_CLOSE	0x70212081  /* kanban close */
#define NA_SYS_HIGH_SCORE	0x70222081  /* kanban close */
#define NA_SYS_FILE_SELECT	0x7023FF81  /* file select */
#define NA_SYS_LETS_GO		0x7024FF81  /* into wall */

#define NA_SYS_REDCOIN	    0x78289081	    /* get red coin  */
#define NA_SYS_SECRETPOINT	    0x70302081	    /* get red coin  */


/*** DISMISSED SE FLAG ***/
/*
#define NA_SE3_HIT	    0x501b0081 	    /* Taihou Bomb!   */

/*** bird chirp at main map ***/
#define NA_ENV_BIRD0	    0x60104001	    /* bird03*/
#define NA_ENV_BIRD1	    0x90524001 	    /* bird02*/	    
#define NA_ENV_BIRD2	    0x80504001 	    /* bird00*/
#define NA_ENV_BIRD3	    0x50514001 	    /* bird01*/
#define NA_ENV_TIME_SLOW    0x8054F011 	    /* time slow */
#define NA_ENV_TIME_FAST    0x8055F011 	    /* time slow */


#define NA_SE1_KICK	    NA_VC1_KICK	    /* Kick  an object */

#define NA_SE2_KIRAI	    0x802e2081
#define NA_SE2_PLATESWITCH	    0x803EC081
#define NA_SE2_PYRAMID_SHAKING  0x814BE081
#define NA_SE2_PYRAMID_EXPLODE  0x814CF081
#define NA_SE2_ROLLBOX_END	0x80400081
#define NA_SE2_STAR_APPEAR	0x8057FF91
#define NA_SE2_ROLLBOX_START	0x80590081
#define NA_SE2_KUPPA_DISAPPEAR	0x80600081
#define NA_SE2_KUPPA_KEY_FALL	0x80610081
#define NA_SE2_ROLLER_ROLL	0x80482081
#define NA_SE2_ONEUP_APPEAR	0x8063D081
//#define NA_SE2_GET_ALLREDCOIN   0x8068A081
#define NA_SE2_CORRECT_CHIME	0x806AA081

#define NA_SE3_FLOWERVOICE  0x90105081
#define NA_SE3_FLOWERDOWN   0x90116081

#define NA_SE3_KUPAGAO	    0x90040081
#define NA_SE3_MOTOS_HIT_S  0x901c0081
#define NA_SE3_MOTOS_HIT_B  0x90570081
#define NA_SE3_PUZZLE	    0x90192081

#define NA_SE3_BOMBKING_DAMAGE	0x91424081
#define NA_SE3_KUMO_WALK	0x90434081
#define NA_SE3_KUMO_JUMP	0x90444081
#define NA_SE3_PENGUINVOICE_S	0x90450081
#define NA_SE3_BATVOICE	    0x90490081
#define NA_SE3_HAND_DAMAGE  0x935A0081
#define NA_SE3_BATTANKING_DAMAGE    0x935AC081
#define NA_SE3_HAND_DOWN    0x925B0081
#define NA_SE3_TEREPORT	    0x90668081
#define NA_SE3_MOGURA_APPEAR	0x90678081
#define NA_SE3_EYEROLL	    0x906B0081	    /** usagi **/
#define NA_SE3_BATTANKING_VOICE   0x90694081


/* @@@@@@@@@@@@@@@@@@@@@@@
          Const.
   @@@@@@@@@@@@@@@@@@@@@@@ */

#define NA_VIEWMODE_VOL  40
#define NA_SEBGMMUTE_VOL  20
#define NA_FANFARE_BGMMUTE_VOL  20
#define NA_FANFARE_BGMMUTE_SPD  50
#define MESSAGE_MUTE_SPEED    50   /* 0 */
#define NA_ENDING_SPEC 7

#define NA_PUKKUN_VOL  0
#define NA_MERRY_VOL00  45
#define NA_MERRY_VOL01  20
#define NA_MERRY_VOL10   0
#define NA_MERRY_VOL11  78
#define Na_LockSe()		Na_PortLock(NA_SE_HANDLE, 0x037a)
#define Na_UnlockSe()		Na_PortUnlock(NA_SE_HANDLE, 0x037a)
#define Na_Title_LockSe()	Na_PortLock(NA_SE_HANDLE, 0x017f)
#define Na_Title_UnlockSe()	Na_PortUnlock(NA_SE_HANDLE, 0x017f)
#define Na_Ending_LockSe()	Na_PortLock(NA_SE_HANDLE, 0x03ff)
#define Na_Ending_UnlockSe()	Na_PortUnlock(NA_SE_HANDLE, 0x03ff)
#define Na_Opening_LockSe()	Na_PortLock(NA_SE_HANDLE, 0x0330)
#define Na_Opening_UnlockSe()	Na_PortUnlock(NA_SE_HANDLE, 0x0330)
#define Na_Enddemo_LockSe()	Na_PortLock(NA_SE_HANDLE, 0x03f0)
#define Na_Enddemo_UnlockSe()	Na_PortUnlock(NA_SE_HANDLE, 0x03f0)


/*********************
function extern define
    1996. 1.12(FRI)
*********************/
extern void interface_init(void);
extern void     Nas_InitAudio(void);
extern RSPTask  *Nas_AudioMain(void);

#define Na_interface_init()	interface_init()
#define InitAudio()	Nas_InitAudio()
#define AudioMain()	Nas_AudioMain()

/*********************
function extern define
    for old entry.m
    1996. 1.18(THU)
*********************/
extern void Na_PicMorphParam(u16);
extern void Na_WritePort(u8, u8, u8, u8);


/*
#define Na_EnvLevelStop()    Na_SeStop(NA_SE_HANDLE,NA_ENV_LEVEL,NA_PORT_LEVEL1)
#define Na_SeStop(h, n, m)	Na_AllSeStop(n)
*/
/*
#define Na_Se1Start(h, s, n, v) Na_SeFlagEntry(n, &_zeroptr[0])
*/

#define Na_PicMorphStart()   Na_WritePort(NA_SE_HANDLE, NA_PIC_MORPH, NA_PORT_TRIG1,1)
#define Na_PicMorphEnd()     Na_WritePort(NA_SE_HANDLE, NA_PIC_MORPH, NA_PORT_TRIG1+4, 255)

/* @@@@@@@@@@@@@@@@@@@@
       Ports
   @@@@@@@@@@@@@@@@@@@@ */
/*
#define NA_PORT_TRIG1     0x00	    /* Single SE Mode */
/*
#define NA_PORT_LEVEL1    0x00
*/

/* @@@@@@@@@@@@@@@@@@@@@@
       SYSTEM TRIGGER
   @@@@@@@@@@@@@@@@@@@@@@ */
/*
#define NA_ENV_TRIG     0x03
*/
/*
#define NA_ENV_LEVEL    0x04
#define NA_PIC_MORPH  0x08
*/

