/********************************************************************************
						Ultra 64 MARIO Brothers

					 stage23 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 11, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx gfx_wl_a[]		 ;
extern Gfx gfx_wl_ap[]		 ;
extern Gfx gfx_wl_a_a[]		 ;
extern Gfx gfx_wl_a_ht[]	 ;

extern Gfx gfx_wl_b[]		 ;
extern Gfx gfx_wl_bp[]		 ;
extern Gfx gfx_wl_b_a[]		 ;
extern Gfx gfx_wl_b_ht[]	 ;
extern Gfx gfx_wl_b_sango[]	 ;
extern Gfx gfx_wl_b_ng[]	 ;

extern Gfx gfx_wl_b_htk[]	 ;
extern Gfx gfx_wl_b_huta[]	 ;
extern Gfx gfx_wl_b_huta_ht[];
extern Gfx gfx_wl_bo[]		 ;

extern Gfx gfx_sm[]			 ;
extern Gfx gfx_sm_s[]		 ;


/* ===============================================================================
        : Hierarchy map data of WlandBou.
================================================================================== */
Hierarchy RCP_HmsWlandBou[] = {
    hmsHeader(700)
    hmsBegin()
		hmsShadow(120, 180, 10)
		hmsBegin()
	        hmsGfx(RM_SURF, gfx_wl_bo)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of WlandFuta.
================================================================================== */
Hierarchy RCP_HmsWlandHuta[] = {
    hmsHeader(10000)
    hmsBegin()
        hmsGfx(RM_XSURF, gfx_wl_b_htk)
        hmsGfx(RM_SURF , gfx_wl_b_huta)
        hmsGfx(RM_XSURF, gfx_wl_b_huta_ht)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of WlandShip.
================================================================================== */
Hierarchy RCP_HmsWlandSm[] = {
    hmsHeader(10000)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_sm)
        hmsGfx(RM_SPRITE, gfx_sm_s)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage23Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(0, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR,ZoomControl)
			hmsBegin()
				hmsCamera(CAM_INSIDE,  0,2000,6000,  2560,0,512, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF  , gfx_wl_a)
					hmsGfx(RM_SURF  , gfx_wl_ap)
					hmsGfx(RM_SPRITE, gfx_wl_a_a)
					hmsGfx(RM_XSURF , gfx_wl_a_ht)
					hmsCProg(0	   , WaterInit)
					hmsCProg(0x2301, WaterDraw)
					hmsObject()
					hmsCProg(13, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 2.														*/
/********************************************************************************/
Hierarchy RCP_Stage23Scene2[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0,0,0,1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON,  0,2000,6000,  2560,0,512, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF  , gfx_wl_b)
					hmsGfx(RM_SURF  , gfx_wl_bp)
					hmsGfx(RM_SPRITE, gfx_wl_b_a)
					hmsGfx(RM_XSURF , gfx_wl_b_ht)
					hmsGfx(RM_SPRITE, gfx_wl_b_sango)
					hmsGfx(RM_SURF  , gfx_wl_b_ng)
					hmsCProg(0	   , WaterInit)
					hmsCProg(0x2302, WaterDraw)
					hmsObject()
					hmsCProg(14, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
