/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage5 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx gfx_st_body[]	;
extern Gfx gfx_st_yuka[]	;
extern Gfx gfx_st_ft[]		;
extern Gfx gfx_st_saku[]	;
extern Gfx gfx_st_kage[]	;

extern Gfx gfx_st_ie1a[]	;
extern Gfx gfx_st_ie1b[]	;
extern Gfx gfx_st_ie2a[]	;
extern Gfx gfx_st_ie2b[]	;
extern Gfx gfx_st_ie2c[]	;
extern Gfx gfx_st_dai[]		;
extern Gfx gfx_st_hasira[]	;
extern Gfx gfx_st_ura_a[]	;
extern Gfx gfx_st_ura_b[]	;
extern Gfx gfx_st_ura_c[]	;

extern Gfx gfx_rift[]		;
extern Gfx gfx_rift_wa[]	;
extern Gfx gfx_snow_rock[]	;
extern Gfx gfx_snow_head[]	;
extern Gfx gfx_snow_eye[]	;

extern Gfx gfx_s_s[]		;
extern Gfx gfx_s_s_inout[]	;
extern Gfx gfx_s_s_gate[]	;
extern Gfx gfx_s_s_alpha[]	;
extern Gfx gfx_s_s_mado[]	;
extern Gfx gfx_s_s_clear[]	;
extern Gfx gfx_s_s_ia[]		;

/* ===============================================================================
        : Hierarchy map data of YukiyamaRift.
================================================================================== */
Hierarchy RCP_HmsYukiyamaRift[] = {
    hmsHeader(500)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_rift)
        hmsGfx(RM_SURF  , gfx_rift_wa)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Yuki_rock.
================================================================================== */
Hierarchy RCP_HmsEnemySnow_rock[] = {
	hmsShadow(400, 200, 1)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_snow_rock)
	hmsEnd()
	hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of EnemySnow_head.
================================================================================== */
Hierarchy RCP_HmsEnemySnow_head[] = {
    hmsHeader(300)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_snow_head)
        hmsGfx(RM_SPRITE, gfx_snow_eye)
    hmsEnd()
    hmsExit()
};


/* ------------------------------------------------------------------------------------	*/
/* ------------------------------------------------------------------------------------	*/


/* ===============================================================================
        : Hierarchy map data of Ym1Ie1.
================================================================================== */
Hierarchy RCP_HmsYm1Ie1[] = {
    hmsHeader(800)
    hmsBegin()
		hmsLOD(-1000, 4000)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_st_ie1a)
	        hmsGfx(RM_SPRITE, gfx_st_ie1b)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ym1Ie2.
================================================================================== */
Hierarchy RCP_HmsYm1Ie2[] = {
    hmsHeader(900)
    hmsBegin()
		hmsLOD(-1000, 7000)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_st_ie2a)
	        hmsGfx(RM_SPRITE, gfx_st_ie2b)
	        hmsGfx(RM_XSURF , gfx_st_ie2c)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ym1Dai.
================================================================================== */
Hierarchy RCP_HmsYm1Dai[] = {
    hmsHeader(400)
    hmsBegin()
		hmsLOD(-500, 7000)
		hmsBegin()
	        hmsGfx(RM_SURF, gfx_st_dai)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ym1Hasira.
================================================================================== */
Hierarchy RCP_HmsYm1Hasira[] = {
    hmsHeader(800)
    hmsBegin()
		hmsLOD(-1000, 7000)
		hmsBegin()
	        hmsGfx(RM_XSURF, gfx_st_hasira)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ym1Ura.
================================================================================== */
Hierarchy RCP_HmsYm1Ura[] = {
    hmsHeader(3500)
    hmsBegin()
		hmsLOD(-3000, 6000)
		hmsBegin()
	        hmsGfx(RM_SURF  , gfx_st_ura_a)
	        hmsGfx(RM_SPRITE, gfx_st_ura_b)
	        hmsGfx(RM_XSURF , gfx_st_ura_c)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage5Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(4, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_FIELD,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF	, gfx_st_body)
					hmsGfx(RM_SURF	, gfx_st_yuka)
					hmsGfx(RM_SURF	, gfx_st_ft	 )
					hmsGfx(RM_SPRITE, gfx_st_saku)
					hmsGfx(RM_XDECAL, gfx_st_kage)
					hmsCProg(0, WaterInit)
					hmsCProg(0x0501, WaterDraw)
					hmsObject()
					hmsCProg(1, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

		hmsLayer(0)
		hmsBegin()
			hmsCProg(0, CannonEdge)
		hmsEnd()

	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 2.														*/
/********************************************************************************/
Hierarchy RCP_Stage5Scene2[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0, 0, 0, 1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_BACK,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF  , gfx_s_s)
					hmsGfx(RM_SURF  , gfx_s_s_inout)
					hmsGfx(RM_DECAL	, gfx_s_s_gate)
					hmsGfx(RM_SPRITE, gfx_s_s_alpha)
					hmsGfx(RM_SPRITE, gfx_s_s_mado)
					hmsGfx(RM_XSURF , gfx_s_s_clear)
					hmsGfx(RM_XSURF , gfx_s_s_ia)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

	hmsEnd()
	hmsExit()
};
