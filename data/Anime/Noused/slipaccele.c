/********************************************************************************
						Ultra 64 MARIO Brothers

				   mario chase accele animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							Swptember 04, 1995
 ********************************************************************************/

#include "../headers.h"


static short mario_slip_accel_prm[]={
	    0,   54,16383, -118, -594,-1823,-3504,-5335,
	-7015,-8244,-8721,-1664,-1865,-2384,-3093,-3865,
	-4574,-5093,-5294,-3985,-3853,-3513,-3048,-2541,
	-2076,-1735,-1604,-10742,-11611,-12815,-12251,-6354,
	    0,    0,    0,-24240,-28189,-37050,-46348,-56183,
	-67390,-76592,-80409,-13163,-14529,-17595,-20812,-24215,
	-28093,-31276,-32597,19238,22201,28850,35827,43208,
	51618,58523,61387,    0,  324, 1163, 2309, 3557,
	 4703, 5541, 5866,    0,  309, 1107, 2197, 3386,
	 4477, 5274, 5584,-4724,-4662,-4501,-4281,-4041,
	-3821,-3660,-3597,-9282,-9068,-8517,-7763,-6942,
	-6188,-5637,-5423,12583,11221, 9624,11531,19818,
	31674,42304,46913,12698,12748,12651,12045,10518,
	 8437, 6602, 5812, 6305, 3635, -742,-1375, 4884,
	14685,23722,27691,   27,  603,-13531, 1600,  -59,
	    2,  162,  381,  620,  839,  999, 1061, -335,
	 -307, -234, -134,  -26,   72,  145,  173,-25567,
	-25766,-26277,-26977,-27739,-28439,-28950,-29149,  149,
	 -247,-13246, 2503,    0,  -84, -301, -599, -923,
	-1220,-1437,-1522,  108,   99,   76,   45,   11,
	  -19,  -42,  -50,-24265,-24569,-25353,-26426,-27594,
	-28666,-29450,-29754,32767,32767,16753,-16545,32767,
	32767,16753,-16545,    0,  -67, -219, -383, -487,
	 -471, -389, -340,  834,  803,  735,  665,  626,
	  657,  724,  761, 6165, 5208, 3040,  715, -712,
	 -346,  988, 1772,    0,  -22,  -79, -157, -242,
	 -321, -378, -400, -834, -828, -814, -794, -772,
	 -752, -737, -732, 5100, 5390, 6137, 7158, 8271,
	 9292,10039,10329, 7786, 7986, 8443, 9052, 9709,
	10308,10744,10913
};
static short mario_slip_accel_tbl[]={
	    1,    0,	/* chn14translate x */
	    1,    1,	/* chn14 translate y */
	    1,    0,	/* chn14 translate z */
	    1,    0,	/* chn14 rotate x */
	    1,    2,	/* chn14 rotate y */
	    1,    0,	/* chn14 rotate z */
	    1,    0,	/* <m_waist>1_3 rotate x */
	    1,    0,	/* <m_waist>1_3 rotate y */
	    8,  227,	/* <m_waist>1_3 rotate z */
	    8,  203,	/* <m_body>1 rotate x */
	    8,  211,	/* <m_body>1 rotate y */
	    8,  219,	/* <m_body>1 rotate z */
	    8,  179,	/* <m_head>2 rotate x */
	    8,  187,	/* <m_head>2 rotate y */
	    8,  195,	/* <m_head>2 rotate z */

	    1,  175,	/* chn6 rotate x */
	    1,  176,	/* chn6 rotate y */
	    1,  177,	/* chn6 rotate z */
	    8,   35,	/* <m_larmA>1 rotate x */
	    8,   43,	/* <m_larmA>1 rotate y */
	    8,   51,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	    8,   27,	/* <m_larmB>1 rotate z */
	    8,    3,	/* <m_lhand>1 rotate x */
	    8,   11,	/* <m_lhand>1 rotate y */
	    8,   19,	/* <m_lhand>1 rotate z */

	    1,  171,	/* chn10 rotate x */
	    1,  172,	/* chn10 rotate y */
	    1,  173,	/* chn10 rotate z */
	    8,   91,	/* <m_rarmA>1 rotate x */
	    8,   99,	/* <m_rarmA>1 rotate y */
	    8,  107,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	    8,   83,	/* <m_rarmB>1 rotate z */
	    8,   59,	/* <m_rhand>1 rotate x */
	    8,   67,	/* <m_rhand>1 rotate y */
	    8,   75,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  174,	/* chn15 rotate z */
	    8,  147,	/* <m_llegA>1 rotate x */
	    8,  155,	/* <m_llegA>1 rotate y */
	    8,  163,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	    1,  146,	/* <m_llegB>1 rotate z */
	    1,  143,	/* <m_ltoot>1 rotate x */
	    1,  144,	/* <m_ltoot>1 rotate y */
	    1,  145,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  178,	/* chn17 rotate z */
	    8,  119,	/* <m_rlegA>1 rotate x */
	    8,  127,	/* <m_rlegA>1 rotate y */
	    8,  135,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	    1,  118,	/* <m_rlegB>1 rotate z */
	    1,  115,	/* <m_rfoot>1 rotate x */
	    1,  116,	/* <m_rfoot>1 rotate y */
	    1,  117,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioSlipAccele = {
	MAP_ANIM_ONETIME,			/* attribute       	 */
	  0,						/* syncro			 */
	  0,						/* start frame     	 */
	  0,						/* loop frame		 */
	  8,						/* number of frames	 */
	 20,						/* number of joints	 */
	mario_slip_accel_prm,
	mario_slip_accel_tbl
};
