/********************************************************************************
						Ultra 64 MARIO Brothers

				  stage33 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							Decemver 11, 1995
*********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE33_FOG_R		200
#define		STAGE33_FOG_G		 50
#define		STAGE33_FOG_B		  0
#define		STAGE33_FOG_START	960

#include "kopa_round2/kopa2_ext_texture.h"		//	Texture.

#include "kopa_round2/kopa2_A_shape.sou"
#include "kopa_round2/kopa2_B_shape.sou"

#include "kopa_round2/cx3301.flk"
#include "kopa_round2/kopa2_A.flk"
