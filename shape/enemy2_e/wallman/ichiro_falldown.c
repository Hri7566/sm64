/* ichiro_falldown */
#include<ultra64.h>
#define ShapeColor_ichiro_falldown(R,G,B) {{R/4,G/4,B/4,0,R/4,G/4,B/4,0},{R,G,B,0,R,G,B,0,0,0,120,0}}
static Lights1 light_ichiro_falldown[]={
};
static Vtx vtx_ichiro_falldown0[]={
	{152,-259,2,0,0,0,-86,-92,0,0},
	{157,-143,199,0,0,0,-93,-37,77,0},
	{-29,-89,2,0,0,0,-125,-22,0,0},
	{173,181,176,0,0,0,-90,44,76,255},
	{178,255,2,0,0,0,-67,107,0,255},
	{21,157,2,0,0,0,-103,73,-1,255},
	{159,-151,-189,0,0,0,126,-6,0,255},
	{175,180,-170,0,0,0,126,-6,0,255},
	{178,255,2,0,0,0,126,-6,0,255},
	{173,181,176,0,0,0,126,-6,0,255},
	{157,-143,199,0,0,0,126,-6,0,255},
	{152,-259,2,0,0,0,126,-6,0,255},
	{175,180,-170,0,0,0,-89,43,-78,255},
	{159,-151,-189,0,0,0,-92,-38,-78,255},
};
static Vtx vtx_ichiro_falldown1[]={
	{-29,-89,-2,0,0,0,-125,-22,0,0},
	{157,-143,-199,0,0,0,-93,-37,-77,0},
	{152,-259,-2,0,0,0,-86,-92,0,0},
	{21,157,-2,0,0,0,-103,73,1,255},
	{178,255,-2,0,0,0,-67,107,0,255},
	{173,181,-176,0,0,0,-90,44,-76,255},
	{152,-259,-2,0,0,0,126,-6,0,255},
	{157,-143,-199,0,0,0,126,-6,0,255},
	{173,181,-176,0,0,0,126,-6,0,255},
	{178,255,-2,0,0,0,126,-6,0,255},
	{175,180,170,0,0,0,126,-6,0,255},
	{159,-151,189,0,0,0,126,-6,0,255},
	{159,-151,189,0,0,0,-92,-38,78,255},
	{175,180,170,0,0,0,-89,43,78,255},
};
static Vtx vtx_ichiro_falldown2[]={
	{-151,0,-151,0,0,0,0,-127,0,0},
	{151,0,-151,0,0,0,0,-127,0,0},
	{151,0,151,0,0,0,0,-127,0,0},
	{-151,0,151,0,0,0,0,-127,0,255},
};
static Vtx vtx_ichiro_falldown3[]={
	{287,-48,-65,0,0,0,-16,-119,-38,0},
	{287,-79,27,0,0,0,-16,-119,-38,0},
	{-10,-34,12,0,0,0,-16,-119,-38,0},
	{-10,-21,-27,0,0,0,-16,-119,-38,255},
	{287,0,84,0,0,0,-16,74,101,255},
	{287,79,27,0,0,0,-16,74,101,255},
	{-10,34,12,0,0,0,-16,74,101,255},
	{-10,0,37,0,0,0,-16,74,101,255},
	{287,-79,27,0,0,0,-16,-74,101,255},
	{287,0,84,0,0,0,-16,-74,101,255},
	{-10,0,37,0,0,0,-16,-74,101,255},
	{-10,-34,12,0,0,0,-16,-74,101,255},
	{287,48,-65,0,0,0,-16,0,-125,255},
	{287,-48,-65,0,0,0,-16,0,-125,255},
	{-10,-21,-27,0,0,0,-16,0,-125,255},
	{-10,21,-27,0,0,0,-16,0,-125,255},
	{287,79,27,0,0,0,-16,119,-38,255},
	{287,48,-65,0,0,0,-16,119,-38,0},
	{-10,21,-27,0,0,0,-16,119,-38,0},
	{-10,34,12,0,0,0,-16,119,-38,255},
};
static Vtx vtx_ichiro_falldown4[]={
	{-151,0,-151,0,0,0,0,-127,0,0},
	{151,0,-151,0,0,0,0,-127,0,0},
	{151,0,151,0,0,0,0,-127,0,0},
	{-151,0,151,0,0,0,0,-127,0,255},
};
static Vtx vtx_ichiro_falldown5[]={
	{-10,-21,27,0,0,0,-16,-119,38,0},
	{-10,-34,-12,0,0,0,-16,-119,38,0},
	{287,-79,-27,0,0,0,-16,-119,38,0},
	{287,-48,65,0,0,0,-16,-119,38,255},
	{-10,0,-37,0,0,0,-16,74,-101,255},
	{-10,34,-12,0,0,0,-16,74,-101,255},
	{287,79,-27,0,0,0,-16,74,-101,255},
	{287,0,-84,0,0,0,-16,74,-101,255},
	{-10,-34,-12,0,0,0,-16,-74,-101,255},
	{-10,0,-37,0,0,0,-16,-74,-101,255},
	{287,0,-84,0,0,0,-16,-74,-101,255},
	{287,-79,-27,0,0,0,-16,-74,-101,255},
	{-10,21,27,0,0,0,-16,0,125,255},
	{-10,-21,27,0,0,0,-16,0,125,255},
	{287,-48,65,0,0,0,-16,0,125,255},
	{287,48,65,0,0,0,-16,0,125,255},
	{-10,34,-12,0,0,0,-16,119,38,255},
	{-10,21,27,0,0,0,-16,119,38,0},
	{287,48,65,0,0,0,-16,119,38,0},
	{287,79,-27,0,0,0,-16,119,38,255},
};
static Vtx vtx_ichiro_falldown6[]={
	{-52,605,-200,0,0,0,0,127,0,0},
	{-52,605,200,0,0,0,0,127,0,0},
	{1547,594,200,0,0,0,0,127,0,0},
	{1547,594,-200,0,0,0,0,127,0,255},
	{-52,605,200,0,0,0,0,0,127,255},
	{-60,-594,200,0,0,0,0,0,127,255},
	{1539,-605,200,0,0,0,0,0,127,255},
	{1547,594,200,0,0,0,0,0,127,255},
	{-60,-594,200,0,0,0,0,-127,0,255},
	{-60,-594,-200,0,0,0,0,-127,0,255},
	{1539,-605,-200,0,0,0,0,-127,0,255},
	{1539,-605,200,0,0,0,0,-127,0,255},
	{-60,-594,-200,0,0,0,0,0,-127,255},
	{-52,605,-200,0,0,0,0,0,-127,255},
	{1547,594,-200,0,0,0,0,0,-127,255},
	{1539,-605,-200,0,0,0,0,0,-127,255},
	{-60,-594,-200,0,0,0,-127,0,0,255},
	{-60,-594,200,0,0,0,-127,0,0,0},
	{-52,605,200,0,0,0,-127,0,0,0},
	{-52,605,-200,0,0,0,-127,0,0,255},
	{1547,594,-200,0,0,0,127,0,0,255},
	{1547,594,200,0,0,0,127,0,0,255},
	{1539,-605,200,0,0,0,127,0,0,255},
	{1539,-605,-200,0,0,0,127,0,0,255},
};
Gfx RCP_ichiro_falldown0[]={
	gsSPLight((&light_ichiro_falldown[0].l[0]),1),
	gsSPLight((&light_ichiro_falldown[0].a),2),
	gsSPVertex(&vtx_ichiro_falldown0[0],14,0),
	gsSP1Triangle(0,1,2,0),
	gsSP1Triangle(3,4,5,0),
	gsSP1Triangle(6,7,8,0),
	gsSP1Triangle(6,8,9,0),
	gsSP1Triangle(6,9,10,0),
	gsSP1Triangle(6,10,11,0),
	gsSP1Triangle(12,13,2,0),
	gsSP1Triangle(2,13,0,0),
	gsSP1Triangle(12,2,5,0),
	gsSP1Triangle(2,1,3,0),
	gsSP1Triangle(5,4,12,0),
	gsSP1Triangle(5,2,3,0),
	gsSPEndDisplayList()
};
Gfx RCP_ichiro_falldown1[]={
	gsSPLight((&light_ichiro_falldown[0].l[0]),1),
	gsSPLight((&light_ichiro_falldown[0].a),2),
	gsSPVertex(&vtx_ichiro_falldown1[0],14,0),
	gsSP1Triangle(0,1,2,0),
	gsSP1Triangle(3,4,5,0),
	gsSP1Triangle(6,7,8,0),
	gsSP1Triangle(6,8,9,0),
	gsSP1Triangle(6,9,10,0),
	gsSP1Triangle(6,10,11,0),
	gsSP1Triangle(0,12,13,0),
	gsSP1Triangle(2,12,0,0),
	gsSP1Triangle(3,0,13,0),
	gsSP1Triangle(5,1,0,0),
	gsSP1Triangle(13,4,3,0),
	gsSP1Triangle(5,0,3,0),
	gsSPEndDisplayList()
};
Gfx RCP_ichiro_falldown2[]={
	gsSPLight((&light_ichiro_falldown[0].l[0]),1),
	gsSPLight((&light_ichiro_falldown[0].a),2),
	gsSPVertex(&vtx_ichiro_falldown2[0],4,0),
	gsSP1Triangle(0,1,2,0),
	gsSP1Triangle(0,2,3,0),
	gsSPEndDisplayList()
};
Gfx RCP_ichiro_falldown3[]={
	gsSPLight((&light_ichiro_falldown[0].l[0]),1),
	gsSPLight((&light_ichiro_falldown[0].a),2),
	gsSPVertex(&vtx_ichiro_falldown3[0],16,0),
	gsSP1Triangle(0,1,2,0),
	gsSP1Triangle(0,2,3,0),
	gsSP1Triangle(4,5,6,0),
	gsSP1Triangle(4,6,7,0),
	gsSP1Triangle(8,9,10,0),
	gsSP1Triangle(8,10,11,0),
	gsSP1Triangle(12,13,14,0),
	gsSP1Triangle(12,14,15,0),
	gsSPVertex(&vtx_ichiro_falldown3[16],4,0),
	gsSP1Triangle(0,1,2,0),
	gsSP1Triangle(0,2,3,0),
	gsSPEndDisplayList()
};
Gfx RCP_ichiro_falldown4[]={
	gsSPLight((&light_ichiro_falldown[0].l[0]),1),
	gsSPLight((&light_ichiro_falldown[0].a),2),
	gsSPVertex(&vtx_ichiro_falldown4[0],4,0),
	gsSP1Triangle(0,1,2,0),
	gsSP1Triangle(0,2,3,0),
	gsSPEndDisplayList()
};
Gfx RCP_ichiro_falldown5[]={
	gsSPLight((&light_ichiro_falldown[0].l[0]),1),
	gsSPLight((&light_ichiro_falldown[0].a),2),
	gsSPVertex(&vtx_ichiro_falldown5[0],16,0),
	gsSP1Triangle(0,1,2,0),
	gsSP1Triangle(0,2,3,0),
	gsSP1Triangle(4,5,6,0),
	gsSP1Triangle(4,6,7,0),
	gsSP1Triangle(8,9,10,0),
	gsSP1Triangle(8,10,11,0),
	gsSP1Triangle(12,13,14,0),
	gsSP1Triangle(12,14,15,0),
	gsSPVertex(&vtx_ichiro_falldown5[16],4,0),
	gsSP1Triangle(0,1,2,0),
	gsSP1Triangle(0,2,3,0),
	gsSPEndDisplayList()
};
Gfx RCP_ichiro_falldown6[]={
	gsSPLight((&light_ichiro_falldown[0].l[0]),1),
	gsSPLight((&light_ichiro_falldown[0].a),2),
	gsSPVertex(&vtx_ichiro_falldown6[0],16,0),
	gsSP1Triangle(0,1,2,0),
	gsSP1Triangle(0,2,3,0),
	gsSP1Triangle(4,5,6,0),
	gsSP1Triangle(4,6,7,0),
	gsSP1Triangle(8,9,10,0),
	gsSP1Triangle(8,10,11,0),
	gsSP1Triangle(12,13,14,0),
	gsSP1Triangle(12,14,15,0),
	gsSPVertex(&vtx_ichiro_falldown6[16],8,0),
	gsSP1Triangle(0,1,2,0),
	gsSP1Triangle(0,2,3,0),
	gsSP1Triangle(4,5,6,0),
	gsSP1Triangle(4,6,7,0),
	gsSPEndDisplayList()
};
Gfx light_sourcex[]={
	gsSPNumLights(NUMLIGHTS_1),
	gsSPEndDisplayList()
};
static short ichiro_falldown_prm[]={
	    0,    5,  180,  104,    0,    0,    0,    0,
	    1,    1,    1,    1,    0,    0,    0,    0,
	    0,    0,    0,-15887,-16017,-16327,-16698,-17008,
	-17138,-17096,-16984,-16820,-16626,-16421,-16224,-16056,
	-15938,-15887,    0,    0,    2,    5,    7,    8,
	    7,    7,    6,    4,    3,    2,    1,    0,
	    0,    0,    0,    0,    0,    1,    1,    1,
	    1,    0,    0,    0,    0,    0,    0,    0,
	-15887,-16017,-16327,-16698,-17008,-17138,-17096,-16984,
	-16820,-16626,-16421,-16224,-16056,-15938,-15887,    3,
	    3,    4,    4,    5,    6,    6,    7,    8,
	    9,   10,   10,   11,   11,   11, 6675, 6953,
	 7734, 8934,10470,12260,14219,16266,18317,20290,
	22101,23668,24907,25736,26071,-16380,-16380,-16381,
	-16381,-16381,-16382,-16382,-16383,-16384,-16384,-16385,
	-16385,-16385,-16386,-16386,    0,    0,    1,    3,
	    6,    9,   13,   16,   20,   23,   26,   29,
	   31,   33,   33,-6362,-6647,-7444,-8670,-10239,
	-12067,-14069,-16159,-18255,-20270,-22120,-23720,-24986,
	-25833,-26176,-16384,-16384,-16384,-16384,-16384,-16383,
	-16383,-16383,-16383,-16383,-16382,-16382,-16382,-16382,
	-16382, 8226,16383,-8226, 8226,16383,-8226, 8226,
	16383,-8226, 8226,16383,-8226,    0, -151, -513,
	 -946,-1308,-1460,-1411,-1279,-1089, -862, -622,
	 -393, -198,  -59,    0,16453,16453,16453,16453,
	16453,16453,16453,16453,16453,16453,16453,16453,
	16453,16453,16453,};
static unsigned short ichiro_falldown_tbl[]={
	    1,    1,	/* chn18translate x */
	    1,    2,	/* chn18 translate y */
	    1,    0,	/* chn18 translate z */
	    1,    0,	/* chn18 rotate x */
	    1,    0,	/* chn18 rotate y */
	    1,    3,	/* chn18 rotate z */
	    1,    0,	/* ichi_Rfoot rotate x */
	   15,    4,	/* ichi_Rfoot rotate y */
	   15,   19,	/* ichi_Rfoot rotate z */
	   15,   34,	/* ichi_Lfoot rotate x */
	   15,   49,	/* ichi_Lfoot rotate y */
	   15,   64,	/* ichi_Lfoot rotate z */
	    1,    0,	/* ichi_Rhand rotate x */
	    1,    0,	/* ichi_Rhand rotate y */
	    1,    0,	/* ichi_Rhand rotate z */
	   15,   79,	/* ichi_Rarm rotate x */
	   15,   94,	/* ichi_Rarm rotate y */
	   15,  109,	/* ichi_Rarm rotate z */
	    1,    0,	/* ichi_Lhand rotate x */
	    1,    0,	/* ichi_Lhand rotate y */
	    1,    0,	/* ichi_Lhand rotate z */
	   15,  124,	/* ichi_Larm rotate x */
	   15,  139,	/* ichi_Larm rotate y */
	   15,  154,	/* ichi_Larm rotate z */
	    1,  169,	/* chn26 rotate x */
	    1,  170,	/* chn26 rotate y */
	    1,  171,	/* chn26 rotate z */
	    1,  172,	/* chn28 rotate x */
	    1,  173,	/* chn28 rotate y */
	    1,  174,	/* chn28 rotate z */
	    1,  175,	/* chn19 rotate x */
	    1,  176,	/* chn19 rotate y */
	    1,  177,	/* chn19 rotate z */
	    1,  178,	/* chn21 rotate x */
	    1,  179,	/* chn21 rotate y */
	    1,  180,	/* chn21 rotate z */
	    1,    0,	/* ichi_body rotate x */
	   15,  181,	/* ichi_body rotate y */
	   15,  196,	/* ichi_body rotate z */
};
AnimeRecord ichiro_falldown_anm={
	15,
	12,
	ichiro_falldown_prm,
	ichiro_falldown_tbl
};
static SkeletonRecord ichi_Rfoot[1]=
	{RCP_ichiro_falldown0,0.000000,0.000000,0.000000,0,0,-15887,NULL,NULL};
static SkeletonRecord ichi_Lfoot[1]=
	{RCP_ichiro_falldown1,0.000000,0.000000,0.000000,0,0,-15887,NULL,NULL};
static SkeletonRecord ichi_Rhand[1]=
	{RCP_ichiro_falldown2,400.719971,0.000000,0.000000,0,0,0,NULL,NULL};
static SkeletonRecord ichi_Rarm[1]=
	{RCP_ichiro_falldown3,0.000000,0.000000,0.000000,12,26071,-16386,NULL,ichi_Rhand};
static SkeletonRecord ichi_Lhand[1]=
	{RCP_ichiro_falldown4,400.719971,0.000000,0.000000,0,0,0,NULL,NULL};
static SkeletonRecord ichi_Larm[1]=
	{RCP_ichiro_falldown5,0.000000,0.000000,0.000000,33,-26176,-16382,NULL,ichi_Lhand};
static SkeletonRecord chn26[1]=
	{NULL,1068.280029,-574.599976,0.000000,8226,16383,-8226,NULL,ichi_Larm};
static SkeletonRecord chn28[1]=
	{NULL,1075.920044,570.679993,0.000000,8226,16383,-8226,chn26,ichi_Rarm};
static SkeletonRecord chn19[1]=
	{NULL,-18.799999,-479.840027,-5.200000,8226,16383,-8226,chn28,ichi_Lfoot};
static SkeletonRecord chn21[1]=
	{NULL,-12.360000,490.440002,-5.200000,8226,16383,-8226,chn19,ichi_Rfoot};
static SkeletonRecord ichi_body[1]=
	{RCP_ichiro_falldown6,0.000000,0.000000,0.000000,0,0,16453,NULL,chn21};
static SkeletonRecord chn18[1]=
	{light_sourcex,5.200000,181.000000,0.000000,0,0,104,NULL,ichi_body};
SkeletonPtr ichiro_falldown_list[]={
	chn18,
	ichi_Rfoot,
	ichi_Lfoot,
	ichi_Rhand,
	ichi_Rarm,
	ichi_Lhand,
	ichi_Larm,
	chn26,
	chn28,
	chn19,
	chn21,
	ichi_body,
	NULL
};
