/**************************************************************************************************
					Message Data 	(5/12)															   
								programed by Iwamoto Daiki										   
**************************************************************************************************/

static unsigned char star_name_00[] = { 0x63, 0x5e, 0x58, 0x42, 0x43, 0x58, 0x9e, 0xf0, 0x8d, 0x90, 0x76, 0x9d, 0xf0, 0x77, 0xff,};
static unsigned char star_name_01[] = { 0x59, 0x47, 0x57, 0x51, 0x9e, 0x88, 0x79, 0x88, 0x79, 0x99, 0x9f, 0x7c, 0xff,};
static unsigned char star_name_02[] = { 0x4e, 0x66, 0x58, 0x4b, 0x5e, 0x5e, 0xf0, 0x52, 0x9e, 0xf0, 0x5b, 0xa1, 0x53, 0xf0, 0x5c, 0xff,};
static unsigned char star_name_03[] = { 0x08, 0x5e, 0x41, 0x58, 0x9e, 0x40, 0x45, 0x79, 0x71, 0x9d, 0xff,};
static unsigned char star_name_04[] = { 0x4e, 0x66, 0x55, 0x59, 0xf0, 0x59, 0x4f, 0x48, 0x9e, 0x59, 0x57, 0x8e, 0x97, 0x74, 0xff,};
static unsigned char star_name_05[] = { 0x9b, 0x9d, 0x9b, 0x9d, 0x58, 0x9e, 0x41, 0x56, 0xf0, 0x49, 0x63, 0xf0, 0x52, 0xff,};
static unsigned char star_name_06[] = { 0x41, 0x45, 0x67, 0x58, 0xf0, 0x89, 0xd1, 0x7f, 0x9d, 0x9e, 0x76, 0x9d, 0xf0, 0x77, 0xff,};
static unsigned char star_name_07[] = { 0x53, 0x67, 0xf0, 0x52, 0x58, 0x9e, 0x52, 0xa1, 0xf1, 0x5c, 0x6d, 0x5c, 0xff,};
static unsigned char star_name_08[] = { 0x4f, 0x41, 0x5d, 0x42, 0xf0, 0x52, 0x9e, 0x5a, 0x53, 0xa1, 0x53, 0xf0, 0x5a, 0xff,};
static unsigned char star_name_09[] = { 0x42, 0x46, 0x4b, 0x5e, 0x58, 0x9e, 0x08, 0x5e, 0x41, 0x79, 0x71, 0x9d, 0xff,};
static unsigned char star_name_10[] = { 0x53, 0x67, 0x45, 0xf0, 0x49, 0x5c, 0x9e, 0x7c, 0x83, 0x9f, 0x9d, 0xff,};
static unsigned char star_name_11[] = { 0x4f, 0x41, 0x5d, 0x42, 0xf0, 0x52, 0x9e, 0xf0, 0x5b, 0xa1, 0x49, 0x6b, 0x4d, 0xf2, 0xff,};
static unsigned char star_name_12[] = { 0x50, 0x6d, 0xf0, 0x5d, 0x51, 0x4d, 0x6d, 0x58, 0x9e, 0x44, 0x4f, 0x45, 0x66, 0xff,};
static unsigned char star_name_13[] = { 0xf0, 0x52, 0x52, 0x49, 0x41, 0x9e, 0x46, 0xa4, 0xf0, 0x4f, 0x41, 0x72, 0x81, 0xf0, 0x8d, 0xff,};
static unsigned char star_name_14[] = { 0x45, 0x41, 0x52, 0x41, 0xf0, 0x53, 0x42, 0x47, 0x51, 0x58, 0x9e, 0x44, 0x4f, 0x45, 0x66, 0xff,};
static unsigned char star_name_15[] = { 0x42, 0x45, 0x6d, 0xf0, 0x4f, 0x8b, 0x87, 0x58, 0x9e, 0x40, 0x45, 0x79, 0x71, 0x9d, 0xff,};
static unsigned char star_name_16[] = { 0x41, 0x6b, 0x58, 0x59, 0x4b, 0x66, 0x5c, 0x9e, 0x5a, 0x53, 0xa1, 0x53, 0xf0, 0x5a, 0xff,};
static unsigned char star_name_17[] = { 0x5b, 0x46, 0xf0, 0x4f, 0x4c, 0x9e, 0x5f, 0xf0, 0x4c, 0x6c, 0x9e, 0x47, 0xf0, 0x47, 0x69, 0xff,};
static unsigned char star_name_18[] = { 0x7c, 0x9f, 0xf1, 0x89, 0x9f, 0x7c, 0x88, 0x9f, 0x7c, 0x96, 0x71, 0xf0, 0x7f, 0x9f, 0xff,};
static unsigned char star_name_19[] = { 0x5e, 0x41, 0xf0, 0x49, 0x58, 0x9e, 0x49, 0xf1, 0x8c, 0x9d, 0xf0, 0x76, 0x9d, 0xff,};
static unsigned char star_name_20[] = { 0xf1, 0x8c, 0x9d, 0xf0, 0x76, 0x9d, 0x9e, 0x80, 0xd2, 0x9d, 0xf1, 0x8a, 0x74, 0x9d, 0x99, 0x9f, 0x7c, 0xff,};
static unsigned char star_name_21[] = { 0x4c, 0xf0, 0x5c, 0xa1, 0x52, 0x9e, 0x08, 0x5e, 0x41, 0x40, 0x45, 0x79, 0x71, 0x9d, 0xff,};
static unsigned char star_name_22[] = { 0xf0, 0x79, 0x9a, 0xf0, 0x79, 0x9a, 0x9e, 0x64, 0x46, 0xf0, 0x7f, 0x98, 0x8e, 0xff,};
static unsigned char star_name_23[] = { 0x45, 0x47, 0x69, 0x9e, 0x7c, 0x9f, 0xf1, 0x89, 0x9f, 0x9e, 0x75, 0xf0, 0x8c, 0x76, 0xd1, 0x77, 0xff,};
static unsigned char star_name_24[] = { 0x44, 0x63, 0x45, 0x4f, 0x82, 0x99, 0x7a, 0x6c, 0x9e, 0x4a, 0xf0, 0x45, 0x4d, 0xff,};
static unsigned char star_name_25[] = { 0x82, 0x99, 0x7a, 0x58, 0x9e, 0x91, 0x97, 0x9f, 0xf0, 0x79, 0x9f, 0x96, 0x9d, 0xf0, 0x83, 0xff,};
static unsigned char star_name_26[] = { 0x5d, 0x6d, 0xf0, 0x4f, 0x54, 0x74, 0xf0, 0x89, 0x78, 0x58, 0x9e, 0x84, 0xf0, 0x7e, 0xff,};
static unsigned char star_name_27[] = { 0x08, 0x5e, 0x41, 0x79, 0x71, 0x9d, 0x59, 0x9e, 0xf0, 0x53, 0x49, 0xf0, 0x4f, 0xff,};
static unsigned char star_name_28[] = { 0xf0, 0x89, 0x98, 0x79, 0x85, 0x9f, 0x58, 0x9e, 0xf0, 0x8d, 0x7c, 0x82, 0x99, 0x7a, 0xff,};
static unsigned char star_name_29[] = { 0x45, 0x47, 0x4b, 0xf0, 0x5c, 0x63, 0x58, 0x9e, 0x44, 0x44, 0x61, 0xf0, 0x4f, 0x5e, 0xff,};
static unsigned char star_name_30[] = { 0xf0, 0x83, 0xd1, 0x7b, 0x9f, 0x58, 0x41, 0x68, 0x9e, 0x50, 0x52, 0x41, 0x49, 0xff,};
static unsigned char star_name_31[] = { 0x4e, 0x42, 0x4a, 0x97, 0x8b, 0x83, 0x58, 0x9e, 0x40, 0x45, 0x79, 0x71, 0x9d, 0xff,};
static unsigned char star_name_32[] = { 0x91, 0x7f, 0x98, 0xf0, 0x52, 0x9e, 0xf0, 0x7f, 0xd1, 0x7b, 0xd3, 0xff,};
static unsigned char star_name_33[] = { 0x78, 0x90, 0x97, 0x61, 0x41, 0x6a, 0x6c, 0x9e, 0x56, 0x48, 0x52, 0xff,};
static unsigned char star_name_34[] = { 0x78, 0x90, 0x97, 0x61, 0x41, 0x6a, 0x58, 0x9e, 0x5a, 0xf0, 0x4b, 0xa4, 0x42, 0xf0, 0x47, 0x50, 0xff,};
static unsigned char star_name_35[] = { 0xf0, 0x79, 0x9a, 0xf0, 0x79, 0x9a, 0x41, 0x6b, 0x58, 0x9e, 0x5a, 0x5f, 0x51, 0xff,};
static unsigned char star_name_36[] = { 0x44, 0x53, 0x4d, 0x9e, 0xf0, 0x8d, 0x7c, 0xf0, 0x53, 0x6d, 0x48, 0x51, 0xff,};
static unsigned char star_name_37[] = { 0x4f, 0x4f, 0x45, 0x43, 0xf2, 0xf0, 0x53, 0x6d, 0x48, 0x51, 0x4f, 0x41, 0xff,};
static unsigned char star_name_38[] = { 0x01, 0x05, 0xf1, 0x89, 0xf0, 0x7c, 0x98, 0x58, 0x9e, 0x08, 0x5e, 0x41, 0x79, 0x71, 0x9d, 0xff,};
static unsigned char star_name_39[] = { 0x79, 0x9a, 0x79, 0x9a, 0x9e, 0x5e, 0x68, 0x4f, 0x6b, 0x4f, 0x67, 0xff,};
static unsigned char star_name_40[] = { 0x45, 0xf0, 0x4a, 0x6d, 0x58, 0x9e, 0xf1, 0x89, 0x9b, 0x9f, 0x7c, 0x7f, 0x9f, 0xff,};
static unsigned char star_name_41[] = { 0x45, 0xf0, 0x4a, 0x6d, 0x58, 0x9e, 0x97, 0x8b, 0x83, 0x81, 0x70, 0x9f, 0xff,};
static unsigned char star_name_42[] = { 0x41, 0x4f, 0xf0, 0x4c, 0x66, 0x89, 0xf0, 0x78, 0x4f, 0x45, 0x9e, 0xf0, 0x7b, 0xd2, 0x9d, 0xf0, 0x79, 0xff,};
static unsigned char star_name_43[] = { 0xf1, 0x8a, 0x96, 0x8f, 0xd1, 0xf0, 0x83, 0x58, 0x9e, 0x52, 0xa1, 0xf1, 0x5c, 0x6d, 0xf0, 0x52, 0xff,};
static unsigned char star_name_44[] = { 0x46, 0xa4, 0xf0, 0x4f, 0x41, 0xf1, 0x8a, 0x96, 0x8f, 0xd1, 0xf0, 0x83, 0x58, 0x9e, 0x54, 0x41, 0xf0, 0x5b, 0xff,};
static unsigned char star_name_45[] = { 0x04, 0x51, 0x58, 0x59, 0x4b, 0x66, 0x55, 0x9e, 0x4f, 0x51, 0x62, 0x58, 0x5c, 0xff,};
static unsigned char star_name_46[] = { 0x53, 0xf0, 0x5a, 0x5e, 0x6b, 0x69, 0x9e, 0x08, 0x5e, 0x41, 0x79, 0x71, 0x9d, 0xff,};
static unsigned char star_name_47[] = { 0x46, 0xa4, 0xf0, 0x4f, 0x41, 0xf1, 0x8a, 0x96, 0x8f, 0xd1, 0xf0, 0x83, 0x58, 0x84, 0xf0, 0x7e, 0xff,};
static unsigned char star_name_48[] = { 0x77, 0xd1, 0xf1, 0x89, 0x58, 0x9e, 0x4d, 0x6d, 0x4c, 0x41, 0x45, 0x6d, 0xff,};
static unsigned char star_name_49[] = { 0x42, 0xf0, 0x4c, 0x4b, 0x44, 0x58, 0x9e, 0x4f, 0x45, 0x66, 0xf0, 0x59, 0x49, 0xff,};
static unsigned char star_name_50[] = { 0x55, 0xf0, 0x48, 0x4f, 0x77, 0xd1, 0xf1, 0x89, 0x58, 0x9e, 0x40, 0x45, 0x79, 0x71, 0x9d, 0xff,};
static unsigned char star_name_51[] = { 0x5b, 0x46, 0xf0, 0x4f, 0x4c, 0x9e, 0x5f, 0xf0, 0x4c, 0x6c, 0x9e, 0x47, 0xf0, 0x47, 0x69, 0xff,};
static unsigned char star_name_52[] = { 0x8e, 0x9d, 0x7f, 0x58, 0x9e, 0x44, 0x47, 0x67, 0x62, 0x58, 0xff,};
static unsigned char star_name_53[] = { 0xf0, 0x8d, 0x72, 0x7b, 0xf0, 0x45, 0x9e, 0x4e, 0x6a, 0xa1, 0x4f, 0x66, 0xff,};
static unsigned char star_name_54[] = { 0x44, 0x44, 0x64, 0x46, 0xf0, 0x7f, 0x98, 0x8e, 0x58, 0x9e, 0x44, 0xf0, 0x52, 0x49, 0xff,};
static unsigned char star_name_55[] = { 0x49, 0x44, 0x67, 0x58, 0x47, 0x55, 0x58, 0x9e, 0xf0, 0x53, 0x6d, 0x48, 0x51, 0xff,};
static unsigned char star_name_56[] = { 0x49, 0x44, 0x67, 0x58, 0x9e, 0x74, 0xf0, 0x8b, 0xf0, 0x7b, 0xd0, 0xff,};
static unsigned char star_name_57[] = { 0x51, 0x61, 0x4f, 0x41, 0x9e, 0x41, 0x48, 0x6c, 0x9e, 0x49, 0x43, 0x52, 0xff,};
static unsigned char star_name_58[] = { 0x79, 0x72, 0x96, 0x55, 0x58, 0xa1, 0x52, 0x9e, 0x40, 0x45, 0x79, 0x71, 0x9d, 0xff,};
static unsigned char star_name_59[] = { 0x8b, 0x9b, 0x8b, 0x9b, 0x4a, 0x6d, 0x58, 0x9e, 0x44, 0x42, 0x50, 0xff,};
static unsigned char star_name_60[] = { 0xf0, 0x8a, 0x97, 0xf0, 0x8a, 0x97, 0x58, 0x9e, 0x5e, 0x61, 0x97, 0x8b, 0x83, 0xff,};
static unsigned char star_name_61[] = { 0x83, 0xd1, 0xf1, 0x8b, 0x74, 0xf0, 0x8b, 0x9e, 0xf0, 0x7a, 0x9e, 0x7b, 0x82, 0xd6, 0x9f, 0xff,};
static unsigned char star_name_62[] = { 0x40, 0x4a, 0x4d, 0x53, 0x9e, 0x4e, 0x66, 0x58, 0x7b, 0x9f, 0x77, 0x99, 0xd1, 0x83, 0xff,};
static unsigned char star_name_63[] = { 0x41, 0x4e, 0xf0, 0x48, 0xf2, 0x45, 0x54, 0x40, 0x5f, 0x73, 0x99, 0xf0, 0x8c, 0x9f, 0x7f, 0x9f, 0xff,};
static unsigned char star_name_64[] = { 0xf0, 0x7f, 0x72, 0x9d, 0x7f, 0x72, 0x9d, 0x58, 0x9e, 0x40, 0x45, 0x79, 0x71, 0x9d, 0xff,};
static unsigned char star_name_65[] = { 0xf0, 0x7f, 0x72, 0x9d, 0x7f, 0x72, 0x9d, 0x6c, 0x9e, 0x45, 0x48, 0x6a, 0xff,};
static unsigned char star_name_66[] = { 0x4f, 0x45, 0x41, 0x4f, 0x45, 0x41, 0x9e, 0x63, 0x5e, 0x58, 0x42, 0x43, 0xff,};
static unsigned char star_name_67[] = { 0x41, 0x4f, 0xf0, 0x4c, 0x66, 0xf0, 0x7a, 0x98, 0x9e, 0x72, 0xd1, 0x76, 0xd6, 0x58, 0x74, 0x97, 0xff,};
static unsigned char star_name_68[] = { 0x44, 0xf0, 0x59, 0x48, 0x76, 0x88, 0x79, 0x58, 0x9e, 0x40, 0x45, 0x79, 0x71, 0x9d, 0xff,};
static unsigned char star_name_69[] = { 0x8e, 0x72, 0x9d, 0x82, 0x9d, 0x9e, 0x7c, 0x96, 0x71, 0xf0, 0x7f, 0x9f, 0xff,};
static unsigned char star_name_70[] = { 0x59, 0x4b, 0x58, 0x42, 0x43, 0x45, 0x66, 0x9e, 0x5f, 0x6b, 0x4f, 0x4d, 0xf0, 0x59, 0xff,};
static unsigned char star_name_71[] = { 0xf0, 0x5b, 0xa1, 0x53, 0xf0, 0x5c, 0x9e, 0x59, 0x54, 0x69, 0x76, 0x88, 0x79, 0x5c, 0xff,};
static unsigned char star_name_72[] = { 0x46, 0xa4, 0xf0, 0x4f, 0x41, 0xf1, 0x89, 0xd1, 0x77, 0x9d, 0x8b, 0x96, 0x9b, 0x9f, 0xff,};
static unsigned char star_name_73[] = { 0xf0, 0x82, 0x75, 0xf0, 0x4b, 0x5e, 0x58, 0x9e, 0x52, 0xa1, 0xf1, 0x5c, 0x6d, 0xf0, 0x52, 0xff,};
static unsigned char star_name_74[] = { 0x88, 0x79, 0x88, 0x79, 0x9e, 0x97, 0x7f, 0x9f, 0x9d, 0x8e, 0xd1, 0x80, 0xff,};
static unsigned char star_name_75[] = { 0x80, 0xf0, 0x8a, 0xf0, 0x4b, 0x5e, 0x58, 0x9e, 0x05, 0x7b, 0x9f, 0x77, 0x99, 0xd1, 0x83, 0xff,};
static unsigned char star_name_76[] = { 0x89, 0x84, 0x80, 0xd2, 0x9d, 0x58, 0x9e, 0x40, 0x45, 0x79, 0x71, 0x9d, 0xff,};
static unsigned char star_name_77[] = { 0x71, 0x75, 0x97, 0x58, 0x9e, 0x89, 0x84, 0x80, 0xd2, 0x9d, 0xff,};
static unsigned char star_name_78[] = { 0xf0, 0x77, 0x98, 0xf0, 0x77, 0x98, 0x9e, 0x45, 0xf0, 0x49, 0x58, 0x54, 0x45, 0x5c, 0xff,};
static unsigned char star_name_79[] = { 0x5b, 0x67, 0x49, 0x58, 0x9e, 0x5c, 0x63, 0xff,};
static unsigned char star_name_80[] = { 0x80, 0xd1, 0x77, 0x7f, 0xd1, 0x77, 0x59, 0x67, 0x58, 0x9e, 0x42, 0x43, 0xff,};
static unsigned char star_name_81[] = { 0x43, 0xa1, 0x5c, 0x6d, 0x9e, 0x52, 0xa1, 0xf1, 0x5c, 0x6d, 0x9e, 0xf0, 0x83, 0xd1, 0x7c, 0x9d, 0xff,};
static unsigned char star_name_82[] = { 0x51, 0x46, 0xf0, 0x4f, 0x4b, 0x6c, 0x9e, 0x58, 0x67, 0x49, 0x43, 0x52, 0xff,};
static unsigned char star_name_83[] = { 0x00, 0x00, 0x5b, 0x6d, 0x58, 0x9e, 0x40, 0x45, 0x79, 0x71, 0x9d, 0xff,};
static unsigned char star_name_84[] = { 0x55, 0xf0, 0x4b, 0x6c, 0x9e, 0x6b, 0x4f, 0x68, 0x9e, 0x5b, 0x57, 0xff,};
static unsigned char star_name_85[] = { 0x52, 0x6d, 0x47, 0x42, 0x58, 0x9e, 0x44, 0x63, 0x4b, 0x46, 0xff,};
static unsigned char star_name_86[] = { 0x7f, 0x82, 0x61, 0x41, 0x6a, 0x58, 0x9e, 0x40, 0x45, 0x79, 0x71, 0x9d, 0xff,};
static unsigned char star_name_87[] = { 0x75, 0xf0, 0x7d, 0x46, 0x68, 0x9e, 0x46, 0xa4, 0xf0, 0x4f, 0x41, 0xf0, 0x8b, 0x96, 0x9d, 0x79, 0xff,};
static unsigned char star_name_88[] = { 0x44, 0x44, 0xf0, 0x4e, 0x66, 0x9e, 0x70, 0x7c, 0x99, 0x80, 0xd1, 0x77, 0xff,};
static unsigned char star_name_89[] = { 0x55, 0xf0, 0x4b, 0x58, 0x9e, 0x45, 0x54, 0x4f, 0x58, 0x9e, 0x4b, 0x5e, 0xff,};
static unsigned char star_name_90[] = { 0x44, 0x4b, 0x6a, 0x58, 0x45, 0x47, 0x69, 0x7c, 0x7f, 0x9f, 0xff,};
static unsigned char star_name_91[] = { 0x01, 0x00, 0x00, 0x5e, 0x41, 0x79, 0x71, 0x9d, 0x58, 0x9e, 0x7c, 0x7f, 0x9f, 0xff,};
static unsigned char star_name_92[] = { 0x8e, 0x72, 0x9d, 0x82, 0x9d, 0x58, 0x5c, 0x63, 0xf0, 0x45, 0x9e, 0x5a, 0x66, 0x47, 0xf2, 0xff,};
static unsigned char star_name_93[] = { 0x5f, 0xf0, 0x4c, 0x53, 0x64, 0x46, 0x02, 0x51, 0x58, 0x5c, 0x63, 0xf0, 0x45, 0x9e, 0x5a, 0x66, 0x47, 0xf2, 0xff,};
static unsigned char star_name_94[] = { 0x44, 0x44, 0x46, 0x54, 0x5d, 0x4b, 0x58, 0x9e, 0xf0, 0x83, 0x70, 0xf0, 0x45, 0x5a, 0x66, 0x47, 0xf2, 0xff,};
static unsigned char star_name_95[] = { 0x77, 0xd1, 0xf1, 0x89, 0x5c, 0x58, 0x9e, 0x53, 0xf0, 0x5a, 0x66, 0xf0, 0x45, 0x5a, 0x66, 0x47, 0xf2, 0xff,};
static unsigned char star_name_96[] = { 0x03, 0xf0, 0x45, 0x41, 0x5c, 0x58, 0x9e, 0x53, 0xf0, 0x5a, 0x66, 0xf0, 0x45, 0x5a, 0x66, 0x47, 0xf2, 0xff,};

unsigned char *starnamePtr[] = {star_name_00,star_name_01,star_name_02,star_name_03,star_name_04,star_name_05,star_name_06,star_name_07,star_name_08,star_name_09,star_name_10,star_name_11,star_name_12,star_name_13,star_name_14,star_name_15,
								star_name_16,star_name_17,star_name_18,star_name_19,star_name_20,star_name_21,star_name_22,star_name_23,star_name_24,star_name_25,star_name_26,star_name_27,star_name_28,star_name_29,star_name_30,star_name_31,
								star_name_32,star_name_33,star_name_34,star_name_35,star_name_36,star_name_37,star_name_38,star_name_39,star_name_40,star_name_41,star_name_42,star_name_43,star_name_44,star_name_45,star_name_46,star_name_47,
								star_name_48,star_name_49,star_name_50,star_name_51,star_name_52,star_name_53,star_name_54,star_name_55,star_name_56,star_name_57,star_name_58,star_name_59,star_name_60,star_name_61,star_name_62,star_name_63,
								star_name_64,star_name_65,star_name_66,star_name_67,star_name_68,star_name_69,star_name_70,star_name_71,star_name_72,star_name_73,star_name_74,star_name_75,star_name_76,star_name_77,star_name_78,star_name_79,
								star_name_80,star_name_81,star_name_82,star_name_83,star_name_84,star_name_85,star_name_86,star_name_87,star_name_88,star_name_89,star_name_90,star_name_91,star_name_92,star_name_93,star_name_94,star_name_95,
								star_name_96,NULL};

