/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 21 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage21


/* ================================================================================
		: Move BGs sequence.
===================================================================================	*/
SEQ_Stage21_MoveBGs:

		seqActor(S_movebg01,   -2370,-4525,    0,   0,  0,0,   0, 16,0,   e_derublock	 )
		seqActor(S_movebg02,   -2611, 3544, -904,   0,  0,0,   0,207,0,   e_derublock	 )
		seqActor(S_movebg02,   -4700, 3544, -904,   0,180,0,   0,143,0,   e_derublock	 )
		seqActor(S_movebg04,    4139,-1740,-1831,   0,  0,0,   2,  0,0,   e_rotstand	 )	
		seqActor(S_movebg04,   -6459, 1732, -904,   0,  0,0,   0,  0,0,   e_rotstand	 )
		seqActor(S_movebg04,   -4770, 1732, -904,   0,  0,0,   2,  0,0,   e_rotstand	 )
		seqActor(S_movebg08,   -1748,-1330,-1094,   0,  0,0,   0,  0,0,   e_pedallift	 )
		seqActor(S_movebg08,    2275, 5628,-1315,   0,  0,0,   0,  0,0,   e_pedallift	 )
		seqActor(S_movebg08,    3114, 4701,-1320,   0,  0,0,   0,  0,0,   e_pedallift	 )
		seqActor(S_movebg09,    2793, 2325, -904,   0,  0,0,   0, 97,0,   e_ridestartlift)
		seqActor(S_movebg10,      27,-1555, -713,   0, 90,0,   0,  1,0,   e_seesaw_s17	 )
		seqActor(S_movebg11,    -306,-4300,    0,   0,  0,0,   0,  2,0,   e_seesaw		 )
		seqActor(S_movebg12,    1769, -234, -899,   0,  0,0,   0,  0,0,   e_stepslope	 )

		seqActor(S_hanbutton,   -279,-234, -900,   0,0,0,   0,0,0,   e_onoff_switch)

		seqActor(S_NULL,  -6460,2039,-905,  0,0,0,  0,207,0,  e_bar)	
		seqActor(S_NULL,  -3326,3227,-905,  0,0,0,  0, 77,0,  e_bar)	

		seqActor(S_NULL,  5518,3184,-4019,   0,0,0,   0,0,0,   e_firebigbar)
		seqActor(S_NULL,  6465,3731,-1915,   0,0,0,   0,0,0,   e_firebigbar)
		seqActor(S_NULL,  5915,3718,-4019,   0,0,0,   0,0,0,   e_firebigbar)

		seqReturn


/* ================================================================================
		: Stars sequence.
===================================================================================	*/
SEQ_Stage21_Stars:

		seqActor(S_NULL,   350, 6800, -6800,   0,0,0,   0,0,0,   e_extstar_getcoins)

		seqReturn


/* ================================================================================
		: Stage 21 main sequence.
===================================================================================	*/
SEQ_DoStage21:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage21SegmentRomStart	  , _GfxStage21SegmentRomEnd	)
	seqLoadPres(SEGMENT_BACK	  , _BackKuppa3SegmentRomStart    , _BackKuppa3SegmentRomEnd 	)
	seqLoadText(SEGMENT_TEXTURE	  , _EMtTextureSegmentRomStart	  , _EMtTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_eSegmentRomStart	  , _GfxEnemy2_eSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_eSegmentRomStart	  , _HmsEnemy2_eSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetEnemy2_e)
		seqCall(SetBasicEnemy)

		seqHmsShape(S_bg01, RCP_HmsExt3Bg01)
		seqHmsShape(S_bg02, RCP_HmsExt3Bg02)
		seqHmsShape(S_bg03, RCP_HmsExt3Bg03)
		seqHmsShape(S_bg04, RCP_HmsExt3Bg04)
		seqHmsShape(S_bg05, RCP_HmsExt3Bg05)
		seqHmsShape(S_bg06, RCP_HmsExt3Bg06)
		seqHmsShape(S_bg07, RCP_HmsExt3Bg07)
		seqHmsShape(S_bg08, RCP_HmsExt3Bg08)
		seqHmsShape(S_bg09, RCP_HmsExt3Bg09)
		seqHmsShape(S_bg10, RCP_HmsExt3Bg10)
		seqHmsShape(S_bg11, RCP_HmsExt3Bg11)
		seqHmsShape(S_bg12, RCP_HmsExt3Bg12)
		seqHmsShape(S_bg13, RCP_HmsExt3Bg13)
		seqHmsShape(S_bg14, RCP_HmsExt3Bg14)
		seqHmsShape(S_bg15, RCP_HmsExt3Bg15)
		seqHmsShape(S_bg16, RCP_HmsExt3Bg16)
		seqHmsShape(S_bg17, RCP_HmsExt3Bg17)
		seqHmsShape(S_bg18, RCP_HmsExt3Bg18)

		seqHmsShape(S_movebg01, RCP_HmsExt3Deru01	 )
		seqHmsShape(S_movebg02, RCP_HmsExt3Deru02	 )
		seqHmsShape(S_movebg04, RCP_HmsExt3Kaitendai )
		seqHmsShape(S_movebg07, RCP_HmsExt3Kuruma	 )
		seqHmsShape(S_movebg08, RCP_HmsExt3KurumaJiku)
		seqHmsShape(S_movebg09, RCP_HmsExt3Rifuto	 )
		seqHmsShape(S_movebg10, RCP_HmsExt3Shiso	 )
		seqHmsShape(S_movebg11, RCP_HmsExt3YokoShiso )
		seqHmsShape(S_movebg12, RCP_HmsExt3Dan0		 )
		seqHmsShape(S_movebg13, RCP_HmsExt3Dan1		 )
		seqHmsShape(S_movebg14, RCP_HmsExt3Dan2		 )
		seqHmsShape(S_movebg15, RCP_HmsExt3Dan3		 )
		seqHmsShape(S_movebg16, RCP_HmsExt3Dan4		 )

		seqHmsShape(S_movebg20, RCP_HmsDokan)

		seqBeginScene(1, RCP_Stage21Scene1)
			seqActor(S_NULL,  	 -7039,-4812+START_H,    4,  0, 90,0,  0,10,0,	e_player_landing)
			seqActor(S_movebg20,   351, 		6652,-6030,  0,  0,0,  0,11,0,	e_dokan			)
			seqActor(S_NULL,       351, 		6800,-3900,  0,180,0,  0,12,0,  e_player_downing)
			seqPort(10, 21, 1, 10)		/*	Mario stage in.				*/
			seqPort(11, 34, 1, 10)		/*	To St34 [ Kuppa2 ].			*/
			seqPort(12, 21, 1, 12)		/*	St33 [ Kuppa2 ]  missed.	*/
			seqGameOver(6, 2, 107)		/*	Ext3 Missed.				*/
			seqCall(SEQ_Stage21_MoveBGs)
			seqCall(SEQ_Stage21_Stars)
			seqMapInfo(cx2101_info)
			seqTagInfo(cx2101_info_tag)
			seqSetMusic(NA_STG_GROUND, NA_EXTRA_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 90, -7039,-4812,4)				/* default start */
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
