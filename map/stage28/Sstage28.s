/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 28 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							  Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage28


/* ================================================================================
		: BG parts sequence.
===================================================================================	*/
SEQ_Stage28_BGParts:

		seqActor(S_bikkuri_button,	0,363,-6144,  0,0,0,  0,1,0,  e_bikkuri_switch)

		seqActor(S_NULL,  0, 500,-7373,  0,0,0,  0,0,0,  e_falls)
		seqActor(S_NULL,  0, 500, 3584,  0,0,0,  0,0,0,  e_falls)

		seqReturn


/* ================================================================================
		: Stars sequence.
===================================================================================	*/
SEQ_Stage28_Stars:

		seqActor(S_NULL, 0,-200, -7000,  0,0,0,  0,0,0,  e_tripstar_getcoins) /* 8 coins  */

		seqReturn


/* ================================================================================
		: Stage 28 main sequence.
===================================================================================	*/
SEQ_DoStage28:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage28SegmentRomStart	  , _GfxStage28SegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE   , _GCaveTextureSegmentRomStart  , _GCaveTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_hSegmentRomStart	  , _GfxEnemy1_hSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_hSegmentRomStart	  , _HmsEnemy1_hSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_hSegmentRomStart	  , _GfxEnemy2_hSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_hSegmentRomStart	  , _HmsEnemy2_hSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetEnemy1_h)
		seqCall(SetEnemy2_h)
		seqCall(SetBasicEnemy)

		seqBeginScene(1, RCP_Stage28Scene1)
			seqActor(S_NULL,  -4185,20+START_H,-47,  0,90,0,   0,10,0,  e_player_landing)
			seqPort(10, 28, 1, 10)			/*	MARIO stage in.	*/
			seqGameClear(6, 3,  52)			/*	Get trip star.	*/
			seqGameOver (6, 3, 102)			/*	Down.			*/
			seqCourseOut(16, 1, 20)			/*	Exit.			*/
			seqCall(SEQ_Stage28_Stars)
			seqCall(SEQ_Stage28_BGParts)
			seqMapInfo(cx2801_info)
			seqTagInfo(cx2801_info_tag)
			seqMessage(SEQ_MESG_ENTRANT, 130)
			seqSetMusic(NA_STG_DUNGEON, NA_DUNGEON_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 90,  -4185,20,-47)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
