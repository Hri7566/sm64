/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 25 sequence module

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							May 12, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage25

/* ====================================================================================	
		: Stage25 [ Ending ] main sequence.
=======================================================================================	*/
SEQ_DoStage25:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1, _GfxStage25SegmentRomStart, _GfxStage25SegmentRomEnd)

	seqBeginConstruction()
		seqBeginScene(1, RCP_Stage25Scene1)
		seqEndScene()
	seqEndConstruction()


	seqWait(60)
	seqBlanking(0)
	seqOpenScene(1)
	seqWipe(WIPE_FADE_IN, 75, 0, 0, 0)
	seqWait(120)
	seqCProgram(GameTheEnd, 0)

repeat:
	seqWait(1)
	seqJump(repeat)
