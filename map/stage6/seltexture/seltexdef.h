/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage6 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
*********************************************************************************/

#define		y_bom_ue_txt		selr_wave00_1_txt
#define		y_bom_sita_txt		selr_wave00_0_txt

#define		y_yuki_ue_txt		selr_wave01_1_txt
#define		y_yuki_sita_txt		selr_wave01_0_txt

#define		y_yama_ue_txt		selr_wave02_1_txt
#define		y_yama_sita_txt		selr_wave02_0_txt

#define		y_mizu_ue_txt		selr_wave03_1_txt
#define		y_mizu_sita_txt		selr_wave03_0_txt

#define		l_kyoue_txt			selr_wave09_1_txt
#define		l_kyosita_txt		selr_wave09_0_txt

#define		y_kuri_ue_txt		selr_wave09_1_txt
#define		y_kuri_sita_txt		selr_wave09_0_txt

#define		kp_hu_txt			kuppa_000_TxT
#define		kp_mu_txt			kuppa_001_TxT
#define		kp_hs_txt			kuppa_002_TxT
#define		kp_ms_txt			kuppa_003_TxT

#define		ph_hu_txt			peach_000_TxT
#define		ph_mu_txt			peach_001_TxT
#define		ph_hs_txt			peach_002_TxT
#define		ph_ms_txt			peach_003_TxT
