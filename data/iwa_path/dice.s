/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Strategy

				Feburary 31 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
						Dice Path Data
 ********************************************************************************/
e_dice:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON |stf_YangleSAME | stf_catchON)
	p_setd(objname,OBJNAME_TAKE)
	p_BGcheckYset
	p_sethitbox(40,40)
	p_program(s_dice_init)

dice_main:
	p_while
		p_hitON
		p_program(s_diceMove)
	p_loop


/*################*/
#else
/*################*/


/***************************************************************************************************
		C Program
 ****************************************************************************************************/

/***************************************************************************************************
						Dice Event
***************************************************************************************************/
/***************************************************************************************************
						Dice Initialize
***************************************************************************************************/
extern void s_dice_init(void)
{

	execstp->s[stw_gravity].f   = 2.5;
	execstp->s[stw_friction].f  = 0.98;
	execstp->s[stw_specificG].f = 1.3;
	
}

/***************************************************************************************************
						Dice Rotate Event
***************************************************************************************************/
static void DiceRotateEvent(void)
{
	short angleY = (short)execstp->s[stw_angleY].d ;

	execstp->s[stw_animeangleX].d += (short)(execstp->s[stw_speedF].f*500);


}
/***************************************************************************************************
						Dice Move Event
***************************************************************************************************/
extern void DiceMoveEvent(void)
{
	short bg_flag = 0;
	float offsety;
		
	bg_flag = ObjMoveEvent();

	DiceRotateEvent();

	if ((bg_flag & 0x0001)!= 0x00) {
		offsety = sin(execstp->s[stw_animeangleX].d*2)*10;
		if (offsety < 0) offsety *= -1;
		execstp->s[stw_animepositionY].f = (offsety+25);


		if (execstp->s[stw_speedF].f < 5.0 && ( (int)offsety==10 || (int)offsety==0)) {
			execstp->s[stw_speedF].f = 0.0;
		}



	}

}

/*-------------------------------------------------------------------------------------------------*/
/***************************************************************************************************
						Dice Catch
***************************************************************************************************/
static void DiceCatch(void)
{

	MapHideShape(&execstp->map);


}
/***************************************************************************************************
						Dice Drop
***************************************************************************************************/
static void DiceDrop(void)
{

	s_drop_object();	

	execstp->s[stw_actionmode].d = CHILEDMODE_NO_CATCH;

}
/***************************************************************************************************
						Dice Throw
***************************************************************************************************/
static void DiceThrow(void)
{

	s_throw_object();	
	execstp->s[stw_actionmode].d = CHILEDMODE_NO_CATCH;
//	execstp->s[stw_flag].d &= (stf_YangleSAME^0xffffffff); 
	
	execstp->s[stw_speedF].f = 25.;
	execstp->s[stw_speedY].f = 20.;
	execstp->s[state_flag].d = 2;
}
/***************************************************************************************************
						Dice Event
***************************************************************************************************/
extern void s_diceMove(void)
{

	switch(execstp->s[stw_actionmode].d) {
		case CHILEDMODE_NO_CATCH:	DiceMoveEvent();		break;
		case CHILEDMODE_CATCH:		DiceCatch();			break;
		case CHILEDMODE_THROW:		DiceThrow();			break;
		case CHILEDMODE_DROP:		DiceDrop();				break;
	}


}





/*################*/
#endif
/*################*/


