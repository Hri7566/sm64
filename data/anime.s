/********************************************************************************
						Ultra 64 MARIO Brothers

				  MARIO skeleton animation data module

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							May 9, 1996
 ********************************************************************************/

#define	MARIO_HEIGHT			189

#define	MAP_ANIM_NORMAL			0x00
#define	MAP_ANIM_ONETIME		0x01
#define	MAP_ANIM_REVERSE		0x02
#define	MAP_ANIM_FREEZE			0x04
#define	MAP_ANIM_TRVERTI		0x08
#define	MAP_ANIM_TRPLANE		0x10
#define	MAP_ANIM_FIXSHADOW		0x20
#define	MAP_ANIM_NOTRANS		0x40


	.data


	.word	209						/* number of animations */
	.word	0						/* padding              */

	.word	animMarioAscend,			0x00000B40
	.word	animMarioBackDown,			0x00001ADC
	.word	animMarioJumpBackDown,			0x00001AC4
	.word	animMarioBackDownEnd,			0x00001228
	.word	animMarioBackJumping,			0x000009EC
	.word	animMarioBarClimb,			0x000017B8
	.word	animMarioBarHang,			0x00000490
	.word	animMarioBarJumping,			0x000006E8
	.word	animMarioBarJumpEnd,			0x000006D0
	.word	animMarioBarStanding,			0x00000D00
	.word	animMarioBarStandJump,			0x00000B04
	.word	animMarioBarStandStart,			0x00000670
	.word	animMarioBarStandEnd,			0x00000658
	.word	animMarioBarWaiting,			0x00000416
	.word	animMarioBaseData,			0x00000164
	.word	animMarioBraking,			0x0000070A
	.word	animMarioBrakeEnd,			0x000006F2
	.word	animMarioBroadJumpEnd1,			0x000005AA
	.word	animMarioBroadJumpEnd2,			0x0000063A
	.word	animMarioBroadJumping1,			0x000007A2
	.word	animMarioBroadJumping2,			0x00000562
	.word	animMarioCannonFly,			0x000004B6
	.word	animMarioCarryJogging,			0x00001278
	.word	animMarioCarryRunning,			0x00000C92
	.word	animMarioCarryWalking,			0x00001278
	.word	animMarioColdWaitStart,			0x000015FE
	.word	animMarioColdWaitEnd,			0x0000058E
	.word	animMarioColdWaiting,			0x00000C3E
	.word	animMarioDescend,			0x00000420
	.word	animMarioEndingByeBye,			0x00000678
	.word	animMarioEndingHatWait,			0x0000070E
	.word	animMarioEndingLookDown,			0x00000462
	.word	animMarioEndingLookUp1,			0x00001CE8
	.word	animMarioEndingLookUp2,			0x00000B46
	.word	animMarioEndingPutOff,			0x000007EA
	.word	animMarioEndingWalkA,			0x0000212C
	.word	animMarioEndingWalkB,			0x000025B4
	.word	animMarioEndingWinFlyA,			0x00002066
	.word	animMarioEndingWinFlyB,			0x00001DCC
	.word	animMarioEndingYaaha,			0x00002428
	.word	animMarioFireJumpEnd,			0x00000506
	.word	animMarioFireJumping,			0x0000035A
	.word	animMarioFlight,			0x000005F8
	.word	animMarioFlying,			0x00000742
	.word	animMarioForeDown,			0x00001720
	.word	animMarioJumpForeDown,			0x00001708
	.word	animMarioForeDownEnd,			0x00000CE8
	.word	animMarioGasDowning,			0x00001E10
	.word	animMarioGasWaiting,			0x00001648
	.word	animMarioGetDoorKey,			0x00002AB8
	.word	animMarioGiddyDown,			0x00001FE6
	.word	animMarioHanging,			0x00000C2C
	.word	animMarioHangJump,			0x00000628
	.word	animMarioHangRoof,			0x00000F8C
	.word	animMarioHatTaking,			0x00000F88
	.word	animMarioHatWaiting,			0x00001BC0
	.word	animMarioHatWaitEnd,			0x00000460
	.word	animMarioHeadDowning,			0x00003196
	.word	animMarioHipAttackEnd,			0x0000028A
	.word	animMarioHipAttackFly,			0x000005C8
	.word	animMarioHipAttackStart,			0x00000432
	.word	animMarioHipAttacking,			0x0000041A
	.word	animMarioHipDowning,			0x00002DF6
	.word	animMarioHolding,			0x00000D2A
	.word	animMarioHoldJumpEnd,			0x0000055A
	.word	animMarioHoldJumping,			0x0000032C
	.word	animMarioHoldLandEnd,			0x000004D0
	.word	animMarioHoldLanding,			0x00000168
	.word	animMarioHoldSlipLanding,			0x0000039E
	.word	animMarioHoldSlipping,			0x000005D2
	.word	animMarioHoldSlipEnd,			0x000005BA
	.word	animMarioBoardJumpend,			0x0000045A
	.word	animMarioJogging,			0x000014FE
	.word	animMarioJumpBack,			0x00000626
	.word	animMarioBoardJumping,			0x0000037C
	.word	animMarioJumpEnd2,			0x0000067E
	.word	animMarioJumpFall,			0x000003B2
	.word	animMarioJumping,			0x00000AC4
	.word	animMarioJumpEnd,			0x00000AAC
	.word	animMarioJumpKick,			0x0000073E
	.word	animMarioJumpStep2,			0x000002B8
	.word	animMarioJumpStep3,			0x000004FE
	.word	animMarioJumpThrow,			0x00000482
	.word	animMarioKickLanding,			0x000008C6
	.word	animMarioLandBoard,			0x00000636
	.word	animMarioLandDowning,			0x00002D18
	.word	animMarioLanding,			0x00000744
	.word	animMarioLandEnd,			0x0000072C
	.word	animMarioLifted,			0x0000054E
	.word	animMarioLifting,			0x00000D00
	.word	animMarioLostStandup,			0x00000DE2
	.word	animMarioMantFlying,			0x000003E4
	.word	animMarioMoveRoofL,			0x000005FE
	.word	animMarioMoveRoofR,			0x0000058A
	.word	animMarioNoHatWaiting,			0x00003062
	.word	animMarioOpenDoor1,			0x000014D6
	.word	animMarioOpenDoor2,			0x000010C8
	.word	animMarioOpenKeyDoor,			0x00003288
	.word	animMarioOshinWaiting1,			0x000003FA
	.word	animMarioOshinWaiting2,			0x0000076E
	.word	animMarioOshinWaiting3,			0x000005F2
	.word	animMarioPitching,			0x000007B8
	.word	animMarioPowerKick,			0x000008B0
	.word	animMarioPunchBase,			0x00000296
	.word	animMarioSecPunchBase,			0x00000268
	.word	animMarioPunchEnd,			0x00000410
	.word	animMarioSecPunchEnd,			0x00000538
	.word	animMarioPunchTake,			0x000004C0
	.word	animMarioPushing,			0x00000618
	.word	animMarioPutonBoard,			0x0000061A
	.word	animMarioPutting,			0x00000584
	.word	animMarioRolling,			0x0000018E
	.word	animMarioBackRolling,			0x00000176
	.word	animMarioRollingKick,			0x00000932
	.word	animMarioRunning,			0x00001222
	.word	animMarioTurnCont,			0x0000120A
	.word	animMarioSafeBackDown,			0x00000848
	.word	animMarioSafeForeDown,			0x00000878
	.word	animMarioSandDowning,			0x0000133C
	.word	animMarioSandWaiting,			0x00000AB4
	.word	animMarioSandWalking,			0x000029EA
	.word	animMarioShockDowning,			0x00000F46
	.word	animMarioShocking,			0x000002A6
	.word	animMarioShortBackDown,			0x00000A4C
	.word	animMarioShortForeDown,			0x00000A10
	.word	animMarioShoulder,			0x0000056E
	.word	animMarioSideWait,			0x000012CE
	.word	animMarioSideWalkL,			0x00000F3A
	.word	animMarioSideWalkR,			0x000011AC
	.word	animMarioSleeping1,			0x000011E8
	.word	animMarioSleeping2,			0x0000179E
	.word	animMarioSleeping3,			0x00001392
	.word	animMarioSleeping4,			0x000013A8
	.word	animMarioSleeping5,			0x00000258
	.word	animMarioSleeping6,			0x00000C6A
	.word	animMarioSleeping7,			0x00000730
	.word	animMarioSlideCatch,			0x00000618
	.word	animMarioSlipBack,			0x00000600
	.word	animMarioSlideDown,			0x00000CB8
	.word	animMarioSlideStandup,			0x00000CAE
	.word	animMarioSlidingKick,			0x0000039A
	.word	animMarioSlidingKickEnd,			0x00000552
	.word	animMarioSlip,			0x0000077E
	.word	animMarioSlipEnd,			0x00000766
	.word	animMarioSlipLanding,			0x00000508
	.word	animMarioSlipping,			0x0000021E
	.word	animMarioSoftStep,			0x000016F4
	.word	animMarioSpinJpEnd,			0x0000036E
	.word	animMarioSpinJumping,			0x00000164
	.word	animMarioSpinJpStart,			0x0000022C
	.word	animMarioSquatEnd,			0x000003BC
	.word	animMarioSquatStart,			0x000002B4
	.word	animMarioSquatWaiting,			0x00000BDA
	.word	animMarioSquatWalking,			0x00001C7A
	.word	animMarioSquatWalkEnd,			0x00000398
	.word	animMarioSquatWalkStart,			0x0000038A
	.word	animMarioStarDoorA,			0x00001428
	.word	animMarioStarDoorB,			0x000008AC
	.word	animMarioSwimBackDown,			0x000010BC
	.word	animMarioSwimCarryA1,			0x0000041A
	.word	animMarioSwimCarryA2,			0x0000029A
	.word	animMarioSwimCarryB,			0x000004AC
	.word	animMarioSwimCarryStopA,			0x000006BC
	.word	animMarioSwimCarryStopB,			0x00000B32
	.word	animMarioSwimCarryWait,			0x00000786
	.word	animMarioSwimDown1,			0x00001530
	.word	animMarioSwimDown2,			0x0000133A
	.word	animMarioSwimDownEnd,			0x00000D08
	.word	animMarioSwimForeDown,			0x0000105A
	.word	animMarioSwimLanding,			0x000007D4
	.word	animMarioSwimming1,			0x000004E0
	.word	animMarioSwimming2,			0x00000402
	.word	animMarioSwimming3,			0x000007BE
	.word	animMarioSwimStop,			0x00000858
	.word	animMarioSwimTakeEnd,			0x000004D0
	.word	animMarioSwimTakeMiss,			0x00000984
	.word	animMarioSwimTaking,			0x000002CC
	.word	animMarioSwimThrow,			0x00000A86
	.word	animMarioSwimWait,			0x00000834
	.word	animMarioSwimWinDemo,			0x00001A72
	.word	animMarioSwimWinEnd,			0x0000084A
	.word	animMarioSwingStart,			0x00000692
	.word	animMarioSwingAttack,			0x0000067A
	.word	animMarioSwingDown,			0x00000CB8
	.word	animMarioSwingWait,			0x0000041C
	.word	animMarioThrowing,			0x00000928
	.word	animMarioTiredWaiting,			0x000004EE
	.word	animMarioTransfer,			0x00001138
	.word	animMarioTurning,			0x00000724
	.word	animMarioTurnEnd,			0x0000070C
	.word	animMarioUJumpEnd,			0x0000040C
	.word	animMarioUJumping,			0x000008A4
	.word	animMarioUltraJumpEnd,			0x00000AA8
	.word	animMarioUltraJumping,			0x00000AB8
	.word	animMarioViewing,			0x00001642
	.word	animMarioWaiting1,			0x000008AA
	.word	animMarioWaiting2,			0x000008AA
	.word	animMarioWaiting3,			0x000008AA
	.word	animMarioWaitRoofL,			0x00000796
	.word	animMarioWaitRoofR,			0x00000C30
	.word	animMarioWakeup,			0x00000784
	.word	animMarioWakeup2,			0x000008A8
	.word	animMarioWalking,			0x000008E2
	.word	animMarioWallJump,			0x00000692
	.word	animMarioWallStay,			0x0000067A
	.word	animMarioWinDemoA,			0x00001554
	.word	animMarioWinDemoAEnd,			0x000006C8
	.word	animMarioWingJumpEnd,			0x00000BA8
	.word	animMarioWingJumping,			0x000003EE

#include "Anime/ascend.s"
#include "Anime/backdown.s"
#include "Anime/backdownend.s"
#include "Anime/backjump.s"
#include "Anime/barclimb.s"
#include "Anime/barhang.s"
#include "Anime/barjump.s"
#include "Anime/barstanding.s"
#include "Anime/barstandjump.s"
#include "Anime/barstandstart.s"
#include "Anime/barwaiting.s"
#include "Anime/basedata.s"
#include "Anime/braking.s"
#include "Anime/broadjumpend1.s"
#include "Anime/broadjumpend2.s"
#include "Anime/broadjumping1.s"
#include "Anime/broadjumping2.s"
#include "Anime/cannon.s"
#include "Anime/carryjog.s"
#include "Anime/carryrun.s"
#include "Anime/carrywalk.s"
#include "Anime/coldwaitbgn.s"
#include "Anime/coldwaitend.s"
#include "Anime/coldwaiting.s"
#include "Anime/descend.s"
#include "Anime/ending_byebye.s"
#include "Anime/ending_hatwait.s"
#include "Anime/ending_lookdown.s"
#include "Anime/ending_lookup1.s"
#include "Anime/ending_lookup2.s"
#include "Anime/ending_putoff.s"
#include "Anime/ending_walkA.s"
#include "Anime/ending_walkB.s"
#include "Anime/ending_winflyA.s"
#include "Anime/ending_winflyB.s"
#include "Anime/ending_yaaha.s"
#include "Anime/firejumpend.s"
#include "Anime/firejumping.s"
#include "Anime/flight.s"
#include "Anime/flying.s"
#include "Anime/foredown.s"
#include "Anime/foredownend.s"
#include "Anime/gasdown.s"
#include "Anime/gaswait.s"
#include "Anime/getdoorkey.s"
#include "Anime/giddydown.s"
#include "Anime/hanging.s"
#include "Anime/hangjump.s"
#include "Anime/hangroof.s"
#include "Anime/hattaking.s"
#include "Anime/hatwait.s"
#include "Anime/hatwaitend.s"
#include "Anime/headdown.s"
#include "Anime/hipattackend.s"
#include "Anime/hipattackfly.s"
#include "Anime/hipattacking.s"
#include "Anime/hipdown.s"
#include "Anime/holding.s"
#include "Anime/holdjumpend.s"
#include "Anime/holdjumping.s"
#include "Anime/holdlandend.s"
#include "Anime/holdlanding.s"
#include "Anime/holdslipland.s"
#include "Anime/holdslipping.s"
#include "Anime/jendboard.s"
#include "Anime/jogging.s"
#include "Anime/jumpback.s"
#include "Anime/jumpboard.s"
#include "Anime/jumpend2.s"
#include "Anime/jumpfall.s"
#include "Anime/jumping.s"
#include "Anime/jumpkick.s"
#include "Anime/jumpstep2.s"
#include "Anime/jumpstep3.s"
#include "Anime/jumpthrow.s"
#include "Anime/kickland.s"
#include "Anime/landboard.s"
#include "Anime/landdown.s"
#include "Anime/landing.s"
#include "Anime/lifted.s"
#include "Anime/lifting.s"
#include "Anime/loststandup.s"
#include "Anime/mantfly.s"
#include "Anime/moveroofL.s"
#include "Anime/moveroofR.s"
#include "Anime/nohatwait.s"
#include "Anime/opendoor1.s"
#include "Anime/opendoor2.s"
#include "Anime/openkeydoor.s"
#include "Anime/oshinwait1.s"
#include "Anime/oshinwait2.s"
#include "Anime/oshinwait3.s"
#include "Anime/pitching.s"
#include "Anime/powerkick.s"
#include "Anime/punchbase.s"
#include "Anime/punchbase2.s"
#include "Anime/punchend.s"
#include "Anime/punchend2.s"
#include "Anime/punchtake.s"
#include "Anime/pushing.s"
#include "Anime/putonboard.s"
#include "Anime/putting.s"
#include "Anime/rolling.s"
#include "Anime/rollingkick.s"
#include "Anime/running.s"
#include "Anime/safedownB.s"
#include "Anime/safedownF.s"
#include "Anime/sanddown.s"
#include "Anime/sandwait.s"
#include "Anime/sandwalk.s"
#include "Anime/shockdown.s"
#include "Anime/shocking.s"
#include "Anime/shortdownB.s"
#include "Anime/shortdownF.s"
#include "Anime/shoulder.s"
#include "Anime/sidewait.s"
#include "Anime/sidewalkL.s"
#include "Anime/sidewalkR.s"
#include "Anime/sleeping1.s"
#include "Anime/sleeping2.s"
#include "Anime/sleeping3.s"
#include "Anime/sleeping4.s"
#include "Anime/sleeping5.s"
#include "Anime/sleeping6.s"
#include "Anime/sleeping7.s"
#include "Anime/slidecatch.s"
#include "Anime/slidedown.s"
#include "Anime/slidestandup.s"
#include "Anime/slidingkick.s"
#include "Anime/slidingkickend.s"
#include "Anime/slipend.s"
#include "Anime/slipland.s"
#include "Anime/slipping.s"
#include "Anime/softstep.s"
#include "Anime/spinend.s"
#include "Anime/spinjump.s"
#include "Anime/spinstart.s"
#include "Anime/squatend.s"
#include "Anime/squatstart.s"
#include "Anime/squatwait.s"
#include "Anime/squatwalk.s"
#include "Anime/squatwalkend.s"
#include "Anime/squatwalkstart.s"
#include "Anime/stardoorA.s"
#include "Anime/stardoorB.s"
#include "Anime/swimbackdown.s"
#include "Anime/swimcarryA1.s"
#include "Anime/swimcarryA2.s"
#include "Anime/swimcarryB.s"
#include "Anime/swimcstopA.s"
#include "Anime/swimcstopB.s"
#include "Anime/swimcwait.s"
#include "Anime/swimdown1.s"
#include "Anime/swimdown2.s"
#include "Anime/swimdownend.s"
#include "Anime/swimforedown.s"
#include "Anime/swimlanding.s"
#include "Anime/swimming1.s"
#include "Anime/swimming2.s"
#include "Anime/swimming3.s"
#include "Anime/swimstop.s"
#include "Anime/swimtakeend.s"
#include "Anime/swimtakemiss.s"
#include "Anime/swimtaking.s"
#include "Anime/swimthrow.s"
#include "Anime/swimwait.s"
#include "Anime/swimwindemo.s"
#include "Anime/swimwinend.s"
#include "Anime/swingattack.s"
#include "Anime/swingdown.s"
#include "Anime/swingwait.s"
#include "Anime/throwing.s"
#include "Anime/tired.s"
#include "Anime/transfer.s"
#include "Anime/turning.s"
#include "Anime/ujumpend.s"
#include "Anime/ujumping.s"
#include "Anime/ultrajumpend.s"
#include "Anime/ultrajumping.s"
#include "Anime/viewing.s"
#include "Anime/waiting1.s"
#include "Anime/waiting2.s"
#include "Anime/waiting3.s"
#include "Anime/waitroofL.s"
#include "Anime/waitroofR.s"
#include "Anime/wakeup.s"
#include "Anime/wakeup2.s"
#include "Anime/walking.s"
#include "Anime/walljump.s"
#include "Anime/windemoA.s"
#include "Anime/windemoend.s"
#include "Anime/wingjumpend.s"
#include "Anime/wingjumping.s"
