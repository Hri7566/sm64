/********************************************************************************
						Ultra 64 MARIO Brothers

				  mario junmping step 2 animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							August 25, 1995
 ********************************************************************************/

#include "../headers.h"


static short mario_2step_jump1_prm[]={
	    0,  170,    0,    0,    0,    0,    0,    0,
	16383,    0,  -88, -301, -556, -775, -877,-1668,
	-1655,-1618,-1563,-1495,-1421,-4724,-4132,-2716,
	-1016,  426, 1071,-6533,-6311,-5797,-5216,-4795,
	-4759,-13787,-12547,-9579,-6012,-2976,-1598,-10534,
	-10019,-8758,-7175,-5696,-4746, -603,-1677,-4296,
	-7558,-10561,-12403,  523,  544,  593,  650,  694,
	  706,  937,  923,  889,  850,  818,  808,-2696,
	-2448,-1866,-1188, -653, -501,-9501,-8969,-7675,
	-6073,-4615,-3754,15341,14060,11025, 7449, 4542,
	 3517,10658,10058, 8622, 6891, 5408, 4717,-2740,
	-3769,-6205,-9074,-11401,-12211,  -59,  -66,  -84,
	 -106, -127, -138, -199, -195, -185, -173, -162,
	 -155,-10839,-10414,-9380,-8101,-6936,-6249,16828,
	15908,13659,10848, 8241, 6606, 1896, 1984, 2202,
	 2483, 2759, 2962, 2388, 2225, 1830, 1347,  918,
	  686,-23577,-22937,-21378,-19442,-17670,-16605,-13551,
	-13377,-12954,-12431,-11955,-11674,19644,18918,17188,
	15121,13387,12656,-1900,-1993,-2221,-2511,-2788,
	-2976,-2554,-2388,-1998,-1546,-1197,-1112,-26393,
	-25752,-24230,-22432,-20960,-20418,32767,32767,16753,
	-16545,32767,32767,16753,-16545,  124,   94,   24,
	  -58, -128, -157,  533,  532,  530,  527,  525,
	  524,  754,  190,-1153,-2759,-4106,-4676,  -79,
	  -64,  -27,   16,   54,   73, -153, -153, -154,
	 -154, -155, -156, 8475, 7520, 5226, 2449,   48,
	-1121,16545
};
static short mario_2step_jump1_tbl[]={
	    1,    0,	/* chn14translate x */
	    1,    1,	/* chn14 translate y */
	    6,    2,	/* chn14 translate z */
	    1,    0,	/* chn14 rotate x */
	    1,    8,	/* chn14 rotate y */
	    1,    0,	/* chn14 rotate z */
	    1,    0,	/* <m_waist>1_3 rotate x */
	    1,    0,	/* <m_waist>1_3 rotate y */
	    1,  209,	/* <m_waist>1_3 rotate z */
	    6,  191,	/* <m_body>1 rotate x */
	    6,  197,	/* <m_body>1 rotate y */
	    6,  203,	/* <m_body>1 rotate z */
	    6,  173,	/* <m_head>2 rotate x */
	    6,  179,	/* <m_head>2 rotate y */
	    6,  185,	/* <m_head>2 rotate z */

	    1,  169,	/* chn6 rotate x */
	    1,  170,	/* chn6 rotate y */
	    1,  171,	/* chn6 rotate z */
	    6,   33,	/* <m_larmA>1 rotate x */
	    6,   39,	/* <m_larmA>1 rotate y */
	    6,   45,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	    6,   27,	/* <m_larmB>1 rotate z */
	    6,    9,	/* <m_lhand>1 rotate x */
	    6,   15,	/* <m_lhand>1 rotate y */
	    6,   21,	/* <m_lhand>1 rotate z */

	    1,  165,	/* chn10 rotate x */
	    1,  166,	/* chn10 rotate y */
	    1,  167,	/* chn10 rotate z */
	    6,   75,	/* <m_rarmA>1 rotate x */
	    6,   81,	/* <m_rarmA>1 rotate y */
	    6,   87,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	    6,   69,	/* <m_rarmB>1 rotate z */
	    6,   51,	/* <m_rhand>1 rotate x */
	    6,   57,	/* <m_rhand>1 rotate y */
	    6,   63,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  168,	/* chn15 rotate z */
	    6,  147,	/* <m_llegA>1 rotate x */
	    6,  153,	/* <m_llegA>1 rotate y */
	    6,  159,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	    6,  141,	/* <m_llegB>1 rotate z */
	    1,    0,	/* <m_ltoot>1 rotate x */
	    1,    0,	/* <m_ltoot>1 rotate y */
	    6,  135,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  172,	/* chn17 rotate z */
	    6,  117,	/* <m_rlegA>1 rotate x */
	    6,  123,	/* <m_rlegA>1 rotate y */
	    6,  129,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	    6,  111,	/* <m_rlegB>1 rotate z */
	    6,   93,	/* <m_rfoot>1 rotate x */
	    6,   99,	/* <m_rfoot>1 rotate y */
	    6,  105,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioJumpStep2 = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	 6,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_2step_jump1_prm,
	mario_2step_jump1_tbl
};
