/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage28 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE28_FOG_R		0
#define		STAGE28_FOG_G		0
#define		STAGE28_FOG_B		0
#define		STAGE28_FOG_START	980

#include "ext5_in_fall/ext5_ext_texture.h"

#include "ext5_in_fall/taki_shape.sou"
#include "ext5_in_fall/taki_metal1_shape.sou"
#include "ext5_in_fall/taki_kage_shape.sou"
#include "ext5_in_fall/cx2801.flk"
#include "ext5_in_fall/cx2801.tag"

/* ====================================================================================	
		: Metal fall in the stage28 initialize. [ translucent ]
=======================================================================================	*/
Gfx mtfll_xlu_init[] = {
	gsDPPipeSync(),
	gsDPSetCycleType(G_CYC_2CYCLE),
	gsDPSetRenderMode(G_RM_FOG_SHADE_A, G_RM_AA_ZB_XLU_INTER2),
	gsDPSetDepthSource(G_ZS_PIXEL),
	gsDPSetFogColor(STAGE28_FOG_R, STAGE28_FOG_G, STAGE28_FOG_B, 255),
	gsSPFogPosition(STAGE28_FOG_START, 1000),
	gsSPSetGeometryMode(G_FOG),
	gsDPSetEnvColor(255, 255, 255, 140),
	gsDPSetCombineMode(G_CC_DECALRGB_ENVA, G_CC_PASS2),
	gsSPClearGeometryMode(G_LIGHTING | G_CULL_BACK),
	gsSPTexture(0xffff, 0xffff, 0, 0, G_ON),
	gs_Tani_SetUpTileDescrip(G_IM_FMT_RGBA, G_IM_SIZ_16b, 32, 32, 0, 0,
							 G_TX_WRAP|G_TX_NOMIRROR, 5, G_TX_NOLOD,
							 G_TX_WRAP|G_TX_NOMIRROR, 5, G_TX_NOLOD),
	gsSPEndDisplayList()
};

Gfx mtfll_xlu_reset[] = {
	gsSPTexture(0xffff, 0xffff, 0, 0, G_OFF),
	gsDPPipeSync(),
	gsDPSetCycleType(G_CYC_1CYCLE),
	gsSPSetGeometryMode(G_LIGHTING | G_CULL_BACK),
	gsSPClearGeometryMode(G_FOG),
	gsDPSetEnvColor(255, 255, 255, 255),
	gsDPSetCombineMode(G_CC_SHADE, G_CC_SHADE),
	gsDPSetRenderMode(G_RM_AA_ZB_XLU_INTER, G_RM_AA_ZB_XLU_INTER),
	gsSPEndDisplayList()
};

/* ====================================================================================	
		: Metal fall data 1 in the stage28.
=======================================================================================	*/
short metalfall_2801_pt[] = {
		30,
		256,     0,  -7373,	 0, 0,
		256,  5120,  -7373,	 4, 0,
	   -256,     0,  -7373,	 0, 1,
	   -256,  5120,  -7373,	 4, 1,

	   1536,  -204,   3584,  0, 0,
	   1536,     0,   3430,  1, 0,
	   1536,     0,  -7680,  5, 0,
	  -1536,  -204,   3584,  0, 2,
	  -1536,     0,   3430,  1, 2,
	  -1536,     0,  -7680,  5, 2,

	  -1024,  -614,   3584,  0, 0,
	  -1024,  1434,   3584,  1, 0,
	   1024,  -614,   3584,  0, 1,
	   1024,  1434,   3584,  1, 1,
};

Gfx metalfall_2801_draw[] = {
	gsSP1Triangle( 0,  1,  2, 0),
	gsSP1Triangle( 2,  1,  3, 0),
	gsSP1Triangle( 4,  5,  7, 0),
	gsSP1Triangle(10, 11, 12, 0),
	gsSP1Triangle(12, 11, 13, 0),
	gsSP1Triangle( 7,  5,  8, 0),
	gsSP1Triangle( 5,  6,  8, 0),
	gsSP1Triangle( 8,  6,  9, 0),
	gsSPEndDisplayList()
};
