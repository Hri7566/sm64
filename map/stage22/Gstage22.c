/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage22 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 11, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE22_FOG_R		0
#define		STAGE22_FOG_G		0
#define		STAGE22_FOG_B		0
#define		STAGE22_FOG_START	980


#define		a_b_TxT						a_renga01_txt
#define		a_renga_02_TxT				a_renga02_txt
#define		a_castle_wall2_TxT			a_renga03_txt
#define		a_renga_04_TxT				a_renga04_txt
#define		a_shingles_TxT				a_renga06_txt
#define		a_copy_of_shingles_TxT		a_renga07_txt
#define		a_renga_08_TxT				a_renga08_txt
#define		a_copy_of_yogan_0_TxT		a_renga09_txt
#define		a_shingles2_TxT				a_renga11_txt
#define		a_yuka_1_TxT				a_yuka01_txt
#define		a_kabe_8_TxT				a_yuka02_txt
#define		a_ya_TxT					a_yuka04_txt
#define		a_naokisan_TxT				a_ami01_txt
#define		a_naokisan2_TxT				a_ami02_txt
#define		a_yogan_0_TxT				a_yogan01_txt
#define		a_kabe_6_2_TxT				a_yogan02_txt
#define		a_fire_TxT					a_yogan01_txt
#define		a_gravei1_0_TxT				a_kabe02_txt		//	?
#define		a_kabe_6_TxT				a_kabe05_txt		//	?


#include "motos/motos_texture.h"
#include "in_valcano/in_valcano_ext_texture.h"


/* ----------------------------------------------------------------------------------------	*/
/* ----------------------------------------------------------------------------------------	*/

#include "motos/motos_light.sou"
#include "motos/mo_body_shape.sou"
#include "motos/mo_rgba_shape.sou"
#include "motos/mo_plate_shape.sou"
#include "motos/motos_bg01.sou"
#include "motos/motos_bg02.sou"
#include "motos/motos_bg03.sou"
#include "motos/motos_bg04.sou"
#include "motos/motos_bg05.sou"
#include "motos/motos_bg06.sou"
#include "motos/motos_bg07.sou"
#include "motos/motos_bg08.sou"
#include "motos/motos_bg09.sou"
#include "motos/motos_bg10.sou"
#include "motos/motos_bg11.sou"
#include "motos/motos_bg12.sou"

#include "motos/motos_obj01.sou"
#include "motos/motos_obj03.sou"
#include "motos/motos_obj05.sou"
#include "motos/motos_obj06.sou"
#include "motos/motos_obj07.sou"
#include "motos/motos_obj08.sou"
#include "motos/motos_obj09.sou"
#include "motos/motos_obj10.sou"
#include "motos/motos_obj11.sou"
#include "motos/motos_obj12.sou"

#include "motos/mo_mobj02_shape.sou"
#include "motos/mo_mobj04_shape.sou"

#include "motos/cx2201.flk"
#include "motos/cx2201.tag"

#include "motos/motos_obj01.flk"
#include "motos/motos_obj03.flk"
#include "motos/motos_obj05.flk"
#include "motos/motos_obj06.flk"
#include "motos/motos_obj07.flk"
#include "motos/motos_obj08.flk"
#include "motos/motos_obj09.flk"
#include "motos/motos_obj10.flk"
#include "motos/motos_obj11.flk"
#include "motos/motos_obj12.flk"

#include "motos/mo_mobj02_check.flk"
#include "motos/mo_mobj04_check.flk"
#include "motos/mo_kaiten_check.flk"


/* ----------------------------------------------------------------------------------------	*/
/* ----------------------------------------------------------------------------------------	*/


#define		STAGE2202_FOG_R			0
#define		STAGE2202_FOG_G			0
#define		STAGE2202_FOG_B			0
#define		STAGE2202_FOG_START		940

#include "in_valcano/fm_shape.sou"
#include "in_valcano/fm_gouraud_shape.sou"
#include "in_valcano/fm_i_alpha_shape.sou"
#include "in_valcano/fm_kakou_shape.sou"
#include "in_valcano/fm_rgba_shape.sou"
#include "in_valcano/fm_battan_shape.sou"

#include "in_valcano/cx2202.flk"
#include "in_valcano/cx2202.tag"

#include "in_valcano/fm_battan_check.flk"

#include "in_valcano/kazan1.ral"
#include "in_valcano/kazan2.ral"

/********************************************************************************/
/*	Water fall data in 22 - 1.													*/
/********************************************************************************/
short waterfall_2201_pt[] = {
	 1,

	 8191,  0,  8192,		0,	0,
	    0,  0,  8192,		0,	2,
	-8191,  0,  8192,		0,	4,

	 8191,  0,     0,		2,	0,
	    0,  0,     0,		2,	2,
	-8191,  0,     0,		2,	4,

	 8191,  0, -8192,		4,	0,
	    0,  0, -8192,		4,	2,
	-8191,  0, -8192,		4,	4,
};

Gfx waterfall_2201_draw[] = {
	gsSP1Triangle(0, 3, 1, 0),
	gsSP1Triangle(1, 3, 4, 0),
	gsSP1Triangle(1, 4, 2, 0),
	gsSP1Triangle(2, 4, 5, 0),
	gsSP1Triangle(3, 6, 4, 0),
	gsSP1Triangle(4, 6, 7, 0),
	gsSP1Triangle(4, 7, 5, 0),
	gsSP1Triangle(5, 7, 8, 0),
	gsSPEndDisplayList()
};

/********************************************************************************/
/*	Water surface records in 22 - 2.											*/
/********************************************************************************/
static short waterSurf_22_02_00[] = {
	1,
	0,  8,  5, -3071,-3071,   -3071, 3072,    3072, 3072,    3072,-3071,  1,  200,  4,
};

WaterSurfRec waterSurf_22_02[] = {
		{	10, waterSurf_22_02_00	 },
		{	-1,	NULL	}					//	End of WaterSurfRec data.
};

/********************************************************************************/
/*	Waterfall data in 22 - 2.													*/
/********************************************************************************/
short waterfall_2202_pt[] = {
		50,
		 655,  -86, -2934,		0,	0,
		 527,  256, -2934,		2,	0,
		 436, 1042, -2934,		4,	0,
		 430, 2555, -2934,		7,	0,
		 430, 5248, -2934,	   11,	0,

		 148,  -86, -2669,		0,	1,
		 256,  281, -2823,		2,	1,
		 138,  946, -2870,		4,	1,
		   1, 2555, -2917,		7,	1,
		   0, 5248, -2928,	   11,	1,

		  36,   63, -2766,		1,	1,

		-574,  -86, -2934,		0,	2,
		-471,  247, -2934,		2,	2,
		-389,  775, -2934,		4,	2,
		-390, 2555, -2934,		7,	2,
		-430, 5248, -2934,	   11,	2,
};

Gfx waterfall_2202_draw[] = {
	gsSP1Triangle(15, 14,  9, 0),
	gsSP1Triangle( 4,  9,  3, 0),
	gsSP1Triangle( 9, 14,  8, 0),
	gsSP1Triangle( 9,  8,  3, 0),
	gsSP1Triangle(14, 13,  8, 0),
	gsSP1Triangle( 8, 13,  7, 0),
	gsSP1Triangle( 8,  7,  2, 0),
	gsSP1Triangle( 8,  2,  3, 0),
	gsSP1Triangle(10,  6,  7, 0),
	gsSP1Triangle(13, 10,  7, 0),
	gsSP1Triangle(13, 12, 10, 0),
	gsSP1Triangle(12, 11, 10, 0),
	gsSP1Triangle(10, 11,  5, 0),
	gsSP1Triangle( 6, 10,  5, 0),
	gsSP1Triangle( 6,  5,  0, 0),
	gsSP1Triangle( 6,  0,  1, 0),
	gsSP1Triangle( 2,  6,  1, 0),
	gsSP1Triangle( 7,  6,  2, 0),
	gsSPEndDisplayList()
};
