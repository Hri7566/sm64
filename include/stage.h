/********************************************************************************
	project.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1995 Nintendo co., ltd.  All rights reserved

	September 6, 1995
 ********************************************************************************/

#ifndef STAGE_H
#define	STAGE_H

static char stageName[64][16] = {
	"",						/* No.01 */
	"" ,					/* No.02 */
	"",						/* No.03 */
	"TERESA OBAKE",			/* No.04 */
	"YYAMA1 % YSLD1",		/* No.05 */
	"SELECT ROOM",			/* No.06 */
	"HORROR DUNGEON",		/* No.07 */
	"SABAKU % PYRMD",		/* No.08 */
	"BATTLE FIELD",			/* No.09 */
	"YUKIYAMA2",			/* No.10 */
	"POOL KAI",				/* No.11 */
	"WTDG % TINBOTU",		/* No.12 */
	"BIG WORLD",			/* No.13 */
	"CLOCK TOWER",			/* No.14 */
	"RAINBOW CRUISE",		/* No.15 */
	"MAIN MAP",				/* No.16 */
	"EXT1 YOKO SCRL",		/* No.17 */
	"EXT7 HORI MINI",		/* No.18 */
	"EXT2 TIKA LAVA",		/* No.19 */
	"EXT9 SUISOU",			/* No.20 */
	"EXT3 HEAVEN",			/* No.21 */
	"FIREB1 % INVLC",		/* No.22 */
	"WATER LAND",			/* No.23 */
	"MOUNTAIN",				/* No.24 */
	"ENDING",				/* No.25 */
	"URANIWA",				/* No.26 */
	"EXT4 MINI SLID",		/* No.27 */
	"IN THE FALL",			/* No.28 */
	"EXT6 MARIO FLY",		/* No.29 */
	"KUPPA1",				/* No.30 */
	"EXT8 BLUE SKY",		/* No.31 */
	"",						/* No.32 */
	"KUPPA2",				/* No.33 */
	"KUPPA3",				/* No.34 */
	"",						/* No.35 */
	"DONKEY % SLID2",		/* No.36 */
	"",						/* No.37 */
	""						/* No.38 */
};

#endif
