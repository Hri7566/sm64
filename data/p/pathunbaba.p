/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: pathunbaba.s
					Description	: 
					Date		: 1995.11.20
					Author		: H.yajima

 ********************************************************************************/

#define		ANM_unbaba_swim	 	0
#define		ANM_unbaba_attack	1    	

/*################*/
#ifdef ASSEMBLER
/*################*/

e_unbaba:
	p_initialize(option)
	p_setbit(flag,stf_moveON | stf_YangleSAME | stf_FspeedON | stf_playerdistON | stf_playerangleON )
	p_set_pointer(skelanime,unbaba_anime)
	p_save_nowpos
	p_set_skelanime_number(0)
	p_while
		p_program(s_unbaba)
	p_loop

/*################*/
#else
/*################*/


#define		unbaba_movemode	(execstp->s[stw_work0].d)
#define		unbaba_movecounter	(execstp->s[stw_work1].d)

/********************************************************************************
		unbaba initialize	!!
 ********************************************************************************/

static void unbaba_init(void)
{
	obj_mode++;
}

/********************************************************************************
		unbaba swimming 	!!
 ********************************************************************************/

static void unbaba_swim(void)
{
	s_set_skelanimeNo(ANM_unbaba_swim);
	if ( obj_playerdist < 600 )	obj_mode = mode_unbaba_attack;
}

/********************************************************************************
		unbaba attack 		!!
 ********************************************************************************/

static void unbaba_attack(void)
{
	s_set_skelanimeNo(ANM_unbaba_attack);
	if ( s_check_animeend() )	obj_mode = mode_unbaba_swim;
}


/********************************************************************************
		#####################
		 unbaba main program
		#####################
 ********************************************************************************/

static void *unbaba_modejmp[] = {
  
   unbaba_init,
   unbaba_swim,
   unbaba_attack
  
};

extern void s_unbaba(void)
{
	int	mode;

	s_set_scale(2.0);

	s_modejmp(unbaba_modejmp);

}


/*################*/
#endif
/*################*/
/*===============================================================================
		end end end end end end end end 
===============================================================================*/

