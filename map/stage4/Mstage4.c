/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage4 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 18, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx gfx_tere_a[]		;
extern Gfx gfx_tere_a_a[]	;
extern Gfx gfx_tere_a_s[]	;
extern Gfx gfx_tere_a_ia[]	;
extern Gfx gfx_tere_a_kn[]	;
extern Gfx gfx_tere_a_mr[]	;
extern Gfx gfx_tere_b[]		;
extern Gfx gfx_tere_b_s[]	;
extern Gfx gfx_tere_b_ia[]	;
extern Gfx gfx_tere_b_kn[]	;
extern Gfx gfx_tere_b_mr[]	;
extern Gfx gfx_tere_c[]		;
extern Gfx gfx_tere_c_a[]	;
extern Gfx gfx_tere_c_s[]	;
extern Gfx gfx_tere_c_mr[]	;
extern Gfx gfx_tere_d[]		;
extern Gfx gfx_tere_d_a[]	;
extern Gfx gfx_tere_e[]		;
extern Gfx gfx_tere_f[]		;
extern Gfx gfx_tere_f_r[]	;
extern Gfx gfx_tere_f_cr[]	;
extern Gfx gfx_tere_g[]		;
extern Gfx gfx_tere_g_ia[]	;
extern Gfx gfx_tere_h[]		;
extern Gfx gfx_tere_h_a[]	;
extern Gfx gfx_tere_h_ia[]	;
extern Gfx gfx_tere_h_kn[]	;
extern Gfx gfx_tere_i[]		;
extern Gfx gfx_tere_i_s[]	;
extern Gfx gfx_tere_i_mr[]	;
extern Gfx gfx_tere_j[]		;
extern Gfx gfx_tere_j_a[]	;
extern Gfx gfx_tere_k[]		;
extern Gfx gfx_tere_l[]		;
extern Gfx gfx_tere_m[]		;
extern Gfx gfx_tere_m_a[]	;
extern Gfx gfx_tere_m_s[]	;
extern Gfx gfx_tere_m_ia[]	;
extern Gfx gfx_tere_m_kn[]	;

extern Gfx gfx_a_trap[]		;
extern Gfx gfx_b_trap[]		;
extern Gfx gfx_d_trap_a[]	;
extern Gfx gfx_d_trap_b[]	;
extern Gfx gfx_f_trap[]		;
extern Gfx gfx_l_trap[]		;
extern Gfx gfx_j_trap[]		;
extern Gfx gfx_kanoke[]		;


/* --------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------- */


/*********************************************************************************
    : Hierarchy map data of Tere_a_trap.
**********************************************************************************/
Hierarchy RCP_HmsTere_a_trap[] = {
    hmsHeader(700)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_a_trap)
    hmsEnd()
    hmsExit()
};

/*********************************************************************************
    : Hierarchy map data of Tere_b_trap.
**********************************************************************************/
Hierarchy RCP_HmsTere_b_trap[] = {
    hmsHeader(600)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_b_trap)
    hmsEnd()
    hmsExit()
};

/*********************************************************************************
    : Hierarchy map data of Tere_d_trap_a.
**********************************************************************************/
Hierarchy RCP_HmsTere_d_trap_a[] = {
    hmsHeader(650)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_d_trap_a)
    hmsEnd()
    hmsExit()
};

/*********************************************************************************
    : Hierarchy map data of Tere_d_trap_b.
**********************************************************************************/
Hierarchy RCP_HmsTere_d_trap_b[] = {
    hmsHeader(300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_d_trap_b)
    hmsEnd()
    hmsExit()
};

/*********************************************************************************
    : Hierarchy map data of Tere_f_trap.
**********************************************************************************/
Hierarchy RCP_HmsTere_f_trap[] = {
    hmsHeader(1000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_f_trap)
    hmsEnd()
    hmsExit()
};

/*********************************************************************************
    : Hierarchy map data of Tere_l_trap.
**********************************************************************************/
Hierarchy RCP_HmsTere_l_trap[] = {
    hmsHeader(600)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_l_trap)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Tere_j_trap.
================================================================================== */
Hierarchy RCP_HmsTere_j_trap[] = {
    hmsHeader(2300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_j_trap)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of TeresaKanoke.
================================================================================== */
Hierarchy RCP_HmsTeresaKanoke[] = {
    hmsHeader(800)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kanoke)
    hmsEnd()
    hmsExit()
};


/* ---------------------------------------------------------------------------------------- */
/* ---------------------------------------------------------------------------------------- */


/* ================================================================================
        : Switch map shape 01.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap01[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_a)
        hmsGfx(RM_SPRITE, gfx_tere_a_a)
        hmsGfx(RM_SURF  , gfx_tere_a_s)
        hmsGfx(RM_XSURF , gfx_tere_a_ia)
        hmsGfx(RM_SURF  , gfx_tere_a_kn)
        hmsGfx(RM_XSURF , gfx_tere_a_mr)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 02.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap02[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF , gfx_tere_b)
        hmsGfx(RM_SURF , gfx_tere_b_s)
        hmsGfx(RM_XSURF, gfx_tere_b_ia)
        hmsGfx(RM_SURF , gfx_tere_b_kn)
        hmsGfx(RM_XSURF, gfx_tere_b_mr)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 03.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap03[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_c)
        hmsGfx(RM_SPRITE, gfx_tere_c_a)
        hmsGfx(RM_SURF  , gfx_tere_c_s)
        hmsGfx(RM_XSURF , gfx_tere_c_mr)

        hmsGfx(RM_SURF  , gfx_tere_f)
		hmsGfx(RM_SURF  , gfx_tere_f_r)
		hmsGfx(RM_XSURF , gfx_tere_f_cr)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 04.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap04[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_d)
        hmsGfx(RM_SPRITE, gfx_tere_d_a)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 05.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap05[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_e)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 06.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap06[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_c)
        hmsGfx(RM_SPRITE, gfx_tere_c_a)
        hmsGfx(RM_SURF  , gfx_tere_c_s)
        hmsGfx(RM_XSURF , gfx_tere_c_mr)

        hmsGfx(RM_SURF , gfx_tere_f)
		hmsGfx(RM_SURF , gfx_tere_f_r)
		hmsGfx(RM_XSURF, gfx_tere_f_cr)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 07.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap07[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF , gfx_tere_g)
		hmsGfx(RM_XSURF, gfx_tere_g_ia)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 08.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap08[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_h)
        hmsGfx(RM_SPRITE, gfx_tere_h_a)
		hmsGfx(RM_XSURF , gfx_tere_h_ia)
		hmsGfx(RM_SURF  , gfx_tere_h_kn)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 09.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap09[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF , gfx_tere_i)
        hmsGfx(RM_SURF , gfx_tere_i_s)
		hmsGfx(RM_XSURF, gfx_tere_i_mr)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 10.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap10[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_j)
		hmsGfx(RM_SPRITE, gfx_tere_j_a)
		hmsCProg(0, WaterInit)
		hmsCProg(0x0400, WaterDraw)
		hmsCProg(0x0401, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 11.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap11[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF, gfx_tere_k)
		hmsCProg(0, WaterInit)
		hmsCProg(0x0400, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 12.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap12[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF, gfx_tere_l)
		hmsCProg(0, WaterInit)
		hmsCProg(0x0400, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 13.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap13[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_m)
        hmsGfx(RM_SPRITE, gfx_tere_m_a)
        hmsGfx(RM_SURF  , gfx_tere_m_s)
		hmsGfx(RM_XSURF , gfx_tere_m_ia)
		hmsGfx(RM_SURF  , gfx_tere_m_kn)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 14.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap14[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_a)
        hmsGfx(RM_SPRITE, gfx_tere_a_a)
        hmsGfx(RM_SURF  , gfx_tere_a_s)
        hmsGfx(RM_XSURF , gfx_tere_a_ia)
        hmsGfx(RM_SURF  , gfx_tere_a_kn)
        hmsGfx(RM_XSURF , gfx_tere_a_mr)

        hmsGfx(RM_SURF , gfx_tere_b)
        hmsGfx(RM_SURF , gfx_tere_b_s)
        hmsGfx(RM_XSURF, gfx_tere_b_ia)
        hmsGfx(RM_SURF , gfx_tere_b_kn)
        hmsGfx(RM_XSURF, gfx_tere_b_mr)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 15.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap15[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_a)
        hmsGfx(RM_SPRITE, gfx_tere_a_a)
        hmsGfx(RM_SURF  , gfx_tere_a_s)
        hmsGfx(RM_XSURF , gfx_tere_a_ia)
        hmsGfx(RM_SURF  , gfx_tere_a_kn)
        hmsGfx(RM_XSURF , gfx_tere_a_mr)

        hmsGfx(RM_SURF  , gfx_tere_c)
        hmsGfx(RM_SPRITE, gfx_tere_c_a)
        hmsGfx(RM_SURF  , gfx_tere_c_s)
        hmsGfx(RM_XSURF , gfx_tere_c_mr)

        hmsGfx(RM_SURF  , gfx_tere_f)
		hmsGfx(RM_SURF  , gfx_tere_f_r)
		hmsGfx(RM_XSURF , gfx_tere_f_cr)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 16.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap16[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_a)
        hmsGfx(RM_SPRITE, gfx_tere_a_a)
        hmsGfx(RM_SURF  , gfx_tere_a_s)
        hmsGfx(RM_XSURF , gfx_tere_a_ia)
        hmsGfx(RM_SURF  , gfx_tere_a_kn)
        hmsGfx(RM_XSURF , gfx_tere_a_mr)

        hmsGfx(RM_SURF  , gfx_tere_d)
        hmsGfx(RM_SPRITE, gfx_tere_d_a)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 17.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap17[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_a)
        hmsGfx(RM_SPRITE, gfx_tere_a_a)
        hmsGfx(RM_SURF  , gfx_tere_a_s)
        hmsGfx(RM_XSURF , gfx_tere_a_ia)
        hmsGfx(RM_SURF  , gfx_tere_a_kn)
        hmsGfx(RM_XSURF , gfx_tere_a_mr)

        hmsGfx(RM_SURF  , gfx_tere_e)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 18.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap18[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_a)
        hmsGfx(RM_SPRITE, gfx_tere_a_a)
        hmsGfx(RM_SURF  , gfx_tere_a_s)
        hmsGfx(RM_XSURF , gfx_tere_a_ia)
        hmsGfx(RM_SURF  , gfx_tere_a_kn)
        hmsGfx(RM_XSURF , gfx_tere_a_mr)

        hmsGfx(RM_SURF  , gfx_tere_c)
        hmsGfx(RM_SPRITE, gfx_tere_c_a)
        hmsGfx(RM_SURF  , gfx_tere_c_s)
        hmsGfx(RM_XSURF , gfx_tere_c_mr)

        hmsGfx(RM_SURF  , gfx_tere_f)
		hmsGfx(RM_SURF  , gfx_tere_f_r)
		hmsGfx(RM_XSURF , gfx_tere_f_cr)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 19.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap19[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_a)
        hmsGfx(RM_SPRITE, gfx_tere_a_a)
        hmsGfx(RM_SURF  , gfx_tere_a_s)
        hmsGfx(RM_XSURF , gfx_tere_a_ia)
        hmsGfx(RM_SURF  , gfx_tere_a_kn)
        hmsGfx(RM_XSURF , gfx_tere_a_mr)

        hmsGfx(RM_SURF , gfx_tere_g)
		hmsGfx(RM_XSURF, gfx_tere_g_ia)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 20.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap20[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_a)
        hmsGfx(RM_SPRITE, gfx_tere_a_a)
        hmsGfx(RM_SURF  , gfx_tere_a_s)
        hmsGfx(RM_XSURF , gfx_tere_a_ia)
        hmsGfx(RM_SURF  , gfx_tere_a_kn)
        hmsGfx(RM_XSURF , gfx_tere_a_mr)

        hmsGfx(RM_SURF  , gfx_tere_h)
        hmsGfx(RM_SPRITE, gfx_tere_h_a)
		hmsGfx(RM_XSURF , gfx_tere_h_ia)
		hmsGfx(RM_SURF  , gfx_tere_h_kn)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 21.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap21[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_a)
        hmsGfx(RM_SPRITE, gfx_tere_a_a)
        hmsGfx(RM_SURF  , gfx_tere_a_s)
        hmsGfx(RM_XSURF , gfx_tere_a_ia)
        hmsGfx(RM_SURF  , gfx_tere_a_kn)
        hmsGfx(RM_XSURF , gfx_tere_a_mr)

        hmsGfx(RM_SURF  , gfx_tere_m)
        hmsGfx(RM_SPRITE, gfx_tere_m_a)
        hmsGfx(RM_SURF  , gfx_tere_m_s)
		hmsGfx(RM_XSURF , gfx_tere_m_ia)
		hmsGfx(RM_SURF  , gfx_tere_m_kn)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 22.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap22[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF , gfx_tere_b)
        hmsGfx(RM_SURF , gfx_tere_b_s)
        hmsGfx(RM_XSURF, gfx_tere_b_ia)
        hmsGfx(RM_SURF , gfx_tere_b_kn)
        hmsGfx(RM_XSURF, gfx_tere_b_mr)

        hmsGfx(RM_SURF  , gfx_tere_d)
        hmsGfx(RM_SPRITE, gfx_tere_d_a)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 23.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap23[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF , gfx_tere_b)
        hmsGfx(RM_SURF , gfx_tere_b_s)
        hmsGfx(RM_XSURF, gfx_tere_b_ia)
        hmsGfx(RM_SURF , gfx_tere_b_kn)
        hmsGfx(RM_XSURF, gfx_tere_b_mr)

        hmsGfx(RM_SURF , gfx_tere_i)
        hmsGfx(RM_SURF , gfx_tere_i_s)
		hmsGfx(RM_XSURF, gfx_tere_i_mr)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 24.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap24[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF , gfx_tere_b)
        hmsGfx(RM_SURF , gfx_tere_b_s)
        hmsGfx(RM_XSURF, gfx_tere_b_ia)
        hmsGfx(RM_SURF , gfx_tere_b_kn)
        hmsGfx(RM_XSURF, gfx_tere_b_mr)

        hmsGfx(RM_SURF  , gfx_tere_j)
		hmsGfx(RM_SPRITE, gfx_tere_j_a)

		hmsCProg(0, WaterInit)
		hmsCProg(0x0400, WaterDraw)
		hmsCProg(0x0401, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 25.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap25[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_c)
        hmsGfx(RM_SPRITE, gfx_tere_c_a)
        hmsGfx(RM_SURF  , gfx_tere_c_s)
        hmsGfx(RM_XSURF , gfx_tere_c_mr)

        hmsGfx(RM_SURF  , gfx_tere_e)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 26.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap26[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_c)
        hmsGfx(RM_SPRITE, gfx_tere_c_a)
        hmsGfx(RM_SURF  , gfx_tere_c_s)
		hmsGfx(RM_XSURF , gfx_tere_c_mr)

        hmsGfx(RM_SURF , gfx_tere_f)
		hmsGfx(RM_SURF , gfx_tere_f_r)
		hmsGfx(RM_XSURF, gfx_tere_f_cr)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 27.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap27[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_d)
        hmsGfx(RM_SPRITE, gfx_tere_d_a)

        hmsGfx(RM_SURF  , gfx_tere_j)
		hmsGfx(RM_SPRITE, gfx_tere_j_a)

		hmsCProg(0, WaterInit)
		hmsCProg(0x0400, WaterDraw)
		hmsCProg(0x0401, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 28.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap28[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_e)

        hmsGfx(RM_SURF  , gfx_tere_m)
        hmsGfx(RM_SPRITE, gfx_tere_m_a)
        hmsGfx(RM_SURF  , gfx_tere_m_s)
		hmsGfx(RM_XSURF , gfx_tere_m_ia)
		hmsGfx(RM_SURF  , gfx_tere_m_kn)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 29.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap29[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF , gfx_tere_i)
        hmsGfx(RM_SURF , gfx_tere_i_s)
		hmsGfx(RM_XSURF, gfx_tere_i_mr)

        hmsGfx(RM_SURF  , gfx_tere_m)
        hmsGfx(RM_SPRITE, gfx_tere_m_a)
        hmsGfx(RM_SURF  , gfx_tere_m_s)
		hmsGfx(RM_XSURF , gfx_tere_m_ia)
		hmsGfx(RM_SURF  , gfx_tere_m_kn)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 30.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap30[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_tere_j)
		hmsGfx(RM_SPRITE, gfx_tere_j_a)

        hmsGfx(RM_SURF, gfx_tere_k)
		hmsCProg(0, WaterInit)
		hmsCProg(0x0400, WaterDraw)
		hmsCProg(0x0401, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 31.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap31[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF, gfx_tere_k)
        hmsGfx(RM_SURF, gfx_tere_l)
		hmsCProg(0, WaterInit)
		hmsCProg(0x0400, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 32.
=================================================================================== */
Hierarchy RCP_Hms_teresa_switchmap32[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF, gfx_tere_l)

        hmsGfx(RM_SURF  , gfx_tere_m)
        hmsGfx(RM_SPRITE, gfx_tere_m_a)
        hmsGfx(RM_SURF  , gfx_tere_m_s)
		hmsGfx(RM_XSURF , gfx_tere_m_ia)
		hmsGfx(RM_SURF  , gfx_tere_m_kn)

		hmsCProg(0, WaterInit)
		hmsCProg(0x0400, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage4Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(6, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, 50, 10000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()

					hmsSelect(32, ControlSwitchMap)
					hmsBegin()
						hmsCall(RCP_Hms_teresa_switchmap01)
						hmsCall(RCP_Hms_teresa_switchmap02)
						hmsCall(RCP_Hms_teresa_switchmap03)
						hmsCall(RCP_Hms_teresa_switchmap04)
						hmsCall(RCP_Hms_teresa_switchmap05)
						hmsCall(RCP_Hms_teresa_switchmap06)
						hmsCall(RCP_Hms_teresa_switchmap07)
						hmsCall(RCP_Hms_teresa_switchmap08)
						hmsCall(RCP_Hms_teresa_switchmap09)
						hmsCall(RCP_Hms_teresa_switchmap10)
						hmsCall(RCP_Hms_teresa_switchmap11)
						hmsCall(RCP_Hms_teresa_switchmap12)
						hmsCall(RCP_Hms_teresa_switchmap13)
						hmsCall(RCP_Hms_teresa_switchmap14)
						hmsCall(RCP_Hms_teresa_switchmap15)
						hmsCall(RCP_Hms_teresa_switchmap16)
						hmsCall(RCP_Hms_teresa_switchmap17)
						hmsCall(RCP_Hms_teresa_switchmap18)
						hmsCall(RCP_Hms_teresa_switchmap19)
						hmsCall(RCP_Hms_teresa_switchmap20)
						hmsCall(RCP_Hms_teresa_switchmap21)
						hmsCall(RCP_Hms_teresa_switchmap22)
						hmsCall(RCP_Hms_teresa_switchmap23)
						hmsCall(RCP_Hms_teresa_switchmap24)
						hmsCall(RCP_Hms_teresa_switchmap25)
						hmsCall(RCP_Hms_teresa_switchmap26)
						hmsCall(RCP_Hms_teresa_switchmap27)
						hmsCall(RCP_Hms_teresa_switchmap28)
						hmsCall(RCP_Hms_teresa_switchmap29)
						hmsCall(RCP_Hms_teresa_switchmap30)
						hmsCall(RCP_Hms_teresa_switchmap31)
						hmsCall(RCP_Hms_teresa_switchmap32)
					hmsEnd()

					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

	hmsEnd()
	hmsExit()
};
