/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 23 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
*********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage23


/* ================================================================================
		: Scene 1 Enemies sequence.
===================================================================================	*/
SEQ_Stage2301_Enemies:

		seqActor(S_shark	, -3071, -270,  0,  0,0,0,  0,0,0,	e_shark				 )
		seqActor(S_shark	, -3071,-4270,  0,  0,0,0,  0,0,0,	e_shark				 )
		seqActor(S_NULL	 	, -3071, -130,  0,  0,0,0,  0,0,0,	e_fishmother_little	 )
		seqActor(S_NULL	 	, -3071,-4270,  0,  0,0,0,  0,0,0,	e_fishmother_many 	 )
		seqActor(S_NULL	 	, -3071,-2000,  0,  0,0,0,  0,0,0,	e_pukumother	 	 )
		seqActor(S_NULL	 	, -3071,-3000,  0,  0,0,0,  0,0,0,	e_pukumother	 	 )
		seqActor(S_w_tornade, -3174,-4915,102,  0,0,0,  0,0,0,	e_uzumaki		 	 )


		seqReturn


/* ================================================================================
		: Scene 1 Stars sequence.
===================================================================================	*/
SEQ_Stage2301_Stars:

		seqLevelActor(0x003f,S_NULL, 	-2400, -4607,  125,  0,0,0,  1,0,0,  e_tbox_quize23)	/* Takara box	*/
		seqLevelActor(0x003e,S_manta,   -4640, -1380,   40,  0,0,0,  4,0,0,  e_d_manta			 )  /* manta 			*/

		seqReturn


/* ================================================================================
		: Scene 2 Move BGs sequence.
===================================================================================	*/
SEQ_Stage2302_MoveBGs:

		seqActor(S_movebg01,  0,0,0,  0,0,0,  0,0,0,  e_sensuikan_futa)
		seqActor(S_movebg02,  0,0,0,  0,0,0,  0,0,0,  e_sensuikan)

		seqActor(S_movebg03,  5120, 1005, 3584,  0,180,0,  0,30,0,  e_movebar)
		seqActor(S_movebg03,  5605, 1005, 3380,  0,270,0,  0,21,0,  e_movebar)
		seqActor(S_movebg03,  1800, 1005, 1275,  0,  0,0,  0,11,0,  e_movebar)
		seqActor(S_movebg03,  4000, 1005, 1075,  0,180,0,  0,11,0,  e_movebar)
		seqActor(S_movebg03,  1830, 1005,  520,  0,270,0,  0,20,0,  e_movebar)
		seqActor(S_movebg03,  4000, 1005, 1275,  0,  0,0,  0,11,0,  e_movebar)
		seqActor(S_movebg03,  5760, 1005,  360,  0,270,0,  0,23,0,  e_movebar)
		seqActor(S_movebg03,  3310, 1005,-1945,  0,  0,0,  0,23,0,  e_movebar)
		seqActor(S_movebg03,  3550, 1005,-2250,  0,  0,0,  0,13,0,  e_movebar)

		seqReturn


/* ================================================================================
		: Scene 2 Enemies sequence.
===================================================================================	*/
SEQ_Stage2302_Enemies:

		seqActor(S_NULL,   3404,-3319,-489,  0,0,0,  0,0,0,	 e_fukidasi		 	)

		seqReturn


/* ================================================================================
		: Scene 2 Stars sequence.
===================================================================================	*/
SEQ_Stage2302_Stars:

		seqLevelActor(0x003f,S_polystar,  3900,   850, -600,  0,0,0,  0,0,0,  e_tripstar) 			/* on_the_submarine	*/
		seqLevelActor(0x003f,S_NULL    ,  5513,  1200,  900,  0,0,0,  2,0,0,  e_tripstar_getcoins)	/* 8_coin 			*/
		seqLevelActor(0x003f,S_NULL	   ,  3404, -3319, -489,  0,0,0,  3,0,0,  e_ring		     )	/* ring 			*/
		seqLevelActor(0x003f,S_polystar,  2030, -3700,-2780,  0,0,0,  5,0,0,  e_tripstar)			/* in kanaami		*/

		seqReturn


/* ================================================================================
		: Stage 23 main sequence.
===================================================================================	*/
SEQ_DoStage23:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1 	  , _GfxStage23SegmentRomStart	  , _GfxStage23SegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE   , _DWaterTextureSegmentRomStart , _DWaterTextureSegmentRomEnd )
	seqLoadPres(SEGMENT_WEATHER	  , _WeatherSegmentRomStart	      , _WeatherSegmentRomEnd		)
	seqLoadPres(SEGMENT_BACK	  , _BackMainmapSegmentRomStart	  , _BackMainmapSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_dSegmentRomStart	  , _GfxEnemy1_dSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_dSegmentRomStart	  , _HmsEnemy1_dSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_dSegmentRomStart	  , _GfxEnemy2_dSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_dSegmentRomStart	  , _HmsEnemy2_dSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario	)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_d)
		seqCall(SetEnemy2_d)

		seqHmsShape(S_movebg01, RCP_HmsWlandHuta)
		seqHmsShape(S_movebg02, RCP_HmsWlandSm	)
		seqHmsShape(S_movebg03, RCP_HmsWlandBou	)

		seqBeginScene(1, RCP_Stage23Scene1)
			seqActor(S_NULL,  -3071,3000,0,  0,7,0,  0,10,0,  e_player_entpict)
			seqPort(10, 23, 1, 10)
			seqGameClear(6, 3,  53)
			seqGameOver (6, 3, 103)
			seqWaterJet(0, -3174,-4915,102, 20, SEQ_KOOPA_NON)			/*	Suikomi.	*/
			seqCall(SEQ_Stage2301_Enemies)
			seqCall(SEQ_Stage2301_Stars)
			seqConnect(3, 2, -8192,0,0)
			seqMapInfo(cx2301_info)
			seqTagInfo(cx2301_info_tag)
			seqSetMusic(NA_STG_WATER, NA_SEA_BGM)
			seqEnvironment(ENV_WATER)
		seqEndScene()

		seqBeginScene(2, RCP_Stage23Scene2)
			seqWaterJet(0, 3355,-3575, -515, -30, SEQ_KOOPA_NON )		/*	Hakidashi.	*/	
			seqWaterJet(1, 3917,-2040,-6041,  50, SEQ_KOOPA_DIED)		/*	Suikomi.	*/
			seqGameClear(6, 3,  53)
			seqGameOver (6, 3, 103)
			seqCourseOut(16, 1, 30)
			seqCall(SEQ_Stage2302_MoveBGs)
			seqCall(SEQ_Stage2302_Enemies)
			seqCall(SEQ_Stage2302_Stars)
			seqConnect(2, 1, 8192,0,0)
			seqMapInfo(cx2302_info)
			seqTagInfo(cx2302_info_tag)
			seqSetMusic(NA_STG_WATER, NA_SEA_BGM)
			seqEnvironment(ENV_WATER)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 180,  -3071,3000,500)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
