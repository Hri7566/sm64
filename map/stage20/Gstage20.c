/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage20 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE20_FOG_R		0
#define		STAGE20_FOG_G		0
#define		STAGE20_FOG_B		0
#define		STAGE20_FOG_START	980

#include "ext9/suisou_shape.sou"
#include "ext9/suisou_ht_shape.sou"

#include "ext9/cx2001.flk"
#include "ext9/cx2001.tag"
