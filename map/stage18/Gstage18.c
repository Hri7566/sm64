/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage18 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE18_FOG_R		0
#define		STAGE18_FOG_G		0
#define		STAGE18_FOG_B		0
#define		STAGE18_FOG_START	980

#include "dummy/ext7_ext_texture.h"

#include "dummy/hs_1_shape.sou"
#include "dummy/hs_alpha_shape.sou"
#include "dummy/hs_gaiheki_shape.sou"
#include "dummy/hs_clear_shape.sou"

#include "dummy/hs_obj_shape.sou"

#include "dummy/cx1801.flk"
#include "dummy/cx1801.tag"

#include "dummy/hs_obj_check.flk"
