/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: pathwaterdrop.s
					Description	: 
					Date		: 1995.
					Author		: H.yajima

 ********************************************************************************/


/*################*/
#ifdef ASSEMBLER
/*################*/

/*--------------------------------------------------------------*/
/*		small water drop program								*/
/*--------------------------------------------------------------*/

e_smallwaterdrop:
	p_initialize(effect)
 	p_setbit(flag,stf_moveON | stf_FspeedON | stf_YangleSAME )
	p_softspritemodeON
	p_while
		p_program(s_smalldrop_main)
	p_loop


/*################*/
#else
/*################*/

/********************************************************************************
		smalldrop
 ********************************************************************************/
extern void s_smalldrop_main(void)
{

	StrategyRecord *stratp;
	float waterY = mcWaterCheck(obj_worldX,obj_worldZ);

	if (obj_timer == 0 ){
		if ( s_check_shapename(S_fish) ) MapBboardOff(&execstp->map);	/* softwart sprite off  */	
		else							 MapBboardOn(&execstp->map);	/* softwart sprite on   */
		obj_animeangleY = Randomd();
	}

	obj_speedY -= 4;
	obj_worldY += obj_speedY;

	if ( obj_speedY < 0 ){
		if ( waterY > obj_worldY ){
			s_makeobj_effect(0,1,execstp,S_rippleA,e_smalldropripple);
			s_remove_obj(execstp);
		} else {
			if ( obj_timer > 20 )	s_remove_obj(execstp);
		}
	}

	if ( waterY < -10000 )	s_remove_obj(execstp);

}


/*################*/
#endif
/*################*/
/*===============================================================================
		end end end end end end end end 
===============================================================================*/



