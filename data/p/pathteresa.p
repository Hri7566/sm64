#define		OBAKESET_STARCOIN		12
/*-------------------------------------------------------

	effect_p1 .......... (5)teresa ===> (BOSS)teresa

--------------------------------------------------------*/


/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: pathob.s
					Description	: 
					Date		: 1995.
					Author		: H.yajima

 ********************************************************************************/


/*################*/
#ifdef ASSEMBLER
/*################*/


/********************************************************************************
		---------------------
		 wakidashi 5 teresa 	
		---------------------
 ********************************************************************************/

e_5teresa:
	p_initialize(option)
	p_setbit(flag,stf_moveON | stf_playerdistON )
	p_while
		p_program(s_5teresa);
	p_loop

/********************************************************************************
		---------------------
		 Kago Teresa Program
		---------------------
 ********************************************************************************/

e_obake_fire:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON )
	p_setmovedata(30,-400,-70,1000,1000,200,0,0)
	p_softspritemodeON
	p_while
		p_program(s_obake_fire)
		p_animeinc
		p_inc_frame(animecounter,2)
	p_loop



/********************************************************************************
		---------------------
		 Kago Teresa Program
		---------------------
 ********************************************************************************/

e_michi_teresa:
	p_initialize(option)
	p_setbit( flag,stf_moveON | stf_YangleSAME | stf_playerdistON | stf_playerangleON )
	p_save_nowpos
	p_setf(animepositionY,60)
	p_setmovedata(30,0,-50,1000,1000,200,0,0)
	p_program(s_areastage_init)	
	p_while
		p_program(s_michi_teresa)
	p_loop



e_boss_teresa:
	p_initialize(enemyA)
	p_setbit( flag,stf_moveON | stf_YangleSAME | stf_playerdistON | stf_playerangleON )
	p_save_nowpos
	p_setd(ap,3)
	p_setdamagebox(80,120)
	p_sethitbox(180,140)
	p_setf(animepositionY,120/2)
	p_setmovedata(30,0,-50,1000,1000,200,0,0)
	p_program(s_kagoteresa_makekago)
	p_program(s_areastage_init)	
	p_while
		p_program(s_kagoteresa)
	p_loop



/********************************************************************************
		-----------------------
		 kaidan teresa program
		-----------------------
 ********************************************************************************/

/*--------------------------------------*/
/*	 3F ..... teresa					*/
/*--------------------------------------*/

e_3Fteresa:
	p_initialize(enemyA)
	p_setd(programselect,2)
	p_setd(work8,10)				/* terecount */
	p_jmp(bigteresa_common)

/*--------------------------------------*/
/*	B1F ..... teresa					*/
/*--------------------------------------*/
e_BFteresa:
	p_initialize(enemyA)
	p_setd(programselect,1)
	p_setd(work8,10)				/* terecount */
	p_jmp(bigteresa_common)

/*--------------------------------------*/
/*	Kaidan Teresa						*/
/*--------------------------------------*/
e_kaidanteresa:
	p_initialize(enemyA)
bigteresa_common:
	p_setbit( flag,stf_moveON | stf_YangleSAME | stf_playerdistON | stf_playerangleON )
	p_save_nowpos
	p_setmovedata(30,0,-50,1000,1000,200,0,0)
	p_program(s_areastage_init)	
	p_program(s_obake_reset)
	p_while
		p_program(s_kaidanteresa)
	p_loop


/********************************************************************************
		-------------------
		 ( uraniwa teresa )
		-------------------
 ********************************************************************************/

e_obake_fly:
	p_initialize(option)
	p_shapeDISABLE								 
	p_program(s_make3teresa)
	p_killshape


/********************************************************************************
		
		(3F) .... random teresa

 ********************************************************************************/

e_normal_teresa:
	p_initialize(enemyA)
	p_setd(programselect,1)
	p_jmp(teresa_common)



/********************************************************************************
		
		(BF) .... wakidashi teresa

 ********************************************************************************/

e_kabenuketeresa:
	p_initialize(enemyA)
	p_setd(programselect,2)
	p_jmp(teresa_common)
		

/********************************************************************************
		
		(1F) .... 5 teresa 

 ********************************************************************************/

e_cointeresa:
e_teresa:
e_miniteresa:
	p_initialize(enemyA)
teresa_common:
	p_setbit( flag,stf_moveON | stf_YangleSAME | stf_playerdistON | stf_playerangleON )
	p_hitON
	p_save_nowpos
	p_setd(ap,2)
	p_sethitbox(140,80)
	p_setdamagebox(40,60)
	p_setf(animepositionY,60/2)
	p_program(s_areastage_init)
	p_makeshape(S_coin,e_obake_coin)
	p_setmovedata(30,0,-50,1000,1000,200,0,0)
	p_program(s_obake_reset)
	p_while
		p_program(s_oba)
	p_loop

/*################*/
#else
/*################*/


#define			oba_alphawork	(execstp->s[stw_work0].d)
#define			oba_scale	(execstp->s[stw_work1].f)
#define			oba_sinmoveY	(execstp->s[stw_work2].d)
#define			oba_damageANG	(execstp->s[stw_work3].d)
#define			oba_stockY	(execstp->s[stw_work4].f)
#define		oba_kaidanptr	(execstp->s[stw_work5].pointer)
#define			oba_randomspeed	(execstp->s[stw_work6].f)
#define			oba_startangle	(execstp->s[stw_work7].d)

#define	oba_randomangle		(execstp->s[stw_work9].word[0] )
		
/*------------------

---------------*/

#define	terecount(n)	(n->s[stw_work8].d)

static void obake_speedstop(void)
{
	obj_speedF  = 0;
	obj_speedY  = 0;
	obj_gravity = 0;
}

extern void s_obake_reset(void)
{
	oba_startangle = obj_angleY;
}


/********************************************************************************
		areaout check
 ********************************************************************************/



static int teresa_areaout_check(void)
{

	if ( s_check_pathname(e_BFteresa) || s_check_pathname(e_kabenuketeresa) ){

		if ( bf_teresa_flag == 0 )  return(1);
		else						return(0);

	} else {
		if ( execstp->status   & OBJECT_AREAOUT 		) 	return(1);
		if ( obj_areamap == 10 ){
			if ( strategy_mainflag & STRATMAIN_DOOROPEN )	return(1);
		}
	}
	return(0);
}

/********************************************************************************
		hitcheck parameter
 ********************************************************************************/

static HitCheckRecord teresa_hit = {
	0,	
	0,3,3,0,	/* flag,ap,hp,coin 		 */
	140,80,
	40,60,
};


/********************************************************************************
		Appear / No-Appear Check
 ********************************************************************************/

static int obake_appearcheck(void)
{


	float	dist;
	if ( s_check_pathname(e_3Fteresa) )	dist = 5000;
	else								dist = 1500;			



	if ( s_check_pathname(e_BFteresa) || s_check_pathname(e_kabenuketeresa) ){

		if ( bf_teresa_flag == 1 )  return(1);
		else						return(0);

	} else {

		if ( obj_areamap == -1 ){
		 	if ( obj_playerdist < dist )	return(1);
		} else {
			if ( teresa_areaout_check() == 0 ){
				if ( obj_playerdist < dist && (( obj_areamap == map_areacode ) || ( map_areacode == 0 )) ) return(1);
			}
		}
	}
		
	return(0);
}

/********************************************************************************
		Effect Mini Teresa
 ********************************************************************************/

extern void s_make3teresa(void)
{	
	static  PositionRecord teredata[3] = {
		{    0,  50,   0 },
		{  210, 110, 210 },
		{ -210,  70,-210 },
	};

	int i;
 	StrategyRecord *stp;

	if ( playerMeter.star < OBAKESET_STARCOIN )	s_remove_obj(execstp);
	else {
		for (i=0;i<3;i++){
			stp = s_makeobj_chain(1,teredata[i].Xpos,
								    teredata[i].Ypos,
								    teredata[i].Zpos,
								    execstp,S_teresa,e_miniteresa);
			stp->s[stw_angleY].d = Randomd();
		}
	}

}

/********************************************************************************
		Effect Mini Teresa
 ********************************************************************************/

static void oba_effect(void)
{

	float s;

	if ( oba_alphawork != obj_alpha ){
		if ( oba_alphawork > obj_alpha ){
			obj_alpha += 20;
			if ( oba_alphawork < obj_alpha )	obj_alpha = oba_alphawork;
		} else {
			obj_alpha -= 20;
			if ( oba_alphawork > obj_alpha )	obj_alpha = oba_alphawork;
		}
	}

	s = ( (obj_alpha  / 255.0f) * 0.4 + 0.6 ) * oba_scale;
	s_scale( execstp , s );

}

/********************************************************************************
		=================================
		 obake animation scale moving !!
		=================================
 ********************************************************************************/

static void oba_animation_scale(int flag)
{
	execstp->s[stw_animeangleX].d = sin(oba_sinmoveY)*0x400;

	if ( obj_alpha == 255 || flag == 1 ){
		execstp->map.scale[0] = ( sin(oba_sinmoveY) * 0.08 + oba_scale); 
		execstp->map.scale[1] = (-sin(oba_sinmoveY) * 0.08 + oba_scale);
		execstp->map.scale[2] = execstp->map.scale[0];
		obj_gravity = sin(oba_sinmoveY) * oba_scale;
/*-----------------------------------------------------------------------------------------
        if ( ( obj_worldY - obj_animepositionY - obj_groundY ) < (50*oba_scale) ){
           if ( obj_gravity < 0 ) obj_gravity = -obj_gravity;
        }
-----------------------------------------------------------------------------------------*/
		oba_sinmoveY	+= 0x400;
	}

}

/********************************************************************************
		mode check
 ********************************************************************************/

static int teresa_modecheck(void)
{

//	short 	dangle1 = s_calc_dangle(obj_targetangle,player1stp->s[stw_animeangleY].d);

	short 	dangle1 = s_calc_dangle(obj_targetangle,obj_angleY);
	short 	dangle2 = s_calc_dangle(obj_angleY,player1stp->s[stw_animeangleY].d);
	short 	data2	= 0x5000-15000;
	short 	data3 	= 0x5000+ 7000;
	int		flag 	= 0;

	obj_speedY = 0;

	if ( dangle1 > data2 || dangle2 < data3 ){
		if ( obj_alpha == 40 ){
			oba_alphawork 	= 255;
		 	objsound(NA_SE3_TERESA_APPEAR);
		}
		if ( obj_alpha > 180 )	flag = 1;
	} else {
		if ( obj_alpha == 255 )	oba_alphawork 	= 40;
	}

	return(flag);

}

/********************************************************************************
		=================
		 ob roll
		=================
 ********************************************************************************/

#define	MT_ROLL_TIME			32			/* obake to player ga atatta toki!!		 */
#define	MT_PULU_TIME			(8*2)		/* obake to player ga atatta toki!!		 */
#define	MT_PULU_SPEED			0x400		
#define	MT_ROLL_SPEED_Y			3			/* rolling back speed Y 				 */

#define	TERESA_DAMAGE_ROLL		0
#define	TERESA_ATTACK_ROLL		1

/*------------------------------------------------------------------------------*/

static void obroll_init(int flag)
{
	s_hitOFF();
	Mbitclr(obj_mainflag,stf_YangleSAME);
	oba_stockY = obj_angleY;

	if ( flag )		oba_damageANG = player1stp->s[stw_angleY].d;
	else {
		if	( cos((short)obj_angleY-(short)obj_targetangle) < 0 ){
			oba_damageANG	 = obj_angleY;
		} else {
			oba_damageANG	 = (short)(obj_angleY+0x8000);
		}
	}

}
/*------------------------------------------------------------------------------*/
static void obroll_roll(int flag,float sF)
{
	int 	time;
	time = (obj_timer+1)*( 0x10000/MT_ROLL_TIME );
	obj_speedF  = sF;		
	obj_speedY	= cos(time);
	obj_angleY	= oba_damageANG;
	if ( flag ){
		execstp->s[stw_animeangleY].d += rotdata[obj_timer];
		execstp->s[stw_animeangleZ].d += rotdata[obj_timer];
	}
}
/*------------------------------------------------------------------------------*/
static void obroll_rollend(void)
{
	int	time;
	time = (obj_timer-MT_ROLL_TIME+1) * (0x10000/8);
	execstp->s[stw_animeangleY].d += cos(time) * MT_PULU_SPEED;

}
/*------------------------------------------------------------------------------*/
static void obroll_exit(void)
{
	obj_angleY = oba_stockY;
	Mbitset(obj_mainflag,stf_YangleSAME);
	Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
}

/*------------------------------------------------------------------------------*/
/*	speed ...... roll speed 													*/
/*	flag  ... (0) damage Roll	 												*/
/*			  (1) attack Roll 													*/
/*------------------------------------------------------------------------------*/




static int obake_winstop(float speed)
{

	obake_speedstop();

	if 		( obj_timer == 0 )					obroll_init(0);
	if 		( obj_timer <  MT_ROLL_TIME )		obroll_roll(0,(rotdata[obj_timer]/5000.0f)*speed);
	else {
		s_hitON();
		obroll_exit();
		obj_mode = mode_oba_playersearch;
		return(1);
	}

	return(0);

}

static int obake_roll(float speed)
{

	obake_speedstop();

	if 		( obj_timer == 0 )								obroll_init(1);
	if 		( obj_timer <  MT_ROLL_TIME )					obroll_roll(1,(rotdata[obj_timer]/5000.0f)*speed);
	else if ( obj_timer < (MT_ROLL_TIME)+(MT_PULU_TIME) )	obroll_rollend();
	else {
		s_hitON();
		obroll_exit();
		obj_mode = mode_oba_playersearch;
		return(1);
	}

	return(0);
}


/********************************************************************************
		============
		 obake away
		============
 ********************************************************************************/

#define	MT_REMOVEDEMO_TIME	30

static int obake_hitaway(void)
{

	StrategyRecord *stp;

	if ( obj_timer == 0 ){ 
		obj_speedF 			= 40;
		obj_angleY			= player1stp->s[stw_angleY].d;
		execstp->s[stw_imm].d = 1;	/* set message */
		Mbitclr(execstp->s[stw_flag].d,stf_YangleSAME);
	} else {
		if ( obj_timer == MT_REMOVEDEMO_TIME-25 )	oba_alphawork = 0;
		if ( obj_timer > MT_REMOVEDEMO_TIME || obj_movestatus & MOVESTAT_WALL ){
			s_kemuri();
			execstp->s[stw_imm].d = 2;	/* set message */
			if ( oba_kaidanptr != NULL ){
			 	stp = (StrategyRecord *)oba_kaidanptr;
				if ( s_check_pathname(e_normal_teresa) == 0 ) terecount(stp)++;
			}
			return(1);
		}
	}

	obj_speedY 			= 5;
	obj_animeangleZ += 0x800;
	obj_animeangleY += 0x800;

	return(0);

}


/********************************************************************************

		ob hitcheck

		flag ... 0  safe
				 1  Hit Win  !!
				-1	Hit Lost !!

 ********************************************************************************/

extern int s_check_hitpattern(int data )
{
	if (( obj_mail & 0xff ) == data )	return(1);
	else								return(0);
}

static int oba_hitcheck(void)
{

	int flag = 0;

	if ( (obj_mail & EMAIL_PLAYERHIT) != 0 ){
		if ( ( obj_mail & EMAIL_PLAYERATTACK ) && ( s_check_hitpattern(EMAIL_PLAYER_TRAMP) == 0 ) ){
			s_hitOFF();
			Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
			objsound(NA_SE3_TEREAWAY);
			flag = 1;
		} else	{
			objsound(NA_SE3_TERECRASH);
			Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
			flag = -1;
		}

	}


	return(flag);

}


/********************************************************************************

		obake mainmove program

 ********************************************************************************/


static void obake_mainmove(float ymin,short anglespeed,float chasespeed)
{

 	float	Ydist;
	short	angle;

	if ( teresa_modecheck() ){
	 	s_set_objname(PUNCHATTACK);		/* punch attack enemy */

		if ( s_calc_playerscope() > 1500 ) angle = s_calc_returnangle();
		else							   angle = obj_targetangle;
		s_chase_angleY(angle,anglespeed);

		obj_speedY = 0;
		if(	s_check_playerjump() == 0 ){
			Ydist =  obj_worldY - player_worldY;
			if (  ( ymin < Ydist ) && ( Ydist < 500 ) ){
				obj_speedY = s_chase_set_speedY(obj_worldY,player_worldY+50,10,2);
			}
		}
		s_chase_playerspeed( 10 - oba_randomspeed,chasespeed);

		if ( obj_speedF != 0 ) oba_animation_scale(0);

	} else {
	 	s_set_objname(0);	/* hitcheck off	*/
		obj_speedF  = 0;
		obj_speedY  = 0;
		obj_gravity = 0;
	}

}


/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------*/

#define	OBAKECODE_WAKIDASHI		2

/********************************************************************************
		 ob init 
 ********************************************************************************/

static void oba_init(void)
{

	Mbitset(execstp->status,OBJECT_GHOST);

	if ( obj_programselect == OBAKECODE_WAKIDASHI ) obj_areamap = 10;

	s_copy_initpos();
	obj_angleY = oba_startangle;

	obake_speedstop();	/* speed stop */

	oba_kaidanptr	= (void *)s_find_obj(e_kaidanteresa);	/* kaidan pointer */
	oba_scale 		= 1.0;									/* scale 		  */
	oba_alphawork	= 255;
	if ( obake_appearcheck() ){
		if ( obj_programselect == OBAKECODE_WAKIDASHI ){
			oba_kaidanptr = NULL; 
			obj_mode 	  = mode_oba_datamove;
		} else { 	
			obj_mode = mode_oba_playersearch;
		}
	}
}

/********************************************************************************
		 ob playersearch
 ********************************************************************************/

static void oba_datamove(void)
{

	if ( obj_timer < 30 ){
		obj_speedY = 0;
		obj_speedF = 13;
		oba_animation_scale(0);
		execstp->s[stw_wall_offsetR].f = 0;
	} else {
	 	obj_mode = mode_oba_playersearch;
		execstp->s[stw_wall_offsetR].f = 30;
	}

}

static void oba_playersearch(void)
{

	int		flag;

	if ( obj_timer == 0 ){
		oba_randomspeed = - Randomf()*5;
	 	oba_randomangle	= Randomf()*0x80;
	}
	obake_mainmove(-100,0x180+oba_randomangle,0.5);
	flag = oba_hitcheck();

	if ( teresa_areaout_check() ) obj_mode = mode_oba_init;
	if ( flag == -1 )	obj_mode = mode_oba_roll;
	if ( flag ==  1 )	obj_mode = mode_oba_hitaway;

	if ( flag == 1 ) obj_remove_sound(NA_SE3_MOGURA_HIT);

}

/********************************************************************************
		 oba roll
 ********************************************************************************/

static void oba_roll(void)
{
	if ( obake_winstop(20) ) obj_mode = mode_oba_playersearch;
}

/********************************************************************************
		 oba hitaway
 ********************************************************************************/

static void oba_hitaway(void)
{
	if ( obake_hitaway() ){
		if ( obj_programselect ){
			s_remove_obj(execstp);
		} else {
			obj_mode = mode_oba_endmessage;
			s_allOFF();
		}
	}
}

/********************************************************************************
		 oba hitaway
 ********************************************************************************/

static void oba_endmessage(void)
{

	int code;

	if ( s_find_obj(e_miniteresa) == NULL )	code =	108;
	else									code = 	107;

	if ( s_call_enemydemo(DLOG_LOOKUP,ENEMYDEMO_SETMESSAGE,code,0) ){
		obj_remove_sound(NA_SE3_MOGURA_HIT);
		s_remove_obj(execstp);
		if ( code == 108 ) Na_NazoClearBgm();				
	}

}



/********************************************************************************
	======================
	 mini teresa program
	======================
 ********************************************************************************/

static void *oba_modejmp[] = {
	
	 oba_init,
	 oba_playersearch,
	 oba_roll,
	 oba_hitaway,
	 oba_endmessage,
	 oba_datamove
	
};

/*-----------------------------------------------------------------------------*/
extern void s_oba(void)
{

	s_enemybgcheck();
	s_modejmp(oba_modejmp);
	s_enemymove(78);
	oba_effect();


	if ( s_test_pathname(execstp->motherobj,e_5teresa) ){
		/* remove teresa */
		if ( ( execstp->status ) == 0 ){	
			(execstp->motherobj->s[stw_imm].d)++;
		}					   
	}
	obj_mail = 0;

}

/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/





/********************************************************************************
	======================
	 kaidan teresa program
	======================
 ********************************************************************************/


/*------------------------------------------------------------------------------*/
/*		kaidan teresa initialize												*/
/*------------------------------------------------------------------------------*/
static void kaidanteresa_init(void)
{

	/*========= 3F teresa mode ==========*/
	if ( s_check_pathname(e_3Fteresa) ){
		s_3Fteresa_init();
		terecount(execstp) = 10;
	}

	oba_kaidanptr	= NULL;

	if ( obake_appearcheck() && ( terecount(execstp) >= 5+enemy_p0 ) ){

		obj_mode = mode_kaidanteresa_main;
		s_copy_initpos();
		obj_angleY = oba_startangle;
		s_shape_disp();
		oba_alphawork = 255;
		oba_scale	  = 3.0;
		obj_hp 		  = 3;
		s_set_scale(3.0);
		s_hitON();	

	} else {
		s_shape_hide();
		s_hitOFF();
		obake_speedstop();	/* speed stop */
	}


}

/*------------------------------------------------------------------------------*/
/*		kaidan teresa mainprogram												*/
/*------------------------------------------------------------------------------*/
static void kaidanteresa_main(void)
{
 
	int		flag;
	short	searchspeed;
	float	chasespeed;

	if 		( obj_hp == 3 ){ searchspeed = 0x180; chasespeed = 0.5;	}
	else if ( obj_hp == 2 ){ searchspeed = 0x240; chasespeed = 0.6; }
	else 				   { searchspeed = 0x300; chasespeed = 0.8; }

	obake_mainmove(-100,searchspeed,chasespeed);
	flag = oba_hitcheck();

	if ( s_check_pathname(e_BFteresa) ){
	 	if ( bf_teresa_flag == 0 )	  obj_mode = mode_kaidanteresa_init;
	} else {
		if ( teresa_areaout_check() ) obj_mode = mode_kaidanteresa_init;
	}

	if ( flag == -1 )			  obj_mode = mode_kaidanteresa_roll;
	if ( flag ==  1 )			  obj_mode = mode_kaidanteresa_hitaway;

	if ( flag == 1 ) obj_remove_sound(NA_SE3_STONE);
	

}

/*------------------------------------------------------------------------------*/
/*		kaidan teresa mainprogram												*/
/*------------------------------------------------------------------------------*/
static void kaidanteresa_roll(void)
{
	if ( obake_winstop(20) ) obj_mode = mode_kaidanteresa_main;
}

/*------------------------------------------------------------------------------*/
/*		kaidan teresa mainprogram												*/
/*------------------------------------------------------------------------------*/

/*

static short kaidanstar_rail[] = {
	0,951,160,1253,
	1,951,160,1253,
	2,948,600,883,
	3,969,1045,131,
	4,1240,1045,50,
	-1
};

*/


/*------------------------------------------------------------------------------*/
/*		clearstar make															*/
/*------------------------------------------------------------------------------*/
static void clearstar_make(void)
{
	s_enemyset_star(980,1100,250);
}

/*------------------------------------------------------------------------------*/
/*		3F teresa Clear Star													*/
/*------------------------------------------------------------------------------*/
static void clearstar_3F(void) 
{
	s_enemyset_star(700,3200,1900);
}

/*------------------------------------------------------------------------------*/
/*		BF teresa Clear Star													*/
/*------------------------------------------------------------------------------*/
static void clearstar_BF(void) 
{
	StrategyRecord *stp;

	// s_make_enemystar_entry(0,0);
	s_enemyset_star(-1600,-2100,205);	

	if (  (stp = s_find_obj(e_j_teretrap)) != NULL ){
		stp->s[stw_imm].d = 1;
	}
}

/*------------------------------------------------------------------------------*/
/*		KaidanTeresa HitAway													*/
/*------------------------------------------------------------------------------*/

static void kaidanteresa_hitaway(void)
{

	if ( obj_timer == 0 ) obj_hp--;

	if ( obj_hp == 0 ){
		if ( obake_hitaway() ){
			s_allOFF();
			obj_mode = mode_kaidanteresa_demo;
			s_set_angle(execstp,0,0,0);
			if 		( obj_programselect == 0 ) clearstar_make();    /* kaidan teresa 	*/
			else if	( obj_programselect == 1 ) clearstar_BF();		/* B1F teresa    	*/
			else 							   clearstar_3F();		/*  3F teresa		*/
		}

	} else {
		if ( obj_timer == 0 ){
			s_kemuri();
			oba_scale -= 0.5;	
		}
		if ( obake_roll(40) ) obj_mode = mode_kaidanteresa_main;
	}

}

/*------------------------------------------------------------------------------*/
/*		kaidan teresa demo														*/
/*------------------------------------------------------------------------------*/

static void kaidanteresa_demo(void)
{

	obake_speedstop();

	if ( obj_programselect == 0 ){
		s_set_world(execstp,973,0,626);
		if ( obj_timer > 60 && obj_playerdist < 600 ){
			s_set_world(execstp,973,0,717);
			s_makeobj_chain(0,0,0,   0,execstp,S_movebg00,e_teretrap_kaidan);
			s_makeobj_chain(1,0,0,-200,execstp,S_movebg00,e_teretrap_kaidan);
			s_makeobj_chain(2,0,0, 200,execstp,S_movebg00,e_teretrap_kaidan);
			s_remove_obj(execstp);
		}
	} else {
	 	s_remove_obj(execstp);
	}

/*
	dbErrPrint("sp %d",obj_speedY);
	dbErrPrint("wo %d",obj_worldY);
*/

}

/*-----------------------------------------------------------------------------*/

static void *kaidanteresa_modejmp[] = {
	
	 kaidanteresa_init,
	 kaidanteresa_main,
	 kaidanteresa_roll,
	 kaidanteresa_hitaway,
	 kaidanteresa_demo
	
};

/*-----------------------------------------------------------------------------*/

extern void s_kaidanteresa(void)
{


	s_set_hitparam(execstp,&teresa_hit);	
	obj_animepositionY = 60*oba_scale;
	s_enemybgcheck();
	s_modejmp(kaidanteresa_modejmp);

	s_enemymove(78);

	oba_effect();

	obj_mail = 0;

}

/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/





/********************************************************************************
	======================
	 kago teresa program
	======================
 ********************************************************************************/


/*------------------------------------------------------------------------------*/
/*		kago teresa initialize													*/
/*------------------------------------------------------------------------------*/
static void kagoteresa_init(void)
{
	oba_kaidanptr	= NULL;
	oba_alphawork = 255;
	oba_scale	  = 2;
	s_set_scale(2);
	s_hitON();	
	if ( obake_appearcheck() ) obj_mode = mode_kagoteresa_main;

}

/*------------------------------------------------------------------------------*/
/*		kago teresa mainprogram													*/
/*------------------------------------------------------------------------------*/
static void kagoteresa_main(void)
{
 
	int		flag;
	obake_mainmove(100,0x200,0.5);
	flag = oba_hitcheck();

	if ( teresa_areaout_check() ) obj_mode = mode_kagoteresa_init;
	if ( flag == -1 )			  obj_mode = mode_kagoteresa_roll;
	if ( flag ==  1 )			  obj_mode = mode_kagoteresa_hitaway;

}

/*------------------------------------------------------------------------------*/
/*		kago teresa mainprogram													*/
/*------------------------------------------------------------------------------*/
static void kagoteresa_roll(void)
{
	if ( obake_winstop(20) ) obj_mode = mode_kagoteresa_main;
}


/*------------------------------------------------------------------------------*/
/*		kago teresa mainprogram													*/
/*------------------------------------------------------------------------------*/
static void kagoteresa_hitaway(void)
{

	if ( obake_hitaway() ) s_remove_obj(execstp);

}


/*-----------------------------------------------------------------------------*/

static void *kagoteresa_modejmp[] = {
	
	 kagoteresa_init,
	 kagoteresa_main,
	 kagoteresa_roll,
	 kagoteresa_hitaway
	
};

/*-----------------------------------------------------------------------------*/

extern void s_kagoteresa_makekago(void)
{
	if ( playerMeter.star < OBAKESET_STARCOIN )	s_remove_obj(execstp);
	else {
		StrategyRecord *stp = s_makeobj_nowpos(execstp,S_teresakago,e_teresa_kago);
		stp->s[stw_actorcode].d = execstp->s[stw_actorcode].d;	
	}
}

extern void s_kagoteresa(void)
{

	s_enemybgcheck();
	s_modejmp(kagoteresa_modejmp);
	s_enemymove(78);
	oba_effect();
	obj_mail = 0;
}

/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/

#define		wakiteresa_pointer	(execstp->s[stw_work1].pointer)
#define			wakiteresa_counter	(execstp->s[stw_work2].d)
#define			wakiteresa_nowtime	(execstp->s[stw_work3].d)

extern void s_5teresa(void)
{
	StrategyRecord *stp;

	switch(obj_mode){

		case 0:
			if ( obj_playerdist < 1000 ){
	
				if ( obj_imm < 5 ){
					if ( wakiteresa_counter != 5 ){
						if ( wakiteresa_counter - obj_imm < 2 ){
							s_makeobj_nowpos(execstp,S_teresa,e_kabenuketeresa);
							wakiteresa_counter++;
						}
					}
					obj_mode++;
				}

				if ( obj_imm >= 5 ){
					stp = s_makeobj_nowpos(execstp,S_teresa,e_BFteresa);
					s_copy_actorcode(stp,execstp);
					obj_mode = 2;
				//	Na_FixSeFlagEntry(NA_SE2_CORRECT_CHIME);
					Na_NazoClearBgm();
				}

			} 
			break;
		case 1:
			if ( obj_timer > 60 ) obj_mode = 0;
			break;
		case 2:
			break;
	}

}


/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/

extern void s_3Fteresa_init(void)
{

 	camBOSSstp = execstp;

}

extern void s_obake_fire(void)
{

	 s_debug_position();

}

extern void s_michi_teresa(void)
{

	short 	angle;

	oba_scale = 2.0;

	
	if ( obj_mode == 0 ){
		s_shape_hide();
		if ( playerMeter.star < OBAKESET_STARCOIN )	s_remove_obj(execstp);
		if ( map_areacode == 1 ) obj_mode++;
	} else if ( obj_mode == 1 ){
		s_shape_disp();
	 	obj_alpha = 180;
		if ( obj_timer == 0 ) s_set_scale(oba_scale);
		if ( obj_playerdist < 1000 ){
			obj_mode++;
			objsound(NA_SE3_TERESA_APPEAR);
		}
		obj_speedF = 0;
		angle = obj_targetangle;
	} else {
		s_chase_speedF( 32,1 );
		obj_attX = -1000;
		obj_attZ = -9000;
		angle = s_calc_returnangle();

		if ( obj_worldZ < -5000 ){
			if ( obj_alpha > 0 ) obj_alpha -= 20;
			else				 obj_alpha = 0;
		}
		if ( execstp->status   & OBJECT_AREAOUT )  obj_mode = 1;

	}

	obj_speedY = 0;

	angle = s_calc_returnangle();
	s_chase_angleY(angle,0x800-600);	/* chase angle		*/

	oba_animation_scale(1);			   		/* animation scale 	*/
	s_optionmove_F();



}

/*################*/
#endif
/*################*/
/*===============================================================================
		end end end end end end end end 
===============================================================================*/



