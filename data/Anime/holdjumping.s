/********************************************************************************
						Ultra 64 MARIO Brothers

					mario hold jumping animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 26, 1995
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioHoldJumping:
	.half	 MAP_ANIM_ONETIME 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	10 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x032C						/* total bytes       */


	.half	    1,    0 	/* chn14translate x */
	.half	   10,    1 	/* chn14 translate y */
	.half	   10,   11 	/* chn14 translate z */
	.half	    1,    0 	/* chn14 rotate x */
	.half	    1,   21 	/* chn14 rotate y */
	.half	    1,    0 	/* chn14 rotate z */
	.half	    1,    0 	/* <m_waist>1 rotate x */
	.half	    1,    0 	/* <m_waist>1 rotate y */
	.half	   10,  258 	/* <m_waist>1 rotate z */
	.half	    1,    0 	/* <m_body>1_2 rotate x */
	.half	    1,    0 	/* <m_body>1_2 rotate y */
	.half	   10,  248 	/* <m_body>1_2 rotate z */
	.half	    1,    0 	/* <m_head>1 rotate x */
	.half	    1,    0 	/* <m_head>1 rotate y */
	.half	   10,  238 	/* <m_head>1 rotate z */
	.half	    1,  234 	/* chn6_2 rotate x */
	.half	    1,  235 	/* chn6_2 rotate y */
	.half	    1,  236 	/* chn6_2 rotate z */
	.half	   10,   26 	/* <m_larmA>1 rotate x */
	.half	   10,   36 	/* <m_larmA>1 rotate y */
	.half	   10,   46 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1_1 rotate x */
	.half	    1,    0 	/* <m_larmB>1_1 rotate y */
	.half	    1,   25 	/* <m_larmB>1_1 rotate z */
	.half	    1,   22 	/* <m_lhand>1_1 rotate x */
	.half	    1,   23 	/* <m_lhand>1_1 rotate y */
	.half	    1,   24 	/* <m_lhand>1_1 rotate z */
	.half	    1,  230 	/* chn10_2 rotate x */
	.half	    1,  231 	/* chn10_2 rotate y */
	.half	    1,  232 	/* chn10_2 rotate z */
	.half	   10,   60 	/* <m_rarmA>1 rotate x */
	.half	   10,   70 	/* <m_rarmA>1 rotate y */
	.half	   10,   80 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1_2 rotate x */
	.half	    1,    0 	/* <m_rarmB>1_2 rotate y */
	.half	    1,   59 	/* <m_rarmB>1_2 rotate z */
	.half	    1,   56 	/* <m_rhand>1_1 rotate x */
	.half	    1,   57 	/* <m_rhand>1_1 rotate y */
	.half	    1,   58 	/* <m_rhand>1_1 rotate z */
	.half	    1,    0 	/* chn15_2 rotate x */
	.half	    1,    0 	/* chn15_2 rotate y */
	.half	    1,  233 	/* chn15_2 rotate z */
	.half	   10,  200 	/* <m_llegA>1_2 rotate x */
	.half	   10,  210 	/* <m_llegA>1_2 rotate y */
	.half	   10,  220 	/* <m_llegA>1_2 rotate z */
	.half	    1,    0 	/* <m_llegB>1_2 rotate x */
	.half	    1,    0 	/* <m_llegB>1_2 rotate y */
	.half	   10,  190 	/* <m_llegB>1_2 rotate z */
	.half	   10,  160 	/* <m_ltoot>1_2 rotate x */
	.half	   10,  170 	/* <m_ltoot>1_2 rotate y */
	.half	   10,  180 	/* <m_ltoot>1_2 rotate z */
	.half	    1,    0 	/* chn17_2 rotate x */
	.half	    1,    0 	/* chn17_2 rotate y */
	.half	    1,  237 	/* chn17_2 rotate z */
	.half	   10,  130 	/* <m_rlegA>1_2 rotate x */
	.half	   10,  140 	/* <m_rlegA>1_2 rotate y */
	.half	   10,  150 	/* <m_rlegA>1_2 rotate z */
	.half	    1,    0 	/* <m_rlegB>1_2 rotate x */
	.half	    1,    0 	/* <m_rlegB>1_2 rotate y */
	.half	   10,  120 	/* <m_rlegB>1_2 rotate z */
	.half	   10,   90 	/* <m_rtoot>1 rotate x */
	.half	   10,  100 	/* <m_rtoot>1 rotate y */
	.half	   10,  110 	/* <m_rtoot>1 rotate z */

	.half	0x0000,0x009A,0x009D,0x00A0,0x00A3,0x00A6,0x00A9,0x00AB,0x00AD,0x00AF,0x00B1,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF
	.half	0x0000,0x0000,0x0000,0x0000,0x0000,0x3FFF,0xE7CE,0xF984,0xE96B,0xF446,0x115F,0x1199,0x11D1,0x1206,0x1236,0x1261
	.half	0x1260,0x1236,0x1222,0x1261,0xF317,0xF2E1,0xF2AD,0xF27C,0xF24E,0xF226,0xF21A,0xF22C,0xF23B,0xF226,0x9205,0x91A6
	.half	0x914B,0x90F5,0x90A5,0x905F,0x90A7,0x9160,0x9199,0x905F,0x0ACF,0x020D,0xEDD2,0xE463,0xF545,0xF510,0xF4DC,0xF4AB
	.half	0xF47F,0xF457,0xF453,0xF473,0xF484,0xF457,0x12D7,0x133E,0x13A2,0x1400,0x1456,0x14A2,0x14C5,0x14B6,0x149E,0x14A2
	.half	0x9AB7,0x9AD4,0x9AEF,0x9B0A,0x9B22,0x9B37,0x9BBD,0x9C88,0x9CAB,0x9B37,0x0104,0x0129,0x0148,0x0161,0x0175,0x0185
	.half	0x0196,0x01A3,0x01A2,0x0185,0x0124,0x00DC,0x008F,0x0046,0x0006,0xFFD7,0xFFC9,0xFFD7,0xFFE5,0xFFD7,0xCA40,0xD347
	.half	0xDC35,0xE47E,0xEB96,0xF0F0,0xF414,0xF504,0xF3E9,0xF0F0,0x2F66,0x2839,0x203B,0x184F,0x1158,0x0C39,0x093F,0x0833
	.half	0x0929,0x0C39,0x0390,0x06C5,0x0CC6,0x13F4,0x1AB0,0x1F59,0x2145,0x2162,0x207A,0x1F59,0xFF56,0xFD3E,0xFD4D,0xFE90
	.half	0x0014,0x00E9,0x00B3,0x002B,0x0003,0x00E9,0x9113,0x989B,0xA4B5,0xB295,0xBF6B,0xC86C,0xCC71,0xCCFF,0xCB42,0xC86C
	.half	0xFED3,0xFEE3,0xFEF5,0xFF08,0xFF18,0xFF24,0xFF22,0xFF15,0xFF0F,0xFF24,0x005B,0x0089,0x00AA,0x00C1,0x00D2,0x00E1
	.half	0x00EB,0x00EE,0x00EA,0x00E1,0xCB7A,0xD203,0xD6EF,0xDAA1,0xDD79,0xDFD8,0xE197,0xE23C,0xE1AF,0xDFD8,0x5683,0x59F9
	.half	0x5A92,0x5984,0x5808,0x5754,0x5794,0x57E8,0x57F2,0x5754,0xF457,0xF85D,0xF9ED,0xFA02,0xF994,0xF99C,0xFA2F,0xFA8F
	.half	0xFA76,0xF99C,0xF0E3,0xF103,0xF01A,0xEEAC,0xED3C,0xEC4D,0xEC3B,0xECA0,0xECDC,0xEC4D,0xA297,0x9AD5,0x946E,0x8F36
	.half	0x8B01,0x87A5,0x85AA,0x855D,0x8633,0x87A5,0x8001,0x8001,0x49FF,0xBF5F,0x8001,0x8001,0x4171,0xBF5F,0x1113,0x118B
	.half	0x11FE,0x1267,0x12C3,0x130C,0x1389,0x141B,0x1426,0x130C,0x03DF,0x0286,0xFEE5,0xFA39,0xF5BF,0xF2B5,0xF129,0xF07D
	.half	0xF0ED,0xF2B5,0x4B4B,0x4971,0x452F,0x3FDB,0x3ACA,0x3754,0x3602,0x3607,0x36B3,0x3754
