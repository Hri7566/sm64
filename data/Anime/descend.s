/********************************************************************************
						Ultra 64 MARIO Brothers

					  mario ascent animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							August 31, 1995
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioDescend:
	.half	MAP_ANIM_ONETIME 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	10 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x0420						/* total bytes       */


	.half	   10,    1 	/* chn14_1_1translate x */
	.half	   10,   11 	/* chn14_1_1 translate y */
	.half	   10,   21 	/* chn14_1_1 translate z */
	.half	    1,    0 	/* chn14_1_1 rotate x */
	.half	    1,   31 	/* chn14_1_1 rotate y */
	.half	    1,    0 	/* chn14_1_1 rotate z */
	.half	   10,  360 	/* <m_waist>1_3_1 rotate x */
	.half	   10,  370 	/* <m_waist>1_3_1 rotate y */
	.half	   10,  380 	/* <m_waist>1_3_1 rotate z */
	.half	   10,  330 	/* <m_body>1_1 rotate x */
	.half	   10,  340 	/* <m_body>1_1 rotate y */
	.half	   10,  350 	/* <m_body>1_1 rotate z */
	.half	   10,  300 	/* <m_head>2_1 rotate x */
	.half	   10,  310 	/* <m_head>2_1 rotate y */
	.half	   10,  320 	/* <m_head>2_1 rotate z */
	.half	    1,  296 	/* chn6_1 rotate x */
	.half	    1,  297 	/* chn6_1 rotate y */
	.half	    1,  298 	/* chn6_1 rotate z */
	.half	   10,   72 	/* <m_larmA>1_1 rotate x */
	.half	   10,   82 	/* <m_larmA>1_1 rotate y */
	.half	   10,   92 	/* <m_larmA>1_1 rotate z */
	.half	    1,    0 	/* <m_larmB>1_1 rotate x */
	.half	    1,    0 	/* <m_larmB>1_1 rotate y */
	.half	   10,   62 	/* <m_larmB>1_1 rotate z */
	.half	   10,   32 	/* <m_lhand>1_1 rotate x */
	.half	   10,   42 	/* <m_lhand>1_1 rotate y */
	.half	   10,   52 	/* <m_lhand>1_1 rotate z */
	.half	    1,  292 	/* chn10_1 rotate x */
	.half	    1,  293 	/* chn10_1 rotate y */
	.half	    1,  294 	/* chn10_1 rotate z */
	.half	   10,  142 	/* <m_rarmA>1_1 rotate x */
	.half	   10,  152 	/* <m_rarmA>1_1 rotate y */
	.half	   10,  162 	/* <m_rarmA>1_1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1_1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1_1 rotate y */
	.half	   10,  132 	/* <m_rarmB>1_1 rotate z */
	.half	   10,  102 	/* <m_rhand>1_1 rotate x */
	.half	   10,  112 	/* <m_rhand>1_1 rotate y */
	.half	   10,  122 	/* <m_rhand>1_1 rotate z */
	.half	    1,    0 	/* chn15_1 rotate x */
	.half	    1,    0 	/* chn15_1 rotate y */
	.half	    1,  295 	/* chn15_1 rotate z */
	.half	   10,  262 	/* <m_llegA>1_1 rotate x */
	.half	   10,  272 	/* <m_llegA>1_1 rotate y */
	.half	   10,  282 	/* <m_llegA>1_1 rotate z */
	.half	    1,    0 	/* <m_llegB>1_1 rotate x */
	.half	    1,    0 	/* <m_llegB>1_1 rotate y */
	.half	   10,  252 	/* <m_llegB>1_1 rotate z */
	.half	    1,    0 	/* <m_ltoot>1_1 rotate x */
	.half	    1,    0 	/* <m_ltoot>1_1 rotate y */
	.half	   10,  242 	/* <m_ltoot>1_1 rotate z */
	.half	    1,    0 	/* chn17_1 rotate x */
	.half	    1,    0 	/* chn17_1 rotate y */
	.half	    1,  299 	/* chn17_1 rotate z */
	.half	   10,  212 	/* <m_rlegA>1_1 rotate x */
	.half	   10,  222 	/* <m_rlegA>1_1 rotate y */
	.half	   10,  232 	/* <m_rlegA>1_1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1_1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1_1 rotate y */
	.half	   10,  202 	/* <m_rlegB>1_1 rotate z */
	.half	   10,  172 	/* <m_rfoot>1_1 rotate x */
	.half	   10,  182 	/* <m_rfoot>1_1 rotate y */
	.half	   10,  192 	/* <m_rfoot>1_1 rotate z */

	.half	0x0000,0x0000,0x0001,0x0002,0x0003,0x0004,0x0005,0x0006,0x0006,0x0007,0x0007,0x008F,0x0053,0x001C,0xFFEB,0xFFB2
	.half	0xFF79,0xFF45,0xFF19,0xFEFB,0xFEF0,0xFFF9,0xFFD7,0xFFBE,0xFFB6,0xFFAF,0xFFAB,0xFFA8,0xFFA6,0xFFA5,0xFFA4,0x3FFF
	.half	0x0000,0x1A7D,0xE5DB,0xE7B6,0xEB92,0xFFB0,0xFFCF,0x08C7,0x1312,0x17FE,0xF97C,0xFC7F,0xFB05,0xFB8B,0xF075,0xFFF5
	.half	0xFD62,0xFDC5,0xFEB0,0xFF33,0xED8C,0xE781,0xEEB7,0xF02A,0xF45B,0xEA86,0xEE5D,0xF019,0xF164,0xF1E5,0xE59B,0xBE8B
	.half	0xB500,0xB579,0xC35F,0xD1A4,0xE644,0xEE05,0xF328,0xF503,0x2709,0xDD38,0x909B,0x7CD1,0x7180,0xA62F,0xA560,0xA74C
	.half	0xA9A9,0xAAD0,0xE3DD,0xDA26,0xDE0A,0xD294,0xD673,0xADA6,0xA48F,0x9DF1,0x97FF,0x956B,0xA066,0xD097,0x1FBD,0x3A99
	.half	0x4594,0xF1B7,0xE61A,0xE2E0,0xE14C,0xE0E1,0x0000,0xFD15,0xFC8E,0x057C,0x0C40,0xF617,0xF3FE,0xF32C,0xF315,0xF32C
	.half	0x0000,0x00DA,0x01BA,0xFF08,0x03A9,0x1E80,0x17FB,0x1004,0x0946,0x066E,0xF549,0xF516,0xF4E1,0xF5A5,0xF46D,0xED30
	.half	0xEEFA,0xF128,0xF300,0xF3C7,0xC7DD,0xD03C,0xD4FB,0xCB42,0xC519,0xEAD4,0xF150,0xF597,0xF7F2,0xF8AB,0x054B,0xDEF4
	.half	0xC1FE,0xB48E,0xC9C0,0x4076,0x4787,0x47CC,0x4522,0x4363,0x2BD6,0x1E05,0x1435,0x0DDF,0x1C9D,0x6CFB,0x71DD,0x7226
	.half	0x706C,0x6F45,0xCCF6,0x9EF4,0x7C19,0x6FFE,0x846E,0xE58D,0xE799,0xE421,0xDF20,0xDC92,0x00D4,0x06BC,0x0084,0xFFE8
	.half	0x0051,0x0038,0x001C,0x0003,0xFFF1,0xFFEA,0xEDC1,0xF6F1,0xFF74,0x001E,0xFF5E,0xFF53,0xFF47,0xFF3B,0xFF33,0xFF30
	.half	0x9A3A,0xBBDF,0xABB0,0xAF8E,0xB6BC,0xBC4D,0xC279,0xC831,0xCC67,0xCE0B,0x4FFE,0x51AF,0x4AA3,0x23F3,0x0728,0x0742
	.half	0x0ED1,0x1987,0x231B,0x2741,0x056C,0x028B,0xFAA7,0x05A9,0x1123,0x16B9,0x1BAA,0x1F9F,0x223F,0x2333,0x1EFC,0x3240
	.half	0x21A7,0x293A,0x305D,0x2CC2,0x265C,0x1F3B,0x196F,0x170B,0x87D0,0x5D05,0x8178,0x9169,0x9A13,0x9F71,0xA3DE,0xA733
	.half	0xA94E,0xAA0A,0xD196,0xCE9A,0xCD64,0xD36D,0xD75F,0xD29B,0xCB2D,0xC33A,0xBCE3,0xBA4D,0x2124,0x1E8B,0x1C3B,0x18C7
	.half	0x198E,0x22D0,0x2F66,0x3C38,0x462D,0x4A2F,0xED52,0xF037,0xEF4C,0xDFB0,0xD2CF,0xD42C,0xD91E,0xDF68,0xE4D1,0xE71E
	.half	0xFF67,0xF512,0xEC7C,0xE8B8,0xE795,0xE723,0xE717,0xE743,0xE77E,0xE79C,0xAB34,0xA234,0x9ECA,0xB04C,0xBF18,0xBB1F
	.half	0xB24B,0xA7E8,0x9F42,0x9BA4,0x7FFF,0x7FFF,0x4171,0xBF5F,0x7FFF,0x7FFF,0x4171,0xBF5F,0x3A19,0x3B0D,0x3DBE,0x45E6
	.half	0x4809,0x3B44,0x292F,0x1678,0x07CF,0x01E1,0xF0EF,0xFA6E,0x0547,0x016E,0xFBC8,0xFB8A,0xFC44,0xFD6F,0xFE85,0xFEFF
	.half	0xF4BF,0xF444,0xF405,0xF5B2,0xF697,0xF4D4,0xF233,0xEF6F,0xED3F,0xEC5B,0xCFAA,0xCEFF,0xD2FC,0xE86A,0xFB90,0x001C
	.half	0x01B0,0x016B,0x006E,0xFFDB,0x0EC2,0x100A,0x1158,0x13D4,0x1422,0x0FB5,0x0986,0x0329,0xFE32,0xFC30,0xF8D9,0xF1E2
	.half	0xEE01,0xF4A0,0xFBE0,0xFD31,0xFD69,0xFCFE,0xFC68,0xFC1C,0x5589,0x487B,0x3835,0x1969,0x011F,0xFBCA,0xFAC2,0xFC3E
	.half	0xFE72,0xFF94,0xDE19,0xD19D,0xCAAC,0xD55F,0xE397,0xEC7F,0xF594,0xFD9A,0x0352,0x0580,0x3DF7,0x33C6,0x2DBF,0x35BC
	.half	0x3EE0,0x408E,0x40E7,0x4078,0x3FCE,0x3F76
