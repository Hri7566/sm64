/********************************************************************************
						Ultra 64 MARIO Brothers

					mario punching base animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							February 6, 1996
 ********************************************************************************/

#include "../headers.h"


static short mario_punch_base_prm[]={
	    0,    0,    0,   -1,   -1,    0,  178,  178,
	  148,  143,  148,   22,16383,-1668,-4724,-9364,
	-9364,-9364,-9364,-10494,17444,17444,17444,17444,
	10580,-9625,-9625,-9625,-9625,-9865,-30818,-30818,
	-30818,-30818,-23609, 3759, 4624, 6118, 6592, 4945,
	 1849, 2525, 3738, 4268, 3359,-7307,-7488,-7754,
	-7691,-6991,-8738,-7498,-5146,-3732,-4234,26662,
	25813,23592,18439,23722,11834,10587, 9097,10376,
	 9811,-4845, -550, 5397, 5071, 9676,    0,    0,
	   -1,    0,   12, -208, -208, -208, -208, -205,
	-13864,-13829,-13793,-13864,-14498,18392,18208,17784,
	17316,16696, 4154, 4037, 3715, 3235, 1977, 4260,
	 4161, 3990, 3937, 4532,-17672,-16900,-15381,-14307,
	-15855,    0,-1190,  212,  -60, -528,    0,  -61,
	 -780, -456,  -14,-16576,-16897,-14720,-15507,-16912,
	 5250, 8021,11302,11839,11720,-4748,-3457,-2038,
	-1957,-2174,-4309,-2264,  233, 1003, 1454,-21983,
	-24316,-26993,-27323,-27113,-32767,-32767,16753,-16545,
	-32767,-32767,16753,-16545, 7027, 3471,-3837,-9867,
	-12914,-1550,-1060, -152,  333, -132, 1657,  809,
	 -952,-2453,-3337,-5543,-3771,  -17, 3371, 5927,
	 2308,  673,-2584,-4995,-5493,-3030,-1687, 1043,
	 3216, 3992,-5188,-2805, 1959, 5531, 6431, -549,
	  -19,  972, 1530, 1107,15537,16279,17880,19403,
	20804
};
static short mario_punch_base_tbl[]={
	    5,    1,	/* chn14_2translate x */
	    5,    6,	/* chn14_2 translate y */
	    1,   11,	/* chn14_2 translate z */
	    1,    0,	/* chn14_2 rotate x */
	    1,   12,	/* chn14_2 rotate y */
	    1,    0,	/* chn14_2 rotate z */
	    5,  178,	/* <m_waist>1_3 rotate x */
	    5,  183,	/* <m_waist>1_3 rotate y */
	    5,  188,	/* <m_waist>1_3 rotate z */
	    5,  163,	/* <m_body>1 rotate x */
	    5,  168,	/* <m_body>1 rotate y */
	    5,  173,	/* <m_body>1 rotate z */
	    5,  148,	/* <m_head>2 rotate x */
	    5,  153,	/* <m_head>2 rotate y */
	    5,  158,	/* <m_head>2 rotate z */

	    1,  144,	/* chn6 rotate x */
	    1,  145,	/* chn6 rotate y */
	    1,  146,	/* chn6 rotate z */
	    5,   20,	/* <m_larmA>1 rotate x */
	    5,   25,	/* <m_larmA>1 rotate y */
	    5,   30,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	    5,   15,	/* <m_larmB>1 rotate z */
	    1,    0,	/* <m_lhand>1 rotate x */
	    1,   13,	/* <m_lhand>1 rotate y */
	    1,   14,	/* <m_lhand>1 rotate z */

	    1,  140,	/* chn10 rotate x */
	    1,  141,	/* chn10 rotate y */
	    1,  142,	/* chn10 rotate z */
	    5,   55,	/* <m_rarmA>1 rotate x */
	    5,   60,	/* <m_rarmA>1 rotate y */
	    5,   65,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	    5,   50,	/* <m_rarmB>1 rotate z */
	    5,   35,	/* <m_rhand>1 rotate x */
	    5,   40,	/* <m_rhand>1 rotate y */
	    5,   45,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  143,	/* chn15 rotate z */
	    5,  125,	/* <m_llegA>1 rotate x */
	    5,  130,	/* <m_llegA>1 rotate y */
	    5,  135,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	    5,  120,	/* <m_llegB>1 rotate z */
	    5,  105,	/* <m_ltoot>1 rotate x */
	    5,  110,	/* <m_ltoot>1 rotate y */
	    5,  115,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  147,	/* chn17 rotate z */
	    5,   90,	/* <m_rlegA>1 rotate x */
	    5,   95,	/* <m_rlegA>1 rotate y */
	    5,  100,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	    5,   85,	/* <m_rlegB>1 rotate z */
	    5,   70,	/* <m_rfoot>1 rotate x */
	    5,   75,	/* <m_rfoot>1 rotate y */
	    5,   80,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioPunchBase = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	 5,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_punch_base_prm,
	mario_punch_base_tbl
};
