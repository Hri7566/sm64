/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage8 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE0802_FOG_R			0
#define		STAGE0802_FOG_G			0
#define		STAGE0802_FOG_B			0
#define		STAGE0802_FOG_START		965

#include "sabaku/sabaku_ext_texture.h"
#include "in_pyramid/in_pyramid_ext_texture.h"

/* ------------------------------------------------------------------------------------	*/
/* ------------------------------------------------------------------------------------	*/

#include "sandcone/sand_cone.sou"

/* ------------------------------------------------------------------------------------	*/
/* ------------------------------------------------------------------------------------	*/

#include "sabaku/sabaku_f_shape.sou"
#include "sabaku/sabaku_g_shape.sou"
#include "sabaku/sabaku_r_shape.sou"
#include "sabaku/sabaku_shadow_shape.sou"
#include "sabaku/sa_mobj_01_shape.sou"
#include "sabaku/onimasu.sou"

#include "sabaku/cx0801.flk"
#include "sabaku/cx0801.tag"
#include "sabaku/sa_mobj_01_c.flk"
#include "sabaku/onimasu.flk"

/********************************************************************************/
/*	Water surface records in 8 - 1.												*/
/********************************************************************************/
static short waterSurf_08_01_00[] = {
	1,
	0,   10,  3, -6911,-7167,   -6911,-4607,   -4223,-4607,   -4223,-7167,  1,  150,  0,
};

WaterSurfRec waterSurf_08_01[] = {
		{	 0,	waterSurf_08_01_00	 },
		{	-1,	NULL	}					//	End of WaterSurfRec data.
};

static short waterSurf_08_01_51[] = {
	1,
	0,   40,  2,  1024,-7065,    1024, -716,    7578, -716,    7578,-7065,  1,  150,  0,
};

static short waterSurf_08_01_52[] = {
	1,
	0,   40,  2, -3993,-7065,   -3993,-4197,    1024,-4197,    1024,-7065,  1,  150,  0,
};

WaterSurfRec waterSurf_08_51[] = {
		{	51,	waterSurf_08_01_51	 },
		{	52,	waterSurf_08_01_52	 },
		{	-1,	NULL	}					//	End of WaterSurfRec data.
};


/* ====================================================================================	
		: Sand fall in Sabaku initialize.
=======================================================================================	*/
Gfx sabaku_sandfall_init[] = {
	gsSPBranchList(sandcone0801_init)
};

/* ====================================================================================	
		: Sand fall in Sabaku reset.
=======================================================================================	*/
Gfx sabaku_sandfall_reset[] = {
	gsSPBranchList(sandcone0801_reset)
};

/* ====================================================================================	
		: Sand fall data 1 in "Sabaku". [ Around the pyramid part1 ]
=======================================================================================	*/
short sabaku_sandfall01_pt[] = {
	20,
	-4096, -256,  1024, 	 23, 123, -15,		0,	 0,
		0, -256,  1024, 	-15, 123, -23,		0,	 2,
		0, -256, -3072, 	-23, 123,  15,		0,	 4,
	-4096, -256, -3072, 	 17, 123,  23,		0,	 2,
	-4096, 	  0,  2048, 	 17, 123, -21,		1,	 0,
		0,    0,  2048, 	 -6, 126, -12,		1,	 2,
	 1024, 	  0,  1024, 	-14, 126,   2,		1,	 2,
	 1024, 	  0, -3072, 	 -6, 125, -16,		1,	 4,
		0, 	  0, -4096, 	  7, 125, -15,		1,	 4,
	-4096, 	  0, -4096, 	 10, 125, -12,		1,	 2,
	-5120, 	  0, -3072, 	 14, 126,   4,		1,	 2,
	-5120, 	  0,  1024, 	 12, 126,  -6,		1,	 0
};

Gfx sabaku_sandfall01_draw[] = {
	gsSP1Triangle(0,  4,  1, 0),
	gsSP1Triangle(1,  4,  5, 0),
	gsSP1Triangle(1,  6,  2, 0),
	gsSP1Triangle(2,  6,  7, 0),
	gsSP1Triangle(2,  8,  3, 0),
	gsSP1Triangle(3,  8,  9, 0),
	gsSP1Triangle(0,  3, 10, 0),
	gsSP1Triangle(0, 10, 11, 0),
	gsSPEndDisplayList()
};

/* ====================================================================================	
		: Sand fall data 2 in "Sabaku". [ Around the pyramid part2 ]
=======================================================================================	*/
short sabaku_sandfall02_pt[] = {
	20,
	-4096, -256,  1024, 	 23, 123, -15,		0,	 1,
		0, -256,  1024, 	-15, 123, -23,		0,	 3,
		0, -256, -3072, 	-23, 123,  15,		0,	 5,
	-4096, -256, -3072, 	 17, 123,  23,		0,	 3,
	-4096, 	  0,  2048, 	 17, 123, -21,		1,	 0,
		0,    0,  2048, 	 -6, 126, -12,		1,	 2,
	 1024, 	  0,  1024, 	-14, 126,   2,		1,	 2,
	 1024, 	  0, -3072, 	 -6, 125, -16,		1,	 4,
		0, 	  0, -4096, 	  7, 125, -15,		1,	 4,
	-4096, 	  0, -4096, 	 10, 125, -12,		1,	 2,
	-5120, 	  0, -3072, 	 14, 126,   4,		1,	 2,
	-5120, 	  0,  1024, 	 12, 126,  -6,		1,	 0,
	-4608,	  0,  1536,		 15, 125, -14,		1,	 1,
	  512,	  0,  1536,		-10, 126,  -5,		1,	 3,
	  512, 	  0, -3584,		  1, 125, -15,		1,	 5,
	-4608,	  0, -3584,		 12, 126,  -4,		1,	 3,
};

Gfx sabaku_sandfall02_draw[] = {
	gsSP1Triangle(0, 11, 12, 0),
	gsSP1Triangle(0, 12,  4, 0),
	gsSP1Triangle(1,  5, 13, 0),
	gsSP1Triangle(1, 13,  6, 0),
	gsSP1Triangle(2,  7, 14, 0),
	gsSP1Triangle(2, 14,  8, 0),
	gsSP1Triangle(3,  9, 15, 0),
	gsSP1Triangle(3, 15, 10, 0),
	gsSPEndDisplayList()
};

/* ====================================================================================	
		: Sand fall data 3 in "Sabaku". [ Around the whole field ]
=======================================================================================	*/
short sabaku_sandfall03_pt[] = {
	20,
	-8192, -512,  8704,		 0, 113,  56,	0,	 0,
	-8192, -256,  8192,		 0, 113,  56,	1,	 0,
	-8192,	  0,  7680,		 0, 106,  69,	2,	 0,

		0, -512,  8704,		 0, 113,  56,	0,	 3,
		0, -256,  8192,		 0, 113,  56,	1,	 3,
		0,	  0,  7680,		 2, 122,  34,	2,	 3,

	 8704, -512,  8704,		29, 119,  29,	0,	 6,
	 8192, -256,  8192,		29, 119,  29,	1,	 6,	
	 7680,	  0,  7680,		 6, 126,   6,	2,	 6,

	 8704, -512,	 0,	    56, 113,   0,	0,	 9,
	 8192, -256,  	 0,		56, 113,   0,	1,	 9,
	 7680,	  0,	 0,		20, 125,   0,	2,	 9,

	 8704, -512, -8192,	    56, 113,   0,	0,	12,
	 8192, -256, -8192,		56, 113,   0,	1,	12,
	 7680,	  0, -8192,		43, 119,   0,	2,	12,	
};

Gfx sabaku_sandfall03_draw[] = {
	gsSP1Triangle( 2,  1,  5, 0),
	gsSP1Triangle( 1,  4,  5, 0),
	gsSP1Triangle( 0,  3,  1, 0),
	gsSP1Triangle( 1,  3,  4, 0),
	gsSP1Triangle( 4,  7,  5, 0),
	gsSP1Triangle( 5,  7,  8, 0),
	gsSP1Triangle( 4,  3,  7, 0),
	gsSP1Triangle( 3,  6,  7, 0),
	gsSP1Triangle( 8,  7, 11, 0),
	gsSP1Triangle(11,  7, 10, 0),
	gsSP1Triangle( 7,  9, 10, 0),
	gsSP1Triangle( 7,  6,  9, 0),
	gsSP1Triangle(11, 10, 13, 0),
	gsSP1Triangle(11, 13, 14, 0),
	gsSP1Triangle(10,  9, 13, 0),
	gsSP1Triangle(13,  9, 12, 0),
	gsSPEndDisplayList()
};

/* --------------------------------------------------------------------------------	*/
/* --------------------------------------------------------------------------------	*/

#include "in_pyramid/py_shape.sou"
#include "in_pyramid/py_ami_shape.sou"
#include "in_pyramid/py_amikege_shape.sou"
#include "in_pyramid/py_sn3_shape.sou"
#include "in_pyramid/py_sn3_alp_shape.sou"

#include "in_pyramid/py_obj01_shape.sou"
#include "in_pyramid/py_obj01_ah_shape.sou"
#include "in_pyramid/py_obj02_shape.sou"
#include "in_pyramid/py_obj02_ah_shape.sou"
#include "in_pyramid/py_obj03_shape.sou"
#include "in_pyramid/py_obj04_shape.sou"
#include "in_pyramid/py_obj04_ah_shape.sou"

#include "in_pyramid/cx0802.flk"
#include "in_pyramid/cx0803.flk"
#include "in_pyramid/cx0802.tag"
#include "in_pyramid/cx0803.tag"

#include "in_pyramid/py_obj01_check.flk"
#include "in_pyramid/py_obj02_check.flk"
#include "in_pyramid/py_obj03_check.flk"
#include "in_pyramid/py_obj04_check.flk"

#include "in_pyramid/hand_checkA.flk"
#include "in_pyramid/hand_checkB.flk"
#include "in_pyramid/hand_checkC.flk"
#include "in_pyramid/hand_checkD.flk"

/* ====================================================================================	
		: Sand fall in pyramid initialize. [ opaque ]
=======================================================================================	*/
Gfx sf_08_opa_init[] = {
	gsDPPipeSync(),
	gsDPSetCycleType(G_CYC_2CYCLE),
	gsDPSetRenderMode(G_RM_FOG_SHADE_A, G_RM_AA_ZB_OPA_INTER2),
	gsDPSetDepthSource(G_ZS_PIXEL),
	gsDPSetFogColor(STAGE0802_FOG_R, STAGE0802_FOG_G, STAGE0802_FOG_B, 255),
	gsSPFogPosition(STAGE0802_FOG_START, 1000),
	gsSPSetGeometryMode(G_FOG),
	gsDPSetCombineMode(G_CC_DECALRGB, G_CC_PASS2),
	gsSPClearGeometryMode(G_LIGHTING | G_CULL_BACK),
	gsSPTexture(0xffff, 0xffff, 0, 0, G_ON),
	gs_Tani_SetUpTileDescrip(G_IM_FMT_RGBA, G_IM_SIZ_16b, 32, 32, 0, 0,
							 G_TX_WRAP|G_TX_NOMIRROR, 5, G_TX_NOLOD,
							 G_TX_WRAP|G_TX_NOMIRROR, 5, G_TX_NOLOD),
	gsSPEndDisplayList()
};

Gfx sf_08_opa_reset[] = {
	gsSPTexture(0xffff, 0xffff, 0, 0, G_OFF),
	gsDPPipeSync(),
	gsDPSetCycleType(G_CYC_1CYCLE),
	gsSPSetGeometryMode(G_LIGHTING | G_CULL_BACK),
	gsSPClearGeometryMode(G_FOG),
	gsDPSetCombineMode(G_CC_SHADE, G_CC_SHADE),
	gsDPSetRenderMode(G_RM_AA_ZB_OPA_INTER, G_RM_AA_ZB_OPA_INTER),
	gsSPEndDisplayList()
};

/* ====================================================================================	
		: Sand fall in pyramid initialize. [ translucent ]
=======================================================================================	*/
Gfx sf_08_xlu_init[] = {
	gsDPPipeSync(),
	gsDPSetCycleType(G_CYC_2CYCLE),
	gsDPSetRenderMode(G_RM_FOG_SHADE_A, G_RM_AA_ZB_XLU_INTER2),
	gsDPSetDepthSource(G_ZS_PIXEL),
	gsDPSetFogColor(STAGE0802_FOG_R, STAGE0802_FOG_G, STAGE0802_FOG_B, 255),
	gsSPFogPosition(STAGE0802_FOG_START, 1000),
	gsSPSetGeometryMode(G_FOG),
	gsDPSetEnvColor(255, 255, 255, 180),
	gsDPSetCombineMode(G_CC_DECALRGB_ENVA, G_CC_PASS2),
	gsSPClearGeometryMode(G_LIGHTING | G_CULL_BACK),
	gsSPTexture(0xffff, 0xffff, 0, 0, G_ON),
	gs_Tani_SetUpTileDescrip(G_IM_FMT_RGBA, G_IM_SIZ_16b, 32, 32, 0, 0,
							 G_TX_WRAP|G_TX_NOMIRROR, 5, G_TX_NOLOD,
							 G_TX_WRAP|G_TX_NOMIRROR, 5, G_TX_NOLOD),
	gsSPEndDisplayList()
};

Gfx sf_08_xlu_reset[] = {
	gsSPTexture(0xffff, 0xffff, 0, 0, G_OFF),
	gsDPPipeSync(),
	gsDPSetCycleType(G_CYC_1CYCLE),
	gsSPSetGeometryMode(G_LIGHTING | G_CULL_BACK),
	gsSPClearGeometryMode(G_FOG),
	gsDPSetEnvColor(255, 255, 255, 255),
	gsDPSetCombineMode(G_CC_SHADE, G_CC_SHADE),
	gsDPSetRenderMode(G_RM_AA_ZB_XLU_INTER, G_RM_AA_ZB_XLU_INTER),
	gsSPEndDisplayList()
};

/* ====================================================================================	
		: Waterfall data 1 in pyramid.
=======================================================================================	*/
short waterfall_0801_pt[] = {
		50,
		102, 1229, -742,	 0,	0,
		102, 4275, -742,	 5,	0,
		102, 4300, -768,	 6,	0,
		102, 4300, -870,	 8,	0,
	   -102, 1229, -742,	 0,	1,
	   -102, 4275, -742,	 5,	1,
	   -102, 4300, -768,	 6,	1,
	   -102, 4300, -870,	 8,	1,
};

Gfx waterfall_0801_draw[] = {
	gsSP1Triangle(0, 1, 4, 0),
	gsSP1Triangle(4, 1, 5, 0),
	gsSP1Triangle(1, 2, 5, 0),
	gsSP1Triangle(5, 2, 6, 0),
	gsSP1Triangle(2, 3, 6, 0),
	gsSP1Triangle(6, 3, 7, 0),
	gsSPEndDisplayList()
};

/* ====================================================================================	
		: Waterfall data 2 in pyramid.
=======================================================================================	*/
short waterfall_0802_pt[] = {
		8,
		1178, 1229, 2150,	0,	0,
	   -1741, 1229, 2150,	2,	0,
	   -1741, 1229, -589,	4,	0,
	     154, 1229, -589,   5,	0,
		1178, 1229, 2560,	0,	1,
	   -2150, 1229, 2560,	2,	1,
	   -2150, 1229, -794,	4,	1,
		 154, 1229, -794,	5,	1,
};

/* ====================================================================================	
		: Waterfall data 3 in pyramid.
=======================================================================================	*/
short waterfall_0803_pt[] = {
		50,
		1229,  -307, 2150,	0,	0,
		1229,  1168, 2150,	1,	0,
		1178,  1229, 2150,	2,	0,
		1229,  -307, 2560,	0,	1,
		1229,  1168, 2560,  1,	1,
		1178,  1229, 2560,	2,	1,
};

Gfx waterfall_0803_draw[] = {	
	gsSP1Triangle(0, 1, 3, 0),
	gsSP1Triangle(1, 4, 3, 0),
	gsSP1Triangle(1, 2, 4, 0),
	gsSP1Triangle(2, 5, 4, 0),
	gsSPEndDisplayList()
};
