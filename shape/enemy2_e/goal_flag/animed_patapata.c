/* animed_patapata */
#include<ultra64.h>
#define ShapeColor_animed_patapata(R,G,B) {{R/4,G/4,B/4,0,R/4,G/4,B/4,0},{R,G,B,0,R,G,B,0,0,0,120,0}}
static Lights1 light_animed_patapata[]={
	ShapeColor_animed_patapata(0,110,0),
	ShapeColor_animed_patapata(255,230,87),
	ShapeColor_animed_patapata(172,84,5),
};
static Vtx vtx_animed_patapata0[]={
	{0,0,112,0,0,0,0,127,0,0},
	{288,0,0,0,0,0,0,127,0,0},
	{0,0,-112,0,0,0,0,127,0,0},
};
static Vtx vtx_animed_patapata1[]={
	{0,0,224,0,0,0,0,127,0,0},
	{288,0,112,0,0,0,0,127,0,0},
	{288,0,-112,0,0,0,0,127,0,0},
	{0,0,-224,0,0,0,0,127,0,255},
};
static Vtx vtx_animed_patapata2[]={
	{0,0,360,0,0,0,0,127,0,0},
	{344,0,224,0,0,0,0,127,0,0},
	{344,0,-224,0,0,0,0,127,0,0},
	{0,0,-360,0,0,0,0,127,0,255},
};
static Vtx vtx_animed_patapata3[]={
	{0,0,540,0,0,0,0,127,0,0},
	{460,0,360,0,0,0,0,127,0,0},
	{460,0,-360,0,0,0,0,127,0,0},
	{0,0,-540,0,0,0,0,127,0,255},
};
static Vtx vtx_animed_patapata4[]={
	{460,0,-540,0,0,0,0,127,0,0},
	{0,0,-719,0,0,0,0,127,0,0},
	{0,0,0,0,0,0,0,127,0,0},
	{0,0,720,0,0,0,0,127,0,255},
	{460,0,540,0,0,0,0,127,0,255},
	{460,0,540,0,0,0,0,127,0,0},
	{460,0,-540,0,0,0,0,127,0,0},
	{0,0,0,0,0,0,0,127,0,0},
};
static Vtx vtx_animed_patapata5[]={
	{0,18,57,0,0,0,0,39,120,0},
	{0,-48,35,0,0,0,0,-102,74,0},
	{2800,-18,57,0,0,0,95,-25,79,0},
	{0,-48,-35,0,0,0,0,-102,-74,255},
	{0,18,-57,0,0,0,0,39,-120,255},
	{2800,-18,-57,0,0,0,77,-31,-95,255},
	{0,60,0,0,0,0,0,127,0,255},
	{2800,48,35,0,0,0,45,95,69,255},
	{2800,-60,0,0,0,0,45,-118,0,255},
	{2800,48,-35,0,0,0,77,81,-59,255},
};
Gfx RCP_animed_patapata0[]={
	gsSPLight((&light_animed_patapata[0].l[0]),1),
	gsSPLight((&light_animed_patapata[0].a),2),
	gsSPVertex(&vtx_animed_patapata0[0],3,0),
	gsSP1Triangle(0,1,2,0),
	gsSPEndDisplayList()
};
Gfx RCP_animed_patapata1[]={
	gsSPLight((&light_animed_patapata[0].l[0]),1),
	gsSPLight((&light_animed_patapata[0].a),2),
	gsSPVertex(&vtx_animed_patapata1[0],4,0),
	gsSP1Triangle(0,1,2,0),
	gsSP1Triangle(0,2,3,0),
	gsSPEndDisplayList()
};
Gfx RCP_animed_patapata2[]={
	gsSPLight((&light_animed_patapata[0].l[0]),1),
	gsSPLight((&light_animed_patapata[0].a),2),
	gsSPVertex(&vtx_animed_patapata2[0],4,0),
	gsSP1Triangle(0,1,2,0),
	gsSP1Triangle(0,2,3,0),
	gsSPEndDisplayList()
};
Gfx RCP_animed_patapata3[]={
	gsSPLight((&light_animed_patapata[0].l[0]),1),
	gsSPLight((&light_animed_patapata[0].a),2),
	gsSPVertex(&vtx_animed_patapata3[0],4,0),
	gsSP1Triangle(0,1,2,0),
	gsSP1Triangle(0,2,3,0),
	gsSPEndDisplayList()
};
Gfx RCP_animed_patapata4[]={
	gsSPLight((&light_animed_patapata[1].l[0]),1),
	gsSPLight((&light_animed_patapata[1].a),2),
	gsSPVertex(&vtx_animed_patapata4[0],5,0),
	gsSP1Triangle(0,1,2,0),
	gsSP1Triangle(2,3,4,0),
	gsSPLight((&light_animed_patapata[0].l[0]),1),
	gsSPLight((&light_animed_patapata[0].a),2),
	gsSPVertex(&vtx_animed_patapata4[5],3,0),
	gsSP1Triangle(0,1,2,0),
	gsSPEndDisplayList()
};
Gfx RCP_animed_patapata5[]={
	gsSPLight((&light_animed_patapata[2].l[0]),1),
	gsSPLight((&light_animed_patapata[2].a),2),
	gsSPVertex(&vtx_animed_patapata5[0],10,0),
	gsSP1Triangle(0,1,2,0),
	gsSP1Triangle(3,4,5,0),
	gsSP1Triangle(6,0,7,0),
	gsSP1Triangle(1,3,8,0),
	gsSP1Triangle(4,6,9,0),
	gsSP1Triangle(9,7,2,0),
	gsSP1Triangle(9,2,5,0),
	gsSP1Triangle(2,8,5,0),
	gsSP1Triangle(5,8,3,0),
	gsSP1Triangle(9,5,4,0),
	gsSP1Triangle(7,9,6,0),
	gsSP1Triangle(2,7,0,0),
	gsSP1Triangle(8,2,1,0),
	gsSPEndDisplayList()
};
Gfx light_sourcex[]={
	gsSPNumLights(NUMLIGHTS_1),
	gsSPEndDisplayList()
};
static short animed_patapata_prm[]={
	    0, 4835, 3907, 2463,  721,-1101,-2789,-4125,
	-4928,-5481,-5883,-6121,-6182,-6054,-5723,-5177,
	-4309,-2785, -812, 1339, 3401, 5102, 6174, 6692,
	 6939, 6952, 6769, 6428, 5967, 5423, 3581, 2871,
	 1610,  109,-1318,-2361,-2855,-3215,-3491,-3677,
	-3768,-3758,-3642,-3414,-3068,-2526,-1519, -188,
	 1277, 2685, 3848, 4573, 4906, 5041, 5011, 4851,
	 4594, 4274, 3925,-3009,-3210,-3342,-3401,-3389,
	-3303,-3143,-2907,-2485,-1816, -982,  -59,  871,
	 1731, 2443, 2931, 3205, 3300, 3237, 3037, 2722,
	 2296, 1701,  979,  182, -633,-1414,-2107,-2657,
	-2628,-2534,-2270,-1862,-1337, -720,  -40,  678,
	 1408, 2089, 2618, 3004, 3257, 3391, 3404, 3140,
	 2619, 1925, 1144,  361, -339, -873,-1218,-1535,
	-1829,-2091,-2312,-2481,-2590, 1258, 1203, 1057,
	  844,  592,  327,   75, -137, -342, -573, -807,
	-1022,-1195,-1305,-1328,-1243,-1060, -810, -524,
	 -233,   32,  242,  418,  595,  766,  922, 1058,
	 1164, 1233,16383,-16383,16383,};
static unsigned short animed_patapata_tbl[]={
	    1,    0,	/* chn8translate x */
	    1,    0,	/* chn8 translate y */
	    1,    0,	/* chn8 translate z */
	    1,    0,	/* chn8 rotate x */
	    1,    0,	/* chn8 rotate y */
	    1,    0,	/* chn8 rotate z */
	    1,    0,	/* face11 rotate x */
	    1,    0,	/* face11 rotate y */
	   29,    1,	/* face11 rotate z */
	    1,    0,	/* face10 rotate x */
	    1,    0,	/* face10 rotate y */
	   29,   30,	/* face10 rotate z */
	    1,    0,	/* face9 rotate x */
	    1,    0,	/* face9 rotate y */
	   29,   59,	/* face9 rotate z */
	    1,    0,	/* face8 rotate x */
	    1,    0,	/* face8 rotate y */
	   29,   88,	/* face8 rotate z */
	    1,    0,	/* face7 rotate x */
	    1,    0,	/* face7 rotate y */
	   29,  117,	/* face7 rotate z */
	    1,  146,	/* chn7 rotate x */
	    1,    0,	/* chn7 rotate y */
	    1,  147,	/* chn7 rotate z */
	    1,    0,	/* cyl9 rotate x */
	    1,    0,	/* cyl9 rotate y */
	    1,  148,	/* cyl9 rotate z */
};
AnimeRecord animed_patapata_anm={
	29,
	8,
	animed_patapata_prm,
	animed_patapata_tbl
};
static SkeletonRecord face11[1]=
	{RCP_animed_patapata0,288.000000,0.000000,0.000000,0,0,-812,NULL,NULL};
static SkeletonRecord face10[1]=
	{RCP_animed_patapata1,344.000000,0.000000,0.000000,0,0,-188,NULL,face11};
static SkeletonRecord face9[1]=
	{RCP_animed_patapata2,460.000000,0.000000,0.000000,0,0,3300,NULL,face10};
static SkeletonRecord face8[1]=
	{RCP_animed_patapata3,460.000000,0.000000,0.000000,0,0,1925,NULL,face9};
static SkeletonRecord face7[1]=
	{RCP_animed_patapata4,0.000000,0.000000,0.000000,0,0,-810,NULL,face8};
static SkeletonRecord chn7[1]=
	{NULL,2040.000000,0.000000,0.000000,16383,0,-16383,NULL,face7};
static SkeletonRecord cyl9[1]=
	{RCP_animed_patapata5,0.000000,0.000000,0.000000,0,0,16383,NULL,chn7};
static SkeletonRecord chn8[1]=
	{light_sourcex,0.000000,0.000000,0.000000,0,0,0,NULL,cyl9};
SkeletonPtr animed_patapata_list[]={
	chn8,
	face11,
	face10,
	face9,
	face8,
	face7,
	chn7,
	cyl9,
	NULL
};
