/********************************************************************************
						Ultra 64 MARIO Brothers

					  mario slipping animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 19, 1995
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioSlipping:
	.half	 MAP_ANIM_NORMAL 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	 8 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x021E						/* total bytes       */


	.half	    1,    0 	/* chn14translate x */
	.half	    8,    1 	/* chn14 translate y */
	.half	    1,    0 	/* chn14 translate z */
	.half	    1,    0 	/* chn14 rotate x */
	.half	    1,    9 	/* chn14 rotate y */
	.half	    1,    0 	/* chn14 rotate z */
	.half	    1,    0 	/* <m_waist>1_3 rotate x */
	.half	    1,    0 	/* <m_waist>1_3 rotate y */
	.half	    1,  132 	/* <m_waist>1_3 rotate z */
	.half	    1,    0 	/* <m_body>1 rotate x */
	.half	    1,  130 	/* <m_body>1 rotate y */
	.half	    1,  131 	/* <m_body>1 rotate z */
	.half	    8,  106 	/* <m_head>2 rotate x */
	.half	    8,  114 	/* <m_head>2 rotate y */
	.half	    8,  122 	/* <m_head>2 rotate z */
	.half	    1,  102 	/* chn6 rotate x */
	.half	    1,  103 	/* chn6 rotate y */
	.half	    1,  104 	/* chn6 rotate z */
	.half	    8,   21 	/* <m_larmA>1 rotate x */
	.half	    8,   29 	/* <m_larmA>1 rotate y */
	.half	    8,   37 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1 rotate x */
	.half	    1,    0 	/* <m_larmB>1 rotate y */
	.half	    8,   13 	/* <m_larmB>1 rotate z */
	.half	    1,   10 	/* <m_lhand>1 rotate x */
	.half	    1,   11 	/* <m_lhand>1 rotate y */
	.half	    1,   12 	/* <m_lhand>1 rotate z */
	.half	    1,   98 	/* chn10 rotate x */
	.half	    1,   99 	/* chn10 rotate y */
	.half	    1,  100 	/* chn10 rotate z */
	.half	    8,   61 	/* <m_rarmA>1 rotate x */
	.half	    8,   69 	/* <m_rarmA>1 rotate y */
	.half	    8,   77 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1 rotate y */
	.half	    8,   53 	/* <m_rarmB>1 rotate z */
	.half	    1,    0 	/* <m_rhand>1 rotate x */
	.half	    1,    0 	/* <m_rhand>1 rotate y */
	.half	    8,   45 	/* <m_rhand>1 rotate z */
	.half	    1,    0 	/* chn15 rotate x */
	.half	    1,    0 	/* chn15 rotate y */
	.half	    1,  101 	/* chn15 rotate z */
	.half	    1,    0 	/* <m_llegA>1 rotate x */
	.half	    1,   96 	/* <m_llegA>1 rotate y */
	.half	    1,   97 	/* <m_llegA>1 rotate z */
	.half	    1,    0 	/* <m_llegB>1 rotate x */
	.half	    1,    0 	/* <m_llegB>1 rotate y */
	.half	    1,   95 	/* <m_llegB>1 rotate z */
	.half	    1,   92 	/* <m_ltoot>1 rotate x */
	.half	    1,   93 	/* <m_ltoot>1 rotate y */
	.half	    1,   94 	/* <m_ltoot>1 rotate z */
	.half	    1,    0 	/* chn17 rotate x */
	.half	    1,    0 	/* chn17 rotate y */
	.half	    1,  105 	/* chn17 rotate z */
	.half	    1,   89 	/* <m_rlegA>1 rotate x */
	.half	    1,   90 	/* <m_rlegA>1 rotate y */
	.half	    1,   91 	/* <m_rlegA>1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1 rotate y */
	.half	    1,   88 	/* <m_rlegB>1 rotate z */
	.half	    1,   85 	/* <m_rfoot>1 rotate x */
	.half	    1,   86 	/* <m_rfoot>1 rotate y */
	.half	    1,   87 	/* <m_rfoot>1 rotate z */

	.half	0x0000,0x0036,0x0037,0x003B,0x003C,0x003B,0x0039,0x0037,0x0036,0x3FFF,0xFF8A,0xF980,0xF06F,0xD60A,0xD19A,0xC95D
	.half	0xC4ED,0xC752,0xCCBA,0xD278,0xD5DE,0xA150,0xA55B,0xACDC,0xB0E7,0xAEB8,0xA9CB,0xA490,0xA177,0xCC95,0xCFC7,0xD5B6
	.half	0xD8E8,0xD72E,0xD349,0xCF27,0xCCB4,0x4B26,0x46B3,0x3E70,0x39FD,0x3C65,0x41D0,0x4791,0x4AFA,0xED8C,0xECB3,0xEB21
	.half	0xEA49,0xEABE,0xEBC6,0xECDD,0xED83,0xDBBE,0xD6C7,0xCD8F,0xC898,0xCB46,0xD153,0xD7BF,0xDB8D,0x3127,0x3220,0x33EE
	.half	0x34E7,0x3460,0x3331,0x31F0,0x3131,0x319A,0x2DEE,0x271C,0x2370,0x256C,0x29E5,0x2EA6,0x3176,0x18A1,0x1AB3,0x1E8A
	.half	0x209C,0x1F7E,0x1CF8,0x1A4B,0x18B5,0x001B,0x025B,0xCB25,0x0640,0xFFC5,0xFEB1,0x9C21,0x0095,0xFF09,0xCC42,0x09C7
	.half	0x006C,0xA137,0x8001,0x8001,0x4171,0xBF5F,0x8001,0x8001,0x4171,0xBF5F,0x0000,0x001C,0x0053,0x007A,0x0070,0x0048
	.half	0x001C,0x0001,0x0342,0x033E,0x0338,0x0333,0x0334,0x0338,0x033E,0x0342,0x1815,0x18E8,0x1A85,0x1BA8,0x1B61,0x1A3C
	.half	0x18EC,0x1820,0xFCBE,0x13EC,0x1E68
