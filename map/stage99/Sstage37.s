/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 1 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage37


SEQ_DoStage37:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1, _GfxStage37SegmentRomStart, _GfxStage37SegmentRomEnd)

	seqBeginConstruction()
		seqHmsMario(S_Mario, ShapePlayer1, e_mario)

		seqHmsShape(S_fish			, RCP_HmsEnemyfish			)
		seqHmsShape(S_fishshadow	, RCP_HmsEnemyfish_shadow	)
		seqHmsShape(S_PukuPuku		, RCP_HmsEnemypuku_anim	)

		seqHmsShape(S_bird       , RCP_birdHierarchy	)
		seqHmsShape(S_bird_egg   , RCP_bird_eggHierarchy)

		seqHmsShape(S_red_kame   , RCP_HmsRedNoko		)
		seqHmsShape(S_green_kame , RCP_HmsGreenNoko		)

		seqHmsShape(S_bom        , RCP_HmsEnemybom_basedata )
		seqHmsShape(S_bombfire   , RCP_HmsItembombfire		)
		seqHmsShape(S_bomparts	 , RCP_HmsBomParts			)

		seqGfxShape(S_test3		 , RCP_bar , RM_SURF	)


		seqBeginScene(1, RCP_Stage1Scene1)
			seqActor(S_green_kame	,  -512,    0,   512,  0,0,0,  0,0,0,    e_kame2	)
			seqActor(S_red_kame		,   512,    0,   512,  0,0,0,  0,0,0,    e_kame2	)
			seqActor(S_bom      	,  -200,    0,   512,  0,0,0,  0,0,0,    e_bomhei	)
			seqActor(S_star1		, -6094, 8500, -6094,  0,0,0,  0,0,0,    e_star		)
			seqActor(S_bird_egg		, -1500,    0,  4000,  0,0,0,  0,0,0,    e_bird_egg	)

			seqActor( S_fish		,   100, 1080,   100,  0,  0,  0,  0,0,0, 	e_fish   ) 

			seqActor( S_test3	,  0,0,0  			,	0,0,0, 0,0,0, 	e_bar			)	
			seqActor( S_test3	,  300,50+1024,300  	,	0,0,0, 0,0,0, 	e_bar_swing		)	
			seqActor( S_test3	,  2806,0,763  			,	0,0,0, 0,0,0, 	e_bar			)	

			seqActor(S_coin			,  5534, 1902, -5365,  0,0,0,  4,0,0,    e_coin		)
		    seqActor(S_coin			,  3631, 2534, -4254,  0,0,0,  4,0,0,    e_coin		)
		    seqActor(S_coin			, -1368, 2027, -1353,  0,0,0,  4,0,0,    e_coin		)
		    seqActor(S_coin			, -1111, 2027, -1623,  0,0,0,  4,0,0,    e_coin		)
		    seqActor(S_coin			, -5567, 2652,  2956,  0,0,0,  4,0,0,    e_coin		)
		    seqActor(S_coin			, -3643, 3933,  3437,  0,0,0,  4,0,0,    e_coin		)
		    seqActor(S_coin			, -4211, 5496, -2565,  0,0,0,  4,0,0,    e_coin		)
		    seqActor(S_coin			, -3822, 5496, -2802,  0,0,0,  4,0,0,    e_coin		)
		    seqActor(S_coin			, -3495, 5496, -3054,  0,0,0,  4,0,0,    e_coin		)

			seqPort(1, 1, 2, 2)
			seqPort(2, 1, 1, 3)
			seqPort(3, 1, 2, 1)
			seqMapInfo(stage37_info)
		seqEndScene()

	seqEndConstruction()


	seqEnterMario(1, 0,  0,1024,0)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
