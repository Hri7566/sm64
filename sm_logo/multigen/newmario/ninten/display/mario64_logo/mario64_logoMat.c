/**********************************************************************
 *                                                                    *
 *  Materials for model:                                              *
 *     ./newmario/nin/display/mario64_logo.nin                        *
 *                                                                    *
 *  THIS FILE WAS CREATED BY NINGEN                                   *
 *                                                                    *
 *   12 Materials,    192 bytes                                       *
 *                                                                    *
 **********************************************************************/


#include <mbi.h>

Material mario64_logo_material[] = {
{196,	193,	136,	255,	 /* ambient            */ 
196,	193,	136,	255,	 /* diffuse            */ 
196,	193,	136,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{255,	255,	57,	255,	 /* ambient            */ 
255,	255,	57,	255,	 /* diffuse            */ 
255,	255,	57,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{255,	47,	47,	255,	 /* ambient            */ 
255,	47,	47,	255,	 /* diffuse            */ 
255,	47,	47,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{255,	255,	254,	255,	 /* ambient            */ 
255,	255,	254,	255,	 /* diffuse            */ 
255,	255,	254,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{0,	147,	0,	255,	 /* ambient            */ 
0,	147,	0,	255,	 /* diffuse            */ 
0,	147,	0,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{31,	223,	31,	255,	 /* ambient            */ 
31,	223,	31,	255,	 /* diffuse            */ 
31,	223,	31,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{178,	0,	0,	255,	 /* ambient            */ 
178,	0,	0,	255,	 /* diffuse            */ 
178,	0,	0,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{147,	147,	0,	255,	 /* ambient            */ 
147,	147,	0,	255,	 /* diffuse            */ 
147,	147,	0,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{32,	57,	229,	255,	 /* ambient            */ 
32,	57,	229,	255,	 /* diffuse            */ 
32,	57,	229,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{43,	84,	255,	255,	 /* ambient            */ 
43,	84,	255,	255,	 /* diffuse            */ 
43,	84,	255,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{0,	0,	0,	255,	 /* ambient            */ 
0,	0,	0,	255,	 /* diffuse            */ 
0,	0,	0,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{47,	255,	47,	255,	 /* ambient            */ 
47,	255,	47,	255,	 /* diffuse            */ 
47,	255,	47,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
};
