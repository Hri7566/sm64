/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage9 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"


#define		STAGE09_FOG_R		160
#define		STAGE09_FOG_G		160
#define		STAGE09_FOG_B		160
#define		STAGE09_FOG_START	980

#include "battle_field/battle_field_ext_texture.h"

#include "battle_field/plain3_shape.sou"
#include "battle_field/plain3_flat_shape.sou"
#include "battle_field/plain3_shadow_shape.sou"
#include "battle_field/plain3_saku_shape.sou"
#include "battle_field/plain3_gro_shape.sou"
#include "battle_field/plain3_gro2_shape.sou"

#include "battle_field/pl_moveobj_01_shape.sou"
#include "battle_field/pl_moveobj_02_shape.sou"
#include "battle_field/pl_moveobj_03_shape.sou"

#include "battle_field/cx0901.flk"
#include "battle_field/cx0901.tag"

#include "battle_field/pl_moveobj_01_check.flk"
#include "battle_field/pl_moveobj_02_check.flk"
#include "battle_field/pl_moveobj_03_check.flk"

#include "battle_field/pl_ironball.ral"
#include "battle_field/pl_ironball2.ral"
#include "battle_field/kame_root.ral"
