#include <ultra64.h>
#undef osMotorStart
#undef osMotorStop

s32
osMotorStart(OSPfs *x) {
	__osMotorAccess((x), MOTOR_START);
}

s32
osMotorStop(OSPfs *x) {
	__osMotorAccess((x), MOTOR_STOP);
}

static void
__initialize(void) {
	osInitialize();
}

#undef osInitialize

void
osInitialize(void) {
	__initialize();
}
