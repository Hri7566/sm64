/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage17 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE17_FOG_R		0
#define		STAGE17_FOG_G		0
#define		STAGE17_FOG_B		0
#define		STAGE17_FOG_START	980

#include "ext1_yokoscroll/ext1_yokoscroll_ext_texture.h"

#include "ext1_yokoscroll/e1_base_shape.sou"
#include "ext1_yokoscroll/e1_bg01_shape.sou"
#include "ext1_yokoscroll/e1_bg02_shape.sou"
#include "ext1_yokoscroll/e1_bg03_shape.sou"
#include "ext1_yokoscroll/e1_bg04_shape.sou"
#include "ext1_yokoscroll/e1_bg05_shape.sou"
#include "ext1_yokoscroll/e1_bg06_shape.sou"
#include "ext1_yokoscroll/e1_bg07_shape.sou"
#include "ext1_yokoscroll/e1_bg08_shape.sou"
#include "ext1_yokoscroll/e1_bg09_shape.sou"
#include "ext1_yokoscroll/e1_bg10_shape.sou"
#include "ext1_yokoscroll/e1_bg11_shape.sou"
#include "ext1_yokoscroll/e1_bg12_shape.sou"
#include "ext1_yokoscroll/e1_bg13_shape.sou"
#include "ext1_yokoscroll/e1_bg14_shape.sou"
#include "ext1_yokoscroll/e1_bg15_shape.sou"
#include "ext1_yokoscroll/e1_deru_shape.sou"
#include "ext1_yokoscroll/e1_shiso_shape.sou"
#include "ext1_yokoscroll/e1_ukishima_shape.sou"
#include "ext1_yokoscroll/e1_kurumajiku_shape.sou"
#include "ext1_yokoscroll/e1_kuruma_shape.sou"
#include "ext1_yokoscroll/e1_dorifu0_shape.sou"
#include "ext1_yokoscroll/e1_dorifu1_shape.sou"
#include "ext1_yokoscroll/e1_dorifu2_shape.sou"
#include "ext1_yokoscroll/e1_dorifu3_shape.sou"
#include "ext1_yokoscroll/e1_dorifu4_shape.sou"

#include "ext1_yokoscroll/cx1701.flk"
#include "ext1_yokoscroll/cx1701.tag"

#include "ext1_yokoscroll/e1_deru_check.flk"
#include "ext1_yokoscroll/e1_shiso_check.flk"
#include "ext1_yokoscroll/e1_ukishima_check.flk"
#include "ext1_yokoscroll/e1_kurumajiku_check.flk"
#include "ext1_yokoscroll/e1_kuruma_check.flk"
#include "ext1_yokoscroll/e1_dorifu0_check.flk"
#include "ext1_yokoscroll/e1_dorifu1_check.flk"
#include "ext1_yokoscroll/e1_dorifu2_check.flk"
#include "ext1_yokoscroll/e1_dorifu3_check.flk"
#include "ext1_yokoscroll/e1_dorifu4_check.flk"
