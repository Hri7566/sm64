/********************************************************************************
						Ultra 64 MARIO Brothers

					  mario slipping animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 19, 1995
 ********************************************************************************/

#include "../headers.h"


static short mario_slipping_prm[]={
	    0,   54,   55,   59,   60,   59,   57,   55,
	   54,16383, -118,-1664,-3985,-10742,-11878,-13987,
	-15123,-14510,-13126,-11656,-10786,-24240,-23205,-21284,
	-20249,-20808,-22069,-23408,-24201,-13163,-12345,-10826,
	-10008,-10450,-11447,-12505,-13132,19238,18099,15984,
	14845,15461,16848,18321,19194,-4724,-4941,-5343,
	-5559,-5442,-5178,-4899,-4733,-9282,-10553,-12913,
	-14184,-13498,-11949,-10305,-9331,12583,12832,13294,
	13543,13408,13105,12784,12593,12698,11758,10012,
	 9072, 9580,10725,11942,12662, 6305, 6835, 7818,
	 8348, 8062, 7416, 6731, 6325,   27,  603,-13531,
	 1600,  -59, -335,-25567,  149, -247,-13246, 2503,
	  108,-24265,-32767,-32767,16753,-16545,-32767,-32767,
	16753,-16545,    0,   28,   83,  122,  112,   72,
	   28,    1,  834,  830,  824,  819,  820,  824,
	  830,  834, 6165, 6376, 6789, 7080, 7009, 6716,
	 6380, 6176, -834, 5100, 7784
};
static short mario_slipping_tbl[]={
	    1,    0,	/* chn14translate x */
	    8,    1,	/* chn14 translate y */
	    1,    0,	/* chn14 translate z */
	    1,    0,	/* chn14 rotate x */
	    1,    9,	/* chn14 rotate y */
	    1,    0,	/* chn14 rotate z */
	    1,    0,	/* <m_waist>1_3 rotate x */
	    1,    0,	/* <m_waist>1_3 rotate y */
	    1,  132,	/* <m_waist>1_3 rotate z */
	    1,    0,	/* <m_body>1 rotate x */
	    1,  130,	/* <m_body>1 rotate y */
	    1,  131,	/* <m_body>1 rotate z */
	    8,  106,	/* <m_head>2 rotate x */
	    8,  114,	/* <m_head>2 rotate y */
	    8,  122,	/* <m_head>2 rotate z */

	    1,  102,	/* chn6 rotate x */
	    1,  103,	/* chn6 rotate y */
	    1,  104,	/* chn6 rotate z */
	    8,   21,	/* <m_larmA>1 rotate x */
	    8,   29,	/* <m_larmA>1 rotate y */
	    8,   37,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	    8,   13,	/* <m_larmB>1 rotate z */
	    1,   10,	/* <m_lhand>1 rotate x */
	    1,   11,	/* <m_lhand>1 rotate y */
	    1,   12,	/* <m_lhand>1 rotate z */

	    1,   98,	/* chn10 rotate x */
	    1,   99,	/* chn10 rotate y */
	    1,  100,	/* chn10 rotate z */
	    8,   61,	/* <m_rarmA>1 rotate x */
	    8,   69,	/* <m_rarmA>1 rotate y */
	    8,   77,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	    8,   53,	/* <m_rarmB>1 rotate z */
	    1,    0,	/* <m_rhand>1 rotate x */
	    1,    0,	/* <m_rhand>1 rotate y */
	    8,   45,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  101,	/* chn15 rotate z */
	    1,    0,	/* <m_llegA>1 rotate x */
	    1,   96,	/* <m_llegA>1 rotate y */
	    1,   97,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	    1,   95,	/* <m_llegB>1 rotate z */
	    1,   92,	/* <m_ltoot>1 rotate x */
	    1,   93,	/* <m_ltoot>1 rotate y */
	    1,   94,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  105,	/* chn17 rotate z */
	    1,   89,	/* <m_rlegA>1 rotate x */
	    1,   90,	/* <m_rlegA>1 rotate y */
	    1,   91,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	    1,   88,	/* <m_rlegB>1 rotate z */
	    1,   85,	/* <m_rfoot>1 rotate x */
	    1,   86,	/* <m_rfoot>1 rotate y */
	    1,   87,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioSlipping = {
	 MAP_ANIM_NORMAL,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	 8,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_slipping_prm,
	mario_slipping_tbl
};
