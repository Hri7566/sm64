/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
				Kombu Strategy

				Junuary 25 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
		e_Kombu one
 ********************************************************************************/

e_kombu_one:
	p_initialize(item)
	p_setbit(flag,stf_moveON)
	p_set_pointer(skelanime,RCP_KombuAnime)
	p_set_skelanime_number(0)
	p_program(s_kombu1_init)

kombu_one_main:
	p_while
	p_loop

/********************************************************************************
		e_Kombu
 ********************************************************************************/

e_kombu:
	p_initialize(item)
	p_setbit(flag,stf_moveON)
	p_BGcheckYset
	p_program(s_kombu_init)


kombu_main:
	p_while
	p_loop




/*################*/
#else
/*################*/
/***************************************************************************************************
		C Program
 ****************************************************************************************************/

#define		kombu_flag	(execstp->s[stw_work0].d)

/***************************************************************************************************
						Kombu Main
***************************************************************************************************/
extern void s_kombu1_init(void)
{

	execstp->map.skelanim.frame = (int)(Randomf()*80);

}
/***************************************************************************************************
						Kombu Main
***************************************************************************************************/
extern void s_kombu_init(void)
{

	StrategyRecord *stratp;

	stratp = s_makeobj_nowpos(execstp,S_kombu,e_kombu_one);
	stratp->s[stw_animeangleY].d = 14523;
	stratp->s[stw_animeangleX].d = 5500;
	stratp->s[stw_animeangleZ].d = 9600;
	stratp->map.scale[0] = 1.0;
	stratp->map.scale[1] = 1.0;
	stratp->map.scale[2] = 1.0;

	stratp = s_makeobj_nowpos(execstp,S_kombu,e_kombu_one);
	stratp->s[stw_animeangleY].d = 41800;
	stratp->s[stw_animeangleX].d = 6102;
	stratp->s[stw_animeangleZ].d = 0;
	stratp->map.scale[0] = 0.8;
	stratp->map.scale[1] = 0.9;
	stratp->map.scale[2] = 0.8;
	stratp->map.skelanim.frame = (int)(Randomf()*80);

	stratp = s_makeobj_nowpos(execstp,S_kombu,e_kombu_one);
	stratp->s[stw_animeangleY].d = 40500;
	stratp->s[stw_animeangleX].d = 8700;
	stratp->s[stw_animeangleZ].d = 4100;
	stratp->map.scale[0] = 0.8;
	stratp->map.scale[1] = 0.8;
	stratp->map.scale[2] = 0.8;
	stratp->map.skelanim.frame = (int)(Randomf()*80);

	stratp = s_makeobj_nowpos(execstp,S_kombu,e_kombu_one);
	stratp->s[stw_animeangleY].d = 57236;
	stratp->s[stw_animeangleX].d = 9500;
	stratp->s[stw_animeangleZ].d = 0;
	stratp->map.scale[0] = 1.2;
	stratp->map.scale[1] = 1.2;
	stratp->map.scale[2] = 1.2;
	stratp->map.skelanim.frame = (int)(Randomf()*80);


}



/*################*/
#endif
/*################*/


