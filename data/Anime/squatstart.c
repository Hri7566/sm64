/********************************************************************************
						Ultra 64 MARIO Brothers

					mario squat start animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 19, 1995
 ********************************************************************************/

#include "../headers.h"


static short mario_squat_start_prm[]={
	    0,  170,  163,  140,  112,  105,  105,    0,
	   -7,  -19,  -27,  -32,  -35,16383,    0,    0,
	 1145,    0,-10305,-18320,-1668,-1668,-1344,-1668,
	-4585,-6854,-4724,-4724,-4814,-4724,-3916,-3287,
	-13745,-10767,-8571,-18678,-15547,-11867,-1251,-1156,
	 2187, -401,-26837,18051,-11556,-11700,-12222,-12846,
	-13704,-14243,-11084,-11932,-16996,-18661,-5682, 5159,
	    0,    0,-1027,    0, 9246,16437,    0,    0,
	 -441,    0, 3974, 7065,-2743,-2743,-2834,-2743,
	-1921,-1282,-14371,-11263,-8958,-19411,-15578,-11346,
	 1355, 1915, 1127, 6358,-30561,-8799,11222,11112,
	10533,10235,11201,12049,-13066,-13389,-16590,-15953,
	  433,13464,    0, -254, -512, -109,   -1,   18,
	 -208, -220, -222, -211, -208, -207,-13864,-14138,
	-14298,-14547,-14717,-14803, 7754, 8784,13008,16965,
	17702,17662, 6921, 6268, 3592, 1085,  618,  643,
	 4420, 4527, 4966, 5376, 5453, 5449,-21436,-22951,
	-29158,30559,29477,29535,    0,  597, 1037, -900,
	-1388,-1460,    0,   31,  167,  -48,  -37,    0,
	-16576,-17018,-18603,-18359,-17907,-17619, 9527,11310,
	15763,19119,19644,19540,-10678,-9071,-5562, -660,
	  484,  601,-5055,-5271,-6254,-4986,-4788,-4827,
	-19042,-21432,-26866,32218,30725,30580,-32767,-32767,
	16753,-16545,-32767,-32767,16753,-16545, -926,-1794,
	-5418,-8683,-9444,-9270, 2180, 2917, 5730, 8773,
	11827,13551,16545,16979,18791,20424,20804,20717
};
static short mario_squat_start_tbl[]={
	    1,    0,	/* chn14_3_2translate x */
	    6,    1,	/* chn14_3_2 translate y */
	    6,    7,	/* chn14_3_2 translate z */
	    1,    0,	/* chn14_3_2 rotate x */
	    1,   13,	/* chn14_3_2 rotate y */
	    1,    0,	/* chn14_3_2 rotate z */
	    1,    0,	/* <m_waist>1_3 rotate x */
	    1,    0,	/* <m_waist>1_3 rotate y */
	    6,  202,	/* <m_waist>1_3 rotate z */
	    1,    0,	/* <m_body>1 rotate x */
	    1,    0,	/* <m_body>1 rotate y */
	    6,  196,	/* <m_body>1 rotate z */
	    1,    0,	/* <m_head>2 rotate x */
	    1,    0,	/* <m_head>2 rotate y */
	    6,  190,	/* <m_head>2 rotate z */

	    1,  186,	/* chn6 rotate x */
	    1,  187,	/* chn6 rotate y */
	    1,  188,	/* chn6 rotate z */
	    6,   38,	/* <m_larmA>1 rotate x */
	    6,   44,	/* <m_larmA>1 rotate y */
	    6,   50,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	    6,   32,	/* <m_larmB>1 rotate z */
	    6,   14,	/* <m_lhand>1 rotate x */
	    6,   20,	/* <m_lhand>1 rotate y */
	    6,   26,	/* <m_lhand>1 rotate z */

	    1,  182,	/* chn10 rotate x */
	    1,  183,	/* chn10 rotate y */
	    1,  184,	/* chn10 rotate z */
	    6,   80,	/* <m_rarmA>1 rotate x */
	    6,   86,	/* <m_rarmA>1 rotate y */
	    6,   92,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	    6,   74,	/* <m_rarmB>1 rotate z */
	    6,   56,	/* <m_rhand>1 rotate x */
	    6,   62,	/* <m_rhand>1 rotate y */
	    6,   68,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  185,	/* chn15 rotate z */
	    6,  164,	/* <m_llegA>1 rotate x */
	    6,  170,	/* <m_llegA>1 rotate y */
	    6,  176,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	    6,  158,	/* <m_llegB>1 rotate z */
	    6,  140,	/* <m_ltoot>1 rotate x */
	    6,  146,	/* <m_ltoot>1 rotate y */
	    6,  152,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  189,	/* chn17 rotate z */
	    6,  122,	/* <m_rlegA>1 rotate x */
	    6,  128,	/* <m_rlegA>1 rotate y */
	    6,  134,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	    6,  116,	/* <m_rlegB>1 rotate z */
	    6,   98,	/* <m_rfoot>1 rotate x */
	    6,  104,	/* <m_rfoot>1 rotate y */
	    6,  110,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioSquatStart = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	 6,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_squat_start_prm,
	mario_squat_start_tbl
};
