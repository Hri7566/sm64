/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 5 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage05


/* ====================================================================================
		: Scene 1 [ Yukiyama1 ] Move BGs sequence.
=======================================================================================	*/
SEQ_Stage0501_MoveBGs:

		seqActor(S_movebg01,  531,-4430,6426,  0,0,0,  7,18,0,  e_linelift)

		seqReturn


/* ====================================================================================
		: Scene 1 [ Yukiyama1 ] Enemies sequence.
=======================================================================================	*/
SEQ_Stage0501_Enemies:

		seqActor(S_ping,   	   2650,-3735, 3970,   0,   0,0,  0,1,0,  e_ping_kodomo )
		seqActor(S_ping,   	   -555, 3470,-1000,   0,   0,0,  0,0,0,  e_ping_kodomo )

		seqActor(S_shadow_snowman, -2376,-1589, 4256,   0, 252,0,  0,1,0,  e_snowman )
		seqActor(S_shadow_snowman,  -394,-1589, 4878,   0,  74,0,  0,1,0,  e_snowman )

		seqLevelActor(0x10,S_snow_rock,  2560, 2662,-1122,  0,  0,0,  0,0,0,  e_snowrock_rail)

		seqReturn


/* ====================================================================================
		: Scene 1 [ Yukiyama1 ] Stars sequence.
=======================================================================================	*/
SEQ_Stage0501_Stars:

		seqLevelActor(0x003f,S_NULL, 	  2665,-4607, 4525,  0,   0,0,  0,0,0,  e_house_star 	    )
		seqLevelActor(0x003f,S_ping    ,  3450,-4700, 4550,  0,   0,0,  1,0,0,  e_ping_oya		    )	/*	sita pen	*/
		seqLevelActor(0x003f,S_NULL    ,  4200, -927,  400,  0,   0,0,  3,0,0,  e_tripstar_getcoins )	/*	8 coin 		*/
		seqLevelActor(0x003f,S_movebg02, -4230,-1169, 1813,  0, 270,0,	4,0,0,	e_snowball_head	    )	/*	ukidama		*/
		seqLevelActor(0x003f,S_polystar, -2000,-2200,-3000,  0,   0,0,  5,0,0,  e_tripstar		    )	/*	kabe kick 	*/

		seqReturn


/* ====================================================================================
		: Scene 2 [ Slider ] Stars sequence.
=======================================================================================	*/
SEQ_Stage0502_Stars:

		seqLevelActor(0x003e,S_ping,   	 -4952,  6656, -6075,  0, 270,0,  2,0,0,  e_racepengin )	/* pen slider*/

		seqActor(S_NULL,   	  -6500, -5836, -6400,   0,  0,0,  0,0,0,  e_pengingoal  )
		seqActor(S_NULL,   	  -6393,  -716,  7503,   0,  0,0,  0,0,0,  e_penginjudge )
		seqActor(S_NULL,   	  -4943,  1321,   667,   0,  0,0,  0,0,0,  e_slider_se   )

		seqReturn


/* ====================================================================================
		: Stage 5 main sequence.
=======================================================================================	*/
SEQ_DoStage05:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage5SegmentRomStart	  , _GfxStage5SegmentRomEnd		)
	seqLoadText(SEGMENT_TEXTURE	  , _FSnowMtTextureSegmentRomStart, _FSnowMtTextureSegmentRomEnd)
	seqLoadPres(SEGMENT_WEATHER	  , _WeatherSegmentRomStart		  , _WeatherSegmentRomEnd		)
	seqLoadPres(SEGMENT_BACK   	  , _BackSnowtowerSegmentRomStart , _BackSnowtowerSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_gSegmentRomStart	  , _GfxEnemy1_gSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_gSegmentRomStart	  , _HmsEnemy1_gSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_gSegmentRomStart	  , _GfxEnemy2_gSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_gSegmentRomStart	  , _HmsEnemy2_gSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_g)
		seqCall(SetEnemy2_g)

		seqHmsShape(S_bg01, RCP_HmsYm1Ie1	)
		seqHmsShape(S_bg02, RCP_HmsYm1Ie2	)
		seqHmsShape(S_bg03, RCP_HmsYm1Dai	)
		seqHmsShape(S_bg04, RCP_HmsYm1Hasira)
		seqHmsShape(S_bg05, RCP_HmsYm1Ura	)

		seqHmsShape(S_tripdoor1, RCP_HmsMainDoor_OneWay	)
		seqHmsShape(S_tree3	   , RCP_HmsTree03 			)	
		seqHmsShape(S_movebg01 , RCP_HmsYukiyamaRift	)

		seqHmsShape(S_snow_rock, RCP_HmsEnemySnow_rock	)
		seqHmsShape(S_movebg02 , RCP_HmsEnemySnow_head	)

		seqBeginScene(1, RCP_Stage5Scene1)
			seqActor(S_NULL,  -1512,2560+START_H,-2305,  0, 140,0,   0,10,0,  e_player_entpict)
			seqActor(S_NULL,   -181,        2918,-1486,	 0,   0,0,  15,30,0,  e_tripchimney2  )
			seqActor(S_NULL,  -1847,	    2815, -321,  0,-158,0,   0,31,0,  e_warp)
			seqActor(S_NULL,   3349,	   -4694, -183,  0, -34,0,   0,32,0,  e_warp)
			seqPort(10, 5, 1, 10)					/*	MARIO stage in.	*/
			seqPort(20, 5, 2, 20)					/*	Trip door.		*/
			seqPort(30, 5, 2, 10)					/*	Trip chimney.	*/
			seqPort(31, 5, 1, 32)					/*	Warp.			*/
			seqPort(32, 5, 1, 31)					/*	Warp.			*/
			seqGameClear(6, 1,  51)
			seqGameOver (6, 1, 101)
			seqCall(SEQ_Stage0501_MoveBGs)
			seqCall(SEQ_Stage0501_Enemies)
			seqCall(SEQ_Stage0501_Stars)
			seqMapInfo(cx0501_info)
			seqTagInfo(cx0501_info_tag)
			seqMessage(SEQ_MESG_ENTRANT, 48)
			seqSetMusic(NA_STG_MOUNTAIN, NA_SNOW_BGM)
			seqEnvironment(ENV_SNOWMT)
		seqEndScene()

		seqBeginScene(2, RCP_Stage5Scene2)
			seqActor(S_NULL,  -5836,7465,-6143,  0,90,0,  0,10,0,  e_player_landing)
			seqPort(20, 5, 1, 20)					/*	Trip door.	*/
			seqPort(10, 5, 2, 10)					/*	Enter from the above chimney.	*/
			seqGameClear(6, 1,  51)
			seqGameOver (6, 1, 101)
			seqCall(SEQ_Stage0502_Stars)
			seqMapInfo(cx0502_info)
			seqTagInfo(cx0502_info_tag)
			seqSetMusic(NA_STG_CASTLE, NA_SLIDER_BGM)
			seqEnvironment(ENV_SLIDER)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 140, -1512,2560,-2305)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
