/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Strategy

				Feburary 31 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
						Effect Path Data
 ********************************************************************************/


/********************************************************************************/
/*		Water Buble for Item													*/
/********************************************************************************/
e_item_buble:
	p_initialize(effect)
	p_setbit(flag,stf_moveON | stf_FspeedON | stf_YspeedON )
	p_softspritemodeON
 	p_setbit(flag,stf_moveON)
	p_animereset
	p_program(s_item_buble_init)

	p_setf_random(speedY,3,6)
	p_setangle_random(angleY,0,0)

	p_wait(1)
	p_while
		p_program(s_item_buble)
	p_loop

/*-----------------------------------------------------------------------------*/

e_item_ripple:
	p_initialize(effect)
	p_setbit(flag,stf_moveON)
	p_setf(animeangleX,0)
	p_setf(animeangleY,0)
	p_setf(animeangleZ,0)
	p_animereset
	p_program(s_item_ripple_init)		/* PlayerRipple Initialize */

	p_animeinc
	p_wait(6)

	p_while
		p_program(s_item_ripple_main)	
		p_animeinc
		p_do(6)
			p_program(s_item_ripple_main)	
		p_next	
	p_loop

/*--------------------------------------------------------------------------------------------*/

e_bombfire:
	p_initialize(plfire)
	p_setbit(flag,stf_moveON | stf_playerdistON)
	p_softspritemodeON
	p_setobjname(DAMAGE)
	p_setd(ap,2)
	p_hitON
	p_sethitbox2(150,150,150)
	p_animereset
	p_program(s_bom_fire_init)

	p_while
		p_program(s_bom_fire)
		p_animeinc
	p_loop

/*--------------------------------------------------------------------------------------------*/

e_bom_smoke:
	p_initialize(effect)
	p_setbit(flag,stf_moveON | stf_FspeedON | stf_YspeedON )
	p_softspritemodeON
	p_animereset
	p_program(s_bomsmoke_init)

	p_wait(1)
	p_while
		p_program(s_samplesmoke_main)
		p_animeinc
	p_loop

/*--------------------------------------------------------------------------------------------*/

e_iwa_smoke:
	p_initialize(effect)
	p_setbit(flag,stf_moveON | stf_FspeedON | stf_YspeedON )
	p_softspritemodeON
	p_animereset

	p_wait(1)
	p_while
		p_program(s_samplesmoke_main)
		p_animeinc
	p_loop




/*------------------------------------------------------------------------------*/
e_fire_bubble:
	p_initialize(option)
	p_softspritemodeON
	p_setbit(flag,stf_moveON)
	p_program(s_fire_bubble_init)
	p_addf_random(worldX,-50,100)	/* Xposition Random */
	p_addf_random(worldY,-50,100)	/* Zposition Random */
	p_addf_random(worldZ,-50,100)	/* Zposition Random */
	p_jsr(move_f_bubble)			/* 1 Set */

	p_wait(1)
	p_while
		p_jsr(move_f_bubble)		
		p_program(s_fire_bubble_event)		
	p_loop

move_f_bubble:
	p_addf_random(worldX,-2,4)
	p_addf_random(worldZ,-2,4)
	p_rts
/*--------------------------------------------------------------------------------------------*/

e_reset:
	p_initialize(option)
	p_setbit(flag,stf_moveON)

	p_while
		p_program(s_reset_main)
	p_loop





/*################*/
#else
/*################*/


/***************************************************************************************************
		C Program
 ****************************************************************************************************/


/***************************************************************************************************
						Water Buble Event
***************************************************************************************************/

extern void s_item_buble_init(void)
{

	execstp->s[stw_worldX].f +=  Randomf()*30;
	execstp->s[stw_worldY].f +=  Randomf()*30;
	execstp->s[stw_worldZ].f +=  Randomf()*30;
}



extern void s_item_buble(void)
{

	StrategyRecord *stratp;
	float waterY = mcWaterCheck(execstp->s[stw_worldX].f,execstp->s[stw_worldZ].f); 
	float bgY = execstp->s[stw_worldY].f;

	if ( bgY > waterY ){
		if (stratfree.next != NULL) {
			stratp = s_makeobj(execstp,0,S_rippleA,e_dropripple);
			stratp->s[stw_worldX].f = execstp->s[stw_worldX].f;
			stratp->s[stw_worldY].f = bgY + 5;
			stratp->s[stw_worldZ].f = execstp->s[stw_worldZ].f;
		}
		execstp->status = 0;		/* s_removeobj */	/* object remove */

	}

}
/********************************************************************************/
/*		item ripple 															*/
/********************************************************************************/
extern void s_item_ripple_init(void)
{

	execstp->s[stw_worldY].f = mcWaterCheck(execstp->s[stw_worldX].f,execstp->s[stw_worldZ].f);
}

extern void s_item_ripple_main(void)
{

    ulong counter = frameCounter;

	/*---- End Check ----*/
	

	if ((counter & 0x00000f) == 0x00)  	{
		execstp->status = 0;	/* Shape Remove */
	}
}



/***************************************************************************************************/
/***************************************************************************************************
						Bomb Fire Event
***************************************************************************************************/
extern void s_bom_fire_init(void)
{
	obj_remove_sound(NA_SE2_KIRAI);  //SOUND
	Viewshake(VS_SMALL);
	execstp->s[stw_alpha].d = 255;

}
/***************************************************************************************************
						Bomb Fire Event
***************************************************************************************************/
extern void s_bom_fire(void)
{
	int i;

	if (obj_timer == 9) {		

		if((mcWaterCheck(execstp->s[stw_worldX].f,execstp->s[stw_worldZ].f)) > execstp->s[stw_worldY].f) {	
			for (i=0; i<40; i++) s_makeobj_nowpos(execstp,S_waterdrop,e_fire_bubble);
		}
		else {
			s_makeobj_nowpos(execstp,S_dust,e_bom_smoke);
		}

		execstp->status = 0;		/* s_removeobj */	/* object remove */
	}


	(execstp->s[stw_alpha].d) -= 14;
		
	s_set_scale((float)obj_timer/9+1.0);		
	

}
/***************************************************************************************************
						Bom Smoke Init
***************************************************************************************************/
extern void s_bomsmoke_init(void)
{

	execstp->s[stw_worldY].f -=  300;
	s_set_scale(10.0);		


}

/***************************************************************************************************
						Water Bomb Buble Init
***************************************************************************************************/
extern void s_fire_bubble_init(void)
{
	stSetScale(execstp,2.0,2.0,1.0);
	execstp->s[stw_work4].d = (int)(Randomf()*0x800)+0x800;
	execstp->s[stw_work5].d = (int)(Randomf()*0x800)+0x800;

	obj_timer  = (int)(Randomf()*10);
	obj_speedY = (int)(Randomf()*4)+4;

}
/***************************************************************************************************
						Water Bomb Buble Event
***************************************************************************************************/
extern void s_fire_bubble_event(void)
{
	float water = playerWorks[0].water;

	/*------ scale Animation ---------*/

	execstp->map.scale[0] = sin(execstp->s[stw_work2].d)*0.5+2.0;
	execstp->s[stw_work2].d += execstp->s[stw_work4].d;

	execstp->map.scale[1] = sin(execstp->s[stw_work3].d)*0.5+2.0;
	execstp->s[stw_work3].d += execstp->s[stw_work5].d;


	/*------ water check -----------*/

 	if ( execstp->s[stw_worldY].f > water ){
		execstp->status = 0;
		execstp->s[stw_worldY].f += 5;
		s_makeobj_nowpos( execstp,S_rippleA,e_rippleA );	/* make water ripple */
	}

	if (obj_timer > FRAME*2) {
		execstp->status = 0;
	}

	execstp->s[stw_worldY].f += execstp->s[stw_speedY].f;
	obj_timer++;
}

/*================================================================================================*/
/*				Obj Reset Event
/*
/*================================================================================================*/
/***************************************************************************************************
						Reset
***************************************************************************************************/
extern void s_reset_main(void)
{

	if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,(execstp->s[stw_work1].f))==0) {

		StrategyRecord* stp = s_makeobj_nowpos(execstp,(int)(execstp->s[stw_work0].d),(unsigned long*)(execstp->s[stw_work2].d));
		stp->s[stw_actorcode].d =  execstp->s[stw_actorcode].d;
		execstp->status = 0;

	}

}
/***************************************************************************************************
						Reset
***************************************************************************************************/
extern void Obj_reset(int shape,unsigned long *pathpointer,int dist)
{

	StrategyRecord* stp = s_makeobj_absolute(execstp,0,S_NULL,e_reset,obj_attX,obj_attY,obj_attZ,0,0,0);	

//	stp->s[stw_programselect].d = obj_programselect;
	stp->s[stw_actorcode].d =  execstp->s[stw_actorcode].d;
	stp->s[stw_work0].d = shape;
	stp->s[stw_work1].f = dist;
	stp->s[stw_work2].d = (long)pathpointer;


}


/*################*/
#endif
/*################*/


