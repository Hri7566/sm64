/********************************************************************************
	ground.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	February 3, 1996
 ********************************************************************************/

#ifndef GROUND_H
#define	GROUND_H


static char groundCondition[7][6] = {

/** ENV_PLAIN **/
	NA_ON_GRND >> 16,
	NA_ON_CONC >> 16,
	NA_ON_GRAS >> 16,
	NA_ON_GRAS >> 16,
	NA_ON_GRAS >> 16,
	NA_ON_GRND >> 16,

/** ENV_ROCKMT **/
	NA_ON_CONC >> 16,
	NA_ON_CONC >> 16,
	NA_ON_CONC >> 16,
	NA_ON_CONC >> 16,
	NA_ON_GRAS >> 16,
	NA_ON_GRAS >> 16,

/** ENV_SNOWMT **/
	NA_ON_SNOW >> 16,
	NA_ON_ICE  >> 16,
	NA_ON_SNOW >> 16,
	NA_ON_ICE  >> 16,
	NA_ON_CONC >> 16,
	NA_ON_CONC >> 16,

/** ENV_DESERT **/
	NA_ON_SAND >> 16,
	NA_ON_CONC >> 16,
	NA_ON_SAND >> 16,
	NA_ON_SAND >> 16,
	NA_ON_CONC >> 16,
	NA_ON_CONC >> 16,

/** ENV_HOUSE **/
	NA_ON_FLOOR >> 16,
	NA_ON_FLOOR >> 16,
	NA_ON_FLOOR >> 16,
	NA_ON_FLOOR >> 16,
	NA_ON_CONC  >> 16,
	NA_ON_CONC  >> 16,

/** ENV_WATER **/
	NA_ON_GRND >> 16,
	NA_ON_CONC >> 16,
	NA_ON_GRAS >> 16,
	NA_ON_ICE  >> 16,
	NA_ON_CONC >> 16,
	NA_ON_ICE  >> 16,

/** ENV_SLIDER **/
	NA_ON_CONC >> 16,
	NA_ON_CONC >> 16,
	NA_ON_CONC >> 16,
	NA_ON_CONC >> 16,
	NA_ON_ICE  >> 16,
	NA_ON_ICE  >> 16,
};

#endif
