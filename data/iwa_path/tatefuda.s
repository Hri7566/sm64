/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					  Tatefuda  Strategy

					Feburary 3 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
					e_Tatefuda
 ********************************************************************************/

e_tatefuda:
	p_initialize(moveBG)
	p_setbit( flag,stf_moveON)
	p_setshapeinfo(tatefuda_check_info)
	p_setobjname(FRIEND)
	p_setd(enemyinfo,ENEMYINFO_KANBAN)
	p_BGcheckYset
	p_sethitbox(150,80)
	p_setd(work0,0)

	p_while
		p_hitON
		p_program(stMainMoveBG)
		p_setd(mail,0)
	p_loop

e_kanban:
	p_initialize(moveBG)
	p_setbit( flag,stf_moveON)
	p_setobjname(FRIEND)
	p_setd(enemyinfo,ENEMYINFO_KANBAN)
	p_sethitbox(150,80)
	p_setd(work0,0)

	p_while
		p_hitON
		p_setd(mail,0)
	p_loop


/*################*/
#else
/*################*/


/***************************************************************************************************
		C Program
 ****************************************************************************************************/
#define tatefuda_messNo		(execstp->s[stw_programselect].d)
#define tatefuda_trig		(execstp->s[stw_work0].d)


















#if 0
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_tatefuda_main(void)
{


	switch(obj_mode) {

		case 0: if ( s_hitcheck(execstp,player1stp) == 1  ){
					if (tatefuda_trig == 0) {
						tatefuda_trig = 1; 
						obj_timer = 0;
						Mbitset(execstp->s[stw_flag].d,stf_playerangleON);
					}

					if (tatefuda_trig == 1 &&  ShapeSameAngle(obj_angleY,player_angleY+0x8000,0x2000)==1 
							   &&  ShapeSameAngle(obj_angleY,obj_targetangle     ,0x800)==1) {
							
							obj_mode = 1;
					}
				}
				else {
					tatefuda_trig = 0;
					Mbitclr(execstp->s[stw_flag].d,stf_playerangleON);
				}				
				break;

		case 1: if (obj_timer == 0) {
					ViewingCam(VIEW_ON);
				}
				if (obj_timer == 20) {
					CallMessageFuda(tatefuda_messNo);
					obj_mode = 2;
				}
				break;

		case 2: if ( s_hitcheck(execstp,player1stp) == 0  ){
					tatefuda_trig = 0;
					Mbitclr(execstp->s[stw_flag].d,stf_playerangleON);
					obj_mode = 0;
				}
				break;					

	}

		
}

#endif



/*################*/
#endif
/*################*/


