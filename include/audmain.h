/********************************************************************************
	audmain.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	May 3, 1996
 ********************************************************************************/

#ifndef AUDMAIN_H
#define	AUDMAIN_H

#define	AUD_MUTE_PAUSE		(1<<0)
#define	AUD_MUTE_VIEWING	(1<<1)

#define	AUD_MUSIC_METAL		0
#define	AUD_MUSIC_GHOST		1
#define	AUD_MUSIC_WING		2
#define	AUD_MUSIC_SKATE		3



#define	AUD_FACE_APPEAR				(1<<0)
#define	AUD_FACE_DISAPPEAR			(1<<1)
#define	AUD_FACE_SPARK				(1<<2)
#define	AUD_FACE_PINCH				(1<<3)
#define	AUD_FACE_PULL				(1<<4)
#define	AUD_FACE_REFORM				(1<<5)
#define	AUD_FACE_ZOOMUP				(1<<6)
#define	AUD_FACE_ZOOMDOWN			(1<<7)
#define	AUD_FACE_TWINKLE			(1<<8)

#if 0
#define	AUD_LSE1_SLIP				 0
#define	AUD_LSE1_SLIP_GRND			 0
#define	AUD_LSE1_SLIP_GRAS			 1
#define	AUD_LSE1_SLIP_WATRSIDE		 2
#define	AUD_LSE1_SLIP_CONC			 3
#define	AUD_LSE1_SLIP_FLOOR			 4
#define	AUD_LSE1_SLIP_SNOW			 5
#define	AUD_LSE1_SLIP_ICE			 6
#define	AUD_LSE1_SMOKE				 7
#define	AUD_LSE1_TREEDOWN			 8
#define	AUD_LSE1_POLEDOWN			 9
#define	AUD_LSE1_SANDSINK			10
#define	AUD_LSE1_KOURA_SLIP			11
#endif

#define	AUD_LSE2_BRIDGESLOPE		13
#define	AUD_LSE2_LIFTROLLER			14
#define	AUD_LSE2_ROCKROLL			15
#define	AUD_LSE2_RIVER				16
#define	AUD_LSE2_FALL				17
#define	AUD_LSE2_MOVE				18
#define	AUD_LSE2_MAGMA				19
#define	AUD_LSE2_KIRAKIRA			20
#define	AUD_LSE2_CONVEYOR			21
#define	AUD_LSE2_SANDSTORM			22
#define	AUD_LSE2_WATERTORNADO		23

#define	AUD_LSE3_KUPAFIRE			24
#define	AUD_LSE3_CANNONAIM			25
#define	AUD_LSE3_JUGEMU_FLY			26
#define	AUD_LSE3_SPARK				27
#define	AUD_ENV_BIRD0				28
#define	AUD_ENV_BIRD1				29
#define	AUD_ENV_BIRD2				30
#define	AUD_ENV_BIRD3				31

#define	AUD_LSE2_STAND_APPEAR		32
#define	AUD_LSE3_SNOWMAN_BREATH		33
#define	AUD_LSE3_BURNER				34
#define	AUD_LSE2_WOODLIFT			35



/********************************************************************************/
/*																				*/
/*	external variables.															*/
/*																				*/
/********************************************************************************/

// extern ushort	audEnemyLevelSw;
// extern ushort	audEnvLevelSw;


/********************************************************************************/
/*																				*/
/*	audio macros.																*/
/*																				*/
/********************************************************************************/

// #define	AudSetEnemyLevel(n)
// #define	AudSetEnvLevel(n)

#define	AudStartSound(stratp,code)				Na_SeFlagEntry(code,&(stratp)->map.viewCoord[0])
#define	AudStopSound(stratp,code)				Na_SeEnd(code,&(stratp)->map.viewCoord[0])
#define	AudLevelSound(stratp,num)				AudSetLevelSound(num,&(stratp)->map.viewCoord[0])

#define	AudGetPlayingMusic()					Na_GetPlayingBgmFlag()
#define	AudFadeOutTime(n)						((n)*8-2)

/********************************************************************************/
/*																				*/
/*	music mute routine prototypes.												*/
/*																				*/
/********************************************************************************/

extern void
AudResetMute(void);

extern void
AudStartMute(int mute_sw);

extern void
AudEndMute(int mute_sw);

extern void
AudLockSound(void);

extern void
AudUnlockSound(void);


/********************************************************************************/
/*																				*/
/*	music and sound control routine prototypes.									*/
/*																				*/
/********************************************************************************/

extern void
AudSetSoundMode(ushort mode);

extern void
AudFaceSound(short sound);

extern void
AudPictWaveSound(void);

extern void
AudUnlimitedMusic(void);



extern void
AudPlayMusic(ushort mode, ushort music, short fadein);

extern void
AudStopMusic(short fadeout);

extern void
AudStopMusicOnly(short fadeout);


extern void
AudPlayStageMusic(ushort music);

extern void
AudPlaySkateMusic(void);

extern void
AudStopSkateMusic(void);

extern void
AudPlaySpecialMusic(ushort music);

extern void
AudFadeSpecialMusic(void);

extern void
AudStopSpecialMusic(void);

extern void
AudSetLevelSound(int sndnum, float *position);


/********************************************************************************/
/*																				*/
/*	audio main routine prototypes.												*/
/*																				*/
/********************************************************************************/

extern void
AudStartSoundEffect(void);

extern void
AudioProcess(void *arg);

#endif
