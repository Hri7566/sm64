/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage7 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE7_FOG_R		0
#define		STAGE7_FOG_G		0
#define		STAGE7_FOG_B		0
#define		STAGE7_FOG_START	960

#include "horror_dungeon/horror_dungeon_ext_texture.h"
#include "horror_dungeon/NEWSlift_texture.h"
#include "horror_dungeon/bigrock_texture.h"

#include "horror_dungeon/kdow_a_shape.sou"
#include "horror_dungeon/kdow_a_a_shape.sou"
#include "horror_dungeon/kdow_a_ia_shape.sou"
#include "horror_dungeon/kdow_a_nk_shape.sou"
#include "horror_dungeon/kdow_b_shape.sou"
#include "horror_dungeon/kdow_b_a_shape.sou"
#include "horror_dungeon/kdow_b_ia_shape.sou"
#include "horror_dungeon/kdow_b_kn_shape.sou"
#include "horror_dungeon/kdow_b_kn2_shape.sou"
#include "horror_dungeon/kdow_b_s_shape.sou"
#include "horror_dungeon/kdow_c_shape.sou"
#include "horror_dungeon/kdow_c_a_shape.sou"
#include "horror_dungeon/kdow_c_ia_shape.sou"
#include "horror_dungeon/kdow_c_kn_shape.sou"
#include "horror_dungeon/kdow_c_ht_shape.sou"
#include "horror_dungeon/kdow_c_ia2_shape.sou"
#include "horror_dungeon/kdow_d_shape.sou"
#include "horror_dungeon/kdow_d_a_shape.sou"
#include "horror_dungeon/kdow_d_ia_shape.sou"
#include "horror_dungeon/kdow_d_kn_shape.sou"
#include "horror_dungeon/kdow_d_ht_shape.sou"
#include "horror_dungeon/kdow_e_shape.sou"
#include "horror_dungeon/kdow_e_a_shape.sou"
#include "horror_dungeon/kdow_f_shape.sou"
#include "horror_dungeon/kdow_f_a_shape.sou"
#include "horror_dungeon/kdow_f_ia_shape.sou"
#include "horror_dungeon/kdow_f_ia2_shape.sou"
#include "horror_dungeon/kdow_f_kn_shape.sou"
#include "horror_dungeon/kdow_f_tr_shape.sou"
#include "horror_dungeon/kdow_g_shape.sou"
#include "horror_dungeon/kdow_h_shape.sou"
#include "horror_dungeon/kdow_h_ia_shape.sou"
#include "horror_dungeon/kdow_h_kn_shape.sou"
#include "horror_dungeon/kdow_el_shape.sou"

#include "horror_dungeon/NEWSlift_shape.sou"
#include "horror_dungeon/NEWSbut_shape.sou"

#include "horror_dungeon/big_rock_shape.sou"
#include "horror_dungeon/rock_pieceA_shape.sou"
#include "horror_dungeon/rock_pieceB_shape.sou"

#include "horror_dungeon/wdou_wavedat.sou"

#include "horror_dungeon/cx0701.flk"
#include "horror_dungeon/cx0701.tag"
#include "horror_dungeon/cx0701.ara"

#include "horror_dungeon/kdow_el_check.flk"
#include "horror_dungeon/kdow_f_tr_check.flk"
#include "horror_dungeon/NEWSliftcheck.flk"
#include "horror_dungeon/NEWSbutcheck.flk"

#include "horror_dungeon/ana_lift3.ral"

/********************************************************************************/
/*	Water surface records in 7 - 1 - 0 [ Underground lake ].					*/
/********************************************************************************/
static short waterSurf_07_01_00[] = {
	1,
	0,   20,  12,   -7628,-2559,   -7628, 7654,     563, 7654,     563,-2559,	 1,	150,	0
};

WaterSurfRec waterSurf_07_01[] = {
		{	 0, waterSurf_07_01_00	 },
		{	-1,	NULL	}					//	End of  WaterSurfRec data.
};


/********************************************************************************/
/*	Water surface records in 7 - 2 - 0 [ Strange smog ].						*/
/********************************************************************************/
static short waterSurf_07_02_00[] = {
	1,
	0,  15,   3,    1690,-6348,    1690,  819,    6298,  819,    6298,-6348,	 1,	120,	1
};

static short waterSurf_07_02_01[] = {
	1,
	0,   8,   3,    1690,-6348,    1690,  819,    6298,  819,    6298,-6348,	 0,	180,	1
};

WaterSurfRec waterSurf_07_02[] = {
		{	50, waterSurf_07_02_00	 },
		{	51, waterSurf_07_02_01	 },
		{	-1,	NULL	}					//	End of  WaterSurfRec data.
};
