/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: pathbomking.s
					Description	: 
					Date		: 1995.10.17
					Author		: H.yajima

 ********************************************************************************/

#define	BOMKING_STAR_X		 2000
#define	BOMKING_STAR_Y		 4500
#define	BOMKING_STAR_Z		-4500


/********************************************************************************
		Enemy bomking Animation !!
 ********************************************************************************/

#define ANM_bom_king_carry_anm			0
#define ANM_bom_king_carry_walk_anm		1
#define ANM_bom_king_damage_anm			2 /* new */
#define ANM_bom_king_explosion_anm		3
#define ANM_bom_king_fight_start_anm	4 /* new */
#define ANM_bom_king_first_wait_anm		5 /* new */
#define ANM_bom_king_help_anm			6
#define ANM_bom_king_jump_end_anm		7 /* new */
#define ANM_bom_king_jump_start_anm		8 /* new */
#define ANM_bom_king_pitch_anm			9
#define ANM_bom_king_standup_anm		10
#define ANM_bom_king_walk_anm			11


/*################*/
#ifdef ASSEMBLER
/*################*/

e_bomking:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON | stf_YangleSAME | stf_playerdistON | stf_playerangleON | stf_alldispON )
	p_set_pointer(skelanime,bomking_anime)
	p_setd(objname,OBJNAME_TAKE)
	p_sethitbox(100,100)
	p_setmovedata(30,-400,-50,1000,1000,200,0,0)
	p_hitON
	p_BGcheckYset
	p_save_nowpos
	p_makeobj_child(S_NULL,e_bomking_hand)
	p_setd(hp,3)
	p_setd(ap,1)
	p_while
		p_program(s_bomking)
	p_loop

e_bomking_hand:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON | stf_YangleSAME )
	p_softspritemodeON
	p_setf(skeletonX,100)
	p_setf(skeletonZ,150)
	p_while
		p_program(s_bomking_hand)
	p_loop

/*################*/
#else
/*################*/

#define		bomking_work	(execstp->s[stw_work0].d)
#define		bomking_movemode	(execstp->s[stw_work1].d)
#define		bomking_keycheck	(execstp->s[stw_work2].d)
#define		bomking_flag	(execstp->s[stw_work3].d)
#define		bomking_timer	(execstp->s[stw_work4].d)
#define		bomking_searchwait	(execstp->s[stw_work5].d)

/********************************************************************************
		bomking 
 ********************************************************************************/

extern unsigned long MotosProc1(int code,MapNode *node, void *data)
{
	if (code == MAP_CBACK_EXEC) {
		AffineMtx	 modelmtx;	
	  	StrategyRecord	*stp = (StrategyRecord *)hmsActiveShape;
		if ( stp->childobj != NULL ){
			s_calc_skeleton_glbmtx(&modelmtx,(AffineMtx *)data,hmsActiveCamera->matrix);
			s_calc_skeleton_glbpos(modelmtx,stp->childobj);
			s_copy_worldXYZmappos(stp->childobj);
		}
	}

	return(0);

}

extern void s_bomking_hand(void)
{
/*
	execstp->s[stw_skeletonX].f = 100 ; 
	execstp->s[stw_skeletonY].f = 0	  ;
	execstp->s[stw_skeletonZ].f = 150 ;
*/
	s_motoshand_main( 50 , 50 , PLAYERMAIL_CARRYTHROW_SMALL );
}


/********************************************************************************
		bomking initialize
 ********************************************************************************/

static void bomking_init(void)
{

	obj_speedF = 0;
	obj_speedY = 0;
	if ( obj_process == 0 ){
		s_hitOFF();
		camBOSSstp = execstp;
		s_set_skelanimeNo(ANM_bom_king_first_wait_anm );
		s_copy_initpos();
		obj_hp = 3;


		if ( s_hitcheck_message(500,100) ){
			obj_process++;
			Na_MusicMuteOn();
		}

	} else {
		if ( s_call_talkdemo(DLOG_LOOKUP,ENEMYDEMO_LOOKPLAYER,CAM_DEMO_TALK,17) ){
			obj_mode = mode_bomking_player_search;
			Mbitset(obj_mainflag,stf_catchON);
		//	Na_MusicStart(NA_MUS_HANDLE,NA_BOSS_BGM,0);
		}
	}

}

/********************************************************************************
		=========================
		 bomking search program
		=========================
 ********************************************************************************/

extern int s_player_bossarea_out(float y)
{
 	if ( obj_worldY - player_worldY > y ) return(1);
	else								   return(0); 

}

static void bomking_player_search(void)
{

	s_hitON();									/* take hitcheck ON !! */
	if ( obj_worldY - obj_attY < -100 ){
		obj_mode = mode_bomking_superjump;
		s_hitOFF();
	}


	if ( bomking_flag == 0 ) {
		if ( s_check_animenumber(15) )	s_call_Viewshake(VS_SMALL);
		if ( s_setanime_endcheck(ANM_bom_king_fight_start_anm )) bomking_flag++;



	} else {

		if ( bomking_flag == 1 ){
			s_setanime_start(ANM_bom_king_walk_anm,7);
		 	bomking_flag = 2;
		} else {
			s_set_skelanimeNo(ANM_bom_king_walk_anm);
		}

		if ( bomking_searchwait == 0 ){
			obj_speedF = 3;
			s_chase_angleY(obj_targetangle,0x100);
		} else {
			obj_speedF = 0;
		 	bomking_searchwait--;
		}
	}

	if ( s_check_playercarry() ) 		obj_mode = mode_bomking_player_carry;


	if ( s_player_bossarea_out(1200) ){
		obj_mode = mode_bomking_init;
		Na_EntryMusicFree(NA_BOSS_BGM);
	}

}

/********************************************************************************
		=======================
		 bomking carry program
		=======================
 ********************************************************************************/

static void bomking_player_carry(void)
{

	if ( obj_process == 0 ){

		obj_speedF 		 = 0;
		bomking_timer    = 0;
		bomking_keycheck = 0;
		if ( obj_timer == 0 )		objsound(NA_SE3_CATCH_B);
		if ( s_setanime_endcheck(ANM_bom_king_carry_anm) ){
			obj_process++;
			s_setanime_start(ANM_bom_king_carry_walk_anm,0);	/* carry walk */
		}
		
	} else if ( obj_process == 1 ){

		s_set_skelanimeNo(ANM_bom_king_carry_walk_anm);	/* carry walk */

		bomking_keycheck += key_gacha();
		dbErrPrint("%d",bomking_keycheck);				/* bomking_keycheck */
		if ( bomking_keycheck > 10 ){
			obj_imm = 3;
			obj_mode = mode_bomking_player_search;
			bomking_searchwait = 35;
			Mbitclr(execstp->s[stw_mail].d,EMAIL_CARRYMODE_ON);
		} else {
			obj_speedF = 3;
			if ( bomking_timer > 20 && s_chase_angleY(0,0x400) ){
				obj_process++;
				s_setanime_start(ANM_bom_king_pitch_anm,22);
			}
		}
		bomking_timer++;

	} else {

		s_set_skelanimeNo(ANM_bom_king_pitch_anm);

		if ( s_check_animenumber(31)){
			execstp->s[stw_imm].d = 2;		/* nageru shyn kan	*/
		 	objsound(NA_SE3_THROW_B);
		} else {
			if ( s_check_animeend() ){
				obj_mode = mode_bomking_wait;
				Mbitclr(execstp->s[stw_mail].d,EMAIL_CARRYMODE_ON);
			}
		}
	}


}

/********************************************************************************
		=========================
		 bomking wait program
		=========================
 ********************************************************************************/

static void bomking_wait(void)
{
	obj_speedF = 0;
	obj_speedY = 0;
	s_set_skelanimeNo(ANM_bom_king_walk_anm);
	obj_angleY = s_chase_angle(obj_angleY,obj_targetangle,0x200);
	if ( obj_playerdist < 2500 ) obj_mode = mode_bomking_player_search;

	if ( s_player_bossarea_out(1200) ){
		obj_mode = mode_bomking_init;
		Na_EntryMusicFree(NA_BOSS_BGM);
	}

}


/********************************************************************************
		=========================
		 bomking damage program
		=========================
 ********************************************************************************/

static void bomking_damage(void)
{

	if ( obj_process == 0 ){
		if ( obj_timer == 0 ){
			bomking_timer = 0;
			objsound(NA_SE3_BOMBKING_BOUND);
			objsound(NA_SE3_BOMBKING_DAMAGE);
			s_call_Viewshake(VS_SMALL);
			s_burneffect(0,0,100);
			obj_objname = OBJNAME_DAMAGE;	/* damage hit */
			s_hitON();
		//	s_demobegin();			
		}
		if ( s_setanime_endcheck(ANM_bom_king_damage_anm) )  bomking_timer++;
		if ( bomking_timer > 3 ) obj_process++;

	} else if ( obj_process == 1 ){

		if ( s_setanime_endcheck(ANM_bom_king_standup_anm) ){
			obj_process++;
			obj_objname = OBJNAME_TAKE;
			s_hitOFF();
		//	s_demoend();
		}

	} else {
		s_set_skelanimeNo(ANM_bom_king_walk_anm);
		if ( s_chase_angleY(obj_targetangle,0x800) == 1 ) obj_mode = mode_bomking_player_search;
	}

}

/********************************************************************************
		=========================
		 bomking dead
		=========================
 ********************************************************************************/

static void bomking_dead(void)
{

	s_set_skelanimeNo(ANM_bom_king_damage_anm);  
	
	if ( s_call_talkdemo(DLOG_LOOKUP,ENEMYDEMO_SETMESSAGE,CAM_DEMO_TALK,116)) {
		obj_remove_sound(NA_SE3_BOMBKING_DOWN);
		s_shape_hide();				/* shape hide	*/
		s_hitOFF();   				/* hitcheck OFF */
		s_burneffect(0,0,200);
		s_boxeffect(20,S_sankaku,3,4);
		s_call_Viewshake(VS_SMALL);
	/*	Na_BossBgmStop();	*/
		sv2_enemyset_star(BOMKING_STAR_X,BOMKING_STAR_Y,BOMKING_STAR_Z,200);
		obj_mode = mode_bomking_dead_stop;
	}	 

}

static void bomking_dead_stop(void)
{
	
	if ( obj_timer == 60 ) Na_EntryMusicFree(NA_BOSS_BGM);

}



/********************************************************************************
		=========================
		 bomking fly program
		=========================
 ********************************************************************************/

static void bomking_fly(void)
{
	if ( obj_worldY - obj_attY > -100 ){
	 	if ( obj_movestatus & MOVESTAT_BGBOUND ){
		
			obj_hp--;
			obj_speedF = 0;
			obj_speedY = 0;
			objsound(NA_SE3_BOMBKING_BOUND);
			if ( obj_hp != 0 )	obj_mode = mode_bomking_damage;
			else				obj_mode = mode_bomking_dead;
		}
	} else {
		if ( obj_process == 0 ){
		 	if ( obj_movestatus & MOVESTAT_BGTOUCH ){
				obj_speedF = 0;
				obj_speedY = 0;
				obj_process++;
			} else {
				if ( obj_movestatus & MOVESTAT_BGBOUND ) objsound(NA_SE3_BOMBKING_BOUND);
			}
		} else {
			if ( s_setanime_endcheck(ANM_bom_king_standup_anm) ) obj_mode = mode_bomking_superjump;
			 obj_process++;
		}

	}

}


/********************************************************************************
		============================
		 bomking superjump program
		============================
 *******************************************************************************/

static void bomking_superjump(void)
{



	switch(obj_process){

	 	case 0:
			if ( obj_timer == 0 )	objsound(NA_SE3_ENEMY_JUMP);
			bomking_movemode 	= 1;
			s_setanime_endstop(ANM_bom_king_jump_start_anm);	/* superjump start */

			obj_angleY = s_calc_returnangle();	/* return angle */
			if ( obj_worldY < obj_attY ) obj_speedY	= 100;
			else {
				s_calc_target_jump(&obj_attX,&obj_worldX,100,-4);
				obj_process++;
			}
			
			break;

		case 1:
			s_setanime_endstop(ANM_bom_king_jump_start_anm);	/* superjump start */
			if ( obj_speedY < 0 && obj_worldY < obj_attY ){
				obj_worldY 			= obj_attY;
				obj_speedY = 0;
				obj_speedF = 0;
				obj_gravity = -4;
				bomking_movemode 	= 0;
				s_set_skelanimeNo(ANM_bom_king_jump_end_anm);
				objsound(NA_SE3_BOMBKING_BOUND);
				s_call_Viewshake(VS_SMALL);
				obj_process++;
			}
			break;

		case 2:
			if ( s_setanime_endcheck(ANM_bom_king_jump_end_anm) ) obj_process++;
			break;

		case 3:
			if ( s_player_bossarea_out(1200) ){
				obj_mode = mode_bomking_init;
				Na_EntryMusicFree(NA_BOSS_BGM);
			}
			if ( s_hitcheck_message(500,100) ) obj_process++;
			break;

		case 4:
			if ( s_call_talkdemo(DLOG_LOOKUP,ENEMYDEMO_LOOKPLAYER,CAM_DEMO_TALK,128) ){
				obj_mode = mode_bomking_player_search;
			//	Na_MusicStart(NA_MUS_HANDLE,NA_BOSS_BGM,0);
			}
			break;

	}


}


/********************************************************************************
		=====================
		 bomking normal main
		=====================
 *******************************************************************************/

static void *bomking_modejmp[] = {
	
	 bomking_init,
	 bomking_wait,
	 bomking_player_search,
	 bomking_player_carry,
	 bomking_fly,
	 bomking_superjump,
	 bomking_damage,
	 bomking_dead,
	 bomking_dead_stop
	
};

static EnemySoundRecord bomking_sound[] = {
	{ 0,0,0,0 },						/* bom_king_carry_anm		*/	
	{ 1,1,20,  NA_SE3_KATSUGI_WALK},	/* bom_king_carry_walk_anm	*/
	{ 0,0,0,0 },						/* bom_king_damage_anm		*/
	{ 0,0,0,0 },						/* bom_king_explosion_anm	*/
	{ 1,15,-1, NA_SE3_KATSUGI_WALK },	/* bom_king_fight_start_anm */
	{ 0,0,0,0 },						/* bom_king_first_wait_anm	*/
	{ 0,0,0,0 },						/* bom_king_help_anm		*/
	{ 0,0,0,0 },						/* bom_king_jump_end_anm	*/
	{ 0,0,0,0 },						/* bom_king_jump_start_anm	*/
	{ 1,33,-1, NA_SE3_KATSUGI_WALK },	/* bom_king_pitch_anm		*/
	{ 0,0,0,0 },						/* bom_king_standup_anm		*/
	{ 1,1,15, NA_SE3_KATSUGI_WALK },	/* bom_king_walk_anm		*/
};

static void bomking_main(void)
{

	s_enemybgcheck();		
	if ( bomking_movemode == 0 ) 	s_enemymove(-78);
	else							s_optionmove_F();
	s_modejmp(bomking_modejmp);

	s_enemysound(bomking_sound);	/* bomking sound */

	if ( obj_playerdist < 5000 )	s_shapeON();
	else							s_shapeOFF();

}

/********************************************************************************
		==============
		 bomking main 
		==============
 ********************************************************************************/
extern void s_bomking(void)
{

	float	f,y;
	StrategyRecord	*stp;
	int	anime_frame;
	f = 20;
	y = 50;


	Mbitset(execstp->s[stw_enemyinfo].d,ENEMYINFO_CARRY);
	switch(execstp->s[stw_actionmode].d){
		case	CHILEDMODE_NO_CATCH:	bomking_main();											break;
		case	CHILEDMODE_CATCH:		s_mode_catch(ANM_bom_king_help_anm ,mode_bomking_wait);	break;
		case	CHILEDMODE_THROW:
		case	CHILEDMODE_DROP:
			s_mode_throw(f,y,mode_bomking_fly);
			s_hitOFF();
			obj_worldY += 20;
			break;
	}


	obj_mail = 0;

#ifdef yajima
	anime_frame = execstp->map.skelanim.frame;

	dbErrPrint("md %d",obj_mode);
	dbErrPrint("pr %d",obj_process);
	dbErrPrint("am %d",execstp->s[stw_skelanimeNo].d);
	dbErrPrint("fr %d",anime_frame);
#endif

}

/*################*/
#endif
/*################*/
/*===============================================================================
		end end end end end end end end 
===============================================================================*/

