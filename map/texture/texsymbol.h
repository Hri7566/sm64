/********************************************************************************
						Ultra 64 MARIO Brothers

					include texture symbol data files.

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							Dec 18, 1995
*********************************************************************************/

#include "a_lava_texsymb.h"
#include "b_house_texsymb.h"
#include "c_field_texsymb.h"
#include "d_water_texsymb.h"
#include "e_mt_texsymb.h"
#include "f_snow_mt_texsymb.h"
#include "g_cave_texsymb.h"
#include "h_mecha_texsymb.h"
#include "i_slider_texsymb.h"
#include "j_fort_texsymb.h"
#include "k_castlein_texsymb.h"
#include "l_castleout_texsymb.h"
