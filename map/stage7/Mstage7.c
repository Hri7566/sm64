/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage7 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

#define		STAGE7_CLEAR_R		0
#define		STAGE7_CLEAR_G		0
#define		STAGE7_CLEAR_B		0

extern Gfx gfx_kdow_a[]	 	;
extern Gfx gfx_kdow_a_a[]	;
extern Gfx gfx_kdow_a_ia[]	;
extern Gfx gfx_kdow_a_nk[]	;
extern Gfx gfx_kdow_b[]	 	;
extern Gfx gfx_kdow_b_a[]	;
extern Gfx gfx_kdow_b_ia[]	;
extern Gfx gfx_kdow_b_kn[]	;
extern Gfx gfx_kdow_b_kn2[]	;
extern Gfx gfx_kdow_b_s[]	;
extern Gfx gfx_kdow_c[]	 	;
extern Gfx gfx_kdow_c_a[]	;
extern Gfx gfx_kdow_c_ia[]	;
extern Gfx gfx_kdow_c_kn[]	;
extern Gfx gfx_kdow_c_ht[]	;
extern Gfx gfx_kdow_c_ia2[]	;
extern Gfx gfx_kdow_d[]  	;
extern Gfx gfx_kdow_d_a[]	;
extern Gfx gfx_kdow_d_ia[]	;
extern Gfx gfx_kdow_d_kn[]	;
extern Gfx gfx_kdow_d_ht[]	;
extern Gfx gfx_kdow_e[]	 	;
extern Gfx gfx_kdow_e_a[]	;
extern Gfx gfx_kdow_f[]	 	;
extern Gfx gfx_kdow_f_a[]	;
extern Gfx gfx_kdow_f_ia[]	;
extern Gfx gfx_kdow_f_ia2[]	;
extern Gfx gfx_kdow_f_kn[]	;
extern Gfx gfx_kdow_f_tr[]	;
extern Gfx gfx_kdow_g[]	 	;
extern Gfx gfx_kdow_h[]	 	;
extern Gfx gfx_kdow_h_ia[]	;
extern Gfx gfx_kdow_h_kn[]	;
extern Gfx gfx_kdow_el[]	;

extern Gfx gfx_NEWSlift[]	;
extern Gfx gfx_NEWSbut[] 	;

extern Gfx gfx_big_rock[]	;
extern Gfx gfx_rock_pieceA[];
extern Gfx gfx_rock_pieceB[];


/* --------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------- */


/* ===============================================================================
        : Hierarchy map data of KdTrap.
================================================================================== */
Hierarchy RCP_HmsKdTrap[] = {
    hmsHeader(1000)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_kdow_f_tr)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of KdBigRock.
================================================================================== */
Hierarchy RCP_HmsKdBigRock[] = {
    hmsHeader(300)
    hmsBegin()
		hmsShadow(400, 180, 0)
		hmsBegin()
	        hmsGfx(RM_SURF, gfx_big_rock)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of KdRockPiA.
================================================================================== */
Hierarchy RCP_HmsKdRockPiA[] = {
    hmsHeader(150)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_rock_pieceA)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of KdRockPiB.
================================================================================== */
Hierarchy RCP_HmsKdRockPiB[] = {
    hmsHeader(100)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_rock_pieceB)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of NEWSLift.
================================================================================== */
Hierarchy RCP_HmsNEWSLift[] = {
    hmsHeader(550)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_NEWSlift)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of NEWSBut.
================================================================================== */
Hierarchy RCP_HmsNEWSBut[] = {
    hmsHeader(200)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_NEWSbut)
    hmsEnd()
    hmsExit()
};


/* ---------------------------------------------------------------------------------------- */
/* ---------------------------------------------------------------------------------------- */


/* ===============================================================================
        : Hierarchy map data of KdowElv.
================================================================================== */
Hierarchy RCP_HmsKdowElv[] = {
    hmsHeader(500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kdow_el)
    hmsEnd()
    hmsExit()
};

/* ================================================================================
        : Switch map shape 01.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap01[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_a)
        hmsGfx(RM_SPRITE, gfx_kdow_a_a)
		hmsGfx(RM_XSURF , gfx_kdow_a_ia)
		hmsGfx(RM_SURF  , gfx_kdow_a_nk)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 02.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap02[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_b)
        hmsGfx(RM_SPRITE, gfx_kdow_b_a)
		hmsGfx(RM_XSURF , gfx_kdow_b_ia)
		hmsGfx(RM_SURF  , gfx_kdow_b_kn)
		hmsGfx(RM_XSURF , gfx_kdow_b_kn2)
		hmsGfx(RM_DECAL , gfx_kdow_b_s)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 03.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap03[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_c)
        hmsGfx(RM_SPRITE, gfx_kdow_c_a)
		hmsGfx(RM_XSURF , gfx_kdow_c_ia)
		hmsGfx(RM_XSURF , gfx_kdow_c_kn)
		hmsGfx(RM_SURF  , gfx_kdow_c_ht)
		hmsGfx(RM_XSURF , gfx_kdow_c_ia2)
		hmsCProg(0	   , WaterInit)
		hmsCProg(0x0702, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 04.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap04[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_d)
        hmsGfx(RM_SPRITE, gfx_kdow_d_a)
		hmsGfx(RM_XSURF , gfx_kdow_d_ia)
		hmsGfx(RM_SURF  , gfx_kdow_d_kn)
		hmsGfx(RM_XSURF , gfx_kdow_d_ht)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 05.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap05[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_e)
        hmsGfx(RM_SPRITE, gfx_kdow_e_a)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 06.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap06[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_f)
        hmsGfx(RM_SPRITE, gfx_kdow_f_a)
		hmsGfx(RM_XSURF , gfx_kdow_f_ia)
		hmsGfx(RM_XSURF , gfx_kdow_f_ia2)
		hmsGfx(RM_SURF  , gfx_kdow_f_kn)
		hmsCProg(0	   , WaterInit)
		hmsCProg(0x0701, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 07.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap07[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF, gfx_kdow_g)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0000, WaveMove)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 08.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap08[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF , gfx_kdow_h)
		hmsGfx(RM_XSURF, gfx_kdow_h_ia)
		hmsGfx(RM_SURF , gfx_kdow_h_kn)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 09.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap09[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_a)
        hmsGfx(RM_SPRITE, gfx_kdow_a_a)
		hmsGfx(RM_XSURF , gfx_kdow_a_ia)
		hmsGfx(RM_SURF  , gfx_kdow_a_nk)
        hmsGfx(RM_SURF	, gfx_kdow_b)
        hmsGfx(RM_SPRITE, gfx_kdow_b_a)
		hmsGfx(RM_XSURF , gfx_kdow_b_ia)
		hmsGfx(RM_SURF  , gfx_kdow_b_kn)
		hmsGfx(RM_XSURF , gfx_kdow_b_kn2)
		hmsGfx(RM_DECAL , gfx_kdow_b_s)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 10.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap10[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_a)
        hmsGfx(RM_SPRITE, gfx_kdow_a_a)
		hmsGfx(RM_XSURF , gfx_kdow_a_ia)
		hmsGfx(RM_SURF  , gfx_kdow_a_nk)
        hmsGfx(RM_SURF	, gfx_kdow_d)
        hmsGfx(RM_SPRITE, gfx_kdow_d_a)
		hmsGfx(RM_XSURF , gfx_kdow_d_ia)
		hmsGfx(RM_SURF  , gfx_kdow_d_kn)
		hmsGfx(RM_XSURF , gfx_kdow_d_ht)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 11.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap11[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_b)
        hmsGfx(RM_SPRITE, gfx_kdow_b_a)
		hmsGfx(RM_XSURF , gfx_kdow_b_ia)
		hmsGfx(RM_SURF  , gfx_kdow_b_kn)
		hmsGfx(RM_XSURF , gfx_kdow_b_kn2)
		hmsGfx(RM_DECAL , gfx_kdow_b_s)
        hmsGfx(RM_SURF	, gfx_kdow_c)
        hmsGfx(RM_SPRITE, gfx_kdow_c_a)
		hmsGfx(RM_XSURF , gfx_kdow_c_ia)
		hmsGfx(RM_XSURF , gfx_kdow_c_kn)
		hmsGfx(RM_SURF  , gfx_kdow_c_ht)
		hmsGfx(RM_XSURF , gfx_kdow_c_ia2)
		hmsCProg(0	   , WaterInit)
		hmsCProg(0x0702, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 12.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap12[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_b)
        hmsGfx(RM_SPRITE, gfx_kdow_b_a)
		hmsGfx(RM_XSURF , gfx_kdow_b_ia)
		hmsGfx(RM_SURF  , gfx_kdow_b_kn)
		hmsGfx(RM_XSURF , gfx_kdow_b_kn2)
		hmsGfx(RM_DECAL , gfx_kdow_b_s)
        hmsGfx(RM_SURF	, gfx_kdow_d)
        hmsGfx(RM_SPRITE, gfx_kdow_d_a)
		hmsGfx(RM_XSURF , gfx_kdow_d_ia)
		hmsGfx(RM_SURF  , gfx_kdow_d_kn)
		hmsGfx(RM_XSURF , gfx_kdow_d_ht)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 13.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap13[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_c)
        hmsGfx(RM_SPRITE, gfx_kdow_c_a)
		hmsGfx(RM_XSURF , gfx_kdow_c_ia)
		hmsGfx(RM_XSURF , gfx_kdow_c_kn)
		hmsGfx(RM_SURF  , gfx_kdow_c_ht)
		hmsGfx(RM_XSURF , gfx_kdow_c_ia2)
        hmsGfx(RM_SURF	, gfx_kdow_e)
        hmsGfx(RM_SPRITE, gfx_kdow_e_a)
		hmsCProg(0	   , WaterInit)
		hmsCProg(0x0702, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 14.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap14[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_c)
        hmsGfx(RM_SPRITE, gfx_kdow_c_a)
		hmsGfx(RM_XSURF , gfx_kdow_c_ia)
		hmsGfx(RM_XSURF , gfx_kdow_c_kn)
		hmsGfx(RM_SURF  , gfx_kdow_c_ht)
		hmsGfx(RM_XSURF , gfx_kdow_c_ia2)
        hmsGfx(RM_SURF	, gfx_kdow_f)
        hmsGfx(RM_SPRITE, gfx_kdow_f_a)
		hmsGfx(RM_XSURF , gfx_kdow_f_ia)
		hmsGfx(RM_XSURF , gfx_kdow_f_ia2)
		hmsGfx(RM_SURF  , gfx_kdow_f_kn)
		hmsCProg(0	   , WaterInit)
		hmsCProg(0x0701, WaterDraw)
		hmsCProg(0x0702, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 15.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap15[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_d)
        hmsGfx(RM_SPRITE, gfx_kdow_d_a)
		hmsGfx(RM_XSURF , gfx_kdow_d_ia)
		hmsGfx(RM_SURF  , gfx_kdow_d_kn)
		hmsGfx(RM_XSURF , gfx_kdow_d_ht)
        hmsGfx(RM_SURF	, gfx_kdow_f)
        hmsGfx(RM_SPRITE, gfx_kdow_f_a)
		hmsGfx(RM_XSURF , gfx_kdow_f_ia)
		hmsGfx(RM_XSURF , gfx_kdow_f_ia2)
		hmsGfx(RM_SURF  , gfx_kdow_f_kn)
		hmsCProg(0	   , WaterInit)
		hmsCProg(0x0701, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 16.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap16[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_f)
        hmsGfx(RM_SPRITE, gfx_kdow_f_a)
		hmsGfx(RM_XSURF , gfx_kdow_f_ia)
		hmsGfx(RM_XSURF , gfx_kdow_f_ia2)
		hmsGfx(RM_SURF  , gfx_kdow_f_kn)
        hmsGfx(RM_SURF	, gfx_kdow_g)
		hmsCProg(0, WaveInit)
		hmsCProg(0x0000, WaveMove)
		hmsCProg(0	   , WaterInit)
		hmsCProg(0x0701, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 17.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap17[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_f)
        hmsGfx(RM_SPRITE, gfx_kdow_f_a)
		hmsGfx(RM_XSURF , gfx_kdow_f_ia)
		hmsGfx(RM_XSURF , gfx_kdow_f_ia2)
		hmsGfx(RM_SURF  , gfx_kdow_f_kn)
        hmsGfx(RM_SURF	, gfx_kdow_h)
		hmsGfx(RM_XSURF , gfx_kdow_h_ia)
		hmsGfx(RM_SURF  , gfx_kdow_h_kn)
		hmsCProg(0	   , WaterInit)
		hmsCProg(0x0701, WaterDraw)
    hmsEnd()
    hmsReturn()
};

/* ================================================================================
        : Switch map shape 18.
=================================================================================== */
Hierarchy RCP_Hms_kdow_switchmap18[] = {
    hmsGroup()
    hmsBegin()
        hmsGfx(RM_SURF	, gfx_kdow_d)
        hmsGfx(RM_SPRITE, gfx_kdow_d_a)
		hmsGfx(RM_XSURF , gfx_kdow_d_ia)
		hmsGfx(RM_SURF  , gfx_kdow_d_kn)
		hmsGfx(RM_XSURF , gfx_kdow_d_ht)
        hmsGfx(RM_SURF	, gfx_kdow_e)
        hmsGfx(RM_SPRITE, gfx_kdow_e_a)
    hmsEnd()
    hmsReturn()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage7Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(STAGE7_CLEAR_R, STAGE7_CLEAR_G, STAGE7_CLEAR_B, 1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON_O,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsSelect(18, ControlSwitchMap)
					hmsBegin()
						hmsCall(RCP_Hms_kdow_switchmap01)
						hmsCall(RCP_Hms_kdow_switchmap02)
						hmsCall(RCP_Hms_kdow_switchmap03)
						hmsCall(RCP_Hms_kdow_switchmap04)
						hmsCall(RCP_Hms_kdow_switchmap05)
						hmsCall(RCP_Hms_kdow_switchmap06)
						hmsCall(RCP_Hms_kdow_switchmap07)
						hmsCall(RCP_Hms_kdow_switchmap08)
						hmsCall(RCP_Hms_kdow_switchmap09)
						hmsCall(RCP_Hms_kdow_switchmap10)
						hmsCall(RCP_Hms_kdow_switchmap11)
						hmsCall(RCP_Hms_kdow_switchmap12)
						hmsCall(RCP_Hms_kdow_switchmap13)
						hmsCall(RCP_Hms_kdow_switchmap14)
						hmsCall(RCP_Hms_kdow_switchmap15)
						hmsCall(RCP_Hms_kdow_switchmap16)
						hmsCall(RCP_Hms_kdow_switchmap17)
						hmsCall(RCP_Hms_kdow_switchmap18)
					hmsEnd()
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

	hmsEnd()
	hmsExit()
};
