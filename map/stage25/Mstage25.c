/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage25 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

extern ulong EndingBGDraw(int, MapNode*, void*);

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage25Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()

		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsCProg(0, EndingBGDraw)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_FIELD,  0,2000,6000,  0,0,0, GameCamera)
//				hmsBegin()
//					hmsObject()
//				hmsEnd()
			hmsEnd()
		hmsEnd()

	hmsEnd()
	hmsExit()
};
