/********************************************************************************
	gfxmain.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	March 29, 1996
 ********************************************************************************/

#ifndef GFXMAIN_H
#define	GFXMAIN_H

#define	GRAPH_TASK		0
#define	AUDIO_TASK		1

#define	SCREEN_WIDTH		320
#define	SCREEN_HEIGHT		240
#define	SCREEN_ASPECT		(320.0f/240.0f)
#define	SCREEN_NEAR			100.0f
#define	SCREEN_FAR			12800.0f
#define	SCREEN_ANGLE		30.0f

#define	CLIP_UPPER			8
#define	CLIP_LOWER			(SCREEN_HEIGHT-8)


#define	LIGHT_X				40
#define	LIGHT_Y				40
#define	LIGHT_Z				40

#define	SC_CLIENT_AUDIO		1
#define	SC_CLIENT_GRAPHIC	2


#define	GAME_DEMO			0
#define	GAME_ONE_PLAYER		1
#define	GAME_TWO_PLAYER		2

#define	MARIO_ID			0
#define	LUIGI_ID			1


typedef void (*RDPDoneProc)(void);


/********************************************************************************/
/*																				*/
/*	Controller button assignment												*/
/*																				*/
/********************************************************************************/

#define	CONT_XA		CONT_A
#define	CONT_XB		CONT_B
#define	CONT_XL		CONT_C
#define	CONT_XD		CONT_D
#define	CONT_XU		CONT_E
#define	CONT_XR		CONT_F
#define	CONT_BACK	CONT_G
#define	CONT_EXIT	0x0080

#define AUTO_A		0x80
#define AUTO_B		0x40
#define AUTO_Z		0x20
#define	AUTO_END	0x10
#define AUTO_CU		0x08
#define AUTO_CD		0x04
#define AUTO_CL		0x02
#define AUTO_CR		0x01


/*################################################################################
 *
 *
 *	Memory types.
 *
 */

/********************************************************************************/
/*	Pixel map.																	*/
/********************************************************************************/

typedef ushort	Pixmap[SCREEN_HEIGHT][SCREEN_WIDTH];

/********************************************************************************/
/*	Dynamic data record.														*/
/********************************************************************************/

typedef struct {
	wlong	memblock[DYNAMIC_BLKSIZE];			/* dynamic memory block			*/
	RSPTask	rsptask;							/* graphic task record			*/
} DynamicRecord, *DynamicPtr;

/********************************************************************************/
/*	player's cap location storage record.										*/
/********************************************************************************/

typedef struct {
	uchar		stage;					/*   1 byte: cap stage					*/
	uchar		scene;					/*   1 byte: cap scene					*/
	SVector		position;				/*   6 byte: cap position				*/
} BuCapLocate;							/*   8 byte: (total)					*/

/********************************************************************************/
/*	player's cap location storage record.										*/
/********************************************************************************/

typedef struct {
	BuCapLocate	caplocate;				/*   8 byte: cap location				*/
	ulong		itemflags;				/*   4 byte: item flags					*/
	uchar		starflags[25];			/*  25 byte: star flags					*/
	uchar		numcoins[15];			/*  15 byte: number of coins			*/
	ushort		dataID;					/*   2 byte: data ID					*/
	ushort		checksum;				/*   2 byte: check sum					*/
} BuStorage;							/*  56 byte: total						*/

/********************************************************************************/
/*	High score backup record.													*/
/********************************************************************************/

typedef struct {
	ulong		order[4];				/*  16 byte: winner						*/
	ushort		sndmode;				/*   2 byte: audio mode					*/
	ushort		reserved[5];			/*	10 byte: reserved					*/
	ushort		dataID;					/*   2 byte: data ID					*/
	ushort		checksum;				/*   2 byte: check sum					*/
} BuHiScore;							/*  32 byte: (total)					*/

/********************************************************************************/
/*	player's cap location storage record.										*/
/********************************************************************************/

typedef struct {
	BuStorage	storage[4][2];			/* 56*2*4 = 448 byte: player storage	*/
	BuHiScore	hiscore[2];				/* 32*2   =  64 byte: high score		*/
} BackupRecord;							/*			512 byte: total				*/

/********************************************************************************/
/*	Controller data record.														*/
/********************************************************************************/

typedef struct {
	short		 stickx;				/* analog stick position x				*/
	short		 sticky;				/* analog stick position y				*/
	float		 levelx;				/* level of stick x						*/
	float		 levely;				/* level of stick y						*/
	float		 length;
	ushort		 status;				/* controller button status				*/
	ushort		 trigger;				/* controller button trigger			*/
	OSContStatus *contst;				/* pointer to the controller status		*/
	OSContPad	 *contdt;				/* pointer to the controller data		*/
	int			  contNo;				/* controller number					*/
} Controller;

/********************************************************************************/
/*	Controller demo data record.												*/
/********************************************************************************/

typedef struct {
	uchar	repeat;						/* repeat time							*/
	char	stickx;						/* analog stick position x				*/
	char	sticky;						/* analog stick position y				*/
	uchar	button;						/* controller button					*/
} DemoController;

/********************************************************************************
 *
 *	External works
 *
 ********************************************************************************/

extern BackupRecord backupMemory;		/* Backup EEROM I/O buffer				*/ 

extern ulong	   frameCounter;		/* game frame counter					*/
extern RSPTask	   *taskPtr;			/* pointer to the task list				*/
extern GfxPtr	   graphPtr;			/* pointer to the gfx list				*/
extern MemPtr	   blockPtr;			/* pointer to the dynamic memory		*/
extern DynamicPtr  currentDynamic;		/* pointer to the current dynamic work	*/
extern RDPDoneProc rdpdoneProc;

/*--------------------------------------------------------------------------------
 *	Controller works
 */
extern Controller controller[3];		/* contoller works						*/
extern Controller *cont1p;				/* pointer to the controller 1			*/
extern Controller *cont2p;				/* pointer to the controoler 2			*/
extern Controller *contOr;				/* pointer to the controller 1 or 2		*/

extern DemoController *autoDemoPtr;
extern ushort		  demoIndexNo;

extern uchar contConnection;			/* controller connection				*/
extern char  eepromExists;				/* EEPROM exists flag					*/

/********************************************************************************
 *
 *	External static datas
 *
 ********************************************************************************/

extern Pixmap frameMemoryA;
extern Pixmap frameMemoryB;
extern Pixmap frameMemoryC;
extern Pixmap depthMemory;
extern ushort textureMemory[32][2048];


extern uwlong rspDramStack[SP_DRAM_STACK_SIZE64];	/* RSP tasks stack			*/

extern DynamicRecord dynamicData[DBLBUF];			/* dynamic data memory		*/
extern PartialRecord playerAnimation;
extern PartialRecord controllerData;


extern Gfx RCP_ClearScreen[];
extern Gfx RCP_hmsBeginString[];
extern Gfx RCP_hmsDrawCharacter[];
extern Gfx RCP_hmsEndString[];


/********************************************************************************
 *
 *	Function prototypes
 *
 ********************************************************************************/

/* RCP segment operations */

extern void
GraphProcess(void *arg);


/* drawing oparations */

extern void BeginDrawing(void);
extern int  EndDrawing(void);
extern void SoftwareBlanking(long color);
extern void SoftwareTrimBlanking(Vp *trimming, long color);
extern void SetTrimmingRectangle(Vp *trimming);

#endif
