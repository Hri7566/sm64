/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Uzumaki Strategy  (for gusheffect )

				Junuary 31 1996       programed by Iwamoto Daiki
 ********************************************************************************/


/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
		Uzumaki Path Data
 ********************************************************************************/
e_uzumaki:
	p_initialize(player_friend)
	p_setbit(flag,stf_moveON|stf_playerdistON )
	p_program(s_uzumaki_init)

uzumaki_main:
	p_while
	p_program(s_uzumaki_main)
	p_loop



/********************************************************************************
		Fukidasi Path Data
 ********************************************************************************/
e_fukidasi:
	p_initialize(option)
	p_setbit(flag,stf_moveON|stf_playerdistON )

	p_while
	p_program(s_fukidasi_main)
	p_loop




/*################*/
#else
/*################*/


/*===================================================================================================
		C Program
 ====================================================================================================*/
#include "../../include/weather.h"

#define uzumaki_angX (execstp->s[stw_work0].d)
#define uzumaki_angZ (execstp->s[stw_work1].d)


static HitCheckRecord uzumaki_hit = {
	OBJNAME_DRAINPIPE,
	0,0,0,0,	/* flag,ap,hp,coin 		 */
	200,500,		/* player attack hitarea */
	 0, 0,		/* player damage hitarea */
};

/***************************************************************************************************
						    Initial
***************************************************************************************************/
extern void s_uzumaki_init(void)
{
	
	uzumaki_angX = obj_animeangleX;
	uzumaki_angZ = obj_animeangleZ;

	obj_animeangleX = 0;
	obj_animeangleZ = 0;
}
/***************************************************************************************************
						    Hit Check
***************************************************************************************************/
static void Uzumaki_HitCheck(void)
{

	s_set_hitparam(execstp,&uzumaki_hit);		/* Set HitCheck Param */



}
/***************************************************************************************************
						    ShapeRotXZ
***************************************************************************************************/
static void ShapeRotXZ(void)
{

	float cosx = cos(obj_animeangleX);
	float sinx = sin(obj_animeangleX);
	float cosz = cos(obj_animeangleZ);
	float sinz = sin(obj_animeangleZ);

	float a = (sinz*cosx);
	float b = (cosx*cosz);
	float c =        sinx;

	ShapeIncline(execstp,a,b,c);


}
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_uzumaki_main(void)
{

	if (obj_playerdist < 5000) {
		MapDispShape(&execstp->map);
		Uz_objNum =  60;
		Uz_stx = obj_worldX;
		Uz_stz = obj_worldZ;
		Uz_enx = obj_worldX;
		Uz_eny = obj_worldY;
		Uz_enz = obj_worldZ;
		Uz_sty = obj_worldY + 800;

		Uz_angleX = uzumaki_angX;
		Uz_angleZ = uzumaki_angZ;

		ShapeRotXZ();
		obj_animeangleY += 8000;	

	}
	else {
		MapHideShape(&execstp->map);
		Uz_objNum = 0;
		

	}
	objsound_level(NA_LSE2_WATERTORNADO);
	Uzumaki_HitCheck();

}



/*===================================================================================================
		Fukidasi Event
 ====================================================================================================*/
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_fukidasi_main(void)
{

	if (obj_playerdist < 5000) {
		Fu_objNum =  60;
		Fu_stx = obj_worldX;
		Fu_sty = obj_worldY;
		Fu_stz = obj_worldZ;
	}
	else {
		Fu_objNum = 0;
		

	}

	objsound_level(NA_LSE2_WATERTORNADO);

}



/*################*/
#endif
/*################*/


