/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 9 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage09


/* ================================================================================
		: Move BGs sequence.
===================================================================================	*/
SEQ_Stage09_MoveBGs:

		seqActor(S_movebg01,   1456, 768,  446,  0,326,0,  0,0,0,  e_wanwan_fence	)
		seqActor(S_movebg02,  -2303, 717, 1024,  0, 45,0,  0,3,0,  e_seesaw		)
		seqActor(S_NULL	   ,  -2050,   0,-3069,  0, 25,0,  0,0,0,  e_switchdoor	)

		seqActor(S_hanbutton,  -2283,0,-3682,  0,27,0,  0,0,0,  e_switchdoor_switch)

		seqActor(S_commonlift,  1612, 300, 4611,  0,0,0,  0,40,0,   e_doublelift	)

		seqReturn


/* ================================================================================
		: Enemies sequence.
===================================================================================	*/
SEQ_Stage09_Enemies:

		seqLevelActor(0x0003,S_NULL	   ,  1535,3840,-5561,  0,   0,0,  0,0,0,  e_gush_ironball_few	)
		seqLevelActor(0x003c,S_NULL	   ,  1535,3840,-5561,  0,   0,0,  0,0,0,  e_gush_ironball		)
		seqLevelActor(0x0003,S_NULL	   ,   524,2825,-5400,  0,   0,0,  0,2,0,  e_gush_ironball_few	)
		seqLevelActor(0x003c,S_NULL	   ,   524,2825,-5400,  0,   0,0,  0,2,0,  e_gush_ironball		)
		seqActor(S_ironball,  -993, 886,-3565,  0,	 0,0,  0,0,0,  e_ironball		)
		seqActor(S_ironball,  -785, 886,-4301,  0,	 0,0,  0,0,0,  e_ironball		)

		seqLevelActor(0x003e,S_ironball,   -93, 886,-3414,  0,	 0,0,  0,0,0,  e_ironball	)
		seqLevelActor(0x0001,S_redbom  , -5723, 140, 6017,  0,	 0,0,  0,2,0,  e_messbom	)
		seqLevelActor(0x0001,S_redbom  , -6250,   0, 6680,  0,	 0,0,  0,1,0,  e_messbom	)
		seqLevelActor(0x003e,S_redbom  , -5723, 140, 6017,  0,	 0,0,  0,0,0,  e_futa_bom	)
		seqLevelActor(0x003e,S_redbom  , -6250,   0, 6680,  0,	 0,0,  0,3,0,  e_messbom	)

		seqLevelActor(0x0001, S_cannon_base, -5694, 128, 5600,  0,135,0,  0,1,0,  e_watercanon)
		seqLevelActor(0x003e, S_bom_futa, 	 -5694, 128, 5600,  0,180,0,  0,0,0,  e_bom_cannon)

		seqLevelActor(0x0002,S_NULL,       3304,4242,-4603,  0,0,0,  0,0,0,  e_kamegoal)
		seqLevelActor(0x003c,S_nokonoko,   3400, 770, 6500,  0,0,0,  0,1,0,  e_noko	   )

		seqReturn


/* ================================================================================
		: Stars sequence.
===================================================================================	*/
SEQ_Stage09_Stars:

		seqLevelActor(0x0001,S_bomking ,  1636,4242,-5567, 0,-147,0, 0,0,0,  e_bomking 			) 	/*bomb king  */
		seqLevelActor(0x0002,S_nokonoko, -4004,   0, 5221, 0,0,0,    1,2,0,  e_noko				)	/* kame race */
/*		seqLevelActor(0x003f,S_polystar,  5500,3400, 1200, 0,0,0,    2,0,0,  e_tripstar			)	/* Ukisima 	*/
	   	seqLevelActor(0x003f,S_NULL    , -6000,1000, 2400, 0,0,0,    3,0,0,  e_tripstar_getcoins)  	/*  8 coin	*/
	   	seqLevelActor(0x003f,S_NULL	   , -6600,1000, 1250, 0,0,0,    4,4,0,  e_tripstar_getdummy)	/* secret	*/
		seqLevelActor(0x003f,S_polystar,  1550,1200,  300, 0,0,0,    5,0,0,  e_tripstar			)  	/* one one	*/

		seqReturn


/* ================================================================================
		: Stage 9 main sequence.
===================================================================================	*/
SEQ_DoStage09:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage9SegmentRomStart	  , _GfxStage9SegmentRomEnd		)
	seqLoadText(SEGMENT_TEXTURE	  , _CFieldTextureSegmentRomStart , _CFieldTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_BACK	  , _BackMainmapSegmentRomStart	  , _BackMainmapSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_cSegmentRomStart	  , _GfxEnemy1_cSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_cSegmentRomStart	  , _HmsEnemy1_cSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_eSegmentRomStart	  , _GfxEnemy2_eSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_eSegmentRomStart	  , _HmsEnemy2_eSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_c)
		seqCall(SetEnemy2_e)

		seqHmsShape(S_tree1, RCP_HmsMainTree)	/*	Main tree.	*/

		seqHmsShape(S_movebg01, RCP_HmsPlMoveObj01)
		seqHmsShape(S_movebg02, RCP_HmsPlMoveObj02)
		seqHmsShape(S_movebg03, RCP_HmsPlMoveObj03)

		seqBeginScene(1, RCP_Stage9Scene1)
			seqCall(SEQ_Stage09_MoveBGs)
			seqCall(SEQ_Stage09_Enemies)
			seqCall(SEQ_Stage09_Stars)
			seqActor(S_NULL,  -6558,START_H, 6464,  0, 135,0,  0,10,0,  e_player_entpict)
			seqActor(S_NULL,    583,   2683,-5387,  0,-154,0,  0,11,0,  e_warp)
			seqActor(S_NULL,   1680,   3835,-5523,  0,-153,0,  0,12,0,  e_warp)
			seqActor(S_NULL,  -6612,   1024,-3351,  0, 107,0,  0,13,0,  e_warp)
			seqActor(S_NULL,   1980,    768, 6618,  0,-151,0,  0,14,0,  e_warp)
			seqPort(10, 9, 1, 10)					/*	MARIO stage in.	*/
			seqPort(11, 9, 1, 12)					/*	Warp.			*/
			seqPort(12, 9, 1, 11)					/*	Warp.			*/
			seqPort(13, 9, 1, 14)					/*	Warp.			*/
			seqPort(14, 9, 1, 13)					/*	Warp.			*/
			seqGameClear(6, 1,  50)
			seqGameOver (6, 1, 100)
			seqMapInfo(cx0901_info)
			seqTagInfo(cx0901_info_tag)
			seqMessage(SEQ_MESG_ENTRANT, 0)
			seqSetMusic(NA_STG_GROUND, NA_MAINMAP_BGM)
			seqEnvironment(ENV_PLAIN)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 135,  -6558,0,6464)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
