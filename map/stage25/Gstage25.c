/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage25 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE25_FOG_R		0
#define		STAGE25_FOG_G		0
#define		STAGE25_FOG_B		0
#define		STAGE25_FOG_START	980

#if CHINA
#include "../../i10n/assets/zh/cake_zh.h"
#else
#include "ext10/ending_texture.h"
#endif
#include "ext10/ending_shape.sou"
