/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 14 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							  Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage14


/* ================================================================================
		: Stage 14 Move BGs sequence.
===================================================================================	*/
SEQ_Stage14_MoveBGs:

		seqActor(S_NULL,  -1080, -840,1573,  0,0,0, 0,86,0, e_bar)

		seqActor(S_dossun, 	1919,	6191, 1919,  0, 225,0, 	0, 0,0,  e_dosun		)

		seqReturn


/* ================================================================================
		: Stage 14 Move Stars sequence.
===================================================================================	*/
SEQ_Stage14_Stars:

		seqLevelActor(0x003f,S_polystar, -1450, -1130, -1050, 0,0,0,  0,0,0,  e_tripstar) /* center */
		seqLevelActor(0x003f,S_polystar, -1850,   300,  -950, 0,0,0,  1,0,0,  e_tripstar) /* furiko */
		seqLevelActor(0x003f,S_polystar, -1300, -2250, -1300, 0,0,0,  2,0,0,  e_tripstar) /* hari */
		seqLevelActor(0x003f,S_polystar,  2200,  7300,  2210, 0,0,0,  3,0,0,  e_tripstar) /* top */
		seqLevelActor(0x003f,S_polystar, -1050,  2400,  -790, 0,0,0,  4,0,0,  e_tripstar) /* lift box */
		seqLevelActor(0x003f,S_NULL    ,  1815, -3200,   800, 0,0,0,  5,0,0,  e_tripstar_getcoins) /* 8coin     */

		seqReturn


/* ================================================================================
		: Stage 14 main sequence.
===================================================================================	*/
SEQ_DoStage14:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage14SegmentRomStart	  , _GfxStage14SegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE	  , _HMechaTextureSegmentRomStart , _HMechaTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_aSegmentRomStart	  , _GfxEnemy1_aSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_aSegmentRomStart	  , _HmsEnemy1_aSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_a)

		seqHmsShape(S_movebg01, RCP_HmsMechaObj01	)
		seqHmsShape(S_movebg02, RCP_HmsMechaObj02	)
		seqHmsShape(S_movebg03, RCP_HmsMechaObj03	)
		seqHmsShape(S_movebg04, RCP_HmsMechaObj04_l	)
		seqHmsShape(S_movebg05, RCP_HmsMechaObj04_s	)
		seqHmsShape(S_movebg06, RCP_HmsMechaObj05	)
		seqHmsShape(S_movebg07, RCP_HmsMechaObj06	)
		seqHmsShape(S_movebg08, RCP_HmsMechaObj07	)
		seqHmsShape(S_movebg09, RCP_HmsMechaObj08_a	)
		seqHmsShape(S_movebg10, RCP_HmsMechaObj08_b	)
		seqHmsShape(S_movebg11, RCP_HmsMechaObj09	)
		seqHmsShape(S_movebg12, RCP_HmsMechaObj10	)
		seqHmsShape(S_movebg13, RCP_HmsMechaObj11	)
		seqHmsShape(S_movebg14, RCP_HmsMechaObj12s	)
		seqHmsShape(S_movebg15, RCP_HmsMechaObj12l	)

		seqBeginScene(1, RCP_Stage14Scene1)
			seqActor(S_NULL,  1417,-4822+START_H,-548,  0,316,0,  0,10,0,  e_player_entpict)
			seqPort(10, 14, 1, 10)					/*	MARIO stage in.	*/
			seqGameClear(6, 2,  53)
			seqGameOver (6, 2, 103)
			seqCall(SEQ_Stage14_MoveBGs)
			seqCall(SEQ_Stage14_Stars)
			seqMapInfo(cx1401_info)
			seqTagInfo(cx1401_info_tag)
			seqSetMusic(NA_STG_CASTLE, NA_SLIDER_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 316, 1417,-4822,-548)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
