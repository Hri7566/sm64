/********************************************************************************
                   -------------------------									  
                    Ultra 64 MARIO Brothers									  
                   -------------------------									  
																				  
                    File          : pathmacs.h								  
                    Description   : pathdata command macros					  
                    Date          : 1995.x.x									  
                    Author        : H.yajima									  
																				  
********************************************************************************/


/********************************************************************************
		ENUM LABEL LIST			 						 						  
********************************************************************************/

#define	stw_imm                   0
#define	stw_flag                  1
#define	stw_enemyflag             2
#define	stw_my_status             3
#define	stw_hit_status            4
#define	stw_hit_timer             5
#define	stw_worldX                6
#define	stw_worldY                7
#define	stw_worldZ                8
#define	stw_speedX                9
#define	stw_speedY                10
#define	stw_speedZ                11
#define	stw_speedF                12
#define	stw_speedL                13
#define	stw_speedU                14
#define	stw_angleX                15
#define	stw_angleY                16
#define	stw_angleZ                17
#define	stw_animeangleX           18
#define	stw_animeangleY           19
#define	stw_animeangleZ           20
#define	stw_animepositionY        21
#define	stw_effect                22
#define	stw_gravity               23
#define	stw_groundY               24
#define	stw_movestatus            25
#define	stw_animecounter          26
#define	stw_work0                 27
#define	stw_work1                 28
#define	stw_work2                 29
#define	stw_work3                 30
#define	stw_work4                 31
#define	stw_work5                 32
#define	stw_work6                 33
#define	stw_work7                 34
#define	stw_anglespeedX           35
#define	stw_anglespeedY           36
#define	stw_anglespeedZ           37
#define	stw_skelanime             38
#define	stw_actionmode            39
#define	stw_wall_offsetR          40
#define	stw_air_fric              41
#define	stw_objname               42
#define	stw_mail                  43
#define	stw_skeletonX             44
#define	stw_skeletonY             45
#define	stw_skeletonZ             46
#define	stw_programselect         47
#define	stw_breakflag             48
#define	stw_mode                  49
#define	stw_process               50
#define	stw_timer                 51
#define	stw_bound_e               52
#define	stw_playerdist            53
#define	stw_targetangle           54
#define	stw_worldX_attention      55
#define	stw_worldY_attention      56
#define	stw_worldZ_attention      57
#define	stw_friction              58
#define	stw_specificG             59
#define	stw_skelanimeNo           60
#define	stw_alpha                 61
#define	stw_ap                    62
#define	stw_hp                    63
#define	stw_actorcode             64
#define	stw_oldmode               65
#define	stw_enemyinfo             66
#define	stw_movebg_checkdist      67
#define	stw_havecoin              68
#define	stw_shapeLOD              69
#define	stw_areamap_info          70
#define	stw_enemytype             71
#define	stw_taginfo               72
#define	stw_work8                 73
#define	stw_work9                 74
#define	stw_wallangleY            75
#define	stw_bgparam               76
#define	stw_returnangle           77
#define	stw_bgpointer             78
#define	stw_soundparam            79
/*-----------------------------------------------------------------------------*/
#define	look_player1               0
#define	look_player2               1
#define	look_mother                2
#define	look_nearshape             3
#define	look_nearenemy             4
#define	look_nearplayer            5
#define	look_neartarget            6
/*-----------------------------------------------------------------------------*/
#define	CLEARSTAR_PLAYERPOS             0
#define	CLEARSTAR_NOWPOS                1
#define	CLEARSTAR_LOWSTAR               2
#define	CLEARSTAR_RAILSTAR              3
/*-----------------------------------------------------------------------------*/
#define	CHILEDMODE_NO_CATCH              0
#define	CHILEDMODE_CATCH                 1
#define	CHILEDMODE_THROW                 2
#define	CHILEDMODE_DROP                  3
#define	CHILEDMODE_RESET                 4
/*-----------------------------------------------------------------------------*/
#define	ENFLAG_FLY                   1
#define	ENFLAG_DOWN                  2
/*-----------------------------------------------------------------------------*/
#define	BGTYPE_MOVEBG                1
#define	BGTYPE_BGCHECK_CAMERASKIP    2
#define	BGTYPE_BGCHECK_PLAYERSKIP    4
#define	BGTYPE_BGCHECK_WALL_XTYPE    8
/*-----------------------------------------------------------------------------*/
#define	MOVESTAT_BGBOUND               1
#define	MOVESTAT_BGTOUCH               2
#define	MOVESTAT_BGTAKEOFF             4
#define	MOVESTAT_DIVE                  8
#define	MOVESTAT_SURFACEWATER          16
#define	MOVESTAT_UNDERWATER            32
#define	MOVESTAT_BOTTOMWATER           64
#define	MOVESTAT_SKY                   128
#define	MOVESTAT_OUTSCOPE              256
#define	MOVESTAT_WALL                  512
#define	MOVESTAT_GAKE                  1024
#define	MOVESTAT_FIRE                  2048
#define	MOVESTAT_WATERJUMP             4096
#define	MOVESTAT_BIGBOUND              8192
#define	MOVESTAT_DEATHCODE             16384
/*-----------------------------------------------------------------------------*/
#define	STRATMAIN_COINCLEAR             1
#define	STRATMAIN_DEMOMODE              2
#define	STRATMAIN_ENEMYSTOP             4
#define	STRATMAIN_PLAYERSTOP            8
#define	STRATMAIN_ALLSTOP               16
#define	STRATMAIN_DOOROPEN              32
#define	STRATMAIN_DEMOMODE_OK           64
/*-----------------------------------------------------------------------------*/
#define	OBJNAME_BIRD                  1
#define	OBJNAME_TAKE                  2
#define	OBJNAME_DOOR                  4
#define	OBJNAME_DAMAGE                8
#define	OBJNAME_COIN                  16
#define	OBJNAME_ITEMHAT               32
#define	OBJNAME_BAR                   64
#define	OBJNAME_NOKONOKO              128
#define	OBJNAME_TOGEZO                256
#define	OBJNAME_PUNCHITEM             512
#define	OBJNAME_WIND                  1024
#define	OBJNAME_TRIPDOOR              2048
#define	OBJNAME_TRIPSTAR              4096
#define	OBJNAME_TRIPCHIMNEY           8192
#define	OBJNAME_CANNON                16384
#define	OBJNAME_PUNCHATTACK           32768
#define	OBJNAME_RECOVER               65536
#define	OBJNAME_MOTOS                 131072
#define	OBJNAME_FIRE                  262144
#define	OBJNAME_NOKOBOARD             524288
#define	OBJNAME_HANACYAN              1048576
#define	OBJNAME_WANWAN                2097152
#define	OBJNAME_HEYHO                 4194304
#define	OBJNAME_FRIEND                8388608
#define	OBJNAME_TORNEDO               16777216
#define	OBJNAME_DRAINPIPE             33554432
#define	OBJNAME_SHELL                 67108864
#define	OBJNAME_TRIPKAGO              134217728
#define	OBJNAME_TAMA                  268435456
#define	OBJNAME_BIRIBIRI              536870912
/*-----------------------------------------------------------------------------*/
#define	ENEMYINFO_ROBCOIN               1
#define	ENEMYINFO_DAMAGEOFF             2
#define	ENEMYINFO_CARRY                 4
#define	ENEMYINFO_KUPPA                 8
#define	ENEMYINFO_FRIEND_ENEMY          16
#define	ENEMYINFO_AUTO_DOOR             32
#define	ENEMYINFO_CATCH_OFF_REQUEST     64
#define	ENEMYINFO_FURAFURA              128
#define	ENEMYINFO_BOMB                  256
#define	ENEMYINFO_HORIAGE               512
#define	ENEMYINFO_EXTSTAR               1024
#define	ENEMYINFO_FINALSTAR             2048
#define	ENEMYINFO_KANBAN                4096
#define	ENEMYINFO_EAT                   8192
#define	ENEMYINFO_MESSAGE               16384
/*-----------------------------------------------------------------------------*/
#define	objtype_player                0
#define	objtype_allfire               1
#define	objtype_plfire                2
#define	objtype_enfire                3
#define	objtype_enemyA                4
#define	objtype_enemyB                5
#define	objtype_item                  6
#define	objtype_build                 7
#define	objtype_option                8
#define	objtype_moveBG                9
#define	objtype_player_friend         10
#define	objtype_control               11
#define	objtype_effect                12
#define	objtype_totalcount            13
/*-----------------------------------------------------------------------------*/
#define	code_player                1
#define	code_enemy                 2
#define	code_zako                  4
#define	code_kabe                  8
/*-----------------------------------------------------------------------------*/
#define	OBJECT_ON                    1
#define	OBJECT_ERASE                 2
#define	OBJECT_CALC_MOVEBG           4
#define	OBJECT_AREAOUT               8
#define	OBJECT_DEMOMOVEOBJ           16
#define	OBJECT_DEMOMOVEENTRY         32
#define	OBJECT_GHOST                 64
#define	OBJECT_DITHER                128
#define	OBJECT_INITIAL               256
#define	OBJECT_PLFIRE_OFF            512
#define	OBJECT_NOWATER               1024
/*-----------------------------------------------------------------------------*/
#define	ENEMYDEMO_LOOKPLAYER            1
#define	ENEMYDEMO_SETMESSAGE            2
#define	ENEMYDEMO_SETMESSAGE_YESNO      4
#define	ENEMYDEMO_CAMERA                8
#define	ENEMYDEMO_DLOG_NOSTOP           16
/*-----------------------------------------------------------------------------*/
#define	stf_smoke                 1
#define	stf_splash                2
#define	stf_sleep                 4
#define	stf_twinkle               8
#define	stf_crash                 16
#define	stf_bubble                32
#define	stf_diving                64
#define	stf_ripple                128
#define	stf_sink                  256
#define	stf_bubblejet             512
#define	stf_wave                  1024
#define	stf_firesmoke             2048
#define	stf_jumpsink              4096
#define	stf_grass                 8192
#define	stf_sand                  16384
#define	stf_mowan                 32768
#define	stf_snow                  65536
#define	stf_breath                131072
#define	stf_wallcrash             262144
#define	stf_punchcrash            524288
/*-----------------------------------------------------------------------------*/
#define	stf_moveON                1
#define	stf_FspeedON              2
#define	stf_YspeedON              4
#define	stf_YangleSAME            8
#define	stf_ALLangleSAME          16
#define	stf_createON              32
#define	stf_playerdistON          64
#define	stf_alldispON             128
#define	stf_playerON              256
#define	stf_relativeON            512
#define	stf_catchON               1024
#define	stf_matrixON              2048
#define	stf_programstopON         4096
#define	stf_playerangleON         8192
#define	stf_NOramsave             16384
/*-----------------------------------------------------------------------------*/
#define	stratmode_player1               1
#define	stratmode_player2               2
#define	stratmode_program               4
#define	stratmode_actorset_object       8
#define	stratmode_wakidashi_on          16
#define	stratmode_enemy_dead            32
/*-----------------------------------------------------------------------------*/
#define	mks_exit                  0
#define	mks_root                  1
#define	mks_begin                 2
#define	mks_end                   3
#define	mks_joint                 4
/*-----------------------------------------------------------------------------*/
#define	ACTORTYPE_MAKEOBJ               0
#define	ACTORTYPE_ACTOR                 1
#define	ACTORTYPE_TAG                   2
/*-----------------------------------------------------------------------------*/
#define	MODE_SWITCH_OFF                   0
#define	MODE_SWITCH_ON_ANIME              1
#define	MODE_SWITCH_ON                    2
#define	MODE_SWITCH_OFF_ANIME             3
#define	MODE_SWITCH_OFF_WAIT              4
/*-----------------------------------------------------------------------------*/
#define	BURNSET_NOTHING               0
#define	BURNSET_NORMAL_COIN           1
#define	BURNSET_ESCAPE_COIN           2
/*-----------------------------------------------------------------------------*/
#define	ITEMCODE_HAT_WING              0
#define	ITEMCODE_HAT_METAL             1
#define	ITEMCODE_HAT_ERASE             2
#define	ITEMCODE_NOKO                  3
#define	ITEMCODE_COIN_1                4
#define	ITEMCODE_COIN_3                5
#define	ITEMCODE_COIN_10               6
#define	ITEMCODE_1UP                   7
#define	ITEMCODE_GOALSTAR              8
#define	ITEMCODE_1UP_ESCAPE            9
#define	ITEMCODE_GOALSTAR_1            10
#define	ITEMCODE_GOALSTAR_2            11
#define	ITEMCODE_GOALSTAR_3            12
#define	ITEMCODE_GOALSTAR_4            13
#define	ITEMCODE_GOALSTAR_5            14
#define	ITEMCODE_GOALSTAR_6            15
/*-----------------------------------------------------------------------------*/

/********************************************************************************
		PATH PROGRAM MACRO														  
********************************************************************************/

#define			stw_erasedist		stw_movebg_checkdist
#define		MOVESTAT_NOSKY	(MOVESTAT_BGBOUND+MOVESTAT_BGTOUCH+MOVESTAT_UNDERWATER+MOVESTAT_SURFACEWATER)
#define		MOVESTAT_GROUND	(MOVESTAT_BGBOUND+MOVESTAT_BGTOUCH)
#define		MOVESTAT_WATER	(MOVESTAT_DIVE+MOVESTAT_SURFACEWATER+MOVESTAT_UNDERWATER+MOVESTAT_BOTTOMWATER)
#define		MOVESTAT_STOP	(MOVESTAT_SURFACEWATER+MOVESTAT_BOTTOMWATER+MOVESTAT_BGTOUCH)
#define				OBJNAME_KAMAKURA	(0x40000000)
#define			OBJNAME_0	0
#define			OBJNAME_KURIBO		OBJNAME_PUNCHATTACK
#define			OBJNAME_KILLER		OBJNAME_DAMAGE
#define			OBJNAME_BIGSTONE	OBJNAME_PUNCHATTACK
#define			objtype_enemyC	objtype_item
#define			STF_HITINITIAL	(1<<30)
#define		ITEMCODE_END	99
#define		allbit				0xffff
#define		p_animereset		p_setd(animecounter,-1)
#define		p_animeinc			p_addd(animecounter,1)
#define		p_animeset(n)		p_setd(animecounter,(n))
#define		p_hitOFF			p_setd(hit_timer,-1)
#define		p_hitON				p_setd(hit_timer,0)
#define		p_setname(name)		p_setd(objname,OBJNAME_##name)
#define		p_movebginfo(addr)	p_setshapeinfo(addr)
#define		_pcmd(num)					((num)<<24)
#define		_pindex(num,index,work)		((num)<<24)+((index)<<16)+((work)&0xffff)
#define		_pdata(dataH,dataL)			(((dataH)&0xffff)<<16)+((dataL)&0xffff)
#define		_pbyte(c0,c1,c2,c3)			((c0)<<24)+((c1)<<16)+((c2)<<8)+(c3)
#define		p_killobj					p_killshape

/********************************************************************************
		PATH COMMAND INDEX LIST													  
********************************************************************************/

#define	pc_initialize                                         0
#define	pc_wait                                               1
#define	pc_jsr                                                2
#define	pc_rts                                                3
#define	pc_jmp                                                4
#define	pc_do                                                 5
#define	pc_next                                               6
#define	pc_next_imm                                           7
#define	pc_while                                              8
#define	pc_loop                                               9
#define	pc_end                                                10
#define	pc_exit                                               11
#define	pc_program                                            12
#define	pc_addf                                               13
#define	pc_setf                                               14
#define	pc_addd                                               15
#define	pc_setd                                               16
#define	pc_setbit                                             17
#define	pc_clrbit                                             18
#define	pc_setangle_random                                    19
#define	pc_setf_random                                        20
#define	pc_setd_random                                        21
#define	pc_addf_random                                        22
#define	pc_addd_random                                        23
#define	pc_dprintf                                            24
#define	pc_dprintx                                            25
#define	pc_dprintd                                            26
#define	pc_changeshape                                        27
#define	pc_makeshape                                          28
#define	pc_killshape                                          29
#define	pc_BGcheckYset                                        30
#define	pc_WADDf                                              31
#define	pc_WADDd                                              32
#define	pc_softspritemodeON                                   33
#define	pc_shapeOFF                                           34
#define	pc_sethitbox                                          35
#define	pc_softspriteanime                                    36
#define	pc_wait_work                                          37
#define	pc_do_work                                            38
#define	pc_set_pointer                                        39
#define	pc_set_skelanime_number                               40
#define	pc_makeobj                                            41
#define	pc_setshapeinfo                                       42
#define	pc_sethitbox2                                         43
#define	pc_makeobj_child                                      44
#define	pc_save_nowpos                                        45
#define	pc_setdamagebox                                       46
#define	pc_setobjname                                         47
#define	pc_setmovedata                                        48
#define	pc_setobjinfo                                         49
#define	pc_set_scale                                          50
#define	pc_mother_clrbit                                      51
#define	pc_inc_frame                                          52
#define	pc_shapeDISABLE                                       53
#define	pc_setlong                                            54
#define	pc_makeobj_initdata                                   55

/********************************************************************************
		PATH COMMAND MACRO														  
********************************************************************************/

#define	p_initialize(type)                                 	.word _pindex(pc_initialize,objtype_##type,0)
#define	p_wait(var)                                        	.word _pindex(pc_wait,0,var)
#define	p_jsr(addr)                                        	.word _pcmd(pc_jsr),addr
#define	p_rts                                              	.word _pcmd(pc_rts)
#define	p_jmp(addr)                                        	.word _pcmd(pc_jmp),addr
#define	p_do(var)                                          	.word _pindex(pc_do,0,var)
#define	p_next                                             	.word _pcmd(pc_next)
#define	p_next_imm                                         	.word _pcmd(pc_next_imm)
#define	p_while                                            	.word _pcmd(pc_while)
#define	p_loop                                             	.word _pcmd(pc_loop)
#define	p_end                                              	.word _pcmd(pc_end)
#define	p_exit                                             	.word _pcmd(pc_exit)
#define	p_program(addr)                                    	.word _pcmd(pc_program),addr
#define	p_addf(index,work)                                 	.word _pindex(pc_addf,stw_##index,work)
#define	p_setf(index,work)                                 	.word _pindex(pc_setf,stw_##index,work)
#define	p_addd(index,work)                                 	.word _pindex(pc_addd,stw_##index,work)
#define	p_setd(index,work)                                 	.word _pindex(pc_setd,stw_##index,work)
#define	p_setbit(index,flag)                               	.word _pindex(pc_setbit,stw_##index,flag)
#define	p_clrbit(index,flag)                               	.word _pindex(pc_clrbit,stw_##index,flag)
#define	p_setangle_random(index,base,offset)               	.word _pindex(pc_setangle_random,stw_##index,base),_pdata(offset,0)
#define	p_setf_random(index,base,offset)                   	.word _pindex(pc_setf_random,stw_##index,base),_pdata(offset,0)
#define	p_setd_random(index,base,offset)                   	.word _pindex(pc_setd_random,stw_##index,base),_pdata(offset,0)
#define	p_addf_random(index,base,offset)                   	.word _pindex(pc_addf_random,stw_##index,base),_pdata(offset,0)
#define	p_addd_random(index,base,offset)                   	.word _pindex(pc_addd_random,stw_##index,base),_pdata(offset,0)
#define	p_dprintf(index)                                   	.word _pindex(pc_dprintf,stw_##index,0)
#define	p_dprintx(index)                                   	.word _pindex(pc_dprintx,stw_##index,0)
#define	p_dprintd(index)                                   	.word _pindex(pc_dprintd,stw_##index,0)
#define	p_changeshape(index,shape)                         	.word _pindex(pc_changeshape,stw_##index,shape)
#define	p_makeshape(shape,strat)                           	.word _pcmd(pc_makeshape),shape,strat
#define	p_killshape                                        	.word _pcmd(pc_killshape)
#define	p_BGcheckYset                                      	.word _pcmd(pc_BGcheckYset)
#define	p_WADDf(s1,s2,s3)                                  	.word _pbyte(pc_WADDf,stw_##s1,stw_##s2,stw_##s3)
#define	p_WADDd(s1,s2,s3)                                  	.word _pbyte(pc_WADDd,stw_##s1,stw_##s2,stw_##s3)
#define	p_softspritemodeON                                 	.word _pcmd(pc_softspritemodeON)
#define	p_shapeOFF                                         	.word _pcmd(pc_shapeOFF)
#define	p_sethitbox(Rdata,Hdata)                           	.word _pcmd(pc_sethitbox),_pdata(Rdata,Hdata)
#define	p_softspriteanime(animeNo)                         	.word _pindex(pc_softspriteanime,0,animeNo)
#define	p_wait_work(work)                                  	.word _pindex(pc_wait_work,stw_##work,0)
#define	p_do_work(work)                                    	.word _pindex(pc_do_work,stw_##work,0)
#define	p_set_pointer(work,addr)                           	.word _pindex(pc_set_pointer,stw_##work,0),addr
#define	p_set_skelanime_number(n)                          	.word _pindex(pc_set_skelanime_number,n,0)
#define	p_makeobj(shape,strat,number)                      	.word _pindex(pc_makeobj,0,number),shape,strat
#define	p_setshapeinfo(addr)                               	.word _pcmd(pc_setshapeinfo),addr
#define	p_sethitbox2(Rdata,Hdata,Ldata)                    	.word _pcmd(pc_sethitbox2),_pdata(Rdata,Hdata),_pdata(Ldata,0)
#define	p_makeobj_child(shape,strat)                       	.word _pcmd(pc_makeobj_child),shape,strat
#define	p_save_nowpos                                      	.word _pcmd(pc_save_nowpos)
#define	p_setdamagebox(Rdata,Hdata)                        	.word _pcmd(pc_setdamagebox),_pdata(Rdata,Hdata)
#define	p_setobjname(name)                                 	.word _pcmd(pc_setobjname),OBJNAME_##name
#define	p_setmovedata(d0,d1,d2,d3,d4,d5,d6,d7)             	.word _pcmd(pc_setmovedata),_pdata(d0,d1),_pdata(d2,d3),_pdata(d4,d5),_pdata(d6,d7)
#define	p_setobjinfo(name)                                 	.word _pcmd(pc_setobjinfo),ENEMYINFO_##name
#define	p_set_scale(scale)                                 	.word _pindex(pc_set_scale,0,scale)
#define	p_mother_clrbit(index,flag)                        	.word _pindex(pc_mother_clrbit,stw_##index,0),flag
#define	p_inc_frame(index,frame)                           	.word _pindex(pc_inc_frame,stw_##index,frame)
#define	p_shapeDISABLE                                     	.word _pcmd(pc_shapeDISABLE)
#define	p_setlong(index,data)                              	.word _pindex(pc_setlong,stw_##index,0),data
#define	p_makeobj_initdata(addr)                           	.word _pcmd(pc_makeobj_initdata),addr


/*===============================================================================
		End Of File												                  
===============================================================================*/
