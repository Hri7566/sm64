/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 24 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage24


/* ================================================================================
		: BG parts sequence.
===================================================================================	*/
SEQ_Stage24_BGparts:

		seqActor(S_bg01,  2305, 2432, -255,   0,0,0,  0,0,0,  e_stop)
		seqActor(S_bg02,  3405, 1664,-1791,   0,0,0,  0,0,0,  e_stop)
		seqActor(S_bg03,  3840,    0,-2303,   0,0,0,  0,0,0,  e_stop)
		seqActor(S_bg03,  3840,    0,-1279,   0,0,0,  0,0,0,  e_stop)
		seqActor(S_bg04,     0,    0,    0,   0,0,0,  0,0,0,  e_stop)
		seqActor(S_bg05,  1757, 3519,-3151,   0,0,0,  0,0,0,  e_stop)
		seqActor(S_bg08,  3840,  794, 2688,   0,0,0,  0,0,0,  e_stop)
		seqActor(S_bg10,  1408, 2522, 2431,   0,0,0,  0,0,0,  e_stop)
		seqActor(S_bg11, -2560, 2560, -256,   0,0,0,  0,0,0,  e_ballbar)

		seqReturn


/* ================================================================================
		: Move BG parts sequence.
===================================================================================	*/
SEQ_Stage24_MoveBGs:

		seqActor(S_dossunbar,  		 3300,1070,    1,   0, 90,0,   0, 0,0,   e_dossun_bar	)
		seqActor(S_dossunbar,  		 3300,1070, 1281,   0, 90,0,   0, 0,0,   e_dossun_bar	)
		seqActor(S_dossunbar2,  	 3300,1070,  641,   0,  0,0,   0, 0,0,   e_dossun_bar2	)
		seqActor(S_rotebar,  		 -255,2560, 2304,   0,  0,0,   0, 0,0,   e_castle_rotbar)
		seqActor(S_transbar_50,  	 3328,1075,-1791,   0, 90,0,   0, 2,0,   e_trans_bar	 )
		seqActor(S_transbar_50,  	 3328,1075, -767,   0, 90,0,   0, 1,0,   e_trans_bar	 )
		seqActor(S_transbar_50,  	 3328,1075,-2815,   0, 90,0,   0, 3,0,   e_trans_bar	 )
		seqActor(S_down_bridge_far,  1792,2496, 1600,   0,  0,0,   0, 0,0,   e_castle_downbar)

		seqActor(S_movebg01,    512,2176,2944,	0,0,0,  0,0,0,  e_castle_attackwall_1)
		seqActor(S_movebg02,  -1023,2176,2944,	0,0,0,  0,0,0,  e_castle_attackwall_2)

		seqLevelActor(0x003e,  S_movebg03,        13,3584,-1407,  0,315,0,  0,0,0,  e_castle_attackbridge)
		seqLevelActor(0x003e,  S_oneup_kinoko,  -384,3584,    6,  0,  0,0,  0,0,0,  e_1up_kinoko_stop)

		seqActor(S_bg14,   2304,3584,-2303,   0,0,0,   8,0,0,   e_rotland)
		seqActor(S_bg14,   3200,3328,-1791,   0,0,0,   8,0,0,   e_rotland)
		seqActor(S_bg14,   2688,3584, -895,   0,0,0,   8,0,0,   e_rotland)

		seqActor(S_NULL,  -2495,1331,-256,   0,0,0,   0,61,0,   e_bar)

		seqReturn


/* ================================================================================
		: Enemies sequence.
===================================================================================	*/
SEQ_Stage24_Enemies:

		seqActor(S_dossun, 		  3462,	1939, -1545,  0,180,0, 	0, 0,0,  e_dosun_power	)
		seqActor(S_dossun, 		  3462,	1075, -3314,  0, 90,0, 	0, 0,0,  e_dosun		)
		seqActor(S_NULL, 		  -856,  922,  3819,  0,  0,0, 	0, 0,0,  e_funsui		)
		seqActor(S_pakun, 		  1822, 2560,  -101,  0, 90,0,  0, 0,0,  e_pakun		)
		seqActor(S_pakun, 		  4625,  256,  5017,  0,-90,0,  0, 0,0,  e_pakun		)
		seqActor(S_pakun, 		   689, 2560, 1845,   0, 90,0,  0, 0,0,  e_pakun		)
		seqActor(S_wallman,		 -1545,	2560,  -286,  0,  0,0,  0, 0,0,  e_wallman		)		
		seqActor(S_wallman,		   189,	2560, -1857,  0,-135,0, 0, 0,0,  e_wallman		)		
		seqActor(S_butterfly,     4736,  256,  4992,  0,  0,0,  0, 0,0,  e_butterfly	)
		seqActor(S_butterfly,     4608,  256,  5120,  0,  0,0,  0, 0,0,  e_butterfly	)
		seqActor(S_butterfly,     4608,  256,  4992,  0,  0,0,  0, 0,0,  e_butterfly	)
		seqActor(S_butterfly,     4608,  256,  4864,  0,  0,0,  0, 0,0,  e_butterfly	)
		seqActor(S_butterfly,     4480,  256,  4992,  0,  0,0,  0, 0,0,  e_butterfly	)
		seqActor(S_butterfly,     4608,  256,   256,  0,  0,0,  0, 0,0,  e_butterfly	)
		seqActor(S_butterfly,     4736,  256,   128,  0,  0,0,  0, 0,0,  e_butterfly	)
		seqActor(S_butterfly,     4480,  256,   128,  0,  0,0,  0, 0,0,  e_butterfly	)
		seqActor(S_butterfly,     4608,  256,     0,  0,  0,0,  0, 0,0,  e_butterfly	)
		seqActor(S_butterfly,     4608,  256,   128,  0,  0,0,  0, 0,0,  e_butterfly	)

		seqActor(S_NULL,		  1035,2880, -900,  0,45,0,  0,0,0,  e_doublelift)

		seqLevelActor(0x003e,S_killer,   1280,3712,  968,   0,180,0,  0,0,0,  e_killer				)
		seqLevelActor(0x003e,S_bg06,        0,3584,    0,   0,  0,0,  0,0,0,  e_stage24_tower_check )
		seqLevelActor(0x003e,S_bg07,  	 1280,3584,  896,   0,  0,0,  0,0,0,  e_stage24_killer_check)
		seqLevelActor(0x003e,S_NULL, 	 	0,3483,	   0,   0,  0,0,  0,0,0,  e_castle_goalbar		)
		seqLevelActor(0x003e,S_movebg04, -511,3584,	   0,	0,  0,0,  0,0,0,  e_castle_futa	 		)
		seqLevelActor(0x003c,S_redbom,  -1700,1140,	3500,	0,  0,0,  0,0,0,  e_futa_bom )
		seqLevelActor(0x003c,S_bird,  	 2560, 700,	4608,	0,  0,0,  0,0,0,  e_bird 				)

		seqReturn


/* ================================================================================
		: Stars sequence.
===================================================================================	*/
SEQ_Stage24_Stars:

		seqLevelActor(0x0001,S_wallman,	     0, 3584,     0,  0,0,0,  0,0,0,  e_wallman_boss )	/* itiro	*/
		seqLevelActor(0x003e,S_polystar,   300, 5550,     0,  0,0,0,  1,0,0,  e_tripstar)		/* top		*/
		seqLevelActor(0x003f,S_polystar, -2500, 1500,  -750,  0,0,0,  2,0,0,  e_tripstar)		/* taihou	*/
		seqLevelActor(0x003f,S_NULL	   ,  4600,  550,  2500,  0,0,0,  3,0,0,  e_tripstar_getcoins)	/* 8 coin	*/		
		seqLevelActor(0x003f,S_polystar,  2880, 4300,   190,  0,0,0,  4,0,0,  e_tripstar)		/* bird	de	*/
		seqLevelActor(0x003f,S_polystar,   590, 2450,  2650,  0,0,0,  5,0,0,  e_tripstar)		/* in the wall */

		seqReturn


/* ================================================================================
		: Stage 24 [ Mountain ] main sequence.
===================================================================================	*/
SEQ_DoStage24:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage24SegmentRomStart    , _GfxStage24SegmentRomEnd	)
	seqLoadPres(SEGMENT_BACK  	  , _BackMountainSegmentRomStart  , _BackMountainSegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE   , _JFortTextureSegmentRomStart  , _JFortTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_aSegmentRomStart   , _GfxEnemy1_aSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_aSegmentRomStart   , _HmsEnemy1_aSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_eSegmentRomStart   , _GfxEnemy2_eSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_eSegmentRomStart   , _HmsEnemy2_eSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()
		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_a)
		seqCall(SetEnemy2_e)

		seqHmsShape(S_tree1, RCP_HmsMainTree)

		seqHmsShape(S_bg01, RCP_HmsHfCastle2BG01)
		seqHmsShape(S_bg02, RCP_HmsHfCastle2BG02)
		seqHmsShape(S_bg03, RCP_HmsHfCastle2BG03)
		seqHmsShape(S_bg04, RCP_HmsHfCastle2BG04)
		seqHmsShape(S_bg05, RCP_HmsHfCastle2BG05)
		seqHmsShape(S_bg06, RCP_HmsHfCastle2BG06)
		seqHmsShape(S_bg07, RCP_HmsHfCastle2BG07)
		seqHmsShape(S_bg08, RCP_HmsHfCastle2BG08)
		seqHmsShape(S_bg10, RCP_HmsHfCastle2BG10)
		seqHmsShape(S_bg11, RCP_HmsMappingBar	)
		seqHmsShape(S_bg12, RCP_HmsCaBG12		)
		seqHmsShape(S_bg13, RCP_HmsCaBG13		)
		seqHmsShape(S_bg14, RCP_HmsCaBG14		)
		seqHmsShape(S_bg15, RCP_HmsCaBG15		)
		seqHmsShape(S_bg16, RCP_HmsCaBG16		)

		seqHmsShape(S_dossunbar  	 , RCP_HmsDossunBar	)
		seqHmsShape(S_dossunbar2 	 , RCP_HmsDossunBar2)
		seqHmsShape(S_rotebar    	 , RCP_HmsRoteBar	)
		seqHmsShape(S_transbar_50	 , RCP_HmsTransbar50)
		seqHmsShape(S_down_bridge	 , RCP_HmsDownBNear	)
		seqHmsShape(S_down_bridge_far, RCP_HmsDownBFar	)

		seqHmsShape(S_lift0, RCP_HmsLift0)
		seqHmsShape(S_lift1, RCP_HmsLift1)
		seqHmsShape(S_lift2, RCP_HmsLift2)
		seqHmsShape(S_lift3, RCP_HmsLift3)

		seqHmsShape(S_movebg01, RCP_HmsCasMobj01)
		seqHmsShape(S_movebg02, RCP_HmsCasMobj02)
		seqHmsShape(S_movebg03, RCP_HmsCasMobj03)
		seqHmsShape(S_movebg04, RCP_HmsCasMobj04)
		seqHmsShape(S_movebg05,	RCP_HmsCasMobj03_ns)

		seqBeginScene(1, RCP_Stage24Scene1)
			seqActor(S_NULL,   2600,256+START_H, 5120,  0,90,0,  0,10,0,  e_player_entpict)
			seqActor(S_NULL,  -2925,	   2560, -947,  0,19,0,  0,11,0,  e_warp)
			seqActor(S_NULL,   2548,	   1075,-3962,  0,51,0,  0,12,0,  e_warp)
			seqPort(10, 24, 1, 10)					/*	MARIO stage in.	*/
			seqPort(11, 24, 1, 12)					/*	Warp.			*/
			seqPort(12, 24, 1, 11)					/*	Warp.			*/
			seqGameClear(6, 1,  52)
			seqGameOver (6, 1, 102)
			seqCall(SEQ_Stage24_BGparts)
			seqCall(SEQ_Stage24_MoveBGs)
			seqCall(SEQ_Stage24_Enemies)
			seqCall(SEQ_Stage24_Stars)
			seqMapInfo(cx2401_info)
			seqTagInfo(cx2401_info_tag)
			seqMessage(SEQ_MESG_ENTRANT, 30)
			seqSetMusic(NA_STG_MOUNTAIN_24, NA_MAINMAP_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 90,  2600,256,5120)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
