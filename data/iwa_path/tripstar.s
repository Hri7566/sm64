/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Strategy

					March 31 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/*===============================================================================
						New Star Path Data
================================================================================*/
e_tripstar:
	p_initialize(item)
	p_setbit( flag,stf_moveON )
	p_program(s_areastage_init)
	p_program(s_polystar_init)

	p_while
		p_program(s_polystar_main)
	p_loop


/*===============================================================================*/
e_enemy_star:
	p_initialize(item)
	p_setbit( flag,stf_moveON )
	p_program(s_polystar_init)
	p_program(s_enemy_star_init)

	p_while
		p_program(s_enemy_star_main)
	p_loop

/*===============================================================================*/
e_tripstar_getcoins:
	p_initialize(item)
	p_setbit( flag,stf_moveON | stf_NOramsave)
	p_program(s_getcoins_star_init)

	p_while
		p_program(s_getcoins_star_main)
	p_loop

e_coin_appstar:
	p_initialize(item)
	p_setbit(flag,stf_moveON)
	p_softspritemodeON
	p_hitON
	p_animereset
	p_program(s_areastage_init)
	p_program(s_coin_appear_init)

	p_while
		p_program(s_coin_appear_main)
		p_animeinc
	p_loop

e_extstar_getcoins:
	p_initialize(item)
	p_setbit( flag,stf_moveON | stf_NOramsave)

	p_while
		p_program(s_getcoins_extstar_main)
	p_loop

/*===============================================================================*/
e_tripstar_getdummy:
	p_initialize(item)
	p_setbit( flag,stf_moveON | stf_NOramsave)
	p_program(s_getdummy_star_init)

	p_while
		p_program(s_getdummy_star_main)
	p_loop

e_dummy_appstar:
	p_initialize(item)
	p_setbit(flag,stf_moveON)
	p_sethitbox(100,100)
	p_hitON

	p_while
		p_program(s_dummy_appear)
	p_loop


/*################*/
#else
/*################*/


/*==================================================================================================
		C Program
 ===================================================================================================*/
static HitCheckRecord polystar_hit = {
	OBJNAME_TRIPSTAR,
	0,0,0,0,	/* flag,ap,hp,coin 		 */
	 80,50,		/* player attack hitarea */
	  0, 0,		/* player damage hitarea */
};


/***************************************************************************************************
						Polygon Star Init
***************************************************************************************************/
extern void s_polystar_init(void) {

	char starNo = ((execstp->s[stw_actorcode].d >> 24) & 0xff);

	unsigned char star_flag = BuGetStarFlag(activePlayerNo-1,activeCourseNo-1);

	if (star_flag & (0x0001 << starNo)) {
		execstp->map.shape = stageShapes[S_shadestar];  	//Change Shape 
	}
	else {
		execstp->map.shape = stageShapes[S_polystar];  		//Change Shape 
	}			 

	s_set_hitparam(execstp,&polystar_hit);		/* Set HitCheck Param */

}
/***************************************************************************************************
						Polygon Star Main
***************************************************************************************************/
extern void s_polystar_main(void) {


	obj_animeangleY += 0x800;

	if ( obj_mail & EMAIL_PLAYERHIT ){
		RemoveShape(execstp);
		Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
	}
}




/*=======================Enemy Set Star============================================================*/
#define enemystar_target_dist	(execstp->s[stw_work0].f)
#define enemystar_spY			(execstp->s[stw_work1].f)
#define enemystar_worldY		(execstp->s[stw_work2].f)
#define STARFLY					30

/***************************************************************************************************
						Enemy Set Stars
***************************************************************************************************/
extern void s_enemy_star_init() 
{
	obj_angleY = atan( (obj_attZ-obj_worldZ),(obj_attX-obj_worldX) );

	enemystar_target_dist = sqrtf((obj_attX-obj_worldX)*(obj_attX-obj_worldX)+(obj_attZ-obj_worldZ)*(obj_attZ-obj_worldZ));	
	
	obj_speedY = (obj_attY-obj_worldY)/STARFLY;
	obj_speedF = enemystar_target_dist/STARFLY;
	enemystar_worldY = obj_worldY;

	if (obj_programselect == 0 || activeCourseNo == 5)		cameraDemoStrat(CAM_DEMO_LOOKOBJ,execstp);	//CAMERA DEMO
	else						 							cameraDemoStrat(CAM_DEMO_STAR8,execstp);	//CAMERA DEMO


	s_begin_enemydemo(STRATMAIN_DEMOMODE+STRATMAIN_PLAYERSTOP);		//Strategy Stop 
	Mbitset( execstp->status,OBJECT_DEMOMOVEENTRY );				//kaijo only me

	s_hitOFF();
}
/***************************************************************************************************
						Enemy Set Stars
***************************************************************************************************/
extern void s_enemy_star_main() 
{
	float worldY;

	switch (obj_mode) {
		case 0: obj_animeangleY += 0x1000;
				if (obj_timer > 20) obj_mode = 1;
				break;

		case 1: ObjSpeedOn(execstp);
				enemystar_worldY += obj_speedY;
				obj_worldY = enemystar_worldY + sin(0x8000*obj_timer/STARFLY)*400;
				obj_animeangleY += 0x1000;
				s_makeobj_nowpos(execstp,S_NULL,e_twinkle_special);
				objsound_level(NA_LSE2_STAR_MOVE);
				if (obj_timer == STARFLY) { 
					obj_mode = 2; 
					obj_speedF = 0;
					Na_StarAppearBgm();
				}
				break;

		case 2: if (obj_timer < 20) obj_speedY = (20 - obj_timer);
				else				obj_speedY = -10;
				s_makeobj_nowpos(execstp,S_NULL,e_twinkle_special);
				ObjSpeedOn(execstp);
				obj_animeangleY += 0x1000 - obj_timer*0x10;
				objsound_level(NA_LSE2_STAR_MOVE);
				if (obj_worldY < obj_attY) {
					objsound(NA_SE2_STAR_FINISH);
					s_hitON();
					obj_worldY = obj_attY;
					obj_mode = 3;
				}
				break;

		case 3: obj_animeangleY += 0x800;
				if (obj_timer == 20) { 
					demoseqcode = 1;		// CAMERA DEMO END
					s_end_enemydemo(STRATMAIN_DEMOMODE+STRATMAIN_PLAYERSTOP);
					Mbitclr( execstp->status,OBJECT_DEMOMOVEENTRY );
				}
				
				if ( obj_mail & EMAIL_PLAYERHIT ){
					RemoveShape(execstp);
					Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
				}
				break;
	}


}
/***************************************************************************************************
						Enemy Set Stars
***************************************************************************************************/
static StrategyRecord* EnemysetStar(StrategyRecord* stp,float posx,float posy,float posz) 
{

	stp =  s_makeobj_absolute(execstp,0,S_polystar,e_enemy_star,obj_worldX,obj_worldY,obj_worldZ,0,0,0);

	stp->s[stw_actorcode].d = execstp->s[stw_actorcode].d;
	stp->s[stw_worldX_attention].f = posx;
	stp->s[stw_worldY_attention].f = posy;
	stp->s[stw_worldZ_attention].f = posz;

	stp->s[stw_animeangleX].d = 0;
	stp->s[stw_animeangleZ].d = 0;

	return(stp);
}

/***************************************************************************************************
						Enemy Set Stars
***************************************************************************************************/
extern void s_enemyset_star(float posx,float posy,float posz) 
{
	StrategyRecord* stp;
	
	stp = EnemysetStar(stp,posx,posy,posz);
	stp->s[stw_programselect].d = 0;	
}
/***************************************************************************************************
						8coin Set Stars
***************************************************************************************************/
static void s_coinset_star(float posx,float posy,float posz) 
{
	StrategyRecord* stp;
	
	stp = EnemysetStar(stp,posx,posy,posz);
	stp->s[stw_programselect].d = 1;	
}

/***************************************************************************************************
						Enemy Set Stars
***************************************************************************************************/
extern void s_extraset_star(float posx,float posy,float posz) 
{

	StrategyRecord* stp;

	stp = EnemysetStar(stp,posx,posy,posz);
	stp->s[stw_programselect].d = 1;
	Mbitset((stp->s[stw_enemyinfo].d),ENEMYINFO_EXTSTAR);
 		
}














/*=======================8 Coins Get Version======================================================*/

extern char redcoin_num;

#define appearstar_flag		(execstp->s[stw_work0].d)

static HitCheckRecord redcoin_hit = {
	OBJNAME_COIN,
	  0,2,0,0,		/* flag,ap,hp,coin 		 */
	100, 64,		/* player damage hitarea */
	  0,  0,		/* player attack hitarea */
};
/***************************************************************************************************
						8 Coins Star Main
***************************************************************************************************/
extern void s_getcoins_star_init() {

	short coin_num;

	if (activeCourseNo != 3) s_makeobj_nowpos(execstp,S_shadestar,e_starpanel);

	coin_num = s_count_obj(e_coin_appstar);	

	if (coin_num == 0) {
		StrategyRecord* stp =  s_makeobj_absolute(execstp,0,S_polystar,e_tripstar,obj_worldX,obj_worldY,obj_worldZ,0,0,0);
		stp->s[stw_actorcode].d = execstp->s[stw_actorcode].d;
		execstp->status = 0;
	}

	appearstar_flag = (8 - coin_num);
}
/***************************************************************************************************
						8 Coins Star Main
***************************************************************************************************/
extern void s_getcoins_star_main() {

		redcoin_num = appearstar_flag;

	switch (obj_mode) {
		case 0: if (appearstar_flag == 8) {
					obj_mode = 1;
				}
				break;

		case 1: if (obj_timer > 2) {
					s_coinset_star(obj_worldX,obj_worldY,obj_worldZ);
					s_kemuri();
					execstp->status = 0;
				} 
				break;
	}

}

/***************************************************************************************************
						8 Coins Star (COIN)
***************************************************************************************************/
extern void s_coin_appear_init() {

	BGCheckData* bgcp;
	float posy = mcBGGroundCheck(obj_worldX,obj_worldY,obj_worldZ,&bgcp);
	StrategyRecord* stratp;

	if ((stratp = s_find_obj(e_tripstar_getcoins)) != NULL) {
		execstp->motherobj = stratp;
	}
	else if ((stratp = s_find_obj(e_extstar_getcoins)) != NULL){
		execstp->motherobj = stratp;
	}
	else { execstp->motherobj = NULL; }


/*
	if (obj_worldY > posy+1500) {
		execstp->map.shape = stageShapes[S_redcoin_noshadow];  	//Change Shape 
	}
	else {
		execstp->map.shape = stageShapes[S_redcoin];  	//Change Shape 
	}
*/

	s_set_hitparam(execstp,&redcoin_hit);		/* Set HitCheck Param */

}
/***************************************************************************************************
						8 Coins Star (COIN)
***************************************************************************************************/
extern void s_coin_appear_main() {


	if ( obj_mail & EMAIL_PLAYERHIT ){
		if (motherp != NULL) {
			(motherp->s[stw_work0].d) += 1;
//rmonpf(("ff %d\n",(motherp->s[stw_work0].d) ));
			if ((motherp->s[stw_work0].d)!=8) { AppNumber( (char)(motherp->s[stw_work0].d),0,0,0);  }
			Na_RedCoinSe((u8)(motherp->s[stw_work0].d));
		}

		RemoveCoinCatch();
		Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
		
	}



}


/*=======================Dummyss Get Version======================================================*/

/***************************************************************************************************
						Dummy Star Main
***************************************************************************************************/
extern void s_getdummy_star_init() {

	short dummy_num;

	dummy_num = s_count_obj(e_dummy_appstar);	

	if (dummy_num == 0) {
		StrategyRecord* stp =  s_makeobj_absolute(execstp,0,S_polystar,e_tripstar,obj_worldX,obj_worldY,obj_worldZ,0,0,0);
		stp->s[stw_actorcode].d = execstp->s[stw_actorcode].d;
		execstp->status = 0;
	}

	appearstar_flag = (5 - dummy_num);


}
/***************************************************************************************************
						Dummy  Star Init
***************************************************************************************************/
extern void s_getdummy_star_main() {

	switch (obj_mode) {
		case 0: if (appearstar_flag == 5) {
					obj_mode = 1;
				}
				break;

		case 1: if (obj_timer > 2) {
					s_coinset_star(obj_worldX,obj_worldY,obj_worldZ);
					s_kemuri();
					execstp->status = 0;
				} 
				break;
	}

}

/***************************************************************************************************
						Dummy Get Star
***************************************************************************************************/
extern void s_dummy_appear() {

	if (  s_hitcheck(execstp,player1stp) == 1 ){
		StrategyRecord* stratp = s_find_obj(e_tripstar_getdummy);
		if (stratp != NULL) {
			(stratp->s[stw_work0].d) += 1;
			if ((stratp->s[stw_work0].d) != 5) AppNumber( (char)(stratp->s[stw_work0].d),0,0,0);
			Na_SecretPointSe((u8)(stratp->s[stw_work0].d));
		}

		execstp->status = 0;
		
	}



}



/*================================================================================================*/
/***************************************************************************************************
						8 Coins  Extra Star Main
***************************************************************************************************/
extern void s_getcoins_extstar_main() {

	redcoin_num = appearstar_flag;

	switch (obj_mode) {
		case 0: if (appearstar_flag == 8) {
					obj_mode = 1;
				}
				break;

		case 1: if (obj_timer > 2) {
					s_extraset_star(obj_worldX,obj_worldY,obj_worldZ);
					s_kemuri();
					execstp->status = 0;
				} 
				break;
	}

}






/*################*/
#endif
/*################*/


