/********************************************************************************
						Ultra 64 MARIO Brothers

						  dynamic data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

				This module was programmed by Y.Nishida

						  February 06, 1995
 ********************************************************************************/

#include "../headers.h"

DynamicRecord RCP_DynamicData;
