/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					   Rock Strategy

				Feburary 28 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
						Path Data
 ********************************************************************************/
e_snowrock_rail:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON| stf_YangleSAME | stf_playerangleON)
	p_BGcheckYset
	p_hitON
	p_program(s_snowrock_init)

	p_while
		p_program(s_snowrock_rail_main)
	p_loop

/********************************************************************************
						Snow Rock Head
 ********************************************************************************/
e_snowball_head:
	p_initialize(option)
	p_setbit(flag,stf_moveON | stf_playerangleON)
	p_BGcheckYset
	p_setf(animepositionY,110)
	p_program(s_snowball_head_init)

	p_while
		p_program(s_snowball_head_main)
	p_loop

/********************************************************************************
						Path Data
 ********************************************************************************/
e_snowrock_dummy:
	p_initialize(option)
	p_setbit(flag,stf_moveON)

	p_while
		p_program(s_snowrock_dummy_main)
	p_loop


/*--------------------------------------------------------------------------------*/
e_snow_body:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON)
	p_setf(animepositionY,180)
	p_setobjname(FRIEND)
	p_sethitbox(210,550)

	p_while
		p_hitON
	p_loop


/*===============================================================================
		Dunjon Rock
 ================================================================================*/

e_dunjon_rock :
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON| stf_YangleSAME | stf_playerangleON)
	p_setf(animepositionY,180)
	p_program(s_dunjon_rock_init)
	p_setf(erasedist,20000)

	p_while
		p_hitON
		p_program(s_dunjon_rock_main)
	p_loop

e_dunjon_rock_st:
	p_initialize(option)
	p_setbit(flag,stf_moveON)

	p_while
		p_program(s_dunjon_rock_st)
	p_loop

/*################*/
#else
/*################*/


/*==================================================================================================
		C Program
 ===================================================================================================*/
#define snowrock_scale		(execstp->s[stw_work0].f)
#define rock_target_angY	(execstp->s[stw_work1].d)
#define snowrock_flag		(execstp->s[stw_work8].d)

#define SNOWBALL_POSX	-4230
#define SNOWBALL_POSY	-1344
#define SNOWBALL_POSZ	 1813

#define SNOWBALL_STARX	-4700
#define SNOWBALL_STARY	-1024
#define SNOWBALL_STARZ	 1890

extern short yuki_goro_raildata[]; 

static HitCheckRecord snow_rock_hit = {
	OBJNAME_DAMAGE,
	  0,3,0,0,		/* flag,ap,hp,coin 		 */
	 210, 350,		/* player damage hitarea */
	   0,   0,		/* player attack hitarea */
};

/***************************************************************************************************
						snow rock Init
***************************************************************************************************/
extern void s_snowrock_init(void)
{
	StrategyRecord* stp;

	obj_attX = obj_worldX;
	obj_attY = obj_worldY;
	obj_attZ = obj_worldZ;

	execstp->s[stw_gravity].f   = 10;
	execstp->s[stw_friction].f  = 0.999;
	execstp->s[stw_specificG].f = 2.0;

	obj_speedY = 0;
	obj_speedF = 0;

	snowrock_scale = 0.4;

	if ((stp = s_find_obj(e_snowball_head)) != NULL) {
			execstp->motherobj = stp;
	}


	s_makeobj_absolute(execstp,0,S_NULL,e_snowrock_dummy,-402,461,-2898,0,0,0);
	

}
/***************************************************************************************************
						    Snow Rock Attack
***************************************************************************************************/
static void rock_PlayerTouch(void)
{

	s_set_hitparam(execstp,&snow_rock_hit);		/* Set HitCheck Param */


	if ( obj_mail & EMAIL_PLAYERHIT ){

		Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
	}


}

/***************************************************************************************************
						    Snow Rock Rotate
***************************************************************************************************/
static void rock_Rotate(float scale)
{
	obj_animeangleX += (short)(execstp->s[stw_speedF].f*(100/scale));
	snowrock_scale += obj_speedF*(0.0001);
	if (snowrock_scale > 1.0) snowrock_scale = 1.0; 

}
/***************************************************************************************************
						    Snow Rock Move
***************************************************************************************************/
static void snowrock_Move(void)
{

	short move_flag;
	int	  lift_flag;


	road_root(execstp) = (short*) (SegmentToVirtual(yuki_goro_raildata));			/* ten_lift Data Read*/


	move_flag = ObjMoveEvent_noInc();
	lift_flag = s_road_move(lift_flag);

	rock_target_angY = road_angleY(execstp);
	obj_angleY = s_chase_angle(obj_angleY,rock_target_angY,0x400);


	if (execstp->s[stw_speedF].f > 70.0) execstp->s[stw_speedF].f = 70.0;

	if (lift_flag == -1) {
		short angle = (unsigned short)obj_targetangle - (unsigned short)obj_angleY;

		if (ShapeSameAngle(obj_angleY,obj_targetangle,0x2000)==1 && snowrock_flag == 1 ) {
			rock_target_angY = obj_targetangle;
		}
		else {rock_target_angY = obj_angleY; }
		obj_mode = 2;		
	}



}

/***************************************************************************************************
						    Snow Rock ByeBye
***************************************************************************************************/
static void snowrock_ByeBye(void)
{
	short move_flag;

	move_flag = ObjMoveEvent_noInc();
	if (execstp->s[stw_speedF].f > 70.0) execstp->s[stw_speedF].f = 70.0;

	obj_angleY = s_chase_angle(obj_angleY,rock_target_angY,0x400);

	if (ObjApproach(execstp,SNOWBALL_POSX,SNOWBALL_POSY,SNOWBALL_POSZ,300) ) { 
		s_burneffect(0,0,70);
		obj_angleY = atan( (SNOWBALL_POSZ-obj_worldZ),(SNOWBALL_POSX-obj_worldX));
		obj_speedY = 80;
		obj_speedF = 15;
		obj_mode = 3; 

		mother_mode = 2;
		motherp->s[stw_speedY].f = 100;
		objsound(NA_SE3_SNOWBODY_JUMP);
	}

	if (obj_timer == 200) {
		Obj_reset(S_snow_rock,e_snowrock_rail,3000);
		execstp->status = 0;
	} 	
}

/***************************************************************************************************
						    Snow Rock ByeBye
***************************************************************************************************/
static void snowrock_Stop(void)
{
	short move_flag;

	move_flag = ObjMoveEvent_noInc();

	if ((move_flag & OM_GROUND)==OM_GROUND) {
		obj_mode = 4;
		s_hitOFF();
	}

	if ((move_flag & OM_TOUCH)) {
		s_burneffect(0,0,70);
		obj_worldX = SNOWBALL_POSX;
		obj_worldZ = SNOWBALL_POSZ;
		obj_speedF = 0;
	}


}
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_snowrock_rail_main(void)
{
	short mess_demo;

	switch (obj_mode) {
		case 0: if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,400)==1) {
					if (CtrlPlayerDialog(DLOG_LOOKFRONT)==DLOG_RESULT_READY) {
						mess_demo = cameraDemoStratMsgNum(CAM_DEMO_TALK,execstp,110);
						if (mess_demo != 0) {
							obj_speedF = 10;
							obj_mode = 1;
							CtrlPlayerDialog(DLOG_DONE);
						}
					}
				}
				break;

		case 1: snowrock_Move();
				rock_Rotate(snowrock_scale);
				objsound_level(NA_LSE2_ROCKROLL);
				break;

		case 2: snowrock_ByeBye();
				rock_Rotate(snowrock_scale);
				objsound_level(NA_LSE2_ROCKROLL);
				break;

		case 3: snowrock_Stop();
				break;

		case 4: s_player_slideout_RH(210,550);
				break;
	}
	

	rock_PlayerTouch();

	PlayerApproachOnOff(execstp,8000);

	s_set_scale(snowrock_scale);
	obj_animepositionY = 180*snowrock_scale;

}

/*==================================================================================================
		Snow Ball Head
 ===================================================================================================*/
#define snow_head_mess_flag	(execstp->s[stw_work0].d)


/***************************************************************************************************
						    Init
***************************************************************************************************/
extern void s_snowball_head_init(void)
{
	unsigned char get_starFlag = BuGetStarFlag(activePlayerNo-1,activeCourseNo-1);
	char starNo = ((execstp->s[stw_actorcode].d >> 24) & 0xff);

	s_set_scale(0.7);

	execstp->s[stw_gravity].f   = 5;
	execstp->s[stw_friction].f  = 0.999;
	execstp->s[stw_specificG].f = 2.0;

	if ( (get_starFlag & (0x01<<starNo)) && activeLevelNo != starNo+1) {
		s_makeobj_absolute(execstp,0,S_snow_rock,e_snow_body,SNOWBALL_POSX,SNOWBALL_POSY, SNOWBALL_POSZ,0,0,0);
		obj_worldX = SNOWBALL_POSX;
		obj_worldY = SNOWBALL_POSY+350;
		obj_worldZ = SNOWBALL_POSZ;
		obj_mode = 1;
	} 


}

/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_snowball_head_main(void)
{
	short mess_demo;
	short move_flag;



	switch (obj_mode) {

		case 0: if (iwa_ObjMessage((int*)&snow_head_mess_flag,109,400,DLOG_LOOKFRONT) != 0) {
					obj_mode = 1;
				}
				break;

		case 1: break;

		case 2: move_flag = ObjMoveEvent_noInc();
				if (move_flag & 0x08) obj_mode = 3;
				break;

		case 3: ObjMoveEvent_noInc();
				if (obj_worldY < SNOWBALL_POSY+350) {
					obj_worldY = SNOWBALL_POSY+350;
					obj_mode = 4;
					objsound(NA_SE3_SNOWBODY_LAND);
					Na_NazoClearBgm();
				}
				break;

		case 4: if (iwa_ObjMessage((int*)&snow_head_mess_flag,111,700,DLOG_LOOKUP) != 0) {
					s_kemuri();
					s_enemyset_star(SNOWBALL_STARX,SNOWBALL_STARY,SNOWBALL_STARZ);
					obj_mode = 1;
				}
				break;


	}

	s_player_slideout_RH(180,150);
}


/*==================================================================================================
		Snow Rock Dummy
 ===================================================================================================*/
/***************************************************************************************************
						snow rock Init
***************************************************************************************************/
extern void s_snowrock_dummy_main(void)
{
	if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,800)) {
		(motherp->s[stw_work8].d) ++;
		execstp->status = 0;
	} 

	if (motherp->status == 0) execstp->status = 0;

}
















/*==================================================================================================
		Dunjon Rock
 ===================================================================================================*/
/***************************************************************************************************
						Dunjon Rock Init
***************************************************************************************************/
extern void s_dunjon_rock_init(void)
{

	obj_attX = obj_worldX;
	obj_attY = obj_worldY;
	obj_attZ = obj_worldZ;

	execstp->s[stw_gravity].f   = 8.;
	execstp->s[stw_friction].f  = 0.999;
	execstp->s[stw_specificG].f = 2.0;

}

/***************************************************************************************************
						    Rock Move
***************************************************************************************************/
static void dunjonrock_Move(void)
{

	short move_flag;

	move_flag = ObjMoveEvent_noInc();

	if ( ((move_flag & 0x09) == 0x01) && obj_speedY > 10) {
		objsound(NA_SE2_ROCKBOUND);
		s_kemuri();
	} 

	if (obj_speedF > 70.0) obj_speedF = 70.0;

	if (obj_worldY < -1000) {
		execstp->status = 0;
	} 


}
/***************************************************************************************************
							DunjonRock Main
***************************************************************************************************/
extern void s_dunjon_rock_main(void)
{
	s_set_scale(1.5);
	obj_animepositionY = 180*1.5;

	switch (obj_mode) {
		case 0: obj_speedF = 40;
				obj_mode = 1;
				break;

		case 1: dunjonrock_Move();
				rock_Rotate(1.5);
				objsound_level(NA_LSE2_ROCKROLL);
				break;

	}
	

	rock_PlayerTouch();

}

/***************************************************************************************************
						Dunjon Rock Start
***************************************************************************************************/
extern void s_dunjon_rock_st(void)
{

	StrategyRecord *stratp;

	if (obj_timer > 255) obj_timer = 0;

	if (iwa_MapAreaCheck(4)==0 || PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,1500) ) {
		return;
	}

	if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,6000)) {
		if ((obj_timer & 0x3f)== 0) {	
			stratp = s_makeobj_nowpos(execstp,S_movebg04,e_dunjon_rock);
			stratp->s[stw_angleY].d = (Randomf()*0x1000);
		}
	}
	else {
		if ((obj_timer & 0x7f)== 0) {	
			stratp = s_makeobj_nowpos(execstp,S_movebg04,e_dunjon_rock);
			stratp->s[stw_angleY].d = (Randomf()*0x1000);
		}
	}



}





/*################*/
#endif
/*################*/


