#!/usr/bin/python

import sys

def rgb32to16(pixels):
    r = int(31.0/255.0 * ord(pixels[0]) + 0.5)
    g = int(31.0/255.0 * ord(pixels[1]) + 0.5)
    b = int(31.0/255.0 * ord(pixels[2]) + 0.5)
    if ord(pixels[3]) != 0:
        a = 1
    else:
        a = 0
    return ((r & 0x1f) << 11) | ((g & 0x1f) << 6) | ((b & 0x1f) << 1) | a

chunk_size = 80*20*4
image = open(sys.argv[1]).read()
out = open(sys.argv[2], "w")
for chunk in xrange(0, 48):
    out.write("static unsigned short cake%03d_txt[] = {\n" % chunk)
    start = chunk/4 * chunk_size * 4 + (chunk % 4) * 80 * 4
    for y in xrange(0, 20):
        out.write(" "*8)
        for x in xrange(0, 80):
            out.write("0x%04x,  " % rgb32to16(image[start+x*4:start+x*4+4]))
        start = start + 320 * 4
        out.write("\n")
    out.write("};\n\n")
