/********************************************************************************
	seqlang.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	April 15, 1996
 ********************************************************************************/

#ifndef SEQLANG_H
#define	SEQLANG_H

#define	ShapePlayer1		1
#define	ShapePlayer2		2


#define	SEQ_WIPE_FADEIN				0
#define	SEQ_WIPE_FADEOUT			1

#define	WIPE_FADE_IN				0
#define	WIPE_FADE_OUT				1

#define	WIPE_TRI_SCR_OPEN			2
#define	WIPE_TRI_SCR_CLOSE			3

#define	WIPE_RECT_SCR_OPEN			4
#define	WIPE_RECT_SCR_CLOSE			5

#define	WIPE_R_DITHER_IN			6
#define	WIPE_R_DITHER_OUT			7

#define	WIPE_STAR_WIN_OPEN			8
#define	WIPE_STAR_WIN_CLOSE			9

#define	WIPE_CIRCLE_WIN_OPEN		10
#define	WIPE_CIRCLE_WIN_CLOSE		11

#define	WIPE_POLY_STAR_OPEN			12
#define	WIPE_POLY_STAR_CLOSE		13


#define	SEQ_MESG_ENTRANT			0


#define	SEQ_WATER_IN		0
#define	SEQ_WATER_OUT		1

#define	SEQ_KOOPA_NON		0
#define	SEQ_KOOPA_LIVE		1
#define	SEQ_KOOPA_DIED		2
#define	SEQ_STAR_LEVEL		3


#define	ENV_SNOW					2

#define	ENV_PLAIN					0x00
#define	ENV_ROCKMT					0x01
#define	ENV_SNOWMT					0x02
#define	ENV_DESERT					0x03
#define	ENV_HOUSE					0x04
#define	ENV_WATER					0x05
#define	ENV_SLIDER					0x06
#define	ENV_CODEMASK				0x07


#define	SEQ_AND		0
#define	SEQ_NAND	1
#define	SEQ_EQ		2
#define	SEQ_NE		3
#define	SEQ_GT		4
#define	SEQ_GE		5
#define	SEQ_LT		6
#define	SEQ_LE		7

#define	PAD			0


#define	STAR_00001		0x01
#define	STAR_00010		0x02
#define	STAR_00011		0x03
#define	STAR_00100		0x04
#define	STAR_00101		0x05
#define	STAR_00110		0x06
#define	STAR_00111		0x07
#define	STAR_01000		0x08
#define	STAR_01001		0x09
#define	STAR_01010		0x0A
#define	STAR_01011		0x0B
#define	STAR_01100		0x0C
#define	STAR_01101		0x0D
#define	STAR_01110		0x0E
#define	STAR_01111		0x0F
#define	STAR_10000		0x10
#define	STAR_10001		0x11
#define	STAR_10010		0x12
#define	STAR_10011		0x13
#define	STAR_10100		0x14
#define	STAR_10101		0x15
#define	STAR_10110		0x16
#define	STAR_10111		0x17
#define	STAR_11000		0x18
#define	STAR_11001		0x19
#define	STAR_11010		0x1A
#define	STAR_11011		0x1B
#define	STAR_11100		0x1C
#define	STAR_11101		0x1D
#define	STAR_11110		0x1E
#define	STAR_11111		0x1F


/********************************************************************************/
/*	Geometry render mode.														*/
/********************************************************************************/

#define	GMODE_PLAYER	0
#define	GMODE_COURSE	1
#define	GMODE_LEVEL		2
#define	GMODE_STAGE		3
#define	GMODE_SCENE		4



/********************************************************************************/
/*	C Language only.															*/
/********************************************************************************/

#ifndef ASSEMBLER

typedef uchar	Sequence, *SequencePtr;

extern SequencePtr
ExecuteSequence(SequencePtr sequence);

#endif

/********************************************************************************/
/*	Sequence language command ID.												*/
/********************************************************************************/

#define	_seqExecute_id				 0
#define	_seqChain_id				 1
#define	_seqExit_id					 2
#define	_seqWait_id					 3
#define	_seqFreeze_id				 4
#define	_seqJump_id					 5
#define	_seqCall_id					 6
#define	_seqReturn_id				 7
#define	_seqDo_id					 8
#define	_seqNext_id					 9
#define	_seqRepeat_id				10
#define	_seqUntil_id				11
#define	_seqTstJump_id				12
#define	_seqTstCall_id				13
#define	_seqIf_id					14
#define	_seqElse_id					15
#define	_seqEndif_id				16
#define	_seqCProgram_id				17
#define	_seqRunning_id				18
#define	_seqSetResult_id			19

#define	_seqLinkMemory_id			20
#define	_seqUnlinkMemory_id			21
#define	_seqLoadCode_id				22
#define	_seqLoadData_id				23
#define	_seqLoadPres_id				24
#define	_seqLoadFace_id				25
#define	_seqLoadText_id				26

#define	_seqInitStage_id			27
#define	_seqDestroyStage_id			28
#define	_seqBeginConstruction_id	29
#define	_seqEndConstruction_id		30
#define	_seqBeginScene_id			31
#define	_seqEndScene_id				32
#define	_seqGfxShape_id				33
#define	_seqHmsShape_id				34
#define	_seqSclShape_id				35
#define	_seqActor_id				36
#define	_seqStarring_id				37
#define	_seqPort_id					38
#define	_seqBGPort_id				39
#define	_seqConnect_id				40

#define	_seqOpenScene_id			41
#define	_seqCloseScene_id			42
#define	_seqEnterPlayer_id			43
#define	_seqExitPlayer_id			44
#define	_seqExecStrategy_id			45

#define	_seqMapInfo_id				46
#define	_seqAreaInfo_id				47
#define	_seqMessage_id				48
#define	_seqEnvironment_id			49
#define	_seqBlankColor_id			50

#define	_seqWipe_id					51
#define	_seqBlanking_id				52
#define	_seqGamma_id				53

#define	_seqSetMusic_id				54
#define	_seqPlayMusic_id			55
#define	_seqStopMusic_id			56

#define	_seqTagInfo_id				57
#define	_seqWindZone_id				58
#define	_seqWaterJet_id				59

#define	_seqGameMode_id				60

#ifdef ASSEMBLER

#define	RGBA16(r,g,b,a)		(((r)<<11) | ((g)<<6) | ((b)<<1) | (a))
#define	AudFrame(n)			((n)*8-2)

/*===============================================================================
 *
 *	Sequence control command
 */

/********************************************************************************/
/*	seqExecute																	*/
/********************************************************************************/

#define	seqExecute(segment, romStart, romEnd, procedure) \
	.byte	_seqExecute_id; \
	.byte	16;				\
	.half	segment;		\
	.word	romStart;		\
	.word	romEnd;			\
	.word	procedure

/********************************************************************************/
/*	seqChain																	*/
/********************************************************************************/

#define	seqChain(segment, romStart, romEnd, procedure) \
	.byte	_seqChain_id; \
	.byte	16;			  \
	.half	segment;	  \
	.word	romStart;	  \
	.word	romEnd;		  \
	.word	procedure

/********************************************************************************/
/*	seqExit																		*/
/********************************************************************************/

#define	seqExit			 \
	.byte	_seqExit_id; \
	.byte	4;			 \
	.half	PAD

/********************************************************************************/
/*	seqEnd																		*/
/********************************************************************************/

#define	seqEnd			 \
	.byte	_seqWait_id; \
	.byte	4;			 \
	.half	PAD

/********************************************************************************/
/*	seqWait																		*/
/********************************************************************************/

#define	seqWait(nframes) \
	.byte	_seqWait_id; \
	.byte	4;			 \
	.half	nframes

/********************************************************************************/
/*	seqFreeze																	*/
/********************************************************************************/

#define	seqFreeze(nframes) \
	.byte	_seqFreeze_id; \
	.byte	4;			   \
	.half	nframes

/********************************************************************************/
/*	seqJump																		*/
/********************************************************************************/

#define	seqJump(procedure) \
	.byte	_seqJump_id;   \
	.byte	8;			   \
	.half	PAD;		   \
	.word	procedure	

/********************************************************************************/
/*	seqCall																		*/
/********************************************************************************/

#define	seqCall(procedure) \
	.byte	_seqCall_id;   \
	.byte	8;			   \
	.half	PAD;		   \
	.word	procedure

/********************************************************************************/
/*	seqReturn																	*/
/********************************************************************************/

#define	seqReturn		   \
	.byte	_seqReturn_id; \
	.byte	4;			   \
	.half	PAD

/********************************************************************************/
/*	seqDo																		*/
/********************************************************************************/

#define	seqDo(nrounds) \
	.byte	_seqDo_id; \
	.byte	4;		   \
	.half	nrounds

/********************************************************************************/
/*	seqNext																		*/
/********************************************************************************/

#define	seqNext			 \
	.byte	_seqNext_id; \
	.byte	4;			 \
	.half	PAD

/********************************************************************************/
/*	seqRepeat																	*/
/********************************************************************************/

#define	seqRepeat		   \
	.byte	_seqRepeat_id; \
	.byte	4;			   \
	.half	PAD

/********************************************************************************/
/*	seqUntil																	*/
/********************************************************************************/

#define	seqUntil(condition, value) \
	.byte	_seqUntil_id; \
	.byte	8;			  \
	.byte	condition;	  \
	.byte	PAD;		  \
	.word	value

/********************************************************************************/
/*	seqTstJump																	*/
/********************************************************************************/

#define	seqTstJump(condition, value, procedure) \
	.byte	_seqTstJump_id; \
	.byte	12;				\
	.byte	condition;		\
	.byte	PAD;			\
	.word	value;			\
	.word	procedure

/********************************************************************************/
/*	seqTstCall																	*/
/********************************************************************************/

#define	seqTstCall(condition, value, procedure) \
	.byte	_seqTstCall_id; \
	.byte	12;				\
	.byte	condition;		\
	.byte	PAD;			\
	.word	value;			\
	.word	procedure

/********************************************************************************/
/*	seqIf																		*/
/********************************************************************************/

#define	seqIf(condition, index, value) \
	.byte	_seqIf_id; \
	.byte	8;		   \
	.byte	condition; \
	.byte	PAD;	   \
	.word	value

/********************************************************************************/
/*	seqElse																		*/
/********************************************************************************/

#define	seqElse			 \
	.byte	_seqElse_id; \
	.byte	4;			 \
	.half	PAD

/********************************************************************************/
/*	seqEndif																	*/
/********************************************************************************/

#define	seqEndif		  \
	.byte	_seqEndif_id; \
	.byte	4;			  \
	.half	PAD

/********************************************************************************/
/*	seqCProgram																	*/
/********************************************************************************/

#define	seqCProgram(program, code) \
	.byte	_seqCProgram_id; \
	.byte	8;				 \
	.half	code;			 \
	.word	program

/********************************************************************************/
/*	seqRunning																	*/
/********************************************************************************/

#define	seqRunning(program, code) \
	.byte	_seqRunning_id; \
	.byte	8;				\
	.half	code;			\
	.word	program

/********************************************************************************/
/*	seqSetResult																*/
/********************************************************************************/

#define	seqSetResult(code) \
	.byte	_seqSetResult_id; \
	.byte	4;				  \
	.half	code;







/*===============================================================================
 *
 *	Memory allocation command
 */

/********************************************************************************/
/*	seqLinkMemory																*/
/********************************************************************************/

#define	seqLinkMemory()		   \
	.byte	_seqLinkMemory_id; \
	.byte	4;				   \
	.half	PAD

/********************************************************************************/
/*	seqUnlinkMemory																*/
/********************************************************************************/

#define	seqUnlinkMemory()		 \
	.byte	_seqUnlinkMemory_id; \
	.byte	4;					 \
	.half	PAD

/********************************************************************************/
/*	seqLoadCode																	*/
/********************************************************************************/

#define	seqLoadCode(ramAddr, romStart, romEnd) \
	.byte	_seqLoadCode_id; \
	.byte	16;				 \
	.half	PAD;			 \
	.word	ramAddr;		 \
	.word	romStart;		 \
	.word	romEnd

/********************************************************************************/
/*	seqLoadData																	*/
/********************************************************************************/

#define	seqLoadData(segment, romStart, romEnd) \
	.byte	_seqLoadData_id; \
	.byte	12;				 \
	.half	segment;		 \
	.word	romStart;		 \
	.word	romEnd

/********************************************************************************/
/*	seqLoadPres																	*/
/********************************************************************************/

#define	seqLoadPres(segment, romStart, romEnd) \
	.byte	_seqLoadPres_id; \
	.byte	12;				 \
	.half	segment;		 \
	.word	romStart;		 \
	.word	romEnd

/********************************************************************************/
/*	seqLoadFace																	*/
/********************************************************************************/

#define	seqLoadFace(number)  \
	.byte	_seqLoadFace_id; \
	.byte	4;				 \
	.half	number

/********************************************************************************/
/*	seqLoadText																	*/
/********************************************************************************/

#define	seqLoadText(segment, romStart, romEnd) \
	.byte	_seqLoadText_id; \
	.byte	12;				 \
	.half	segment;		 \
	.word	romStart;		 \
	.word	romEnd














/*================================================================================
 *
 *	Stage construction command
 */

/********************************************************************************/
/*	seqInitStage																*/
/********************************************************************************/

#define seqInitStage() \
	.byte	_seqInitStage_id; \
	.byte	4;				  \
	.half	PAD

/********************************************************************************/
/*	seqDestroyStage																*/
/********************************************************************************/

#define seqDestroyStage()		 \
	.byte	_seqDestroyStage_id; \
	.byte	4;					 \
	.half	PAD

/********************************************************************************/
/*	seqStageConstruction														*/
/********************************************************************************/

#define	seqBeginConstruction()		  \
	.byte	_seqBeginConstruction_id; \
	.byte	4;						  \
	.half	PAD

/********************************************************************************/
/*	seqEndConstruction															*/
/********************************************************************************/

#define	seqEndConstruction()  		\
	.byte	_seqEndConstruction_id; \
	.byte	4;						\
	.half	PAD

/********************************************************************************/
/*	seqBeginScene																*/
/********************************************************************************/

#define seqBeginScene(sceneNo, hierarchy) \
	.byte	_seqBeginScene_id; \
	.byte	8;				   \
	.byte	sceneNo;		   \
	.byte	PAD;			   \
	.word	hierarchy

/********************************************************************************/
/*	seqEndScene																	*/
/********************************************************************************/

#define seqEndScene() \
	.byte	_seqEndScene_id; \
	.byte	4;				 \
	.half	PAD

/********************************************************************************/
/*	seqGfxShape																	*/
/********************************************************************************/

#define	seqGfxShape(shapeNo, gfxlist, mode) \
	.byte	_seqGfxShape_id;   \
	.byte	8;				   \
	.half	mode*4096+shapeNo; \
	.word	gfxlist

/********************************************************************************/
/*	seqHmsShape																	*/
/********************************************************************************/

#define	seqHmsShape(shapeNo, hierarchy) \
	.byte	_seqHmsShape_id; \
	.byte	8;				 \
	.half	shapeNo;		 \
	.word	hierarchy

/********************************************************************************/
/*	seqSclShape																	*/
/********************************************************************************/

#define	seqSclShape(shapeNo, gfxlist, mode, scale) \
	.byte	_seqSclShape_id;   \
	.byte	12;				   \
	.half	mode*4096+shapeNo; \
	.word	gfxlist;		   \
	.float	scale

/********************************************************************************/
/*	seqHmsMario																	*/
/********************************************************************************/

#define	seqHmsMario(shapeNo, code, strategy) \
	.byte	_seqStarring_id; \
	.byte	12;				 \
	.byte	0;				 \
	.byte	shapeNo;		 \
	.word	code;			 \
	.word	strategy

/********************************************************************************/
/*	seqHmsLuigi																	*/
/********************************************************************************/

#define	seqHmsLuigi(shapeNo, code, strategy) \
	.byte	_seqStarring_id; \
	.byte	12;				 \
	.byte	1;				 \
	.byte	shapeNo;		 \
	.word	code;			 \
	.word	strategy

/********************************************************************************/
/*	seqActor																	*/
/********************************************************************************/

#define	seqActor(shapeNo, posx, posy, posz, angx, angy, angz, code1, code2, flags, strategy) \
	.byte	_seqActor_id; \
	.byte	24;			  \
	.byte	0x1f;		  \
	.byte	shapeNo;	  \
	.half	posx;		  \
	.half	posy;		  \
	.half	posz;		  \
	.half	angx;		  \
	.half	angy;		  \
	.half	angz;		  \
	.byte	code1;		  \
	.byte	code2;		  \
	.half	flags;		  \
	.word	strategy

#define	seqLevelActor(level, shapeNo, posx, posy, posz, angx, angy, angz, code1, code2, flags, strategy) \
	.byte	_seqActor_id; \
	.byte	24;			  \
	.byte	level;		  \
	.byte	shapeNo;	  \
	.half	posx;		  \
	.half	posy;		  \
	.half	posz;		  \
	.half	angx;		  \
	.half	angy;		  \
	.half	angz;		  \
	.byte	code1;		  \
	.byte	code2;		  \
	.half	flags;		  \
	.word	strategy

/********************************************************************************/
/*	seqPort																		*/
/********************************************************************************/

#define	seqGameClear(stage, scene, port)		seqPort(0xf0,stage,scene,port)
#define	seqGameOver(stage, scene, port)			seqPort(0xf1,stage,scene,port)
#define	seqViewRoof(stage, scene, port)			seqPort(0xf2,stage,scene,port)
#define	seqCourseOut(stage, scene, port)		seqPort(0xf3,stage,scene,port)

#define	seqPort(number, stage, scene, port) \
	.byte	_seqPort_id; \
	.byte	8;			 \
	.byte	number;		 \
	.byte	stage;		 \
	.byte	scene;		 \
	.byte	port;		 \
	.byte	0x00;		 \
	.byte	PAD

#define	seqMidPort(number, stage, scene, port) \
	.byte	_seqPort_id; \
	.byte	8;			 \
	.byte	number;		 \
	.byte	stage;		 \
	.byte	scene;		 \
	.byte	port;		 \
	.byte	0x80;		 \
	.byte	PAD

/********************************************************************************/
/*	seqBGPort																	*/
/********************************************************************************/

#define	seqBGPort(bgattr, stage, scene, port) \
	.byte	_seqBGPort_id; \
	.byte	8;			   \
	.byte	bgattr;		   \
	.byte	stage;		   \
	.byte	scene;		   \
	.byte	port;		   \
	.byte	0x00;		   \
	.byte	PAD

#define	seqMidBGPort(bgattr, stage, scene, port) \
	.byte	_seqBGPort_id; \
	.byte	8;			   \
	.byte	bgattr;		   \
	.byte	stage;		   \
	.byte	scene;		   \
	.byte	port;		   \
	.byte	0x80;		   \
	.byte	PAD

/********************************************************************************/
/*	seqConnect																	*/
/********************************************************************************/

#define	seqConnect(number, scene, offsetx, offsety, offsetz) \
	.byte	_seqConnect_id; \
	.byte	12;				\
	.byte	number;			\
	.byte	scene;			\
	.half	offsetx;		\
	.half	offsety;		\
	.half	offsetz;		\
	.half	PAD

/********************************************************************************/
/*	seqGround																	*/
/********************************************************************************/

#define	seqGround(address) \
	.byte	_seqGround_id; \
	.byte	8;			   \
	.half	PAD;		   \
	.word	address

/********************************************************************************/
/*	seqMapInfo																	*/
/********************************************************************************/

#define	seqMapInfo(address) \
	.byte	_seqMapInfo_id; \
	.byte	8;				\
	.half	PAD;			\
	.word	address

/********************************************************************************/
/*	seqAreaInfo																	*/
/********************************************************************************/

#define	seqAreaInfo(address) \
	.byte	_seqAreaInfo_id; \
	.byte	8;				 \
	.half	PAD;			 \
	.word	address

/********************************************************************************/
/*	seqTagInfo																	*/
/********************************************************************************/

#define	seqTagInfo(address) \
	.byte	_seqTagInfo_id; \
	.byte	8;				\
	.half	PAD;			\
	.word	address







/*===============================================================================
 *
 *	Scene control command
 */

/********************************************************************************/
/*	seqOpenScene																*/
/********************************************************************************/

#define	seqOpenScene(scene)   \
	.byte	_seqOpenScene_id; \
	.byte	4;				  \
	.byte	scene;			  \
	.byte	PAD

/********************************************************************************/
/*	seqCloseScene																*/
/********************************************************************************/

#define	seqCloseScene(scene)   \
	.byte	_seqCloseScene_id; \
	.byte	4;				   \
	.byte	scene;			   \
	.byte	PAD

/********************************************************************************/
/*	seqEnterMario																*/
/********************************************************************************/

#define	seqEnterMario(scene, angle, posx, posy, posz) \
	.byte	_seqEnterPlayer_id; \
	.byte	12;					\
	.byte	scene;				\
	.byte	0;					\
	.half	angle;				\
	.half	posx;				\
	.half	posy;				\
	.half	posz

/********************************************************************************/
/*	seqEnterLuigi																*/
/********************************************************************************/

#define	seqEnterLuigi(scene, angle, posx, posy, posz) \
	.byte	_seqEnterPlayer_id; \
	.byte	12;					\
	.byte	scene;				\
	.byte	1;					\
	.half	angle;				\
	.half	posx;				\
	.half	posy;				\
	.half	posz


/********************************************************************************/
/*	seqExitMario																*/
/********************************************************************************/

#define	seqExitMario()		   \
	.byte	_seqExitPlayer_id; \
	.byte	4;				   \
	.byte	PAD;			   \
	.byte	0

/********************************************************************************/
/*	seqExitLuigi																*/
/********************************************************************************/

#define	seqExitLuigi()		   \
	.byte	_seqExitPlayer_id; \
	.byte	4;				   \
	.byte	PAD;			   \
	.byte	1

/********************************************************************************/
/*	seqExecStrategy																*/
/********************************************************************************/

#define	seqExecStrategy()		 \
	.byte	_seqExecStrategy_id; \
	.byte	4;					 \
	.half	PAD

/********************************************************************************/
/*	seqWipe																		*/
/********************************************************************************/

#define	seqWipe(mode, nframes, r, g, b) \
	.byte	_seqWipe_id; \
	.byte	8;			 \
	.byte	mode;		 \
	.byte	nframes;	 \
	.byte	r;			 \
	.byte	g;			 \
	.byte	b;			 \
	.byte	PAD

/********************************************************************************/
/*	seqBlanking																	*/
/********************************************************************************/

#define	seqBlanking(blankSw) \
	.byte	_seqBlanking_id; \
	.byte	4;				 \
	.byte	blankSw;		 \
	.byte	PAD

/********************************************************************************/
/*	seqGamma																	*/
/********************************************************************************/

#define	seqGammaCorrection(gammaSw) \
	.byte	_seqGamma_id; \
	.byte	4;			  \
	.byte	gammaSw;	  \
	.byte	PAD

/********************************************************************************/
/*	seqEnvironment																*/
/********************************************************************************/

#define	seqEnvironment(flags)	\
	.byte	_seqEnvironment_id; \
	.byte	4;					\
	.half	flags

/********************************************************************************/
/*	seqBlankColor																*/
/********************************************************************************/

#define	seqBlankColor(color) \
	.byte	_seqBlankColor_id; \
	.byte	4;				   \
	.half	color

/********************************************************************************/
/*	seqMessage																	*/
/********************************************************************************/

#define	seqMessage(type, message) \
	.byte	_seqMessage_id; \
	.byte	4;				   \
	.byte	type;			   \
	.byte	message

/********************************************************************************/
/*	seqSetMusic																	*/
/********************************************************************************/

#define	seqSetMusic(type, music) \
	.byte	_seqSetMusic_id; \
	.byte	8;				 \
	.half	type;			 \
	.half	music;			 \
	.half	PAD

/********************************************************************************/
/*	seqPlayMusic																*/
/********************************************************************************/

#define	seqPlayMusic(music, fade) \
	.byte	_seqPlayMusic_id; \
	.byte	4;				  \
	.half	music

/********************************************************************************/
/*	seqStopMusic																*/
/********************************************************************************/

#define	seqStopMusic(fade) \
	.byte	_seqStopMusic_id; \
	.byte	4;				  \
	.half	fade

/********************************************************************************/
/*	seqWindZone																	*/
/********************************************************************************/

#define	seqWindZone(px1, pz1, px2, pz2, angle)  \
	.byte	_seqWindZone_id; \
	.byte	12;				 \
	.half	px1;			 \
	.half	pz1;			 \
	.half	px2;			 \
	.half	pz2;			 \
	.half	angle

/********************************************************************************/
/*	seqWaterJet																	*/
/********************************************************************************/

#define	seqWaterJet(number, posx, posy, posz, force, koopa2)  \
	.byte	_seqWaterJet_id; \
	.byte	12;				 \
	.byte	number;			 \
	.byte	koopa2;		 	 \
	.half	posx;			 \
	.half	posy;			 \
	.half	posz;			 \
	.half	force

/********************************************************************************/
/*	seqSetGameMode																*/
/********************************************************************************/

#define	seqSetGameMode(type) \
	.byte	_seqGameMode_id; \
	.byte	4;				 \
	.byte	0;				 \
	.byte	type

/********************************************************************************/
/*	seqGetGameMode																*/
/********************************************************************************/

#define	seqGetGameMode(type) \
	.byte	_seqGameMode_id; \
	.byte	4;				 \
	.byte	1;				 \
	.byte	type


#endif
#endif
