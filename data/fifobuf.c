/********************************************************************************
						Ultra 64 MARIO Brothers

						 RSP FIFO buffer module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							April 30, 1995
 ********************************************************************************/

#include "../headers.h"


uwlong	rspDramFifo[RSP_FIFO_SIZE64];
