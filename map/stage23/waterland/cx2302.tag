extern short cx2302_info_tag[]={
	TAGCODE_e_green_block+(  0<<9), 6800,  500, -850,    0,
	TAGCODE_e_blue_block+(  0<<9), 6800,  500, -150,    0,
	TAGCODE_e_timeb_sw+(  0<<9), 6800,  110, 2000,    0,
	TAGCODE_e_timeblock1+(  0<<9), 6075, -100, 2000,    0,
	TAGCODE_e_timeblock1+(  0<<9), 5875,  100, 2000,    0,
	TAGCODE_e_timeblock1+(  0<<9), 5675,  100, 2000,    0,
	TAGCODE_e_timeblock1+(  0<<9), 5475,  300, 2000,    0,
	TAGCODE_e_timeblock1+(  0<<9), 5275,  300, 2000,    0,
	TAGCODE_e_timeblock1+(  0<<9), 5075,  500, 2000,    0,
	TAGCODE_e_timeblock1+(  0<<9), 4875,  500, 2000,    0,
	TAGCODE_e_timeblock1+(  0<<9), 6275, -100, 2000,    0,
	TAGCODE_e_timeblock1+(  0<<9), 6475, -100, 2000,    0,
	TAGCODE_e_coin_appstar+(  0<<9), 4680, 1155,-2200,    0,
	TAGCODE_e_coin_appstar+(  0<<9), 3525, 1155,-2250,    0,
	TAGCODE_e_coin_appstar+(  0<<9), 3850, 1111, 3550,    0,
	TAGCODE_e_coin_appstar+(  0<<9), 4400, 1288, 3595,    0,
	TAGCODE_e_coin_appstar+(  0<<9), 2974, 1015, 3584,    0,
	TAGCODE_e_coin_appstar+(  0<<9), 5550, 1015, 3575,    0,
	TAGCODE_e_coin_appstar+(  0<<9), 3300, 1111,-1900,    0,
	TAGCODE_e_coin_appstar+(  0<<9), 2100, 1025,-1600,    0,
	TAGCODE_e_sakana_5+(  0<<9), 2061,-3323, 2246,    0,
	TAGCODE_e_pirania_5+(  0<<9), 5661,-2923,-1415,    0,
	TAGCODE_e_bigshell+( 32<<9), 1273,-4106, 2320,    0,
	TAGCODE_e_t_kanban+( 64<<9), 3086,  110, 6120,   53,
	TAGCODE_e_wakame+(  0<<9), 6420,-4106,-2026,    0,
	TAGCODE_e_wakame+(  0<<9), 6340,-4106,-1413,    0,
	TAGCODE_e_bigshell+( 48<<9), 1966,-4106, 3226,    0,
	TAGCODE_e_bigshell+( 64<<9), 3886,-4006, 5130,    0,
	TAGCODE_e_timeblock1+(  0<<9), 5740,  710, 3900,    0,
	TAGCODE_e_timeblock1+(  0<<9), 5740,  510, 4100,    0,
	TAGCODE_e_timeblock1+(  0<<9), 5740,  310, 4300,    0,
	TAGCODE_e_timeblock1+(  0<<9), 5740,  110, 4500,    0,
	TAGCODE_e_wakame+(  0<<9), 2999,-4092, 4430,    0,
	TAGCODE_e_manycoin2+(  0<<9), 4876,-4087, 4430,    0,
	TAGCODE_e_manycoin5+(  0<<9), 3892,-4030,-3830,    0,
	TAGCODE_e_manycoin1+( 32<<9), 3923,  110, 5953,    0,
	TAGCODE_e_fireball+(  0<<9),  938,  153, 4107,    0,
	TAGCODE_e_timeblock1+(  0<<9), 5940,  110, 4500,    0,
	TAGCODE_e_switchcoin+(  0<<9), 1614,  928,  922,    0,
	TAGCODE_e_bluecoin+(  0<<9), 1414, 1020,  622,    0,
	TAGCODE_e_bluecoin+(  0<<9), 1814, 1020,  622,    0,
	TAGCODE_e_bluecoin+(  0<<9), 1814, 1020, 1222,    0,
	TAGCODE_e_bluecoin+(  0<<9), 1414, 1020, 1222,    0,
	TAGCODE_e_bluecoin+(  0<<9), 1414, 1020,  922,    0,
	TAGCODE_e_bluecoin+(  0<<9), 1814, 1020,  922,    0,
	TAGCODE_e_koura+(  0<<9), 3886,-4050, 5100,    0,
	TAGCODE_END
};
