
/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
   					-------------------------

					File		: democamera.h
					Description	: 
					Date		: 1995.11.13
					Author		: -------------

 ********************************************************************************/



/********************************************************************************/

#define		DEMO_CANNON_APPEAR			1		/* cannon demo start trigger 	 */
#define		DEMO_CANNON_FIRE			2		/* cannon demo end	 trigger	 */
#define		DEMO_TRAP_FALL				3		/* ochiru demo start trigger	 */
#define		BATTLE_KUPPA				4		/* kuppa to battle suru trigger  */
#define		DEMO_TRIPDOOR				5		/* door animation trigger		 */
#define		DEMO_NORMALDOOR				6		/* door animation trigger		 */
#define		BATTLE_KUPPA_JUMPEND		7		/* kuppa ga jump owatta toki	 */
#define		BATTLE_KUPPA_BOUND			8		/* kuppa ga damage de bound suru */
#define		DEMO_MAINMAP				9		/* main map staring demo		 */
#define		DEMO_ENDING_A				10		/* ending demo A part  */
#define		DEMO_ENDING_B				11		/* ending demo B part  */
#define		DEMO_SAYONARA				12		/* Final sequence  */
#define		DEMO_CREDITS				13		/* Stuff roll  */

/********************************************************************************/

