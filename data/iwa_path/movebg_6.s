/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Stage 6 Strategy

					April 11 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/*==============================================================================
						Path Data
 ===============================================================================*/
e_clock_furiko:
	p_initialize(option)
	p_setbit(flag,stf_moveON)
	p_program(s_clock_furiko_init)

	p_while
		p_program(s_clock_furiko_main)
	p_loop



/*################*/
#else
/*################*/


/*==================================================================================================
		C Program
 ===================================================================================================*/
#define furiko_accel	(execstp->s[stw_work0].d)
#define furiko_velo		(execstp->s[stw_work1].d)


/***************************************************************************************************
						    Init
***************************************************************************************************/
extern void s_clock_furiko_init(void)
{

	obj_anglespeedZ = 0x100;

	s_areastage_init();
}
/***************************************************************************************************
						    Main
***************************************************************************************************/
extern void s_clock_furiko_main(void)
{

	if (obj_animeangleZ > 0) obj_anglespeedZ -= 0x8;
	else					 obj_anglespeedZ += 0x8;

	obj_animeangleZ += obj_anglespeedZ; 

	if (obj_anglespeedZ == 0x10 || obj_anglespeedZ == -0x10) objsound(NA_SE2_FURIKO_S);
}




/*################*/
#endif
/*################*/


