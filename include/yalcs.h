/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
   					-------------------------

					File		: yalcs.h
					Description	: yajima " golbal memory "
					Date		: 1995.4.10
					Author		: H.yajima

 ********************************************************************************/



/********************************************************************************
		 		Global Memory
 ********************************************************************************/


/*------------------------------------------------------------------------------*/
/*			Debug Memory														*/
/*------------------------------------------------------------------------------*/

#define					debug_all					0xff
#define					debug_display				1
#define					debug_error					1<<1

Mglobal	int				debugflag;					/* debug display 			*/
												
Mglobal	int				DBF_BGnull;					/* BGcheck (ground)NULL 	*/
Mglobal int				DBF_roofnull;				/* BGcheck (root)NULL		*/
Mglobal	int				DBF_wall;					/* BGcheck Wall Hit !!		*/
Mglobal	int				DBF_objcount;				/* strategy counterq		*/

Mglobal BGDebugInfo		bgdebuginfo;				/* bgcheck debug information */

Mglobal short			yd_work[16][8];				/* yajima debug work 		 */
Mglobal short			yd_init[16][8];				/* yajima debug work 		 */

/*------------------------------------------------------------------------------*/
/*			Strategy Memory														*/
/*------------------------------------------------------------------------------*/

Mglobal	unsigned long	strategy_mainflag;			/* strategy main control flag */

Mglobal StrategyRecord 	strategy[NUM_STRATEGY];		/* Strategy Buffer			*/
Mglobal StrategyRecord	dmmy;						/* dmmy Strategy Record		*/

Mglobal	StrategyList	*stratroot;					/* strategy root pointer	*/
Mglobal	StrategyList	stratfree;					/* freelist root			*/
					
Mglobal StrategyRecord 	*player1stp;				/* player1 strategy pointer	*/
Mglobal StrategyRecord 	*player2stp;				/* player1 strategy pointer	*/
Mglobal StrategyRecord 	*execstp;					/* execute strategy pointer */

Mglobal unsigned long  	*path;						/* path pointer				*/

Mglobal	short			beforeframe_objcount;		/* before object counter	*/


/*------------------------------------------------------------------------------*/
/*			BG Check Memory														*/
/*------------------------------------------------------------------------------*/

Mglobal int				bgcheck_listNo;				/* RAM Pointer BGCheckList		*/
Mglobal int				bgcheck_dataNo;				/* RAM Pointer BGCheckData		*/
Mglobal int				bgcheck_listEND;			/*	static data end 			*/
Mglobal int				bgcheck_dataEND;			/*	static data end				*/

Mglobal	HeapPtr			stratheap;					/* strategy heap pointer		*/

Mglobal	short			camera_bgflag;				/* camera & player bgcheck ON/OFF */
Mglobal	short			bgcheck_nukiflag;			/* bgcheck(18)	data			  */

Mglobal short			*waterline;					/* water data pointer			*/
Mglobal	long			waterline_basic[WATERLINE_MAX];		/* waterline buffer		*/

/*------------------------------------------------------------------------------*/
/*		area enemy buffer														*/
/*------------------------------------------------------------------------------*/

#define	AREA_COUNT_MAX		60		/* max 60 area	*/
Mglobal char			map_areaenemy[AREA_COUNT_MAX][2];

Mglobal short			map_areacode;				/* player area NumberCode 	*/
Mglobal	short			area_coin;					/* area coin counter		*/
Mglobal	short			area_door;					/* area door counter		*/

Mglobal short			object_courseflag;			/* course flag */
Mglobal	short			stage_objparameter;			/* course parameter */

Mglobal	short			cannon_useflag;				/* cannon used flag			*/
Mglobal short			course_ramsave_flag;		/* course ramsave flag 		*/

Mglobal	short			areain_obj;		/* area IN  object count	*/
Mglobal	short			areaout_obj;	/* area OUT object count	*/

Mglobal	short			waterswitch_flag;	/* water switch flag */
Mglobal	short			bf_teresa_flag;		/* BF teresa flag    */

#define 	BIGWORLD_WATER		1

#define		RAMSAVE_SLIDER_STAR		1

/*==============================================================================*/
/*				End End End End End End											*/
/*==============================================================================*/


