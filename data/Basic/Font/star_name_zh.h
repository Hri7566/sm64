/**************************************************************************************************
					Message Data 	for ENGLISH															   
								programed by Iwamoto Daiki										   
**************************************************************************************************/

#include "../../../i10n/assets/zh/star_name_zh.msg"

static unsigned char star_name_91[] = { 0xff, 0xff,};
static unsigned char star_name_92[] = { 0xff, 0xff,};
static unsigned char star_name_93[] = { 0xff, 0xff,};
static unsigned char star_name_94[] = { 0xff, 0xff,};
static unsigned char star_name_95[] = { 0xff, 0xff,};
static unsigned char star_name_96[] = { 0xff, 0xff,};


unsigned char *starnamePtr[] = {star_name_00,star_name_01,star_name_02,star_name_03,star_name_04,star_name_05,star_name_06,star_name_07,star_name_08,star_name_09,star_name_10,star_name_11,star_name_12,star_name_13,star_name_14,star_name_15,
								star_name_16,star_name_17,star_name_18,star_name_19,star_name_20,star_name_21,star_name_22,star_name_23,star_name_24,star_name_25,star_name_26,star_name_27,star_name_28,star_name_29,star_name_30,star_name_31,
								star_name_32,star_name_33,star_name_34,star_name_35,star_name_36,star_name_37,star_name_38,star_name_39,star_name_40,star_name_41,star_name_42,star_name_43,star_name_44,star_name_45,star_name_46,star_name_47,
								star_name_48,star_name_49,star_name_50,star_name_51,star_name_52,star_name_53,star_name_54,star_name_55,star_name_56,star_name_57,star_name_58,star_name_59,star_name_60,star_name_61,star_name_62,star_name_63,
								star_name_64,star_name_65,star_name_66,star_name_67,star_name_68,star_name_69,star_name_70,star_name_71,star_name_72,star_name_73,star_name_74,star_name_75,star_name_76,star_name_77,star_name_78,star_name_79,
								star_name_80,star_name_81,star_name_82,star_name_83,star_name_84,star_name_85,star_name_86,star_name_87,star_name_88,star_name_89,star_name_90,star_name_91,star_name_92,star_name_93,star_name_94,star_name_95,
								star_name_96,NULL};

