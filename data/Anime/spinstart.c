/********************************************************************************
						Ultra 64 MARIO Brothers

				mario spin jumping start animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

						   September 19, 1995
 ********************************************************************************/

#include "../headers.h"


static short mario_revolving_jump_start_prm[]={
	    0,  189,16383, -118,-1664,-3985,-1147,-1205,
	-1337,-1477,-1560,-31256,-33828,-39630,-45791,-49440,
	-27465,-26240,-23476,-20541,-18803,13315,16689,24300,
	32382,37168,-4724,-1773,-1700,-1535,-1360,-1256,
	  795, 3510, 9633,16134,19985, 5527, 6687, 9304,
	12084,13730,-19438,-15739,-7394, 1465, 6713,  -20,
	   50,  210,  380,  481,  604,  569,  492,  409,
	  360,-14367,-12949,-9750,-6353,-4341,19122,16669,
	11133, 5256, 1775,  -40,  -74, -149, -230, -277,
	 -338, -317, -272, -224, -195,-26193,-24952,-22150,
	-19176,-17414,  168,  131,   46,  -42,  -95, -234,
	 -240, -251, -264, -271,-14080,-12663,-9466,-6071,
	-4061,17939,15617,10378, 4815, 1521,  -18,   -6,
	   19,   47,   63,  106,  103,   97,   91,   87,
	-25542,-24373,-21737,-18938,-17281,32767,32767,16753,
	-16545,32767,32767,16753,-16545, 1993, 1321, -193,
	-1803,-2756, 5100, 4355, 2675,  890, -165,19095,
	18705,17830,16914,16399
};
static short mario_revolving_jump_start_tbl[]={
	    1,    0,	/* chn14translate x */
	    1,    1,	/* chn14 translate y */
	    1,    0,	/* chn14 translate z */
	    1,    0,	/* chn14 rotate x */
	    1,    2,	/* chn14 rotate y */
	    1,    0,	/* chn14 rotate z */
	    1,    0,	/* <m_waist>1_3 rotate x */
	    1,    0,	/* <m_waist>1_3 rotate y */
	    5,  135,	/* <m_waist>1_3 rotate z */
	    1,    0,	/* <m_body>1 rotate x */
	    1,    0,	/* <m_body>1 rotate y */
	    5,  130,	/* <m_body>1 rotate z */
	    1,    0,	/* <m_head>2 rotate x */
	    1,    0,	/* <m_head>2 rotate y */
	    5,  125,	/* <m_head>2 rotate z */

	    1,  121,	/* chn6 rotate x */
	    1,  122,	/* chn6 rotate y */
	    1,  123,	/* chn6 rotate z */
	    5,   11,	/* <m_larmA>1 rotate x */
	    5,   16,	/* <m_larmA>1 rotate y */
	    5,   21,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	    5,    6,	/* <m_larmB>1 rotate z */
	    1,    3,	/* <m_lhand>1 rotate x */
	    1,    4,	/* <m_lhand>1 rotate y */
	    1,    5,	/* <m_lhand>1 rotate z */

	    1,  117,	/* chn10 rotate x */
	    1,  118,	/* chn10 rotate y */
	    1,  119,	/* chn10 rotate z */
	    5,   32,	/* <m_rarmA>1 rotate x */
	    5,   37,	/* <m_rarmA>1 rotate y */
	    5,   42,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	    5,   27,	/* <m_rarmB>1 rotate z */
	    1,    0,	/* <m_rhand>1 rotate x */
	    1,    0,	/* <m_rhand>1 rotate y */
	    1,   26,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  120,	/* chn15 rotate z */
	    5,  102,	/* <m_llegA>1 rotate x */
	    5,  107,	/* <m_llegA>1 rotate y */
	    5,  112,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	    5,   97,	/* <m_llegB>1 rotate z */
	    5,   82,	/* <m_ltoot>1 rotate x */
	    5,   87,	/* <m_ltoot>1 rotate y */
	    5,   92,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  124,	/* chn17 rotate z */
	    5,   67,	/* <m_rlegA>1 rotate x */
	    5,   72,	/* <m_rlegA>1 rotate y */
	    5,   77,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	    5,   62,	/* <m_rlegB>1 rotate z */
	    5,   47,	/* <m_rfoot>1 rotate x */
	    5,   52,	/* <m_rfoot>1 rotate y */
	    5,   57,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioSpinJpStart = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	 5,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_revolving_jump_start_prm,
	mario_revolving_jump_start_tbl
};
