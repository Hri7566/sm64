extern short cx3602_info_tag[]={
	TAGCODE_e_dummy_kinoko+(  0<<9), 6100, 4836, 6981,    0,
	TAGCODE_e_dummy_kinoko+(  0<<9), 6645, 4800, 7563,    0,
	TAGCODE_e_dummy_kinoko+(  0<<9), 7845, 4836, 6327,    0,
	TAGCODE_e_dummy_kinoko+(  0<<9), 7263, 4836, 5745,    0,
	TAGCODE_e_1up_sec+(  0<<9), 6936, 4800, 6654,    4,
	TAGCODE_e_coin_ground+(  0<<9), 6281, 4836, 6472,    0,
	TAGCODE_e_1up_kinoko+(  0<<9), 6754, 4800, 5963,    0,
	TAGCODE_e_1up_slider+(  0<<9), 1764, 2943, 1480,    0,
	TAGCODE_e_manycoin1+( 16<<9), 2972, 3963, 2690,    0,
	TAGCODE_e_manycoin1+( 16<<9),   27, 2145, -290,    0,
	TAGCODE_e_coin_ground+(  0<<9),-2763,  -37,-2981,    0,
	TAGCODE_END
};
