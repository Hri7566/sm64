/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage11 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx gfx_po_body01[];
extern Gfx gfx_po_rgba01[];
extern Gfx gfx_po_body02[];
extern Gfx gfx_po_rgba02[];
extern Gfx gfx_po_seal[]  ;
extern Gfx gfx_po_seal2[] ;
extern Gfx gfx_po_mobj01[];
extern Gfx gfx_po_mobj02[];
extern Gfx gfx_po_mobj03[];
extern Gfx gfx_po_mobj04[];
extern Gfx gfx_po_mobj05[];
extern Gfx gfx_po_mobj06[];
extern Gfx gfx_po_mobj07[];

extern ulong WaterPoolHeight(int, MapNode*, void*);


/* --------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------- */


/* ===============================================================================
        : Hierarchy map data of PoMobj01.
================================================================================== */
Hierarchy RCP_HmsPoMobj01[] = {
    hmsHeader(550)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_po_mobj01)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of PoMobj02.
================================================================================== */
Hierarchy RCP_HmsPoMobj02[] = {
    hmsHeader(350)
    hmsBegin()
		hmsShadow(110, 150, 12)
		hmsBegin()
	        hmsGfx(RM_SURF, gfx_po_mobj02)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of PoMobj03.
================================================================================== */
Hierarchy RCP_HmsPoMobj03[] = {
    hmsHeader(200)
    hmsBegin()
		hmsShadow(90, 150, 11)
		hmsBegin()
	        hmsGfx(RM_XSURF, gfx_po_mobj03)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of PoMobj04.
================================================================================== */
Hierarchy RCP_HmsPoMobj04[] = {
    hmsHeader(420)
    hmsBegin()
		hmsShadow(240, 150, 12)
		hmsBegin()
			hmsGfx(RM_SURF, gfx_po_mobj04)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of PoMobj05.
================================================================================== */
Hierarchy RCP_HmsPoMobj05[] = {
    hmsHeader(800)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_po_mobj05)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of PoMobj06.
================================================================================== */
Hierarchy RCP_HmsPoMobj06[] = {
    hmsHeader(900)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_po_mobj06)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of PoMobj07.
================================================================================== */
Hierarchy RCP_HmsPoMobj07[] = {
    hmsHeader(450)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_po_mobj07)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage11Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(2, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_FIELD,  0,2000,6000,  -4352,0,-4352, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF  , gfx_po_body01)
					hmsGfx(RM_SPRITE, gfx_po_rgba01)
					hmsGfx(RM_XSURF , gfx_po_seal)
					hmsGfx(RM_XDECAL, gfx_po_seal2)
					hmsCProg(0, WaterPoolHeight)
					hmsCProg(0	   , WaterInit)
					hmsCProg(0x1101, WaterDraw)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

		hmsLayer(0)
		hmsBegin()
			hmsCProg(0, CannonEdge)
		hmsEnd()

	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 2.														*/
/********************************************************************************/
Hierarchy RCP_Stage11Scene2[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(0,0,0,1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_FIELD,  0,2000,6000,  -4352,0,-4352, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF	, gfx_po_body02)
					hmsGfx(RM_SPRITE, gfx_po_rgba02)
					hmsGfx(RM_XDECAL, gfx_po_seal2)
					hmsCProg(0	   , WaterInit)
					hmsCProg(0x1102, WaterDraw)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
