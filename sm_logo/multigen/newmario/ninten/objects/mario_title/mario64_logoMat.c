/**********************************************************************
 *                                                                    *
 *  Materials for model:                                              *
 *     ./newmario/nin/objects/mario64_logo.nin                        *
 *                                                                    *
 *  THIS FILE WAS CREATED BY NINGEN                                   *
 *                                                                    *
 *   25 Materials,    400 bytes                                       *
 *                                                                    *
 **********************************************************************/


#include <mbi.h>

Material mario64_logo_material[] = {
{252,	68,	68,	255,	 /* ambient            */ 
252,	68,	68,	255,	 /* diffuse            */ 
252,	68,	68,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{237,	14,	1,	255,	 /* ambient            */ 
237,	14,	1,	255,	 /* diffuse            */ 
237,	14,	1,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{90,	0,	0,	255,	 /* ambient            */ 
90,	0,	0,	255,	 /* diffuse            */ 
90,	0,	0,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{254,	254,	80,	255,	 /* ambient            */ 
254,	254,	80,	255,	 /* diffuse            */ 
254,	254,	80,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{150,	150,	0,	255,	 /* ambient            */ 
150,	150,	0,	255,	 /* diffuse            */ 
150,	150,	0,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{80,	80,	0,	255,	 /* ambient            */ 
80,	80,	0,	255,	 /* diffuse            */ 
80,	80,	0,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{254,	255,	0,	255,	 /* ambient            */ 
254,	255,	0,	255,	 /* diffuse            */ 
254,	255,	0,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{80,	80,	255,	255,	 /* ambient            */ 
80,	80,	255,	255,	 /* diffuse            */ 
80,	80,	255,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{0,	0,	150,	255,	 /* ambient            */ 
0,	0,	150,	255,	 /* diffuse            */ 
0,	0,	150,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{0,	0,	80,	255,	 /* ambient            */ 
0,	0,	80,	255,	 /* diffuse            */ 
0,	0,	80,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{0,	0,	255,	255,	 /* ambient            */ 
0,	0,	255,	255,	 /* diffuse            */ 
0,	0,	255,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{150,	0,	0,	255,	 /* ambient            */ 
150,	0,	0,	255,	 /* diffuse            */ 
150,	0,	0,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{215,	141,	251,	255,	 /* ambient            */ 
215,	141,	251,	255,	 /* diffuse            */ 
215,	141,	251,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{164,	18,	242,	255,	 /* ambient            */ 
164,	18,	242,	255,	 /* diffuse            */ 
164,	18,	242,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{156,	13,	233,	255,	 /* ambient            */ 
156,	13,	233,	255,	 /* diffuse            */ 
156,	13,	233,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{115,	8,	171,	255,	 /* ambient            */ 
115,	8,	171,	255,	 /* diffuse            */ 
115,	8,	171,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{97,	7,	144,	255,	 /* ambient            */ 
97,	7,	144,	255,	 /* diffuse            */ 
97,	7,	144,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{255,	255,	255,	255,	 /* ambient            */ 
255,	255,	255,	255,	 /* diffuse            */ 
255,	255,	255,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{143,	143,	143,	255,	 /* ambient            */ 
143,	143,	143,	255,	 /* diffuse            */ 
143,	143,	143,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{93,	93,	93,	255,	 /* ambient            */ 
93,	93,	93,	255,	 /* diffuse            */ 
93,	93,	93,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{216,	216,	216,	255,	 /* ambient            */ 
216,	216,	216,	255,	 /* diffuse            */ 
216,	216,	216,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{80,	254,	80,	255,	 /* ambient            */ 
80,	254,	80,	255,	 /* diffuse            */ 
80,	254,	80,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{0,	150,	0,	255,	 /* ambient            */ 
0,	150,	0,	255,	 /* diffuse            */ 
0,	150,	0,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{0,	80,	0,	255,	 /* ambient            */ 
0,	80,	0,	255,	 /* diffuse            */ 
0,	80,	0,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
{0,	255,	0,	255,	 /* ambient            */ 
0,	255,	0,	255,	 /* diffuse            */ 
0,	255,	0,	255,	 /* specular           */ 
6,	0,	0,	0},	       /* shininess etc. */ 
};
