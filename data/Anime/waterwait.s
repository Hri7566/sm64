/********************************************************************************
						Ultra 64 MARIO Brothers

					mario water waiting animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							January 29, 1996
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioWaterWait:
	.half	 MAP_ANIM_NORMAL 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	80 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x0F9C						/* total bytes       */


	.half	    1,    0 	/* chn14_2_1translate x */
	.half	    1,    1 	/* chn14_2_1 translate y */
	.half	    1,    0 	/* chn14_2_1 translate z */
	.half	    1,    0 	/* chn14_2_1 rotate x */
	.half	    1,    2 	/* chn14_2_1 rotate y */
	.half	    1,    0 	/* chn14_2_1 rotate z */
	.half	    1,    0 	/* <m_waist>1_3 rotate x */
	.half	    1,    0 	/* <m_waist>1_3 rotate y */
	.half	   80, 1780 	/* <m_waist>1_3 rotate z */
	.half	    1,    0 	/* <m_body>1 rotate x */
	.half	    1,    0 	/* <m_body>1 rotate y */
	.half	    1, 1779 	/* <m_body>1 rotate z */
	.half	   80, 1619 	/* <m_head>2 rotate x */
	.half	    1,    0 	/* <m_head>2 rotate y */
	.half	   80, 1699 	/* <m_head>2 rotate z */
	.half	    1, 1615 	/* chn6 rotate x */
	.half	    1, 1616 	/* chn6 rotate y */
	.half	    1, 1617 	/* chn6 rotate z */
	.half	    1,  244 	/* <m_larmA>1 rotate x */
	.half	    1,  245 	/* <m_larmA>1 rotate y */
	.half	    1,  246 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1 rotate x */
	.half	    1,    0 	/* <m_larmB>1 rotate y */
	.half	    1,  243 	/* <m_larmB>1 rotate z */
	.half	   80,    3 	/* <m_rhand2>2 rotate x */
	.half	   80,   83 	/* <m_rhand2>2 rotate y */
	.half	   80,  163 	/* <m_rhand2>2 rotate z */
	.half	    1, 1611 	/* chn10 rotate x */
	.half	    1, 1612 	/* chn10 rotate y */
	.half	    1, 1613 	/* chn10 rotate z */
	.half	    1,  488 	/* <m_rarmA>1 rotate x */
	.half	    1,  489 	/* <m_rarmA>1 rotate y */
	.half	    1,  490 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1 rotate y */
	.half	    1,  487 	/* <m_rarmB>1 rotate z */
	.half	   80,  247 	/* <m_rhand2>1 rotate x */
	.half	   80,  327 	/* <m_rhand2>1 rotate y */
	.half	   80,  407 	/* <m_rhand2>1 rotate z */
	.half	    1,    0 	/* chn15 rotate x */
	.half	    1,    0 	/* chn15 rotate y */
	.half	    1, 1614 	/* chn15 rotate z */
	.half	   80, 1371 	/* <m_llegA>1 rotate x */
	.half	   80, 1451 	/* <m_llegA>1 rotate y */
	.half	   80, 1531 	/* <m_llegA>1 rotate z */
	.half	    1,    0 	/* <m_llegB>1 rotate x */
	.half	    1,    0 	/* <m_llegB>1 rotate y */
	.half	   80, 1291 	/* <m_llegB>1 rotate z */
	.half	   80, 1051 	/* <m_ltoot>1 rotate x */
	.half	   80, 1131 	/* <m_ltoot>1 rotate y */
	.half	   80, 1211 	/* <m_ltoot>1 rotate z */
	.half	    1,    0 	/* chn17 rotate x */
	.half	    1,    0 	/* chn17 rotate y */
	.half	    1, 1618 	/* chn17 rotate z */
	.half	   80,  811 	/* <m_rlegA>1 rotate x */
	.half	   80,  891 	/* <m_rlegA>1 rotate y */
	.half	   80,  971 	/* <m_rlegA>1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1 rotate y */
	.half	   80,  731 	/* <m_rlegB>1 rotate z */
	.half	   80,  491 	/* <m_rfoot>1 rotate x */
	.half	   80,  571 	/* <m_rfoot>1 rotate y */
	.half	   80,  651 	/* <m_rfoot>1 rotate z */

	.half	0x0000,0x00B4,0x3FFF,0x1D62,0x1D5A,0x1D44,0x1D20,0x1CF2,0x1CBB,0x1C7C,0x1C38,0x1BF0,0x1BA6,0x1B5D,0x1B15,0x1AD1
	.half	0x1A92,0x1A5B,0x1A2D,0x1A09,0x19F3,0x19EB,0x19F1,0x1A04,0x1A21,0x1A47,0x1A75,0x1AAA,0x1AE5,0x1B23,0x1B64,0x1BA6
	.half	0x1BE9,0x1C2A,0x1C68,0x1CA2,0x1CD7,0x1D06,0x1D2C,0x1D49,0x1D5C,0x1D62,0x1D6B,0x1D83,0x1DAB,0x1DDF,0x1E1E,0x1E66
	.half	0x1EB6,0x1F0C,0x1F66,0x1FC3,0x2021,0x207E,0x20D8,0x212E,0x217E,0x21C7,0x2206,0x223A,0x2261,0x227A,0x2282,0x2279
	.half	0x225E,0x2233,0x21FA,0x21B5,0x2167,0x2111,0x20B4,0x2054,0x1FF2,0x1F90,0x1F30,0x1ED4,0x1E7D,0x1E2F,0x1DEA,0x1DB2
	.half	0x1D87,0x1D6B,0x1D62,0x09A2,0x0983,0x092B,0x08A1,0x07EC,0x0713,0x061E,0x0513,0x03FA,0x02DA,0x01B9,0x00A0,0xFF96
	.half	0xFEA1,0xFDC8,0xFD13,0xFC89,0xFC31,0xFC12,0xFC2B,0xFC73,0xFCE5,0xFD7B,0xFE31,0xFF00,0xFFE5,0x00D8,0x01D6,0x02DA
	.half	0x03DD,0x04DC,0x05D0,0x06B4,0x0783,0x0839,0x08CF,0x0941,0x0989,0x09A2,0x0992,0x0963,0x091A,0x08B8,0x0841,0x07BA
	.half	0x0723,0x0682,0x05D8,0x052A,0x047A,0x03CC,0x0322,0x0281,0x01EA,0x0162,0x00EC,0x008A,0x0041,0x0012,0x0002,0x0014
	.half	0x0047,0x0098,0x0102,0x0183,0x0216,0x02B8,0x0365,0x041A,0x04D2,0x058A,0x063F,0x06EC,0x078E,0x0821,0x08A2,0x090C
	.half	0x095D,0x0990,0x09A2,0xE3BD,0xE3BA,0xE3B1,0xE3A4,0xE392,0xE37D,0xE365,0xE34B,0xE330,0xE314,0xE2F8,0xE2DC,0xE2C2
	.half	0xE2AA,0xE295,0xE283,0xE276,0xE26D,0xE26A,0xE26D,0xE274,0xE27F,0xE28E,0xE29F,0xE2B3,0xE2CA,0xE2E2,0xE2FA,0xE314
	.half	0xE32D,0xE346,0xE35E,0xE374,0xE388,0xE39A,0xE3A9,0xE3B4,0xE3BB,0xE3BD,0xE3AE,0xE384,0xE340,0xE2E7,0xE27B,0xE1FF
	.half	0xE175,0xE0E2,0xE047,0xDFA8,0xDF07,0xDE68,0xDDCD,0xDD39,0xDCB0,0xDC34,0xDBC8,0xDB6E,0xDB2B,0xDB00,0xDAF2,0xDB02
	.half	0xDB31,0xDB7A,0xDBDC,0xDC51,0xDCD8,0xDD6C,0xDE0A,0xDEAF,0xDF57,0xE000,0xE0A5,0xE143,0xE1D7,0xE25D,0xE2D3,0xE334
	.half	0xE37E,0xE3AD,0xE3BD,0xCBE0,0x76C0,0xA1DF,0x5630,0xE839,0xE83D,0xE84A,0xE85F,0xE87A,0xE89A,0xE8BE,0xE8E6,0xE910
	.half	0xE93B,0xE966,0xE990,0xE9B7,0xE9DC,0xE9FC,0xEA17,0xEA2B,0xEA39,0xEA3D,0xEA39,0xEA2F,0xEA1E,0xEA07,0xE9EC,0xE9CE
	.half	0xE9AC,0xE987,0xE961,0xE93B,0xE914,0xE8EE,0xE8CA,0xE8A8,0xE889,0xE86E,0xE858,0xE847,0xE83C,0xE839,0xE83C,0xE846
	.half	0xE855,0xE869,0xE882,0xE89E,0xE8BD,0xE8DD,0xE900,0xE923,0xE946,0xE968,0xE989,0xE9A8,0xE9C4,0xE9DC,0xE9F1,0xEA00
	.half	0xEA0A,0xEA0D,0xEA0A,0xEA01,0xE9F3,0xE9E1,0xE9CA,0xE9B0,0xE994,0xE975,0xE955,0xE933,0xE912,0xE8F1,0xE8D1,0xE8B2
	.half	0xE895,0xE87C,0xE865,0xE852,0xE844,0xE83C,0xE839,0xEF87,0xEFA4,0xEFF5,0xF074,0xF11B,0xF1E2,0xF2C4,0xF3B9,0xF4BB
	.half	0xF5C5,0xF6CE,0xF7D0,0xF8C5,0xF9A7,0xFA6E,0xFB15,0xFB94,0xFBE5,0xFC02,0xFBEA,0xFBA8,0xFB3F,0xFAB5,0xFA0E,0xF950
	.half	0xF87E,0xF79D,0xF6B3,0xF5C5,0xF4D6,0xF3EC,0xF30B,0xF239,0xF17B,0xF0D4,0xF04A,0xEFE1,0xEF9F,0xEF87,0xEF9A,0xEFCF
	.half	0xF023,0xF092,0xF118,0xF1B2,0xF25B,0xF30F,0xF3CB,0xF48B,0xF54B,0xF607,0xF6BB,0xF764,0xF7FD,0xF883,0xF8F2,0xF946
	.half	0xF97B,0xF98E,0xF97D,0xF94D,0xF900,0xF89A,0xF81F,0xF791,0xF6F5,0xF64C,0xF59C,0xF4E6,0xF42F,0xF37A,0xF2C9,0xF221
	.half	0xF184,0xF0F7,0xF07B,0xF016,0xEFC9,0xEF98,0xEF87,0xE7ED,0xE7DB,0xE7A7,0xE755,0xE6EA,0xE66B,0xE5DA,0xE53D,0xE497
	.half	0xE3ED,0xE343,0xE29D,0xE200,0xE16F,0xE0F0,0xE085,0xE033,0xDFFF,0xDFED,0xDFFC,0xE026,0xE06A,0xE0C2,0xE12D,0xE1A7
	.half	0xE22E,0xE2BE,0xE354,0xE3ED,0xE486,0xE51C,0xE5AC,0xE633,0xE6AD,0xE718,0xE771,0xE7B4,0xE7DE,0xE7ED,0xE7E1,0xE7BE
	.half	0xE787,0xE73F,0xE6E8,0xE684,0xE616,0xE5A1,0xE526,0xE4A9,0xE42C,0xE3B2,0xE33C,0xE2CE,0xE26A,0xE213,0xE1CB,0xE194
	.half	0xE171,0xE165,0xE170,0xE190,0xE1C2,0xE204,0xE254,0xE2B1,0xE317,0xE384,0xE3F7,0xE46D,0xE4E5,0xE55B,0xE5CE,0xE63C
	.half	0xE6A2,0xE6FE,0xE74E,0xE790,0xE7C2,0xE7E2,0xE7ED,0xCFE4,0x0CCD,0x1BB4,0xD174,0x0960,0x095A,0x094A,0x0931,0x0910
	.half	0x08E9,0x08BD,0x088C,0x085A,0x0825,0x07F1,0x07BE,0x078E,0x0762,0x073B,0x071A,0x0701,0x06F1,0x06EB,0x06F0,0x06FD
	.half	0x0711,0x072D,0x074D,0x0773,0x079C,0x07C8,0x07F6,0x0825,0x0854,0x0882,0x08AF,0x08D8,0x08FD,0x091E,0x0939,0x094E
	.half	0x095B,0x0960,0x095B,0x094E,0x0939,0x091E,0x08FD,0x08D8,0x08AF,0x0882,0x0854,0x0825,0x07F6,0x07C8,0x079C,0x0773
	.half	0x074D,0x072D,0x0711,0x06FD,0x06F0,0x06EB,0x06EF,0x06FB,0x070E,0x0727,0x0745,0x0768,0x078E,0x07B7,0x07E3,0x080F
	.half	0x083C,0x0868,0x0894,0x08BD,0x08E3,0x0906,0x0924,0x093D,0x0950,0x095C,0x0960,0x00FC,0x00FC,0x00FD,0x00FE,0x00FF
	.half	0x0101,0x0102,0x0104,0x0106,0x0108,0x010A,0x010C,0x010E,0x010F,0x0111,0x0112,0x0113,0x0114,0x0114,0x0114,0x0113
	.half	0x0112,0x0111,0x0110,0x010F,0x010D,0x010B,0x010A,0x0108,0x0106,0x0104,0x0103,0x0101,0x0100,0x00FF,0x00FE,0x00FD
	.half	0x00FC,0x00FC,0x00FC,0x00FD,0x00FE,0x00FF,0x0100,0x0101,0x0103,0x0104,0x0106,0x0108,0x010A,0x010B,0x010D,0x010F
	.half	0x0110,0x0111,0x0112,0x0113,0x0114,0x0114,0x0114,0x0113,0x0113,0x0112,0x0110,0x010F,0x010E,0x010C,0x010A,0x0109
	.half	0x0107,0x0105,0x0104,0x0102,0x0101,0x00FF,0x00FE,0x00FD,0x00FD,0x00FC,0x00FC,0xBE35,0xBE35,0xBE32,0xBE2E,0xBE29
	.half	0xBE22,0xBE1B,0xBE13,0xBE0B,0xBE03,0xBDFA,0xBDF2,0xBDEA,0xBDE3,0xBDDD,0xBDD7,0xBDD3,0xBDD1,0xBDD0,0xBDD1,0xBDD3
	.half	0xBDD6,0xBDDA,0xBDE0,0xBDE6,0xBDEC,0xBDF4,0xBDFB,0xBE03,0xBE0A,0xBE12,0xBE19,0xBE1F,0xBE26,0xBE2B,0xBE2F,0xBE33
	.half	0xBE35,0xBE35,0xBE35,0xBE33,0xBE2F,0xBE2B,0xBE26,0xBE1F,0xBE19,0xBE12,0xBE0A,0xBE03,0xBDFB,0xBDF4,0xBDEC,0xBDE6
	.half	0xBDE0,0xBDDA,0xBDD6,0xBDD3,0xBDD1,0xBDD0,0xBDD1,0xBDD2,0xBDD5,0xBDDA,0xBDDE,0xBDE4,0xBDEA,0xBDF1,0xBDF8,0xBDFF
	.half	0xBE06,0xBE0D,0xBE14,0xBE1B,0xBE21,0xBE27,0xBE2C,0xBE30,0xBE33,0xBE35,0xBE35,0x2491,0x24A1,0x24CE,0x2515,0x2571
	.half	0x25E0,0x265D,0x26E5,0x2775,0x2808,0x289B,0x292A,0x29B3,0x2A30,0x2A9F,0x2AFB,0x2B42,0x2B6F,0x2B7E,0x2B71,0x2B4D
	.half	0x2B13,0x2AC6,0x2A69,0x29FF,0x298B,0x290E,0x288C,0x2808,0x2783,0x2701,0x2685,0x2610,0x25A6,0x254A,0x24FD,0x24C3
	.half	0x249E,0x2491,0x249E,0x24C3,0x24FD,0x254A,0x25A6,0x2610,0x2685,0x2701,0x2783,0x2808,0x288C,0x290E,0x298B,0x29FF
	.half	0x2A69,0x2AC6,0x2B13,0x2B4D,0x2B71,0x2B7E,0x2B73,0x2B51,0x2B1C,0x2AD6,0x2A81,0x2A1F,0x29B3,0x293E,0x28C4,0x2847
	.half	0x27C9,0x274B,0x26D1,0x265D,0x25F1,0x258F,0x253A,0x24F3,0x24BE,0x249D,0x2491,0x09D0,0x09D2,0x09D8,0x09E1,0x09EC
	.half	0x09FA,0x0A0A,0x0A1B,0x0A2D,0x0A3F,0x0A52,0x0A64,0x0A75,0x0A85,0x0A92,0x0A9E,0x0AA7,0x0AAC,0x0AAE,0x0AAD,0x0AA8
	.half	0x0AA1,0x0A97,0x0A8C,0x0A7F,0x0A70,0x0A60,0x0A50,0x0A3F,0x0A2F,0x0A1F,0x0A0F,0x0A00,0x09F3,0x09E8,0x09DE,0x09D7
	.half	0x09D2,0x09D0,0x09D2,0x09D7,0x09DE,0x09E8,0x09F3,0x0A00,0x0A0F,0x0A1F,0x0A2F,0x0A3F,0x0A50,0x0A60,0x0A70,0x0A7F
	.half	0x0A8C,0x0A97,0x0AA1,0x0AA8,0x0AAD,0x0AAE,0x0AAD,0x0AA9,0x0AA2,0x0A99,0x0A8F,0x0A82,0x0A75,0x0A66,0x0A57,0x0A47
	.half	0x0A38,0x0A28,0x0A19,0x0A0A,0x09FC,0x09F0,0x09E6,0x09DD,0x09D6,0x09D2,0x09D0,0x0CBE,0x0CBC,0x0CB8,0x0CB0,0x0CA7
	.half	0x0C9C,0x0C8F,0x0C81,0x0C72,0x0C63,0x0C54,0x0C45,0x0C37,0x0C2A,0x0C1F,0x0C15,0x0C0E,0x0C0A,0x0C08,0x0C09,0x0C0D
	.half	0x0C13,0x0C1B,0x0C24,0x0C2F,0x0C3B,0x0C48,0x0C55,0x0C63,0x0C71,0x0C7E,0x0C8B,0x0C97,0x0CA1,0x0CAB,0x0CB3,0x0CB9
	.half	0x0CBD,0x0CBE,0x0CBD,0x0CB9,0x0CB3,0x0CAB,0x0CA1,0x0C97,0x0C8B,0x0C7E,0x0C71,0x0C63,0x0C55,0x0C48,0x0C3B,0x0C2F
	.half	0x0C24,0x0C1B,0x0C13,0x0C0D,0x0C09,0x0C08,0x0C09,0x0C0D,0x0C12,0x0C19,0x0C22,0x0C2C,0x0C37,0x0C43,0x0C50,0x0C5C
	.half	0x0C69,0x0C76,0x0C83,0x0C8F,0x0C9A,0x0CA4,0x0CAD,0x0CB4,0x0CB9,0x0CBD,0x0CBE,0xA399,0xA3A0,0xA3B3,0xA3D0,0xA3F7
	.half	0xA425,0xA459,0xA492,0xA4CE,0xA50C,0xA549,0xA585,0xA5BE,0xA5F2,0xA621,0xA647,0xA665,0xA678,0xA67E,0xA679,0xA669
	.half	0xA651,0xA631,0xA60A,0xA5DE,0xA5AD,0xA579,0xA543,0xA50C,0xA4D4,0xA49E,0xA46A,0xA439,0xA40D,0xA3E6,0xA3C6,0xA3AE
	.half	0xA39E,0xA399,0xA39E,0xA3AE,0xA3C6,0xA3E6,0xA40D,0xA439,0xA46A,0xA49E,0xA4D4,0xA50C,0xA543,0xA579,0xA5AD,0xA5DE
	.half	0xA60A,0xA631,0xA651,0xA669,0xA679,0xA67E,0xA679,0xA66B,0xA655,0xA638,0xA614,0xA5EB,0xA5BE,0xA58E,0xA55B,0xA526
	.half	0xA4F1,0xA4BD,0xA48A,0xA459,0xA42C,0xA403,0xA3E0,0xA3C2,0xA3AC,0xA39E,0xA399,0xF488,0xF488,0xF488,0xF487,0xF486
	.half	0xF485,0xF484,0xF483,0xF481,0xF480,0xF47F,0xF47D,0xF47C,0xF47B,0xF47A,0xF479,0xF478,0xF478,0xF478,0xF478,0xF478
	.half	0xF479,0xF479,0xF47A,0xF47B,0xF47C,0xF47D,0xF47F,0xF480,0xF481,0xF482,0xF484,0xF485,0xF486,0xF487,0xF487,0xF488
	.half	0xF488,0xF488,0xF488,0xF488,0xF487,0xF487,0xF486,0xF485,0xF484,0xF482,0xF481,0xF480,0xF47F,0xF47D,0xF47C,0xF47B
	.half	0xF47A,0xF479,0xF479,0xF478,0xF478,0xF478,0xF478,0xF478,0xF478,0xF479,0xF47A,0xF47B,0xF47C,0xF47D,0xF47E,0xF47F
	.half	0xF481,0xF482,0xF483,0xF484,0xF485,0xF486,0xF487,0xF487,0xF488,0xF488,0xF488,0xFEA0,0xFEA2,0xFEA7,0xFEAF,0xFEB9
	.half	0xFEC6,0xFED5,0xFEE4,0xFEF5,0xFF06,0xFF16,0xFF27,0xFF36,0xFF45,0xFF52,0xFF5C,0xFF64,0xFF69,0xFF6B,0xFF6A,0xFF66
	.half	0xFF5F,0xFF56,0xFF4B,0xFF3F,0xFF32,0xFF24,0xFF15,0xFF06,0xFEF6,0xFEE7,0xFED9,0xFECC,0xFEC0,0xFEB5,0xFEAC,0xFEA5
	.half	0xFEA1,0xFEA0,0xFEA1,0xFEA5,0xFEAC,0xFEB5,0xFEC0,0xFECC,0xFED9,0xFEE7,0xFEF6,0xFF06,0xFF15,0xFF24,0xFF32,0xFF3F
	.half	0xFF4B,0xFF56,0xFF5F,0xFF66,0xFF6A,0xFF6B,0xFF6A,0xFF66,0xFF60,0xFF58,0xFF4E,0xFF43,0xFF36,0xFF29,0xFF1B,0xFF0D
	.half	0xFEFE,0xFEF0,0xFEE2,0xFED5,0xFEC8,0xFEBD,0xFEB3,0xFEAB,0xFEA5,0xFEA1,0xFEA0,0xBC85,0xBC8B,0xBC9D,0xBCB9,0xBCDE
	.half	0xBD09,0xBD3B,0xBD71,0xBDAA,0xBDE4,0xBE1E,0xBE57,0xBE8D,0xBEBE,0xBEEA,0xBF0F,0xBF2A,0xBF3C,0xBF42,0xBF3D,0xBF2F
	.half	0xBF18,0xBEFA,0xBED5,0xBEAB,0xBE7D,0xBE4C,0xBE18,0xBDE4,0xBDAF,0xBD7C,0xBD4B,0xBD1D,0xBCF3,0xBCCE,0xBCB0,0xBC99
	.half	0xBC8A,0xBC85,0xBC8A,0xBC99,0xBCB0,0xBCCE,0xBCF3,0xBD1D,0xBD4B,0xBD7C,0xBDAF,0xBDE4,0xBE18,0xBE4C,0xBE7D,0xBEAB
	.half	0xBED5,0xBEFA,0xBF18,0xBF2F,0xBF3D,0xBF42,0xBF3E,0xBF31,0xBF1C,0xBF00,0xBEDE,0xBEB7,0xBE8D,0xBE5F,0xBE2E,0xBDFD
	.half	0xBDCB,0xBD99,0xBD69,0xBD3B,0xBD10,0xBCE9,0xBCC8,0xBCAC,0xBC97,0xBC8A,0xBC85,0x24AC,0x24BA,0x24E2,0x2520,0x2571
	.half	0x25D3,0x2642,0x26BA,0x2738,0x27BA,0x283C,0x28BB,0x2933,0x29A1,0x2A03,0x2A55,0x2A93,0x2ABB,0x2AC9,0x2ABD,0x2A9D
	.half	0x2A6A,0x2A26,0x29D4,0x2977,0x2910,0x28A2,0x282F,0x27BA,0x2745,0x26D3,0x2665,0x25FE,0x25A1,0x254F,0x250B,0x24D8
	.half	0x24B7,0x24AC,0x24B7,0x24D8,0x250B,0x254F,0x25A1,0x25FE,0x2665,0x26D3,0x2745,0x27BA,0x282F,0x28A2,0x2910,0x2977
	.half	0x29D4,0x2A26,0x2A6A,0x2A9D,0x2ABD,0x2AC9,0x2ABE,0x2AA1,0x2A72,0x2A34,0x29E9,0x2992,0x2933,0x28CC,0x2861,0x27F2
	.half	0x2782,0x2714,0x26A8,0x2642,0x25E2,0x258C,0x2541,0x2503,0x24D4,0x24B6,0x24AC,0xFD7B,0xFD79,0xFD75,0xFD6E,0xFD64
	.half	0xFD59,0xFD4C,0xFD3E,0xFD30,0xFD21,0xFD12,0xFD03,0xFCF5,0xFCE9,0xFCDD,0xFCD4,0xFCCD,0xFCC8,0xFCC7,0xFCC8,0xFCCC
	.half	0xFCD2,0xFCD9,0xFCE3,0xFCEE,0xFCFA,0xFD06,0xFD13,0xFD21,0xFD2E,0xFD3C,0xFD48,0xFD54,0xFD5F,0xFD68,0xFD70,0xFD76
	.half	0xFD7A,0xFD7B,0xFD7A,0xFD76,0xFD70,0xFD68,0xFD5F,0xFD54,0xFD48,0xFD3C,0xFD2E,0xFD21,0xFD13,0xFD06,0xFCFA,0xFCEE
	.half	0xFCE3,0xFCD9,0xFCD2,0xFCCC,0xFCC8,0xFCC7,0xFCC8,0xFCCB,0xFCD1,0xFCD8,0xFCE0,0xFCEA,0xFCF5,0xFD01,0xFD0E,0xFD1A
	.half	0xFD27,0xFD34,0xFD40,0xFD4C,0xFD57,0xFD61,0xFD6A,0xFD71,0xFD76,0xFD7A,0xFD7B,0xF255,0xF256,0xF256,0xF258,0xF25A
	.half	0xF25C,0xF25F,0xF262,0xF265,0xF268,0xF26B,0xF26E,0xF270,0xF273,0xF275,0xF277,0xF279,0xF27A,0xF27A,0xF27A,0xF279
	.half	0xF278,0xF276,0xF274,0xF272,0xF270,0xF26D,0xF26A,0xF268,0xF265,0xF262,0xF260,0xF25D,0xF25B,0xF259,0xF257,0xF256
	.half	0xF255,0xF255,0xF255,0xF256,0xF257,0xF259,0xF25B,0xF25D,0xF260,0xF262,0xF265,0xF268,0xF26A,0xF26D,0xF270,0xF272
	.half	0xF274,0xF276,0xF278,0xF279,0xF27A,0xF27A,0xF27A,0xF279,0xF278,0xF276,0xF275,0xF273,0xF270,0xF26E,0xF26B,0xF269
	.half	0xF266,0xF264,0xF261,0xF25F,0xF25C,0xF25A,0xF259,0xF257,0xF256,0xF255,0xF255,0xA45D,0xA462,0xA470,0xA486,0xA4A3
	.half	0xA4C5,0xA4EC,0xA516,0xA543,0xA571,0xA59E,0xA5CB,0xA5F5,0xA61C,0xA63E,0xA65B,0xA671,0xA67F,0xA684,0xA680,0xA675
	.half	0xA662,0xA64B,0xA62E,0xA60D,0xA5E9,0xA5C2,0xA59A,0xA571,0xA547,0xA51F,0xA4F8,0xA4D4,0xA4B3,0xA497,0xA47F,0xA46D
	.half	0xA461,0xA45D,0xA461,0xA46D,0xA47F,0xA497,0xA4B3,0xA4D4,0xA4F8,0xA51F,0xA547,0xA571,0xA59A,0xA5C2,0xA5E9,0xA60D
	.half	0xA62E,0xA64B,0xA662,0xA675,0xA680,0xA684,0xA680,0xA676,0xA665,0xA650,0xA635,0xA617,0xA5F5,0xA5D1,0xA5AB,0xA584
	.half	0xA55D,0xA536,0xA510,0xA4EC,0xA4CA,0xA4AC,0xA492,0xA47C,0xA46B,0xA461,0xA45D,0x85AF,0x7FF3,0x4016,0xBF5F,0x7AA7
	.half	0x8051,0x400F,0xBF5F,0x0000,0x0009,0x0026,0x0055,0x0093,0x00E1,0x013C,0x01A2,0x0214,0x028E,0x0311,0x039A,0x0427
	.half	0x04B9,0x054D,0x05E1,0x0675,0x0708,0x0797,0x0821,0x08A5,0x0921,0x0995,0x09FE,0x0A5C,0x0AAD,0x0AEF,0x0B22,0x0B43
	.half	0x0B51,0x0B4C,0x0B31,0x0B00,0x0AAA,0x0A26,0x0979,0x08A6,0x07B3,0x06A2,0x0578,0x0439,0x02E9,0x018E,0x002A,0xFEC2
	.half	0xFD5A,0xFBF6,0xFA9B,0xF94C,0xF80E,0xF6E5,0xF5D4,0xF4E2,0xF410,0xF364,0xF2E2,0xF28E,0xF26A,0xF273,0xF2A6,0xF2FE
	.half	0xF378,0xF40E,0xF4BD,0xF582,0xF657,0xF739,0xF823,0xF913,0xFA03,0xFAEF,0xFBD4,0xFCAD,0xFD77,0xFE2C,0xFECA,0xFF4C
	.half	0xFFAD,0xFFEB,0x0000,0xE8C7,0xE8CB,0xE8D6,0xE8E8,0xE900,0xE91E,0xE941,0xE969,0xE996,0xE9C6,0xE9FA,0xEA31,0xEA6A
	.half	0xEAA5,0xEAE2,0xEB20,0xEB5F,0xEB9D,0xEBDC,0xEC19,0xEC55,0xEC90,0xECC8,0xECFD,0xED2F,0xED5D,0xED88,0xEDAD,0xEDCE
	.half	0xEDE9,0xEDFD,0xEE0C,0xEE13,0xEE12,0xEE07,0xEDF3,0xEDD7,0xEDB4,0xED8B,0xED5C,0xED27,0xECEF,0xECB3,0xEC74,0xEC32
	.half	0xEBF0,0xEBAD,0xEB6A,0xEB28,0xEAE8,0xEAA9,0xEA6E,0xEA37,0xEA05,0xE9D7,0xE9B0,0xE98F,0xE974,0xE95B,0xE945,0xE931
	.half	0xE91F,0xE90F,0xE901,0xE8F5,0xE8EB,0xE8E3,0xE8DB,0xE8D6,0xE8D1,0xE8CD,0xE8CB,0xE8C9,0xE8C7,0xE8C7,0xE8C7,0xE8C7
	.half	0xE8C7,0xE8C7,0xE8C7,0x11E7,0x4C1D,0x4C0E,0x4BE4,0x4BA1,0x4B4A,0x4AE2,0x4A6C,0x49EC,0x4965,0x48DA,0x4850,0x47C9
	.half	0x4749,0x46D3,0x466B,0x4614,0x45D1,0x45A7,0x4598,0x45A4,0x45C7,0x45FD,0x4646,0x469D,0x4700,0x476E,0x47E3,0x485E
	.half	0x48DA,0x4957,0x49D1,0x4A47,0x4AB4,0x4B18,0x4B6F,0x4BB7,0x4BEE,0x4C11,0x4C1D,0x4C11,0x4BEE,0x4BB7,0x4B6F,0x4B18
	.half	0x4AB4,0x4A47,0x49D1,0x4957,0x48DA,0x485E,0x47E3,0x476E,0x4700,0x469D,0x4646,0x45FD,0x45C7,0x45A4,0x4598,0x45A3
	.half	0x45C3,0x45F4,0x4637,0x4687,0x46E3,0x4749,0x47B6,0x4829,0x489F,0x4916,0x498C,0x49FF,0x4A6C,0x4AD2,0x4B2E,0x4B7E
	.half	0x4BC0,0x4BF2,0x4C12,0x4C1D
