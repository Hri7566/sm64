/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 19 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage19


/* ================================================================================	
		: Move BGs sequence.
===================================================================================	*/
SEQ_Stage19_MoveBGs:

		seqActor(S_movebg01,  -5733,-3071,   0,   0,  0,0,   7, 51,0,   e_linelift		)
		seqActor(S_movebg02,  -1945,-3225,-715,   0,  0,0,   0,  0,0,   e_ext3_guragura	)
		seqActor(S_movebg02,  -2866,-3225,-715,   0,  0,0,   0,  0,0,   e_ext3_guragura	)
		seqActor(S_movebg03,  -1381, 3487,  96,	  0,  0,0,   0,  0,0,   e_ukishima		)
		seqActor(S_movebg03,   1126,-3065, 307,	  0,  0,0,   0,  0,0,   e_ukishima		)
		seqActor(S_movebg03,  -3225, 3487,  96,	  0,  0,0,   0,  0,0,   e_ukishima		)

		seqActor(S_movebg05,   6605,-3071,266,    0,0,0,   0,0,0,   e_nobori_ami)
		seqActor(S_movebg05,   1843, 3584, 96,    0,0,0,   0,1,0,   e_nobori_ami)
		seqActor(S_movebg05,    614, 3584, 96,    0,0,0,   0,1,0,   e_nobori_ami)
		seqActor(S_movebg05,   3072, 3584, 96,    0,0,0,   0,1,0,   e_nobori_ami)

		seqActor(S_movebg06,   2867,-1279, 307,	  0,  0,0,   2,159,0,	e_ridestartlift	)
		seqActor(S_movebg07,  -5836,  410, 300,   0,  0,0,   0,  0,0,   e_ext3_nobiru	)
		seqActor(S_movebg08,   4454,-2226, 266,	  0,  0,0,   0,  4,0,   e_seesaw		)
		seqActor(S_movebg08,   5786,-2380, 266,	  0,  0,0,   0,  4,0,   e_seesaw		)
		seqActor(S_movebg09,  -3890,  102, 617,	  0, 90,0,   1, 12,0, 	e_derublock		)
		seqActor(S_movebg09,  -3276,  102,   2,	  0,270,0,   1, 12,0,	e_derublock		)
		seqActor(S_movebg10,   2103,  198, 312,	  0,  0,0,   1,159,0,   e_derublock		)

		seqActor(S_movebg12,   4979, 4250,  96,	  0,  0,0,   0,  3,0,   e_downbridge	)

		seqActor(S_NULL,  	   3890,-2043, 266,   0,  0,0,   0, 82,0,   e_bar			)

		seqReturn


/* ================================================================================	
		: Move BGs sequence.
===================================================================================	*/
SEQ_Stage19_Enemies:

		seqActor(S_NULL,  -3226,3584,-822,   0,0,0,   0,0,0,   e_firebigbar)
		seqActor(S_NULL,  -1382,3584,-822,   0,0,0,   0,0,0,   e_firebigbar)
		seqActor(S_NULL,   1229, 307,-412,   0,0,0,   0,0,0,   e_firebigbar)

		seqReturn


/* ================================================================================	
		: Stars sequence.
===================================================================================	*/
SEQ_Stage19_Stars:

		seqActor(S_NULL,   1200, 5700, 160,   0,0,0,   0,0,0,   e_extstar_getcoins)

		seqReturn


/* ================================================================================	
		: Stage 19 main sequence.
===================================================================================	*/
SEQ_DoStage19:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage19SegmentRomStart	  , _GfxStage19SegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE   , _EMtTextureSegmentRomStart	  , _EMtTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_BACK	  , _BackMotosSegmentRomStart	  , _BackMotosSegmentRomEnd		)
	seqLoadPres(SEGMENT_WEATHER	  , _WeatherSegmentRomStart		  , _WeatherSegmentRomEnd		)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_bSegmentRomStart	  , _GfxEnemy1_bSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_bSegmentRomStart	  , _HmsEnemy1_bSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_hSegmentRomStart	  , _GfxEnemy2_hSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_hSegmentRomStart	  , _HmsEnemy2_hSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetEnemy1_b)
		seqCall(SetEnemy2_h)
		seqCall(SetBasicEnemy)

		seqHmsShape(S_bg01, RCP_HmsExt2Bg01)
		seqHmsShape(S_bg02, RCP_HmsExt2Bg02)
		seqHmsShape(S_bg03, RCP_HmsExt2Bg03)
		seqHmsShape(S_bg04, RCP_HmsExt2Bg04)
		seqHmsShape(S_bg05, RCP_HmsExt2Bg05)
		seqHmsShape(S_bg06, RCP_HmsExt2Bg06)
		seqHmsShape(S_bg07, RCP_HmsExt2Bg07)
		seqHmsShape(S_bg08, RCP_HmsExt2Bg08)
		seqHmsShape(S_bg09, RCP_HmsExt2Bg09)
		seqHmsShape(S_bg10, RCP_HmsExt2Bg10)
		seqHmsShape(S_bg11, RCP_HmsExt2Bg11)
		seqHmsShape(S_bg12, RCP_HmsExt2Bg12)
		seqHmsShape(S_bg13, RCP_HmsExt2Bg13)
		seqHmsShape(S_bg14, RCP_HmsExt2Bg14)
		seqHmsShape(S_bg15, RCP_HmsExt2Bg15)
		seqHmsShape(S_bg16, RCP_HmsExt2Bg16)
		seqHmsShape(S_bg17, RCP_HmsExt2Bg17)
		seqHmsShape(S_bg18, RCP_HmsExt2Bg18)
		seqHmsShape(S_bg19, RCP_HmsExt2Bg19)

		seqHmsShape(S_movebg01, RCP_HmsExt2Susumu	)
		seqHmsShape(S_movebg02, RCP_HmsExt2Gura		)
		seqHmsShape(S_movebg03, RCP_HmsExt2Ukishima	)
		seqHmsShape(S_movebg04, RCP_HmsExt2Bou		)
		seqHmsShape(S_movebg05, RCP_HmsExt2Amiyuka	)
		seqHmsShape(S_movebg06, RCP_HmsExt2Agaru	)
		seqHmsShape(S_movebg07, RCP_HmsExt2Nobiru	)
		seqHmsShape(S_movebg08, RCP_HmsExt2Yokoshiso)
		seqHmsShape(S_movebg09, RCP_HmsExt2Rifuto01	)
		seqHmsShape(S_movebg10, RCP_HmsExt2Rifuto02	)
		seqHmsShape(S_movebg11, RCP_HmsExt2Kuzure01	)
		seqHmsShape(S_movebg12, RCP_HmsExt2Kuzure02	)

		seqBeginScene(1, RCP_Stage19Scene1)
			seqActor(S_NULL,  -7577,-2764+START_H,  0,  0,90,0,   0,10,0,	e_player_landing)
			seqActor(S_NULL,   6735,	     3681, 99,  0, 0,0,  20,11,0,	e_tripchimney2	)
			seqActor(S_NULL,   5886,	     5000, 99,  0,90,0,   0,12,0,	e_player_downing)
			seqPort(10, 19, 1, 10)				/*	Mario stage in.				*/
			seqPort(11, 33, 1, 10)				/*	To St33 [ Kuppa2 ].			*/
			seqPort(12, 19, 1, 12)				/*	St33 [ Kuppa2 ]  missed.	*/
			seqGameOver(6, 3, 104)				/*	Ext2 missed.				*/
			seqCall(SEQ_Stage19_MoveBGs)
			seqCall(SEQ_Stage19_Enemies)
			seqCall(SEQ_Stage19_Stars)
			seqMapInfo(cx1901_info)
			seqTagInfo(cx1901_info_tag)
			seqSetMusic(NA_STG_GROUND, NA_EXTRA_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1,  90,  -7577,-2764,0)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
