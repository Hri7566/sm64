extern short cx2701_info_tag[]={
	TAGCODE_e_coin_ground+(  0<<9),-3233, 4688,-5600,    0,
	TAGCODE_e_coin_ground+(  0<<9),-5636, 4331,-5054,    0,
	TAGCODE_e_coin_ground+(  0<<9),-5980, 3453,  945,    0,
	TAGCODE_e_coin_ground+(  0<<9),-2870, 2837, 2300,    0,
	TAGCODE_e_coin_ground+(  0<<9), 3930,  274, 2425,    0,
	TAGCODE_e_coin_ground+(  0<<9), 5174,   61, 3200,    0,
	TAGCODE_e_coin_ground+(  0<<9), 5707, -128, 4565,    0,
	TAGCODE_e_coin_ground+(  0<<9), 5194, -310, 5845,    0,
	TAGCODE_e_coin_ground+(  0<<9), 3640, -543, 6450,    0,
	TAGCODE_e_coin_ground+(  0<<9), 2550, -700, 6050,    0,
	TAGCODE_e_coin_ground+(  0<<9), 1821, -921, 4616,    0,
	TAGCODE_e_coin_ground+(  0<<9),  -20,-2542,-6304,    0,
	TAGCODE_e_coin_ground+(  0<<9),-1197,-2929,-4692,    0,
	TAGCODE_e_coin_ground+(  0<<9),-2565,-3268,-3525,    0,
	TAGCODE_e_coin_ground+(  0<<9),-4909,-3633,-4218,    0,
	TAGCODE_e_coin_ground+(  0<<9),-6290,-3937,-2545,    0,
	TAGCODE_e_t_kanban+( 32<<9), 3580, 6140,-5180,  149,
	TAGCODE_e_manycoin1+( 32<<9), 3000, 6180,-5640,    0,
	TAGCODE_e_switchcoin+(  0<<9), 5423, 6140,-6540,    0,
	TAGCODE_e_bluecoin+(  0<<9),  780, 5613,-5600,    0,
	TAGCODE_e_bluecoin+(  0<<9), 2360, 5979,-5600,    0,
	TAGCODE_e_manycoin1+(  0<<9), 1880,-1440,   60,    0,
	TAGCODE_e_manycoin1+(  0<<9), 1860,-1760,-1720,    0,
	TAGCODE_e_manycoin1+(  0<<9), 1860,-1200, 1680,    0,
	TAGCODE_e_dummy_kinoko+(  0<<9), 1860,-1220, 1700,    0,
	TAGCODE_e_dummy_kinoko+(  0<<9), 1880,-1460,   80,    0,
	TAGCODE_e_dummy_kinoko+(  0<<9), 1860,-1820,-1680,    0,
	TAGCODE_e_dummy_kinoko+(  0<<9),-6380,-4550, 6320,    0,
	TAGCODE_e_1up_sec+(  0<<9),-6380,-4500, 5980,    4,
	TAGCODE_e_1up_slider+( 64<<9), 1847, -961, 3863,    0,
	TAGCODE_e_manycoin1+( 32<<9),  260, 2580, 2280,    0,
	TAGCODE_e_manycoin1+( 32<<9), -880, 2880, 2280,    0,
	TAGCODE_e_bluecoin+(  0<<9), -809, 5245,-5600,    0,
	TAGCODE_e_bluecoin+(  0<<9),-2409, 4877,-5600,    0,
	TAGCODE_e_coin_ground+(  0<<9),-6400,-4146, -590,    0,
	TAGCODE_e_coin_ground+(  0<<9),-6400,-4335, 1409,    0,
	TAGCODE_e_coin_ground+(  0<<9),-6400,-4530, 3481,    0,
	TAGCODE_e_bluecoin+(  0<<9),-4445, 4490,-5536,    0,
	TAGCODE_e_bluecoin+(  0<<9),-6263, 4150,-4009,    0,
	TAGCODE_e_coin_ground+(  0<<9), 1854,-2132,-4290,    0,
	TAGCODE_e_block_star+(  0<<9),-6385,-4200, 5770,    0,
	TAGCODE_END
};
