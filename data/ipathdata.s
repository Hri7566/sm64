/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: ipathdata.s
					Description	: enemy strategy pathdata for iwamoto
					Date		: Feburary 14 1996
					Author		: Iwamoto Daiki

 ********************************************************************************/
#define ASSEMBLER

#include "../headers.h"
#include "../include/playerstat.h"

/********************************************************************************/
/*		Start AsmCode															*/
/********************************************************************************/

	.data
	.align	2
	.align	0

/********************************************************************************/
/*		Path Strategys															*/
/********************************************************************************/

	.globl		e_kombu	
	.globl		e_kombu_one	
	.globl		e_slider_coin	
	.globl		e_move_coin	
	.globl		e_escape_coin	
	.globl		e_escape_coin2	
	.globl		e_newbom	
	.globl		e_bom_pusu
	.globl		e_messbom	
	.globl		e_futa_bom	
	.globl		e_uzumaki	
	.globl		e_fukidasi	
	.globl		e_tatefuda	
	.globl		e_kanban	
	.globl		e_biribiri	
	.globl		e_butterfly
	.globl		e_bird
	.globl		e_bird_egg
	.globl		e_egg_smoke
	.globl      e_rabbit
	.globl		e_new_kame
	.globl		e_kame2
	.globl		emode_kame_catch
	.globl		emode_kame_drop
	.globl		emode_kame_throw
	.globl		emode_kame_next
	.globl		e_item_buble
	.globl		e_item_ripple
	.globl		e_bombfire
	.globl		e_bom_smoke
	.globl		e_fire_bubble
	.globl		e_otos
	.globl		e_big_otos
	.globl		e_big_otos2
	.globl		e_ice_otos
	.globl		e_big_ice_otos
	.globl		e_ring
	.globl		e_ring_parts
	.globl		e_ring_dummy
	.globl		e_kirai
	.globl		e_kopakirai_fire
	.globl		e_kopakirai_smoke
	.globl		e_V_star
	.globl		e_V_starspark
	.globl		e_V_stardust
	.globl		e_motos_bridge1
	.globl		e_motos_bridge_parts
	.globl		e_dossun_bar
	.globl		e_dossun_bar2
	.globl		e_trans_bar
	.globl		e_kuramochi
	.globl		e_kura_real
	.globl		e_tripstar
	.globl		e_rail_ironball	
	.globl		e_gush_ironball	
	.globl		e_gush_ironball_few	
	.globl		e_gush_ironball2
	.globl		e_ironball	
	.globl		e_ironball_normal	
	.globl		e_falls	
	.globl		e_shipoar	
	.globl		e_py_pencil	
	.globl		e_py_wall	
	.globl		e_castle_flag	
	.globl		e_snowrock_rail	
	.globl		e_snowrock_dummy	
	.globl		e_snowball_head	
	.globl		e_snow_body
	.globl		e_itemhat_wing	
	.globl		e_itemhat_metal	
	.globl		e_itemhat_blow	
	.globl		e_itemhat_erase	
	.globl		e_bom_cannon	
	.globl		e_tripstar_getcoins	
	.globl		e_coin_appstar	
	.globl		e_don_maruta	
	.globl		e_fm_battan	
	.globl		e_fm_maruta	
	.globl		e_1up_kinoko	
	.globl		e_1up_kinoko_escape
	.globl		e_1up_kinoko_slider
	.globl		e_1up_kinoko_stop
	.globl		e_1up_kinoko_set
	.globl		e_1up_kinoko_secret
	.globl		e_dummy_kinoko
	.globl		e_1up_secret_tate
	.globl		e_1up_secret_chase
	.globl		e_dummy_kinoko_chase
	.globl		e_NEWSlift	
	.globl		e_NEWSlift_button
	.globl		e_takeblock
	.globl		e_gurubiri
	.globl		e_py_gondola
	.globl		e_py_gondola_dot
	.globl		e_sb_pytop
	.globl		e_sb_py_hahen
	.globl		e_pytop_dummy
	.globl		e_dunjon_rock
	.globl		e_dunjon_rock_st
	.globl		e_birdsing
	.globl		e_soyokaze
	.globl		e_iwanami
	.globl		e_iwanami_parts
	.globl		e_tripstar_getdummy
	.globl		e_extstar_getcoins
	.globl		e_enemy_star
	.globl		e_dummy_appstar
	.globl		e_po_floatingbord
	.globl		e_12_floatingbord
	.globl		e_po_floatingbord2
	.globl		e_wd_kibako
	.globl		e_po_moveblock
	.globl		e_numbers
	.globl		e_d_manta
	.globl		e_manta_ring
	.globl		e_wd_pillar
	.globl		e_wd_pillarbase
	.globl		e_pillar_hit
	.globl		e_magumafalls
	.globl		e_iwa_smoke
	.globl		e_clock_furiko
	.globl		e_tbox_quize
	.globl		e_tbox_quize_ground
	.globl		e_tbox_quize23
	.globl		e_trebox
	.globl		e_trebox_futa
	.globl		e_sandfall
	.globl		e_reset
	.globl		e_main_net2
	.globl		e_yoshi

	.globl		e_item_test

/********************************************************************************/
/*		Path Strategys															*/
/********************************************************************************/

/*==============================================================================*/
/*		Select Stage Path Data													*/
/*==============================================================================*/
#include "iwa_path/selectstage.s"

/*==============================================================================*/
/*		Others Path Data													*/
/*==============================================================================*/
#include "ipathfile.s"

