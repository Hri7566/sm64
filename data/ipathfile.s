/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					Iwamoto Obj Strategy Program

					Date		: junuary 26 1996
					Author		: Iwamoto Daiki

 ********************************************************************************/

#include "iwa_path/iwacoin.s"
#include "iwa_path/kombu.s"
#include "iwa_path/newbom.s"
#include "iwa_path/uzumaki.s"
#include "iwa_path/tatefuda.s"
#include "iwa_path/biribiri.s"
#include "iwa_path/butterfly.s"
#include "iwa_path/bird.s"
#include "iwa_path/kame.s"
#include "iwa_path/effect.s"
#include "iwa_path/otos.s"
#include "iwa_path/ring.s"
#include "iwa_path/kirai.s"
#include "iwa_path/V_star.s"
#include "iwa_path/movebg.s"
#include "iwa_path/kuramochi.s"
#include "iwa_path/ironball.s"
#include "iwa_path/movebg_31.s"
#include "iwa_path/movebg_8.s"
#include "iwa_path/others.s"
#include "iwa_path/rock.s"
#include "iwa_path/itemhat.s"
#include "iwa_path/tripstar.s"
#include "iwa_path/movebg_36.s"
#include "iwa_path/movebg_22.s"
#include "iwa_path/oneupkinoko.s"
#include "iwa_path/NEWSlift.s"
#include "iwa_path/block.s"
#include "iwa_path/movebg_10.s"
#include "iwa_path/movebg_11.s"
#include "iwa_path/numbers.s"
#include "iwa_path/manta.s"
#include "iwa_path/movebg_12.s"
#include "iwa_path/movebg_6.s"
#include "iwa_path/treasurebox.s"
#include "iwa_path/rabbit.s"
#include "iwa_path/yoshi.s"



