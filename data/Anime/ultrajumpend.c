/********************************************************************************
						Ultra 64 MARIO Brothers

				  mario ultra jump end animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							April 26, 1996
 ********************************************************************************/

#include "../headers.h"


static short mario_ultra_jump_end_prm[]={
	    0,    0,    0,    0,    0,    0,    0,    0,
	    0,    0,    0,    0,    0,    0,    0,    0,
	    0,    0,    0,    0,    0,    0,   -3,  -10,
	  -16,  -21,  -20,  -14,   -8,   -2,    0,    0,
	    0,  122,   95,   90,  103,  116,  130,  144,
	  157,  167,  176,  183,  186,  187,  187,  187,
	  187,  186,  186,  186,  187,  187,  188,  189,
	  184,  175,  173,  171,  171,  170,  170,  170,
	  170,  -24,  -26,  -22,  -23,  -23,  -22,  -20,
	  -12,   -3,    5,   11,   23,   23,   23,   23,
	   23,   23,   23,   23,   22,   18,   13,   10,
	   13,   20,   17,    9,    5,    1,    0,    0,
	    0,16383,  171,  179,  184,  185,  179,  169,
	  156,  141,  126,  112,  100,   92,   88,   88,
	   90,   92,   94,   95,   92,   86,   79,   71,
	   62,   53,   43,   33,   24,   16,    9,    4,
	    1,    0,-1659,-1658,-1658,-1658,-1658,-1659,
	-1660,-1661,-1661,-1662,-1663,-1663,-1663,-1663,
	-1663,-1663,-1663,-1663,-1663,-1664,-1664,-1664,
	-1665,-1665,-1666,-1666,-1667,-1667,-1668,-1668,
	-1668,-1668,-5801,-5852,-5886,-5889,-5855,-5792,
	-5709,-5616,-5520,-5431,-5356,-5305,-5283,-5281,
	-5292,-5308,-5321,-5323,-5305,-5271,-5226,-5175,
	-5118,-5058,-4997,-4938,-4881,-4831,-4787,-4754,
	-4732,-4724,-8755,-9615,-9850,-9457,-8641,-7507,
	-6163,-4714,-3267,-1928, -803,    0,    0,    0,
	    0,    0,    0,    0,    0,-1267,-3149,-5357,
	-7604,-9599,-11056,-12006,-12702,-13183,-13488,-13657,
	-13729,-13745,-7278,-10654,-13072,-13815,-14402,-14860,
	-15210,-15478,-15688,-15864,-16029,-16207,-16442,-16713,
	-16952,-17089,-17057,-16786,-16207,-15312,-14173,-12845,
	-11383,-9842,-8276,-6742,-5293,-3984,-2871,-2008,
	-1449,-1251,-14692,-11626,-9825,-9676,-9782,-10088,
	-10534,-11063,-11617,-12138,-12567,-12848,-12993,-13065,
	-13078,-13049,-12990,-12918,-12848,-12770,-12672,-12557,
	-12431,-12298,-12163,-12030,-11905,-11792,-11696,-11621,
	-11573,-11556,-5934,-2194,  457, 1382, 2136, 2745,
	 3233, 3625, 3946, 4219, 4471, 4725, 5016, 5319,
	 5569, 5700, 5647, 5343, 4725, 3778, 2574, 1170,
	 -375,-2004,-3658,-5280,-6812,-8196,-9372,-10285,
	-10875,-11084,-5633,-5676,-5663,-5588,-5458,-5287,
	-5090,-4881,-4673,-4481,-4320,-4203,-4146,-4141,
	-4169,-4209,-4241,-4246,-4203,-4115,-4004,-3875,
	-3732,-3581,-3429,-3279,-3137,-3010,-2901,-2817,
	-2762,-2743,-10730,-11991,-12392,-11939,-10935,-9515,
	-7819,-5983,-4145,-2444,-1016,    0,    0,    0,
	    0,    0,    0,    0,    0,-1587,-3966,-6746,
	-9539,-11956,-13608,-14488,-14923,-15025,-14908,-14685,
	-14468,-14371,21230,18109,16317,16137,16225,16520,
	16963,17493,18050,18576,19008,19288,19517,19791,
	20037,20181,20153,19879,19288,17251,14008,11539,
	10101, 8658, 7247, 5905, 4668, 3574, 2659, 1959,
	 1512, 1355,13842,10650, 8815, 8671, 8804, 9150,
	 9647,10232,10843,11416,11890,12201,12356,12419,
	12414,12366,12298,12235,12201,12184,12149,12088,
	12000,11898,11786,11671,11558,11452,11360,11287,
	11239,11222, 6676, 3610, 1904, 1869, 2118, 2585,
	 3202, 3905, 4626, 5300, 5859, 6239, 6537, 6848,
	 7106, 7242, 7189, 6877, 6239, 4079,  639,-1993,
	-3544,-5106,-6637,-8097,-9445,-10639,-11640,-12405,
	-12894,-13066,  106,  128,   88,  -66, -260, -461,
	 -638, -759, -691, -486, -200,   16,   16,   16,
	   16,   16,   16,   16,   16, -351, -800,-1012,
	-1067, -933, -714, -516, -350, -182,  -52,    0,
	    0,    0, -189, -189, -183, -170, -152, -135,
	 -120, -113, -132, -164, -188, -203, -194, -166,
	 -133, -107, -102, -129, -203, -568,-1070,-1522,
	-1788,-1660,-1375,-1093, -828, -538, -304, -208,
	 -208, -208,-18121,-18285,-18378,-18206,-17982,-17719,
	-17428,-17123,-16753,-16212,-15248,-14518,-14518,-14518,
	-14518,-14518,-14518,-14518,-14518,-14766,-15213,-15960,
	-16522,-16453,-16141,-15748,-15240,-14616,-14087,-13864,
	-13864,-13864,18507,21286,22275,21204,19470,17289,
	14875,12444,10210, 7750, 5220, 3709, 3709, 3709,
	 3709, 3709, 3709, 3709, 3709, 3850, 4583, 6407,
	 8609,10148, 9829, 8880, 8480, 8118, 7855, 7754,
	 7754, 7754,  757,   94, -115,  210,  652, 1176,
	 1752, 2345, 2923, 3453, 3902, 4239, 4239, 4239,
	 4239, 4239, 4239, 4239, 4239, 4598, 5008, 5029,
	 5042, 5277, 5583, 5881, 6195, 6535, 6809, 6921,
	 6921, 6921, 1826, 1530, 1289, 1169, 1052,  941,
	  835,  737,  647,  566,  496,  438,  438,  438,
	  438,  438,  438,  438,  438,  340,  355,  760,
	 1343, 1987, 2667, 3222, 3656, 4041, 4315, 4420,
	 4420, 4420,-29364,-32024,32645,-31880,-30431,-28662,
	-26693,-24646,-22640,-20797,-19237,-18081,-18081,-18081,
	-18081,-18081,-18081,-18081,-18081,-18159,-18461,-19283,
	-20148,-20675,-21083,-21347,-21464,-21483,-21457,-21436,
	-21436,-21436,   -3,   -6,    0,   24,   53,   84,
	  114,  141,  171,  170,   80,    0,    0,    0,
	    0,    0,    0,    0,    0, -100, -263, -490,
	 -785,-1390,-1978,-1670,-1186, -637, -186,    0,
	    0,    0,    0,   -1,    0,    4,   10,   16,
	   23,   30,   41,   45,   22,    0,    0,    0,
	    0,    0,    0,    0,    0, -107, -281, -439,
	 -498, -233,  118,   83,   56,   29,    8,    0,
	    0,    0,-19159,-19377,-19478,-19187,-18807,-18377,
	-17935,-17519,-17181,-16749,-15873,-15202,-15202,-15202,
	-15202,-15202,-15202,-15202,-15202,-14981,-14620,-14317,
	-14269,-15043,-16075,-16254,-16360,-16465,-16544,-16576,
	-16576,-16576,18887,21808,22955,21841,20068,17851,
	15406,12948,10689, 8217, 5688, 4181, 4181, 4181,
	 4181, 4181, 4181, 4181, 4181, 4319, 4536, 4615,
	 4978, 6325, 7612, 8227, 8754, 9165, 9432, 9527,
	 9527, 9527, -148,  233,  316,   89, -214, -573,
	 -966,-1370,-1763,-2123,-2429,-2659,-2659,-2659,
	-2659,-2659,-2659,-2659,-2659,-3067,-3727,-4519,
	-5322,-6295,-7267,-8111,-9019,-9844,-10445,-10678,
	-10678,-10678,-1366,-1406,-1367,-1253,-1114, -960,
	 -796, -632, -473, -329, -205, -111, -111, -111,
	 -111, -111, -111, -111, -111, -654,-1535,-2508,
	-3330,-3817,-4095,-4353,-4610,-4836,-4995,-5055,
	-5055,-5055,-28811,-31607,-32567,-31581,-30147,-28387,
	-26422,-24376,-22369,-20524,-18964,-17809,-17809,-17809,
	-17809,-17809,-17809,-17809,-17809,-18023,-18371,-18708,
	-18885,-18502,-18106,-18218,-18451,-18723,-18948,-19042,
	-19042,-19042,-32767,-32767,16753,-16545,-32767,-32767,
	16753,-16545,    0,    0,    0,    0,    0,    0,
	    0,    0,    0,    0,    0,    0,   -7,  -25,
	  -45,  -61,  -63,  -45,    0,  103,  265,  451,
	  628,  760,  813,  731,  529,  288,   85,    0,
	    0,    0,  -37,  -18,    0,    0,    0,    0,
	    0,    0,    0,    0,    0,    0,    1,    6,
	   11,   15,   16,   11,    0,  -26,  -67, -115,
	 -160, -195, -211, -192, -140,  -77,  -22,    0,
	    0,    0, 1367, 1471, 1575, 1575, 1575, 1575,
	 1575, 1575, 1575, 1575, 1575, 1575, 1588, 1619,
	 1654, 1680, 1685, 1654, 1575, 1425, 1205,  943,
	  662,  390,  147, -120, -406, -666, -854, -926,
	 -926, -926,   43,   23,   11,   14,   19,   25,
	   31,   39,   46,   52,   58,   62,   65,   66,
	   66,   66,   65,   63,   62,   59,   54,   48,
	   42,   36,   29,   23,   16,   10,    4,    0,
	    0,    0,  -23,  -46,  -62,  -56,  -49,  -40,
	  -30,  -21,  -11,   -3,    4,    9,    3,  -15,
	  -39,  -58,  -62,  -43,    9,  243,  590,  737,
	  545,  189, -175, -392, -395, -275, -115,    0,
	   16,    0, 7826,10640,11576,10467, 8893, 6983,
	 4864, 2665,  513,-1463,-3138,-4381,-5122,-5446,
	-5453,-5248,-4932,-4609,-4381,-4065,-3522,-2914,
	-2293,-1623, -941, -281,  384, 1062, 1684, 2180,
	 2282, 2180,    0,    0,    0,    0,    0,    0,
	    0,    0,    0,    0,    0,    0,    0,    0,
	    0,    0,    0,    0,    0,   42,  112,  127,
	    0, -734,-1355,-1280,-1087, -822, -533, -268,
	  -75,    0,    0,    0,    0,    0,    0,    0,
	    0,    0,    0,    0,    0,    0,   15,   50,
	   90,  121,  126,   90,    0, -287, -755,-1185,
	-1358, -637,  167,  277,  259,  169,   63,    0,
	   -5,    0,16668,16786,16847,16861,16836,16783,
	16712,16631,16550,16480,16429,16407,16404,16402,
	16401,16402,16403,16405,16407,16411,16418,16425,
	16433,16440,16447,16454,16461,16469,16476,16482,
	16486,16487
};
static short mario_ultra_jump_end_tbl[]={
	   32,    1,	/* chn14_3_1_2_2translate x */
	   32,   33,	/* chn14_3_1_2_2 translate y */
	   32,   65,	/* chn14_3_1_2_2 translate z */
	    1,    0,	/* chn14_3_1_2_2 rotate x */
	    1,   97,	/* chn14_3_1_2_2 rotate y */
	    1,    0,	/* chn14_3_1_2_2 rotate z */
	   32, 1130,	/* <m_waist>1_3 rotate x */
	   32, 1162,	/* <m_waist>1_3 rotate y */
	   32, 1194,	/* <m_waist>1_3 rotate z */
	   32, 1034,	/* <m_body>1 rotate x */
	   32, 1066,	/* <m_body>1 rotate y */
	   32, 1098,	/* <m_body>1 rotate z */
	   32,  938,	/* <m_head>2 rotate x */
	   32,  970,	/* <m_head>2 rotate y */
	   32, 1002,	/* <m_head>2 rotate z */

	    1,  934,	/* chn6 rotate x */
	    1,  935,	/* chn6 rotate y */
	    1,  936,	/* chn6 rotate z */
	   32,  226,	/* <m_larmA>1 rotate x */
	   32,  258,	/* <m_larmA>1 rotate y */
	   32,  290,	/* <m_larmA>1 rotate z */
	    1,    0,	/* <m_larmB>1 rotate x */
	    1,    0,	/* <m_larmB>1 rotate y */
	   32,  194,	/* <m_larmB>1 rotate z */
	   32,   98,	/* <m_lhand>1 rotate x */
	   32,  130,	/* <m_lhand>1 rotate y */
	   32,  162,	/* <m_lhand>1 rotate z */

	    1,  930,	/* chn10 rotate x */
	    1,  931,	/* chn10 rotate y */
	    1,  932,	/* chn10 rotate z */
	   32,  386,	/* <m_rarmA>1 rotate x */
	   32,  418,	/* <m_rarmA>1 rotate y */
	   32,  450,	/* <m_rarmA>1 rotate z */
	    1,    0,	/* <m_rarmB>1 rotate x */
	    1,    0,	/* <m_rarmB>1 rotate y */
	   32,  354,	/* <m_rarmB>1 rotate z */
	    1,    0,	/* <m_rhand>1 rotate x */
	    1,    0,	/* <m_rhand>1 rotate y */
	   32,  322,	/* <m_rhand>1 rotate z */

	    1,    0,	/* chn15 rotate x */
	    1,    0,	/* chn15 rotate y */
	    1,  933,	/* chn15 rotate z */
	   32,  834,	/* <m_llegA>1 rotate x */
	   32,  866,	/* <m_llegA>1 rotate y */
	   32,  898,	/* <m_llegA>1 rotate z */
	    1,    0,	/* <m_llegB>1 rotate x */
	    1,    0,	/* <m_llegB>1 rotate y */
	   32,  802,	/* <m_llegB>1 rotate z */
	   32,  706,	/* <m_ltoot>1 rotate x */
	   32,  738,	/* <m_ltoot>1 rotate y */
	   32,  770,	/* <m_ltoot>1 rotate z */

	    1,    0,	/* chn17 rotate x */
	    1,    0,	/* chn17 rotate y */
	    1,  937,	/* chn17 rotate z */
	   32,  610,	/* <m_rlegA>1 rotate x */
	   32,  642,	/* <m_rlegA>1 rotate y */
	   32,  674,	/* <m_rlegA>1 rotate z */
	    1,    0,	/* <m_rlegB>1 rotate x */
	    1,    0,	/* <m_rlegB>1 rotate y */
	   32,  578,	/* <m_rlegB>1 rotate z */
	   32,  482,	/* <m_rfoot>1 rotate x */
	   32,  514,	/* <m_rfoot>1 rotate y */
	   32,  546,	/* <m_rfoot>1 rotate z */
};
AnimeRecord animMarioUltraJumpEnd = {
	 MAP_ANIM_ONETIME,			/* attribute       	 */
	 0,							/* syncro			 */
	 0,							/* start frame     	 */
	 0,							/* loop frame		 */
	32,							/* number of frames	 */
	20,							/* number of joints	 */
	mario_ultra_jump_end_prm,
	mario_ultra_jump_end_tbl
};
