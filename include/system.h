/********************************************************************************
	system.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	January 17, 1996
 ********************************************************************************/

#ifndef SYSTEM_H
#define	SYSTEM_H

#include "scheduler.h"

#define	malloc(size)		AllocHeap(systemHeap,size)
#define	free(addr)			FreeHeap(systemHeap,addr)

/*-------------------------------------------------------------------------------
 *	Length of message buffer.
 */
#define	NUM_DMA_MESSAGES		1
#define	NUM_PAD_MESSAGES		1
#define	NUM_PIM_MESSAGES		32
#define	NUM_SCD_MESSAGES		16
#define	NUM_TSK_MESSAGES		16


/*-------------------------------------------------------------------------------
 *	Priority of system threads.
 */
#define	PRIORITY_SYSTEM			100
#define	PRIORITY_AUDIO			 20
#define	PRIORITY_GRAPHICS		 10


/********************************************************************************
 *
 *	External buffers
 *
 ********************************************************************************/

extern uwlong	bootThreadStack[BOOT_STACKSIZE64];		/* boot thread stack memory		*/
extern uwlong  idleThreadStack[IDLE_STACKSIZE64];		/* idel thread stack memory		*/
extern uwlong  mainThreadStack[MAIN_STACKSIZE64];		/* main thread stack memory		*/
extern uwlong audioThreadStack[MAIN_STACKSIZE64];		/* audio thread stack memory	*/
extern uwlong graphThreadStack[MAIN_STACKSIZE64];		/* graphic thread stack memory	*/
extern uwlong motorThreadStack[MAIN_STACKSIZE64];		/* motor thread stack memory	*/

extern uwlong datYieldBuffer[DAT_YIELD_SIZE64];
// extern uwlong audYieldBuffer[AUD_YIELD_SIZE64];

extern uwlong freeZoneStart;



/********************************************************************************
 *
 *	External works
 *
 ********************************************************************************/

extern char  sysDebugFlag;				/* debug flag							*/
extern char  sysGvdActive;				/* gvd active flag						*/
extern char  sysProcessMeter;			/* process meter active flag			*/
extern char  sysDebugMessage;			/* debug message active flag			*/

extern char  sysHardwareReset;			/* hardware reset flag					*/
extern char  sysWipeCounter;			/* hardware reset wipe counter			*/

extern ulong videoFrame;				/* videl frame counter					*/

/*--------------------------------------------------------------------------------
 *	Thread message works
 */
extern OSIoMesg		dmaIOMessageBuf;				/* DMA I/O message buffer	*/
extern OSMesg		dummyMessage;					/* dummy message data		*/

extern OSMesgQueue	dmaMessageQ;					/* for DMA					*/
extern OSMesgQueue	padMessageQ;					/* for controller pad		*/


/********************************************************************************/
/*																				*/
/*	System function prototypes													*/
/*																				*/
/********************************************************************************/

extern void
SysChangeDebugMode(void);

extern void
BootProcess(void);

/********************************************************************************/
/*																				*/
/*	Time manager function prototypes											*/
/*																				*/
/********************************************************************************/

extern void
SetGraphCPUTime(int num);

extern void
SetAudioCPUTime(void);

extern void
SetGraphRCPTime(int num);

extern void
SetAudioRCPTime(void);

extern void
DisplayProcTime(void);

#endif
