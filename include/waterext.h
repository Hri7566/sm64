/********************************************************************************
						Ultra 64 MARIO Brothers

			Water surface and Water fall extern define header file.

			Copyright 1995 Nintendo co., ltd.  All rights reserved

				This module was programmed by Y.Tanimoto.

								1996.4.10
*********************************************************************************/


#define		FALL_NOLIGHT	0
#define		FALL_LIGHT		1


/* ========================================================================================
		: Data structure of the fall.
===========================================================================================	*/
typedef	struct	{
	ulong	index	  ;
	int		texNo	  ;
	int		n_point_s ;
	short*	pt_data	  ;
	Gfx*	init_gfx  ;
	Gfx*	reset_gfx ;
	Gfx*	draw_gfx  ;
	uchar	red		  ;
	uchar	green	  ;
	uchar	blue	  ;
	uchar	alpha	  ;
	int		r_mode	  ;
} FallData ;

/* ========================================================================================
		: Ptr to the image data.
=========================================================================================== */
extern ushort	water_rotate_txt[];
extern ushort	 smog_rotate_txt[];
extern ushort	ydm02_rotate_txt[];
extern ushort	ydm05_rotate_txt[];
extern ushort	 lava_rotate_txt[];
extern ushort  sabaku_ryuusa_txt[];
extern ushort	in_py_ryuusa_txt[];
extern ushort	  mecha_conv_txt[];


/* ========================================================================================
		: Ptr to the water surface record of each scene.
=========================================================================================== */
extern WaterSurfRec	  waterSurf_04_00[];	//	Teresa obake.
extern WaterSurfRec	  waterSurf_04_01[];	//	Teresa obake.
extern WaterSurfRec	  waterSurf_05_01[];	//	Yukiyama1.
extern WaterSurfRec	  waterSurf_06_00[];	//	Select room B1-0.
extern WaterSurfRec	  waterSurf_06_12[];	//	Select room B1-1,2.
extern WaterSurfRec	  waterSurf_07_01[];	//	Horror dungeon. Underground lake.
extern WaterSurfRec	  waterSurf_07_02[];	//	Horror dungeon. Strange smog. [ I/A ]
extern WaterSurfRec	  waterSurf_08_01[];	//	Sabaku.
extern WaterSurfRec	  waterSurf_08_51[];	//	Sabaku. Strange  smog.
extern WaterSurfRec	  waterSurf_10_01[];	//	Yukiyama2.
extern WaterSurfRec	  waterSurf_11_01[];	//	Pool 1F.
extern WaterSurfRec	  waterSurf_11_02[];	//	Pool B1.
extern WaterSurfRec	  waterSurf_12_01[];	//	Water dungeon. Spoiled water.
extern WaterSurfRec	  waterSurf_12_05[];	//	Water dungeon. Strange smog. [ I/A ]
extern WaterSurfRec	  waterSurf_12_02[];	//	In the sunken ship.
extern WaterSurfRec	  waterSurf_13_01[];	//	Big world. Big.
extern WaterSurfRec	  waterSurf_13_02[];	//	Big world. Small.
extern WaterSurfRec	  waterSurf_16_01[];	//	Main map.
extern WaterSurfRec	  waterSurf_22_02[];	//	In valcano. Lava pond.
extern WaterSurfRec	  waterSurf_23_01[];	//	Water land. Scene1.
extern WaterSurfRec	  waterSurf_23_02[];	//	Water land. Scene2.
extern WaterSurfRec	  waterSurf_24_01[];	//	Mountain.
extern WaterSurfRec	  waterSurf_26_01[];	//	Uraniwa.
extern WaterSurfRec	  waterSurf_36_01[];	//	Donkey kai.


// ----------------------------------------------------------------------------------------
//
//
//		Ptr to the Gfx and point data of each fall [ No Light ].
//
//
/* ========================================================================================
		: Point data.
=========================================================================================== */
extern short	waterfall_0801_pt[];		//	In pyramid sandfall-1 [ xlu, Fog ].
extern short	waterfall_0802_pt[];		//	In pyramid sandfall-2 [ opa, Fog ].
extern short	waterfall_0803_pt[];		//	In pyramid sandfall-3 [ xlu, Fog ].
extern short	waterfall_1601_pt[];		//	Main map. Water fall.
extern short	waterfall_1901_pt[];		//	Ext2. Lava yoko scroll-1.
extern short	waterfall_1902_pt[];		//	Ext2. Lava yoko scroll-2.
extern short	waterfall_1903_pt[];		//	Ext2. Lava yoko scroll-3.
extern short	waterfall_2201_pt[];		//	Motos. Ground lava fall.
extern short	waterfall_2202_pt[];		//	In valcano. Fire fall.
extern short	metalfall_2801_pt[];		//	In the fall. [ xlu, Fog ].
extern short	donkey_wfA_pt[];			//	Donkey waterfall A.
extern short	donkey_wfB_pt[];			//	Donkey waterfall B.
extern short	donkey_wfC_pt[];			//	Donkey waterfall C.
extern short	donkey_wfD_pt[];			//	Donkey waterfall D.
extern short	donkey_wfE_pt[];			//	Donkey waterfall E.


/* ========================================================================================
		: Initialize Gfx.
=========================================================================================== */
extern Gfx  sf_08_xlu_init[];		//	In pyramid sandfall-1,3 [ xlu, Fog ].
extern Gfx  sf_08_opa_init[];		//	In pyramid sandfall-2.	[ opa, Fog ].
extern Gfx	mtfll_xlu_init[];		//	In the fall.			[ xlu, Fog ].


/* ========================================================================================
		: Reset Gfx.
=========================================================================================== */
extern Gfx  sf_08_xlu_reset[];		//	In pyramid sandfall-1,3 [ xlu, Fog ].
extern Gfx  sf_08_opa_reset[];		//	In pyramid sandfall-2.	[ opa, Fog ].
extern Gfx	mtfll_xlu_reset[];		//	In the fall.			[ xlu, Fog ].


/* ========================================================================================
		: Drawing Gfx.
=========================================================================================== */
extern Gfx	waterfall_0801_draw[];		//	In pyramid sandfall-1,2.
extern Gfx	waterfall_0803_draw[];		//	In pyramid sandfall-3.
extern Gfx	waterfall_1601_draw[];		//	Main map. Water fall.
extern Gfx	waterfall_1901_draw[];		//	Ext2. Lava yoko scroll-1,2.
extern Gfx	waterfall_1903_draw[];		//	Ext2. Lava yoko scroll-3.
extern Gfx	waterfall_2201_draw[];		//	Motos. Ground lava fall.
extern Gfx	waterfall_2202_draw[];		//	In valcano. Fire fall.
extern Gfx	metalfall_2801_draw[];		//	In the fall.
extern Gfx	donkey_wfAB_draw[]	 ;		//	Donkey waterfall A,B.
extern Gfx	donkey_wfCD_draw[]	 ;		//	Donkey waterfall C,D.
extern Gfx	donkey_wfE_draw[]	 ;		//	Donkey waterfall E.



// ----------------------------------------------------------------------------------------
//
//
//		Ptr to the Gfx and point data of each fall [ With Light ].
//
//
/* ========================================================================================
		: Point data.
=========================================================================================== */
extern short	sabaku_sandfall01_pt[];		//	Sabaku sandfall-1.
extern short	sabaku_sandfall02_pt[];		//	Sabaku sandfall-2.
extern short	sabaku_sandfall03_pt[];		//	Sabaku sandfall-3.
extern short	beltcv14_lg_pt[];			//	Clock Tower. Belt conveyor. Long.
extern short	beltcv14_st_pt[];			//	Clock Tower. Belt conveyor. Short.


/* ========================================================================================
		: Initialize Gfx.
=========================================================================================== */
extern Gfx	sabaku_sandfall_init[];			//	Sabaku sandfall-1,2,3.
extern Gfx	beltcv1400_init[];				//	Clock Tower. Belt conveyor.


/* ========================================================================================
		: Reset Gfx.
=========================================================================================== */
extern Gfx	sabaku_sandfall_reset[];		//	Sabaku sandfall-1,2,3.
extern Gfx	beltcv1400_reset[];				//	Clock Tower. Belt conveyor.


/* ========================================================================================
		: Drawing Gfx.
=========================================================================================== */
extern Gfx	sabaku_sandfall01_draw[];		//	Sabaku sandfall-1.
extern Gfx	sabaku_sandfall02_draw[];		//	Sabaku sandfall-2.
extern Gfx	sabaku_sandfall03_draw[];		//	Sabaku sandfall-3.
extern Gfx	beltcv14_ls_draw[];				//	Clock Tower. Belt conveyor.



// ----------------------------------------------------------------------------------------
//
//
//		Ptr to the Gfx and point data of sand cone [ With Light ].
//
//
/* ========================================================================================
		: Point data.
=========================================================================================== */
extern short	sandcone01_pt[];		// Sand cone-1.
extern short	sandcone02_pt[];		// Sand cone-2. [ Fog ]


/* ========================================================================================
		: Initialize Gfx.
=========================================================================================== */
extern Gfx  sandcone0801_init[]	;		//	Sand cone-1.
extern Gfx  sandcone0802_init[]	;		//	Sand cone-2. [ Fog ]


/* ========================================================================================
		: Reset Gfx.
=========================================================================================== */
extern Gfx	sandcone0801_reset[];		//	Sand cone-1.
extern Gfx	sandcone0802_reset[];		//	Sand cone-2. [ Fog ]


/* ========================================================================================
		: Drawing Gfx.
=========================================================================================== */
extern Gfx	sandcone_draw[];			//	Sand cone-1,2.

