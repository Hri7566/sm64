/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 18 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage18


/* ================================================================================
		: Move BGs sequence.
===================================================================================	*/
SEQ_Stage18_MoveBGs:

		seqActor(S_movebg01,     154,-1919,-6256,   0,270,0,   0,7,0,   e_seesaw)
		seqActor(S_movebg01,   -2047,-3378,-2047,   0,  0,0,   0,7,0,   e_seesaw)

		seqActor(S_movebg02,    3251,-1082,-6256,   0,270,0,   1,75,0,   e_doublelift)
		seqActor(S_movebg02,    2355,-1901,-6256,   0,270,0,   1,75,0,   e_doublelift)
		seqActor(S_movebg02,    1459,-1594,-6256,   0, 90,0,   1,75,0,   e_doublelift)
		seqActor(S_movebg02,   -1151,-2413,-6256,   0,270,0,   1,75,0,   e_doublelift)

		seqReturn


/* ================================================================================
		: Enemies sequence.
===================================================================================	*/
SEQ_Stage18_Enemies:

		seqActor(S_bikkuri_button,	4506,26,-6246,  0,0,0,  0,2,0,  e_bikkuri_switch)

		seqReturn


/* ================================================================================
		: Stars sequence.
===================================================================================	*/
SEQ_Stage18_Stars:

		seqActor(S_NULL,  4600, 250, -4500, 0,0,0,  0,0,0,  e_tripstar_getcoins) /* 8 coins  */

		seqReturn


/* ================================================================================
		: Stage 18 main sequence.
===================================================================================	*/
SEQ_DoStage18:

	seqInitStage()
	seqLoadText(SEGMENT_TEXTURE	  , _KCastleInTextureSegmentRomStart, _KCastleInTextureSegmentRomEnd)
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage18SegmentRomStart		, 	_GfxStage18SegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_hSegmentRomStart	    , _GfxEnemy1_hSegmentRomEnd		)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_hSegmentRomStart	    , _HmsEnemy1_hSegmentRomEnd		)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart  , _GfxBasic_enemySegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart  , _HmsBasic_enemySegmentRomEnd	)

	seqBeginConstruction()

		seqHmsMario(S_Mario  , ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_h)

		seqHmsShape(S_movebg01, RCP_HmsExt7Shiso)

		seqHmsShape(S_bg20, RCP_HmsDokan)

		seqBeginScene(1, RCP_Stage18Scene1)
			seqActor(S_NULL,  -6143,5734+START_H,-6143,  0,0,0,  0,10,0,  e_player_landing)
			seqPort(10,  18, 1, 10)			/*	MARIO stage in.	*/
			seqCourseOut(16, 1,  7)			/*	Exit.			*/
			seqGameClear(16, 1,  8)			/*	Get trip star.	*/
			seqGameOver (16, 1,  6)			/*	Down. 			*/
			seqCall(SEQ_Stage18_Stars)
			seqCall(SEQ_Stage18_MoveBGs)
			seqCall(SEQ_Stage18_Enemies)
			seqMapInfo(cx1801_info)
			seqTagInfo(cx1801_info_tag)
			seqMessage(SEQ_MESG_ENTRANT, 129)
			seqSetMusic(NA_STG_GROUND, NA_SLIDER_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()
	seqEndConstruction()

	seqEnterMario(1, 0, -6143,5734,-6143)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
