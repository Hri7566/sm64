/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage18 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

#define		STAGE18_CLEAR_R		0
#define		STAGE18_CLEAR_G		0
#define		STAGE18_CLEAR_B		0

extern Gfx gfx_hs_1[]	 	;
extern Gfx gfx_hs_alpha[]	;
extern Gfx gfx_hs_clear[]	;
extern Gfx gfx_hs_gaiheki[]	;
extern Gfx gfx_hs_obj[]	 	;

/* ===============================================================================
        : Hierarchy map data of Ext7Shiso.
================================================================================== */
Hierarchy RCP_HmsExt7Shiso[] = {
    hmsHeader(1000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_hs_obj)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage18Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(RGBA16(STAGE18_CLEAR_R, STAGE18_CLEAR_G, STAGE18_CLEAR_B, 1), NULL)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, 25000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_PARALLEL,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF  , gfx_hs_1)
					hmsGfx(RM_SURF  , gfx_hs_gaiheki)
					hmsGfx(RM_SURF  , gfx_hs_clear)
					hmsGfx(RM_SPRITE, gfx_hs_alpha)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
