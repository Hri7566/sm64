/********************************************************************************
						Ultra 64 MARIO Brothers

						  boot sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#define	ASSEMBLER
#define	SCENE_NUMBER	0

#include "../headers.h"


		.data
		.align	2
		.align	0

		.globl	SEQ_BootSequence

/********************************************************************************/
/*																				*/
/*	Boot sequence.																*/
/*																				*/
/********************************************************************************/

SEQ_BootSequence:

	seqInitStage()

	seqWait(2)
	seqBlanking(0)
	seqSetResult(SCENE_NUMBER)

#if SCENE_NUMBER == 0
		seqExecute(SEGMENT_DEMODATA, _TitleSegmentRomStart, _TitleSegmentRomEnd, SEQ_LogoSequence)
#else
		seqExecute(SEGMENT_GAMEDATA, _GameSegmentRomStart, _GameSegmentRomEnd, SEQ_GameSequence)
#endif
		seqJump(SEQ_BootSequence)
