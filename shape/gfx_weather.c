/********************************************************************************
						Ultra 64 MARIO Brothers

						game weather data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 12, 1995
 ********************************************************************************/

#include "../headers.h"

static long long weather_patch = 0;

#include "Weather/Snow/flowerA.h"
#include "Weather/Snow/maguma_txt.h"
#include "Weather/Snow/gush_txt.h"

#include "Weather/snowdata.sou"
