/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 7 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

				This module was programmed by Y.Nishida

							 Mar 18, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage07


/* ====================================================================================	
		: BG parts sequence.
=======================================================================================	*/
SEQ_Stage07_BGParts:

		seqActor(S_fireball_yellow,  4936,-357,-4146,  0,0,0,  0,0,0,  e_fire_animation)
		seqActor(S_fireball_yellow,  5018,-460,-5559,  0,0,0,  0,0,0,  e_fire_animation)
		seqActor(S_fireball_yellow,  1997, 666, -235,  0,0,0,  0,0,0,  e_fire_animation)
		seqActor(S_fireball_yellow,  1762,-460,-2610,  0,0,0,  0,0,0,  e_fire_animation)
		seqActor(S_fireball_yellow,  4178,-255,-3737,  0,0,0,  0,0,0,  e_fire_animation)
		seqActor(S_fireball_yellow,  2233,-460,  256,  0,0,0,  0,0,0,  e_fire_animation)
		seqActor(S_fireball_yellow,  5510,-255,-3429,  0,0,0,  0,0,0,  e_fire_animation)
		seqActor(S_fireball_yellow,  4690,-357, -767,  0,0,0,  0,0,0,  e_fire_animation)
		seqActor(S_fireball_yellow,  3462,-255,-1125,  0,0,0,  0,0,0,  e_fire_animation)
		seqActor(S_fireball_yellow,  1762, 666,    0,  0,0,0,  0,0,0,  e_fire_animation)
		seqActor(S_fireball_yellow,  1762,-460,  256,  0,0,0,  0,0,0,  e_fire_animation)

		seqActor(S_fireball_yellow,  6482, 461,3226,   0,0,0,  0,0,0,  e_fire_animation)
		seqActor(S_fireball_yellow,  1075, 461,6543,   0,0,0,  0,0,0,  e_fire_animation)
		seqActor(S_fireball_yellow,  6912, 461,6543,   0,0,0,  0,0,0,  e_fire_animation)
		seqActor(S_fireball_yellow,  6912, 461,3697,   0,0,0,  0,0,0,  e_fire_animation)
		seqActor(S_fireball_yellow,  6482, 461,7014,   0,0,0,  0,0,0,  e_fire_animation)
		seqActor(S_fireball_yellow,  3817, 717,1034,   0,0,0,  0,0,0,  e_fire_animation)

		seqActor(S_NULL,  799,1024,4434,  0,0,0,  0,184,0,  e_bar)	
		seqActor(S_NULL,  889,1024,3277,  0,0,0,  0,184,0,  e_bar)

		seqReturn


/* ====================================================================================	
		: Move BGs sequence.
=======================================================================================	*/
SEQ_Stage07_MoveBGs:

		seqActor(S_movebg01,   1100, 950, 6350,  0, 0,0,  0,0,0,  e_NEWSlift)
		seqActor(S_movebg03,  -3243,1434, 1392,  0,27,0,  0,0,0,  e_udlift_s)
		seqActor(S_movebg03,  -2816,2253,-2509,  0, 0,0,  0,1,0,  e_udlift_s)
		seqActor(S_movebg03,   -973,1741,-7347,  0, 0,0,  0,2,0,  e_udlift_s)
		seqActor(S_movebg03,  -3533,1741,-7040,  0, 0,0,  0,3,0,  e_udlift_s)

		seqActor(S_NULL,   	   614,-4690,2330,  0,270,0,  0,1,0,  e_switchdoor)
		seqActor(S_hanbutton, -307,-4997,2483,  0,270,0,  0,0,0,  e_switchdoor_switch)

		seqActor(S_commonlift, 1270,2000,4000,  0,270,0,  9,164,0,  e_linelift)

		seqReturn


/* ====================================================================================	
		: Enemies sequence.
=======================================================================================	*/
SEQ_Stage07_Enemies:

		seqActor(S_nessy,  -3533,-4969, 3558,  0,0,0,  0,0,0,  e_nessy)
		seqActor(S_NULL,   -6093, 3075,-7807,  0,0,0,  0,0,0,  e_dunjon_rock_st)

		seqActor(S_NULL,    -500, 1600, 3500,  0,0,0,  0,4,0,  e_firebigbar)
		seqActor(S_NULL,    -500, 1600, 3800,  0,0,0,  0,4,0,  e_firebigbar)

		seqReturn


/* ====================================================================================	
		: Stars sequence.
=======================================================================================	*/
SEQ_Stage07_Stars:

		seqLevelActor(0x003f,S_polystar, -3600,-4000, 3600,  0,0,0,  0,0,0,  e_tripstar)			/* nessy				*/
		seqLevelActor(0x003f,S_NULL    ,  4000,  300, 5000,  0,0,0,  1,0,0,  e_tripstar_getcoins)	/* 8_coin_in_lift_room 	*/
		seqLevelActor(0x003f,S_polystar,  6200,-4400, 2300,  0,0,0,  2,0,0,  e_tripstar) 			/* metal 				*/
		seqLevelActor(0x003f,S_polystar, -2100, 2100,-7550,  0,0,0,  3,0,0,  e_tripstar) 			/* gus_room 			*/
		seqLevelActor(0x003f,S_polystar, -6500, 2700,-1600,  0,0,0,  4,0,0,  e_tripstar) 			/* up_to_the_hole 		*/
		seqLevelActor(0x003f,S_polystar, -5000, 3050,-6700,  0,0,0,  5,0,0,  e_tripstar) 			/* secret_hole 			*/

		seqReturn


/* ====================================================================================	
		: Stage 7 main sequence.
=======================================================================================	*/
SEQ_DoStage07:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage7SegmentRomStart	  , _GfxStage7SegmentRomEnd		)
	seqLoadText(SEGMENT_TEXTURE	  , _GCaveTextureSegmentRomStart  , _GCaveTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_fSegmentRomStart	  , _GfxEnemy1_fSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_fSegmentRomStart	  , _HmsEnemy1_fSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_hSegmentRomStart	  , _GfxEnemy2_hSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_hSegmentRomStart	  , _HmsEnemy2_hSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_f)
		seqCall(SetEnemy2_h)

		seqHmsShape(S_door1, RCP_HmsDoor1)			/*	Wood 	door.	*/
		seqHmsShape(S_door3, RCP_HmsDoor3)			/*	Iron 	door.	*/
		seqHmsShape(S_door4, RCP_HmsDoor4)			/*	Stone	door.	*/

		seqHmsShape(S_movebg01, RCP_HmsNEWSLift	)
		seqHmsShape(S_movebg02, RCP_HmsNEWSBut	)
		seqHmsShape(S_movebg03, RCP_HmsKdowElv	)

		seqHmsShape(S_movebg04, RCP_HmsKdBigRock)
		seqHmsShape(S_movebg05, RCP_HmsKdRockPiA)
		seqHmsShape(S_movebg06, RCP_HmsKdRockPiB)

		seqHmsShape(S_movebg07, RCP_HmsKdTrap)

		seqBeginScene(1, RCP_Stage7Scene1)
			seqActor(S_NULL,  -7152,2161+START_H,7181,  0,135,0,   0,10,0,  e_player_entpict)
			seqActor(S_NULL,   3351,	   -4690,4773,  0,  0,0,  52,11,0,  e_tripchimney2	)
			seqPort(10,  7, 1, 10)			/*	MARIO stage in.					*/
			seqPort(11, 28, 1, 10)			/*	To St28 [ Ext 5. In Fall. ].	*/
			seqGameClear(6, 3,  52)
			seqGameOver (6, 3, 102)
			seqCall(SEQ_Stage07_BGParts)
			seqCall(SEQ_Stage07_MoveBGs)
			seqCall(SEQ_Stage07_Enemies)
			seqCall(SEQ_Stage07_Stars)
			seqMapInfo(cx0701_info)
			seqTagInfo(cx0701_info_tag)
			seqAreaInfo(cx0701_area)
			seqSetMusic(NA_STG_DUNGEON, NA_DUNGEON_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 135,  -7152,2161,7181)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
