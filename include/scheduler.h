/********************************************************************************
	scheduler.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	May 11, 1996
 ********************************************************************************/

#ifndef	SCHEDULER_H
#define	SCHEDULER_H


/*-------------------------------------------------------------------------------
 *	scheduler messages.
 */
#define	SC_MESG_RSPDONE			100
#define	SC_MESG_RDPDONE			101
#define	SC_MESG_RETRACE			102
#define	SC_MESG_GFXEXEC			103
#define	SC_MESG_HWRESET			104

/*-------------------------------------------------------------------------------
 *	RSP task status.
 */
#define	SC_STAT_IDLE			0
#define	SC_STAT_RUNNING			1
#define	SC_STAT_SUSPENDED		2
#define	SC_STAT_RSPDONE			3
#define	SC_STAT_RDPDONE			4

/*-------------------------------------------------------------------------------
 *	Scheduler client name.
 */
#define	SC_CLIENT_AUDIO			1
#define	SC_CLIENT_GRAPH			2

/********************************************************************************
 *
 *	type definition
 *
 ********************************************************************************/


/*-------------------------------------------------------------------------------
 *	RSP task record for scheduler.
 */
typedef struct {
	OSTask		ostask;
	OSMesgQueue	*msgque;
	OSMesg		osmesg;
	int			status;
	int			pad;
} RSPTask;

/*-------------------------------------------------------------------------------
 *	Scheduler client record.
 */
typedef struct {
	OSMesgQueue  *msgque;			/* message queue for send retrace message	*/
	OSMesg		  osmesg;			/* retrace message							*/
} ScClient;


/********************************************************************************
 *
 *	Function prototypes
 *
 ********************************************************************************/

extern void
ScSetClient(int type, ScClient *client, OSMesgQueue *queue, OSMesg mesg);

extern void
ScSetRSPTask(RSPTask *rsptask);

extern void
ScStartGraphicTask(RSPTask *rsptask);

extern void
ScStartAudioTask(RSPTask *rsptask);

extern int
ScEnableAudioTask(void);

extern void
ScDisableAudioTask(void);


#endif
