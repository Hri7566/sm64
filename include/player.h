/********************************************************************************
	player.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	May 11, 1996
 ********************************************************************************/

#ifndef PLAYER_H
#define	PLAYER_H

#include "playerstat.h"

#define	DLOG_DONE				0
#define	DLOG_LOOKFRONT			1
#define	DLOG_LOOKUP				2
#define	DLOG_LOOKDOWN			3

#define	DLOG_RESULT_BUSY		0
#define	DLOG_RESULT_START		1
#define	DLOG_RESULT_READY		2

#define	CONDOR_STEAL_HAT		1
#define	MONKEY_STEAL_HAT		2

#define	VOICE_JMP		0x00000000
#define	VOICE_NON		0xffffffff


#define	PL_FLAG_REDCAP		0x00000001
#define	PL_FLAG_GHOSTCAP	0x00000002
#define	PL_FLAG_METALCAP	0x00000004
#define	PL_FLAG_WINGCAP		0x00000008
#define	PL_FLAG_HAVECAP		0x0000000F

#define	PL_FLAG_HEADCAP		0x00000010
#define	PL_FLAG_HANDCAP		0x00000020
#define	PL_FLAG_FLASHING	0x00000040
#define	PL_FLAG_FADING		0x00000080

#define	PL_FLAG_JMPCANCEL	0x00000100
#define	PL_FLAG_GETSTAR		0x00000200

#define	PL_FLAG_SOUNDTRIG	0x00010000
#define	PL_FLAG_VOICETRIG	0x00020000
#define	PL_FLAG_VOICEDOWN	0x00040000

#define	PL_FLAG_PUNCH		0x00100000
#define	PL_FLAG_PWKICK		0x00200000
#define	PL_FLAG_SQKICK		0x00400000

#define	PL_FLAG_TRAMPOLINE	0x01000000
#define	PL_FLAG_SETPOSINFO	0x02000000
#define	PL_FLAG_JUMPBOUND	0x40000000
#define	PL_FLAG_PUSHWALL	0x80000000

#define	PL_WATER_WALK		100
#define	PL_WATER_SWIM		 80


#define	PL_INFO_ENTRANT		0x00010000

/********************************************************************************/
/*																				*/
/*	Player status	(16bits value)												*/
/*																				*/
/********************************************************************************/

/* mask pattern */

#define	PA_MOVEREQ			0x0003
#define	PA_MOTIONS			0x000F
#define	PA_EVENTS			0xA41F


/* action bits */

#define	PA_WALKREQ			0x0001
#define	PA_JUMPREQ			0x0002
#define	PA_SWIMREQ			0x0002
#define	PA_LANDREQ			0x0004
#define	PA_SLIPREQ			0x0008
#define	PA_VIEWREQ			0x0010
#define	PA_WAITREQ			0x0020
#define	PA_PRESREQ			0x0040
#define	PA_JUMPBTN			0x0080
#define	PA_SWIMBTN			0x0080

#define	PA_GASSY			0x0100
#define	PA_PUDDLE			0x0200
#define	PA_QUAKING			0x0400
#define	PA_TAKEREQ			0x0800

#define	PA_ATKREACT			0x1000
#define	PA_ACTTRIG			0x2000
#define	PA_BAKSTAT			0x4000
#define	PA_BAKTRIG			0x8000



/********************************************************************************/
/*																				*/
/*	Player hit status	(32bits value)											*/
/*																				*/
/********************************************************************************/

#define	PH_HIPDROP			0x000001
#define	PH_PUNCH			0x000002
#define	PH_POWERKICK		0x000004
#define	PH_SQUATKICK		0x000008
#define	PH_SLIDEKICK		0x000010
#define	PH_SLIDING			0x000020
#define	PH_TRAMPLE			0x000040
#define	PH_HEADATK			0x000080

#define	PH_PUNCHKICK		0x000006
#define	PH_KICK				0x00000C
#define	PH_ATTACK			0x00007F
#define	PH_ATTACKALL		0x0000FF


/********************************************************************************/
/*																				*/
/*	Player status	(32bits value)												*/
/*																				*/
/********************************************************************************/

/* player process type flag */

#define	PS_CODE_MASK		0x000001FF
#define	PS_PROC_MASK		0x000001C0
#define	PS_PROC_WAIT		0x00000000
#define	PS_PROC_MOVE		0x00000040
#define	PS_PROC_JUMP		0x00000080
#define	PS_PROC_SWIM		0x000000C0
#define	PS_PROC_DEMO		0x00000100
#define	PS_PROC_SPEC		0x00000140
#define	PS_PROC_ACTN		0x00000180


/* player status type flag */

#define	PS_TYPE_WAIT		0x00000200		/* wait				*/
#define	PS_TYPE_MOVE		0x00000400		/* move				*/
#define	PS_TYPE_JUMP		0x00000800		/* jump				*/

#define	PS_TYPE_DEMO		0x00001000		/* demonstration	*/
#define	PS_TYPE_SWIM		0x00002000		/* swimming			*/
#define	PS_TYPE_SINK		0x00004000		/* sinking			*/
#define	PS_TYPE_SQRT		0x00008000		/* squating			*/
#define	PS_TYPE_SKTE		0x00010000		/* skating			*/
#define	PS_TYPE_DOWN		0x00020000		/* downing			*/
#define	PS_TYPE_SLIP		0x00040000		/* slipping		-	*/
#define	PS_TYPE_DIVE		0x00080000		/* diving			*/
#define	PS_TYPE_POLE		0x00100000		/* hanging pole		*/
#define	PS_TYPE_ROOF		0x00200000		/* hanging roof		*/
#define	PS_TYPE_READ		0x00400000		/* read billboard	*/
#define	PS_TYPE_ATCK		0x00800000		/* attacking		*/
#define	PS_TYPE_WIND		0x01000000		/* wind jump ok		*/
#define	PS_TYPE_JPCN		0x02000000		/* jump cancel		*/

#define	PS_TYPE_VIEW		0x04000000		/* view mode ok		*/
#define	PS_TYPE_QUIT		0x08000000		/* pause quit ok	*/

#define	PS_TYPE_HAND		0x10000000		/* hand animation	*/
#define	PS_TYPE_HEAD		0x20000000		/* head animation	*/
#define	PS_TYPE_BODY		0x40000000		/* body animation	*/
#define	PS_TYPE_THRW		0x80000000		/* throwing flag	*/


#define	PS_TYPE_WALL		0x00000000		/* hanging wall	-	*/

#define	PS_TYPE_BAR			PS_TYPE_POLE


/* player waiting status code */

#define	PS_INACTIVE			( 0x00 )

#define	PS_WAITING			( 0x01 + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT + PS_TYPE_READ )
#define	PS_SLEEPILY			( 0x02 + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT + PS_TYPE_READ )
#define	PS_SLEEPING			( 0x03 + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT )
#define	PS_WAKEUP			( 0x04 + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT )

#define	PS_WAITTIRED		( 0x05 + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT + PS_TYPE_READ )
#define	PS_HOLDTIRED		( 0x06 + PS_TYPE_WAIT 				 + PS_TYPE_QUIT )
#define	PS_HOLDING			( 0x07 + PS_TYPE_WAIT				 + PS_TYPE_QUIT )
#define	PS_SHOULDER			( 0x08 + PS_TYPE_WAIT				 + PS_TYPE_QUIT )
#define	PS_WALLWAIT			( 0x09 + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT + PS_TYPE_READ )
#define	PS_GASWAIT			( 0x0A + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT + PS_TYPE_READ )
#define	PS_COLDWAIT			( 0x0B + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT + PS_TYPE_READ )
#define	PS_OSHINWAIT		( 0x0C + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT )
#define	PS_SANDWAIT			( 0x0D + PS_TYPE_WAIT + PS_TYPE_DOWN				)
#define	PS_ELECWAIT			( 0x0E + PS_TYPE_WAIT + PS_TYPE_DOWN				)


#define	PS_SQUATING			( 0x20 + PS_TYPE_WAIT + PS_TYPE_SQRT + PS_TYPE_VIEW + PS_TYPE_QUIT )
#define	PS_SQSTART			( 0x21 + PS_TYPE_WAIT + PS_TYPE_SQRT + PS_TYPE_VIEW + PS_TYPE_QUIT )
#define	PS_SQSTAND			( 0x22 + PS_TYPE_WAIT + PS_TYPE_SQRT + PS_TYPE_VIEW + PS_TYPE_QUIT )
#define	PS_SQWALKST			( 0x23 + PS_TYPE_WAIT + PS_TYPE_SQRT + PS_TYPE_VIEW + PS_TYPE_QUIT )
#define	PS_SQWALKED			( 0x24 + PS_TYPE_WAIT + PS_TYPE_SQRT + PS_TYPE_VIEW + PS_TYPE_QUIT )
#define	PS_SLIDEKEND		( 0x25 + PS_TYPE_WAIT								+ PS_TYPE_QUIT )

#define	PS_QUAKING			( 0x26 + PS_TYPE_WAIT + PS_TYPE_DOWN )
#define	PS_VIEWING			( 0x27 + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT )

#define	PS_BACKJEND			( 0x2F + PS_TYPE_WAIT				 + PS_TYPE_QUIT )
#define	PS_JUMPEND			( 0x30 + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT )
#define	PS_SECJPEND			( 0x31 + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT )
#define	PS_LANDEND			( 0x32 + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT )
#define	PS_TURNJUMPE		( 0x33 + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT )
#define	PS_HOLDJUMPE		( 0x34 + PS_TYPE_WAIT				 + PS_TYPE_QUIT )
#define	PS_HOLDLANDE		( 0x35 + PS_TYPE_WAIT				 + PS_TYPE_QUIT )
#define	PS_JMPETHROW		( 0x36 + PS_TYPE_WAIT + PS_TYPE_JUMP + PS_TYPE_THRW )
#define	PS_SPINJEND			( 0x38 + PS_TYPE_WAIT + PS_TYPE_ATCK + PS_TYPE_HAND + PS_TYPE_QUIT )
#define	PS_FIREJUMPE		( 0x39 + PS_TYPE_WAIT				 + PS_TYPE_QUIT )
#define	PS_ULTRAJEND		( 0x3A + PS_TYPE_WAIT				 + PS_TYPE_QUIT )
#define	PS_BROADJEND		( 0x3B + PS_TYPE_WAIT				 + PS_TYPE_QUIT )
#define	PS_HIPATKEND		( 0x3C + PS_TYPE_WAIT + PS_TYPE_ATCK				)

#define	PS_BRAKEND			( 0x3D + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT )
#define	PS_SLIPEND			( 0x3E + PS_TYPE_WAIT + PS_TYPE_VIEW + PS_TYPE_QUIT )
#define	PS_HOLDSLIPE		( 0x3F + PS_TYPE_MOVE				 + PS_TYPE_QUIT )



/* player running status code */

#define	PS_RUNNING			( 0x40 + PS_TYPE_MOVE + PS_TYPE_VIEW )
#define	PS_CARRYING			( 0x42 + PS_TYPE_MOVE )
#define	PS_TURNING			( 0x43 + PS_TYPE_MOVE )
#define	PS_TURNEND			( 0x44 + PS_TYPE_MOVE )
#define	PS_BRAKING			( 0x45 + PS_TYPE_MOVE + PS_TYPE_VIEW )
#define	PS_SKATING			( 0x46 + PS_TYPE_MOVE + PS_TYPE_ATCK + PS_TYPE_SKTE + PS_TYPE_HEAD )
#define	PS_TRANSFER			( 0x47 + PS_TYPE_MOVE )
#define	PS_SQWALKING		( 0x48 + PS_TYPE_MOVE + PS_TYPE_SQRT + PS_TYPE_VIEW )
#define	PS_FIREDASH			( 0x49 + PS_TYPE_MOVE + PS_TYPE_DOWN )
#define	PS_WALKEND			( 0x4A + PS_TYPE_MOVE + PS_TYPE_VIEW )
#define	PS_CARRYEND			( 0x4B + PS_TYPE_MOVE )


#define	PS_SLIPPING			( 0x50 )
#define	PS_HOLDSLIPN		( 0x51 )
#define	PS_SLIPFORE			( 0x52 + PS_TYPE_MOVE + PS_TYPE_SLIP + PS_TYPE_ATCK				   )
#define	PS_SLIPBACK			( 0x53 + PS_TYPE_MOVE + PS_TYPE_SLIP + PS_TYPE_ATCK + PS_TYPE_DIVE )
#define	PS_HOLDSLIPF		( 0x54 + PS_TYPE_MOVE + PS_TYPE_SLIP + PS_TYPE_ATCK				   )
#define	PS_HOLDSLIPB		( 0x55 + PS_TYPE_MOVE + PS_TYPE_SLIP + PS_TYPE_ATCK + PS_TYPE_DIVE )
#define	PS_CATCHING			( 0x56 + PS_TYPE_MOVE				 + PS_TYPE_ATCK	+ PS_TYPE_DIVE )
#define	PS_RUNATTACK		( 0x57 + PS_TYPE_MOVE				 + PS_TYPE_ATCK				   )
#define	PS_SQUATSLIP		( 0x59 + PS_TYPE_MOVE + PS_TYPE_SQRT + PS_TYPE_ATCK + PS_TYPE_VIEW )
#define	PS_SLIDEKICK		( 0x5A + PS_TYPE_MOVE + PS_TYPE_ATCK )

#define	PS_BACKDOWN			( 0x60 + PS_TYPE_MOVE + PS_TYPE_DOWN )
#define	PS_FOREDOWN			( 0x61 + PS_TYPE_MOVE + PS_TYPE_DOWN )
#define	PS_SBACKDOWN		( 0x62 + PS_TYPE_MOVE + PS_TYPE_DOWN )
#define	PS_SFOREDOWN		( 0x63 + PS_TYPE_MOVE + PS_TYPE_DOWN )
#define	PS_SAFEBDOWN		( 0x64 + PS_TYPE_MOVE + PS_TYPE_DOWN )
#define	PS_SAFEFDOWN		( 0x65 + PS_TYPE_MOVE + PS_TYPE_DOWN )
#define	PS_CATCHDOWN		( 0x66 + PS_TYPE_MOVE + PS_TYPE_DOWN )
#define	PS_LOSERDOWN		( 0x67 + PS_TYPE_MOVE + PS_TYPE_DOWN )

#define	PS_JUMPSLIP			( 0x70 + PS_TYPE_MOVE + PS_TYPE_VIEW )
#define	PS_LANDSLIP			( 0x71 + PS_TYPE_MOVE + PS_TYPE_VIEW )
#define	PS_SECJPSLIP		( 0x72 + PS_TYPE_MOVE + PS_TYPE_VIEW )
#define	PS_TURNJUMPS		( 0x73 + PS_TYPE_MOVE + PS_TYPE_VIEW )
#define	PS_HOLDJUMPS		( 0x74 + PS_TYPE_MOVE )
#define	PS_HOLDLANDS		( 0x75 + PS_TYPE_MOVE )

#define	PS_SANDJUMP			( 0x76 + PS_TYPE_MOVE				 )
#define	PS_SANDHJUMP		( 0x77 + PS_TYPE_MOVE				 )
#define	PS_ULTRAJMPS		( 0x78 + PS_TYPE_MOVE + PS_TYPE_VIEW )
#define	PS_BROADSLIP		( 0x79 + PS_TYPE_MOVE )
#define	PS_BACKJMPS			( 0x7A + PS_TYPE_MOVE + PS_TYPE_VIEW )



/* player jumping status code */

	/* pole and bird */
#define	PS_JUMPING			( 0x80 + PS_TYPE_JUMP + PS_TYPE_JPCN + PS_TYPE_WIND )
#define	PS_SECJUMP			( 0x81 + PS_TYPE_JUMP + PS_TYPE_JPCN + PS_TYPE_WIND )
#define	PS_ULTRAJUMP		( 0x82 + PS_TYPE_JUMP				 + PS_TYPE_WIND )
#define	PS_BACKJUMP			( 0x83 + PS_TYPE_JUMP				 + PS_TYPE_WIND )
#define	PS_WINGJUMP			( 0x94 + PS_TYPE_JUMP + PS_TYPE_JPCN + PS_TYPE_WIND )
#define	PS_MISSJUMP			( 0x85 + PS_TYPE_JUMP + PS_TYPE_JPCN + PS_TYPE_WIND )
#define	PS_JUMPWALL			( 0x86 + PS_TYPE_JUMP + PS_TYPE_JPCN + PS_TYPE_WIND )
#define	PS_TURNJUMPN		( 0x87 + PS_TYPE_JUMP				 + PS_TYPE_WIND )
#define	PS_BROADJUMP		( 0x88 + PS_TYPE_JUMP + PS_TYPE_JPCN + PS_TYPE_WIND )
#define	PS_WATERNJMP		( 0x89 + PS_TYPE_JUMP				 + PS_TYPE_WIND )
#define	PS_JMPCATCH			( 0x8A + PS_TYPE_JUMP + PS_TYPE_DIVE + PS_TYPE_WIND + PS_TYPE_ATCK )
#define	PS_TRAMPOLIN		( 0x8B + PS_TYPE_JUMP + PS_TYPE_JPCN )
#define	PS_LANDING			( 0x8C + PS_TYPE_JUMP				 + PS_TYPE_WIND )
#define	PS_BARLAND			( 0x8D + PS_TYPE_JUMP + PS_TYPE_JPCN + PS_TYPE_WIND )
#define	PS_SLIPFALL			( 0x8E + PS_TYPE_JUMP + PS_TYPE_JPCN + PS_TYPE_WIND )

	/* pole  */
#define	PS_CANNJUMP			( 0x98 + PS_TYPE_JUMP + PS_TYPE_ATCK + PS_TYPE_DIVE )
#define	PS_FLIGHT			( 0x99 + PS_TYPE_JUMP + PS_TYPE_ATCK + PS_TYPE_DIVE + PS_TYPE_HAND )
#define	PS_BOARDJUMP		( 0x9A + PS_TYPE_JUMP + PS_TYPE_ATCK + PS_TYPE_SKTE + PS_TYPE_JPCN )
#define	PS_BOARDLAND		( 0x9B + PS_TYPE_JUMP + PS_TYPE_ATCK + PS_TYPE_SKTE )
#define	PS_WINDFLY			( 0x9C + PS_TYPE_JUMP + PS_TYPE_DIVE + PS_TYPE_HAND )


#define	PS_HOLDJUMPN		( 0xA0 + PS_TYPE_JUMP + PS_TYPE_JPCN + PS_TYPE_WIND )
#define	PS_HOLDLANDN		( 0xA1 + PS_TYPE_JUMP				 + PS_TYPE_WIND )
#define	PS_HSLPFALL			( 0xA2 + PS_TYPE_JUMP				 + PS_TYPE_WIND )
#define	PS_WATERCJMP		( 0xA3 + PS_TYPE_JUMP				 + PS_TYPE_WIND )

#define	PS_SPINJUMP			( 0xA4 + PS_TYPE_JUMP + PS_TYPE_ATCK + PS_TYPE_HAND )
#define	PS_CATCHSTOP		( 0xA6 + PS_TYPE_JUMP				 + PS_TYPE_WIND )
#define	PS_STAYWALL			( 0xA7 + PS_TYPE_JUMP )
#define	PS_FLYBIRD			( 0xA8 + PS_TYPE_MOVE )
#define	PS_HIPATTACK		( 0xA9 + PS_TYPE_JUMP + PS_TYPE_ATCK )
#define	PS_JUMPKICK			( 0xAA + PS_TYPE_JUMP + PS_TYPE_ATCK + PS_TYPE_WIND )
#define	PS_JMPNTHROW		( 0xAB + PS_TYPE_JUMP + PS_TYPE_THRW + PS_TYPE_WIND + PS_TYPE_JPCN )
#define	PS_JPPOWKICK		( 0xAC + PS_TYPE_JUMP + PS_TYPE_ATCK + PS_TYPE_WIND )
#define	PS_BSLIPSTOP		( 0xAD + PS_TYPE_JUMP				 + PS_TYPE_WIND )
#define	PS_HOPPERJMP		( 0xAE + PS_TYPE_JUMP )
#define	PS_SPECIALJP		( 0xAF + PS_TYPE_JUMP + PS_TYPE_JPCN + PS_TYPE_WIND )

#define	PS_JMPSBDOWN		( 0xB0 + PS_TYPE_JUMP + PS_TYPE_DOWN + PS_TYPE_WIND )
#define	PS_JMPSFDOWN		( 0xB1 + PS_TYPE_JUMP + PS_TYPE_DOWN + PS_TYPE_WIND )
#define	PS_JFOREDOWN		( 0xB2 + PS_TYPE_JUMP + PS_TYPE_DOWN + PS_TYPE_WIND )
#define	PS_JBACKDOWN		( 0xB3 + PS_TYPE_JUMP + PS_TYPE_DOWN + PS_TYPE_WIND )
#define	PS_FIREJUMP			( 0xB4 + PS_TYPE_JUMP + PS_TYPE_DOWN + PS_TYPE_WIND )
#define	PS_FIRELAND			( 0xB5 + PS_TYPE_JUMP + PS_TYPE_DOWN + PS_TYPE_WIND ) 
#define	PS_LANDDOWN			( 0xB6 + PS_TYPE_JUMP + PS_TYPE_DOWN + PS_TYPE_WIND )
#define	PS_FIREDOWN			( 0xB7 + PS_TYPE_JUMP + PS_TYPE_DOWN + PS_TYPE_WIND )
#define	PS_WINDDOWN			( 0xB8 + PS_TYPE_JUMP + PS_TYPE_DOWN + PS_TYPE_WIND )

#define	PS_THROWNFOR		( 0xBD + PS_TYPE_JUMP + PS_TYPE_DOWN + PS_TYPE_WIND )
#define	PS_THROWNBAK		( 0xBE + PS_TYPE_JUMP + PS_TYPE_DOWN + PS_TYPE_WIND )


/* player swimming status code */

#define	PS_SWIMNWAIT		( 0xC0 + PS_TYPE_WAIT + PS_TYPE_SWIM + PS_TYPE_QUIT + PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_SWIMCWAIT		( 0xC1 + PS_TYPE_WAIT + PS_TYPE_SWIM + PS_TYPE_QUIT + PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_SWIMNSTOP		( 0xC2 + PS_TYPE_WAIT + PS_TYPE_SWIM				+ PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_SWIMCSTOP		( 0xC3 + PS_TYPE_WAIT + PS_TYPE_SWIM				+ PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_SWIMDOWN			( 0xC4 + PS_TYPE_WAIT + PS_TYPE_SWIM + PS_TYPE_DEMO + PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_SWIMBDOWN		( 0xC5 + PS_TYPE_WAIT + PS_TYPE_SWIM + PS_TYPE_DOWN + PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_SWIMFDOWN		( 0xC6 + PS_TYPE_WAIT + PS_TYPE_SWIM + PS_TYPE_DOWN + PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_SWIMDOWNE		( 0xC7 + PS_TYPE_WAIT + PS_TYPE_SWIM + PS_TYPE_DEMO + PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_SWIMELEC			( 0xC8 + PS_TYPE_WAIT + PS_TYPE_SWIM + PS_TYPE_DOWN + PS_TYPE_HEAD + PS_TYPE_HAND )


#define	PS_SWIMMING1		( 0xD0 + PS_TYPE_MOVE + PS_TYPE_SWIM				+ PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_SWIMMING2		( 0xD1 + PS_TYPE_MOVE + PS_TYPE_SWIM				+ PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_SWIMMING3		( 0xD2 + PS_TYPE_MOVE + PS_TYPE_SWIM				+ PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_SWIMCARY1		( 0xD3 + PS_TYPE_MOVE + PS_TYPE_SWIM				+ PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_SWIMCARY2		( 0xD4 + PS_TYPE_MOVE + PS_TYPE_SWIM				+ PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_SWIMCARY3		( 0xD5 + PS_TYPE_MOVE + PS_TYPE_SWIM				+ PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_KAMESWIM			( 0xD6 + PS_TYPE_MOVE + PS_TYPE_SWIM				+ PS_TYPE_HEAD + PS_TYPE_HAND )

#define	PS_SWIMTHROW		( 0xE0 + PS_TYPE_MOVE + PS_TYPE_SWIM				+ PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_SWIMTAKE			( 0xE1 + PS_TYPE_MOVE + PS_TYPE_SWIM				+ PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_DIVING			( 0xE2 + PS_TYPE_WAIT + PS_TYPE_SWIM				+ PS_TYPE_HEAD + PS_TYPE_HAND )
#define	PS_DRAINPIPE		( 0xE3 + PS_TYPE_WAIT + PS_TYPE_SWIM + PS_TYPE_DOWN + PS_TYPE_HEAD + PS_TYPE_HAND )

#define	PS_WATRNWAIT		( 0xF0 + PS_TYPE_WAIT + PS_TYPE_SINK + PS_TYPE_QUIT )
#define	PS_WATRCWAIT		( 0xF1 + PS_TYPE_WAIT + PS_TYPE_SINK + PS_TYPE_QUIT )
#define	PS_WATRNWALK		( 0xF2 + PS_TYPE_MOVE + PS_TYPE_SINK )
#define	PS_WATRCWALK		( 0xF3 + PS_TYPE_MOVE + PS_TYPE_SINK )
#define	PS_WATRNLAND		( 0xF4 + PS_TYPE_WAIT + PS_TYPE_SINK )
#define	PS_WATRCLAND		( 0xF5 + PS_TYPE_WAIT + PS_TYPE_SINK )
#define	PS_WATRNLNDE		( 0xF6 + PS_TYPE_WAIT + PS_TYPE_SINK )
#define	PS_WATRCLNDE		( 0xF7 + PS_TYPE_WAIT + PS_TYPE_SINK )
#define	PS_WATRNJUMP		( 0xF8 + PS_TYPE_MOVE + PS_TYPE_SINK )
#define	PS_WATRCJUMP		( 0xF9 + PS_TYPE_MOVE + PS_TYPE_SINK )
#define	PS_WATRNJMPE		( 0xFA + PS_TYPE_MOVE + PS_TYPE_SINK )
#define	PS_WATRCJMPE		( 0xFB + PS_TYPE_MOVE + PS_TYPE_SINK )



/* player auto demonstration status code */

#define	PS_FREEZE			( 0x100 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_CAMDEMO			( 0x101 + PS_TYPE_WAIT + PS_TYPE_DEMO + PS_TYPE_VIEW )
#define	PS_WINDEMO			( 0x102 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_WINSWIM			( 0x103 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_WINLAND			( 0x104 + PS_TYPE_JUMP + PS_TYPE_DEMO ) 
#define	PS_DODIALOG			( 0x105 + PS_TYPE_WAIT + PS_TYPE_DEMO + PS_TYPE_HEAD ) 
#define	PS_DLOGMODE			( 0x106 + PS_TYPE_WAIT + PS_TYPE_DEMO + PS_TYPE_HEAD ) 
#define	PS_WINDEMO2			( 0x107 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_READBOARD		( 0x108 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_WINFLIGHT		( 0x109 + PS_TYPE_JUMP + PS_TYPE_DEMO )
#define	PS_WAITDLOG			( 0x10A + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_FREEMOVE			( 0x10F + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_WINDEMOA			PS_WINDEMO


#define	PS_GIDDYDOWN		( 0x111 + PS_TYPE_WAIT + PS_TYPE_DEMO + PS_TYPE_DOWN )
#define	PS_SANDDOWN			( 0x112 + PS_TYPE_WAIT + PS_TYPE_DEMO + PS_TYPE_DOWN )
#define	PS_ELECDOWN			( 0x113 + PS_TYPE_WAIT + PS_TYPE_DEMO + PS_TYPE_DOWN )
#define	PS_GASDOWN			( 0x114 + PS_TYPE_WAIT + PS_TYPE_DEMO + PS_TYPE_DOWN )
#define	PS_FOREDOWNE		( 0x115 + PS_TYPE_WAIT + PS_TYPE_DEMO + PS_TYPE_DOWN )
#define	PS_BACKDOWNE		( 0x116 + PS_TYPE_WAIT + PS_TYPE_DEMO + PS_TYPE_DOWN )
#define	PS_EATEN			( 0x117 + PS_TYPE_WAIT + PS_TYPE_DEMO + PS_TYPE_DOWN )

#define	PS_ENTENDING		( 0x118 + PS_TYPE_JUMP + PS_TYPE_DEMO )
#define	PS_STAFFROLL		( 0x119 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_FINGAME			( 0x11A + PS_TYPE_WAIT + PS_TYPE_DEMO )


#define	PS_OPENDOOR1		( 0x120 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_OPENDOOR2		( 0x121 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_TRIPDOOR			( 0x122 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_CHIMNEY			( 0x123 + PS_TYPE_JUMP + PS_TYPE_DEMO )
#define	PS_ROLLJUMP			( 0x124 + PS_TYPE_JUMP + PS_TYPE_DEMO )
#define	PS_ROLLEND			( 0x125 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_ENTWINNER		( 0x126 + PS_TYPE_JUMP + PS_TYPE_DEMO ) 
#define	PS_ENTWINEND		( 0x127 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_ENTLOSER			( 0x128 + PS_TYPE_JUMP + PS_TYPE_DEMO )
#define	PS_LANDLOSER		( 0x129 + PS_TYPE_JUMP + PS_TYPE_DEMO )
#define	PS_DOWNLOSER		( 0x12A + PS_TYPE_JUMP + PS_TYPE_DEMO )
#define	PS_PUSHOUT			( 0x12B + PS_TYPE_JUMP + PS_TYPE_DEMO )
#define	PS_PUSHDOWN			( 0x12C + PS_TYPE_JUMP + PS_TYPE_DEMO )
#define	PS_LNDWINNER		( 0x12D + PS_TYPE_JUMP + PS_TYPE_DEMO ) 
#define	PS_OPENKDOOR		( 0x12E + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_STARDOOR			( 0x12F + PS_TYPE_WAIT + PS_TYPE_DEMO )

// #define	PS_WALKDOOR			( 0x130 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_AUTODOOR			( 0x131 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_LANDENTER		( 0x132 + PS_TYPE_JUMP + PS_TYPE_DEMO )
#define	PS_LANDENEND		( 0x133 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_JPBASKET			( 0x134 + PS_TYPE_JUMP + PS_TYPE_DEMO )
#define	PS_INBASKET			( 0x135 + PS_TYPE_MOVE + PS_TYPE_DEMO )
#define	PS_WARPIN			( 0x136 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_WARPOUT			( 0x137 + PS_TYPE_WAIT + PS_TYPE_DEMO )

#define	PS_ELECDMGE			( 0x138 + PS_TYPE_WAIT + PS_TYPE_DOWN )
#define	PS_VCOMPRESS		( 0x139 + PS_TYPE_WAIT + PS_TYPE_DOWN )
#define	PS_HEADDOWN			( 0x13A + PS_TYPE_WAIT + PS_TYPE_DOWN )
#define	PS_HIPDOWN			( 0x13B + PS_TYPE_WAIT + PS_TYPE_DOWN )
#define	PS_FOOTDOWN			( 0x13C + PS_TYPE_WAIT + PS_TYPE_DOWN )
#define	PS_PUTONHAT			( 0x13D + PS_TYPE_WAIT + PS_TYPE_DEMO )



/* player special status code */

#define	PS_BARWAIT			( 0x140 + PS_TYPE_WAIT + PS_TYPE_POLE + PS_TYPE_QUIT )
#define	PS_BARHANG			( 0x141 + PS_TYPE_WAIT + PS_TYPE_POLE )
#define	PS_BARJUMP			( 0x142 + PS_TYPE_WAIT + PS_TYPE_POLE )
#define	PS_BARCLIMB			( 0x143 + PS_TYPE_WAIT + PS_TYPE_POLE )
#define	PS_BARSTAND			( 0x144 + PS_TYPE_WAIT + PS_TYPE_POLE )
#define	PS_BARSWAIT			( 0x145 + PS_TYPE_WAIT + PS_TYPE_POLE )

#define	PS_HANGROOF			( 0x148 + PS_TYPE_WAIT + PS_TYPE_ROOF + PS_TYPE_QUIT )
#define	PS_WAITROOF			( 0x149 + PS_TYPE_WAIT + PS_TYPE_ROOF )
#define	PS_MOVEROOF			( 0x14A + PS_TYPE_MOVE + PS_TYPE_ROOF )

#define	PS_HANGING			( 0x14B + PS_TYPE_WAIT + PS_TYPE_WALL + PS_TYPE_QUIT )
#define	PS_ASCEND1			( 0x14C + PS_TYPE_MOVE + PS_TYPE_WALL )
#define	PS_ASCEND2			( 0x14D + PS_TYPE_MOVE + PS_TYPE_WALL )
#define	PS_DESCEND			( 0x14E + PS_TYPE_MOVE + PS_TYPE_WALL )
#define	PS_HANGJUMP			( 0x14F + PS_TYPE_MOVE + PS_TYPE_WALL )

#define	PS_TAKEN			( 0x170 + PS_TYPE_WAIT + PS_TYPE_DOWN )
#define	PS_INCANNON			( 0x171 + PS_TYPE_WAIT + PS_TYPE_DEMO )
#define	PS_TORNEDO			( 0x172 + PS_TYPE_WAIT + PS_TYPE_DOWN + PS_TYPE_HAND )



/* player action status code */

#define	PS_ATTACK			( 0x180 + PS_TYPE_WAIT + PS_TYPE_ATCK )

#define	PS_TAKING			( 0x183 + PS_TYPE_WAIT )
#define	PS_CATCHEND			( 0x185 + PS_TYPE_WAIT )
#define	PS_CATCHLOST		( 0x186 + PS_TYPE_WAIT )

#define	PS_PUTTING			( 0x187 + PS_TYPE_WAIT )
#define	PS_PITCHING			( 0x188 + PS_TYPE_MOVE + PS_TYPE_THRW )
#define	PS_THROWING			( 0x189 + PS_TYPE_MOVE + PS_TYPE_THRW )

#define	PS_BRANDSTAT		( 0x190 + PS_TYPE_WAIT )
#define	PS_BRANDISH			( 0x191 + PS_TYPE_WAIT )
#define	PS_BRANDEND			( 0x192 + PS_TYPE_WAIT )


/********************************************************************************/
/*																				*/
/*	Player control record														*/
/*																				*/
/********************************************************************************/

typedef struct {
	ushort			number;				/* player ID number						*/
	ushort			action;				/* player action						*/
	ulong			flags;				/* player flags							*/
	ulong			result;				/* player result						*/
	ulong			status;				/* player status						*/
	ulong			oldstatus;			/* player status at the previous frame	*/
	ulong			surface;			/* ground surface condition for sound	*/

	ushort			process;			/* allpurpose process number			*/
	ushort			counter;			/* allpurpose counter					*/
	ulong			auxinfo;			/* auxiliary status information			*/

	float			powLevel;			/* motion power level (temp)			*/
	short			powAngle;			/* motion power angle (temp)			*/

	short			invincible;			/* invincible counter					*/
	uchar			jmpTrgcnt;			/* jump button trigger counter			*/
	uchar			actTrgcnt;			/* action button triger counter			*/
	uchar			walljump;			/* wall jump							*/
	uchar			tripjump;			/* triple jump							*/


	SVector			angle;				/* player angle							*/
	SVector			angvelo;			/* angle velocity for swimming			*/
	short			slideAng;			/* player slide angle					*/
	short			spinAngle;			/* spin jumping angle					*/

	FVector			position;			/* player position						*/
	FVector			speed;				/* player speed (vector)				*/
	float			velocity;			/* player velocity						*/
	float			slidex;				/* player slide speed x					*/
	float			slidez;				/* player slide speed z					*/


	BGCheckData	   *wlPlane;			/* pointer to the wall plane record		*/
	BGCheckData	   *rfPlane;			/* pointer to the roof plane record		*/
	BGCheckData	   *grPlane;			/* pointer to the ground plane record	*/
	float			rfLevel;			/* roof level							*/
	float			grLevel;			/* ground level							*/
	short			grAngle;			/* ground angle y						*/
	short			water;				/* water level							*/

	StrategyPtr		collide;			/* pointer to the collided object		*/ 
	StrategyPtr		taking;				/* pointer to the taken object			*/
	StrategyPtr		couple;				/* pointer to the coupling object		*/
	StrategyPtr		skateboard;			/* pointer to the skate board			*/

	StrategyPtr		strategy;			/* pointer to the strategy record		*/
	ActorPtr		actorinf;			/* pointer to the actor information		*/
	ScenePtr		sceneinf;			/* pointer to the scene information		*/
	CamPLInfoPtr	playerinf;			/* pointer to the player information	*/
	PLShapeCtrl	   *shapeCtrl;			/* pointer to the player shape control	*/
	Controller	   *control;			/* pointer to the controller			*/
	PartialPtr		partial;

	ulong			hitnames;			/* hit enemy names						*/
	short			ncoins;				/* number of the coins					*/
	short			nstars;				/* number of the stars					*/
	char			nkeys;				/* number of the keys					*/
	char			nlifes;				/* number of the lifes					*/
	short			npowers;			/* number of the powers					*/

	short			animbase;			/* skeleton animation base height		*/
	uchar			damage;				/* player power down					*/
	uchar			recover;			/* player power recover					*/
	uchar			compress;			/* player shape vertical compress value	*/ 
	uchar			alpha;				/* player shape alpha value				*/
	ushort			special;			/* special mario counter				*/
	short			starmsg;			/* number of stars for star message		*/
	ushort			reserved2;
	float			fallpos;			/* player falled from this height		*/
	float			sinking;			/* player sink depth					*/
	float			gravity;			/* player jumping gravity				*/

} PlayerRecord, *PlayerPtr;


/********************************************************************************/
/*																				*/
/*	Meter control record														*/
/*																				*/
/********************************************************************************/

#define	METER_LIFE		0x0001
#define	METER_COIN		0x0002
#define	METER_STAR		0x0004
#define	METER_POWER		0x0008
#define	METER_KEY		0x0010
#define	METER_MAP		0x0020
#define	METER_TIMER		0x0040
#define	METER_ALL		0x007F

#define	METER_DAMAGE	0x8000

typedef struct {
	short	life;						/* number of Mario's life				*/
	short	coin;						/* number of the coins					*/
	short	star;						/* number of the stars					*/
	short	power;						/* number of the powers					*/
	short	key;						/* number of the keys					*/
	short	flags;						/* meter switch							*/
	ushort	timer;						/* timer								*/
} MeterRecord, *MeterPtr;


/********************************************************************************/
/*																				*/
/*	External variables															*/
/*																				*/
/********************************************************************************/

extern PlayerRecord playerWorks[1];			/* player control works				*/
extern PlayerRecord *marioWorks;			/* pointer to the mario works		*/
extern MeterRecord	playerMeter;			/* player meter record				*/

extern char			mesgCastle;

/********************************************************************************/
/*																				*/
/*	Audio macros																*/
/*																				*/
/********************************************************************************/

#define	PL_StartSound(player,code)				PL_StartSoundEffect(player,code,PL_FLAG_SOUNDTRIG)
#define	PL_StartVoice(player,code)				PL_StartSoundEffect(player,code,PL_FLAG_VOICETRIG)
#define	PL_StartMarioPant(player)				Na_MarioPant(&(player)->strategy->map.viewCoord[0])


/********************************************************************************/
/*																				*/
/*	Macros																		*/
/*																				*/
/********************************************************************************/

#define	PL_IsGhostMario(player)				((player)->flags & PL_FLAG_GHOSTCAP)
#define	PL_IsMetalMario(player)				((player)->flags & PL_FLAG_METALCAP)
#define	PL_IsWingMario(player)				((player)->flags & PL_FLAG_WINGCAP)


#define	PL_AnimeFrame(player)			((player)->strategy->map.skelanim.frame)
#define	PL_IsAnimeFrame(player,n)		((player)->strategy->map.skelanim.frame==(n))
#define	PL_GetAnimeID(player)			((player)->strategy->map.skelanim.animeID)
#define	PL_ResetAnimation(player)		((player)->strategy->map.skelanim.animeID = -1)

#define	PL_GetEnvironment(player)		((player)->sceneinf->environment&ENV_CODEMASK)
#define	PL_CheckEnvironment(player,env)	(((player)->sceneinf->environment&ENV_CODEMASK)==(env))

#define	PL_ClearViewCamera()			(gameCameraControl &= ~CAM_FLAG_VIEWMODE)
#define	PL_GameOverRequest(n)			(gameOverRequest = (n))

#define	PL_SetPlayerCap(player,n)		((player)->shapeCtrl->ctrlCap =(n))
#define	PL_SetPlayerEye(player,n)		((player)->shapeCtrl->ctrlEye =(n))
#define	PL_SetPlayerHand(player,n)		((player)->shapeCtrl->ctrlHand=(n))
#define	PL_SetPlayerSkin(player,n)		((player)->shapeCtrl->ctrlSkin=(n))

#define	PL_SetOriginalCameraMode(player)	\
				ChangeGameCamera((player)->sceneinf->camera, (player)->sceneinf->camera->orgmode, 1)


#define	RecoverMarioPower(n)			(playerWorks[0].recover+=(n)*4)


/********************************************************************************/
/*																				*/
/*	Ending routines.															*/
/*																				*/
/********************************************************************************/

extern void
DrawStaffRoll(void);

extern void
DoPeachProcess(void);

extern void
DoKinopioProcess(void);

extern ulong
CtrlPeachFace(int code, MapNode *node, void *data);

/********************************************************************************/
/*																				*/
/*	Player animation suport routines.											*/
/*																				*/
/********************************************************************************/

extern int
PL_IsLast1AnimeFrame(PlayerPtr player);

extern int
PL_IsLast2AnimeFrame(PlayerPtr player);

extern int
PL_SetAnimation(PlayerPtr player, int anime_id);

extern int
PL_SetVariableAnime(PlayerPtr player, int anime_id, long speed);

extern void
PL_SetAnimeFrame(PlayerPtr player, short frame);

extern int
PL_CheckAnimeFrame(PlayerPtr player, short frame);

extern int
PL_PresetWaistAnime(PlayerPtr player);

extern short
PL_GetWaistAnimeY(PlayerPtr player);

extern short
PL_GetWaistTranslation(StrategyPtr stratp, short angle, SVector position);

/********************************************************************************/
/*																				*/
/*	Change player status routines.												*/
/*																				*/
/********************************************************************************/

extern int
PL_ChangePlayerTriJump(PlayerPtr player);

extern int
PL_ChangePlayerJumping(PlayerPtr player, ulong status, ulong param);

extern int
PL_ChangePlayerTaking(PlayerPtr player, ulong status, ulong param);

extern int
PL_ChangePlayerDropping(PlayerPtr player, ulong status, ulong param);

extern int
PL_ChangePlayerDamage(PlayerPtr player, ulong status, ulong param, short damage);

extern int
PL_CheckAllMotions(PlayerPtr player);

extern int
PL_CheckAllHoldMotions(PlayerPtr player);

extern int
PL_ChangeFieldMode(PlayerPtr player);

extern int
PL_ChangeWaterMode(PlayerPtr player);

extern int
PL_ChangePlayerStatus(PlayerPtr player, ulong status, ulong auxinfo);


/********************************************************************************/
/*																				*/
/*		Player control utility routines.										*/
/*																				*/
/********************************************************************************/

extern void
PL_StartSoundEffect(PlayerPtr player, ulong code, ulong type);

extern void
PL_StartJumpVoice(PlayerPtr player);

extern void
PL_SetSpeedVolum(PlayerPtr player);

extern void
PL_SpecialEffect(PlayerPtr player, ulong sound, int jumpsink);

extern void
PL_TrigSpecialEffect(PlayerPtr player, ulong sound, int jumpsink);

extern void
PL_StartLandingEffect(PlayerPtr player, ulong sound);

extern void
PL_TrigStartLandingEffect(PlayerPtr player, ulong sound);

extern void
PL_StartDowningEffect(PlayerPtr player, ulong sound);

extern void
PL_TrigStartDowningEffect(PlayerPtr player, ulong sound);

extern void
PL_StartJumpingEffect(PlayerPtr player, ulong sound, ulong voice);



extern void
PL_SetPlayerVelocity(PlayerPtr player, float speed);

extern int
PL_GetSlipCode(PlayerPtr player);

extern ulong
PL_CheckGroundSurface(PlayerPtr player);

extern BGCheckData *
PL_CheckWallPlane(FVector position, float offset, float radius);

extern float
PL_CheckRoofPlane(FVector position, float ground, BGCheckData **plane);

extern int
PL_IsFrontSlip(PlayerPtr player, int backflag);

extern int
PL_IsSlipStart(PlayerPtr player);

extern int
PL_IsSlipLimit(PlayerPtr player);

extern int
PL_IsJumpMiss(PlayerPtr player);

extern float
PL_CheckPlayerAround(PlayerPtr player, short angle, float dist);

extern short
PL_GroundGradient(PlayerPtr player, short offset);

extern void
PL_ResetSpecialCameraMode(PlayerPtr player);

extern int
PL_AttackProcess(PlayerPtr player);

extern int
CheckPlayerDialog(void);

extern int
CtrlPlayerDialog(int type);

extern short
PL_PlayerRunningSound(PlayerPtr player, short frame1, short frame2);

/********************************************************************************/
/*																				*/
/*	Player main routines.														*/
/*																				*/
/********************************************************************************/

extern int
PL_PlayerWaitMain(PlayerPtr player);

extern int
PL_PlayerMoveMain(PlayerPtr player);

extern int
PL_PlayerJumpMain(PlayerPtr player);

extern int
PL_PlayerSwimMain(PlayerPtr player);

extern int
PL_PlayerDemoMain(PlayerPtr player);

extern int
PL_PlayerSpecMain(PlayerPtr player);

extern int
PL_PlayerActnMain(PlayerPtr player);

extern ulong
PlayerControl(StrategyPtr strategy);

extern void
InitPlayer(void);

extern void
InitPlayerWorks(void);


/********************************************************************************/
/*																				*/
/*	Include files.																*/
/*																				*/
/********************************************************************************/

#include "physics.h"
#include "collision.h"
#include "game.h"

#endif
