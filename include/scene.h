/********************************************************************************
	scene.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	April 26, 1996
 ********************************************************************************/

#ifndef SCENE_H
#define	SCENE_H


#define	SN_NUMSCRNS			2
#define	SN_NUMSCENES		8
#define	SN_NUMSHAPES		256

#define	SN_TRIP_DOOR		0x01
#define	SN_TRIP_STAR		0x02
#define	SN_TRIP_CHIMNEY		0x03
#define	SN_WARP_POINT		0x04

#define	SN_ENTER_WAITING	0x10
#define	SN_ENTER_SWIMMING	0x11
#define	SN_ENTER_LANDING	0x12
#define	SN_ENTER_FALLING	0x13
#define	SN_ENTER_ROLLING	0x14
#define	SN_ENTER_DOWNING	0x15
#define	SN_ENTER_PICTURE	0x16
#define	SN_ENTER_FLIGHT		0x17

#define	SN_ENTER_WINNER		0x20
#define	SN_ENTER_LOSER		0x21
#define	SN_ENTER_LANDWINNER	0x22
#define	SN_ENTER_LANDLOSER	0x23
#define	SN_ENTER_PUSHOUT	0x24
#define	SN_ENTER_PUSHDOWN	0x25
#define	SN_ENTER_ENDING		0x26
#define	SN_ENTER_STAFFROLL	0x27


#define	PORT_GAME_CLEAR		0xf0
#define	PORT_GAME_OVER		0xf1
#define	PORT_VIEW_ROOF		0xf2
#define	PORT_COURSE_OUT		0xf3
#define	PORT_START_ENDING	0xF8
#define	PORT_STAFF_ROLL		0xF9
#define	PORT_FIN_ENDING		0xFA


/*-------------------------------------------------------------------------------
 *	wipe data record.
 */
typedef struct {
	uchar	stage;						/* display stage number					*/
	uchar	scene;						/* display scene number					*/
	uchar	info;						/* display information					*/
	char	angle;						/* player angle							*/
	short	posx;						/* player position x					*/
	short	posy;						/* player position y					*/
	short	posz;						/* player position z					*/
	String	*staff;						/* pointer to the staff string			*/
} EndingRecord, *EndingPtr;

/*-------------------------------------------------------------------------------
 *	wipe data record.
 */
typedef union {
	WipeScreenData	screen;
	WipeWindowData	window;
} SnWipeData;


/*-------------------------------------------------------------------------------
 *	wipe control record.
 */
typedef struct {
	uchar		active;					/* wipe active flag						*/
	uchar		mode;					/* wipe mode							*/
	uchar		frame;					/* wipe frame							*/
	uchar		blank;					/* force software blanking counter		*/
	SnWipeData	params;					/* wipe data record						*/
} SnWipeCtrl, *SnWipePtr;


/*-------------------------------------------------------------------------------
 *	wind zone record.
 */
typedef struct {
	short	posx1;						/* wind zone position x1				*/
	short	posz1;						/* wind zone position z1				*/
	short	posx2;						/* wind zone position x2				*/
	short	posz2;						/* wind zone position z2				*/
	short	angle;						/* wind direction						*/
} WindZoneRecord, *WindZonePtr;


/*-------------------------------------------------------------------------------
 *	water jet record.
 */
typedef struct {
	SVector	position;					/* water jet position					*/
	short	force;						/* water jet force						*/
} WaterJetRecord, *WaterJetPtr;


/*-------------------------------------------------------------------------------
 *	Scene connect record.
 */

typedef struct {
	uchar		flags;					/* flags								*/
	uchar		scene;					/* trip scene number					*/
	SVector		offset;					/* scene connection offset				*/
} ConnectRecord, *ConnectPtr;


/*-------------------------------------------------------------------------------
 *	BG port record.
 */

typedef struct {
	uchar		flags;					/* port flag							*/
	uchar		stage;					/* trip stage number					*/
	uchar		scene;					/* trip scene number					*/
	uchar		port;					/* trip port number						*/
} BGPortRecord, *BGPortPtr;


/*-------------------------------------------------------------------------------
 *	scene trip port record.
 */

typedef struct Port {
	uchar		number;					/* port number							*/
	uchar		stage;					/* trip stage number					*/
	uchar		scene;					/* trip scene number					*/
	uchar		port;					/* trip port number						*/
	StrategyPtr	stratp;					/* pointer to the strategy record		*/
	struct Port	*next;					/* pointer to the next port record		*/
} PortRecord, *PortPtr;


/*-------------------------------------------------------------------------------
 *	Entrant record.
 */

typedef struct {
	uchar		type;					/* enter type							*/
	uchar		stage;					/* enter stage number					*/
	uchar		scene;					/* enter scene number					*/
	uchar		port;					/* enter port number					*/
	ulong		auxinfo;				/* auxiliary information				*/
} EntrantRecord, *EntrantPtr;


/*-------------------------------------------------------------------------------
 *	scene information record.
 */
typedef struct {
	char			sceneNo;			/* scene number							*/
	char			entrant;			/* player entrant flag					*/
	ushort			environment;		/* scene environment					*/
	MapScene		*mapScene;			/* pointer to the map scene				*/ 
	short			*mapInfo;			/* pointer to the map information		*/
	char			*areaInfo;			/* pointer to the area information		*/
	short			*tagInfo;			/* pointer to the tag information		*/
	PortRecord		*ports;				/* pointer to the first port record		*/
	BGPortRecord	*bgport;			/* pointer to the BG port record		*/
	ConnectRecord	*connect;			/* pointer to the connect record		*/
	ActorRecord 	*actors;			/* pointer to the first sctor record	*/
	CameraRecord	*camera;			/* pointer to the camera work			*/
	WindZoneRecord	*windzone;			/* pointer to the wind zone record		*/
	WaterJetRecord	*waterjet[2];		/* pointer to the water jet record		*/
	uchar			message[2];			/* message number						*/
	ushort			audmode;			/* audio mode							*/
	ushort			musicno;			/* music number							*/
	ushort			pad;
} SceneRecord, *ScenePtr;


/********************************************************************************/
/*	Global variables.															*/
/********************************************************************************/

extern MapNode	   *shapeList[SN_NUMSHAPES];	/* shape list memory			*/
extern SceneRecord  sceneList[SN_NUMSCENES];	/* scene list memory			*/

extern MapNode	   **stageShapes;				/* pointer to the shape list 	*/
extern SceneRecord  *stageScenes;				/* pointer to the scene list 	*/

extern ScenePtr	   snSceneInfo;		/* pointer to the active scene information	*/
extern EndingPtr   snEndingScene;	/* pointer to the ending scene information	*/
extern StrategyPtr snMagnetInfo;	/* pointer to the magnet strategy record	*/
extern SnWipeCtrl  wipeControl;		/* wipe control record						*/

extern short	activePlayerNo;				/* active player number				*/
extern short	activeCourseNo;				/* active course number				*/
extern short	activeLevelNo;				/* active level number				*/
extern short	activeStageNo;				/* active stage number				*/
extern short	activeSceneNo;				/* active scene number				*/
extern short	ramSaveCourse;				/* active player number				*/

extern short	mesgEvent;					/* result of the message event		*/
extern short	mesgLatch;					/* latch message event				*/

extern ActorRecord	starringActor[1];
extern ActorRecord	*marioActor;





/********************************************************************************/
/*																				*/
/*	Trip port supprt routines.													*/
/*																				*/
/********************************************************************************/

#define	SnGetPortNumber(stratp)		(((stratp)->s[stw_actorcode].d & 0x00ff0000) >> 16)
#define	SnGetStarNumber(stratp)		(((stratp)->s[stw_actorcode].d & 0xff000000) >> 24)


extern int
SnGetPortType(StrategyPtr stratp);

extern PortPtr
SnGetPortPtr(uchar code);

extern PortPtr
SnFindPortPtr(StrategyPtr stratp);


/********************************************************************************/
/*	Function prototype definition.												*/
/********************************************************************************/

extern void
SnSetViewPort(Vp *viewing, Vp *trimming, uchar red, uchar green, uchar blue);

extern void
SnInitSceneInfo(void);

extern void
SnDisposeScene(void);

extern void
SnOpenScene(int sceneNo);

extern void
SnCloseScene(void);

extern void
SnEnterPlayer(void);

extern void
SnExitPlayer(void);

extern void
SnChangeScene(int newScene);

extern void
SnExecuteStrategy(void);

// extern void
// SnSetBlankColor(ushort color);

extern void
SnDisplayDemoMessage(void);

extern void
SnStartFader(short mode, short frame, uchar red, uchar green, uchar blue);

extern void
SnDelayFader(short mode, short frame, uchar red, uchar green, uchar blue, short delay);

extern void
SnDrawScreen(void);


#include "backup.h"

#endif
