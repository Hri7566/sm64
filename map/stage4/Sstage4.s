/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 4 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							  Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage04


/* ====================================================================================	
		: BG Parts sequence.
=======================================================================================	*/
SEQ_Stage04_BGParts:

		seqActor(S_fireball_yellow,   2089,1331,-1125,   0,270,0,   0,0,0,   e_fire_animation)
		seqActor(S_fireball_yellow,   1331,1075,-1330,   0, 90,0,   0,0,0,   e_fire_animation)
		seqActor(S_fireball_yellow,   2089,1331, -511,   0,270,0,   0,0,0,   e_fire_animation)

		seqActor(S_fireball_yellow,   -511, 358,-1330,   0, 90,0,   0,0,0,   e_fire_animation)

		seqActor(S_fireball_yellow,   1126, 358, 2212,   0,  0,0,   0,0,0,   e_fire_animation)
		seqActor(S_fireball_yellow,    205, 358, 2212,   0,  0,0,   0,0,0,   e_fire_animation)

		seqReturn


/* ====================================================================================	
		: Move BGs sequence.
=======================================================================================	*/
SEQ_Stage04_MoveBGs:

		seqActor(S_movebg01,   2866,  820,1897,  0,  0,0,  0,0,0,  e_b_teretrap)
		seqActor(S_movebg02,   2961,    0,-768,  0,  0,0,  0,0,0,  e_d_teretrap)
		seqActor(S_movebg04,  -1994,  819, 213,  0,  0,0,  0,0,0,  e_f_teretrap)
		seqActor(S_movebg05,  -2985, -205,5400,  0,-45,0,  0,0,0,  e_l_teretrap) 
		seqActor(S_movebg06,   -205,-2560, 205,  0,  0,0,  0,0,0,  e_j_teretrap)
		seqActor(S_NULL,       2200,  819,-800,  0,  0,0,  0,0,0,  e_kanoke)

		seqReturn


/* ====================================================================================	
		: Enemies sequence.
=======================================================================================	*/
SEQ_Stage04_Enemies:

		/* Level 1 */
		seqLevelActor(0x0001,S_teresa,   1000,   50,  1000,  0,0,0,  0,0,0,  e_kaidanteresa	 )
		seqLevelActor(0x0001,S_teresa,     20,  100,  -908,  0,0,0,  0,0,0,  e_miniteresa	 )
		seqLevelActor(0x0001,S_teresa,   3150,  100,   398,  0,0,0,  0,0,0,  e_miniteresa	 )
		seqLevelActor(0x0001,S_teresa,  -2000,  150,  -800,  0,0,0,  0,0,0,  e_miniteresa	 )
		seqLevelActor(0x0001,S_teresa,   2851,  100,  2289,  0,0,0,  0,0,0,  e_miniteresa	 )
		seqLevelActor(0x0001,S_teresa,  -1551,  100, -1018,  0,0,0,  0,0,0,  e_miniteresa	 )

		/* Level 2 */
		seqLevelActor(0x003e,S_movebg00,  973,    0,   517,  0,0,0,   0,0,0, e_teresa_kaidan )
		seqLevelActor(0x003e,S_movebg00,  973, -206,   717,  0,0,0,   0,0,0, e_teresa_kaidan )
		seqLevelActor(0x003e,S_movebg00,  973, -412,   917,  0,0,0,   0,0,0, e_teresa_kaidan )
		seqLevelActor(0x003e,S_teresa,     20,  100,  -908,  0,0,0,   0,0,0, e_normal_teresa )
		seqLevelActor(0x003e,S_teresa,   3150,  100,   398,  0,0,0,   0,0,0, e_normal_teresa )
		seqLevelActor(0x003e,S_teresa,  -2000,  150,  -800,  0,0,0,   0,0,0, e_normal_teresa )
		seqLevelActor(0x003e,S_teresa,   2851,  100,  2289,  0,0,0,   0,0,0, e_normal_teresa )
		seqLevelActor(0x003e,S_teresa,  -1551,  100, -1018,  0,0,0,   0,0,0, e_normal_teresa )
		seqLevelActor(0x003e,S_NULL,   	  990,-2146,  -908,  0,-45,0, 0,3,0, e_firebigbar    )	/* BF firebig bar */
		seqLevelActor(0x003e,S_NULL,    -1100,-2372,  1100,  0,135,0, 1,0,0, e_5teresa       )	/* BF BOSS teresa */

		/* All Level */
		seqLevelActor(0x003f,S_teresa,   1030, 1922,  2546,  0,-90,0,   4,0,0, e_3Fteresa)		/* 3F BOSS teresa */
		seqLevelActor(0x003f,S_teresa,    581, 1850,  -206,  0,-90,0,   0,0,0, e_normal_teresa)	/* 3F normal teresa	*/ 

		seqActor(S_piano,     -1300,    0, 2310,  0,243,0,  0,0,0,  e_piano)
		seqActor(S_chair,     -1530,    0, 2200,  0, 66,0,  0,0,0,  e_chair)

		seqActor(S_NULL,      -1330,  890,  200,  0, 90,0,  0,0,0,  e_book_shot)
		seqActor(S_NULL,       -818,  890, -200,  0,270,0,  0,0,0,  e_book_shot)
		seqActor(S_NULL,      -1330,  890, -622,  0, 90,0,  0,0,0,  e_book_shot)
		seqActor(S_NULL,       -818,  890, -686,  0,270,0,  0,0,0,  e_book_shot)
		seqActor(S_NULL,      -1950,  880,    8,  0,180,0,  0,0,0,  e_book_boss)

		seqActor(S_booklight,  2680, 1045,  876,  0,166,0,  0,0,0,  e_book)
		seqActor(S_booklight,  3075, 1045,  995,  0,166,0,  0,0,0,  e_book)
		seqActor(S_booklight, -1411,  218,  922,  0,180,0,  0,0,0,  e_book)

		seqReturn


/* ====================================================================================	
		: Stars sequence.
=======================================================================================	*/
SEQ_Stage04_Stars:

		seqLevelActor(0x003f,S_polystar, -2030, 1350, 1940,  0, 0,0,  2,0,0,  e_tripstar)  			/* hondana oku*/
		seqLevelActor(0x003f,S_NULL    ,  -204, 1100, 1576,  0, 0,0,  3,0,0,  e_tripstar_getcoins)	/* 8_coin */
/*		seqLevelActor(0x003f,S_polystar,   690, 2200, 2500,  0, 0,0,  4,0,0,  e_tripstar)  			/* beranda */
		seqLevelActor(0x003f,S_NULL    ,   923, 1741, -332,  0,18,0,  5,1,0,  e_balloon	)

		seqReturn


/* ====================================================================================	
		: Stage 4 [ Teresa ] main sequence.
=======================================================================================	*/
SEQ_DoStage04:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1, 	 _GfxStage4SegmentRomStart, 	 _GfxStage4SegmentRomEnd	 )
	seqLoadPres(SEGMENT_BACK	  ,  _BackBigWorldSegmentRomStart  , _BackBigWorldSegmentRomEnd	 )
	seqLoadText(SEGMENT_TEXTURE,	 _BHouseTextureSegmentRomStart,  _BHouseTextureSegmentRomEnd )
	seqLoadPres(SEGMENT_ENEMY1	  ,  _GfxEnemy1_iSegmentRomStart   , _GfxEnemy1_iSegmentRomEnd	 )
	seqLoadData(SEGMENT_ENEMYDATA1,  _HmsEnemy1_iSegmentRomStart   , _HmsEnemy1_iSegmentRomEnd	 )
	seqLoadPres(SEGMENT_ENEMY2	  ,  _GfxEnemy2_hSegmentRomStart   , _GfxEnemy2_hSegmentRomEnd	 )
	seqLoadData(SEGMENT_ENEMYDATA2,  _HmsEnemy2_hSegmentRomStart   , _HmsEnemy2_hSegmentRomEnd	 )
	seqLoadPres(SEGMENT_ENEMY3	  ,  _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3,  _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_i)
		seqCall(SetEnemy2_h)

		seqHmsShape(S_door1, RCP_HmsDoor5)

		seqHmsShape(S_movebg00, RCP_HmsTere_a_trap	)
		seqHmsShape(S_movebg01, RCP_HmsTere_b_trap	)
		seqHmsShape(S_movebg02, RCP_HmsTere_d_trap_a)
		seqHmsShape(S_movebg03, RCP_HmsTere_d_trap_b)
		seqHmsShape(S_movebg04, RCP_HmsTere_f_trap	)
		seqHmsShape(S_movebg05, RCP_HmsTere_l_trap	)
		seqHmsShape(S_movebg06, RCP_HmsTere_j_trap	)
		seqHmsShape(S_movebg07, RCP_HmsTeresaKanoke )

		seqBeginScene(1, RCP_Stage4Scene1)
			seqCall(SEQ_Stage04_BGParts)
			seqCall(SEQ_Stage04_MoveBGs)
			seqCall(SEQ_Stage04_Enemies)
			seqCall(SEQ_Stage04_Stars)
			seqActor(S_NULL,  666,-204+START_H,5350,  0,180,0,  0,10,0,	e_player_entpict)
			seqPort(10, 4, 1, 10)			/*	Mario stage in.	*/
			seqGameClear(26, 1, 10)
			seqGameOver (26, 1, 11)
			seqMapInfo(cx0401_info)
			seqTagInfo(cx0401_info_tag)
			seqAreaInfo(cx0401_area)
			seqMessage(SEQ_MESG_ENTRANT, 98)
			seqSetMusic(NA_STG_OBAKE, NA_OBAKE_HOUSE_BGM)
			seqEnvironment(ENV_HOUSE)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 180, 666,-204,5350)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
