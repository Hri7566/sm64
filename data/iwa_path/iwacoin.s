/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
				Iwamoto Coin Strategy

				Junuary 25 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/*==================================================================================================
		Path Data 
 ==================================================================================================*/

/********************************************************************************/
/*		Move Coin path Program											        */
/********************************************************************************/
e_move_coin:
	p_initialize(item)
	p_setbit(flag,stf_moveON)
	p_softspritemodeON
	p_sethitbox(100,64)
	p_setname(COIN)
	p_hitON
	p_animereset
	p_program(s_move_coin_init)

	p_while
		p_program(s_move_coin_event)
		p_animeinc
	p_loop

/********************************************************************************/
/*		Slider Coin path Program											    */
/********************************************************************************/
e_slider_coin:
	p_initialize(item)
	p_setbit(flag,stf_moveON)
	p_softspritemodeON
	p_hitON
	p_animereset
	p_program(s_slider_coin_init)

	p_while
		p_program(s_slider_coin_event)
		p_animeinc
	p_loop

/********************************************************************************/
/*		Escape Coin															    */
/********************************************************************************/
e_escape_coin:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON | stf_playerdistON | stf_playerangleON)
	p_softspritemodeON
	p_hitON
	p_animereset
	p_program(s_escape_coin_init)

	p_while
		p_program(s_escape_coin_main)
		p_animeinc
	p_loop
/********************************************************************************/
/*		Escape Coin															    */
/********************************************************************************/
e_escape_coin2:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON | stf_playerdistON | stf_playerangleON)
	p_softspritemodeON
	p_hitON
	p_animereset
	p_program(s_escape_coin_init)

	p_while
		p_program(s_escape2_coin_main)
		p_animeinc
	p_loop


/*################*/
#else
/*################*/
/*==================================================================================================
		C Program
 ====================================================================================================*/

static HitCheckRecord coin_hit = {
	OBJNAME_COIN,
	  0,1,0,0,		/* flag,ap,hp,coin 		 */
	100, 64,		/* player damage hitarea */
	  0,  0,		/* player attack hitarea */
};

/***************************************************************************************************
						Remove Coin for Catch 
***************************************************************************************************/
static char  CoinBound(short* move_flag)
{
	*move_flag = ObjMoveEvent();

	ObjDangerCheck(*move_flag,moveobj_bgcp);


	if ((*move_flag & 0x0001) && (*move_flag & 0x0008)==0) {
		objsound(NA_SE2_COINBOUND); 
		return(1);
	}


	return(0);

}
/***************************************************************************************************
						Remove EscapeCoin
***************************************************************************************************/
static void RemoveCoin(void)
{

	short move_flag;

	CoinBound(&move_flag); 	

	iwa_TimerRemove(execstp,0);
}
/***************************************************************************************************
						Remove Coin for Catch 
***************************************************************************************************/
static void RemoveCoinCatch(void)
{

	s_makeobj_nowpos(execstp,S_spark,e_coinspark);

	execstp->status = 0;

}
/***************************************************************************************************
						Move Coin Event
***************************************************************************************************/
extern void s_move_coin_init(void)
{

	execstp->s[stw_gravity].f   = 3.0;
	execstp->s[stw_friction].f  = 1.0;
	execstp->s[stw_specificG].f = 1.5;

	s_set_hitparam(execstp,&coin_hit);		/* Set HitCheck Param */

}
/***************************************************************************************************
						Move Coin Event
***************************************************************************************************/
extern void s_move_coin_event(void)
{
	short move_flag;

	switch (obj_mode) {
		case 0:	CoinBound(&move_flag);
				if (obj_timer < 10) s_hitOFF();
				else s_hitON();
				if (obj_timer > 10*FRAME) { 
					obj_mode = 1; 
				}
				break;

		case 1: RemoveCoin();
				break;

		case OBJ_MELT : execstp->status = 0;
						break;
		case OBJ_AIRDEAD : execstp->status = 0; 
						   break;
	}


	if ( obj_mail & EMAIL_PLAYERHIT ){
		RemoveCoinCatch();
		Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
	}

}

/*==================================================================================================
/*					Slider Coin																		*/
/*==================================================================================================
/*==================================================================================================*/

static HitCheckRecord escapecoin_hit = {
	OBJNAME_COIN,
	  0,5,0,0,		/* flag,ap,hp,coin 		 */
	100, 64,		/* player damage hitarea */
	  0,  0,		/* player attack hitarea */
};

/***************************************************************************************************
						Move Coin Event
***************************************************************************************************/
extern void s_slider_coin_init(void)
{

	execstp->s[stw_gravity].f   = 5.0;
	execstp->s[stw_friction].f  = 1.0;
	execstp->s[stw_specificG].f = 1.5;

	s_set_hitparam(execstp,&escapecoin_hit);		/* Set HitCheck Param */

}
/***************************************************************************************************
						Slider Coin Event
***************************************************************************************************/
extern void s_slider_coin_event(void)
{
	short move_flag;

	switch (obj_mode) {
		case 0: if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,1500)) {
					obj_mode = 1;
				}
				break;

		case 1: move_flag = ObjMoveEvent();
				if ((move_flag & 0x01)) {
					 execstp->s[stw_speedF].f += 25;
					 if((move_flag & 0x08)==0) objsound(NA_SE2_COINBOUND);
				}
				else execstp->s[stw_speedF].f *= 0.98;
		
				if (execstp->s[stw_speedF].f > 75.0) execstp->s[stw_speedF].f = 75.0;
				iwa_TimerRemove(execstp,FRAME*20);
				break;
	}

	if ( obj_mail & EMAIL_PLAYERHIT ){
		RemoveCoinCatch();
		Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
	}

}

/*==================================================================================================*/
/*		Escape Coin Event																			*/
/*==================================================================================================*/



/***************************************************************************************************
						Escape Coin Init
***************************************************************************************************/
extern void s_escape_coin_init(void)
{

	execstp->s[stw_gravity].f   = 3.0;
	execstp->s[stw_friction].f  = 0.98;
	execstp->s[stw_specificG].f = 1.5;

	s_set_hitparam(execstp,&escapecoin_hit);		/* Set HitCheck Param */

}
/***************************************************************************************************
						Main
***************************************************************************************************/
static void EscapeFromPlayer(void)
{
	short move_flag;

	obj_speedF = 15;
	obj_angleY = obj_targetangle + 0x8000;


	if (CoinBound(&move_flag)) { obj_speedY += 18;	}

	if (move_flag & 0x0002) {
		 obj_mode = 3;
	}	


	if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,1000)==0) obj_mode = 2;	

}
/***************************************************************************************************
						Wait Player 
***************************************************************************************************/
static void WaitForPlayer(void)
{
	
	short move_flag;

	CoinBound(&move_flag); 	

	if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,500)==1) obj_mode = 1;	
	if (obj_timer > FRAME*5) { obj_mode = 3;} 

}
/***************************************************************************************************
						Escape Coin Main
***************************************************************************************************/
extern void s_escape_coin_main(void)
{
	short move_flag;

	switch(obj_mode) {
		case 0:	if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,500)==1) obj_mode = 1;
				PlayerApproachOnOff(execstp,3000);
				break;

		case 1:	EscapeFromPlayer();
				break;

		case 2:	WaitForPlayer();
				PlayerApproachOnOff(execstp,3000);
				break;

		case 3:	CoinBound(&move_flag);
				if (obj_timer > FRAME*2) { obj_mode = 4;}
				break;

		case 4:	RemoveCoin();
				break;

		case OBJ_MELT : execstp->status = 0;
						break;
		case OBJ_AIRDEAD : execstp->status = 0; 
						   break;
	}


	if ( obj_mail & EMAIL_PLAYERHIT ){
		RemoveCoinCatch();
		Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
	}

}

/***************************************************************************************************/
/***************************************************************************************************/

/***************************************************************************************************
						Escape Coin (for enemy) Main
***************************************************************************************************/
extern void s_escape2_coin_main(void)
{

	short move_flag;

	switch(obj_mode) {
		case 0: if (obj_timer == 0 ) { s_hitOFF(); obj_speedY = 50;}
				ObjMoveEvent();
				if (obj_timer == 15) { s_hitON();obj_mode = 1; }
				break;

		case 1:	EscapeFromPlayer();
				break;

		case 2:	WaitForPlayer();
				PlayerApproachOnOff(execstp,3000);
				break;

		case 3:	CoinBound(&move_flag);
				if (obj_timer > FRAME*2) { obj_mode = 4;}
				break;

		case 4:	RemoveCoin();
				break;

	}

	if ( obj_mail & EMAIL_PLAYERHIT ){
		RemoveCoinCatch();
		Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
	}

}





/*################*/
#endif
/*################*/


