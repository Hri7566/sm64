/********************************************************************************
						Ultra 64 MARIO Brothers

					  debug title shape data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

				This module was programmed by Y.Nishida

							December 5, 1995
 ********************************************************************************/

#include "../../headers.h"


#define	LX		 0
#define	LY		 0
#define	LZ		80

#define	DefMaterial(R,G,B)		{ {R/2,G/2,B/2,0, R/2,G/2,B/2,0}, {R,G,B,0, R,G,B,0, LX,LY,LZ,0}}

#include "shapes/title_superS.sou"
#include "shapes/title_superU.sou"
#include "shapes/title_superP.sou"
#include "shapes/title_superE.sou"
#include "shapes/title_superR.sou"
#include "shapes/title_marioM.sou"
#include "shapes/title_marioA.sou"
#include "shapes/title_marioR.sou"
#include "shapes/title_marioI.sou"
#include "shapes/title_marioO.sou"
