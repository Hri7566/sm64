/********************************************************************************
						Ultra 64 MARIO Brothers

				 mario flying hip attack animation data

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							January 10, 1996
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioHipAttackFly:
	.half	 MAP_ANIM_ONETIME 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	16 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x05C8						/* total bytes       */


	.half	    1,    0 	/* chn14translate x */
	.half	   16,    1 	/* chn14 translate y */
	.half	   16,   17 	/* chn14 translate z */
	.half	    1,    0 	/* chn14 rotate x */
	.half	    1,   33 	/* chn14 rotate y */
	.half	    1,    0 	/* chn14 rotate z */
	.half	    1,    0 	/* <m_waist>1_3 rotate x */
	.half	    1,    0 	/* <m_waist>1_3 rotate y */
	.half	   16,  586 	/* <m_waist>1_3 rotate z */
	.half	   16,  538 	/* <m_body>1 rotate x */
	.half	   16,  554 	/* <m_body>1 rotate y */
	.half	   16,  570 	/* <m_body>1 rotate z */
	.half	   16,  490 	/* <m_head>2 rotate x */
	.half	   16,  506 	/* <m_head>2 rotate y */
	.half	   16,  522 	/* <m_head>2 rotate z */
	.half	    1,  486 	/* chn6 rotate x */
	.half	    1,  487 	/* chn6 rotate y */
	.half	    1,  488 	/* chn6 rotate z */
	.half	   16,   98 	/* <m_larmA>1 rotate x */
	.half	   16,  114 	/* <m_larmA>1 rotate y */
	.half	   16,  130 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1 rotate x */
	.half	    1,    0 	/* <m_larmB>1 rotate y */
	.half	   16,   82 	/* <m_larmB>1 rotate z */
	.half	   16,   34 	/* <m_lhand>1 rotate x */
	.half	   16,   50 	/* <m_lhand>1 rotate y */
	.half	   16,   66 	/* <m_lhand>1 rotate z */
	.half	    1,  482 	/* chn10 rotate x */
	.half	    1,  483 	/* chn10 rotate y */
	.half	    1,  484 	/* chn10 rotate z */
	.half	   16,  210 	/* <m_rarmA>1 rotate x */
	.half	   16,  226 	/* <m_rarmA>1 rotate y */
	.half	   16,  242 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1 rotate y */
	.half	   16,  194 	/* <m_rarmB>1 rotate z */
	.half	   16,  146 	/* <m_rhand>1 rotate x */
	.half	   16,  162 	/* <m_rhand>1 rotate y */
	.half	   16,  178 	/* <m_rhand>1 rotate z */
	.half	    1,    0 	/* chn15 rotate x */
	.half	    1,    0 	/* chn15 rotate y */
	.half	    1,  485 	/* chn15 rotate z */
	.half	   16,  434 	/* <m_llegA>1 rotate x */
	.half	   16,  450 	/* <m_llegA>1 rotate y */
	.half	   16,  466 	/* <m_llegA>1 rotate z */
	.half	    1,    0 	/* <m_llegB>1 rotate x */
	.half	    1,    0 	/* <m_llegB>1 rotate y */
	.half	   16,  418 	/* <m_llegB>1 rotate z */
	.half	   16,  370 	/* <m_ltoot>1 rotate x */
	.half	   16,  386 	/* <m_ltoot>1 rotate y */
	.half	   16,  402 	/* <m_ltoot>1 rotate z */
	.half	    1,    0 	/* chn17 rotate x */
	.half	    1,    0 	/* chn17 rotate y */
	.half	    1,  489 	/* chn17 rotate z */
	.half	   16,  322 	/* <m_rlegA>1 rotate x */
	.half	   16,  338 	/* <m_rlegA>1 rotate y */
	.half	   16,  354 	/* <m_rlegA>1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1 rotate y */
	.half	   16,  306 	/* <m_rlegB>1 rotate z */
	.half	   16,  258 	/* <m_rfoot>1 rotate x */
	.half	   16,  274 	/* <m_rfoot>1 rotate y */
	.half	   16,  290 	/* <m_rfoot>1 rotate z */

	.half	0x0000,0x0000,0x000A,0x0021,0x0036,0x0046,0x0055,0x005D,0x0059,0x004E,0x0046,0x0043,0x0040,0x003E,0x003C,0x003B
	.half	0x003B,0x0000,0xFFF0,0xFFD1,0xFFBC,0xFFBE,0xFFCC,0xFFDE,0xFFF7,0x0013,0x0024,0x0028,0x0028,0x0024,0x001F,0x001B
	.half	0x0019,0x3FFF,0x008A,0x0087,0x007E,0x006F,0x005D,0x0048,0x0030,0x0017,0xFFFE,0xFFE5,0xFFCD,0xFFB8,0xFFA5,0xFF97
	.half	0xFF8E,0xFF8A,0xF435,0xF447,0xF477,0xF4C2,0xF523,0xF594,0xF612,0xF697,0xF71E,0xF7A3,0xF821,0xF892,0xF8F3,0xF93E
	.half	0xF96F,0xF980,0xEA2C,0xEA41,0xEA7A,0xEAD3,0xEB45,0xEBCC,0xEC60,0xECFE,0xED9E,0xEE3B,0xEED0,0xEF56,0xEFC8,0xF021
	.half	0xF05B,0xF06F,0xF913,0xF855,0xF63F,0xF304,0xEEDB,0xE9F9,0xE491,0xDEDB,0xD909,0xD352,0xCDEB,0xC909,0xC4E0,0xC1A5
	.half	0xBF8E,0xBED0,0xB8A2,0xB8C7,0xB92F,0xB9D1,0xBAA0,0xBB94,0xBCA2,0xBDC0,0xBEE2,0xC000,0xC10D,0xC201,0xC2D1,0xC372
	.half	0xC3DB,0xC400,0xC454,0xC48C,0xC52A,0xC61F,0xC759,0xC8CB,0xCA64,0xCC14,0xCDCC,0xCF7C,0xD115,0xD287,0xD3C1,0xD4B6
	.half	0xD554,0xD58C,0xCB3D,0xCC2F,0xCED8,0xD2F4,0xD840,0xDE78,0xE559,0xEC9F,0xF407,0xFB4E,0x022E,0x0866,0x0DB2,0x11CE
	.half	0x1477,0x1569,0x003A,0x003B,0x0040,0x0046,0x004E,0x0058,0x0063,0x006E,0x0079,0x0085,0x008F,0x0099,0x00A1,0x00A8
	.half	0x00AC,0x00AD,0x054A,0x052F,0x04E1,0x0468,0x03CD,0x0317,0x024E,0x0179,0x00A0,0xFFCC,0xFF03,0xFE4D,0xFDB2,0xFD3A
	.half	0xFCEC,0xFCD0,0xEBEE,0xEBD7,0xEB97,0xEB35,0xEAB5,0xEA20,0xE97A,0xE8CB,0xE819,0xE76A,0xE6C5,0xE62F,0xE5B0,0xE54D
	.half	0xE50D,0xE4F6,0xF6A2,0xF603,0xF446,0xF197,0xEE20,0xEA10,0xE591,0xE0CF,0xDBF8,0xD737,0xD2B8,0xCEA7,0xCB31,0xC881
	.half	0xC6C4,0xC626,0x495F,0x4966,0x497B,0x499C,0x49C5,0x49F6,0x4A2C,0x4A65,0x4A9F,0x4AD8,0x4B0E,0x4B3F,0x4B68,0x4B89
	.half	0x4B9D,0x4BA5,0x39E6,0x3991,0x38A2,0x3730,0x3554,0x3324,0x30BA,0x2E2B,0x2B91,0x2902,0x2698,0x2468,0x228C,0x211A
	.half	0x202B,0x1FD6,0xCC24,0xCD77,0xD131,0xD6F3,0xDE5E,0xE715,0xF0B8,0xFAE9,0x0549,0x0F7A,0x191D,0x21D4,0x2940,0x2F02
	.half	0x32BC,0x340F,0x0135,0x0131,0x0127,0x0117,0x0103,0x00EC,0x00D2,0x00B6,0x009A,0x007E,0x0064,0x004D,0x0039,0x0029
	.half	0x001F,0x001B,0x0207,0x0208,0x020B,0x0210,0x0216,0x021D,0x0225,0x022D,0x0235,0x023E,0x0246,0x024D,0x0253,0x0257
	.half	0x025A,0x025B,0xDF23,0xDEE2,0xDE2A,0xDD0F,0xDBA1,0xD9F4,0xD819,0xD624,0xD425,0xD22F,0xD054,0xCEA7,0xCD39,0xCC1E
	.half	0xCB66,0xCB25,0x10D8,0x132E,0x1897,0x1EA8,0x22F8,0x25A5,0x285D,0x2B0E,0x2DAB,0x3021,0x3263,0x345F,0x3606,0x3748
	.half	0x3815,0x385D,0x10FF,0x10AD,0x0FC7,0x0E64,0x0C99,0x0A7F,0x082C,0x05B7,0x0336,0x00C1,0xFE6E,0xFC54,0xFA8A,0xF927
	.half	0xF840,0xF7EF,0x052D,0x0535,0x054E,0x0573,0x05A4,0x05DD,0x061C,0x065F,0x06A3,0x06E6,0x0725,0x075E,0x078F,0x07B5
	.half	0x07CD,0x07D6,0xBDBB,0xBCE1,0xBA7B,0xB6C5,0xB1FE,0xAC61,0xA62C,0x9F9C,0x98ED,0x925D,0x8C28,0x868B,0x81C4,0x7E0C
	.half	0x7BA5,0x7ACB,0x0013,0x0015,0x0019,0x0020,0x002A,0x0035,0x0041,0x004D,0x005A,0x0067,0x0073,0x007E,0x0088,0x008F
	.half	0x0093,0x0095,0xFEE0,0xFEE0,0xFEE2,0xFEE4,0xFEE7,0xFEEB,0xFEEE,0xFEF2,0xFEF6,0xFEFA,0xFEFE,0xFF02,0xFF05,0xFF07
	.half	0xFF08,0xFF09,0xDF6A,0xDF2B,0xDE7C,0xDD6C,0xDC0E,0xDA73,0xD8AC,0xD6CB,0xD4E1,0xD300,0xD13A,0xCF9E,0xCE40,0xCD31
	.half	0xCC81,0xCC42,0x0F13,0x10EC,0x153D,0x1A2D,0x1DE1,0x2071,0x2321,0x25DE,0x2893,0x2B2D,0x2D97,0x2FBE,0x318E,0x32F1
	.half	0x33D5,0x3426,0xF602,0xF622,0xF67C,0xF707,0xF7BA,0xF88C,0xF974,0xFA6A,0xFB64,0xFC59,0xFD42,0xFE14,0xFEC6,0xFF51
	.half	0xFFAB,0xFFCB,0xFC00,0xFBE5,0xFB9A,0xFB25,0xFA8F,0xF9DF,0xF91C,0xF84E,0xF77C,0xF6AE,0xF5EB,0xF53A,0xF4A4,0xF430
	.half	0xF3E4,0xF3CA,0xBCB0,0xBBFF,0xBA0C,0xB70A,0xB329,0xAE9B,0xA992,0xA43F,0x9ED2,0x997F,0x9476,0x8FE8,0x8C07,0x8905
	.half	0x8712,0x8661,0x8001,0x8001,0x4171,0xBF5F,0x8001,0x8001,0x4171,0xBF5F,0x0000,0xFFF8,0xFFF1,0x0008,0x0048,0x009B
	.half	0x00F6,0x014D,0x0193,0x01BC,0x01C1,0x01A8,0x017F,0x0151,0x012D,0x011E,0x0000,0x000E,0x0030,0x0056,0x0076,0x0095
	.half	0x00B4,0x00D8,0x0102,0x0136,0x0182,0x01E5,0x024E,0x02AF,0x02F4,0x030F,0xD6E6,0xD49D,0xD1E7,0xD4FB,0xDF21,0xEC63
	.half	0xFB17,0x0995,0x1635,0x1F4E,0x248B,0x272C,0x27EF,0x2791,0x26CD,0x2662,0x0000,0x000B,0x0022,0x0033,0x002D,0xFFF2
	.half	0xFF97,0xFF61,0xFF5A,0xFF5C,0xFF65,0xFF72,0xFF81,0xFF8F,0xFF9A,0xFF9E,0x0000,0xFFF3,0xFFD2,0xFFA6,0xFF77,0xFF45
	.half	0xFF09,0xFECB,0xFE85,0xFE36,0xFDE1,0xFD8E,0xFD43,0xFD06,0xFCDD,0xFCCE,0xF839,0xF5C8,0xF0B4,0xEC55,0xEC04,0xF4AB
	.half	0x028B,0x0C07,0x0FB3,0x129D,0x14DC,0x1682,0x17A7,0x185E,0x18BC,0x18D7,0x7C1D,0x7E24,0x83DA,0x8CAA,0x9805,0xA55B
	.half	0xB41A,0xC3B3,0xD394,0xE32C,0xF1EC,0xFF41,0x0A9B,0x136B,0x191F,0x1B25
