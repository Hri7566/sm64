



#define ASSEMBLER

#include "../headers.h"

/********************************************************************************/
/*		Start AsmCode															*/
/********************************************************************************/

	.data
	.align	2
	.align	0

/********************************************************************************/
/*		globl labels															*/
/********************************************************************************/

#if	CAMDEBUGSW
	.globl	e_tool_jugem
#endif

	.globl	e_ending_bird
	.globl	e_ending_bird2
	.globl	e_opening_peach
	.globl	e_opening_jugem
	.globl	e_demo_producer

/*
	.globl	e_test_coin2
	.globl	e_test_coin
*/


/****
	.globl	e_demo_director
	.globl	e_demo_test

	.globl	e_KUPPA_crow
	.globl	e_KUPPA_director

	.globl	e_WINDEMO_camcarrier
	.globl	e_WINDEMO_camlooker
	.globl	e_WINDEMO_stardust
	.globl	e_WINDEMO_star0
	.globl	e_WINDEMO_star_start
	.globl	e_WINDEMO_gueststar
	.globl	e_WINDEMO_tailstar
	.globl	e_WINDEMO_director

/************************************************/
/*	object data				*/
/************************************************/

e_opening_jugem:

	p_initialize(option)
	p_setbit( flag,stf_moveON )

	p_set_pointer(skelanime,camerajugem_anime);
	p_set_skelanime_number(0);
	p_setf(alpha,0);

	p_while
	  p_program(s_opening_jugem)
	p_loop


/********************************/
/********************************/
/********************************/
#if	CAMDEBUGSW
e_tool_jugem:
	p_initialize(option)
	p_setbit( flag,stf_moveON )
	p_set_pointer(skelanime,camerajugem_anime);
	p_set_skelanime_number(0);
	p_while
	  p_program(s_tool_jugem)
	p_loop
#endif
/********************************/
/********************************/
/********************************/




e_opening_peach:

	p_initialize(option)
	p_setbit( flag,stf_moveON )

	p_set_pointer(skelanime,peach_anime);
	p_set_skelanime_number(0);

	p_while
	  p_program(s_opening_peach)
	p_loop

e_ending_bird:

	p_initialize(option)
	p_setbit( flag,stf_moveON|stf_ALLangleSAME )

	p_set_pointer(skelanime,minibird_anime);
	p_set_skelanime_number(0);

	p_while
	  p_program(s_ending_bird)
	p_loop

e_ending_bird2:

	p_initialize(option)
	p_setbit( flag,stf_moveON|stf_ALLangleSAME )

	p_set_pointer(skelanime,minibird_anime);
	p_set_skelanime_number(0);

	p_while
	  p_program(s_ending_bird2)
	p_loop


/******
	e_test_coin2:
	p_initialize(option)
	p_setbit( flag,stf_moveON|stf_moveON )
	p_while
	  p_program(s_test_coin2)
	p_loop
*******/

/********
e_test_coin:
	p_initialize(option)
	p_setbit( flag,stf_moveON )

	p_program(s_rou_makeobj)

	p_while
	  p_program(s_test_coin)
	p_loop

********/
/*******e_demo_test:

	p_initialize(option)
	p_setbit( flag,stf_moveON )
	p_softspritemodeON

	p_while
	  p_program(s_demo_test)
	p_loop

e_demo_director:

	p_initialize(option)
	p_setbit( flag,stf_moveON )

	p_program(s_rou_make_demo_testers)

	p_while
	  p_program(s_demo_test)
	p_loop

/*****************************************/
/*************** WINDEMO******************/
/*****************************************/
/*****
e_WINDEMO_stardust:

	p_initialize(option)
	p_setbit( flag,stf_moveON)

	p_softspritemodeON

	p_while
	  p_program(s_WINDEMO_stardust)
	p_loop



e_WINDEMO_star0:

	p_initialize(option)
	p_setbit( flag,stf_moveON|stf_ALLangleSAME)

	p_while
	  p_program(s_WINDEMO_star0)
	p_loop

e_WINDEMO_tailstar:

	p_initialize(option)
	p_setbit( flag,stf_moveON|stf_ALLangleSAME)

	p_while
	  p_program(s_WINDEMO_tailstar)
	p_loop

e_WINDEMO_star_start:

	p_initialize(option)
	p_setbit( flag,stf_moveON )

	p_while
	  p_program(s_WINDEMO_star_start)
	p_loop

e_WINDEMO_gueststar:

	p_initialize(option)
	p_setbit( flag,stf_moveON|stf_ALLangleSAME)

	p_while
	  p_program(s_WINDEMO_gueststar)
	p_loop

e_WINDEMO_camcarrier:

	p_initialize(option)
	p_setbit( flag,stf_moveON)

	p_while
	  p_program(s_WINDEMO_camcarrier)
	p_loop

e_WINDEMO_camlooker:

	p_initialize(option)
	p_setbit( flag,stf_moveON)

	p_while
	  p_program(s_WINDEMO_camlooker)
	p_loop

e_WINDEMO_director:

	p_initialize(option)
	p_setbit( flag,stf_moveON )

	p_program(s_rou_WINDEMO_actors)

	p_while
	  p_program(s_WINDEMO_director)
	p_loop


e_KUPPA_crow:

	p_initialize(option)
	p_setbit( flag,stf_moveON|stf_ALLangleSAME )

	p_while
	  p_program(s_KUPPA_crow)
	p_loop

e_KUPPA_director:

	p_initialize(option)
	p_setbit( flag,stf_moveON )

	p_while
	  p_program(s_KUPPA_director)
	p_loop

****/


e_demo_producer:

	p_initialize(option)
	p_setbit( flag,stf_moveON )

	p_while
	  p_program(s_demo_producer)
	p_loop




