/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: puku.s
					Description	: 
					Date		: 1995.
					Author		: H.yajima

 ********************************************************************************/

#define		PUKU_APPEAR_DIST	1500
#define		PUKU_REMOVE_DIST	2000

/*################*/
#ifdef ASSEMBLER
/*################*/

/*--------------------------------------------------------------*/
e_pukumother_many:
e_pukumother_little:
e_pukumother:
	p_initialize(option)
	p_setd(work0,1)
	p_jmp(e_pukumother_common)
/*--------------------------------------------------------------*/
e_pukumother_common:
	p_shapeDISABLE
	p_setbit(flag,stf_moveON | stf_YangleSAME | stf_playerdistON )
	p_while      
		p_program(s_pukumother_program)
	p_loop
/*--------------------------------------------------------------*/

e_puku:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON | stf_YangleSAME | stf_playerdistON | stf_playerangleON )
	p_set_pointer(skelanime,puku_anime)	
	p_set_skelanime_number(0)
	p_sethitbox2(20,10,10)
	p_setobjname(DAMAGE)
	p_setd(ap,1)
	p_save_nowpos
	p_hitON

	p_while
		p_program(s_puku)
	p_loop

/*################*/
#else
/*################*/

/*******************************************************************************
		mode
*********************************************************************************/

#define		puku_water				(execstp->s[stw_work0].f)
#define		puku_playerY			(execstp->s[stw_work1].f)	
#define		puku_random				(execstp->s[stw_work2].f)	
#define		puku_limitY				(execstp->s[stw_work4].f)
#define		puku_searchrandom		(execstp->s[stw_work5].f)


/********************************************************************************/
static void pukumother_init(void)
{
	int		i;
	int		count = execstp->s[stw_work0].d;

	if ( obj_playerdist <  PUKU_APPEAR_DIST ){
		for(i=0;i<count;i++)	s_makeobj_nowpos(execstp,S_PukuPuku,e_puku);
		obj_mode = mode_pukumother_main;
	}

}

/********************************************************************************/
static void pukumother_main(void)
{

	if ( ( player1stp->s[stw_worldY].f - obj_worldY ) > PUKU_REMOVE_DIST ){
		obj_mode = mode_pukumother_remove;
	}

}

/********************************************************************************/
static void pukumother_remove(void)
{
	obj_mode = mode_pukumother_exit;

}

/********************************************************************************/
static void pukumother_exit(void)
{
	obj_mode = mode_pukumother_init;

}

/********************************************************************************/

static void *pukumother_modejmp[] = {
	
	 pukumother_init,
	 pukumother_main,
	 pukumother_remove,
	 pukumother_exit
	
};

extern void s_pukumother_program(void)
{

	s_modejmp(pukumother_modejmp);

}


/********************************************************************************
		------------------
		 pukumove worldY
		------------------
*********************************************************************************/

static void pukumove_worldY(int	data)
{

	float	motherY = execstp->motherobj->s[stw_worldY].f;

	if ( ((motherY-100-puku_limitY) < obj_worldY ) && ( obj_worldY < (motherY+1000+puku_limitY))) {
		obj_worldY	= s_chase_position(obj_worldY,puku_playerY,data);
	} else {
	}

}



/********************************************************************************
	#####
	#####	puku initialize program
	#####
*********************************************************************************/

static void puku_init(void)
{

	puku_random	= Randomf()*100;
	puku_limitY	= Randomf()*300;
	obj_mode	= mode_puku_search;

}

/********************************************************************************
	#####
	#####	puku search program
	#####
*********************************************************************************/

static void puku_search(void)
{

	float	dy;

	if ( obj_timer == 0 ){
		obj_speedF 	 	  = 2+Randomf()*2;
		puku_searchrandom = Randomf();
	}


	dy = obj_worldY - ( player1stp->s[stw_worldY].f );

	if ( obj_worldY < puku_water-50 ){
		if ( dy < 0 ) dy = 0-dy;
		if ( dy < 500 )	pukumove_worldY(1);
		else			pukumove_worldY(4);	
	} else {
		obj_worldY = puku_water-50;
		if ( dy > 300 )	obj_worldY--;
	}


	if ( s_calc_playerscope() > 800 ) obj_targetangle = s_calc_returnangle();
	s_chase_angleY(obj_targetangle,0x100);
	if ( obj_playerdist < 200 ){
		if ( puku_searchrandom < 0.5 )	obj_mode = mode_puku_runaway;
	}

	if ( (obj_mail & EMAIL_PLAYERHIT) != 0 ){
		obj_mode = mode_puku_runaway;
	}


}

/********************************************************************************
	#####
	#####	puku runaway program
	#####
*********************************************************************************/
static void puku_runaway(void)
{

	float	dy;
	int		sound;

	if ( obj_timer < 20 ){
		if ( (obj_mail & EMAIL_PLAYERHIT) != 0 ){
			s_makeobj_nowpos(execstp,S_waterdrop,e_enemy_bubblejet);
		}
	} else {
		Mbitclr(obj_mail,EMAIL_PLAYERHITALL);
	}

	if ( obj_timer  == 0 ) objsound(NA_SE2_FISH);
	if ( obj_speedF == 0 ) obj_speedF = 6;

	dy = obj_worldY - ( player1stp->s[stw_worldY].f );

	if ( obj_worldY < puku_water-50 ){
		if ( dy < 0 ) dy = 0-dy;
		if ( dy < 500 )	pukumove_worldY(2);
		else			pukumove_worldY(4);
	} else {
		obj_worldY = puku_water-50;
		if ( dy > 300 )	obj_worldY--;
	}

	if ( s_calc_playerscope() > 800 ) obj_targetangle = s_calc_returnangle();
	s_chase_angleY(obj_targetangle+0x8000,0x400);
	if ( obj_timer > 200 && obj_playerdist > 600 )	 obj_mode = mode_puku_search;

}


/********************************************************************************
		C Program
 ********************************************************************************/

static void *puku_modejmp[] = {
	
	 puku_init,
	 puku_search,
	 puku_runaway
	
};

extern void s_puku(void)
{

	puku_water	    	= mcWaterCheck(obj_worldX,obj_worldZ);		/* my position */
	puku_playerY		= player1stp->s[stw_worldY].f+puku_random;

	
	execstp->s[stw_wall_offsetR].f = 30;
	s_enemybgcheck();

	s_modejmp(puku_modejmp);

	s_optionmove_F();

	/*----- remove check ------*/
	if ( execstp->motherobj->s[stw_mode].d == mode_pukumother_remove ) s_remove_obj(execstp);
	/*-------------------------*/

}
							






/*################*/
#endif
/*################*/




