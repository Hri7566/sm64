/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage26 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 11, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#include "uraniwa/main_garden_shape.sou"
#include "uraniwa/main_g_tower_shape.sou"
#include "uraniwa/main_g_shade_shape.sou"
#include "uraniwa/main_g_window_shape.sou"

#include "uraniwa/cx2601.flk"
#include "uraniwa/cx2601.tag"

/********************************************************************************/
/*	Water surface records in 26 - 1.											*/
/********************************************************************************/
static short waterSurf_26_01_00[] = {
	1,
	0,   5,  3,   -656,-2405,    -656,-1074,     674,-1074,     674,-2405,  1,  150,  0,
};

WaterSurfRec waterSurf_26_01[] = {
		{	 0, waterSurf_26_01_00	 },
		{	-1,	NULL	}					//	End of  WaterSurfRec data.
};
