/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage9 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx gfx_plain3[]		  ;
extern Gfx gfx_plain3_flat[]  ;
extern Gfx gfx_plain3_shadow[];
extern Gfx gfx_plain3_saku[]  ;
extern Gfx gfx_plain3_gro[]	  ;
extern Gfx gfx_plain3_gro2[]  ;

extern Gfx gfx_pl_moveobj_01[];
extern Gfx gfx_pl_moveobj_02[];
extern Gfx gfx_pl_moveobj_03[];


/* ===============================================================================
        : Hierarchy map data of PlMoveObj01.
================================================================================== */
Hierarchy RCP_HmsPlMoveObj01[] = {
    hmsHeader(1000)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_pl_moveobj_01)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of PlMoveObj02.
================================================================================== */
Hierarchy RCP_HmsPlMoveObj02[] = {
    hmsHeader(1200)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_pl_moveobj_02)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of PlMoveObj03.
================================================================================== */
Hierarchy RCP_HmsPlMoveObj03[] = {
    hmsHeader(800)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_pl_moveobj_03)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage9Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(0, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, 30000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_FIELD, 0,2000,6000, 3072,0,-4608, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF	, gfx_plain3)
					hmsGfx(RM_SURF  , gfx_plain3_flat)
					hmsGfx(RM_XDECAL, gfx_plain3_shadow)
					hmsGfx(RM_SPRITE, gfx_plain3_saku)
					hmsGfx(RM_SURF  , gfx_plain3_gro)
					hmsGfx(RM_SURF  , gfx_plain3_gro2)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()

		hmsLayer(0)
		hmsBegin()
			hmsCProg(0, CannonEdge)
		hmsEnd()

	hmsEnd()
	hmsExit()
};
