/********************************************************************************
	collision.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	May 6, 1996
 ********************************************************************************/

#ifndef COLLISION_H
#define	COLLISION_H


#define	PL_CheckBurnObject(player)		((player)->strategy->s[stw_mail].d & PLAYERMAIL_BOMB_BURN)
#define	PL_CheckBirdModeOff(player)		((player)->strategy->s[stw_mail].d & PLAYERMAIL_BIRDMODE_OFF)


/********************************************************************************/
/*																				*/
/*	Define function prototypes.													*/
/*																				*/
/********************************************************************************/


extern int
PL_CheckCapType(PlayerPtr player);

extern void
PL_GetOffSkateBoard(PlayerPtr player);

extern void
PL_TakeObject(PlayerPtr player);

extern void
PL_DropObject(PlayerPtr player);

extern void
PL_ThrowObject(PlayerPtr player);

extern void
PL_DropAllObject(PlayerPtr player);

extern int
PL_IsPlayerWearingRedHat(PlayerPtr player);

extern void
PL_BlowHat(PlayerPtr player, float speed);

extern int
PL_StealMariosHat(int kind);

extern void
PL_ReturnMariosHat(void);

extern int
PL_CheckTakingEnemy(PlayerPtr player);

extern StrategyPtr
PL_GetCollidedObject(PlayerPtr player, ulong name);

extern void
PL_CollisionCheck(PlayerPtr player);

extern void
PL_CheckGroundCondition(PlayerPtr player);

extern ulong
PL_GetStarDoorFlag(StrategyPtr stratp);

extern short
PL_GetPlayerAttackAngle(PlayerPtr player, StrategyPtr enemy);

#endif
