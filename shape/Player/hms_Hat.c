/********************************************************************************
						Ultra 64 MARIO Brothers

					   mario hat hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 16, 1995
 ********************************************************************************/

extern Gfx RCP_hat_only[];
extern Gfx RCP_metal_hat_only[];

extern Gfx RCP_wing_only[];
extern Gfx RCP_metal_wing_only[];


// ============================================================================================	
//
//
//		Mario Hat Hierarchy data.
//
//
/*********************************************************************************
    : Hierarchy map data of Hat.
**********************************************************************************/
Hierarchy Hms_HatHierarchy[] = {
	hmsShadow(75, 180, 0)
    hmsBegin()
		hmsScale(0.25f)
	    hmsBegin()
	        hmsGfx(RM_SURF, RCP_hat_only)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/*********************************************************************************
    : Hierarchy map data of MetalHat.
**********************************************************************************/
Hierarchy Hms_MetalHatHierarchy[] = {
	hmsShadow(75, 180, 0)
    hmsBegin()
		hmsScale(0.25f)
	    hmsBegin()
	        hmsGfx(RM_SURF, RCP_metal_hat_only)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/*********************************************************************************
    : Hierarchy map data of WingHat.
**********************************************************************************/
Hierarchy Hms_WingHatHierarchy[] = {
	hmsShadow(75, 180, 0)
    hmsBegin()
		hmsScale(0.25f)
	    hmsBegin()
	        hmsGfx(RM_SURF,   RCP_hat_only)
	        hmsGfx(RM_SPRITE, RCP_wing_only)
		hmsEnd()
    hmsEnd()
    hmsExit()
};

/*********************************************************************************
    : Hierarchy map data of MetalWingHat.
**********************************************************************************/
Hierarchy Hms_MetalWingHatHierarchy[] = {
	hmsShadow(75, 180, 0)
    hmsBegin()
		hmsScale(0.25f)
	    hmsBegin()
	        hmsGfx(RM_SURF,   RCP_metal_hat_only)
	        hmsGfx(RM_SPRITE, RCP_metal_wing_only)
		hmsEnd()
    hmsEnd()
    hmsExit()
};
