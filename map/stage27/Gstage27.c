/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage27 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"


#define		STAGE27_FOG_R		0
#define		STAGE27_FOG_G		0
#define		STAGE27_FOG_B		0
#define		STAGE27_FOG_START	980

#include "ext4_minislider/ext4_minislider_ext_texture.h"

#include "ext4_minislider/mini_s3_shape.sou"
#include "ext4_minislider/mini_s3flat_shape.sou"
#include "ext4_minislider/mini_s3uraomote_shape.sou"
#include "ext4_minislider/mini_s3nonfog_shape.sou"
#include "ext4_minislider/mini_s3_ia_shape.sou"
#include "ext4_minislider/mini_s3_daiza_shape.sou"
#include "ext4_minislider/mini_s3_gate_shape.sou"

#include "ext4_minislider/cx2701.flk"
#include "ext4_minislider/cx2701.tag"
