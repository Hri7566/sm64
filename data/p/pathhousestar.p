/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: pathhousestar.s
					Description	: 
					Date		: 1995.
					Author		: H.yajima

 ********************************************************************************/


/*################*/
#ifdef ASSEMBLER
/*################*/

e_house_star:
	p_initialize(item)
	p_setbit(flag,stf_moveON | stf_NOramsave )
	p_sethitbox(500,500)
	p_hitON
	p_while
		p_program(s_house_star)
	p_loop

/*################*/
#else
/*################*/
/********************************************************************************
		C Program (housestar)
 ********************************************************************************/

extern void s_house_star(void)
{

	if ( course_ramsave_flag & RAMSAVE_SLIDER_STAR ){

		obj_worldY += 100;
		obj_worldX = 2780;
		obj_worldZ = 4666;
		s_enemyset_star(2500,-4350,5750);	/* slider goal */
		s_remove_obj(execstp);

	}


}

/*################*/
#endif
/*################*/
/*===============================================================================
		end end end end end end end end 
===============================================================================*/



