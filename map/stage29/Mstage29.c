/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage29 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

#define		STAGE29_CLEAR_R		0
#define		STAGE29_CLEAR_G		12
#define		STAGE29_CLEAR_B		31

extern Gfx gfx_e6_hontai[];
extern Gfx gfx_e6_niji[]  ;
extern Gfx gfx_e6_shita[] ;
extern Gfx gfx_e6_kumo[]  ;

/********************************************************************************/
/*	Hierarchy data of slider decorations.										*/
/********************************************************************************/
Hierarchy RCP_Ext6Cloud[] = {
	hmsHeader(2000)
	hmsBegin()
		hmsBboard(0,0,0)
		hmsBegin()
			hmsGfx(RM_XSURF, gfx_e6_kumo)
		hmsEnd()
	hmsEnd()
	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage29Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(3, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, 25000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON_O,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF,  gfx_e6_hontai)
					hmsGfx(RM_XSURF, gfx_e6_niji)
					hmsGfx(RM_XSURF, gfx_e6_shita)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
