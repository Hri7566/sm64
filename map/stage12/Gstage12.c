/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage12 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
*********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE12_FOG_R		  5
#define		STAGE12_FOG_G		 80
#define		STAGE12_FOG_B		 75
#define		STAGE12_FOG_START	900

#define		STAGE12A_FOG_R		 15
#define		STAGE12A_FOG_G		 65
#define		STAGE12A_FOG_B		100
#define		STAGE12A_FOG_START	930

#define		STAGE12E_FOG_R		 15
#define		STAGE12E_FOG_G		 65
#define		STAGE12E_FOG_B		100
#define		STAGE12E_FOG_START	930

#include "water_donjon/water_donjon_ext_texture.h"
#include "water_donjon/kibako_texture.h"

#include "water_donjon/tin_a_shape.sou"
#include "water_donjon/tin_b_shape.sou"
#include "water_donjon/tin_c_shape.sou"
#include "water_donjon/tin_d_shape.sou"
#include "water_donjon/tin_e_shape.sou"
#include "water_donjon/tin_f_shape.sou"
#include "water_donjon/tin_g_shape.sou"
#include "water_donjon/tin_iwa_shape.sou"
#include "water_donjon/tin_ita_shape.sou"
#include "water_donjon/kibako_shape.sou"
#include "water_donjon/fune_fl_shape.sou"
#include "water_donjon/fune_bl_shape.sou"
#include "water_donjon/fune_fr_shape.sou"
#include "water_donjon/fune_br_shape.sou"
#include "water_donjon/hasira_shape.sou"
#include "water_donjon/hasiradai_shape.sou"

#include "water_donjon/cx1201.flk"
#include "water_donjon/cx1201.tag"

#include "water_donjon/tin_iwa_c.flk"
#include "water_donjon/tin_ita_check.flk"
#include "water_donjon/kibakocheck.flk"
#include "water_donjon/fune_c_db.flk"
#include "water_donjon/fune_c_a.flk"
#include "water_donjon/fune_c_b.flk"
#include "water_donjon/fune_c_rr.flk"
#include "water_donjon/hasiradai_c.flk"
#include "water_donjon/tin_futa_c.flk"

#include "water_donjon/ubboo1.ral"
#include "water_donjon/ubboo2.ral"

/********************************************************************************/
/*	Water surface records in 12 - 1 [ Water Dungeon ].							*/
/********************************************************************************/
static short waterSurf_12_01_00[] = {
	1,
	0,   20,  6, -6304, -669,   -6304, 7814,    7992, 7814,    7992, -669,  1,  180,  2,
};

static short waterSurf_12_01_01[] = {
	1,
	0,   10,  2, 4433,-4253,    4433, -669,    5969, -669,    5969,-4253,  1,  180,  2,
};

WaterSurfRec waterSurf_12_01[] = {
		{	 0, waterSurf_12_01_00	 },
		{	 1, waterSurf_12_01_01	 },
		{	-1, NULL	}					//	End of WaterSurfRec data.
};

static short waterSurf_12_05_00[] = {		//	Test smog.
	1,
	0,   20,  10, -7818,-1125,   -7818, 7814,    9055, 7814,    9055,-1125,  1,   50,  1,
};

WaterSurfRec waterSurf_12_05[] = {
		{	51, waterSurf_12_05_00	 },
		{	-1, NULL	}					//	End of WaterSurfRec data.
};


/* ----------------------------------------------------------------------------------------	*/
/* ----------------------------------------------------------------------------------------	*/


#include "tinbotusen/miyafune_shape.sou"
#include "tinbotusen/miyafune_alp_shape.sou"
#include "tinbotusen/miyafune_uo_shape.sou"
#include "tinbotusen/cx1202.flk"
#include "tinbotusen/cx1202.tag"


/********************************************************************************/
/*	Water surface records in 12 - 2 [ In the ship ].							*/
/********************************************************************************/
static short waterSurf_12_02_00[] = {
	1,
	0, 20, 6, -4095,-4095,   -4095, 4096,    4096, 4096,    4096,-4095,  1,  180,  2,
};

WaterSurfRec waterSurf_12_02[] = {
		{	 0, waterSurf_12_02_00	 },
		{	-1, NULL	}					//	End of WaterSurfRec data.
};
