/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 17 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage17


/* ================================================================================
		: Move BGs sequence.
===================================================================================	*/
SEQ_Stage17_MoveBGs:

		seqActor(S_movebg01,  -1966,-3154, 3586,  0, 0,0,  0,  0,0,  e_ukishima_s17	)
		seqActor(S_movebg01,  -1352,-3154, 4200,  0, 0,0,  0,  2,0,  e_ukishima_s17	)
		seqActor(S_movebg01,  -2963, 1017,-2464,  0, 0,0,  0,  0,0,  e_ukishima_s17	)
		seqActor(S_movebg01,  -2349, 1017,-1849,  0, 0,0,  0,  2,0,  e_ukishima_s17	)
		seqActor(S_movebg01,  -2349, 1017,-1235,  0, 0,0,  0,  0,0,  e_ukishima_s17	)
		seqActor(S_movebg01,  -1735, 1017, -621,  0, 0,0,  0,  2,0,  e_ukishima_s17	)
		seqActor(S_movebg02,   1491, 1273,  512,  0,90,0,  0,  0,0,  e_seesaw_s17	)
		seqActor(S_movebg02,   -147,  894,  512,  0,90,0,  0,  0,0,  e_seesaw_s17	)
		seqActor(S_movebg03,  -5728,  819,-2151,  0, 0,0,  3,206,0,  e_derublock	)
		seqActor(S_movebg04,   -204,-1924, 3381,  0, 0,0,  0,  1,0,  e_pedallift	)
		seqActor(S_movebg10,   5279, 1740,   -6,  0, 0,0,  0,  1,0,  e_stepslope	)
		seqActor(S_hanbutton,  3922, 1740,   -7,  0, 0,0,  0,  0,0,  e_onoff_switch )

		seqReturn


/* ================================================================================
		: Enemies sequence.
===================================================================================	*/
SEQ_Stage17_Enemies:

		seqActor(S_NULL,  -3092,-2795,2842,   0,0,0,   0,0,0,   e_firebigbar)
		seqActor(S_NULL,   2463,-2386,2844,   0,0,0,   0,0,0,   e_firebigbar)

		seqReturn


/* ================================================================================
		: Stars sequence.
===================================================================================	*/
SEQ_Stage17_Stars:

		seqActor(S_NULL,   7180, 3000, 0,   0,0,0,   0,0,0,   e_extstar_getcoins)

		seqReturn


/* ================================================================================
		: Stage 17 main sequence.
===================================================================================	*/
SEQ_DoStage17:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage17SegmentRomStart	  , _GfxStage17SegmentRomEnd	)
	seqLoadPres(SEGMENT_BACK	  , _BackYokoScrlSegmentRomStart  , _BackYokoScrlSegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE	  , _EMtTextureSegmentRomStart	  , _EMtTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_kSegmentRomStart	  , _GfxEnemy1_kSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_kSegmentRomStart	  , _HmsEnemy1_kSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_hSegmentRomStart	  , _GfxEnemy2_hSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_hSegmentRomStart	  , _HmsEnemy2_hSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()
		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetEnemy1_k)
		seqCall(SetEnemy2_h)
		seqCall(SetBasicEnemy)

		seqHmsShape(S_bg01, RCP_HmsExt1Bg01)
		seqHmsShape(S_bg02, RCP_HmsExt1Bg02)
		seqHmsShape(S_bg03, RCP_HmsExt1Bg03)
		seqHmsShape(S_bg04, RCP_HmsExt1Bg04)
		seqHmsShape(S_bg05, RCP_HmsExt1Bg05)
		seqHmsShape(S_bg06, RCP_HmsExt1Bg06)
		seqHmsShape(S_bg07, RCP_HmsExt1Bg07)
		seqHmsShape(S_bg08, RCP_HmsExt1Bg08)
		seqHmsShape(S_bg09, RCP_HmsExt1Bg09)
		seqHmsShape(S_bg10, RCP_HmsExt1Bg10)
		seqHmsShape(S_bg11, RCP_HmsExt1Bg11)
		seqHmsShape(S_bg12, RCP_HmsExt1Bg12)
		seqHmsShape(S_bg13, RCP_HmsExt1Bg13)
		seqHmsShape(S_bg14, RCP_HmsExt1Bg14)
		seqHmsShape(S_bg15, RCP_HmsExt1Bg15)

		seqHmsShape(S_bg16, RCP_HmsDokan)

		seqHmsShape(S_movebg01, RCP_HmsExt1Ukishima	)
		seqHmsShape(S_movebg02, RCP_HmsExt1Shiso	)
		seqHmsShape(S_movebg03, RCP_HmsExt1Deru		)

		seqHmsShape(S_movebg04, RCP_HmsExt1Kurumajiku)
		seqHmsShape(S_movebg05, RCP_HmsExt1Kuruma	 )

		seqHmsShape(S_movebg06, RCP_HmsExt1Dorifu0)
		seqHmsShape(S_movebg07, RCP_HmsExt1Dorifu1)
		seqHmsShape(S_movebg08, RCP_HmsExt1Dorifu2)
		seqHmsShape(S_movebg09, RCP_HmsExt1Dorifu3)
		seqHmsShape(S_movebg10, RCP_HmsExt1Dorifu4)

		seqBeginScene(1, RCP_Stage17Scene1)
			seqActor(S_NULL,  -7443,-3153+START_H, 3886,  0,90,0,  0,10,0,	e_player_landing)
			seqActor(S_bg16,   6816,	 	 2860,   -7,  0, 0,0,  0,11,0,	e_dokan			)
			seqActor(S_NULL,   5910, 		 3500,   -7,  0,90,0,  0,12,0,  e_player_downing)
			seqPort(10, 17, 1, 10)		/*	Mario stage in.				*/
			seqPort(11, 30, 1, 10)		/*	To St30 [ Kuppa1 ].			*/
			seqPort(12, 17, 1, 12)		/*	St30 [ Kuppa1 ]  missed.	*/
			seqGameOver(6, 1, 37)		/*	Ext1 Missed.				*/
			seqCall(SEQ_Stage17_MoveBGs)
			seqCall(SEQ_Stage17_Enemies)
			seqCall(SEQ_Stage17_Stars)
			seqMapInfo(cx1701_info)
			seqTagInfo(cx1701_info_tag)
			seqMessage(SEQ_MESG_ENTRANT, 90)
			seqSetMusic(NA_STG_GROUND, NA_EXTRA_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()
	seqEndConstruction()

	seqEnterMario(1, 90, -7443,-3153,3886)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
