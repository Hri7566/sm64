/********************************************************************************
	mtrmain.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	May 28, 1997
 ********************************************************************************/

#ifndef MTRMAIN_H
#define	MTRMAIN_H

extern int motorCounter;

extern void CreateMotorProcess(void);
extern void ResetMotorPack(void);
extern void SendMotorMessage(void);
extern void SendMotorEvent(short time, short level);
extern void SendMotorDecay(short level);
extern int  CheckMotorNoEvent(void);

extern void SendMotorSlip(void);
extern void SendMotorSwim(void);
extern void SendMotorVib(int level);

extern void	InitSiSemaphore(void);
extern void	GetSiPermission(void);
extern void	RelSiPermission(void);

#endif
