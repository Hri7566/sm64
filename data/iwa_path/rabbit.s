/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Rabbit Strategy

					April 4 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/*==============================================================================
						Path Data
 ===============================================================================*/
e_rabbit:
	p_initialize(enemyA)
	p_setbit(flag,stf_moveON| stf_YangleSAME | stf_catchON)
	p_set_pointer(skelanime,RCP_RabbitAnime)
	p_setd(objname,OBJNAME_TAKE)
	p_BGcheckYset
	p_sethitbox(50,75)
	p_hitON
	p_program(s_rabbit_init)

	p_while
		p_program(s_rabbit_main)
	p_loop

/*################*/
#else
/*################*/


/*==================================================================================================
		C Program
 ===================================================================================================*/
#define rabbit_flag 	(execstp->s[stw_work0].d)
#define rabbit_rail 	(execstp->s[stw_work1].d)
#define rabbit_speed 	(execstp->s[stw_work8].f)

#define SELECTROOM		-1
#define RABBIT1			15
#define RABBIT2			50
#define RABBIT1_SPEED	40
#define RABBIT2_SPEED	45
#define RAIL_NUM		10
#define RABBIT_MESS1	84
#define RABBIT_MESS2	162

extern short* rabbit_raildata[];

/***************************************************************************************************
						    Init
***************************************************************************************************/
extern void s_rabbit_init(void)
{
	unsigned char star_flag = BuGetStarFlag(activePlayerNo-1,SELECTROOM);

	if (BuGetTotalStars(activePlayerNo-1) >= RABBIT1 && (star_flag & 0x08)==0 ) {
		obj_programselect = 0;
		rabbit_speed = RABBIT1_SPEED;
	}
	else if (BuGetTotalStars(activePlayerNo-1) >= RABBIT2 && (star_flag & 0x10)==0 ) {
		obj_programselect = 1;
		rabbit_speed = RABBIT2_SPEED;
	}
	else {
		execstp->status = 0;
	}

	execstp->s[stw_enemyinfo].d = ENEMYINFO_FRIEND_ENEMY;

	execstp->s[stw_gravity].f   = 15.;
	execstp->s[stw_friction].f  = 0.89;
	execstp->s[stw_specificG].f = 1.2;
	stSetSkelAnimeNumber(0);

}

/***************************************************************************************************
						Rabbit Stay
***************************************************************************************************/
static short rabbit_RailChose(void)
{
	char i;
	short posx,posy,posz;
	short num = -1;
	float len = -10000;
	float plength;
	short** rail  = (short **)(SegmentToVirtual(rabbit_raildata));
	short* rail_data;

	for (i=0; i<RAIL_NUM; i++) {
		rail_data = (short *)(SegmentToVirtual(rail[i]));
		posx = *(rail_data + 1);
		posy = *(rail_data + 2);
		posz = *(rail_data + 3);

		if (ObjApproach(execstp,posx,posy,posz,800)){
			plength = (posx-player_posX)*(posx-player_posX)+(posz-player_posZ)*(posz-player_posZ);
			if ( len < plength ) {
				num = i;
				len = plength;
			}
		}

	}
	
	rabbit_rail = num;

	return((short)rabbit_rail);

}
/***************************************************************************************************
						Rabbit Stay
***************************************************************************************************/
static void RabbitStay(void)
{
	short bg_flag = 0;

	obj_speedF = 0;
	bg_flag = ObjMoveEvent();


	if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,500)) {
		if (rabbit_RailChose()== -1) {
			obj_mode = 2;
		}
		else {
			stSetSkelAnimeNumber(1);
			obj_mode = 1;
		}
	}



}

/***************************************************************************************************
						Rabbit Run Away
***************************************************************************************************/
static void RabbitRunaway(void)
{

	short bg_flag = 0;
	int	  lift_flag;
	short** rail  = (short **)(SegmentToVirtual(rabbit_raildata));
	short* rail_data  = (short *)(SegmentToVirtual(rail[rabbit_rail]));


	road_root(execstp) = rail_data;
	lift_flag = s_road_move(lift_flag);

	obj_speedF = rabbit_speed;

	obj_angleY = road_angleY(execstp);

	bg_flag = ObjMoveEvent();

	if (lift_flag == -1) {
		stSetSkelAnimeNumber(0);
		obj_mode = 0;

	}

	if (s_check_animeend()==1 && (bg_flag & OM_WATER)) {
		objsound(NA_SE3_USAGI_JUMP_W);
		s_makeobj_nowpos(execstp,S_NULL,e_jumpsplash);
	}
	else if (s_check_animeend()==1) {
		objsound(NA_SE3_USAGI_JUMP_F);
	}

}

/***************************************************************************************************
						Rabbit Down Event
***************************************************************************************************/
static void RabbitDown(void)
{

	if (s_check_animeend()==1) {
		stSetSkelAnimeNumber(0);
		obj_mode = 4;
	}

}
/***************************************************************************************************
						Rabbit Throw Event
***************************************************************************************************/
static void RabbitThought(void)
{
	short bg_flag = 0;
		
	bg_flag = ObjMoveEvent();

	execstp->map.skelanim.frame = 0;

	if ((bg_flag & 0x01) == 1) {
		obj_mode = 2;
		execstp->s[stw_flag].d |= stf_YangleSAME; 
		obj_angleY = obj_animeangleY;
		if (bg_flag & OM_WATER)s_makeobj_nowpos(execstp,S_NULL,e_jumpsplash);
	}



}
/***************************************************************************************************
						Rabbit Stay
***************************************************************************************************/
static void RabbitLose(void)
{
	short bg_flag = 0;

	obj_speedF = 0;
	bg_flag = ObjMoveEvent();

	if (rabbit_flag==1) {
		s_make_extstar(obj_programselect+3);
		rabbit_flag=2;
	}
}
/***************************************************************************************************
						Rabbit Consider Event
***************************************************************************************************/
static void RabbitConsiderEvent(void)
{

	switch (obj_mode) {
		case 0:	RabbitStay();			break;
		case 1:	RabbitRunaway(); 		break;
		case 2:	RabbitDown(); 			break;
		case 3:	RabbitThought(); 		break;
		case 4:	RabbitLose();			break;
	}	
}

/***************************************************************************************************/
/***************************************************************************************************
						Rabbit Catch
***************************************************************************************************/
static void RabbitCatch(void)
{
	short messageNo;


	MapHideShape(&execstp->map);
	stSetSkelAnimeNumber(4);
	s_posoffset_mother(player1stp,0,60,100);
	s_hitOFF();

	if (rabbit_flag==0) {
		if (obj_programselect == 0) messageNo = RABBIT_MESS1;
		else						messageNo = RABBIT_MESS2;

		if (CtrlPlayerDialog(DLOG_LOOKFRONT)==DLOG_RESULT_READY) {
			Mbitset( execstp->status,OBJECT_DEMOMOVEENTRY );				//kaijo only me
			if (cameraDemoStratMsgNum(CAM_DEMO_TALK,execstp,messageNo) != 0) {
				Mbitset(obj_enemyinfo,ENEMYINFO_CATCH_OFF_REQUEST);
				Mbitclr( execstp->status,OBJECT_DEMOMOVEENTRY );
				rabbit_flag = 1;
				CtrlPlayerDialog(DLOG_DONE);
			}
		}

	}
}
/***************************************************************************************************
						Rabbit Drop
***************************************************************************************************/
static void RabbitDrop(void)
{

	s_mode_drop();	
	MapDispShape(&execstp->map);
	stSetSkelAnimeNumber(0);
	execstp->s[stw_actionmode].d = CHILEDMODE_NO_CATCH;
	s_hitON();

	obj_speedF = 3;

	obj_mode = 4;

}
/***************************************************************************************************
						Rabbit Throw
***************************************************************************************************/
static void RabbitThrow(void)
{

	s_throw_object();	
	MapDispShape(&execstp->map);
	execstp->s[stw_actionmode].d = CHILEDMODE_NO_CATCH;
	execstp->s[stw_flag].d &= (stf_YangleSAME^0xffffffff); 
	stSetSkelAnimeNumber(2);
	s_hitON();
	
	execstp->s[stw_speedF].f = 25.;
	execstp->s[stw_speedY].f = 20.;
	obj_mode = 3;
}
/***************************************************************************************************
						main
***************************************************************************************************/
extern void s_rabbit_main(void)
{

	switch(execstp->s[stw_actionmode].d) {
		case CHILEDMODE_NO_CATCH:	RabbitConsiderEvent();	break;
		case CHILEDMODE_CATCH:		RabbitCatch();			break;
		case CHILEDMODE_THROW:		RabbitThrow();			break;
		case CHILEDMODE_DROP:		RabbitDrop();			break;
	}



}






/*################*/
#endif
/*################*/


