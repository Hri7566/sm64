/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 31 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage31


/* ================================================================================
		: Stage 31 BG parts sequence.
===================================================================================	*/
SEQ_Stage31_BGParts:

		seqActor(S_NULL,   3996,-2739, 5477,  0,0,0,  0, 82,0,  e_bar)
		seqActor(S_NULL,  -2911, 3564,-3967,  0,0,0,  0, 84,0,  e_bar)
		seqActor(S_NULL,  -3258, 3359,-3946,  0,0,0,  0,105,0,  e_bar)
		seqActor(S_NULL,  -2639, 3154,-4369,  0,0,0,  0,125,0,  e_bar)
		seqActor(S_NULL,  -2980, 4048,-4248,  0,0,0,  0, 36,0,  e_bar)
		seqActor(S_NULL,  -3290, 3636,-4477,  0,0,0,  0, 77,0,  e_bar)

		seqReturn


/* ================================================================================
		: Stage 31 Stars sequence.
===================================================================================	*/
SEQ_Stage31_Stars:

		seqActor(S_NULL,  -160,1950,-470,  0,0,0,  0,0,0,  e_tripstar_getcoins)  	/* 8 coins	*/

		seqReturn


/* ================================================================================
		: Stage 31 main sequence.
===================================================================================	*/
SEQ_DoStage31:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1 	  , _GfxStage31SegmentRomStart	  , _GfxStage31SegmentRomEnd	)
	seqLoadPres(SEGMENT_BACK	  , _BackMountainSegmentRomStart  , _BackMountainSegmentRomEnd	)
	seqLoadText(SEGMENT_TEXTURE	  , _EMtTextureSegmentRomStart	  , _EMtTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1    ,	_GfxEnemy1_bSegmentRomStart	  , _GfxEnemy1_bSegmentRomEnd 	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_bSegmentRomStart	  , _HmsEnemy1_bSegmentRomEnd 	)
	seqLoadPres(SEGMENT_ENEMY2    ,	_GfxEnemy2_hSegmentRomStart	  , _GfxEnemy2_hSegmentRomEnd 	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_hSegmentRomStart	  , _HmsEnemy2_hSegmentRomEnd 	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_b)
		seqCall(SetEnemy2_h)

		seqBeginScene(1, RCP_Stage31Scene1)
			seqActor(S_NULL,  -67,1669+START_H,-16,  0,270,0,  0,10,0,	 e_player_landing)
			seqPort(10, 31, 1, 10)				/*	MARIO stage in.			*/
			seqGameClear(6, 2,  56)				/*	Stage clear.			*/	
			seqGameOver (6, 2, 109)				/*	Died.					*/
			seqCourseOut(16, 1, 10)				/*	Course out. Not die.	*/
			seqCall(SEQ_Stage31_BGParts)
			seqCall(SEQ_Stage31_Stars)
			seqMapInfo(cx3101_info)
			seqTagInfo(cx3101_info_tag)
			seqSetMusic(NA_STG_GROUND, NA_SLIDER_BGM)
			seqEnvironment(ENV_SNOWMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 270,  -67,1669,-16)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
