/********************************************************************************
	playerstat.h: Ultra 64 MARIO Brothers include file

	Copyright (c) 1996 Nintendo co., ltd.  All rights reserved

	May 10, 1996
 ********************************************************************************/

#ifndef PLAYER_STAT_H
#define	PLAYER_STAT_H

#define	PLAYER_SMOKE		(1<< 0)
#define	PLAYER_WALLCLASH	(1<< 1)
#define	PLAYER_SLEEP		(1<< 2)
#define	PLAYER_TWINKLE		(1<< 3)
#define	PLAYER_CLASH		(1<< 4)
#define	PLAYER_BUBBLE		(1<< 5)
#define	PLAYER_DIVING		(1<< 6)
#define	PLAYER_RIPPLE		(1<< 7)
#define	PLAYER_SINK			(1<< 8)
#define	PLAYER_BUBBLEJET	(1<< 9)
#define	PLAYER_WAVE			(1<<10)
#define	PLAYER_FIRESMOKE	(1<<11)
#define	PLAYER_JUMPSINK		(1<<12)
#define	PLAYER_LEAF			(1<<13)

#define	PLAYER_SNOW			(1<<14)
#define	PLAYER_SAND			(1<<15)
#define	PLAYER_MOWAN		(1<<16)
#define	PLAYER_BREATH		(1<<17)
#define	PLAYER_PUNCH		(1<<18)

#endif
