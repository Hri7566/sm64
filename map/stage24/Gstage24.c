/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage24 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 11, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE24_FOG_R		0
#define		STAGE24_FOG_G		0
#define		STAGE24_FOG_B		0
#define		STAGE24_FOG_START	980

#include "mountain/mountain_ext_texture.h"

#include "mountain/ca_n_body_shape.sou"
#include "mountain/ca_n_dodai_shape.sou"
#include "mountain/ca_n_seal_shape.sou"
#include "mountain/ca_n_kabe_shape.sou"

#include "mountain/ca_n_bg01_shape.sou"
#include "mountain/ca_f_bg01_shape.sou"
#include "mountain/ca_n_bg02_shape.sou"
#include "mountain/ca_f_bg02_shape.sou"
#include "mountain/ca_n_bg03_shape.sou"
#include "mountain/ca_n_bg04_shape.sou"
#include "mountain/ca_n_bg05_shape.sou"
#include "mountain/ca_n_bg06_shape.sou"
#include "mountain/ca_f_bg06_shape.sou"
#include "mountain/ca_n_bg07_shape.sou"
#include "mountain/ca_n_bg08_shape.sou"
#include "mountain/ca_f_bg08_shape.sou"
#include "mountain/ca_n_bg10_shape.sou"
#include "mountain/ca_n_stick_shape.sou"
#include "mountain/ca_n_bg12_o_shape.sou"
#include "mountain/ca_f_bg12_o_shape.sou"
#include "mountain/ca_n_bg12_t_shape.sou"
#include "mountain/ca_n_bg13_shape.sou"
#include "mountain/ca_n_bg14_shape.sou"
#include "mountain/ca_n_bg15_shape.sou"
#include "mountain/ca_n_bg16_shape.sou"

#include "mountain/ca_n_dossunbar_shape.sou"
#include "mountain/ca_f_dossunbar_shape.sou"
#include "mountain/ca_n_dossunbar2_shape.sou"
#include "mountain/ca_n_downbar_shape.sou"
#include "mountain/ca_f_downbar_shape.sou"
#include "mountain/ca_n_rotebar_shape.sou"
#include "mountain/ca_f_rotebar_shape.sou"
#include "mountain/ca_n_transbar50_shape.sou"
#include "mountain/ca_n_lift0_shape.sou"
#include "mountain/ca_n_lift1_shape.sou"
#include "mountain/ca_n_moveobj01_shape.sou"
#include "mountain/ca_n_moveobj02_shape.sou"
#include "mountain/ca_n_moveobj03_shape.sou"
#include "mountain/ca_n_moveobj04_shape.sou"

#include "mountain/dossun_bar.flk"
#include "mountain/dossun_bar2.flk"
#include "mountain/rote_bar.flk"
#include "mountain/transbar_50.flk"
#include "mountain/down_b.flk"
#include "mountain/lift_0fan.flk"
#include "mountain/lift_0_3.flk"
#include "mountain/ca_moveobj01.flk"
#include "mountain/ca_moveobj02.flk"
#include "mountain/ca_moveobj03.flk"
#include "mountain/ca_moveobj04.flk"
#include "mountain/ca_mobj14_check.flk"

#include "mountain/ca_n_bg06_check.flk"
#include "mountain/ca_n_bg07_check.flk"

#include "mountain/cx2401.flk"
#include "mountain/cx2401.tag"


/********************************************************************************/
/*	Water surface records in 24 - 1.											*/
/********************************************************************************/
static short waterSurf_24_01_00[] = {
	1,
	0,   10,  10,  -1023,1024,  -1023,4096,  3226,4096,  3226,1024,  0,  120,  0,
};

extern WaterSurfRec	waterSurf_24_01[] = {
		{	 0, waterSurf_24_01_00	 },
		{	-1,	NULL	}					//	End of  WaterSurfRec data.
};
