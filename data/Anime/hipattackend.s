/********************************************************************************
						Ultra 64 MARIO Brothers

				   mario squat waiting animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							November 2, 1995
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioHipAttackEnd:
	.half	 MAP_ANIM_ONETIME 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	 6 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x028A						/* total bytes       */


	.half	    1,    0 	/* chn14translate x */
	.half	    6,    1 	/* chn14 translate y */
	.half	    6,    7 	/* chn14 translate z */
	.half	    1,    0 	/* chn14 rotate x */
	.half	    1,   13 	/* chn14 rotate y */
	.half	    1,    0 	/* chn14 rotate z */
	.half	    1,    0 	/* <m_waist>1_3 rotate x */
	.half	    1,    0 	/* <m_waist>1_3 rotate y */
	.half	    6,  181 	/* <m_waist>1_3 rotate z */
	.half	    6,  163 	/* <m_body>1 rotate x */
	.half	    6,  169 	/* <m_body>1 rotate y */
	.half	    6,  175 	/* <m_body>1 rotate z */
	.half	    6,  145 	/* <m_head>2 rotate x */
	.half	    6,  151 	/* <m_head>2 rotate y */
	.half	    6,  157 	/* <m_head>2 rotate z */
	.half	    1,  141 	/* chn6 rotate x */
	.half	    1,  142 	/* chn6 rotate y */
	.half	    1,  143 	/* chn6 rotate z */
	.half	    6,   23 	/* <m_larmA>1 rotate x */
	.half	    6,   29 	/* <m_larmA>1 rotate y */
	.half	    6,   35 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1 rotate x */
	.half	    1,    0 	/* <m_larmB>1 rotate y */
	.half	    6,   17 	/* <m_larmB>1 rotate z */
	.half	    1,   14 	/* <m_lhand>1 rotate x */
	.half	    1,   15 	/* <m_lhand>1 rotate y */
	.half	    1,   16 	/* <m_lhand>1 rotate z */
	.half	    1,  137 	/* chn10 rotate x */
	.half	    1,  138 	/* chn10 rotate y */
	.half	    1,  139 	/* chn10 rotate z */
	.half	    6,   65 	/* <m_rarmA>1 rotate x */
	.half	    6,   71 	/* <m_rarmA>1 rotate y */
	.half	    6,   77 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1 rotate x */
	.half	    1,    0 	/* <m_rarmB>1 rotate y */
	.half	    6,   59 	/* <m_rarmB>1 rotate z */
	.half	    6,   41 	/* <m_rhand>1 rotate x */
	.half	    6,   47 	/* <m_rhand>1 rotate y */
	.half	    6,   53 	/* <m_rhand>1 rotate z */
	.half	    1,    0 	/* chn15 rotate x */
	.half	    1,    0 	/* chn15 rotate y */
	.half	    1,  140 	/* chn15 rotate z */
	.half	    6,  119 	/* <m_llegA>1 rotate x */
	.half	    6,  125 	/* <m_llegA>1 rotate y */
	.half	    6,  131 	/* <m_llegA>1 rotate z */
	.half	    1,    0 	/* <m_llegB>1 rotate x */
	.half	    1,    0 	/* <m_llegB>1 rotate y */
	.half	    6,  113 	/* <m_llegB>1 rotate z */
	.half	    1,  110 	/* <m_ltoot>1 rotate x */
	.half	    1,  111 	/* <m_ltoot>1 rotate y */
	.half	    1,  112 	/* <m_ltoot>1 rotate z */
	.half	    1,    0 	/* chn17 rotate x */
	.half	    1,    0 	/* chn17 rotate y */
	.half	    1,  144 	/* chn17 rotate z */
	.half	    6,   92 	/* <m_rlegA>1 rotate x */
	.half	    6,   98 	/* <m_rlegA>1 rotate y */
	.half	    6,  104 	/* <m_rlegA>1 rotate z */
	.half	    1,    0 	/* <m_rlegB>1 rotate x */
	.half	    1,    0 	/* <m_rlegB>1 rotate y */
	.half	    6,   86 	/* <m_rlegB>1 rotate z */
	.half	    1,   83 	/* <m_rfoot>1 rotate x */
	.half	    1,   84 	/* <m_rfoot>1 rotate y */
	.half	    1,   85 	/* <m_rfoot>1 rotate z */

	.half	0x0000,0x0034,0x0035,0x0035,0x0035,0x0036,0x0036,0xFFFE,0xFFFE,0xFFFF,0x0000,0x0000,0x0000,0x3FFF,0xFF8A,0xF980
	.half	0xF06F,0xBED0,0xC13B,0xC6FD,0xCDDD,0xD39F,0xD60A,0xC400,0xC064,0xB7CA,0xAD85,0xA4EB,0xA150,0xD58C,0xD49D,0xD264
	.half	0xCFBD,0xCD84,0xCC95,0x1569,0x1B00,0x2853,0x383B,0x458F,0x4B26,0x00AD,0x009B,0x0070,0x003D,0x0012,0x0000,0xFCD0
	.half	0xFD25,0xFDF0,0xFEE1,0xFFAC,0x0000,0xE4F6,0xE5DB,0xE7FC,0xEA86,0xECA7,0xED8C,0xC626,0xC865,0xCDC0,0xD424,0xD97F
	.half	0xDBBE,0x4BA5,0x48E4,0x4252,0x3A7A,0x33E9,0x3127,0x1FD6,0x21AF,0x2617,0x2B59,0x2FC1,0x319A,0x340F,0x3134,0x2A67
	.half	0x2249,0x1B7B,0x18A1,0x001B,0x025B,0xCB25,0x385D,0x3327,0x26B9,0x17E4,0x0B76,0x0640,0xF7EF,0xF8BF,0xFAB1,0xFD03
	.half	0xFEF5,0xFFC5,0x07D6,0x06E2,0x049D,0x01E8,0xFFA5,0xFEB1,0x7ACB,0x7E42,0x8689,0x9066,0x98AA,0x9C21,0x0095,0xFF09
	.half	0xCC42,0x3426,0x2FBE,0x253C,0x18B1,0x0E2F,0x09C7,0xFFCB,0xFFD1,0xFFDE,0xFFEE,0xFFFB,0x0000,0xF3CA,0xF51A,0xF83C
	.half	0xFBFA,0xFF1C,0x006C,0x8661,0x892C,0x8FD3,0x97C5,0x9E6C,0xA137,0x8001,0x8001,0x4171,0xBF5F,0x8001,0x8001,0x4171
	.half	0xBF5F,0x011E,0x0100,0x00B9,0x0064,0x001D,0x0000,0x030F,0x0315,0x0321,0x0330,0x033D,0x0342,0x2662,0x24E6,0x215A
	.half	0x1D1E,0x1992,0x1815,0xFFF9,0xFFFA,0xFFFC,0xFFFE,0x0000,0x0000,0xFCBC,0xFCBC,0xFCBD,0xFCBD,0xFCBE,0xFCBE,0x144D
	.half	0x1443,0x142B,0x140E,0x13F6,0x13EC,0x1B25,0x1B7C,0x1C4B,0x1D42,0x1E11,0x1E68
