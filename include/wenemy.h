/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: wenemy.h
					Description	: 
					Date		: 1995.1.8
					Author		: 

 ********************************************************************************/


/********************************************************************************
		macros
 ********************************************************************************/
/********************************************************************************
		structure
 ********************************************************************************/
/********************************************************************************
		extern
 ********************************************************************************/

extern ulong	e_noko[];
extern void	s_noko(void);

extern ulong	e_kamegoal[];
extern void	s_kamegoal(void);

extern ulong	e_goal_flag[];

extern ulong	e_sanbo[];
extern ulong	e_sanbo_head[];
extern ulong	e_sanbo_body[];
extern void		s_sanbo(void);
extern void		s_sanbo_head(void);
extern void		s_sanbo_body(void);

extern ulong	e_bat[];
extern void	s_bat(void);

extern ulong	e_heyho[];
extern void	s_heyho(void);

extern ulong	e_kuribo[];
extern ulong	e_kuribo_set[];
extern void	s_kuribo_initial(void);
extern void	s_kuribo(void);
extern void	s_kuribo_set(void);

extern ulong	e_wanwan[];
extern ulong	e_wanwan_chn[];
extern void		s_wanwan(void);
extern void		s_wanwan_chn(void);

extern ulong	e_wanwan_fence[];
extern void	s_wanwan_fence_initial(void);
extern void	s_wanwan_fence(void);

extern ulong	e_kui[];
extern void	s_kui(void);

extern ulong	e_hanachan[];
extern ulong	e_hanachan_body[];
extern void		s_hanachan(void);
extern void		s_hanachan_body(void);

extern ulong	e_jugem[];
extern ulong	e_camera_jugem[];
extern void	s_jugem(void);
extern void	s_camera_jugem_initial(void);
extern void	s_camera_jugem(void);

extern ulong	e_cloud[];
extern ulong	e_cloud_child[];
extern void		s_cloud(void);
extern void		s_cloud_child(void);

extern ulong	e_indy[];
extern void	s_indy_initial(void);
extern void	s_indy(void);

extern ulong	e_hole[];
extern void	s_hole(void);

extern ulong	e_stone[];
extern void	s_stone(void);

extern ulong	e_linelift[];
extern ulong	e_linelift_mark[];
extern void		s_linelift_initial(void);
extern void		s_linelift(void);
extern void		s_linelift_mark(void);

extern ulong	e_togezo[];
extern void	s_togezo(void);

extern ulong	e_seesaw[];
extern ulong	e_seesaw_s17[];
extern void		s_seesaw_initial(void);
extern void		s_seesaw(void);

extern ulong	e_pedallift[];
extern ulong	e_pedallift_child[];
extern void		s_pedallift_initial(void);
extern void		s_pedallift_child(void);

extern ulong	e_waterbom[];
extern ulong	e_waterbom_child[];
extern ulong	e_waterbom_shadow[];
extern void		s_waterbom(void);
extern void		s_waterbom_child(void);
extern void		s_waterbom_shadow(void);

extern ulong	e_tokeirot[];
extern void	s_tokeirot_initial(void);
extern void	s_tokeirot(void);

extern ulong	e_tokeiswing[];
extern void	s_tokeiswing_initial(void);
extern void	s_tokeiswing(void);

extern ulong	e_tokeiconv[];
extern void	s_tokeiconv_initial(void);
extern void	s_tokeiconv(void);

extern ulong	e_tokeibiyon[];
extern void	s_tokeibiyon_initial(void);
extern void	s_tokeibiyon(void);

extern ulong	e_tokeikurukuru[];
extern void	s_tokeikurukuru_initial(void);
extern void	s_tokeikurukuru(void);

extern ulong	e_tokeiudblock[];
extern void	s_tokeiudblock_initial(void);
extern void	s_tokeiudblock(void);

extern ulong	e_tokeiudlift[];
extern void	s_tokeiudlift_initial(void);
extern void	s_tokeiudlift(void);

extern ulong	e_tokeisecondhand[];
extern void	s_tokeisecondhand_initial(void);
extern void	s_tokeisecondhand(void);

extern ulong	e_tokeipinwheel[];
extern void	s_tokeipinwheel(void);

extern ulong	e_snowman[];
extern void	s_snowman_initial(void);
extern void	s_snowman(void);

extern ulong	e_snowball[];
extern void	s_snowball(void);

extern ulong	e_derublock[];
extern void	s_derublock_initial(void);
extern void	s_derublock(void);

extern ulong	e_rotstand[];
extern void	s_rotstand_initial(void);
extern void	s_rotstand(void);

extern ulong	e_stepslope[];
extern void	s_stepslope_initial(void);
extern void	s_stepslope(void);

extern ulong	e_ridestartlift[];
extern void	s_ridestartlift_initial(void);
extern void	s_ridestartlift(void);

extern ulong	e_hart[];
extern void	s_hart(void);

extern ulong	e_watercanon[];
extern ulong	e_watercanon_child[];
extern void		s_watercanon(void);
extern void		s_watercanon_child(void);

extern ulong	e_ubboo[];
extern ulong	e_ubboo_dummy[];
extern void	s_ubboo_initial(void);
extern void	s_ubboo(void);
extern void	s_ubboo_dummy(void);

extern ulong	e_nessy[];
extern void	s_nessy(void);

extern ulong	e_chair[];
extern void	s_chair_initial(void);
extern void	s_chair(void);

extern ulong	e_piano[];
extern void	s_piano(void);

extern ulong	e_book[];
extern void	s_book(void);

extern ulong	e_book_shot[];
extern void	s_book_shot(void);

extern ulong	e_book_boss[];
extern ulong	e_book_boss_child[];
extern void		s_book_boss(void);
extern void		s_book_boss_child(void);

extern ulong	e_firepakun[];
extern ulong	e_fire_shot[];
extern ulong	e_firepakun_ball[];
extern ulong	e_firepakun_ball_effect[];
extern void		s_firepakun_initial(void);
extern void		s_firepakun(void);
extern void		s_fire_shot(void);
extern void		s_firepakun_ball(void);
extern void		s_firepakun_ball_effect(void);

extern ulong	e_mucho[];
extern ulong	e_mucho_ball[];
extern void	s_mucho(void);
extern void	s_mucho_ball(void);

extern ulong	e_jiro[];
extern void	s_jiro_initial(void);
extern void	s_jiro(void);

extern ulong	e_handman[];
extern ulong	e_handman_hand[];
extern void	s_handman(void);
extern void	s_handman_hand(void);

extern ulong	e_condor[];
extern void	s_condor_initial(void);
extern void	s_condor(void);

extern ulong	e_minibird[];
extern void	s_minibird(void);

extern ulong	e_racepengin[];
extern ulong	e_pengingoal[];
extern ulong	e_penginjudge[];
extern void	s_racepengin_initial(void);
extern void	s_racepengin(void);
extern void	s_pengingoal(void);
extern void	s_penginjudge(void);

extern ulong	e_kanoke[];
extern ulong	e_kanoke_child[];
extern void	s_kanoke(void);
extern void	s_kanoke_child(void);

extern ulong	e_bigshell[];
extern void	s_bigshell(void);

extern ulong	e_amembow[];
extern ulong	e_amembow_ripple[];
extern void	s_amembow(void);
extern void	s_amembow_ripple(void);

extern ulong	e_swinglift[];
extern void	s_swinglift_initial(void);
extern void	s_swinglift(void);

extern ulong	e_chikuwa[];
extern ulong	e_chikuwa_child[];
extern void	s_chikuwa(void);
extern void	s_chikuwa_child(void);

extern ulong	e_movebar[];
extern void	s_movebar_initial(void);
extern void	s_movebar(void);

extern ulong	e_starpanel[];
extern void	s_starpanel_initial(void);

extern ulong	e_fly[];
extern void	s_fly(void);

extern ulong	e_buku[];
extern void	s_buku(void);


#if 0
extern ulong	e_ubboo_point[];
extern void	s_ubboo_point(void);
#endif

/*==============================================================================*/


