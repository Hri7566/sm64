/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage16 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx gfx_main_castle[];
extern Gfx gfx_main_field[]	;
extern Gfx gfx_main_fence[]	;
extern Gfx gfx_main_shade[]	;
extern Gfx gfx_main_window[];
extern Gfx gfx_main_carpet[];
extern Gfx gfx_main_cannon[];

extern Gfx gfx_main_tower[]	;
extern Gfx gfx_main_net[]	;
extern Gfx gfx_main_net2[]	;

extern Gfx RCP_castle_flag0[];
extern Gfx RCP_castle_flag1[];
extern Gfx RCP_castle_flag2[];
extern Gfx RCP_castle_flag3[];
extern Gfx RCP_castle_flag4[];

/********************************************************************************/
/*	Hierarchy map data of castle_flag.											*/
/********************************************************************************/
Hierarchy RCP_Hmscastle_flag[] = {
 hmsHeader(1000)
 hmsBegin()
 hmsScale(0.375f)
    hmsBegin()
      hmsJoint(RM_SURF,NULL,0,0,0)
      hmsBegin()
         hmsJoint(RM_SURF,RCP_castle_flag4,0,0,0)  /* face6(4) */
         hmsBegin()
            hmsJoint(RM_SURF,RCP_castle_flag3,459,0,0)  /* face5(3) */
            hmsBegin()
               hmsJoint(RM_SURF,RCP_castle_flag2,460,0,0)  /* face4(2) */
               hmsBegin()
                  hmsJoint(RM_SURF,RCP_castle_flag1,345,0,0)  /* face3(1) */
                  hmsBegin()
                     hmsJoint(RM_SURF,RCP_castle_flag0,287,0,0)  /* face2(0) */
                  hmsEnd()
               hmsEnd()
            hmsEnd()
         hmsEnd()
      hmsEnd()
   hmsEnd()
 hmsEnd()
 hmsEnd()
 hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of MainTower.											*/
/********************************************************************************/
Hierarchy RCP_HmsMainTower[] = { 
	hmsHeader(2100)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_main_tower)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of MainNet.												*/
/********************************************************************************/
Hierarchy RCP_HmsMainNet[] = { 
	hmsHeader(15000)
	hmsBegin()
		hmsGfx(RM_SPRITE, gfx_main_net)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map data of MainNet2.												*/
/********************************************************************************/
Hierarchy RCP_HmsMainNet2[] = { 
	hmsHeader(15000)
	hmsBegin()
		hmsGfx(RM_SPRITE, gfx_main_net2)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage16Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(0, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, 20000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON_O,  0,1500,2500,  0,1500,-12000, GameCamera)
				hmsBegin()

					hmsGfx(RM_SURF	, gfx_main_castle)
					hmsGfx(RM_SURF	, gfx_main_field)
					hmsGfx(RM_SPRITE, gfx_main_fence)
					hmsGfx(RM_XDECAL, gfx_main_shade)
					hmsGfx(RM_SPRITE, gfx_main_window)
					hmsGfx(RM_DECAL	, gfx_main_carpet)
					hmsGfx(RM_SURF  , gfx_main_cannon)

					hmsCProg(0	   , WaterInit)
					hmsCProg(0x1601, WaterFall)
					hmsCProg(0x1601, WaterDraw)

					hmsObject()
					hmsCProg(0, WeatherProc)

				hmsEnd()
			hmsEnd()
		hmsEnd()

		hmsLayer(0)
		hmsBegin()
			hmsCProg(0, CannonEdge)
		hmsEnd()

	hmsEnd()
	hmsExit()
};
