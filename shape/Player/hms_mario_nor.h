/********************************************************************************
						Ultra 64 MARIO Brothers

				   Normal mario hierarchy data module

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							April 22, 1996
 ********************************************************************************/




/*################################################################################
 *
 *
 *	Near Mario
 *
 *
 */

/********************************************************************************/
/*	Near mario head hierarchy map data.											*/
/********************************************************************************/

static Hierarchy RCP_NOR_NearMarioHead[] = {
	hmsCProg(0, CtrlMarioHead)
	hmsRotate(0, 0, 0)
	hmsBegin()
		hmsSelect(0, CtrlMarioCap)
		hmsBegin()
			hmsSelect(0, CtrlMarioEye)					/* for the eye animation	*/
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_head1)
				hmsGfx(RM_SURF, RCP_mario_head2)
				hmsGfx(RM_SURF, RCP_mario_head3)
				hmsGfx(RM_SURF, RCP_mario_head4)
				hmsGfx(RM_SURF, RCP_mario_head5)
				hmsGfx(RM_SURF, RCP_mario_head6)
				hmsGfx(RM_SURF, RCP_mario_head7)
				hmsGfx(RM_SURF, RCP_mario_head8)
			hmsEnd()

			hmsSelect(0, CtrlMarioEye)					/* for the eye animation	*/
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_nohat1)
				hmsGfx(RM_SURF, RCP_mario_nohat2)
				hmsGfx(RM_SURF, RCP_mario_nohat3)
				hmsGfx(RM_SURF, RCP_mario_nohat4)
				hmsGfx(RM_SURF, RCP_mario_nohat5)
				hmsGfx(RM_SURF, RCP_mario_nohat6)
				hmsGfx(RM_SURF, RCP_mario_nohat7)
				hmsGfx(RM_SURF, RCP_mario_nohat8)
			hmsEnd()
		hmsEnd()

		hmsCoord(142,-51,-126, 22,-40,-135)
		hmsBegin()
			hmsCProg(0, CtrlMarioWing)
			hmsRotate(0,0,0)
			hmsBegin()
				hmsGfx(RM_SPRITE, RCP_move_wing)
			hmsEnd()
		hmsEnd()

		hmsCoord(142,-51,126, -22,40,-135)
		hmsBegin()
			hmsCProg(1, CtrlMarioWing)
			hmsRotate(0,0,0)
			hmsBegin()
				hmsGfx(RM_SPRITE, RCP_move_wing)
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Near mario left hand hierarchy map data.									*/
/********************************************************************************/

static Hierarchy RCP_NOR_NearMarioLeftHand[] = {
	hmsSelect(1, CtrlMarioHand)
	hmsBegin()

		hmsJoint(RM_SURF, NULL, 60, 0, 0)
		hmsBegin()
			hmsCProg(1, CtrlHandScale)
			hmsScale(1.0f)
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_near0)			/* m_lhamd1		*/
			hmsEnd()
		hmsEnd()

		hmsJoint(RM_SURF, RCP_swim_hand_l, 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_near0, 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_near0, 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_near0, 60, 0, 0)

	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Near mario left hand hierarchy map data.									*/
/********************************************************************************/

static Hierarchy RCP_NOR_NearMarioRightHand[] = {
	hmsSelect(0, CtrlMarioHand)
	hmsBegin()

		hmsJoint(RM_SURF, NULL, 60, 0, 0)
		hmsBegin()
			hmsCProg(0, CtrlHandScale)
			hmsScale(1.0f)
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_near3)			/* m_rhand1		*/
			hmsEnd()
			hmsSucker(0,0,0, 0,CtrlMarioTaking)
		hmsEnd()

		hmsJoint(RM_SURF, RCP_swim_hand_r, 60, 0, 0)
		hmsBegin()
			hmsSucker(0,0,0, 0,CtrlMarioTaking)
		hmsEnd()

		hmsJoint(RM_SURF, RCP_mario_vsign_hand, 60, 0, 0)

		hmsJoint(RM_SURF, RCP_hat_hand_r, 60, 0, 0)

		hmsJoint(RM_SURF, RCP_hat_hand_r, 60, 0, 0)
		hmsBegin()
			hmsGfx(RM_SPRITE, RCP_hat_hand_wing)
		hmsEnd()

	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Near mario hierarchy map data.												*/
/********************************************************************************/

static Hierarchy RCP_NOR_NearMarioHierarchy[] = {
	hmsJoint(RM_SURF, NULL, 0, 0, 0)										/* chn14		*/
	hmsBegin()
		hmsJoint(RM_SURF, RCP_mario_near14, 0, 0, 0)						/* m_waist1_3	*/
		hmsBegin()

			hmsCProg(0, MarioProc1)
			hmsCProg(0, CtrlMarioWaist)
			hmsRotate(0,0,0)
			hmsBegin()

				hmsJoint(RM_SURF, RCP_mario_near_body, 68, 0, 0)			/* m_body1		*/
				hmsBegin()

					hmsJoint(RM_SURF, NULL, 87, 0, 0)						/* m_head2		*/
					hmsBegin()
						hmsCall(RCP_NOR_NearMarioHead)
					hmsEnd()
	
					hmsJoint(RM_SURF, NULL, 67, -10, 79)					/* chn6			*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_near2, 0, 0, 0)			/* m_larmA1		*/
						hmsBegin()
							hmsJoint(RM_SURF, RCP_mario_near1, 65, 0, 0)	/* m_larmB1		*/
							hmsBegin()
								hmsCall(RCP_NOR_NearMarioLeftHand)
							hmsEnd()
						hmsEnd()
					hmsEnd()

					hmsJoint(RM_SURF, NULL, 68, -10, -79)					/* chn10		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_near5, 0, 0, 0)			/* m_rarmA1		*/
						hmsBegin()
							hmsJoint(RM_SURF, RCP_mario_near4, 65, 0, 0)	/* m_rarmB1		*/
							hmsBegin()
								hmsCall(RCP_NOR_NearMarioRightHand)
							hmsEnd()
						hmsEnd()
					hmsEnd()

				hmsEnd()
			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, 42)								/* chn15		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_near11, 0, 0, 0)				/* m_llegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_near10, 89, 0, 0)			/* m_llegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_near9, 67, 0, 0)		/* m_lfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, -42)							/* chn17		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_near8, 0, 0, 0)					/* m_rlegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_near7, 89, 0, 0)			/* m_rlegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, NULL, 67, 0, 0)
						hmsBegin()
							hmsCProg(2, CtrlHandScale)
							hmsScale(1.0f)
							hmsBegin()
								hmsGfx(RM_SURF, RCP_mario_near6)			/* m_rfoot1		*/
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()
			hmsEnd()

		hmsEnd()
	hmsEnd()
	hmsReturn()
};
















/*################################################################################
 *
 *
 *	Middle Mario
 *
 *
 */

/********************************************************************************/
/*	Middle mario left hand hierarchy map data.									*/
/********************************************************************************/

static Hierarchy RCP_NOR_MidMarioLeftHand[] = {
	hmsSelect(1, CtrlMarioHand)
	hmsBegin()

		hmsJoint(RM_SURF, NULL, 60, 0, 0)
		hmsBegin()
			hmsCProg(1, CtrlHandScale)
			hmsScale(1.0f)
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_mid0)					/* m_rhand1		*/
			hmsEnd()
		hmsEnd()

		hmsJoint(RM_SURF, RCP_swim_hand_l, 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_mid0 , 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_mid0 , 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_mid0 , 60, 0, 0)

	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Middle mario left hand hierarchy map data.									*/
/********************************************************************************/

static Hierarchy RCP_NOR_MidMarioRightHand[] = {
	hmsSelect(0, CtrlMarioHand)
	hmsBegin()

		hmsJoint(RM_SURF, NULL, 60, 0, 0)
		hmsBegin()
			hmsCProg(0, CtrlHandScale)
			hmsScale(1.0f)
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_mid3)					/* m_rhand1		*/
			hmsEnd()
			hmsSucker(0,0,0, 0,CtrlMarioTaking)
		hmsEnd()

		hmsJoint(RM_SURF, RCP_swim_hand_r, 60, 0, 0)
		hmsBegin()
			hmsSucker(0,0,0, 0,CtrlMarioTaking)
		hmsEnd()

		hmsJoint(RM_SURF, RCP_mario_vsign_hand, 60, 0, 0)

		hmsJoint(RM_SURF, RCP_hat_hand_r, 60, 0, 0)

		hmsJoint(RM_SURF, RCP_hat_hand_r, 60, 0, 0)
		hmsBegin()
			hmsGfx(RM_SPRITE, RCP_hat_hand_wing)
		hmsEnd()

	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Middle mario hierarchy map data.											*/
/********************************************************************************/

static Hierarchy RCP_NOR_MidMarioHierarchy[] = {
	hmsJoint(RM_SURF, NULL, 0, 0, 0)										/* chn14		*/
	hmsBegin()
		hmsJoint(RM_SURF, RCP_mario_mid14, 0, 0, 0)							/* m_waist1_3	*/
		hmsBegin()

			hmsCProg(0, MarioProc1)
			hmsCProg(0, CtrlMarioWaist)
			hmsRotate(0,0,0)
			hmsBegin()

				hmsJoint(RM_SURF, RCP_mario_mid_body, 68, 0, 0)				/* m_body1		*/
				hmsBegin()

					hmsJoint(RM_SURF, NULL, 87, 0, 0)						/* m_head2		*/
					hmsBegin()
						hmsCall(RCP_NOR_NearMarioHead)
					hmsEnd()

					hmsJoint(RM_SURF, NULL, 67, -10, 79)					/* chn6			*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_mid2, 0, 0, 0)			/* m_larmA1		*/
						hmsBegin()
							hmsJoint(RM_SURF, RCP_mario_mid1, 65, 0, 0)		/* m_larmB1		*/
							hmsBegin()
								hmsCall(RCP_NOR_MidMarioLeftHand)
							hmsEnd()
						hmsEnd()
					hmsEnd()

					hmsJoint(RM_SURF, NULL, 68, -10, -79)					/* chn10		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_mid5, 0, 0, 0)			/* m_rarmA1		*/
						hmsBegin()
							hmsJoint(RM_SURF, RCP_mario_mid4, 65, 0, 0)		/* m_rarmB1		*/
							hmsBegin()
								hmsCall(RCP_NOR_MidMarioRightHand)
							hmsEnd()
						hmsEnd()
					hmsEnd()

				hmsEnd()
			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, 42)								/* chn15		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_mid11, 0, 0, 0)					/* m_llegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_mid10, 89, 0, 0)			/* m_llegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_mid9, 67, 0, 0)			/* m_lfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, -42)							/* chn17		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_mid8, 0, 0, 0)					/* m_rlegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_mid7, 89, 0, 0)				/* m_rlegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, NULL, 67, 0, 0)
						hmsBegin()
							hmsCProg(2, CtrlHandScale)
							hmsScale(1.0f)
							hmsBegin()
								hmsGfx(RM_SURF, RCP_mario_mid6)			/* m_rfoot1		*/
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()
			hmsEnd()

		hmsEnd()
	hmsEnd()
	hmsReturn()
};
















/*################################################################################
 *
 *
 *	Far Mario
 *
 *
 */

/********************************************************************************/
/*	Far mario head hierarchy map data.											*/
/********************************************************************************/

static Hierarchy RCP_NOR_FarMarioHead[] = {
	hmsCProg(0, CtrlMarioHead)
	hmsRotate(0, 0, 0)
	hmsBegin()
		hmsSelect(0, CtrlMarioCap)
		hmsBegin()
			hmsSelect(0, CtrlMarioEye)					/* for the eye animation	*/
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_far_head1)
				hmsGfx(RM_SURF, RCP_mario_far_head2)
				hmsGfx(RM_SURF, RCP_mario_far_head3)
				hmsGfx(RM_SURF, RCP_mario_far_head4)
				hmsGfx(RM_SURF, RCP_mario_far_head5)
				hmsGfx(RM_SURF, RCP_mario_far_head6)
				hmsGfx(RM_SURF, RCP_mario_far_head7)
				hmsGfx(RM_SURF, RCP_mario_far_head8)
			hmsEnd()

			hmsSelect(0, CtrlMarioEye)					/* for the eye animation	*/
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_far_nohat1)
				hmsGfx(RM_SURF, RCP_mario_far_nohat2)
				hmsGfx(RM_SURF, RCP_mario_far_nohat3)
				hmsGfx(RM_SURF, RCP_mario_far_nohat4)
				hmsGfx(RM_SURF, RCP_mario_far_nohat5)
				hmsGfx(RM_SURF, RCP_mario_far_nohat6)
				hmsGfx(RM_SURF, RCP_mario_far_nohat7)
				hmsGfx(RM_SURF, RCP_mario_far_nohat8)
			hmsEnd()
		hmsEnd()

		hmsCoord(142,-51,-126, 22,-40,-135)
		hmsBegin()
			hmsCProg(0, CtrlMarioWing)
			hmsRotate(0,0,0)
			hmsBegin()
				hmsGfx(RM_SPRITE, RCP_move_wing)
			hmsEnd()
		hmsEnd()

		hmsCoord(142,-51,126, -22,40,-135)
		hmsBegin()
			hmsCProg(1, CtrlMarioWing)
			hmsRotate(0,0,0)
			hmsBegin()
				hmsGfx(RM_SPRITE, RCP_move_wing)
			hmsEnd()
		hmsEnd()

	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Far mario left hand hierarchy map data.										*/
/********************************************************************************/

static Hierarchy RCP_NOR_FarMarioLeftHand[] = {
	hmsSelect(1, CtrlMarioHand)
	hmsBegin()

		hmsJoint(RM_SURF, NULL, 60, 0, 0)
		hmsBegin()
			hmsCProg(1, CtrlHandScale)
			hmsScale(1.0f)
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_far0)					/* m_rhand1		*/
			hmsEnd()
		hmsEnd()

		hmsJoint(RM_SURF, RCP_swim_hand_l, 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_far0 , 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_far0 , 60, 0, 0)
		hmsJoint(RM_SURF, RCP_mario_far0 , 60, 0, 0)

	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Far mario left hand hierarchy map data.										*/
/********************************************************************************/

static Hierarchy RCP_NOR_FarMarioRightHand[] = {
	hmsSelect(0, CtrlMarioHand)
	hmsBegin()

		hmsJoint(RM_SURF, NULL, 60, 0, 0)
		hmsBegin()
			hmsCProg(0, CtrlHandScale)
			hmsScale(1.0f)
			hmsBegin()
				hmsGfx(RM_SURF, RCP_mario_far3)					/* m_rhand1		*/
			hmsEnd()
			hmsSucker(0,0,0, 0,CtrlMarioTaking)
		hmsEnd()

		hmsJoint(RM_SURF, RCP_swim_hand_r, 60, 0, 0)
		hmsBegin()
			hmsSucker(0,0,0, 0,CtrlMarioTaking)
		hmsEnd()

		hmsJoint(RM_SURF, RCP_mario_vsign_hand, 60, 0, 0)

		hmsJoint(RM_SURF, RCP_hat_hand_r, 60, 0, 0)

		hmsJoint(RM_SURF, RCP_hat_hand_r, 60, 0, 0)
		hmsBegin()
			hmsGfx(RM_SPRITE, RCP_hat_hand_wing)
		hmsEnd()

	hmsEnd()
	hmsReturn()
};
/********************************************************************************/
/*	Far mario hierarchy map data.												*/
/********************************************************************************/

static Hierarchy RCP_NOR_FarMarioHierarchy[] = {
	hmsJoint(RM_SURF, NULL, 0, 0, 0)										/* chn14		*/
	hmsBegin()
		hmsJoint(RM_SURF, RCP_mario_far14, 0, 0, 0)							/* m_waist1_3	*/
		hmsBegin()

			hmsCProg(0, MarioProc1)
			hmsCProg(0, CtrlMarioWaist)
			hmsRotate(0,0,0)
			hmsBegin()

				hmsJoint(RM_SURF, RCP_mario_far_body, 68, 0, 0)				/* m_body1		*/
				hmsBegin()

					hmsJoint(RM_SURF, NULL, 87, 0, 0)						/* m_head2		*/
					hmsBegin()
						hmsCall(RCP_NOR_FarMarioHead)
					hmsEnd()

					hmsJoint(RM_SURF, NULL, 67, -10, 79)					/* chn6			*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_far2, 0, 0, 0)			/* m_larmA1		*/
						hmsBegin()
							hmsJoint(RM_SURF, RCP_mario_far1, 65, 0, 0)		/* m_larmB1		*/
							hmsBegin()
								hmsCall(RCP_NOR_FarMarioLeftHand)
							hmsEnd()
						hmsEnd()
					hmsEnd()

					hmsJoint(RM_SURF, NULL, 68, -10, -79)					/* chn10		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_far5, 0, 0, 0)			/* m_rarmA1		*/
						hmsBegin()
							hmsJoint(RM_SURF, RCP_mario_far4, 65, 0, 0)		/* m_rarmB1		*/
							hmsBegin()
								hmsCall(RCP_NOR_FarMarioRightHand)
							hmsEnd()
						hmsEnd()
					hmsEnd()

				hmsEnd()
			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, 42)								/* chn15		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_far11, 0, 0, 0)					/* m_llegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_far10, 89, 0, 0)			/* m_llegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, RCP_mario_far9, 67, 0, 0)			/* m_lfoot1		*/
					hmsEnd()
				hmsEnd()
			hmsEnd()

			hmsJoint(RM_SURF, NULL, 13, -8, -42)							/* chn17		*/
			hmsBegin()
				hmsJoint(RM_SURF, RCP_mario_far8, 0, 0, 0)					/* m_rlegA1		*/
				hmsBegin()
					hmsJoint(RM_SURF, RCP_mario_far7, 89, 0, 0)				/* m_rlegB1		*/
					hmsBegin()
						hmsJoint(RM_SURF, NULL, 67, 0, 0)
						hmsBegin()
							hmsCProg(2, CtrlHandScale)
							hmsScale(1.0f)
							hmsBegin()
								hmsGfx(RM_SURF, RCP_mario_far6)			/* m_rfoot1		*/
							hmsEnd()
						hmsEnd()
					hmsEnd()
				hmsEnd()
			hmsEnd()

		hmsEnd()
	hmsEnd()
	hmsReturn()
};
