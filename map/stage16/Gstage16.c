/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage16 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#include "mainmap2/mainmap2_ext_texture.h"		//	Texture.

#include "mainmap2/main_castle_shape.sou"
#include "mainmap2/main_field_shape.sou"
#include "mainmap2/main_tower_shape.sou"
#include "mainmap2/main_fence_shape.sou"
#include "mainmap2/main_shade_shape.sou"
#include "mainmap2/main_window_shape.sou"
#include "mainmap2/main_net_shape.sou"
#include "mainmap2/main_net2_shape.sou"
#include "mainmap2/main_cannon_shape.sou"
#include "mainmap2/main_carpet_shape.sou"
#include "mainmap2/main_castle_flag.sou"
#include "mainmap2/peach_mail_shape.sou"

#include "mainmap2/cx1601.flk"
#include "mainmap2/cx1601.tag"

#include "mainmap2/main_net.flk"
#include "mainmap2/main_net2.flk"


/********************************************************************************/
/*	Water surface records in 16 - 1.											*/
/********************************************************************************/
static short waterSurf_16_01_00[] = {
	1,
	0,   20,  15, -7129,-7222,   -7129,  -58,    8253,  -58,    8253,-7222,  1,  150,  0,
};

static short waterSurf_16_01_01[] = {
	1,
	0,   15,  10,  1024,  -58,    1024, 8137,    8230, 8137,    8230,  -58,  0,  150,  0,
};

WaterSurfRec waterSurf_16_01[] = {
		{	0,	waterSurf_16_01_00	 },
		{	1,	waterSurf_16_01_01	 },
		{	0xffff,	NULL	}					//	End of WaterSurfRec data.
};

/********************************************************************************/
/*	Waterfall data in 16 - 1.													*/
/********************************************************************************/
short waterfall_1601_pt[] = {
	 70,
	-4469, -800, -6413,		0,  0,	
	-5525, 1171, -7026,		2,	0,
	-6292, 2028, -7463,		4,	0,
	-7302, 2955, -7461,		6,	0,

	-4883, -800, -5690,		0,	3,
	-5547, 1110, -6097,		2,	3,
	-6732, 2587, -6770,		4,	3,
	-7603, 3004, -7160,		6,	3,

	-5580, -800, -4740,		0,	6,
	-6205, 1068, -5347,		2,	6,
	-7249, 2566, -6192,		4,	6,

	-6895, -800, -4714,		0,	9,
	-7201, 1083, -5071,		2,	9,
	-7578, 2042, -5766,		4,	9,
	-8132, 2961, -6761,		6,	9,
};

Gfx waterfall_1601_draw[] = {
	gsSP1Triangle( 0,  1,  5, 0),
	gsSP1Triangle( 0,  5,  4, 0),
	gsSP1Triangle( 1,  2,  6, 0),
	gsSP1Triangle( 1,  6,  5, 0),
	gsSP1Triangle( 2,  3,  6, 0),
	gsSP1Triangle( 3,  7,  6, 0),
	gsSP1Triangle( 4,  5,  9, 0),
	gsSP1Triangle( 4,  9,  8, 0),
	gsSP1Triangle( 5,  6,  9, 0),
	gsSP1Triangle( 6, 10,  9, 0),
	gsSP1Triangle( 6,  7, 10, 0),
	gsSP1Triangle( 8,  9, 12, 0),
	gsSP1Triangle( 8, 12, 11, 0),
	gsSP1Triangle( 9, 10, 13, 0),
	gsSP1Triangle( 9, 13, 12, 0),
	gsSP1Triangle(10,  7, 14, 0),
	gsSP1Triangle(10, 14, 13, 0),
	gsSPEndDisplayList()
};
