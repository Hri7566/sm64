/********************************************************************************
						Ultra 64 MARIO Brothers

					 stage26 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 11, 1995
 ********************************************************************************/

#include "../../headers.h"

#define		STAGE26_CLEAR_R		0
#define		STAGE26_CLEAR_G		0
#define		STAGE26_CLEAR_B		0

extern Gfx gfx_main_garden[]  ;
extern Gfx gfx_main_g_tower[] ;
extern Gfx gfx_main_g_shade[] ;
extern Gfx gfx_main_g_window[];

/********************************************************************************/
/*	Hierarchy map data of MainTower.											*/
/********************************************************************************/
Hierarchy RCP_HmsGardenTower[] = { 
	hmsHeader(2600)
	hmsBegin()
		hmsGfx(RM_SURF, gfx_main_g_tower)
	hmsEnd()
  	hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage26Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(0, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, SCREEN_FAR, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_DUNGEON_O,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SURF  , gfx_main_garden)
					hmsGfx(RM_XDECAL, gfx_main_g_shade)
					hmsGfx(RM_SPRITE, gfx_main_g_window)
					hmsCProg(0, 	 WaterInit)
					hmsCProg(0x2601, WaterDraw)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
