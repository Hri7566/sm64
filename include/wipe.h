/********************************************************************************
						Ultra 64 MARIO Brothers

						 Header file of wipe.c.

			Copyright 1995 Nintendo co., ltd.  All rights reserved

				  This module was programmed by Y.Tanimoto

							  Nob 7, 1995
*********************************************************************************/

#ifndef	_WIPE_HEAD
#define	_WIPE_HEAD


#define		WIPE_FADE_IN				0
#define		WIPE_FADE_OUT				1

#define		WIPE_TRI_SCR_OPEN			2
#define		WIPE_TRI_SCR_CLOSE			3

#define		WIPE_RECT_SCR_OPEN			4
#define		WIPE_RECT_SCR_CLOSE			5

#define		WIPE_R_DITHER_IN			6
#define		WIPE_R_DITHER_OUT			7

#define		WIPE_STAR_WIN_OPEN			8
#define		WIPE_STAR_WIN_CLOSE			9

#define		WIPE_CIRCLE_WIN_OPEN		10
#define		WIPE_CIRCLE_WIN_CLOSE		11

#define		WIPE_POLY_STAR_OPEN			12
#define		WIPE_POLY_STAR_CLOSE		13

#define		WIPE_DOCRO_WIN_OPEN			14
#define		WIPE_DOCRO_WIN_CLOSE		15

#define		WIPE_MARIO_WIN_OPEN			16
#define		WIPE_MARIO_WIN_CLOSE		17

#define		WIPE_KOPA_WIN_OPEN			18
#define		WIPE_KOPA_WIN_CLOSE			19


/* =============================================================================
		: Define the structure to store data used for wipe.
		: Color data only.
================================================================================ */
typedef struct	{
	unsigned char	red	  ;
	unsigned char	green ;
	unsigned char	blue  ;
} WipeFadeData, WipeScreenData, WipeDitherData ;


/* =============================================================================
		: Define the structure to store data used for wipe.
		: Window open/close data.
================================================================================ */
typedef struct	{
	uchar	red		   ;
	uchar	green	   ;
	uchar	blue	   ;
	short	start_side ;
	short	end_side   ;
	short	start_xo   ;
	short	start_yo   ;
	short	end_xo	   ;
	short	end_yo     ;
	short	angle_inc  ;
} WipeWindowData ;


/* =============================================================================
		: Prottype function.
================================================================================ */
extern int 	 Wipe(char, char, unsigned char, void*);
extern ulong CannonEdge(int code, MapNode *node, void *data);


#endif
