/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage23 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 11, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE23_FOG_R		0
#define		STAGE23_FOG_G		0
#define		STAGE23_FOG_B		0
#define		STAGE23_FOG_START	980

#include "waterland/waterland_ext_texture.h"		//	Texture.

#include "waterland/wl_a_shape.sou"
#include "waterland/wl_ap_shape.sou"
#include "waterland/wl_a_a_shape.sou"
#include "waterland/wl_a_ht_shape.sou"
#include "waterland/wl_b_shape.sou"
#include "waterland/wl_bp_shape.sou"
#include "waterland/wl_b_a_shape.sou"
#include "waterland/wl_b_ht_shape.sou"
#include "waterland/wl_b_htk_shape.sou"
#include "waterland/wl_b_huta_shape.sou"
#include "waterland/wl_b_huta_ht_shape.sou"
#include "waterland/sm_shape.sou"
#include "waterland/sm_s_shape.sou"
#include "waterland/wl_b_sango_shape.sou"
#include "waterland/wl_b_ng_shape.sou"
#include "waterland/wl_bo_shape.sou"

#include "waterland/cx2301.flk"
#include "waterland/cx2302.flk"

#include "waterland/cx2301.tag"
#include "waterland/cx2302.tag"

#include "waterland/sm_check.flk"
#include "waterland/huta_check.flk"


/********************************************************************************/
/*	Water surface records in 23 - 1.											*/
/********************************************************************************/
static short waterSurf_23_01_00[] = {
	2,
	0,	 20,  20,  -7167,-4095,  -7167,4096,  1024,4096,  1024,-4095,  0,  160,  0,
	0,	  0,   5,   2048, -768,	  2048, 768,  6144, 768,  6144, -768,  0,  160,  0
};

extern WaterSurfRec	waterSurf_23_01[] = {
		{   0, waterSurf_23_01_00  },
		{  -1, NULL  }
};

/********************************************************************************/
/*	Water surface records in 23 - 2.											*/
/********************************************************************************/
static short waterSurf_23_02_00[] = {
	2,
	0,	  20,  20,      0,-5119,     0,7168,   8192,7168,   8192,-5119,  0,  160,  0,
	0,	   0,   5,  -6144, -768, -6144, 768,  -2048, 768,  -2048, -768,  0,  160,  0,
};

extern WaterSurfRec	waterSurf_23_02[] = {
		{   0, waterSurf_23_02_00  },
		{  -1, NULL  }
};
