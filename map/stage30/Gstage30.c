/********************************************************************************
						Ultra 64 MARIO Brothers

				  stage30 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							Decemver 11, 1995
*********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE30_FOG_R		 10
#define		STAGE30_FOG_G		 30
#define		STAGE30_FOG_B		 20
#define		STAGE30_FOG_START	970

#include "kopa_round/kopa1_ext_texture.h"

#include "kopa_round/kopa1_map_shape.sou"
#include "kopa_round/cx3001.flk"
