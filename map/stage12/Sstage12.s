/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 12 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage12

#define	fune_ue_x		2385
#define	fune_ue_y		3589
#define	fune_ue_z		3727
#define	fune_sita_x		5385
#define	fune_sita_y	   -5520
#define	fune_sita_z		2428


/* ========================================================================================
		: Sunken ship sequence.
===========================================================================================	*/
SEQ_Stage1201_Ship:

		seqLevelActor(0x0001,S_movebg04,  fune_ue_x,fune_ue_y,fune_ue_z, 	    0,0,0,  0,0,0,	e_fune_ue	 		)
		seqLevelActor(0x0001,S_movebg05,  fune_ue_x,fune_ue_y,fune_ue_z, 	    0,0,0,  0,0,0,	e_fune_ue	 		)
		seqLevelActor(0x0001,S_movebg00,  fune_sita_x,fune_sita_y,fune_sita_z,  0,0,0,  0,0,0,	e_fune_sita 		)
		seqLevelActor(0x0001,S_movebg02,  fune_sita_x,fune_sita_y,fune_sita_z,  0,0,0,  0,0,0,	e_fune_sita 		)
		seqLevelActor(0x0001,S_NULL,  	  fune_sita_x,fune_sita_y,fune_sita_z,  0,0,0,  0,0,0,	e_fune_sita_atari_1 )
		seqLevelActor(0x0001,S_NULL,  	  fune_sita_x,fune_sita_y,fune_sita_z,  0,0,0,  0,0,0,	e_fune_sita_atari_2 )

		seqLevelActor(0x003e,S_movebg00,   4880,820,2375, 0,0,0,  0,0,0,e_fune_move   )
		seqLevelActor(0x003e,S_movebg01,   4880,820,2375, 0,0,0,  0,0,0,e_fune_move   )
		seqLevelActor(0x003e,S_movebg02,   4880,820,2375, 0,0,0,  0,0,0,e_fune_move   )
		seqLevelActor(0x003e,S_movebg03,   4880,820,2375, 0,0,0,  0,0,0,e_fune_move   )
		seqLevelActor(0x003e,S_NULL,   	   4880,820,2375, 0,0,0,  0,0,0,e_fune_atari  )
		seqLevelActor(0x003e,S_movebg07,   4668,1434,2916,0,0,0,  0,0,0,e_moving_cube )

		seqLevelActor(0x0001,S_ubboo, 6048,-5381, 1154,  0,340,0,  0,0,0,  e_ubboo)
		seqLevelActor(0x0002,S_ubboo, 8270,-3130, 1846,  0,285,0,  1,1,0,  e_ubboo)
		seqLevelActor(0x003c,S_ubboo, 6048,-5381, 1154,  0,340,0,  2,2,0,  e_ubboo)
		seqLevelActor(0x003e,S_NULL,  4988,-5221, 2473,  0,0,  0,  0,0,0,  e_fukidasi	) 

		seqActor(S_NULL,     -1800,-2812,-2100,   0, 0,0,   2,0,0,   e_tbox_quize_ground)
		seqLevelActor(0x003e,S_redbom,  -1956,1331,6500,    0,0,0,   0,0,0,   e_futa_bom)

		seqReturn


/* ========================================================================================
		: Move BGs sequence
===========================================================================================	*/
SEQ_Stage1201_MoveBGs:

		seqActor(S_movebg06,    1834,-2556,-7090,   0,194,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,   -2005,-2556,-3506,   0,135,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    1578,-2556,-5554,   0, 90,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,      42,-2556,-6578,   0,135,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    -981,-2556,-5298,   0,255,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,   -6549, 1536, 4343,   0, 90,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    1322,-2556,-3506,   0,165,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    6442,-2556,-6322,   0,135,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    3882,-2556,-5042,   0,  0,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    1497, 1741, 7810,   0, 14,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,   -3978, 1536,  -85,   0,  0,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,   -5228, 1230, 2106,   0,323,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,   -7481, 1536,  185,   0,149,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,   -5749, 1536,-1113,   0,255,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,   -5332, 1434, 1023,   0,315,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    -815, -613, 3556,   0,315,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,   -3429,  819, 4948,   0,284,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,   -1940,  410, 2377,   0,194,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,   -1798, -716, 4330,   0,  0,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    -845,  922, 7668,   0,315,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    6741,-2886, 3556,   0,135,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,     255, -101, 4719,   0, 45,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    1787, -306, 5133,   0,315,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    1079, -613, 2299,   0, 75,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    2931,-1697,  980,   0,315,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    3036,-4709, 4027,   0,315,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    4222,-1125, 7083,   0,104,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    6650, -613, 4941,   0,315,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    5764,-4709, 4427,   0,315,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    3693,-4709,  856,   0,135,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    7981,  410, 2704,   0,165,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    2917,-3046, 4818,   0,241,0,   0,0,0,   e_sea_stone)
		seqActor(S_movebg06,    5896, -393, -123,   0,315,0,   0,0,0,   e_sea_stone)

		seqActor(S_NULL,     53,2355,2724,  0,0,0,  0,41,0,  e_bar)
		seqActor(S_NULL,    659,2560,3314,  0,0,0,  0,41,0,  e_bar)
		seqActor(S_NULL,   1087,2150,3798,  0,0,0,  0,41,0,  e_bar)
		seqActor(S_NULL,  -2535,1075,6113,  0,0,0,  0,97,0,  e_bar)

		seqActor(S_movebg08,    2078,-2863,-4696,   0, 90,0,   0,0,0,   e_wd_pillar)
		seqActor(S_movebg08,   -1403,-2863,-4696,   0, 90,0,   0,0,0,   e_wd_pillar)
		seqActor(S_movebg08,   -1096,-2863,-3262,   0, 90,0,   0,0,0,   e_wd_pillar)
		seqActor(S_movebg08,     337,-2863,-5106,   0, 90,0,   0,0,0,   e_wd_pillar)
		seqActor(S_movebg08,    2078,-2863,-6232,   0, 90,0,   0,0,0,   e_wd_pillar)
		seqActor(S_movebg08,    4330,-2863,-5618,   0, 90,0,   0,0,0,   e_wd_pillar)
		seqActor(S_movebg09,    2078,-2966,-4696,   0, 90,0,   0,0,0,   e_wd_pillarbase)
		seqActor(S_movebg09,   -1403,-2966,-4696,   0, 90,0,   0,0,0,   e_wd_pillarbase)
		seqActor(S_movebg09,   -1096,-2966,-3262,   0, 90,0,   0,0,0,   e_wd_pillarbase)
		seqActor(S_movebg09,     337,-2966,-5106,   0, 90,0,   0,0,0,   e_wd_pillarbase)
		seqActor(S_movebg09,    2078,-2966,-6232,   0, 90,0,   0,0,0,   e_wd_pillarbase)
		seqActor(S_movebg09,    4330,-2966,-5618,   0, 90,0,   0,0,0,   e_wd_pillarbase)
		seqActor(S_movebg10,   -1059,1025,7072,     0,247,0,   0,0,0,   e_12_floatingbord)

		seqActor(S_NULL, -4236, 1044, 2136,  0,0,0,  0,0,0,  e_taihou_dai	) 	

		seqReturn


/* ========================================================================================
		: Scene 1 [ Water D ] Stars sequence
===========================================================================================	*/
SEQ_Stage1201_Stars:

/*		seqLevelActor(0x003c,S_polystar, 7190,-3200,-2150,  0,0,0,  1,0,0,  e_tripstar)			  /*   after uuubo				*/
		seqLevelActor(0x003f,S_NULL    , 4900, 2400,  800,  0,0,0,  3,0,0,  e_tripstar_getcoins)  /* 8 red coins.			 	*/
#if ENGLISH || CHINA
		seqLevelActor(0x003f,S_itembox, 1540, 2160, 2130,  0,0,0,  4,8,0,  e_itembox)			  /* on the rock by the cannon.	*/
#else
		seqLevelActor(0x003f,S_polystar, 1540, 2160, 2130,  0,0,0,  4,0,0,  e_tripstar)			  /* on the rock by the cannon.	*/
#endif
		seqLevelActor(0x003e,S_polystar, 5000,-4800, 2500,  0,0,0,  5,0,0,  e_tripstar)			  /* over the water flow.		*/

		seqReturn


/* ========================================================================================
		: Scene [ In ship] Enemy sequence.
===========================================================================================	*/
SEQ_Stage1202_Enemys:

		seqActor(S_NULL,     400, -350,-2700,   0, 0,0,   0,0,0,   e_tbox_quize)
		seqReturn


/* ========================================================================================
		: Scene 2 [ In the ship ] Stars sequence
===========================================================================================	*/
SEQ_Stage1202_Stars:

/*		seqLevelActor(0x0001,S_polystar,  0, 1600, 3000,  0,0,0,  0,0,0,  e_tripstar)	treasure boxes and the yellow item block	*/

		seqReturn


/* ========================================================================================
		: Stage12 main sequence.
===========================================================================================	*/
SEQ_DoStage12:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1 	  , _GfxStage12SegmentRomStart	  , _GfxStage12SegmentRomEnd	)
	seqLoadPres(SEGMENT_WEATHER	  , _WeatherSegmentRomStart		  , _WeatherSegmentRomEnd		)
	seqLoadText(SEGMENT_TEXTURE	  , _DWaterTextureSegmentRomStart , _DWaterTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_BACK   	  , _BackKuppaSegmentRomStart	  , _BackKuppaSegmentRomEnd		)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_dSegmentRomStart	  , _GfxEnemy1_dSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_dSegmentRomStart	  , _HmsEnemy1_dSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY2	  , _GfxEnemy2_dSegmentRomStart	  , _GfxEnemy2_dSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA2, _HmsEnemy2_dSegmentRomStart	  , _HmsEnemy2_dSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_d)
		seqCall(SetEnemy2_d)

		seqHmsShape(S_movebg00, RCP_HmsFune_FrontLeft_opa	)
		seqHmsShape(S_movebg01, RCP_HmsFune_BackLeft_opa	)
		seqHmsShape(S_movebg02, RCP_HmsFune_FrontRight		)
		seqHmsShape(S_movebg03, RCP_HmsFune_BackRight		)
		seqHmsShape(S_movebg04, RCP_HmsFune_FrontLeft_xlu	)
		seqHmsShape(S_movebg05, RCP_HmsFune_BackLeft_xlu	)

		seqHmsShape(S_movebg06, RCP_HmsTinIwa		)
		seqHmsShape(S_movebg07, RCP_HmsWDKibako		)
		seqHmsShape(S_movebg08, RCP_HmsWdHasira		)
		seqHmsShape(S_movebg09, RCP_HmsWdHasiradai	)
		seqHmsShape(S_movebg10, RCP_HmsHmsTinIta	)

		seqBeginScene(1, RCP_Stage12Scene1)
			seqActor(S_NULL,  -6750,1126+START_H,1482,  0,90,0,  0,10,0,  e_player_entpict)
			seqPort(10, 12, 1, 10)				/*	MARIO stage in.	*/
			seqCourseOut(12, 2, 10)				/*	To the scene 2.	*/
			seqGameClear(6, 1,  53)
			seqGameOver (6, 1, 103)
			seqWaterJet(0, 4979,-5222,2482, -30, SEQ_STAR_LEVEL)
			seqCall(SEQ_Stage1201_Ship)
			seqCall(SEQ_Stage1201_MoveBGs)
			seqCall(SEQ_Stage1201_Stars)
			seqMapInfo(cx1201_info)
			seqTagInfo(cx1201_info_tag)
			seqSetMusic(NA_STG_WATER, NA_SEA_BGM)
			seqEnvironment(ENV_WATER)
		seqEndScene()

		seqBeginScene(2, RCP_Stage12Scene2)
			seqActor(S_NULL,  928,1050,-1248,  0,180,0,  0,10,0,  e_player_swimming)
			seqPort(10, 12, 2, 10)				/*	MARIO scene in.	*/
			seqGameClear(6, 1,  53)
			seqGameOver (6, 1, 103)
			seqCall(SEQ_Stage1202_Enemys)
			seqCall(SEQ_Stage1202_Stars)
			seqMapInfo(cx1202_info)
			seqTagInfo(cx1202_info_tag)
			seqSetMusic(NA_STG_WATER, NA_SEA_BGM)
			seqEnvironment(ENV_WATER)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1,  90, -6750,1126, 1482)

	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
