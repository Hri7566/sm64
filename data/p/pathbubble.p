/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: 
					Description	: 
					Date		: 1995.7.27
					Author		: H.yajima

 ********************************************************************************/


/*################*/
#ifdef ASSEMBLER
/*################*/
/********************************************************************************/
/*																				*/
/* << OPTION >> 																*/
/*																				*/
/********************************************************************************/

/*------------------------------------------------------*/
/*		player bubble									*/
/*------------------------------------------------------*/
e_bubbleanime:
	p_initialize(option)
	p_shapeDISABLE

	p_setd_random(work0,2,9)
	p_wait_work(work0)
	p_makeshape(S_bubble,e_bubble_main)
	p_mother_clrbit(effect,stf_bubble)
	p_killshape

/*------------------------------------------------------*/
/*		1 bubble										*/
/*------------------------------------------------------*/
e_bubble_main2:
	p_initialize(effect)
	p_setbit(flag,stf_moveON)
	p_softspritemodeON
	p_program(s_bubble_init)
	p_setf_random(work0,-75,150)
	p_setf_random(work1,-75,150)
	p_setf_random(work2,-75,150)
	p_WADDf(worldX,worldX,work0)
	p_WADDf(worldZ,worldZ,work1)
	p_WADDf(worldY,worldY,work2)
	p_animereset
	p_do(60)
		p_animeinc
		p_program(s_bubble_option)
	p_next
	p_killshape


e_bubble_main:
	p_initialize(effect)
	p_setbit(flag,stf_moveON)
	p_softspritemodeON

	p_program(s_bubble_init)

	p_setf_random(work0,-200/4,400/4)
	p_setf_random(work1,-200/4,400/4)
	p_WADDf(worldX,worldX,work0)
	p_WADDf(worldZ,worldZ,work1)
	p_setf_random(work2,0,200/4)
	p_WADDf(worldY,worldY,work2)

	p_animereset
	p_jsr(bubble_frame)

	p_do(60)
		p_jsr(bubble_frame)
		p_program(s_bubble_main)
	p_next
	p_killshape

bubble_frame:
	p_animeinc
	p_addf(worldY,30/4)
	p_setf_random(work0,-10/4,20/4)
	p_setf_random(work1,-10/4,20/4)
	p_WADDf(worldX,worldX,work0)
	p_WADDf(worldZ,worldZ,work1)
	p_rts

/*------------------------------------------------------*/
/*		random bubble									*/
/*------------------------------------------------------*/

e_random_bubble:
	p_initialize(item)
	p_setbit(flag,stf_moveON)
	p_softspritemodeON				/* software sprite mode		*/
	p_sethitbox2(400,150,-150)		/* hitting cube 			*/
	p_hitON							/* hitcheck enable			*/
	p_setobjname(RECOVER)			/* recover object			*/
	p_setd(ap,5)					/* recover point  (1)		*/

	p_program(s_boxbubble_init)
	p_animereset
	p_while
		p_program(s_boxbubble_main)
	p_loop


/*################*/
#else
/*################*/

/********************************************************************************/
/*		box bubble initialize													*/
/********************************************************************************/

extern void s_boxbubble_init(void)
{
	s_set_scale(4.0);

}

/********************************************************************************/
/*		box bubble main															*/
/********************************************************************************/

extern void s_boxbubble_main(void)
{

	int	i;

	execstp->map.scale[0] =  sin(execstp->s[stw_work0].d)*0.5+4.0;
	execstp->map.scale[1] = -sin(execstp->s[stw_work0].d)*0.5+4.0;
	execstp->s[stw_work0].d += 0x400;

	if ( obj_timer < 30 ){
		s_hitOFF();
		obj_worldY += 3;
	} else { 
		s_hitON();
		s_chase_speedF(2,10);
		obj_angleY = s_calc_targetangle(execstp,player1stp);
		s_optionmove_F();
	}

	obj_worldX += Randomf()*4-2;
	obj_worldZ += Randomf()*4-2;

	if ( (obj_mail & EMAIL_PLAYERHIT) != 0 || obj_timer > 200 ){	
		objsound(NA_SE2_BIGBUBBLE); 
		s_remove_obj(execstp);
		for(i=0;i<30;i++){
			s_makeobj_nowpos(execstp,S_bubble,e_bubble_main2);
		}
	}

	if ( mcWaterCheck(obj_worldX,obj_worldZ) < obj_worldY  ) s_remove_obj(execstp);

	obj_mail = 0;
}




/********************************************************************************/
/*		bubble initialize														*/
/********************************************************************************/
extern void s_bubble_init(void)
{

	execstp->s[stw_work2].d = (int)(Randomf()*0x800)+0x800;
	execstp->s[stw_work3].d = (int)(Randomf()*0x800)+0x800;

	objsound(NA_SE2_BIGBUBBLE);

}

/********************************************************************************/
/*		bubble main																*/
/********************************************************************************/
extern void s_randombubble_init(void)
{
	s_set_scale(Randomf()+1.0);

}





/********************************************************************************/
/*		bubble option															*/
/********************************************************************************/
extern void s_bubble_option(void)
{

	obj_worldY += Randomf()*3+6;
	obj_worldX += Randomf()*10-5;
	obj_worldZ += Randomf()*10-5;
	execstp->map.scale[0] = sin(execstp->s[stw_work0].d)*0.2+1.0;
	execstp->s[stw_work0].d += execstp->s[stw_work2].d;
	execstp->map.scale[1] = sin(execstp->s[stw_work1].d)*0.2+1.0;
	execstp->s[stw_work1].d += execstp->s[stw_work3].d;

	/*	if ( mcWaterCheck(obj_worldX,obj_worldZ) < obj_worldY ) s_remove_obj(execstp);	*/

}





/********************************************************************************/
/*		bubble option															*/
/********************************************************************************/
extern void s_bubble_main(void)
{

	float water = mcWaterCheck(obj_worldX,obj_worldZ);

	/*------ scale Animation ---------*/

	execstp->map.scale[0] = sin(execstp->s[stw_work0].d)*0.2+1.0;
	execstp->s[stw_work0].d += execstp->s[stw_work2].d;
	execstp->map.scale[1] = sin(execstp->s[stw_work1].d)*0.2+1.0;
	execstp->s[stw_work1].d += execstp->s[stw_work3].d;


	/*------ water check -----------*/

 	if ( execstp->s[stw_worldY].f > water ){
		execstp->status = 0;
		execstp->s[stw_worldY].f += 5;

		if (stratfree.next != NULL) {
			s_makeobj_nowpos( execstp,S_rippleA,e_rippleA );	/* make water ripple */
		}

	}
	

	/*----- bubble hitting check -------*/

#if	0
	if ( s_hitcheck(execstp,player1stp) ) 		s_remove_obj(execstp);
#else
	if ( (obj_mail & EMAIL_PLAYERHIT) != 0 )	s_remove_obj(execstp);
#endif

	/*----------------------------------*/

}

/*################*/
#endif
/*################*/

/*===============================================================================
		end end end end end end end end 
===============================================================================*/



