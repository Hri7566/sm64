/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------

					File		: pathshipobj.s
					Description	: 
					Date		: 1995.
					Author		: H.yajima

 ********************************************************************************/


/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************/
/*																				*/
/*		moving ship																*/
/*																				*/
/********************************************************************************/

e_moving_cube:
	p_initialize(moveBG)
	p_setbit( flag,stf_moveON )
	p_setshapeinfo(kibakocheck_info)	
	p_save_nowpos
	p_while
		p_program(s_moving_cube)
		p_program(stMainMoveBG)	
	p_loop

/********************************************************************************/
/*																				*/
/*		moving ship																*/
/*																				*/
/********************************************************************************/

e_fune_move:
	p_initialize(option)
	p_setbit( flag,stf_moveON )
	p_save_nowpos
	p_while
		p_program(s_fune_move)
	p_loop

e_fune_atari:
	p_initialize(moveBG)
	p_setbit( flag,stf_moveON )
	p_setshapeinfo(fune_c_db_info)
	p_save_nowpos
	p_setf(movebg_checkdist,4000)
	p_while
		p_program(s_fune_move)
		p_program(stMainMoveBG)	
	p_loop

/********************************************************************************/
/*																				*/
/* << OPTION >>	movebg program													*/
/*																				*/
/********************************************************************************/

e_fune_ue:
	p_initialize(option)
	p_setbit( flag,stf_moveON | stf_playerdistON | stf_alldispON )
	p_set_scale(50)
	p_save_nowpos
	p_while
		p_program(s_fune_ue)
	p_loop

/*--------------------------------------------------------------*/
/*		fune angle												*/
/*--------------------------------------------------------------*/
fune_sita_angle:
	p_setd(animeangleX,-5800)
	p_setd(animeangleY,-4500)
	p_setd(animeangleZ, 3200)
	p_rts


e_fune_sita:
	p_initialize(option)
	p_setbit( flag,stf_moveON | stf_playerdistON )
	p_set_scale(100)
	p_setf(shapeLOD,6000)
	p_save_nowpos
	p_jsr(fune_sita_angle)
	p_end

/*--------------------------------------------------------------*/
e_fune_sita_atari_1:
	p_initialize(moveBG)
	p_setshapeinfo(fune_c_a_info)
	p_jmp(fune_sita_atari_common)

/*--------------------------------------------------------------*/
e_fune_sita_atari_2:
	p_initialize(moveBG)
	p_setshapeinfo(fune_c_rr_info)

fune_sita_atari_common:
	p_setbit( flag,stf_moveON )
	p_setf(movebg_checkdist,4000)
	p_jsr(fune_sita_angle)
	p_while
		p_program(stMainMoveBG)
	p_loop

/*--------------------------------------------------------------*/

/*################*/
#else
/*################*/

/********************************************************************************
		Fune Ue
 ********************************************************************************/

extern void s_fune_ue(void)
{

	
	if ( obj_playerdist > 10000 )   obj_alpha = 140;
	else 							obj_alpha = 140 * obj_playerdist / 10000.0;
	s_shapeOFF();

}

/********************************************************************************
		Fune Move 
 ********************************************************************************/

extern void s_fune_move(void)
{

	short ax = obj_animeangleX;
	short az = obj_animeangleZ;

	s_debug_position();

	obj_work0 += 256;
	obj_animeangleX = sin(obj_work0)*0x400;
	obj_animeangleZ = sin(obj_work1)*0x400;
	obj_anglespeedX = obj_animeangleX - ax;
	obj_anglespeedZ = obj_animeangleZ - az;

	if ( player_worldY > 1000 ) objsound_level(NA_LSE2_SHIPSLOPE);	/* shipslope */

}



/********************************************************************************
		=====================
		 moving cube program
		=====================
 ********************************************************************************/

#define	ship_pointer 	(execstp->s[stw_work0].pointer)
#define	movecube_work 	(execstp->s[stw_work1].d)
#define	movecube_speed 	(execstp->s[stw_work2].f)

static HitCheckRecord movecube_hit = {
	OBJNAME_DAMAGE,
	0,1,1,0,	/* flag,ap,hp,coin 		 */
	130,100,
	  0,  0,
};

extern void s_moving_cube(void)
{
	


	AffineMtx		matrix;
	float			src[3],dst[3];
	short			ang[3];
	StrategyRecord 	*stp;
	BGCheckData		*bgdata;
	float			vector[3];	/* normal vector */
	float			pos[3];
	short 			angleX;
		
	if ( ship_pointer == 0 ){
		if ( ( stp = (void *)s_find_obj(e_fune_atari)) != NULL ) ship_pointer = (void *)stp;
		obj_skeletonX = obj_worldX - (stp->s[stw_worldX].f);
		obj_skeletonY = obj_worldY - (stp->s[stw_worldY].f);
		obj_skeletonZ = obj_worldZ - (stp->s[stw_worldZ].f);
	} else {

		stp = (StrategyRecord *)ship_pointer;
		ang[0] = stp->s[stw_animeangleX].d;
		ang[1] = stp->s[stw_animeangleY].d;
		ang[2] = stp->s[stw_animeangleZ].d;
		src[0] = obj_skeletonX;
		src[1] = obj_skeletonY;
		src[2] = obj_skeletonZ;

		CreateModelAffineMtx(matrix, src,ang);
		stRotatePoint(matrix,dst,src);
		obj_worldX = (stp->s[stw_worldX].f) + dst[0];
		obj_worldY = (stp->s[stw_worldY].f) + dst[1];
		obj_worldZ = (stp->s[stw_worldZ].f) + dst[2];

		angleX = stp->s[stw_animeangleX].d;

	}

	pos[0] = obj_worldX;
	pos[1] = obj_worldY;
	pos[2] = obj_worldZ;


	mcBGGroundCheck(pos[0],pos[1],pos[2],&bgdata);
	if ( bgdata != NULL ){

		vector[0] = bgdata->a;
		vector[1] = bgdata->b;
		vector[2] = bgdata->c;

//		CreateRotationAffineMtx(execstp->matrix,vector,pos,0);
//		execstp->map.matrix = &(execstp->matrix);

		obj_animeangleX = angleX;

	}

	movecube_speed = sin(movecube_work)*20;
	movecube_work += 0x100;
	obj_skeletonZ += movecube_speed;

 

	if ( player_worldY > 1000 ){
		if ( s_abs_f(movecube_speed) > 3 )  objsound_level(NA_LSE3_BOXSLIDE);
	}

	/* db_hittest(&movecube_hit); */
	s_set_hitparam(execstp,&movecube_hit);

	if ( ( movecube_work & 0x7fff ) == 0 )	s_hitON();
	if ( s_hitcheck(execstp,player1stp) ){
		obj_mail = 0;
		s_hitOFF();
	}
		

}


/*################*/
#endif
/*################*/


/*===============================================================================
		end end end end end end end end 
===============================================================================*/



