/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage14 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE14_FOG_R		200
#define		STAGE14_FOG_G		255
#define		STAGE14_FOG_B		255
#define		STAGE14_FOG_START	900

#include "mecha_room/mecha_ext_texture.h"

#include "mecha_room/ct_1_shape.sou"
#include "mecha_room/ct_clear_shape.sou"
#include "mecha_room/ct_alpha_shape.sou"
#include "mecha_room/mecha_obj01_shape.sou"
#include "mecha_room/mecha_obj02_shape.sou"
#include "mecha_room/mecha_obj03_shape.sou"
#include "mecha_room/mecha_obj04_l_shape.sou"
#include "mecha_room/mecha_obj04_s_shape.sou"
#include "mecha_room/mecha_obj05_shape.sou"
#include "mecha_room/mecha_obj06_shape.sou"
#include "mecha_room/mecha_obj07_shape.sou"
#include "mecha_room/mecha_obj08_a_shape.sou"
#include "mecha_room/mecha_obj08_b_shape.sou"
#include "mecha_room/mecha_obj09_shape.sou"
#include "mecha_room/mecha_obj10_shape.sou"
#include "mecha_room/mecha_obj11_shape.sou"
#include "mecha_room/mecha_obj12s_shape.sou"
#include "mecha_room/mecha_obj12l_shape.sou"

#include "mecha_room/cx1401.flk"
#include "mecha_room/mecha_obj01_check.flk"
#include "mecha_room/mecha_obj02_check.flk"
#include "mecha_room/mecha_obj03_check.flk"
#include "mecha_room/mecha_obj04_l_check.flk"
#include "mecha_room/mecha_obj04_s_check.flk"
#include "mecha_room/mecha_obj05_check.flk"
#include "mecha_room/mecha_obj06_check.flk"
#include "mecha_room/mecha_obj07_check.flk"
#include "mecha_room/mecha_obj08_a_check.flk"
#include "mecha_room/mecha_obj08_b_check.flk"
#include "mecha_room/mecha_obj09_check.flk"
#include "mecha_room/mecha_obj10_check.flk"
#include "mecha_room/mecha_obj11_check.flk"

#include "mecha_room/cx1401.tag"


/* ====================================================================================	
		: Belt conveyor data in Mecha room.
======================================================================================= */
static Lights1 light_ct_beltcv[] = {
	{  {255*ct_1_AMB_SCALE, 255*ct_1_AMB_SCALE, 255*ct_1_AMB_SCALE, 0,
		255*ct_1_AMB_SCALE, 255*ct_1_AMB_SCALE, 255*ct_1_AMB_SCALE, 0 },
	   { 255, 255, 255, 0, 255, 255, 255, 0, LIGHT_X, LIGHT_Y, LIGHT_Z, 0 }	 }
};

unsigned short mecha_conv_txt[] = {
    0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  
    0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  
    0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  
    0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xffff,  0xff63,  0xfe21,  0x924b,  0x0001,  0x0001,  
    0xffff,  0xfff5,  0xff63,  0xfed3,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe8f,  0xfe4f,  0xf50b,  0xc347,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xffe7,  0xffd3,  0xfdc1,  0xf481,  0xbac1,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfff9,  0xfff9,  0xe701,  0xc3c1,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x18c7,  0x318d,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfff9,  0xfff9,  0xffd3,  0xffd3,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x318d,  0x6b5b,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfff9,  0xfff9,  0xffd3,  0xffd3,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x18c7,  0x318d,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfff9,  0xfff9,  0xffd3,  0xffd3,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfff9,  0xfff9,  0xffd3,  0xffd3,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfff9,  0xfff9,  0xffd3,  0xffd3,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe47,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfff9,  0xfff9,  0xffd3,  0xffd3,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xf481,  0xc2c1,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfff9,  0xfff9,  0xffd3,  0xffd3,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfff9,  0xfff9,  0xffd3,  0xffd3,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xffe7,  0xffe7,  0xffd3,  0xffd3,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xb593,  0xb4c1,  0xe701,  0xe701,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xc3c1,  0xc3c1,  0xe701,  0xe701,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xc3c1,  0xc3c1,  0xe701,  0xe701,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xc3c1,  0xc3c1,  0xe701,  0xe701,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe47,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xc3c1,  0xc3c1,  0xe701,  0xe701,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xf481,  0xc2c1,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xc3c1,  0xc3c1,  0xe701,  0xe701,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xc3c1,  0xc3c1,  0xe701,  0xe701,  0xffc1,  0xffc1,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xc3c1,  0xc3c1,  0xe701,  0xe701,  0xffc1,  0xffc1,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x18c7,  0x318d,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xc3c1,  0xc3c1,  0xe701,  0xe701,  0xce41,  0xa301,  0xfdc1,  0xfcc1,  0xc2c1,  0x5001,  0x318d,  0x6b5b,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xc3c1,  0xc3c1,  0xce41,  0xa301,  0xfdc1,  0xf4c1,  0xc2c1,  0x5001,  0x18c7,  0x318d,  
    0xffff,  0xfff1,  0xff19,  0xfe45,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xfe01,  0xc3c1,  0xc3c1,  0xfdc1,  0xf481,  0xba81,  0x5001,  0x0001,  0x0001,  
    0xffff,  0xf6eb,  0xf615,  0xe505,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe4c1,  0xe441,  0xd341,  0xa9c1,  0x5001,  0x0001,  0x0001,  
    0xdca1,  0xd3d5,  0xd347,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x90c1,  0x4801,  0x0001,  0x0001,  
    0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  0x6b5b,  
    0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  0x5295,  
    0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  0x18c7,  
};

Gfx beltcv1400_init[] = {
	gsDPPipeSync(),
	gsDPSetCycleType(G_CYC_2CYCLE),
	gsDPSetRenderMode(G_RM_FOG_SHADE_A, G_RM_AA_ZB_OPA_SURF2),
	gsDPSetDepthSource(G_ZS_PIXEL),
	gsDPSetFogColor(STAGE14_FOG_R, STAGE14_FOG_G, STAGE14_FOG_B, 255),
	gsSPFogPosition(STAGE14_FOG_START, 1000),
	gsSPSetGeometryMode(G_FOG),
	gsDPSetCombineMode(G_CC_MODULATERGB, G_CC_PASS2),
	gsSPLight((&light_ct_beltcv[0].l[0]),1 ),
	gsSPLight((&light_ct_beltcv[0].a   ),2 ),
	gsSPTexture(0xffff, 0xffff, 0, 0, G_ON),
	gs_Tani_SetUpTileDescrip(G_IM_FMT_RGBA, G_IM_SIZ_16b, 32, 32, 0, 0,
							 G_TX_WRAP|G_TX_NOMIRROR, 5, G_TX_NOLOD,
							 G_TX_WRAP|G_TX_NOMIRROR, 5, G_TX_NOLOD),
	gsSPEndDisplayList()
};

Gfx beltcv1400_reset[] = {
	gsSPTexture(0xffff, 0xffff, 0, 0, G_OFF),
	gsDPPipeSync(),
	gsDPSetCycleType(G_CYC_1CYCLE),
	gsDPSetRenderMode(G_RM_AA_ZB_OPA_SURF, G_RM_AA_ZB_OPA_SURF),
	gsSPClearGeometryMode(G_FOG),
	gsDPSetCombineMode(G_CC_SHADE, G_CC_SHADE),
	gsSPEndDisplayList()
};

short beltcv14_lg_pt[] = {
	120,
	 230,	-86,	 549,		0,    0,  127,		0,	0,
	 230,	-35,	 549, 		0,   33,  122,		1,	0,
	 230,	  0,	 512,		0,  110,   62,		2,	0,
	 230,	  0,	-511,		0,  123,  -31,	   14,	0,
	 230,	-35,	-547,		0,   65, -108,	   15,	0,
	 230,	-86,	-547,		0,    0, -127,	   16,	0,
	-229,	-86,	 549,		0,    0,  127,		0,	1,
	-229,	-35,	 549,		0,   66,  108,		1,	1,
	-229,	  0,	 512,		0,  123,   31,		2,	1,
	-229,	  0,	-511,		0,  110,  -63,	   14,	1,
	-229,	-35,	-547,		0,   32, -122,	   15,	1,
	-229,	-86,	-547,		0,    0, -127,	   16,	1,
};

short beltcv14_st_pt[] = {
	120,
	 230,	-86,	 344,		0,    0,  127,		0,	0,
	 230,	-35,	 344, 		0,   32,  122,		1,	0,
	 230,	  0,	 308,		0,  110,   63,		2,	0,
	 230,	  0,	-306,		0,  123,  -31,	    9,	0,
	 230,	-35,	-342,		0,   65, -108,	   10,	0,
	 230,	-86,	-342,		0,    0, -127,	   11,	0,
	-229,	-86,	 344,		0,    0,  127,		0,	1,
	-229,	-35,	 344,		0,   65,  108,		1,	1,
	-229,	  0,	 308,		0,  123,   31,		2,	1,
	-229,	  0,	-306,		0,  110,  -63,	    9,	1,
	-229,	-35,	-342,		0,   32, -122,	   10,	1,
	-229,	-86,	-342,		0,    0, -127,	   11,	1,
};

Gfx beltcv14_ls_draw[] = {
	gsSP1Triangle(0, 1, 6, 0),
	gsSP1Triangle(1, 7, 6, 0),
	gsSP1Triangle(1, 2, 7, 0),
	gsSP1Triangle(2, 8, 7, 0),
	gsSP1Triangle(2, 3, 8, 0),
	gsSP1Triangle(3, 9, 8, 0),
	gsSP1Triangle(3, 4, 9, 0),
	gsSP1Triangle(4,10, 9, 0),
	gsSP1Triangle(4, 5,10, 0),
	gsSP1Triangle(5,11,10, 0),
	gsSPEndDisplayList()
};
