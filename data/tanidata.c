 /********************************************************************************
						Ultra 64 MARIO Brothers

				 Data files used for tanimoto routines.

			Copyright 1995 Nintendo co., ltd.  All rights reserved

				This module was programmed by Y.Tanimoto

							Dec 6, 1995
*********************************************************************************/
#include "../headers.h"
#include "tanidata.h"


//---------------------------------------------------------------------------------------------
//
//
//		I-matrix.
//
//
/* ========================================================================================
		: Light.
===========================================================================================	*/
static Lights1 Tani_light[]={
	{  {   64,  64,  64, 0,  64,  64,  64, 0  },
	   {  255, 255, 255, 0, 255, 255, 255, 0, LIGHT_X, LIGHT_Y, LIGHT_Z, 0  }  }
};

/* ========================================================================================
		: I-matrix.
=========================================================================================== */
extern Mtx	Tani_I_matrix = {
			0x00010000, 0x00000000, 0x00000001, 0x00000000,
			0x00000000, 0x00010000, 0x00000000, 0x00000001,
			0x00000000, 0x00000000, 0x00000000, 0x00000000,
			0x00000000, 0x00000000, 0x00000000, 0x00000000
};

/* ========================================================================================
		: Ortho-matrix [ 0.0,320.0,  0.0,240.0,  0.0,2.0 ].
=========================================================================================== */
static Mtx	Tani_O_matrix = {
			0x00000000, 0x00000000, 0x00000000, 0x00000000,
			0x00000000, 0xFFFF0000, 0xFFFFFFFF, 0xFFFF0001,
			0x01990000, 0x00000000, 0x00000222, 0x00000000,
			0x00000000, 0x00000000, 0x00000000, 0x00000000
};

/* ========================================================================================
		: Static DL of drawing one tile.
=========================================================================================== */
extern Gfx Tani_draw_1squr[] = {
	gsSP1Triangle(0, 1, 2, 0),
	gsSP1Triangle(0, 2, 3, 0),
	gsSPEndDisplayList()
};

/* ========================================================================================
		: Static DL of drawing one tile.
=========================================================================================== */
extern Gfx Tani_draw_2squr[] = {
	gsSP1Triangle(4, 5, 6, 0),
	gsSP1Triangle(4, 6, 7, 0),
	gsSPEndDisplayList()
};


//---------------------------------------------------------------------------------------------
//
//
//		Used in 'shade.c'.
//
//
/* ========================================================================================
		: Static DL of shadow initialize.
=========================================================================================== */
extern Gfx shade_gfx_init[] = {
	gsDPPipeSync(),
	gsSPClearGeometryMode(G_LIGHTING | G_CULL_BACK),
	gsDPSetCombineMode(G_CC_MODULATEIA, G_CC_MODULATEIA),
	gsDPLoadTextureBlock(shade_txtdat, G_IM_FMT_IA, G_IM_SIZ_8b, 16, 16, 0,
							G_TX_WRAP|G_TX_MIRROR, G_TX_WRAP|G_TX_MIRROR, 
							4, 4, G_TX_NOLOD, G_TX_NOLOD),
	gsSPTexture(0xffff, 0xffff, 0, G_TX_RENDERTILE, G_ON),
	gsSPEndDisplayList()
};

/* ========================================================================================
		: Static DL of drawing shadow.
=========================================================================================== */
extern Gfx shade_gfx_Poly9[] = {
	gsSP1Triangle(0, 3, 4, 0),
	gsSP1Triangle(0, 4, 1, 0),
	gsSP1Triangle(1, 4, 2, 0),
	gsSP1Triangle(2, 4, 5, 0),
	gsSP1Triangle(3, 6, 4, 0),
	gsSP1Triangle(4, 6, 7, 0),
	gsSP1Triangle(4, 7, 8, 0),
	gsSP1Triangle(4, 8, 5, 0),
	gsSPEndDisplayList()
};

extern Gfx shade_gfx_Poly4[] = {
	gsSP1Triangle(0, 2, 1, 0),
	gsSP1Triangle(1, 2, 3, 0),
	gsSPEndDisplayList()
};

/* ========================================================================================
		: Static DL of shadow reset.
=========================================================================================== */
extern Gfx shade_gfx_clear[] = {
	gsDPPipeSync(),
	gsSPTexture(0xffff,0xffff, 0, G_TX_RENDERTILE, G_OFF),
	gsSPSetGeometryMode(G_LIGHTING | G_CULL_BACK ),
	gsDPSetCombineMode(G_CC_SHADE, G_CC_SHADE),
	gsSPEndDisplayList()
};


//---------------------------------------------------------------------------------------------
//
//
//		Used in 'wipe2.c'.
//
//
/* ========================================================================================
		: Static DL of wipe initialize.
=========================================================================================== */
extern Gfx wipe_gfx_init[] = {
	gsDPPipeSync(),
	gsSPClearGeometryMode(G_LIGHTING),
	gsSPMatrix(&Tani_I_matrix, G_MTX_PROJECTION | G_MTX_LOAD | G_MTX_NOPUSH ),
	gsSPMatrix(&Tani_O_matrix, G_MTX_PROJECTION | G_MTX_MUL  | G_MTX_NOPUSH ),
	gsSPMatrix(&Tani_I_matrix, G_MTX_MODELVIEW  | G_MTX_LOAD | G_MTX_NOPUSH ),
	gsSPPerspNormalize(0xffff),
	gsSPEndDisplayList()
};

/* ========================================================================================
		: Static DL of wipe clear.
=========================================================================================== */
extern Gfx wipe_gfx_clear[] = {
	gsDPPipeSync(),
	gsSPSetGeometryMode(G_LIGHTING),
	gsDPSetCombineMode(G_CC_SHADE, G_CC_SHADE),
	gsDPSetRenderMode(G_RM_OPA_SURF, G_RM_OPA_SURF2),
	gsSPEndDisplayList()
};

/* ========================================================================================
		: Static DL of drawing 8 triangles.
=========================================================================================== */
extern Gfx wipe_gfx_8tri[] = {
	gsSP1Triangle(0, 4, 1, 0),
	gsSP1Triangle(1, 4, 5, 0),
	gsSP1Triangle(1, 5, 2, 0),
	gsSP1Triangle(2, 5, 6, 0),
	gsSP1Triangle(2, 6, 7, 0),
	gsSP1Triangle(2, 7, 3, 0),
	gsSP1Triangle(3, 4, 0, 0),
	gsSP1Triangle(3, 7, 4, 0),
	gsSPEndDisplayList()
};


//---------------------------------------------------------------------------------------------
//
//
//		Used in 'bgdraw3.c'.
//
//
/* ========================================================================================
		: Static DL of initialize projection.
=========================================================================================== */
extern Gfx BG_init_project[] = {
	gsDPPipeSync(),
	gsSPClearGeometryMode(G_LIGHTING),
	gsDPSetCombineMode(G_CC_DECALRGB, G_CC_DECALRGB),
	gsSPPerspNormalize(0xffff),
	gsSPMatrix(&Tani_I_matrix, G_MTX_PROJECTION | G_MTX_LOAD | G_MTX_NOPUSH),
	gsSPEndDisplayList()
};

/* ========================================================================================
		: Static DL of initialize graphic.
=========================================================================================== */
extern Gfx BG_init_graphic[] = {
	gsSPMatrix(&Tani_I_matrix, G_MTX_MODELVIEW | G_MTX_LOAD | G_MTX_NOPUSH),
	gsSPTexture(0xffff,0xffff, 0, 0, G_ON),
	gs_Tani_SetUpTileDescrip(G_IM_FMT_RGBA, G_IM_SIZ_16b, 32, 32, 0, 0,
							 G_TX_CLAMP|G_TX_NOMIRROR, 5, G_TX_NOLOD,
							 G_TX_CLAMP|G_TX_NOMIRROR, 5, G_TX_NOLOD),
	gsSPEndDisplayList()
};

/* ========================================================================================
		: Static DL of clear and reset.
=========================================================================================== */
extern Gfx BG_mode_restore[] = {
	gsDPPipeSync(),
	gsSPTexture(0xffff,0xffff, 0, 0, G_OFF),
	gsSPSetGeometryMode(G_LIGHTING),
	gsDPSetCombineMode(G_CC_SHADE, G_CC_SHADE),
	gsSPEndDisplayList()
};


//---------------------------------------------------------------------------------------------
//
//
//		Used in 'water.c'.
//
//
/* ========================================================================================
		: Static DL of water initialize.
=========================================================================================== */
extern Gfx water_init[] = {
	gsDPPipeSync(),
	gsDPSetCombineMode(G_CC_DECALRGB, G_CC_DECALRGB),
	gsSPClearGeometryMode(G_LIGHTING | G_CULL_BACK),
	gsSPTexture(0xffff, 0xffff, 0, 0, G_ON),
	gs_Tani_SetUpTileDescrip(G_IM_FMT_RGBA, G_IM_SIZ_16b, 32, 32, 0, 0,
							 G_TX_WRAP|G_TX_NOMIRROR, 5, G_TX_NOLOD,
							 G_TX_WRAP|G_TX_NOMIRROR, 5, G_TX_NOLOD),
	gsSPEndDisplayList()
};

/* ========================================================================================
		: Static DL of water reset.
=========================================================================================== */
extern Gfx water_reset[] = {
	gsSPTexture(0xffff, 0xffff, 0, 0, G_OFF),
	gsDPPipeSync(),
	gsSPSetGeometryMode(G_LIGHTING | G_CULL_BACK),
	gsDPSetCombineMode(G_CC_SHADE, G_CC_SHADE),
	gsSPEndDisplayList()
};


//---------------------------------------------------------------------------------------------
//
//
//		Used in 'areamap.c'.
//
//
/* ========================================================================================
		: Texture data of the arrow.
=========================================================================================== */
static unsigned char areamap_arrow_txt[] = {
    0xf0,  0xf0,  0xf0,  0xff,  0xff,  0xf0,  0xf0,  0xf0,  
    0xf0,  0xf0,  0xff,  0xff,  0xff,  0xff,  0xf0,  0xf0,  
    0xf0,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xf0,  
    0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  
    0xf0,  0xf0,  0xf0,  0xff,  0xff,  0xf0,  0xf0,  0xf0,
    0xf0,  0xf0,  0xf0,  0xff,  0xff,  0xf0,  0xf0,  0xf0,  
    0xf0,  0xf0,  0xf0,  0xff,  0xff,  0xf0,  0xf0,  0xf0,  
    0xf0,  0xf0,  0xf0,  0xff,  0xff,  0xf0,  0xf0,  0xf0,  
};

/* ========================================================================================
		: Static DL of initialize projection.
=========================================================================================== */
extern Gfx areamap_init_project[] = {
	gsDPPipeSync(),
	gsSPClearGeometryMode(G_LIGHTING),
	gsDPSetCombineMode(G_CC_MODULATERGBA, G_CC_MODULATERGBA),
	gsDPSetRenderMode(G_RM_XLU_SURF, G_RM_XLU_SURF),
	gsSPPerspNormalize(0xffff),
	gsSPMatrix(&Tani_I_matrix, G_MTX_PROJECTION | G_MTX_LOAD | G_MTX_NOPUSH ),
	gsSPEndDisplayList()
};


/* ========================================================================================
		: Static DL of initialize graphic.
=========================================================================================== */
extern Gfx areamap_init_graphic1[] = {
	gsSPMatrix(&Tani_I_matrix, G_MTX_MODELVIEW | G_MTX_LOAD | G_MTX_NOPUSH ),
	gs_Tani_SetUpTileDescrip(G_IM_FMT_RGBA, G_IM_SIZ_16b, 64, 32, 0, 0,
								G_TX_CLAMP|G_TX_NOMIRROR, 6, G_TX_NOLOD,
								G_TX_CLAMP|G_TX_NOMIRROR, 5, G_TX_NOLOD),
	gsSPTexture(0xffff, 0xffff, 0, 0, G_ON),
	gsSPEndDisplayList()
};

/* ========================================================================================
		: Static DL of initialize graphic.
=========================================================================================== */
extern Gfx areamap_init_graphic2[] = {
	gsDPPipeSync(),
	gsDPSetCombineMode(G_CC_MODULATEIA, G_CC_MODULATEIA),
	gs_Tani_SetUpTileDescrip(G_IM_FMT_IA, G_IM_SIZ_8b, 8, 8, 0, 0,
								G_TX_CLAMP|G_TX_NOMIRROR, 3, G_TX_NOLOD,
								G_TX_CLAMP|G_TX_NOMIRROR, 3, G_TX_NOLOD	),
	gs_Tani_LoadTextureImage(areamap_arrow_txt, G_IM_FMT_IA, G_IM_SIZ_8b, 8, 8, 0, 7),
	gsSPEndDisplayList()
};

/* ========================================================================================
		: Static DL of initialize graphic.
=========================================================================================== */
extern Gfx areamap_mode_restore[] = {
	gsSPTexture(0xffff, 0xffff, 0, 0, G_OFF),
	gsDPPipeSync(),
	gsDPSetCombineMode(G_CC_SHADE, G_CC_SHADE),
	gsSPEndDisplayList()
};


//---------------------------------------------------------------------------------------------
//
//
//		Used in 'wave.c'.
//
//
/* ========================================================================================
		: Common static Gfx of moving status [ Fixed ].
===========================================================================================	*/
extern Gfx wave_move_init[] = {
	gsDPPipeSync(),
	gsSPSetGeometryMode(G_SHADING_SMOOTH | G_LIGHTING),
	gsDPSetCombineMode(G_CC_MODULATERGBA, G_CC_MODULATERGBA),
	gsSPLight((&Tani_light[0].l[0]),1),
	gsSPLight((&Tani_light[0].a   ),2),
	gsSPTexture(0xffff, 0xffff, 0, 0, G_ON),
	gsSPEndDisplayList()
};

extern Gfx wave_move_reset[] = {
	gsSPTexture(0xffff, 0xffff, 0, 0, G_OFF),
	gsDPPipeSync(),
	gsDPSetCombineMode(G_CC_SHADE, G_CC_SHADE),
	gsSPEndDisplayList()
};

extern Gfx wave_move_init_env[] = {
	gsDPPipeSync(),
	gsSPSetGeometryMode(G_SHADING_SMOOTH | G_LIGHTING | G_TEXTURE_GEN),
	gsDPSetCombineMode(G_CC_MODULATERGBA, G_CC_MODULATERGBA),
	gsSPLight((&Tani_light[0].l[0]),1),
	gsSPLight((&Tani_light[0].a   ),2),
	gsSPTexture(0x0c00, 0x0c00, 0, 0, G_ON),
	gsSPEndDisplayList()
};

extern Gfx wave_move_reset_env[] = {
	gsSPTexture(0x0c00, 0x0c00, 0, 0, G_OFF),
	gsDPPipeSync(),
	gsSPClearGeometryMode(G_TEXTURE_GEN),
	gsDPSetCombineMode(G_CC_SHADE, G_CC_SHADE),
	gsSPEndDisplayList()
};

extern Gfx wave_5tris[] = {
	gsSP1Triangle( 0, 1, 2, 0),
	gsSP1Triangle( 3, 4, 5, 0),
	gsSP1Triangle( 6, 7, 8, 0),
	gsSP1Triangle( 9,10,11, 0),
	gsSP1Triangle(12,13,14, 0),
	gsSPEndDisplayList()
};
