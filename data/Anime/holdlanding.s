/********************************************************************************
						Ultra 64 MARIO Brothers

					mario hold landing animation data

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 26, 1995
 ********************************************************************************/


	.data
	.align	2
	.align	0

animMarioHoldLanding:
	.half	 MAP_ANIM_ONETIME 			/* attribute       	 */
	.half	 MARIO_HEIGHT				/* animation height	 */
	.half	 0 							/* start frame     	 */
	.half	 0 							/* loop frame		 */
	.half	 2 							/* number of frames	 */
	.half	20 							/* number of joints	 */
	.word	0x0114						/* parameter offset  */
	.word	0x0018						/* index offset      */
	.word	0x0168						/* total bytes       */


	.half	    1,    0 	/* chn14_1translate x */
	.half	    1,    1 	/* chn14_1 translate y */
	.half	    1,    0 	/* chn14_1 translate z */
	.half	    1,    0 	/* chn14_1 rotate x */
	.half	    1,    2 	/* chn14_1 rotate y */
	.half	    1,    0 	/* chn14_1 rotate z */
	.half	    1,    0 	/* <m_waist>1 rotate x */
	.half	    1,    0 	/* <m_waist>1 rotate y */
	.half	    1,   41 	/* <m_waist>1 rotate z */
	.half	    1,    0 	/* <m_body>1_4 rotate x */
	.half	    1,    0 	/* <m_body>1_4 rotate y */
	.half	    1,   40 	/* <m_body>1_4 rotate z */
	.half	    1,    0 	/* <m_head>1 rotate x */
	.half	    1,    0 	/* <m_head>1 rotate y */
	.half	    1,   39 	/* <m_head>1 rotate z */
	.half	    1,   35 	/* chn6_4 rotate x */
	.half	    1,   36 	/* chn6_4 rotate y */
	.half	    1,   37 	/* chn6_4 rotate z */
	.half	    1,    7 	/* <m_larmA>1 rotate x */
	.half	    1,    8 	/* <m_larmA>1 rotate y */
	.half	    1,    9 	/* <m_larmA>1 rotate z */
	.half	    1,    0 	/* <m_larmB>1_1 rotate x */
	.half	    1,    0 	/* <m_larmB>1_1 rotate y */
	.half	    1,    6 	/* <m_larmB>1_1 rotate z */
	.half	    1,    3 	/* <m_lhand>1_1 rotate x */
	.half	    1,    4 	/* <m_lhand>1_1 rotate y */
	.half	    1,    5 	/* <m_lhand>1_1 rotate z */
	.half	    1,   31 	/* chn10_4 rotate x */
	.half	    1,   32 	/* chn10_4 rotate y */
	.half	    1,   33 	/* chn10_4 rotate z */
	.half	    1,   14 	/* <m_rarmA>1 rotate x */
	.half	    1,   15 	/* <m_rarmA>1 rotate y */
	.half	    1,   16 	/* <m_rarmA>1 rotate z */
	.half	    1,    0 	/* <m_rarmB>1_4 rotate x */
	.half	    1,    0 	/* <m_rarmB>1_4 rotate y */
	.half	    1,   13 	/* <m_rarmB>1_4 rotate z */
	.half	    1,   10 	/* <m_rhand>1_1 rotate x */
	.half	    1,   11 	/* <m_rhand>1_1 rotate y */
	.half	    1,   12 	/* <m_rhand>1_1 rotate z */
	.half	    1,    0 	/* chn15_4 rotate x */
	.half	    1,    0 	/* chn15_4 rotate y */
	.half	    1,   34 	/* chn15_4 rotate z */
	.half	    1,   28 	/* <m_llegA>1_4 rotate x */
	.half	    1,   29 	/* <m_llegA>1_4 rotate y */
	.half	    1,   30 	/* <m_llegA>1_4 rotate z */
	.half	    1,    0 	/* <m_llegB>1_4 rotate x */
	.half	    1,    0 	/* <m_llegB>1_4 rotate y */
	.half	    1,   27 	/* <m_llegB>1_4 rotate z */
	.half	    1,   24 	/* <m_ltoot>1_4 rotate x */
	.half	    1,   25 	/* <m_ltoot>1_4 rotate y */
	.half	    1,   26 	/* <m_ltoot>1_4 rotate z */
	.half	    1,    0 	/* chn17_4 rotate x */
	.half	    1,    0 	/* chn17_4 rotate y */
	.half	    1,   38 	/* chn17_4 rotate z */
	.half	    1,   21 	/* <m_rlegA>1_4 rotate x */
	.half	    1,   22 	/* <m_rlegA>1_4 rotate y */
	.half	    1,   23 	/* <m_rlegA>1_4 rotate z */
	.half	    1,    0 	/* <m_rlegB>1_4 rotate x */
	.half	    1,    0 	/* <m_rlegB>1_4 rotate y */
	.half	    1,   20 	/* <m_rlegB>1_4 rotate z */
	.half	    1,   17 	/* <m_rtoot>1 rotate x */
	.half	    1,   18 	/* <m_rtoot>1 rotate y */
	.half	    1,   19 	/* <m_rtoot>1 rotate z */

	.half	0x0000,0x00AA,0x3FFF,0xE7CE,0xF984,0xE96B,0xF446,0x12E2,0xF1E6,0x8E6F,0x0ACF,0x020D,0xEDD2,0xE463,0xF3F3,0x14EA
	.half	0x9940,0xFDFF,0xFF5C,0xD845,0x1C6F,0x1E12,0x0884,0xBEE5,0xFF96,0x00DB,0xDDFC,0x565B,0xF86A,0xEB45,0x8833,0x8001
	.half	0x8001,0x4171,0xBF5F,0x8001,0x8001,0x4171,0xBF5F,0x1681,0x0A74,0x367D
