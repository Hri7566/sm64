/********************************************************************************
						Ultra 64 MARIO Brothers

					  stage17 hierarchy data module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"

extern Gfx gfx_e1_base[]		;
extern Gfx gfx_e1_bg01[]		;
extern Gfx gfx_e1_bg02[]		;
extern Gfx gfx_e1_bg03[]		;
extern Gfx gfx_e1_bg04[]		;
extern Gfx gfx_e1_bg05[]		;
extern Gfx gfx_e1_bg06[]		;
extern Gfx gfx_e1_bg07[]		;
extern Gfx gfx_e1_bg08[]		;
extern Gfx gfx_e1_bg09[]		;
extern Gfx gfx_e1_bg10[]		;
extern Gfx gfx_e1_bg11[]		;
extern Gfx gfx_e1_bg12[]		;
extern Gfx gfx_e1_bg13[]		;
extern Gfx gfx_e1_bg14[]		;
extern Gfx gfx_e1_bg15[]		;
extern Gfx gfx_e1_deru[]		;
extern Gfx gfx_e1_shiso[]		;
extern Gfx gfx_e1_ukishima[]	;
extern Gfx gfx_e1_kurumajiku[]	;
extern Gfx gfx_e1_kuruma[]		;
extern Gfx gfx_e1_dorifu0[]		;
extern Gfx gfx_e1_dorifu1[]		;
extern Gfx gfx_e1_dorifu2[]		;
extern Gfx gfx_e1_dorifu3[]		;
extern Gfx gfx_e1_dorifu4[]		;


/* ===============================================================================
        : Hierarchy map data of HmsExt1Bg01.
================================================================================== */
Hierarchy RCP_HmsExt1Bg01[] = {
    hmsHeader(2000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e1_bg01)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext1Bg02.
================================================================================== */
Hierarchy RCP_HmsExt1Bg02[] = {
    hmsHeader(2300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e1_bg02)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of HmsExt1Bg03.
================================================================================== */
Hierarchy RCP_HmsExt1Bg03[] = {
    hmsHeader(2000)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e1_bg03)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of HmsExt1Bg04.
================================================================================== */
Hierarchy RCP_HmsExt1Bg04[] = {
    hmsHeader(4000)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e1_bg04)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of HmsExt1Bg05.
================================================================================== */
Hierarchy RCP_HmsExt1Bg05[] = {
    hmsHeader(3500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e1_bg05)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of HmsExt1Bg06.
================================================================================== */
Hierarchy RCP_HmsExt1Bg06[] = {
    hmsHeader(2500)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e1_bg06)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of HmsExt1Bg07.
================================================================================== */
Hierarchy RCP_HmsExt1Bg07[] = {
    hmsHeader(1300)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e1_bg07)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext1Bg08.
================================================================================== */
Hierarchy RCP_HmsExt1Bg08[] = {
    hmsHeader(2900)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e1_bg08)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of HmsExt1Bg09.
================================================================================== */
Hierarchy RCP_HmsExt1Bg09[] = {
    hmsHeader(1500)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e1_bg09)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext1Bg10.
================================================================================== */
Hierarchy RCP_HmsExt1Bg10[] = {
    hmsHeader(2400)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e1_bg10)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext1Bg11.
================================================================================== */
Hierarchy RCP_HmsExt1Bg11[] = {
    hmsHeader(2400)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e1_bg11)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of HmsExt1Bg12.
================================================================================== */
Hierarchy RCP_HmsExt1Bg12[] = {
    hmsHeader(300)
    hmsBegin()
        hmsGfx(RM_XSURF, gfx_e1_bg12)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext1Bg13.
================================================================================== */
Hierarchy RCP_HmsExt1Bg13[] = {
    hmsHeader(2400)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e1_bg13)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext1Bg14.
================================================================================== */
Hierarchy RCP_HmsExt1Bg14[] = {
    hmsHeader(1100)
    hmsBegin()
        hmsGfx(RM_SPRITE, gfx_e1_bg14)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext1Bg15.
================================================================================== */
Hierarchy RCP_HmsExt1Bg15[] = {
    hmsHeader(1400)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e1_bg15)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of HmsExt1Deru.
================================================================================== */
Hierarchy RCP_HmsExt1Deru[] = {
    hmsHeader(1500)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_e1_deru)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of HmsExt1Shiso.
================================================================================== */
Hierarchy RCP_HmsExt1Shiso[] = {
    hmsHeader(1100)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e1_shiso)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of HmsExt1Ukishima.
================================================================================== */
Hierarchy RCP_HmsExt1Ukishima[] = {
    hmsHeader(600)
    hmsBegin()
        hmsGfx(RM_SURF, gfx_e1_ukishima)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext1Kurumajiku.
================================================================================== */
Hierarchy RCP_HmsExt1Kurumajiku[] = {
    hmsHeader(600)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_e1_kurumajiku)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext1Kuruma.
================================================================================== */
Hierarchy RCP_HmsExt1Kuruma[] = {
    hmsHeader(600)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_e1_kuruma)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext1Dorifu0.
================================================================================== */
Hierarchy RCP_HmsExt1Dorifu0[] = {
    hmsHeader(2000)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_e1_dorifu0)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext1Dorifu1.
================================================================================== */
Hierarchy RCP_HmsExt1Dorifu1[] = {
    hmsHeader(2000)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_e1_dorifu1)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext1Dorifu2.
================================================================================== */
Hierarchy RCP_HmsExt1Dorifu2[] = {
    hmsHeader(2000)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_e1_dorifu2)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext1Dorifu3.
================================================================================== */
Hierarchy RCP_HmsExt1Dorifu3[] = {
    hmsHeader(2000)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_e1_dorifu3)
    hmsEnd()
    hmsExit()
};

/* ===============================================================================
        : Hierarchy map data of Ext1Dorifu4.
================================================================================== */
Hierarchy RCP_HmsExt1Dorifu4[] = {
    hmsHeader(2000)
    hmsBegin()
        hmsGfx(RM_SURF  , gfx_e1_dorifu4)
    hmsEnd()
    hmsExit()
};

/********************************************************************************/
/*	Hierarchy map scene 1.														*/
/********************************************************************************/
Hierarchy RCP_Stage17Scene1[] = {
	hmsScene(160, 120, 160, 120, 10)
	hmsBegin()
		hmsLayer(0)
		hmsBegin()
			hmsOrtho(100)
			hmsBegin()
				hmsClear(7, DrawBackGround)
			hmsEnd()
		hmsEnd()

		hmsLayer(1)
		hmsBegin()
			hmsPerspective(45, SCREEN_NEAR, 20000, ZoomControl)
			hmsBegin()
				hmsCamera(CAM_PARALLEL,  0,2000,6000,  0,0,0, GameCamera)
				hmsBegin()
					hmsGfx(RM_SPRITE, gfx_e1_base)
					hmsObject()
					hmsCProg(0, WeatherProc)
				hmsEnd()
			hmsEnd()
		hmsEnd()
	hmsEnd()
	hmsExit()
};
