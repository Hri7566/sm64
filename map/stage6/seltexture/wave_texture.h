/********************************************************************************
						Ultra 64 MARIO Brothers

					Texture data of wave include file.

			Copyright 1995 Nintendo co., ltd.  All rights reserved

				This module was programmed by Y.Tanimoto

							Feb 6, 1996
*********************************************************************************/

#include "wavetexture/selr_wave00_texture.h"
#include "wavetexture/selr_wave01_texture.h"
#include "wavetexture/selr_wave02_texture.h"
#include "wavetexture/selr_wave03_texture.h"
#include "wavetexture/selr_wave04_texture.h"
#include "wavetexture/selr_wave05_texture.h"		//	B1. Brown tile. 32x32.
#include "wavetexture/selr_wave06_texture.h"		//	B1.	Silver Env. 32x32.
#include "wavetexture/selr_wave07_texture.h"		//	B1. Water Env.	32x32.
#include "wavetexture/selr_wave08_texture.h"
#include "wavetexture/selr_wave09_texture.h"
#include "wavetexture/selr_wave10_texture.h"
#include "wavetexture/selr_wave11_texture.h"
#include "wavetexture/selr_wave12_texture.h"
