/********************************************************************************
						Ultra 64 MARIO Brothers

				   stage13 graphic display list module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							December 8, 1995
 ********************************************************************************/

#include "../../headers.h"
#include "../texture/texsymbol.h"

#define		STAGE13_FOG_R		0
#define		STAGE13_FOG_G		0
#define		STAGE13_FOG_B		0
#define		STAGE13_FOG_START	980

#include "big_world/bigworld_ext_texture.h"

#include "big_world/bs1_shape.sou"
#include "big_world/bs2_shape.sou"
#include "big_world/bs3_shape.sou"
#include "big_world/bs4_shape.sou"
#include "big_world/bs5b_shape.sou"
#include "big_world/bs5s_shape.sou"
#include "big_world/bs_futa_shape.sou"
#include "big_world/bs_sima_shape.sou"

#include "big_world/bs_naka1_shape.sou"
#include "big_world/bs_naka2_shape.sou"
#include "big_world/bs_naka3_shape.sou"
#include "big_world/bs_naka4_shape.sou"

#include "big_world/cx1301.flk"
#include "big_world/cx1302.flk"
#include "big_world/cx1303.flk"

#include "big_world/cx1301.tag"
#include "big_world/cx1302.tag"
#include "big_world/cx1303.tag"

#include "big_world/bs_futa_check.flk"

#include "big_world/bigkame.ral"

/********************************************************************************/
/*	Water surface records in 13 - 1.											*/
/********************************************************************************/
static short waterSurf_13_01_00[] = {
	1,
	0,   10,  10,  -8191,-8191,   -8191, 6246,   -5119, 6246,   -5119,-8191,  1,  150,  0,
};

static short waterSurf_13_01_01[] = {
	1,
	0,   10,  10, -4607, 4506,   -4607, 8192,    6963, 8192,    6963, 4506,  1,  150,  0,
};

static short waterSurf_13_01_02[] = {
	1,
	0,   10,  10, -1023,-2555,   -1023, -507,    1024, -507,    1024,-2555,  1,  150,  0,
};

WaterSurfRec waterSurf_13_01[] = {
		{	 0, waterSurf_13_01_00	 },
		{	 1, waterSurf_13_01_01	 },
		{	 2, waterSurf_13_01_02	 },
		{	-1,	NULL	}					//	End of  WaterSurfRec data.
};

/********************************************************************************/
/*	Water surface records in 13 - 2.											*/
/********************************************************************************/
static short waterSurf_13_02_00[] = {
	1,
	0,    3,  3, -2457,-2457,   -2457, 1874,   -1535, 1874,   -1535,-2457,  1,  150,  0,
};

static short waterSurf_13_02_01[] = {
	1,
	0,    3,  3, -1381, 1352,   -1381, 2458,    2089, 2458,    2089, 1352,  1,  150,  0,
};

static short waterSurf_13_02_02[] = {
	1,
	0,    3,  3, -306, -766,    -306, -152,     307, -152,     307, -766,  1,  150,  0,
};

WaterSurfRec waterSurf_13_02[] = {
		{	 0, waterSurf_13_02_00	 },
		{	 1, waterSurf_13_02_01	 },
		{	 2, waterSurf_13_02_02	 },
		{	-1,	NULL	}					//	End of  WaterSurfRec data.
};
