/**************************************************************************************************

					Weather & GushEffect Function header file

							programed by Iwamoto Daiki		Feburary 1 1996

***************************************************************************************************/

typedef struct {
	char flag;
	short anim;
	int posx;
	int posy;
	int posz;
	int work[10];
} Gush;


extern Gfx RCP_snowTxt[];
extern Gfx RCP_marinSnowTxt[];
extern Gfx RCP_snowEnd[];
extern GfxPtr GushEffect(int,short*,short*,short*);
extern char weather_trg;
extern Gush *gushData;
extern unsigned short* flowerDataPtr[];
extern unsigned short* magumaDataPtr[];
extern unsigned short* gushDataPtr[];
extern Gfx RCP_effectTxt[];
extern Gfx RCP_effectLoad[];
extern Gfx RCP_snowEnd[];





extern short gushBuf[10];

extern void SetCameraAngle(short*, short*,short*,short*, short*);
extern void VertexForCameraAngle(short*,short*,short*,short,short);
extern void StopWeather(Gush *data);




/*--------- Uzumaki Define -------------*/

#define Uz_code		(gushBuf[0])
#define Uz_stx		(gushBuf[1])
#define Uz_sty		(gushBuf[2])
#define Uz_stz		(gushBuf[3])
#define Uz_enx		(gushBuf[4])
#define Uz_eny		(gushBuf[5])
#define Uz_enz		(gushBuf[6])
#define Uz_objNum	(gushBuf[7])
#define Uz_angleX	(gushBuf[8])
#define Uz_angleZ	(gushBuf[9])
#define Uz_angY		work[0]
#define Uz_dist		work[1]
#define Uz_count	work[2]
#define Uz_buffY	work[3]

/*--------- Fukidasi Define -------------*/

#define Fu_stx		(gushBuf[1])
#define Fu_sty		(gushBuf[2])
#define Fu_stz		(gushBuf[3])
#define Fu_enx		(gushBuf[4])
#define Fu_eny		(gushBuf[5])
#define Fu_enz		(gushBuf[6])
#define Fu_objNum	(gushBuf[7])
#define Fu_angY		work[0]
#define Fu_dist		work[1]



