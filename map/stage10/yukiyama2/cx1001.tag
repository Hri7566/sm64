extern short cx1001_info_tag[]={
	TAGCODE_e_t_kanban+( 32<<9), 4086, 1024,  400,   86,
	TAGCODE_e_coin_ground+(  0<<9), 1285, 2210,  385,    0,
	TAGCODE_e_coin_ground+(  0<<9), 1728, 2560, -671,    0,
	TAGCODE_e_coin_ground+(  0<<9), 1371, 2188, -500,    0,
	TAGCODE_e_coin_ground+(  0<<9), 1814, 3174,  114,    0,
	TAGCODE_e_coin_ground+(  0<<9),   28, 3328, 1885,    0,
	TAGCODE_e_coin_ground+(  0<<9), -228, 3482, 1742,    0,
	TAGCODE_e_bom_cannon+(  0<<9), 4483,  821, 1168,  128,
	TAGCODE_e_snowman+(  0<<9),-3452, 1110,-3364,    0,
	TAGCODE_e_snowman+(  0<<9), 5422, 1065,-1288,    0,
	TAGCODE_e_snowman+(  0<<9),-6533, 2048,-2444,    0,
	TAGCODE_e_coin_appstar+(  0<<9), 4750, 1061,-5230,    0,
	TAGCODE_e_coin_appstar+(  0<<9),  416, 1050,-4522,    0,
	TAGCODE_e_coin_appstar+(  0<<9),-6560, 2040,-5080,    0,
	TAGCODE_e_coin_appstar+(  0<<9),-6760, 2040,-1360,    0,
	TAGCODE_e_coin_appstar+(  0<<9),-6880, 1857, 1000,    0,
	TAGCODE_e_coin_appstar+(  0<<9),-4211, 1092,-4723,    0,
	TAGCODE_e_coin_appstar+(  0<<9),-6271, 1390, 4764,    0,
	TAGCODE_e_coin_appstar+(  0<<9), -529, 1050,-5329,    0,
	TAGCODE_e_furafura+(  0<<9),-3760, 1120, 1240,    0,
	TAGCODE_e_furafura+(  0<<9), 3840, 1240,-5280,    0,
	TAGCODE_e_furafura+(  0<<9),-3440, 1400,  -40,    0,
	TAGCODE_e_furafura+(  0<<9),  400, 1060, 5860,    0,
	TAGCODE_e_furafura+(  0<<9),  880, 1080, 4860,    0,
	TAGCODE_e_furafura+(  0<<9), 1400, 1080, 3860,    0,
	TAGCODE_e_furafura+(  0<<9), 3400, 1660,-2920,    0,
	TAGCODE_e_furafura+(  0<<9),-3171, 1075,-4765,    0,
	TAGCODE_e_furafura+(  0<<9),-4241, 1009,-3834,    0,
	TAGCODE_e_yellow_block+(  0<<9),-5450, 1300, 5900,    0,
	TAGCODE_e_gurubiri+(  0<<9), 4060,  900,-2940,    0,
	TAGCODE_e_furafura+(  0<<9),-4096, 1125, 3062,    0,
	TAGCODE_e_furafura+(  0<<9),-4536, 1125, 3782,    0,
	TAGCODE_e_manycoin1+( 23<<9), -660, 2120, 1340,    0,
	TAGCODE_e_coin+(  0<<9),-1520, 1040,  940,    0,
	TAGCODE_e_coin+(  0<<9),-1340, 1280, 1020,    0,
	TAGCODE_e_coin+(  0<<9),-1180, 1520, 1120,    0,
	TAGCODE_e_t_kanban+(104<<9), -835, 1125,-3856,   61,
	TAGCODE_e_t_kanban+( 64<<9),-5050, 1020, 6026,   16,
	TAGCODE_e_heyho+(  0<<9), 2766, 1522,-3633,    0,
	TAGCODE_e_t_kanban+( 48<<9),-3600, 1024, -800,  148,
	TAGCODE_e_kuramochi+(  0<<9), 2440, 1024, 4840,    0,
	TAGCODE_e_kuramochi+(  0<<9),-2400, 1177,-4200,    0,
	TAGCODE_e_block_escape+(  0<<9),-3380, 1360,-4140,    0,
	TAGCODE_e_blockstar4+(  0<<9),-4700, 1300, 5850,    0,
	TAGCODE_e_coin_ground+(  0<<9), 2909, 1024, 4245,    0,
	TAGCODE_e_coin_ground+(  0<<9), 3418, 1024, 3554,    0,
	TAGCODE_e_1up_tree+(  0<<9),    0, 5420,    0,    0,
	TAGCODE_END
};
