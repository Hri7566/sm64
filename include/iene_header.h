/***************************************************************************************************
						ienemy Headders.h																

								
					Programed by Iwamoto Daiki

***************************************************************************************************/
#define FRAME 30
#define degree(x) 182.04444 * (x)

/*------------ Player strategy Value      -------------*/
#define player_posX		(player1stp->map.position[0])
#define player_posY		(player1stp->map.position[1])
#define player_posZ		(player1stp->map.position[2])
#define player_angleX	(player1stp->map.angle[0])
#define player_angleY	(player1stp->map.angle[1])
#define player_angleZ	(player1stp->map.angle[2])

/*------------ ObjMoveEvent Return Value -------------*/
#define OM_TOUCH		0x0001
#define OM_WALL			0x0002
#define OM_WATER		0x0004
#define OM_GROUND		0x0009

/*------------ ObjMoveEvent BGPlane Data Macro -------------*/

#define bgdata_attr		(moveobj_bgcp->BGattr)
#define bgdata_angleY	((moveobj_bgcp->BGdata1)&0x00ff)*0x100
#define bgdata_wspeed	(((moveobj_bgcp->BGdata1)&0xff00) >> 8)


/*------------ Obj Strategy Macro -------------*/

#define motherp				(execstp->motherobj)
#define mother_mode			((execstp->motherobj)->s[stw_mode].d)
#define mother_work0		((execstp->motherobj)->s[stw_work0].d)
#define mother_work1		((execstp->motherobj)->s[stw_work1].d)
#define mother_work2		((execstp->motherobj)->s[stw_work2].d)
#define mother_work3		((execstp->motherobj)->s[stw_work3].d)

/*------------ iwamoto enemy external value -------------*/

extern BGCheckData *moveobj_bgcp;

/*------------ iwamoto enemy Prototype functon -------------*/
extern char PlayerApproach(float ,float ,float ,int);
extern void PlayerApproachOnOff(StrategyRecord* ,int);
extern short ObjMoveEvent(void);
extern void Obj_reset(int ,unsigned long*,int);





