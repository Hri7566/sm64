/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Victory Star Strategy

				Feburary 31 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
						Victory Star Path Data
 ********************************************************************************/
e_V_star:
	p_initialize(item)
	p_setbit(flag,stf_moveON )
	p_program(s_V_star_init)


	p_while
		p_program(s_V_star_event)
	p_loop

/*------------------------------------------------------------*/	
e_V_starspark:
	p_initialize(effect)
	p_softspritemodeON
	p_setbit(flag,stf_moveON)
	p_setf(animepositionY,100/4)
	p_animereset

	p_while
		p_animeinc
		p_program(s_V_starspark)
	p_loop


/*------------------------------------------------------------*/	
e_V_stardust:
	p_initialize(option)
	p_softspritemodeON
	p_setbit(flag,stf_moveON)
	p_animereset

	p_while
		p_program(s_V_stardust)
	p_loop


/*################*/
#else
/*################*/


/***************************************************************************************************
		C Program
 ****************************************************************************************************/
#define v_star_radius 	(execstp->s[stw_work5].d)
#define star_shape		(execstp->s[stw_work0].d)
#define VER2	1
#define I_STAR	0
#define I_KEY	1

/***************************************************************************************************
						Victory Star Init
***************************************************************************************************/
extern void s_V_star_init(void)
{

	obj_attX   = player_posX;	
	obj_worldY = player_posY + 30;	
	obj_attZ   = player_posZ;	

	execstp->s[stw_angleY].d = player_angleY + 0x8000;
	v_star_radius = 100;

#if VER2
	if (activeStageNo == 30 || activeStageNo == 33) {
		execstp->map.shape = stageShapes[S_kopakey];  		//Change Shape Koppa Clear
		obj_animeangleX = 0;
		obj_animeangleZ = 0xc000;
		s_set_scale(0.1);
		star_shape = I_KEY;		
	}
	else {
		execstp->map.shape = stageShapes[S_polystar];  		//Change Shape 
		obj_animeangleX = 0;
		obj_animeangleZ = 0;
		s_set_scale(0.4);		
		star_shape = I_STAR;		
	}
#else

	execstp->map.shape = stageShapes[S_polystar];  		//Change Shape
	obj_animeangleX = 0;
	obj_animeangleZ = 0;
	s_set_scale(0.4);		
	star_shape = I_STAR;		

#endif



}

/***************************************************************************************************
						Victory Star Rotation
***************************************************************************************************/
static void StarRotation(void)
{

	execstp->s[stw_worldX].f = obj_attX + sin(obj_angleY)*(v_star_radius/2);	
	execstp->s[stw_worldZ].f = obj_attZ + cos(obj_angleY)*(v_star_radius/2);	
	execstp->s[stw_worldY].f += 5;	

	obj_animeangleY += 0x1000;
	obj_angleY += 0x2000;

	if (obj_timer == 40) {	
		obj_mode = 1;
	}

	if (obj_timer < 35) {
		s_makeobj_nowpos(execstp,S_spark,e_V_starspark);
		v_star_radius++;
	}
	else {
		v_star_radius -= 20;
	}

}

/***************************************************************************************************
						Victory Star Scale
***************************************************************************************************/
static void StarScaleUp(void)
{

	if (obj_timer < 10) {
		if (star_shape == I_STAR) s_set_scale((float)(obj_timer)/10.);		
		else 					  s_set_scale((float)(obj_timer)/30.);
		obj_animeangleY += 0x1000;
	}
	else {
		obj_animeangleY = player_angleY;
	}

	if (obj_timer == 59) {
		execstp->status = 0; 
	}
}
/***************************************************************************************************
						Victory Star Event
***************************************************************************************************/
extern void s_V_star_event(void)
{

	switch (obj_mode) {	
		case 0: StarRotation(); break;
		case 1: StarScaleUp();  break;
	}



}
/***************************************************************************************************
						Victory Star Spark
***************************************************************************************************/
extern void s_V_starspark(void)
{

	execstp->s[stw_worldY].f -= 15;

	if (obj_timer == 12) execstp->status = 0;

}

/***************************************************************************************************
						Victory Star Dust
***************************************************************************************************/
extern void s_V_stardust(void)
{


	s_burneffect(0,10,30);

	execstp->status = 0;

}





/*################*/
#endif
/*################*/


