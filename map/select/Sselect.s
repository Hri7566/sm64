/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 28 sequence module

			Copyright 1996 Nintendo co., ltd.  All rights reserved

							March 14, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

#define	FADEOUT_TIME		190

		.data
		.align	2
		.align	0

		.globl	SEQ_SelectSequence
		.globl	SEQ_CourseMenu


/********************************************************************************/
/*	File select sequence.														*/
/********************************************************************************/

SEQ_SelectSequence:

	seqInitStage()
	seqLoadCode(DEMOCODE_START	, 	  _TCodeSegmentRomStart,	 _TCodeSegmentRomEnd)
	seqLoadPres(SEGMENT_STAGE1	, _GfxSelectSegmentRomStart, _GfxSelectSegmentRomEnd)
	seqLoadData(SEGMENT_PATHDATA,      _PathSegmentRomStart,      _PathSegmentRomEnd)

	seqBeginConstruction()

		seqHmsShape(S_bg01       , RCP_Hms_MarioTEX_Bu)
		seqHmsShape(S_bg02       , RCP_Hms_aka_Button	)
		seqHmsShape(S_bg03       , RCP_Hms_ao_Button	)
		seqHmsShape(S_bg04       , RCP_Hms_ki_Button	)
		seqHmsShape(S_bg05       , RCP_Hms_midori_Button)
		seqHmsShape(S_bg06       , RCP_Hms_MarioNO_Bu)
		seqHmsShape(S_bg07       , RCP_Hms_NewTEX_Bu)
		seqHmsShape(S_bg08       , RCP_Hms_NewNO_Bu)
		seqHmsShape(S_bg09       , RCP_Hms_momo_Button)
		seqHmsShape(S_bg10       , RCP_Hms_base_Button)

		seqBeginScene(1, RCP_SelectScene1)
			seqActor(S_NULL			,    0,   0,   -19000,  0,0,0,  4,0,0,    e_fileSelect	)
			seqActor(S_bg04			,    0,   0,   -19000,  0,0,0,  4,0,0,    e_istop		)    	 
			seqMapInfo(dummy_info)
		seqEndScene()

	seqEndConstruction()


	seqOpenScene(1)
	seqPlayMusic(NA_FILE_SELECT_BGM, 0)
	seqWipe(WIPE_FADE_IN, 16, 255, 255, 255)

	seqCProgram(SeSelectInitProcess, 0)	
	seqRunning(SeSelectProcess, 0)
	seqSetGameMode(GMODE_PLAYER)
	seqStopMusic(FADEOUT_TIME)
	seqWipe(WIPE_FADE_OUT, 16, 255, 255, 255)
	seqWait(16)

	seqDestroyStage()
	seqFreeze(1)

	seqSetResult(16)
	seqChain(SEGMENT_GAMEDATA, _GameSegmentRomStart, _GameSegmentRomEnd, SEQ_GameSequence)



/********************************************************************************/
/*	Course start menu sequence.													*/
/********************************************************************************/

SEQ_CourseMenu:

	seqCProgram(CheckCourseMenu, 0)
	seqTstJump(SEQ_EQ, 0, ExitCourseMenu)


	seqInitStage()
	seqLoadCode(DEMOCODE_START, _TCodeSegmentRomStart	 ,	   _TCodeSegmentRomEnd)
	seqLoadPres(SEGMENT_STAGE1, _GfxSelectSegmentRomStart, _GfxSelectSegmentRomEnd)

	seqBeginConstruction()

		seqBeginScene(2, RCP_SelectScene2)
			seqActor(S_NULL		,    0,   -100,    0,  0,0,0,  4,0,0,    e_courseStarmain	)
			seqMapInfo(dummy_info)

		seqEndScene()

	seqEndConstruction()

	seqOpenScene(2)
	seqWipe(WIPE_FADE_IN, 16, 255, 255, 255)
	seqWait(16)
	seqPlayMusic(NA_STAGESTART_BGM, 0)

	seqCProgram(SeStarSelectInitProc, 0)	
	seqRunning(SeStarSelectProcess, 0)	
	seqSetGameMode(GMODE_LEVEL)
	seqStopMusic(FADEOUT_TIME)
	seqWipe(WIPE_FADE_OUT, 16, 255, 255, 255)
	seqWait(16)

	seqDestroyStage()
	seqFreeze(1)


ExitCourseMenu:

	seqExit
