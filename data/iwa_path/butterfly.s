/********************************************************************************
					-------------------------
				 	 Ultra 64 MARIO Brothers
					-------------------------
					    Butterfly Strategy

				Feburary 13 1996       programed by Iwamoto Daiki
 ********************************************************************************/

/*################*/
#ifdef ASSEMBLER
/*################*/

/********************************************************************************
				butterfly path program   											    
 ********************************************************************************/
e_butterfly:
	p_initialize(option)
	p_setbit(flag,stf_moveON| stf_YangleSAME)
	p_set_pointer(skelanime,RCP_ButterflyAnime)
	p_BGcheckYset
	p_setf(animepositionY,20/4)
	p_program(s_butterflyInit)
	
butterfly_main:
	p_while
		p_program(s_butterflyfunc)
	p_loop





/*################*/
#else
/*################*/


/***************************************************************************************************
		C Program
 ****************************************************************************************************/

#define SPEED 30/4
#define chyo_counter stw_work0    //fukusuu no random count



/***************************************************************************************************
						Butterfly Initialize
***************************************************************************************************/
extern void s_butterflyInit(void)
{

	stSetSkelAnimeNumber(1);

	execstp->s[chyo_counter].d = (int)(Randomf()*100);
	execstp->map.skelanim.frame = (int)(Randomf()*7);

	obj_attX = obj_worldX;
	obj_attY = obj_worldY;
	obj_attZ = obj_worldZ;

}

/***************************************************************************************************
						Butterfly World Move
***************************************************************************************************/
static void ButterflyWorldMove(int speed)
{
	Plane *plane;
	short direct = execstp->s[stw_angleY].d;
	short directx = execstp->s[stw_angleX].d;
	short swing_count = execstp->s[chyo_counter].d;
	float groundY;

	execstp->s[stw_speedX].f  = speed*sin(direct);
	execstp->s[stw_speedY].f  = speed*sin(directx);
	execstp->s[stw_speedZ].f  = speed*cos(direct);

	execstp->s[stw_worldX].f += execstp->s[stw_speedX].f;
	execstp->s[stw_worldZ].f += execstp->s[stw_speedZ].f;

	if (obj_mode == 1) execstp->s[stw_worldY].f -= (execstp->s[stw_speedY].f + cos((short)(swing_count*655.36))*20/4);
	else							  execstp->s[stw_worldY].f -= execstp->s[stw_speedY].f;
	
	groundY = GroundCheck(execstp->s[stw_worldX].f,execstp->s[stw_worldY].f,execstp->s[stw_worldZ].f,&plane);
	if (execstp->s[stw_worldY].f < (groundY + 10/4) ) execstp->s[stw_worldY].f = (groundY + 10/4);

	
	execstp->s[chyo_counter].d ++;
	if (execstp->s[chyo_counter].d > 100) execstp->s[chyo_counter].d = 0;

}
/***************************************************************************************************
						Butterfly Decide
***************************************************************************************************/
static void ButterflyDirection(void)
{

	player1stp->s[stw_worldX].f += (5*execstp->s[chyo_counter].d)/4;
	player1stp->s[stw_worldZ].f += (5*execstp->s[chyo_counter].d)/4;
	s_chase_obj_angle(execstp,player1stp,stw_angleY,0x300);
	player1stp->s[stw_worldX].f -= (5*execstp->s[chyo_counter].d)/4;
	player1stp->s[stw_worldZ].f -= (5*execstp->s[chyo_counter].d)/4;

	player1stp->s[stw_worldY].f += (0x100+(0x5*execstp->s[chyo_counter].d))/4;
	s_chase_obj_angle(execstp,player1stp,stw_angleX,0x500);
	player1stp->s[stw_worldY].f -= (0x100+(0x5*execstp->s[chyo_counter].d))/4;


}




/*----------------------Butterfly Stay------------------------------------------------------------*/
/***************************************************************************************************
						Butterfly Stay
***************************************************************************************************/
static void ButterflyStay(void) 
{

	if (PlayerApproach(obj_worldX,obj_worldY,obj_worldZ,1000)) {
		stSetSkelAnimeNumber(0);
		obj_mode = 1;
		execstp->s[stw_angleY].d = player1stp->map.angle[1];
	}


}

/***************************************************************************************************
						Butterfly TakeOff
***************************************************************************************************/
static void ButterflyTakeOff(void) 
{
	ButterflyDirection();
	ButterflyWorldMove(SPEED);

	if (PlayerApproach(obj_attX,obj_attY,obj_attZ,1200) == 0) {
		obj_mode = 2;
	}

}
/***************************************************************************************************
						Butterfly Come Back
***************************************************************************************************/
static void ButterflyComeback(void) 
{
	float dx = obj_attX - obj_worldX;
	float dy = obj_attY - obj_worldY;
	float dz = obj_attZ - obj_worldZ;
	short targetAngleY = atan(dz,dx);
	short targetAngleX = atan(sqrtf(dx*dx + dz*dz),-dy);

 	execstp->s[stw_angleY].d = s_chase_angle(execstp->s[stw_angleY].d,targetAngleY,0x800);
 	execstp->s[stw_angleX].d = s_chase_angle(execstp->s[stw_angleX].d,targetAngleX,0x50);

	ButterflyWorldMove(SPEED);

	if (dx*dx+dy*dy+dz*dz < (50/4)*(50/4)) {
		stSetSkelAnimeNumber(1);
		obj_mode = 0;
		obj_worldX = obj_attX;
		obj_worldY = obj_attY;
		obj_worldZ = obj_attZ;
	}




}
/****************************************************************************************************
						Butterfly Event
***************************************************************************************************/

extern void s_butterflyfunc(void)
{

	switch (obj_mode) {
		case 0:	ButterflyStay();			break;
		case 1:	ButterflyTakeOff(); 		break;
		case 2:	ButterflyComeback(); 		break;


	}

	PlayerApproachOnOff(execstp,3000);
}










/*################*/
#endif
/*################*/


