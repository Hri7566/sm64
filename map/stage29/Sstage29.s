/********************************************************************************
						Ultra 64 MARIO Brothers

						stage 29 sequence module

			Copyright 1995 Nintendo co., ltd.  All rights reserved

							 Mar 15, 1996
 ********************************************************************************/

#define	ASSEMBLER

#include "../../headers.h"

		.data
		.align	2
		.align	0

		.globl	SEQ_DoStage29


/* ================================================================================
		: Enemies sequence.
===================================================================================	*/
SEQ_Stage29_Enemies:

		seqActor(S_bikkuri_button,	0,-2047,10,  0,0,0,  0,0,0,  e_bikkuri_switch)

		seqReturn


/* ================================================================================
		: Stars sequence.
===================================================================================	*/
SEQ_Stage29_Stars:

		seqActor(S_NULL,  800, -1700, 0,  0,0,0,  0,0,0,  e_tripstar_getcoins) /* 8 coins  */

		seqReturn


/* ================================================================================
		: Stage 29 main sequence.
===================================================================================	*/
SEQ_DoStage29:

	seqInitStage()
	seqLoadPres(SEGMENT_STAGE1	  , _GfxStage29SegmentRomStart	  , _GfxStage29SegmentRomEnd	)
	seqLoadPres(SEGMENT_BACK	  , _BackMountainSegmentRomStart  , _BackMountainSegmentRomEnd 	)
	seqLoadText(SEGMENT_TEXTURE	  , _EMtTextureSegmentRomStart	  , _EMtTextureSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY1	  , _GfxEnemy1_hSegmentRomStart	  , _GfxEnemy1_hSegmentRomEnd	)
	seqLoadData(SEGMENT_ENEMYDATA1, _HmsEnemy1_hSegmentRomStart	  , _HmsEnemy1_hSegmentRomEnd	)
	seqLoadPres(SEGMENT_ENEMY3	  , _GfxBasic_enemySegmentRomStart, _GfxBasic_enemySegmentRomEnd)
	seqLoadData(SEGMENT_ENEMYDATA3, _HmsBasic_enemySegmentRomStart, _HmsBasic_enemySegmentRomEnd)

	seqBeginConstruction()

		seqHmsMario(S_Mario, ShapePlayer1, e_mario)
		seqCall(SetBasicEnemy)
		seqCall(SetEnemy1_h)

		seqHmsShape(S_bg01, RCP_Ext6Cloud)

		seqBeginScene(1, RCP_Stage29Scene1)
			seqActor(S_NULL,  -4095,2935,0,  0,90,0,  0,10,0,  e_player_flight)
			seqPort(10, 29, 1, 10)			/*	MARIO stage in.	*/
			seqCourseOut(6, 1, 32)			/*	Exit.			*/
			seqGameClear(6, 1, 38)			/*	Get trip star.	*/
			seqGameOver (6, 1, 35)			/*	Down. 			*/
			seqCall(SEQ_Stage29_Stars)
			seqCall(SEQ_Stage29_Enemies)
			seqMapInfo(cx2901_info)
			seqTagInfo(cx2901_info_tag)
			seqMessage(SEQ_MESG_ENTRANT, 131)
			seqSetMusic(NA_STG_GROUND, NA_SLIDER_BGM)
			seqEnvironment(ENV_ROCKMT)
		seqEndScene()

	seqEndConstruction()

	seqEnterMario(1, 90,  -4095,2935,0)
	seqCProgram(GameProcess, 0)
	seqRunning(GameProcess, 1)
	seqDestroyStage()
	seqFreeze(1)
	seqExit
